# Greeting
#echo "Welcome to Arch Star"

# Prompt
PROMPT="%F{red}┌[%f%F{cyan}%m%f%F{red}]─[%f%F{yellow}%D{%H:%M-%d/%m}%f%F{red}]─[%f%F{magenta}%d%f%F{red}]%f"$'\n'"%F{red}└╼%f%F{green}$USER%f%F{yellow}$%f"
# Export PATH$
export PATH=~/.local/bin:/snap/bin:/usr/sandbox/:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/usr/share/games:/usr/local/sbin:/usr/sbin:/sbin:$PATH

#####################################################
# Auto completion / suggestion -> Fish-like suggestion for command history
source ~/.config/.archstar/zsh-autosuggestions/zsh-autosuggestions.zsh
#####source ~/zsh-autocomplete/zsh-autocomplete.plugin.zsh
source ~/.oh-my-zsh/mini-oh-my-zsh.sh
# Select all suggestion instead of top on result only
zstyle ':autocomplete:tab:*' insert-unambiguous yes
zstyle ':autocomplete:tab:*' widget-style menu-select
zstyle ':autocomplete:*' min-input 2
#bindkey $key[Up] up-line-or-history
#bindkey $key[Down] down-line-or-history
# Fish like syntax highlighting
source ~/.config/.archstar/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Save type history for completion and easier life
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory

# Useful alias for benchmarking programs
# require install package "time" sudo apt install time
# alias time="/usr/bin/time -f '\t%E real,\t%U user,\t%S sys,\t%K amem,\t%M mmem'"
# Display last command interminal
echo -en "\e]2;Arch Star\a"
preexec () { print -Pn "\e]0;$1 - Arch Star\a" }



# alias
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias sudos="doas "
alias ram="~/./swapf"
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'
alias jctl="journalctl -p 3 -xb"
#zplug "dracula/zsh", as:theme
#force_color_prompt = yes
alias darkmpv=cat " mpv --vf=negate --hwdec=no "
function b(){
pkexec echo "$1" > /sys/class/backlight/acpi_video0/brightness
}
function hex-encode()
{
  echo "$@" | xxd -p
}

function hex-decode()
{
  echo "$@" | xxd -p -r
}

function rot13()
{
  echo "$@" | tr 'A-Za-z' 'N-ZA-Mn-za-m'
}

function by() {
	echo "gcc compiling your c++...
	"
	g++ "$1" &&
	       	./a.out &&
	echo "------EOL-------"
}
function py(){
	echo "SOL" &&
	py "$1" &&
		echo "EOL"

}
function rr(){
	sudo pacman -Sy
	reflector --verbose --sort delay -l 30 --save /etc/pacman.d/mirrorlist
	sudo pacman -Sy
	sudo pacman -Syyuu
}
function sp(){
	sudo fallocate -l 4G /swapfile &&
	sudo chmod 600 /swapfile &&
	ls -lh /swapfile &&
	sudo mkswap /swapfile &&
	sudo swapon /swapfile &&
	swapon -s

}
function mk(){
	sudo pacman -Sy linux-zen linux-zen-headers linux-lts linux-lts-headers &&
	sudo grub-mkconfig -o /boot/grub/grub.cfg
}
function lg() {
    git add . 
    git commit -m "$1"
    git push
            
}
function xs() {
    sudo -S /opt/lampp/lampp start
}

function xr() {
    sudo -S /opt/lampp/lampp reload
    #echo ricm | sudo -S /opt/lampp/lampp reload
}

function xss() {
    sudo -S /opt/lampp/lampp stop
}

function upgrade() {
    sudo -S pacman -Syyuu
}

function 20m() {
   sleep 20m  && espeak -s 150 "sir your 20 min is over" && xcowsay "20 min over" && 20m
}
function pacfix() {
	sudo rm /var/lib/pacman/db.lck
}
function tc(){
sudo timeshift --create
}
function fireoff(){
 systemctl stop firewalld &&
 sudo ufw allow 1714:1764/udp &&
 sudo ufw reload

}
function mnt(){
sudo mount /dev/sda1 /mnt/video

}

function paste() {
              local file=${1:-/dev/stdin}
              curl --data-binary @${file} https://paste.rs
}
function sm(){
		~/.config/.archstar/game/cashclear
}
function apex(){
xdg-open steam://run/1172470
}
alias pb="nc termbin.com 9999"
function awei(){
bluetoothctl connect 45:A6:63:27:9F:2F

echo "power on  |  agent on  |  list"
echo "devices    | default-agent    |    trust A0:E9:AF:10:62:3F    |    pair A0:E9:AF:10:62:3F    |    connect A0:E9:AF:10:62:3F "

}

ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#~/.config/.archstar/./fetch.sh 2> /dev/null
fastfetch
