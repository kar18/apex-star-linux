#!/usr/bin/env bash

## Author  : kazi ar rafi
## Mail    : kazi-ar-rafi@protonmail.com
## Gitlab  : kar18

# Available Styles
# >> Created and tested on : rofi 1.7.0-1
#
# style_1     style_2     style_3     style_4     style_5     style_6
# style_7     style_8     style_9     style_10    style_11    style_12

theme="style_1"
dir="$HOME/.config/rofi/colorful"

# dark
ALPHA="#00000000"
BG="#282a36"
FG="#f8f8f2"
SELECT="#101010ff"

# light
#ALPHA="#00000000"
#BG="#FFFFFFff"
#FG="#000000ff"
#SELECT="#f3f3f3ff"

# accent colors
COLORS=('#ff5555' '#50fa7b' '#ff79c6' '#8be9fd' '#BA68C8' '#4DD0E1' '#00B19F' \
		'#bd93f9' '#ffb86c' '#AC8476' '#6D8895' '#EC407A' '#B9C244' '#6272a4')
ACCENT="${COLORS[$(( $RANDOM % 14 ))]}ff"

# overwrite colors file
cat > $dir/colors.rasi <<- EOF
	/* colors */

	* {
	  al:  $ALPHA;
	  bg:  $BG;
	  se:  $SELECT;
	  fg:  $FG;
	  ac:  $ACCENT;
	}
EOF

# comment these lines to disable random style
themes=($(ls -p --hide="launcher.sh" --hide="colors.rasi" $dir))
theme="${themes[$(( $RANDOM % 5 ))]}"


rofi -no-lazy-grab -show drun -modi drun -theme $dir/"$theme"
