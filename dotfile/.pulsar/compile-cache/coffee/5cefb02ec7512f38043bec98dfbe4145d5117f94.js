(function() {
  var CompositeDisposable, Disposable, Emitter, NotificationsLogItem, moment, ref;

  ref = require('atom'), Emitter = ref.Emitter, CompositeDisposable = ref.CompositeDisposable, Disposable = ref.Disposable;

  moment = require('moment');

  module.exports = NotificationsLogItem = (function() {
    NotificationsLogItem.prototype.subscriptions = null;

    NotificationsLogItem.prototype.timestampInterval = null;

    function NotificationsLogItem(notification) {
      this.notification = notification;
      this.emitter = new Emitter;
      this.subscriptions = new CompositeDisposable;
      this.render();
    }

    NotificationsLogItem.prototype.render = function() {
      var notificationElement, notificationView;
      notificationView = atom.views.getView(this.notification);
      notificationElement = this.renderNotification(notificationView);
      this.timestamp = document.createElement('div');
      this.timestamp.classList.add('timestamp');
      this.notification.moment = moment(this.notification.getTimestamp());
      this.subscriptions.add(atom.tooltips.add(this.timestamp, {
        title: this.notification.moment.format("ll LTS")
      }));
      this.updateTimestamp();
      this.timestampInterval = setInterval(this.updateTimestamp.bind(this), 60 * 1000);
      this.subscriptions.add(new Disposable((function(_this) {
        return function() {
          return clearInterval(_this.timestampInterval);
        };
      })(this)));
      this.element = document.createElement('li');
      this.element.classList.add('notifications-log-item', this.notification.getType());
      this.element.appendChild(notificationElement);
      this.element.appendChild(this.timestamp);
      this.element.addEventListener('click', (function(_this) {
        return function(e) {
          if (e.target.closest('.btn-toolbar a, .btn-toolbar button') == null) {
            return _this.emitter.emit('click');
          }
        };
      })(this));
      this.element.getRenderPromise = function() {
        return notificationView.getRenderPromise();
      };
      if (this.notification.getType() === 'fatal') {
        notificationView.getRenderPromise().then((function(_this) {
          return function() {
            return _this.element.replaceChild(_this.renderNotification(notificationView), notificationElement);
          };
        })(this));
      }
      return this.subscriptions.add(new Disposable((function(_this) {
        return function() {
          return _this.element.remove();
        };
      })(this)));
    };

    NotificationsLogItem.prototype.renderNotification = function(view) {
      var button, buttons, i, j, len, len1, logButton, message, nButtons, nElement, ref1, ref2, tooltip;
      message = document.createElement('div');
      message.classList.add('message');
      message.innerHTML = view.element.querySelector(".content > .message").innerHTML;
      buttons = document.createElement('div');
      buttons.classList.add('btn-toolbar');
      nButtons = view.element.querySelector(".content > .meta > .btn-toolbar");
      if (nButtons != null) {
        ref1 = nButtons.children;
        for (i = 0, len = ref1.length; i < len; i++) {
          button = ref1[i];
          logButton = button.cloneNode(true);
          logButton.originalButton = button;
          logButton.addEventListener('click', function(e) {
            var newEvent;
            newEvent = new MouseEvent('click', e);
            return e.target.originalButton.dispatchEvent(newEvent);
          });
          ref2 = atom.tooltips.findTooltips(button);
          for (j = 0, len1 = ref2.length; j < len1; j++) {
            tooltip = ref2[j];
            this.subscriptions.add(atom.tooltips.add(logButton, tooltip.options));
          }
          buttons.appendChild(logButton);
        }
      }
      nElement = document.createElement('div');
      nElement.classList.add('notifications-log-notification', 'icon', "icon-" + (this.notification.getIcon()), this.notification.getType());
      nElement.appendChild(message);
      nElement.appendChild(buttons);
      return nElement;
    };

    NotificationsLogItem.prototype.getElement = function() {
      return this.element;
    };

    NotificationsLogItem.prototype.destroy = function() {
      this.subscriptions.dispose();
      return this.emitter.emit('did-destroy');
    };

    NotificationsLogItem.prototype.onClick = function(callback) {
      return this.emitter.on('click', callback);
    };

    NotificationsLogItem.prototype.onDidDestroy = function(callback) {
      return this.emitter.on('did-destroy', callback);
    };

    NotificationsLogItem.prototype.updateTimestamp = function() {
      return this.timestamp.textContent = this.notification.moment.fromNow();
    };

    return NotificationsLogItem;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9ub3RpZmljYXRpb25zL2xpYi9ub3RpZmljYXRpb25zLWxvZy1pdGVtLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsTUFBNkMsT0FBQSxDQUFRLE1BQVIsQ0FBN0MsRUFBQyxxQkFBRCxFQUFVLDZDQUFWLEVBQStCOztFQUMvQixNQUFBLEdBQVMsT0FBQSxDQUFRLFFBQVI7O0VBRVQsTUFBTSxDQUFDLE9BQVAsR0FBdUI7bUNBQ3JCLGFBQUEsR0FBZTs7bUNBQ2YsaUJBQUEsR0FBbUI7O0lBRU4sOEJBQUMsWUFBRDtNQUFDLElBQUMsQ0FBQSxlQUFEO01BQ1osSUFBQyxDQUFBLE9BQUQsR0FBVyxJQUFJO01BQ2YsSUFBQyxDQUFBLGFBQUQsR0FBaUIsSUFBSTtNQUNyQixJQUFDLENBQUEsTUFBRCxDQUFBO0lBSFc7O21DQUtiLE1BQUEsR0FBUSxTQUFBO0FBQ04sVUFBQTtNQUFBLGdCQUFBLEdBQW1CLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBWCxDQUFtQixJQUFDLENBQUEsWUFBcEI7TUFDbkIsbUJBQUEsR0FBc0IsSUFBQyxDQUFBLGtCQUFELENBQW9CLGdCQUFwQjtNQUV0QixJQUFDLENBQUEsU0FBRCxHQUFhLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO01BQ2IsSUFBQyxDQUFBLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBckIsQ0FBeUIsV0FBekI7TUFDQSxJQUFDLENBQUEsWUFBWSxDQUFDLE1BQWQsR0FBdUIsTUFBQSxDQUFPLElBQUMsQ0FBQSxZQUFZLENBQUMsWUFBZCxDQUFBLENBQVA7TUFDdkIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixJQUFDLENBQUEsU0FBbkIsRUFBOEI7UUFBQSxLQUFBLEVBQU8sSUFBQyxDQUFBLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBckIsQ0FBNEIsUUFBNUIsQ0FBUDtPQUE5QixDQUFuQjtNQUNBLElBQUMsQ0FBQSxlQUFELENBQUE7TUFDQSxJQUFDLENBQUEsaUJBQUQsR0FBcUIsV0FBQSxDQUFZLElBQUMsQ0FBQSxlQUFlLENBQUMsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBWixFQUF5QyxFQUFBLEdBQUssSUFBOUM7TUFDckIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksVUFBSixDQUFlLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxhQUFBLENBQWMsS0FBQyxDQUFBLGlCQUFmO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWYsQ0FBbkI7TUFFQSxJQUFDLENBQUEsT0FBRCxHQUFXLFFBQVEsQ0FBQyxhQUFULENBQXVCLElBQXZCO01BQ1gsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsd0JBQXZCLEVBQWlELElBQUMsQ0FBQSxZQUFZLENBQUMsT0FBZCxDQUFBLENBQWpEO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLG1CQUFyQjtNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixJQUFDLENBQUEsU0FBdEI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxDQUFEO1VBQ2pDLElBQU8sK0RBQVA7bUJBQ0UsS0FBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsT0FBZCxFQURGOztRQURpQztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBbkM7TUFJQSxJQUFDLENBQUEsT0FBTyxDQUFDLGdCQUFULEdBQTRCLFNBQUE7ZUFBRyxnQkFBZ0IsQ0FBQyxnQkFBakIsQ0FBQTtNQUFIO01BQzVCLElBQUcsSUFBQyxDQUFBLFlBQVksQ0FBQyxPQUFkLENBQUEsQ0FBQSxLQUEyQixPQUE5QjtRQUNFLGdCQUFnQixDQUFDLGdCQUFqQixDQUFBLENBQW1DLENBQUMsSUFBcEMsQ0FBeUMsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFDdkMsS0FBQyxDQUFBLE9BQU8sQ0FBQyxZQUFULENBQXNCLEtBQUMsQ0FBQSxrQkFBRCxDQUFvQixnQkFBcEIsQ0FBdEIsRUFBNkQsbUJBQTdEO1VBRHVDO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6QyxFQURGOzthQUlBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLFVBQUosQ0FBZSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQU8sQ0FBQyxNQUFULENBQUE7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBZixDQUFuQjtJQXpCTTs7bUNBMkJSLGtCQUFBLEdBQW9CLFNBQUMsSUFBRDtBQUNsQixVQUFBO01BQUEsT0FBQSxHQUFVLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO01BQ1YsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFsQixDQUFzQixTQUF0QjtNQUNBLE9BQU8sQ0FBQyxTQUFSLEdBQW9CLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYixDQUEyQixxQkFBM0IsQ0FBaUQsQ0FBQztNQUV0RSxPQUFBLEdBQVUsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDVixPQUFPLENBQUMsU0FBUyxDQUFDLEdBQWxCLENBQXNCLGFBQXRCO01BQ0EsUUFBQSxHQUFXLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYixDQUEyQixpQ0FBM0I7TUFDWCxJQUFHLGdCQUFIO0FBQ0U7QUFBQSxhQUFBLHNDQUFBOztVQUNFLFNBQUEsR0FBWSxNQUFNLENBQUMsU0FBUCxDQUFpQixJQUFqQjtVQUNaLFNBQVMsQ0FBQyxjQUFWLEdBQTJCO1VBQzNCLFNBQVMsQ0FBQyxnQkFBVixDQUEyQixPQUEzQixFQUFvQyxTQUFDLENBQUQ7QUFDbEMsZ0JBQUE7WUFBQSxRQUFBLEdBQVcsSUFBSSxVQUFKLENBQWUsT0FBZixFQUF3QixDQUF4QjttQkFDWCxDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxhQUF4QixDQUFzQyxRQUF0QztVQUZrQyxDQUFwQztBQUdBO0FBQUEsZUFBQSx3Q0FBQTs7WUFDRSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLFNBQWxCLEVBQTZCLE9BQU8sQ0FBQyxPQUFyQyxDQUFuQjtBQURGO1VBRUEsT0FBTyxDQUFDLFdBQVIsQ0FBb0IsU0FBcEI7QUFSRixTQURGOztNQVdBLFFBQUEsR0FBVyxRQUFRLENBQUMsYUFBVCxDQUF1QixLQUF2QjtNQUNYLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsZ0NBQXZCLEVBQXlELE1BQXpELEVBQWlFLE9BQUEsR0FBTyxDQUFDLElBQUMsQ0FBQSxZQUFZLENBQUMsT0FBZCxDQUFBLENBQUQsQ0FBeEUsRUFBb0csSUFBQyxDQUFBLFlBQVksQ0FBQyxPQUFkLENBQUEsQ0FBcEc7TUFDQSxRQUFRLENBQUMsV0FBVCxDQUFxQixPQUFyQjtNQUNBLFFBQVEsQ0FBQyxXQUFULENBQXFCLE9BQXJCO2FBQ0E7SUF2QmtCOzttQ0F5QnBCLFVBQUEsR0FBWSxTQUFBO2FBQUcsSUFBQyxDQUFBO0lBQUo7O21DQUVaLE9BQUEsR0FBUyxTQUFBO01BQ1AsSUFBQyxDQUFBLGFBQWEsQ0FBQyxPQUFmLENBQUE7YUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxhQUFkO0lBRk87O21DQUlULE9BQUEsR0FBUyxTQUFDLFFBQUQ7YUFDUCxJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxPQUFaLEVBQXFCLFFBQXJCO0lBRE87O21DQUdULFlBQUEsR0FBYyxTQUFDLFFBQUQ7YUFDWixJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxhQUFaLEVBQTJCLFFBQTNCO0lBRFk7O21DQUdkLGVBQUEsR0FBaUIsU0FBQTthQUNmLElBQUMsQ0FBQSxTQUFTLENBQUMsV0FBWCxHQUF5QixJQUFDLENBQUEsWUFBWSxDQUFDLE1BQU0sQ0FBQyxPQUFyQixDQUFBO0lBRFY7Ozs7O0FBNUVuQiIsInNvdXJjZXNDb250ZW50IjpbIntFbWl0dGVyLCBDb21wb3NpdGVEaXNwb3NhYmxlLCBEaXNwb3NhYmxlfSA9IHJlcXVpcmUgJ2F0b20nXG5tb21lbnQgPSByZXF1aXJlICdtb21lbnQnXG5cbm1vZHVsZS5leHBvcnRzID0gY2xhc3MgTm90aWZpY2F0aW9uc0xvZ0l0ZW1cbiAgc3Vic2NyaXB0aW9uczogbnVsbFxuICB0aW1lc3RhbXBJbnRlcnZhbDogbnVsbFxuXG4gIGNvbnN0cnVjdG9yOiAoQG5vdGlmaWNhdGlvbikgLT5cbiAgICBAZW1pdHRlciA9IG5ldyBFbWl0dGVyXG4gICAgQHN1YnNjcmlwdGlvbnMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZVxuICAgIEByZW5kZXIoKVxuXG4gIHJlbmRlcjogLT5cbiAgICBub3RpZmljYXRpb25WaWV3ID0gYXRvbS52aWV3cy5nZXRWaWV3KEBub3RpZmljYXRpb24pXG4gICAgbm90aWZpY2F0aW9uRWxlbWVudCA9IEByZW5kZXJOb3RpZmljYXRpb24obm90aWZpY2F0aW9uVmlldylcblxuICAgIEB0aW1lc3RhbXAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIEB0aW1lc3RhbXAuY2xhc3NMaXN0LmFkZCgndGltZXN0YW1wJylcbiAgICBAbm90aWZpY2F0aW9uLm1vbWVudCA9IG1vbWVudChAbm90aWZpY2F0aW9uLmdldFRpbWVzdGFtcCgpKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLnRvb2x0aXBzLmFkZChAdGltZXN0YW1wLCB0aXRsZTogQG5vdGlmaWNhdGlvbi5tb21lbnQuZm9ybWF0KFwibGwgTFRTXCIpKVxuICAgIEB1cGRhdGVUaW1lc3RhbXAoKVxuICAgIEB0aW1lc3RhbXBJbnRlcnZhbCA9IHNldEludGVydmFsKEB1cGRhdGVUaW1lc3RhbXAuYmluZCh0aGlzKSwgNjAgKiAxMDAwKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBuZXcgRGlzcG9zYWJsZSA9PiBjbGVhckludGVydmFsIEB0aW1lc3RhbXBJbnRlcnZhbFxuXG4gICAgQGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdsaScpXG4gICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnbm90aWZpY2F0aW9ucy1sb2ctaXRlbScsIEBub3RpZmljYXRpb24uZ2V0VHlwZSgpKVxuICAgIEBlbGVtZW50LmFwcGVuZENoaWxkKG5vdGlmaWNhdGlvbkVsZW1lbnQpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQHRpbWVzdGFtcClcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyICdjbGljaycsIChlKSA9PlxuICAgICAgdW5sZXNzIGUudGFyZ2V0LmNsb3Nlc3QoJy5idG4tdG9vbGJhciBhLCAuYnRuLXRvb2xiYXIgYnV0dG9uJyk/XG4gICAgICAgIEBlbWl0dGVyLmVtaXQgJ2NsaWNrJ1xuXG4gICAgQGVsZW1lbnQuZ2V0UmVuZGVyUHJvbWlzZSA9IC0+IG5vdGlmaWNhdGlvblZpZXcuZ2V0UmVuZGVyUHJvbWlzZSgpXG4gICAgaWYgQG5vdGlmaWNhdGlvbi5nZXRUeXBlKCkgaXMgJ2ZhdGFsJ1xuICAgICAgbm90aWZpY2F0aW9uVmlldy5nZXRSZW5kZXJQcm9taXNlKCkudGhlbiA9PlxuICAgICAgICBAZWxlbWVudC5yZXBsYWNlQ2hpbGQoQHJlbmRlck5vdGlmaWNhdGlvbihub3RpZmljYXRpb25WaWV3KSwgbm90aWZpY2F0aW9uRWxlbWVudClcblxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBuZXcgRGlzcG9zYWJsZSA9PiBAZWxlbWVudC5yZW1vdmUoKVxuXG4gIHJlbmRlck5vdGlmaWNhdGlvbjogKHZpZXcpIC0+XG4gICAgbWVzc2FnZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgbWVzc2FnZS5jbGFzc0xpc3QuYWRkKCdtZXNzYWdlJylcbiAgICBtZXNzYWdlLmlubmVySFRNTCA9IHZpZXcuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFwiLmNvbnRlbnQgPiAubWVzc2FnZVwiKS5pbm5lckhUTUxcblxuICAgIGJ1dHRvbnMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIGJ1dHRvbnMuY2xhc3NMaXN0LmFkZCgnYnRuLXRvb2xiYXInKVxuICAgIG5CdXR0b25zID0gdmlldy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuY29udGVudCA+IC5tZXRhID4gLmJ0bi10b29sYmFyXCIpXG4gICAgaWYgbkJ1dHRvbnM/XG4gICAgICBmb3IgYnV0dG9uIGluIG5CdXR0b25zLmNoaWxkcmVuXG4gICAgICAgIGxvZ0J1dHRvbiA9IGJ1dHRvbi5jbG9uZU5vZGUodHJ1ZSlcbiAgICAgICAgbG9nQnV0dG9uLm9yaWdpbmFsQnV0dG9uID0gYnV0dG9uXG4gICAgICAgIGxvZ0J1dHRvbi5hZGRFdmVudExpc3RlbmVyICdjbGljaycsIChlKSAtPlxuICAgICAgICAgIG5ld0V2ZW50ID0gbmV3IE1vdXNlRXZlbnQoJ2NsaWNrJywgZSlcbiAgICAgICAgICBlLnRhcmdldC5vcmlnaW5hbEJ1dHRvbi5kaXNwYXRjaEV2ZW50KG5ld0V2ZW50KVxuICAgICAgICBmb3IgdG9vbHRpcCBpbiBhdG9tLnRvb2x0aXBzLmZpbmRUb29sdGlwcyBidXR0b25cbiAgICAgICAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS50b29sdGlwcy5hZGQobG9nQnV0dG9uLCB0b29sdGlwLm9wdGlvbnMpXG4gICAgICAgIGJ1dHRvbnMuYXBwZW5kQ2hpbGQobG9nQnV0dG9uKVxuXG4gICAgbkVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIG5FbGVtZW50LmNsYXNzTGlzdC5hZGQoJ25vdGlmaWNhdGlvbnMtbG9nLW5vdGlmaWNhdGlvbicsICdpY29uJywgXCJpY29uLSN7QG5vdGlmaWNhdGlvbi5nZXRJY29uKCl9XCIsIEBub3RpZmljYXRpb24uZ2V0VHlwZSgpKVxuICAgIG5FbGVtZW50LmFwcGVuZENoaWxkKG1lc3NhZ2UpXG4gICAgbkVsZW1lbnQuYXBwZW5kQ2hpbGQoYnV0dG9ucylcbiAgICBuRWxlbWVudFxuXG4gIGdldEVsZW1lbnQ6IC0+IEBlbGVtZW50XG5cbiAgZGVzdHJveTogLT5cbiAgICBAc3Vic2NyaXB0aW9ucy5kaXNwb3NlKClcbiAgICBAZW1pdHRlci5lbWl0ICdkaWQtZGVzdHJveSdcblxuICBvbkNsaWNrOiAoY2FsbGJhY2spIC0+XG4gICAgQGVtaXR0ZXIub24gJ2NsaWNrJywgY2FsbGJhY2tcblxuICBvbkRpZERlc3Ryb3k6IChjYWxsYmFjaykgLT5cbiAgICBAZW1pdHRlci5vbiAnZGlkLWRlc3Ryb3knLCBjYWxsYmFja1xuXG4gIHVwZGF0ZVRpbWVzdGFtcDogLT5cbiAgICBAdGltZXN0YW1wLnRleHRDb250ZW50ID0gQG5vdGlmaWNhdGlvbi5tb21lbnQuZnJvbU5vdygpXG4iXX0=
