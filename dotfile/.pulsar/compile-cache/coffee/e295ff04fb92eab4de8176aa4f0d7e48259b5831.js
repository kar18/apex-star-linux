(function() {
  var CompositeDisposable, Disposable, TabView, getIconServices, layout, path, ref;

  path = require('path');

  ref = require('atom'), Disposable = ref.Disposable, CompositeDisposable = ref.CompositeDisposable;

  getIconServices = require('./get-icon-services');

  layout = require('./layout');

  module.exports = TabView = (function() {
    function TabView(arg) {
      var base, closeIcon, didClickCloseIcon, location;
      this.item = arg.item, this.pane = arg.pane, didClickCloseIcon = arg.didClickCloseIcon, this.tabs = arg.tabs, location = arg.location;
      if (typeof this.item.getPath === 'function') {
        this.path = this.item.getPath();
      }
      this.element = document.createElement('li');
      this.element.setAttribute('is', 'tabs-tab');
      if (['TextEditor', 'TestView'].indexOf(this.item.constructor.name) > -1) {
        this.element.classList.add('texteditor');
      }
      this.element.classList.add('tab', 'sortable');
      this.itemTitle = document.createElement('div');
      this.itemTitle.classList.add('title');
      this.element.appendChild(this.itemTitle);
      if (location === 'center' || !(typeof (base = this.item).isPermanentDockItem === "function" ? base.isPermanentDockItem() : void 0)) {
        closeIcon = document.createElement('div');
        closeIcon.classList.add('close-icon');
        closeIcon.onclick = didClickCloseIcon;
        this.element.appendChild(closeIcon);
      }
      this.subscriptions = new CompositeDisposable();
      this.handleEvents();
      this.updateDataAttributes();
      this.updateTitle();
      this.updateIcon();
      this.updateModifiedStatus();
      this.setupTooltip();
      if (this.isItemPending()) {
        this.itemTitle.classList.add('temp');
        this.element.classList.add('pending-tab');
      }
      this.element.ondrag = function(e) {
        return layout.drag(e);
      };
      this.element.ondragend = function(e) {
        return layout.end(e);
      };
      this.element.pane = this.pane;
      this.element.item = this.item;
      this.element.itemTitle = this.itemTitle;
      this.element.path = this.path;
    }

    TabView.prototype.handleEvents = function() {
      var base, iconChangedHandler, modifiedHandler, onDidChangeIconDisposable, onDidChangeModifiedDisposable, onDidChangePathDisposable, onDidChangeTitleDisposable, onDidSaveDisposable, pathChangedHandler, titleChangedHandler;
      titleChangedHandler = (function(_this) {
        return function() {
          return _this.updateTitle();
        };
      })(this);
      this.subscriptions.add(this.pane.onDidDestroy((function(_this) {
        return function() {
          return _this.destroy();
        };
      })(this)));
      this.subscriptions.add(this.pane.onItemDidTerminatePendingState((function(_this) {
        return function(item) {
          if (item === _this.item) {
            return _this.clearPending();
          }
        };
      })(this)));
      if (typeof this.item.onDidChangeTitle === 'function') {
        onDidChangeTitleDisposable = this.item.onDidChangeTitle(titleChangedHandler);
        if (Disposable.isDisposable(onDidChangeTitleDisposable)) {
          this.subscriptions.add(onDidChangeTitleDisposable);
        } else {
          console.warn("::onDidChangeTitle does not return a valid Disposable!", this.item);
        }
      } else if (typeof this.item.on === 'function') {
        this.item.on('title-changed', titleChangedHandler);
        this.subscriptions.add({
          dispose: (function(_this) {
            return function() {
              var base;
              return typeof (base = _this.item).off === "function" ? base.off('title-changed', titleChangedHandler) : void 0;
            };
          })(this)
        });
      }
      pathChangedHandler = (function(_this) {
        return function(path1) {
          _this.path = path1;
          _this.updateDataAttributes();
          _this.updateTitle();
          _this.updateTooltip();
          return _this.updateIcon();
        };
      })(this);
      if (typeof this.item.onDidChangePath === 'function') {
        onDidChangePathDisposable = this.item.onDidChangePath(pathChangedHandler);
        if (Disposable.isDisposable(onDidChangePathDisposable)) {
          this.subscriptions.add(onDidChangePathDisposable);
        } else {
          console.warn("::onDidChangePath does not return a valid Disposable!", this.item);
        }
      } else if (typeof this.item.on === 'function') {
        this.item.on('path-changed', pathChangedHandler);
        this.subscriptions.add({
          dispose: (function(_this) {
            return function() {
              var base;
              return typeof (base = _this.item).off === "function" ? base.off('path-changed', pathChangedHandler) : void 0;
            };
          })(this)
        });
      }
      iconChangedHandler = (function(_this) {
        return function() {
          return _this.updateIcon();
        };
      })(this);
      this.subscriptions.add(getIconServices().onDidChange((function(_this) {
        return function() {
          return _this.updateIcon();
        };
      })(this)));
      if (typeof this.item.onDidChangeIcon === 'function') {
        onDidChangeIconDisposable = typeof (base = this.item).onDidChangeIcon === "function" ? base.onDidChangeIcon((function(_this) {
          return function() {
            return _this.updateIcon();
          };
        })(this)) : void 0;
        if (Disposable.isDisposable(onDidChangeIconDisposable)) {
          this.subscriptions.add(onDidChangeIconDisposable);
        } else {
          console.warn("::onDidChangeIcon does not return a valid Disposable!", this.item);
        }
      } else if (typeof this.item.on === 'function') {
        this.item.on('icon-changed', iconChangedHandler);
        this.subscriptions.add({
          dispose: (function(_this) {
            return function() {
              var base1;
              return typeof (base1 = _this.item).off === "function" ? base1.off('icon-changed', iconChangedHandler) : void 0;
            };
          })(this)
        });
      }
      modifiedHandler = (function(_this) {
        return function() {
          return _this.updateModifiedStatus();
        };
      })(this);
      if (typeof this.item.onDidChangeModified === 'function') {
        onDidChangeModifiedDisposable = this.item.onDidChangeModified(modifiedHandler);
        if (Disposable.isDisposable(onDidChangeModifiedDisposable)) {
          this.subscriptions.add(onDidChangeModifiedDisposable);
        } else {
          console.warn("::onDidChangeModified does not return a valid Disposable!", this.item);
        }
      } else if (typeof this.item.on === 'function') {
        this.item.on('modified-status-changed', modifiedHandler);
        this.subscriptions.add({
          dispose: (function(_this) {
            return function() {
              var base1;
              return typeof (base1 = _this.item).off === "function" ? base1.off('modified-status-changed', modifiedHandler) : void 0;
            };
          })(this)
        });
      }
      if (typeof this.item.onDidSave === 'function') {
        onDidSaveDisposable = this.item.onDidSave((function(_this) {
          return function(event) {
            _this.terminatePendingState();
            if (event.path !== _this.path) {
              _this.path = event.path;
              if (atom.config.get('tabs.enableVcsColoring')) {
                return _this.setupVcsStatus();
              }
            }
          };
        })(this));
        if (Disposable.isDisposable(onDidSaveDisposable)) {
          this.subscriptions.add(onDidSaveDisposable);
        } else {
          console.warn("::onDidSave does not return a valid Disposable!", this.item);
        }
      }
      this.subscriptions.add(atom.config.observe('tabs.showIcons', (function(_this) {
        return function() {
          return _this.updateIconVisibility();
        };
      })(this)));
      return this.subscriptions.add(atom.config.observe('tabs.enableVcsColoring', (function(_this) {
        return function(isEnabled) {
          if (isEnabled && (_this.path != null)) {
            return _this.setupVcsStatus();
          } else {
            return _this.unsetVcsStatus();
          }
        };
      })(this)));
    };

    TabView.prototype.setupTooltip = function() {
      var onMouseEnter;
      onMouseEnter = (function(_this) {
        return function() {
          _this.mouseEnterSubscription.dispose();
          _this.hasBeenMousedOver = true;
          _this.updateTooltip();
          return _this.element.dispatchEvent(new CustomEvent('mouseenter', {
            bubbles: true
          }));
        };
      })(this);
      this.mouseEnterSubscription = {
        dispose: (function(_this) {
          return function() {
            _this.element.removeEventListener('mouseenter', onMouseEnter);
            return _this.mouseEnterSubscription = null;
          };
        })(this)
      };
      return this.element.addEventListener('mouseenter', onMouseEnter);
    };

    TabView.prototype.updateTooltip = function() {
      if (!this.hasBeenMousedOver) {
        return;
      }
      this.destroyTooltip();
      if (this.path) {
        return this.tooltip = atom.tooltips.add(this.element, {
          title: this.path,
          html: false,
          delay: {
            show: 1000,
            hide: 100
          },
          placement: 'bottom'
        });
      }
    };

    TabView.prototype.destroyTooltip = function() {
      var ref1;
      if (!this.hasBeenMousedOver) {
        return;
      }
      return (ref1 = this.tooltip) != null ? ref1.dispose() : void 0;
    };

    TabView.prototype.destroy = function() {
      var ref1, ref2, ref3;
      if ((ref1 = this.subscriptions) != null) {
        ref1.dispose();
      }
      if ((ref2 = this.mouseEnterSubscription) != null) {
        ref2.dispose();
      }
      if ((ref3 = this.repoSubscriptions) != null) {
        ref3.dispose();
      }
      this.destroyTooltip();
      return this.element.remove();
    };

    TabView.prototype.updateDataAttributes = function() {
      var itemClass, ref1;
      if (this.path) {
        this.itemTitle.dataset.name = path.basename(this.path);
        this.itemTitle.dataset.path = this.path;
      } else {
        delete this.itemTitle.dataset.name;
        delete this.itemTitle.dataset.path;
      }
      if (itemClass = (ref1 = this.item.constructor) != null ? ref1.name : void 0) {
        return this.element.dataset.type = itemClass;
      } else {
        return delete this.element.dataset.type;
      }
    };

    TabView.prototype.updateTitle = function(arg) {
      var base, base1, i, len, ref1, ref2, ref3, ref4, tab, title, updateSiblings, useLongTitle;
      ref1 = arg != null ? arg : {}, updateSiblings = ref1.updateSiblings, useLongTitle = ref1.useLongTitle;
      if (this.updatingTitle) {
        return;
      }
      this.updatingTitle = true;
      if (updateSiblings === false) {
        title = this.item.getTitle();
        if (useLongTitle) {
          title = (ref2 = typeof (base = this.item).getLongTitle === "function" ? base.getLongTitle() : void 0) != null ? ref2 : title;
        }
        this.itemTitle.textContent = title;
      } else {
        title = this.item.getTitle();
        useLongTitle = false;
        ref3 = this.tabs;
        for (i = 0, len = ref3.length; i < len; i++) {
          tab = ref3[i];
          if (tab !== this) {
            if (tab.item.getTitle() === title) {
              tab.updateTitle({
                updateSiblings: false,
                useLongTitle: true
              });
              useLongTitle = true;
            }
          }
        }
        if (useLongTitle) {
          title = (ref4 = typeof (base1 = this.item).getLongTitle === "function" ? base1.getLongTitle() : void 0) != null ? ref4 : title;
        }
        this.itemTitle.textContent = title;
      }
      return this.updatingTitle = false;
    };

    TabView.prototype.updateIcon = function() {
      return getIconServices().updateTabIcon(this);
    };

    TabView.prototype.isItemPending = function() {
      if (this.pane.getPendingItem != null) {
        return this.pane.getPendingItem() === this.item;
      } else if (this.item.isPending != null) {
        return this.item.isPending();
      }
    };

    TabView.prototype.terminatePendingState = function() {
      if (this.pane.clearPendingItem != null) {
        if (this.pane.getPendingItem() === this.item) {
          return this.pane.clearPendingItem();
        }
      } else if (this.item.terminatePendingState != null) {
        return this.item.terminatePendingState();
      }
    };

    TabView.prototype.clearPending = function() {
      this.itemTitle.classList.remove('temp');
      return this.element.classList.remove('pending-tab');
    };

    TabView.prototype.updateIconVisibility = function() {
      if (atom.config.get('tabs.showIcons')) {
        return this.itemTitle.classList.remove('hide-icon');
      } else {
        return this.itemTitle.classList.add('hide-icon');
      }
    };

    TabView.prototype.updateModifiedStatus = function() {
      var base;
      if (typeof (base = this.item).isModified === "function" ? base.isModified() : void 0) {
        if (!this.isModified) {
          this.element.classList.add('modified');
        }
        return this.isModified = true;
      } else {
        if (this.isModified) {
          this.element.classList.remove('modified');
        }
        return this.isModified = false;
      }
    };

    TabView.prototype.setupVcsStatus = function() {
      if (this.path == null) {
        return;
      }
      return this.repoForPath(this.path).then((function(_this) {
        return function(repo) {
          _this.subscribeToRepo(repo);
          return _this.updateVcsStatus(repo);
        };
      })(this));
    };

    TabView.prototype.subscribeToRepo = function(repo) {
      var ref1;
      if (repo == null) {
        return;
      }
      if ((ref1 = this.repoSubscriptions) != null) {
        ref1.dispose();
      }
      this.repoSubscriptions = new CompositeDisposable();
      this.repoSubscriptions.add(repo.onDidChangeStatus((function(_this) {
        return function(event) {
          if (event.path === _this.path) {
            return _this.updateVcsStatus(repo, event.pathStatus);
          }
        };
      })(this)));
      return this.repoSubscriptions.add(repo.onDidChangeStatuses((function(_this) {
        return function() {
          return _this.updateVcsStatus(repo);
        };
      })(this)));
    };

    TabView.prototype.repoForPath = function() {
      var dir, i, len, ref1;
      ref1 = atom.project.getDirectories();
      for (i = 0, len = ref1.length; i < len; i++) {
        dir = ref1[i];
        if (dir.contains(this.path)) {
          return atom.project.repositoryForDirectory(dir);
        }
      }
      return Promise.resolve(null);
    };

    TabView.prototype.updateVcsStatus = function(repo, status) {
      var newStatus;
      if (repo == null) {
        return;
      }
      newStatus = null;
      if (repo.isPathIgnored(this.path)) {
        newStatus = 'ignored';
      } else {
        if (status == null) {
          status = repo.getCachedPathStatus(this.path);
        }
        if (repo.isStatusModified(status)) {
          newStatus = 'modified';
        } else if (repo.isStatusNew(status)) {
          newStatus = 'added';
        }
      }
      if (newStatus !== this.status) {
        this.status = newStatus;
        return this.updateVcsColoring();
      }
    };

    TabView.prototype.updateVcsColoring = function() {
      this.itemTitle.classList.remove('status-ignored', 'status-modified', 'status-added');
      if (this.status && atom.config.get('tabs.enableVcsColoring')) {
        return this.itemTitle.classList.add("status-" + this.status);
      }
    };

    TabView.prototype.unsetVcsStatus = function() {
      var ref1;
      if ((ref1 = this.repoSubscriptions) != null) {
        ref1.dispose();
      }
      delete this.status;
      return this.updateVcsColoring();
    };

    return TabView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90YWJzL2xpYi90YWItdmlldy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLElBQUEsR0FBTyxPQUFBLENBQVEsTUFBUjs7RUFDUCxNQUFvQyxPQUFBLENBQVEsTUFBUixDQUFwQyxFQUFDLDJCQUFELEVBQWE7O0VBQ2IsZUFBQSxHQUFrQixPQUFBLENBQVEscUJBQVI7O0VBRWxCLE1BQUEsR0FBUyxPQUFBLENBQVEsVUFBUjs7RUFFVCxNQUFNLENBQUMsT0FBUCxHQUNNO0lBQ1MsaUJBQUMsR0FBRDtBQUNYLFVBQUE7TUFEYSxJQUFDLENBQUEsV0FBQSxNQUFNLElBQUMsQ0FBQSxXQUFBLE1BQU0sMkNBQW1CLElBQUMsQ0FBQSxXQUFBLE1BQU07TUFDckQsSUFBRyxPQUFPLElBQUMsQ0FBQSxJQUFJLENBQUMsT0FBYixLQUF3QixVQUEzQjtRQUNFLElBQUMsQ0FBQSxJQUFELEdBQVEsSUFBQyxDQUFBLElBQUksQ0FBQyxPQUFOLENBQUEsRUFEVjs7TUFHQSxJQUFDLENBQUEsT0FBRCxHQUFXLFFBQVEsQ0FBQyxhQUFULENBQXVCLElBQXZCO01BQ1gsSUFBQyxDQUFBLE9BQU8sQ0FBQyxZQUFULENBQXNCLElBQXRCLEVBQTRCLFVBQTVCO01BQ0EsSUFBRyxDQUFDLFlBQUQsRUFBZSxVQUFmLENBQTBCLENBQUMsT0FBM0IsQ0FBbUMsSUFBQyxDQUFBLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBckQsQ0FBQSxHQUE2RCxDQUFDLENBQWpFO1FBQ0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsWUFBdkIsRUFERjs7TUFFQSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixLQUF2QixFQUE4QixVQUE5QjtNQUVBLElBQUMsQ0FBQSxTQUFELEdBQWEsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDYixJQUFDLENBQUEsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFyQixDQUF5QixPQUF6QjtNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixJQUFDLENBQUEsU0FBdEI7TUFFQSxJQUFHLFFBQUEsS0FBWSxRQUFaLElBQXdCLHFFQUFTLENBQUMsK0JBQXJDO1FBQ0UsU0FBQSxHQUFZLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO1FBQ1osU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFwQixDQUF3QixZQUF4QjtRQUNBLFNBQVMsQ0FBQyxPQUFWLEdBQW9CO1FBQ3BCLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixTQUFyQixFQUpGOztNQU1BLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUksbUJBQUosQ0FBQTtNQUVqQixJQUFDLENBQUEsWUFBRCxDQUFBO01BQ0EsSUFBQyxDQUFBLG9CQUFELENBQUE7TUFDQSxJQUFDLENBQUEsV0FBRCxDQUFBO01BQ0EsSUFBQyxDQUFBLFVBQUQsQ0FBQTtNQUNBLElBQUMsQ0FBQSxvQkFBRCxDQUFBO01BQ0EsSUFBQyxDQUFBLFlBQUQsQ0FBQTtNQUVBLElBQUcsSUFBQyxDQUFBLGFBQUQsQ0FBQSxDQUFIO1FBQ0UsSUFBQyxDQUFBLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBckIsQ0FBeUIsTUFBekI7UUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixhQUF2QixFQUZGOztNQUlBLElBQUMsQ0FBQSxPQUFPLENBQUMsTUFBVCxHQUFrQixTQUFDLENBQUQ7ZUFBTyxNQUFNLENBQUMsSUFBUCxDQUFZLENBQVo7TUFBUDtNQUNsQixJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVQsR0FBcUIsU0FBQyxDQUFEO2VBQU8sTUFBTSxDQUFDLEdBQVAsQ0FBVyxDQUFYO01BQVA7TUFFckIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULEdBQWdCLElBQUMsQ0FBQTtNQUNqQixJQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsR0FBZ0IsSUFBQyxDQUFBO01BQ2pCLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBVCxHQUFxQixJQUFDLENBQUE7TUFDdEIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULEdBQWdCLElBQUMsQ0FBQTtJQXZDTjs7c0JBeUNiLFlBQUEsR0FBYyxTQUFBO0FBQ1osVUFBQTtNQUFBLG1CQUFBLEdBQXNCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFDcEIsS0FBQyxDQUFBLFdBQUQsQ0FBQTtRQURvQjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFHdEIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUMsQ0FBQSxJQUFJLENBQUMsWUFBTixDQUFtQixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBQTtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFuQixDQUFuQjtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsSUFBSSxDQUFDLDhCQUFOLENBQXFDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxJQUFEO1VBQ3RELElBQW1CLElBQUEsS0FBUSxLQUFDLENBQUEsSUFBNUI7bUJBQUEsS0FBQyxDQUFBLFlBQUQsQ0FBQSxFQUFBOztRQURzRDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBckMsQ0FBbkI7TUFHQSxJQUFHLE9BQU8sSUFBQyxDQUFBLElBQUksQ0FBQyxnQkFBYixLQUFpQyxVQUFwQztRQUNFLDBCQUFBLEdBQTZCLElBQUMsQ0FBQSxJQUFJLENBQUMsZ0JBQU4sQ0FBdUIsbUJBQXZCO1FBQzdCLElBQUcsVUFBVSxDQUFDLFlBQVgsQ0FBd0IsMEJBQXhCLENBQUg7VUFDRSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsMEJBQW5CLEVBREY7U0FBQSxNQUFBO1VBR0UsT0FBTyxDQUFDLElBQVIsQ0FBYSx3REFBYixFQUF1RSxJQUFDLENBQUEsSUFBeEUsRUFIRjtTQUZGO09BQUEsTUFNSyxJQUFHLE9BQU8sSUFBQyxDQUFBLElBQUksQ0FBQyxFQUFiLEtBQW1CLFVBQXRCO1FBRUgsSUFBQyxDQUFBLElBQUksQ0FBQyxFQUFOLENBQVMsZUFBVCxFQUEwQixtQkFBMUI7UUFDQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUI7VUFBQSxPQUFBLEVBQVMsQ0FBQSxTQUFBLEtBQUE7bUJBQUEsU0FBQTtBQUMxQixrQkFBQTt5RUFBSyxDQUFDLElBQUssaUJBQWlCO1lBREY7VUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVQ7U0FBbkIsRUFIRzs7TUFNTCxrQkFBQSxHQUFxQixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsS0FBRDtVQUFDLEtBQUMsQ0FBQSxPQUFEO1VBQ3BCLEtBQUMsQ0FBQSxvQkFBRCxDQUFBO1VBQ0EsS0FBQyxDQUFBLFdBQUQsQ0FBQTtVQUNBLEtBQUMsQ0FBQSxhQUFELENBQUE7aUJBQ0EsS0FBQyxDQUFBLFVBQUQsQ0FBQTtRQUptQjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFNckIsSUFBRyxPQUFPLElBQUMsQ0FBQSxJQUFJLENBQUMsZUFBYixLQUFnQyxVQUFuQztRQUNFLHlCQUFBLEdBQTRCLElBQUMsQ0FBQSxJQUFJLENBQUMsZUFBTixDQUFzQixrQkFBdEI7UUFDNUIsSUFBRyxVQUFVLENBQUMsWUFBWCxDQUF3Qix5QkFBeEIsQ0FBSDtVQUNFLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQix5QkFBbkIsRUFERjtTQUFBLE1BQUE7VUFHRSxPQUFPLENBQUMsSUFBUixDQUFhLHVEQUFiLEVBQXNFLElBQUMsQ0FBQSxJQUF2RSxFQUhGO1NBRkY7T0FBQSxNQU1LLElBQUcsT0FBTyxJQUFDLENBQUEsSUFBSSxDQUFDLEVBQWIsS0FBbUIsVUFBdEI7UUFFSCxJQUFDLENBQUEsSUFBSSxDQUFDLEVBQU4sQ0FBUyxjQUFULEVBQXlCLGtCQUF6QjtRQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQjtVQUFBLE9BQUEsRUFBUyxDQUFBLFNBQUEsS0FBQTttQkFBQSxTQUFBO0FBQzFCLGtCQUFBO3lFQUFLLENBQUMsSUFBSyxnQkFBZ0I7WUFERDtVQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBVDtTQUFuQixFQUhHOztNQU1MLGtCQUFBLEdBQXFCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFDbkIsS0FBQyxDQUFBLFVBQUQsQ0FBQTtRQURtQjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFHckIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLGVBQUEsQ0FBQSxDQUFpQixDQUFDLFdBQWxCLENBQThCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsVUFBRCxDQUFBO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTlCLENBQW5CO01BRUEsSUFBRyxPQUFPLElBQUMsQ0FBQSxJQUFJLENBQUMsZUFBYixLQUFnQyxVQUFuQztRQUNFLHlCQUFBLGtFQUFpQyxDQUFDLGdCQUFpQixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUNqRCxLQUFDLENBQUEsVUFBRCxDQUFBO1VBRGlEO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQTtRQUVuRCxJQUFHLFVBQVUsQ0FBQyxZQUFYLENBQXdCLHlCQUF4QixDQUFIO1VBQ0UsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLHlCQUFuQixFQURGO1NBQUEsTUFBQTtVQUdFLE9BQU8sQ0FBQyxJQUFSLENBQWEsdURBQWIsRUFBc0UsSUFBQyxDQUFBLElBQXZFLEVBSEY7U0FIRjtPQUFBLE1BT0ssSUFBRyxPQUFPLElBQUMsQ0FBQSxJQUFJLENBQUMsRUFBYixLQUFtQixVQUF0QjtRQUVILElBQUMsQ0FBQSxJQUFJLENBQUMsRUFBTixDQUFTLGNBQVQsRUFBeUIsa0JBQXpCO1FBQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CO1VBQUEsT0FBQSxFQUFTLENBQUEsU0FBQSxLQUFBO21CQUFBLFNBQUE7QUFDMUIsa0JBQUE7MkVBQUssQ0FBQyxJQUFLLGdCQUFnQjtZQUREO1VBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFUO1NBQW5CLEVBSEc7O01BTUwsZUFBQSxHQUFrQixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQ2hCLEtBQUMsQ0FBQSxvQkFBRCxDQUFBO1FBRGdCO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQTtNQUdsQixJQUFHLE9BQU8sSUFBQyxDQUFBLElBQUksQ0FBQyxtQkFBYixLQUFvQyxVQUF2QztRQUNFLDZCQUFBLEdBQWdDLElBQUMsQ0FBQSxJQUFJLENBQUMsbUJBQU4sQ0FBMEIsZUFBMUI7UUFDaEMsSUFBRyxVQUFVLENBQUMsWUFBWCxDQUF3Qiw2QkFBeEIsQ0FBSDtVQUNFLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQiw2QkFBbkIsRUFERjtTQUFBLE1BQUE7VUFHRSxPQUFPLENBQUMsSUFBUixDQUFhLDJEQUFiLEVBQTBFLElBQUMsQ0FBQSxJQUEzRSxFQUhGO1NBRkY7T0FBQSxNQU1LLElBQUcsT0FBTyxJQUFDLENBQUEsSUFBSSxDQUFDLEVBQWIsS0FBbUIsVUFBdEI7UUFFSCxJQUFDLENBQUEsSUFBSSxDQUFDLEVBQU4sQ0FBUyx5QkFBVCxFQUFvQyxlQUFwQztRQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQjtVQUFBLE9BQUEsRUFBUyxDQUFBLFNBQUEsS0FBQTttQkFBQSxTQUFBO0FBQzFCLGtCQUFBOzJFQUFLLENBQUMsSUFBSywyQkFBMkI7WUFEWjtVQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBVDtTQUFuQixFQUhHOztNQU1MLElBQUcsT0FBTyxJQUFDLENBQUEsSUFBSSxDQUFDLFNBQWIsS0FBMEIsVUFBN0I7UUFDRSxtQkFBQSxHQUFzQixJQUFDLENBQUEsSUFBSSxDQUFDLFNBQU4sQ0FBZ0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQyxLQUFEO1lBQ3BDLEtBQUMsQ0FBQSxxQkFBRCxDQUFBO1lBQ0EsSUFBRyxLQUFLLENBQUMsSUFBTixLQUFnQixLQUFDLENBQUEsSUFBcEI7Y0FDRSxLQUFDLENBQUEsSUFBRCxHQUFRLEtBQUssQ0FBQztjQUNkLElBQXFCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQix3QkFBaEIsQ0FBckI7dUJBQUEsS0FBQyxDQUFBLGNBQUQsQ0FBQSxFQUFBO2VBRkY7O1VBRm9DO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFoQjtRQU10QixJQUFHLFVBQVUsQ0FBQyxZQUFYLENBQXdCLG1CQUF4QixDQUFIO1VBQ0UsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLG1CQUFuQixFQURGO1NBQUEsTUFBQTtVQUdFLE9BQU8sQ0FBQyxJQUFSLENBQWEsaURBQWIsRUFBZ0UsSUFBQyxDQUFBLElBQWpFLEVBSEY7U0FQRjs7TUFXQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFaLENBQW9CLGdCQUFwQixFQUFzQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQ3ZELEtBQUMsQ0FBQSxvQkFBRCxDQUFBO1FBRHVEO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF0QyxDQUFuQjthQUdBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQVosQ0FBb0Isd0JBQXBCLEVBQThDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxTQUFEO1VBQy9ELElBQUcsU0FBQSxJQUFjLG9CQUFqQjttQkFBNkIsS0FBQyxDQUFBLGNBQUQsQ0FBQSxFQUE3QjtXQUFBLE1BQUE7bUJBQW9ELEtBQUMsQ0FBQSxjQUFELENBQUEsRUFBcEQ7O1FBRCtEO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE5QyxDQUFuQjtJQXJGWTs7c0JBd0ZkLFlBQUEsR0FBYyxTQUFBO0FBRVosVUFBQTtNQUFBLFlBQUEsR0FBZSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDYixLQUFDLENBQUEsc0JBQXNCLENBQUMsT0FBeEIsQ0FBQTtVQUNBLEtBQUMsQ0FBQSxpQkFBRCxHQUFxQjtVQUNyQixLQUFDLENBQUEsYUFBRCxDQUFBO2lCQUdBLEtBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxDQUF1QixJQUFJLFdBQUosQ0FBZ0IsWUFBaEIsRUFBOEI7WUFBQSxPQUFBLEVBQVMsSUFBVDtXQUE5QixDQUF2QjtRQU5hO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQTtNQVFmLElBQUMsQ0FBQSxzQkFBRCxHQUEwQjtRQUFBLE9BQUEsRUFBUyxDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO1lBQ2pDLEtBQUMsQ0FBQSxPQUFPLENBQUMsbUJBQVQsQ0FBNkIsWUFBN0IsRUFBMkMsWUFBM0M7bUJBQ0EsS0FBQyxDQUFBLHNCQUFELEdBQTBCO1VBRk87UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVQ7O2FBSTFCLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsWUFBMUIsRUFBd0MsWUFBeEM7SUFkWTs7c0JBZ0JkLGFBQUEsR0FBZSxTQUFBO01BQ2IsSUFBQSxDQUFjLElBQUMsQ0FBQSxpQkFBZjtBQUFBLGVBQUE7O01BRUEsSUFBQyxDQUFBLGNBQUQsQ0FBQTtNQUVBLElBQUcsSUFBQyxDQUFBLElBQUo7ZUFDRSxJQUFDLENBQUEsT0FBRCxHQUFXLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixJQUFDLENBQUEsT0FBbkIsRUFDVDtVQUFBLEtBQUEsRUFBTyxJQUFDLENBQUEsSUFBUjtVQUNBLElBQUEsRUFBTSxLQUROO1VBRUEsS0FBQSxFQUNFO1lBQUEsSUFBQSxFQUFNLElBQU47WUFDQSxJQUFBLEVBQU0sR0FETjtXQUhGO1VBS0EsU0FBQSxFQUFXLFFBTFg7U0FEUyxFQURiOztJQUxhOztzQkFjZixjQUFBLEdBQWdCLFNBQUE7QUFDZCxVQUFBO01BQUEsSUFBQSxDQUFjLElBQUMsQ0FBQSxpQkFBZjtBQUFBLGVBQUE7O2lEQUNRLENBQUUsT0FBVixDQUFBO0lBRmM7O3NCQUloQixPQUFBLEdBQVMsU0FBQTtBQUNQLFVBQUE7O1lBQWMsQ0FBRSxPQUFoQixDQUFBOzs7WUFDdUIsQ0FBRSxPQUF6QixDQUFBOzs7WUFDa0IsQ0FBRSxPQUFwQixDQUFBOztNQUNBLElBQUMsQ0FBQSxjQUFELENBQUE7YUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLE1BQVQsQ0FBQTtJQUxPOztzQkFPVCxvQkFBQSxHQUFzQixTQUFBO0FBQ3BCLFVBQUE7TUFBQSxJQUFHLElBQUMsQ0FBQSxJQUFKO1FBQ0UsSUFBQyxDQUFBLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBbkIsR0FBMEIsSUFBSSxDQUFDLFFBQUwsQ0FBYyxJQUFDLENBQUEsSUFBZjtRQUMxQixJQUFDLENBQUEsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFuQixHQUEwQixJQUFDLENBQUEsS0FGN0I7T0FBQSxNQUFBO1FBSUUsT0FBTyxJQUFDLENBQUEsU0FBUyxDQUFDLE9BQU8sQ0FBQztRQUMxQixPQUFPLElBQUMsQ0FBQSxTQUFTLENBQUMsT0FBTyxDQUFDLEtBTDVCOztNQU9BLElBQUcsU0FBQSxnREFBNkIsQ0FBRSxhQUFsQztlQUNFLElBQUMsQ0FBQSxPQUFPLENBQUMsT0FBTyxDQUFDLElBQWpCLEdBQXdCLFVBRDFCO09BQUEsTUFBQTtlQUdFLE9BQU8sSUFBQyxDQUFBLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FIMUI7O0lBUm9COztzQkFhdEIsV0FBQSxHQUFhLFNBQUMsR0FBRDtBQUNYLFVBQUE7MkJBRFksTUFBK0IsSUFBOUIsc0NBQWdCO01BQzdCLElBQVUsSUFBQyxDQUFBLGFBQVg7QUFBQSxlQUFBOztNQUNBLElBQUMsQ0FBQSxhQUFELEdBQWlCO01BRWpCLElBQUcsY0FBQSxLQUFrQixLQUFyQjtRQUNFLEtBQUEsR0FBUSxJQUFDLENBQUEsSUFBSSxDQUFDLFFBQU4sQ0FBQTtRQUNSLElBQXlDLFlBQXpDO1VBQUEsS0FBQSxrSEFBZ0MsTUFBaEM7O1FBQ0EsSUFBQyxDQUFBLFNBQVMsQ0FBQyxXQUFYLEdBQXlCLE1BSDNCO09BQUEsTUFBQTtRQUtFLEtBQUEsR0FBUSxJQUFDLENBQUEsSUFBSSxDQUFDLFFBQU4sQ0FBQTtRQUNSLFlBQUEsR0FBZTtBQUNmO0FBQUEsYUFBQSxzQ0FBQTs7Y0FBc0IsR0FBQSxLQUFTO1lBQzdCLElBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFULENBQUEsQ0FBQSxLQUF1QixLQUExQjtjQUNFLEdBQUcsQ0FBQyxXQUFKLENBQWdCO2dCQUFBLGNBQUEsRUFBZ0IsS0FBaEI7Z0JBQXVCLFlBQUEsRUFBYyxJQUFyQztlQUFoQjtjQUNBLFlBQUEsR0FBZSxLQUZqQjs7O0FBREY7UUFJQSxJQUF5QyxZQUF6QztVQUFBLEtBQUEsb0hBQWdDLE1BQWhDOztRQUVBLElBQUMsQ0FBQSxTQUFTLENBQUMsV0FBWCxHQUF5QixNQWIzQjs7YUFlQSxJQUFDLENBQUEsYUFBRCxHQUFpQjtJQW5CTjs7c0JBcUJiLFVBQUEsR0FBWSxTQUFBO2FBQ1YsZUFBQSxDQUFBLENBQWlCLENBQUMsYUFBbEIsQ0FBZ0MsSUFBaEM7SUFEVTs7c0JBR1osYUFBQSxHQUFlLFNBQUE7TUFDYixJQUFHLGdDQUFIO2VBQ0UsSUFBQyxDQUFBLElBQUksQ0FBQyxjQUFOLENBQUEsQ0FBQSxLQUEwQixJQUFDLENBQUEsS0FEN0I7T0FBQSxNQUVLLElBQUcsMkJBQUg7ZUFDSCxJQUFDLENBQUEsSUFBSSxDQUFDLFNBQU4sQ0FBQSxFQURHOztJQUhROztzQkFNZixxQkFBQSxHQUF1QixTQUFBO01BQ3JCLElBQUcsa0NBQUg7UUFDRSxJQUE0QixJQUFDLENBQUEsSUFBSSxDQUFDLGNBQU4sQ0FBQSxDQUFBLEtBQTBCLElBQUMsQ0FBQSxJQUF2RDtpQkFBQSxJQUFDLENBQUEsSUFBSSxDQUFDLGdCQUFOLENBQUEsRUFBQTtTQURGO09BQUEsTUFFSyxJQUFHLHVDQUFIO2VBQ0gsSUFBQyxDQUFBLElBQUksQ0FBQyxxQkFBTixDQUFBLEVBREc7O0lBSGdCOztzQkFNdkIsWUFBQSxHQUFjLFNBQUE7TUFDWixJQUFDLENBQUEsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFyQixDQUE0QixNQUE1QjthQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQW5CLENBQTBCLGFBQTFCO0lBRlk7O3NCQUlkLG9CQUFBLEdBQXNCLFNBQUE7TUFDcEIsSUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsZ0JBQWhCLENBQUg7ZUFDRSxJQUFDLENBQUEsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFyQixDQUE0QixXQUE1QixFQURGO09BQUEsTUFBQTtlQUdFLElBQUMsQ0FBQSxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQXJCLENBQXlCLFdBQXpCLEVBSEY7O0lBRG9COztzQkFNdEIsb0JBQUEsR0FBc0IsU0FBQTtBQUNwQixVQUFBO01BQUEsOERBQVEsQ0FBQyxxQkFBVDtRQUNFLElBQUEsQ0FBMEMsSUFBQyxDQUFBLFVBQTNDO1VBQUEsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsVUFBdkIsRUFBQTs7ZUFDQSxJQUFDLENBQUEsVUFBRCxHQUFjLEtBRmhCO09BQUEsTUFBQTtRQUlFLElBQXlDLElBQUMsQ0FBQSxVQUExQztVQUFBLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQW5CLENBQTBCLFVBQTFCLEVBQUE7O2VBQ0EsSUFBQyxDQUFBLFVBQUQsR0FBYyxNQUxoQjs7SUFEb0I7O3NCQVF0QixjQUFBLEdBQWdCLFNBQUE7TUFDZCxJQUFjLGlCQUFkO0FBQUEsZUFBQTs7YUFDQSxJQUFDLENBQUEsV0FBRCxDQUFhLElBQUMsQ0FBQSxJQUFkLENBQW1CLENBQUMsSUFBcEIsQ0FBeUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLElBQUQ7VUFDdkIsS0FBQyxDQUFBLGVBQUQsQ0FBaUIsSUFBakI7aUJBQ0EsS0FBQyxDQUFBLGVBQUQsQ0FBaUIsSUFBakI7UUFGdUI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpCO0lBRmM7O3NCQU9oQixlQUFBLEdBQWlCLFNBQUMsSUFBRDtBQUNmLFVBQUE7TUFBQSxJQUFjLFlBQWQ7QUFBQSxlQUFBOzs7WUFHa0IsQ0FBRSxPQUFwQixDQUFBOztNQUNBLElBQUMsQ0FBQSxpQkFBRCxHQUFxQixJQUFJLG1CQUFKLENBQUE7TUFFckIsSUFBQyxDQUFBLGlCQUFpQixDQUFDLEdBQW5CLENBQXVCLElBQUksQ0FBQyxpQkFBTCxDQUF1QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsS0FBRDtVQUM1QyxJQUE0QyxLQUFLLENBQUMsSUFBTixLQUFjLEtBQUMsQ0FBQSxJQUEzRDttQkFBQSxLQUFDLENBQUEsZUFBRCxDQUFpQixJQUFqQixFQUF1QixLQUFLLENBQUMsVUFBN0IsRUFBQTs7UUFENEM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXZCLENBQXZCO2FBRUEsSUFBQyxDQUFBLGlCQUFpQixDQUFDLEdBQW5CLENBQXVCLElBQUksQ0FBQyxtQkFBTCxDQUF5QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQzlDLEtBQUMsQ0FBQSxlQUFELENBQWlCLElBQWpCO1FBRDhDO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6QixDQUF2QjtJQVRlOztzQkFZakIsV0FBQSxHQUFhLFNBQUE7QUFDWCxVQUFBO0FBQUE7QUFBQSxXQUFBLHNDQUFBOztRQUNFLElBQW1ELEdBQUcsQ0FBQyxRQUFKLENBQWEsSUFBQyxDQUFBLElBQWQsQ0FBbkQ7QUFBQSxpQkFBTyxJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFiLENBQW9DLEdBQXBDLEVBQVA7O0FBREY7YUFFQSxPQUFPLENBQUMsT0FBUixDQUFnQixJQUFoQjtJQUhXOztzQkFNYixlQUFBLEdBQWlCLFNBQUMsSUFBRCxFQUFPLE1BQVA7QUFDZixVQUFBO01BQUEsSUFBYyxZQUFkO0FBQUEsZUFBQTs7TUFFQSxTQUFBLEdBQVk7TUFDWixJQUFHLElBQUksQ0FBQyxhQUFMLENBQW1CLElBQUMsQ0FBQSxJQUFwQixDQUFIO1FBQ0UsU0FBQSxHQUFZLFVBRGQ7T0FBQSxNQUFBO1FBR0UsSUFBZ0QsY0FBaEQ7VUFBQSxNQUFBLEdBQVMsSUFBSSxDQUFDLG1CQUFMLENBQXlCLElBQUMsQ0FBQSxJQUExQixFQUFUOztRQUNBLElBQUcsSUFBSSxDQUFDLGdCQUFMLENBQXNCLE1BQXRCLENBQUg7VUFDRSxTQUFBLEdBQVksV0FEZDtTQUFBLE1BRUssSUFBRyxJQUFJLENBQUMsV0FBTCxDQUFpQixNQUFqQixDQUFIO1VBQ0gsU0FBQSxHQUFZLFFBRFQ7U0FOUDs7TUFTQSxJQUFHLFNBQUEsS0FBZSxJQUFDLENBQUEsTUFBbkI7UUFDRSxJQUFDLENBQUEsTUFBRCxHQUFVO2VBQ1YsSUFBQyxDQUFBLGlCQUFELENBQUEsRUFGRjs7SUFiZTs7c0JBaUJqQixpQkFBQSxHQUFtQixTQUFBO01BQ2pCLElBQUMsQ0FBQSxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQXJCLENBQTRCLGdCQUE1QixFQUE4QyxpQkFBOUMsRUFBa0UsY0FBbEU7TUFDQSxJQUFHLElBQUMsQ0FBQSxNQUFELElBQVksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLHdCQUFoQixDQUFmO2VBQ0UsSUFBQyxDQUFBLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBckIsQ0FBeUIsU0FBQSxHQUFVLElBQUMsQ0FBQSxNQUFwQyxFQURGOztJQUZpQjs7c0JBS25CLGNBQUEsR0FBZ0IsU0FBQTtBQUNkLFVBQUE7O1lBQWtCLENBQUUsT0FBcEIsQ0FBQTs7TUFDQSxPQUFPLElBQUMsQ0FBQTthQUNSLElBQUMsQ0FBQSxpQkFBRCxDQUFBO0lBSGM7Ozs7O0FBcFNsQiIsInNvdXJjZXNDb250ZW50IjpbInBhdGggPSByZXF1aXJlICdwYXRoJ1xue0Rpc3Bvc2FibGUsIENvbXBvc2l0ZURpc3Bvc2FibGV9ID0gcmVxdWlyZSAnYXRvbSdcbmdldEljb25TZXJ2aWNlcyA9IHJlcXVpcmUgJy4vZ2V0LWljb24tc2VydmljZXMnXG5cbmxheW91dCA9IHJlcXVpcmUgJy4vbGF5b3V0J1xuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBUYWJWaWV3XG4gIGNvbnN0cnVjdG9yOiAoe0BpdGVtLCBAcGFuZSwgZGlkQ2xpY2tDbG9zZUljb24sIEB0YWJzLCBsb2NhdGlvbn0pIC0+XG4gICAgaWYgdHlwZW9mIEBpdGVtLmdldFBhdGggaXMgJ2Z1bmN0aW9uJ1xuICAgICAgQHBhdGggPSBAaXRlbS5nZXRQYXRoKClcblxuICAgIEBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGknKVxuICAgIEBlbGVtZW50LnNldEF0dHJpYnV0ZSgnaXMnLCAndGFicy10YWInKVxuICAgIGlmIFsnVGV4dEVkaXRvcicsICdUZXN0VmlldyddLmluZGV4T2YoQGl0ZW0uY29uc3RydWN0b3IubmFtZSkgPiAtMVxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgndGV4dGVkaXRvcicpXG4gICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgndGFiJywgJ3NvcnRhYmxlJylcblxuICAgIEBpdGVtVGl0bGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIEBpdGVtVGl0bGUuY2xhc3NMaXN0LmFkZCgndGl0bGUnKVxuICAgIEBlbGVtZW50LmFwcGVuZENoaWxkKEBpdGVtVGl0bGUpXG5cbiAgICBpZiBsb2NhdGlvbiBpcyAnY2VudGVyJyBvciBub3QgQGl0ZW0uaXNQZXJtYW5lbnREb2NrSXRlbT8oKVxuICAgICAgY2xvc2VJY29uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICAgIGNsb3NlSWNvbi5jbGFzc0xpc3QuYWRkKCdjbG9zZS1pY29uJylcbiAgICAgIGNsb3NlSWNvbi5vbmNsaWNrID0gZGlkQ2xpY2tDbG9zZUljb25cbiAgICAgIEBlbGVtZW50LmFwcGVuZENoaWxkKGNsb3NlSWNvbilcblxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGUoKVxuXG4gICAgQGhhbmRsZUV2ZW50cygpXG4gICAgQHVwZGF0ZURhdGFBdHRyaWJ1dGVzKClcbiAgICBAdXBkYXRlVGl0bGUoKVxuICAgIEB1cGRhdGVJY29uKClcbiAgICBAdXBkYXRlTW9kaWZpZWRTdGF0dXMoKVxuICAgIEBzZXR1cFRvb2x0aXAoKVxuXG4gICAgaWYgQGlzSXRlbVBlbmRpbmcoKVxuICAgICAgQGl0ZW1UaXRsZS5jbGFzc0xpc3QuYWRkKCd0ZW1wJylcbiAgICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3BlbmRpbmctdGFiJylcblxuICAgIEBlbGVtZW50Lm9uZHJhZyA9IChlKSAtPiBsYXlvdXQuZHJhZyBlXG4gICAgQGVsZW1lbnQub25kcmFnZW5kID0gKGUpIC0+IGxheW91dC5lbmQgZVxuXG4gICAgQGVsZW1lbnQucGFuZSA9IEBwYW5lXG4gICAgQGVsZW1lbnQuaXRlbSA9IEBpdGVtXG4gICAgQGVsZW1lbnQuaXRlbVRpdGxlID0gQGl0ZW1UaXRsZVxuICAgIEBlbGVtZW50LnBhdGggPSBAcGF0aFxuXG4gIGhhbmRsZUV2ZW50czogLT5cbiAgICB0aXRsZUNoYW5nZWRIYW5kbGVyID0gPT5cbiAgICAgIEB1cGRhdGVUaXRsZSgpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgQHBhbmUub25EaWREZXN0cm95ID0+IEBkZXN0cm95KClcbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgQHBhbmUub25JdGVtRGlkVGVybWluYXRlUGVuZGluZ1N0YXRlIChpdGVtKSA9PlxuICAgICAgQGNsZWFyUGVuZGluZygpIGlmIGl0ZW0gaXMgQGl0ZW1cblxuICAgIGlmIHR5cGVvZiBAaXRlbS5vbkRpZENoYW5nZVRpdGxlIGlzICdmdW5jdGlvbidcbiAgICAgIG9uRGlkQ2hhbmdlVGl0bGVEaXNwb3NhYmxlID0gQGl0ZW0ub25EaWRDaGFuZ2VUaXRsZSh0aXRsZUNoYW5nZWRIYW5kbGVyKVxuICAgICAgaWYgRGlzcG9zYWJsZS5pc0Rpc3Bvc2FibGUob25EaWRDaGFuZ2VUaXRsZURpc3Bvc2FibGUpXG4gICAgICAgIEBzdWJzY3JpcHRpb25zLmFkZChvbkRpZENoYW5nZVRpdGxlRGlzcG9zYWJsZSlcbiAgICAgIGVsc2VcbiAgICAgICAgY29uc29sZS53YXJuIFwiOjpvbkRpZENoYW5nZVRpdGxlIGRvZXMgbm90IHJldHVybiBhIHZhbGlkIERpc3Bvc2FibGUhXCIsIEBpdGVtXG4gICAgZWxzZSBpZiB0eXBlb2YgQGl0ZW0ub24gaXMgJ2Z1bmN0aW9uJ1xuICAgICAgI1RPRE8gUmVtb3ZlIG9uY2Ugb2xkIGV2ZW50cyBhcmUgbm8gbG9uZ2VyIHN1cHBvcnRlZFxuICAgICAgQGl0ZW0ub24oJ3RpdGxlLWNoYW5nZWQnLCB0aXRsZUNoYW5nZWRIYW5kbGVyKVxuICAgICAgQHN1YnNjcmlwdGlvbnMuYWRkIGRpc3Bvc2U6ID0+XG4gICAgICAgIEBpdGVtLm9mZj8oJ3RpdGxlLWNoYW5nZWQnLCB0aXRsZUNoYW5nZWRIYW5kbGVyKVxuXG4gICAgcGF0aENoYW5nZWRIYW5kbGVyID0gKEBwYXRoKSA9PlxuICAgICAgQHVwZGF0ZURhdGFBdHRyaWJ1dGVzKClcbiAgICAgIEB1cGRhdGVUaXRsZSgpXG4gICAgICBAdXBkYXRlVG9vbHRpcCgpXG4gICAgICBAdXBkYXRlSWNvbigpXG5cbiAgICBpZiB0eXBlb2YgQGl0ZW0ub25EaWRDaGFuZ2VQYXRoIGlzICdmdW5jdGlvbidcbiAgICAgIG9uRGlkQ2hhbmdlUGF0aERpc3Bvc2FibGUgPSBAaXRlbS5vbkRpZENoYW5nZVBhdGgocGF0aENoYW5nZWRIYW5kbGVyKVxuICAgICAgaWYgRGlzcG9zYWJsZS5pc0Rpc3Bvc2FibGUob25EaWRDaGFuZ2VQYXRoRGlzcG9zYWJsZSlcbiAgICAgICAgQHN1YnNjcmlwdGlvbnMuYWRkKG9uRGlkQ2hhbmdlUGF0aERpc3Bvc2FibGUpXG4gICAgICBlbHNlXG4gICAgICAgIGNvbnNvbGUud2FybiBcIjo6b25EaWRDaGFuZ2VQYXRoIGRvZXMgbm90IHJldHVybiBhIHZhbGlkIERpc3Bvc2FibGUhXCIsIEBpdGVtXG4gICAgZWxzZSBpZiB0eXBlb2YgQGl0ZW0ub24gaXMgJ2Z1bmN0aW9uJ1xuICAgICAgI1RPRE8gUmVtb3ZlIG9uY2Ugb2xkIGV2ZW50cyBhcmUgbm8gbG9uZ2VyIHN1cHBvcnRlZFxuICAgICAgQGl0ZW0ub24oJ3BhdGgtY2hhbmdlZCcsIHBhdGhDaGFuZ2VkSGFuZGxlcilcbiAgICAgIEBzdWJzY3JpcHRpb25zLmFkZCBkaXNwb3NlOiA9PlxuICAgICAgICBAaXRlbS5vZmY/KCdwYXRoLWNoYW5nZWQnLCBwYXRoQ2hhbmdlZEhhbmRsZXIpXG5cbiAgICBpY29uQ2hhbmdlZEhhbmRsZXIgPSA9PlxuICAgICAgQHVwZGF0ZUljb24oKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGdldEljb25TZXJ2aWNlcygpLm9uRGlkQ2hhbmdlID0+IEB1cGRhdGVJY29uKClcblxuICAgIGlmIHR5cGVvZiBAaXRlbS5vbkRpZENoYW5nZUljb24gaXMgJ2Z1bmN0aW9uJ1xuICAgICAgb25EaWRDaGFuZ2VJY29uRGlzcG9zYWJsZSA9IEBpdGVtLm9uRGlkQ2hhbmdlSWNvbj8gPT5cbiAgICAgICAgQHVwZGF0ZUljb24oKVxuICAgICAgaWYgRGlzcG9zYWJsZS5pc0Rpc3Bvc2FibGUob25EaWRDaGFuZ2VJY29uRGlzcG9zYWJsZSlcbiAgICAgICAgQHN1YnNjcmlwdGlvbnMuYWRkKG9uRGlkQ2hhbmdlSWNvbkRpc3Bvc2FibGUpXG4gICAgICBlbHNlXG4gICAgICAgIGNvbnNvbGUud2FybiBcIjo6b25EaWRDaGFuZ2VJY29uIGRvZXMgbm90IHJldHVybiBhIHZhbGlkIERpc3Bvc2FibGUhXCIsIEBpdGVtXG4gICAgZWxzZSBpZiB0eXBlb2YgQGl0ZW0ub24gaXMgJ2Z1bmN0aW9uJ1xuICAgICAgI1RPRE8gUmVtb3ZlIG9uY2Ugb2xkIGV2ZW50cyBhcmUgbm8gbG9uZ2VyIHN1cHBvcnRlZFxuICAgICAgQGl0ZW0ub24oJ2ljb24tY2hhbmdlZCcsIGljb25DaGFuZ2VkSGFuZGxlcilcbiAgICAgIEBzdWJzY3JpcHRpb25zLmFkZCBkaXNwb3NlOiA9PlxuICAgICAgICBAaXRlbS5vZmY/KCdpY29uLWNoYW5nZWQnLCBpY29uQ2hhbmdlZEhhbmRsZXIpXG5cbiAgICBtb2RpZmllZEhhbmRsZXIgPSA9PlxuICAgICAgQHVwZGF0ZU1vZGlmaWVkU3RhdHVzKClcblxuICAgIGlmIHR5cGVvZiBAaXRlbS5vbkRpZENoYW5nZU1vZGlmaWVkIGlzICdmdW5jdGlvbidcbiAgICAgIG9uRGlkQ2hhbmdlTW9kaWZpZWREaXNwb3NhYmxlID0gQGl0ZW0ub25EaWRDaGFuZ2VNb2RpZmllZChtb2RpZmllZEhhbmRsZXIpXG4gICAgICBpZiBEaXNwb3NhYmxlLmlzRGlzcG9zYWJsZShvbkRpZENoYW5nZU1vZGlmaWVkRGlzcG9zYWJsZSlcbiAgICAgICAgQHN1YnNjcmlwdGlvbnMuYWRkKG9uRGlkQ2hhbmdlTW9kaWZpZWREaXNwb3NhYmxlKVxuICAgICAgZWxzZVxuICAgICAgICBjb25zb2xlLndhcm4gXCI6Om9uRGlkQ2hhbmdlTW9kaWZpZWQgZG9lcyBub3QgcmV0dXJuIGEgdmFsaWQgRGlzcG9zYWJsZSFcIiwgQGl0ZW1cbiAgICBlbHNlIGlmIHR5cGVvZiBAaXRlbS5vbiBpcyAnZnVuY3Rpb24nXG4gICAgICAjVE9ETyBSZW1vdmUgb25jZSBvbGQgZXZlbnRzIGFyZSBubyBsb25nZXIgc3VwcG9ydGVkXG4gICAgICBAaXRlbS5vbignbW9kaWZpZWQtc3RhdHVzLWNoYW5nZWQnLCBtb2RpZmllZEhhbmRsZXIpXG4gICAgICBAc3Vic2NyaXB0aW9ucy5hZGQgZGlzcG9zZTogPT5cbiAgICAgICAgQGl0ZW0ub2ZmPygnbW9kaWZpZWQtc3RhdHVzLWNoYW5nZWQnLCBtb2RpZmllZEhhbmRsZXIpXG5cbiAgICBpZiB0eXBlb2YgQGl0ZW0ub25EaWRTYXZlIGlzICdmdW5jdGlvbidcbiAgICAgIG9uRGlkU2F2ZURpc3Bvc2FibGUgPSBAaXRlbS5vbkRpZFNhdmUgKGV2ZW50KSA9PlxuICAgICAgICBAdGVybWluYXRlUGVuZGluZ1N0YXRlKClcbiAgICAgICAgaWYgZXZlbnQucGF0aCBpc250IEBwYXRoXG4gICAgICAgICAgQHBhdGggPSBldmVudC5wYXRoXG4gICAgICAgICAgQHNldHVwVmNzU3RhdHVzKCkgaWYgYXRvbS5jb25maWcuZ2V0ICd0YWJzLmVuYWJsZVZjc0NvbG9yaW5nJ1xuXG4gICAgICBpZiBEaXNwb3NhYmxlLmlzRGlzcG9zYWJsZShvbkRpZFNhdmVEaXNwb3NhYmxlKVxuICAgICAgICBAc3Vic2NyaXB0aW9ucy5hZGQob25EaWRTYXZlRGlzcG9zYWJsZSlcbiAgICAgIGVsc2VcbiAgICAgICAgY29uc29sZS53YXJuIFwiOjpvbkRpZFNhdmUgZG9lcyBub3QgcmV0dXJuIGEgdmFsaWQgRGlzcG9zYWJsZSFcIiwgQGl0ZW1cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb25maWcub2JzZXJ2ZSAndGFicy5zaG93SWNvbnMnLCA9PlxuICAgICAgQHVwZGF0ZUljb25WaXNpYmlsaXR5KClcblxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbmZpZy5vYnNlcnZlICd0YWJzLmVuYWJsZVZjc0NvbG9yaW5nJywgKGlzRW5hYmxlZCkgPT5cbiAgICAgIGlmIGlzRW5hYmxlZCBhbmQgQHBhdGg/IHRoZW4gQHNldHVwVmNzU3RhdHVzKCkgZWxzZSBAdW5zZXRWY3NTdGF0dXMoKVxuXG4gIHNldHVwVG9vbHRpcDogLT5cbiAgICAjIERlZmVyIGNyZWF0aW5nIHRoZSB0b29sdGlwIHVudGlsIHRoZSB0YWIgaXMgbW91c2VkIG92ZXJcbiAgICBvbk1vdXNlRW50ZXIgPSA9PlxuICAgICAgQG1vdXNlRW50ZXJTdWJzY3JpcHRpb24uZGlzcG9zZSgpXG4gICAgICBAaGFzQmVlbk1vdXNlZE92ZXIgPSB0cnVlXG4gICAgICBAdXBkYXRlVG9vbHRpcCgpXG5cbiAgICAgICMgVHJpZ2dlciBhZ2FpbiBzbyB0aGUgdG9vbHRpcCBzaG93c1xuICAgICAgQGVsZW1lbnQuZGlzcGF0Y2hFdmVudChuZXcgQ3VzdG9tRXZlbnQoJ21vdXNlZW50ZXInLCBidWJibGVzOiB0cnVlKSlcblxuICAgIEBtb3VzZUVudGVyU3Vic2NyaXB0aW9uID0gZGlzcG9zZTogPT5cbiAgICAgIEBlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNlZW50ZXInLCBvbk1vdXNlRW50ZXIpXG4gICAgICBAbW91c2VFbnRlclN1YnNjcmlwdGlvbiA9IG51bGxcblxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZW50ZXInLCBvbk1vdXNlRW50ZXIpXG5cbiAgdXBkYXRlVG9vbHRpcDogLT5cbiAgICByZXR1cm4gdW5sZXNzIEBoYXNCZWVuTW91c2VkT3ZlclxuXG4gICAgQGRlc3Ryb3lUb29sdGlwKClcblxuICAgIGlmIEBwYXRoXG4gICAgICBAdG9vbHRpcCA9IGF0b20udG9vbHRpcHMuYWRkIEBlbGVtZW50LFxuICAgICAgICB0aXRsZTogQHBhdGhcbiAgICAgICAgaHRtbDogZmFsc2VcbiAgICAgICAgZGVsYXk6XG4gICAgICAgICAgc2hvdzogMTAwMFxuICAgICAgICAgIGhpZGU6IDEwMFxuICAgICAgICBwbGFjZW1lbnQ6ICdib3R0b20nXG5cbiAgZGVzdHJveVRvb2x0aXA6IC0+XG4gICAgcmV0dXJuIHVubGVzcyBAaGFzQmVlbk1vdXNlZE92ZXJcbiAgICBAdG9vbHRpcD8uZGlzcG9zZSgpXG5cbiAgZGVzdHJveTogLT5cbiAgICBAc3Vic2NyaXB0aW9ucz8uZGlzcG9zZSgpXG4gICAgQG1vdXNlRW50ZXJTdWJzY3JpcHRpb24/LmRpc3Bvc2UoKVxuICAgIEByZXBvU3Vic2NyaXB0aW9ucz8uZGlzcG9zZSgpXG4gICAgQGRlc3Ryb3lUb29sdGlwKClcbiAgICBAZWxlbWVudC5yZW1vdmUoKVxuXG4gIHVwZGF0ZURhdGFBdHRyaWJ1dGVzOiAtPlxuICAgIGlmIEBwYXRoXG4gICAgICBAaXRlbVRpdGxlLmRhdGFzZXQubmFtZSA9IHBhdGguYmFzZW5hbWUoQHBhdGgpXG4gICAgICBAaXRlbVRpdGxlLmRhdGFzZXQucGF0aCA9IEBwYXRoXG4gICAgZWxzZVxuICAgICAgZGVsZXRlIEBpdGVtVGl0bGUuZGF0YXNldC5uYW1lXG4gICAgICBkZWxldGUgQGl0ZW1UaXRsZS5kYXRhc2V0LnBhdGhcblxuICAgIGlmIGl0ZW1DbGFzcyA9IEBpdGVtLmNvbnN0cnVjdG9yPy5uYW1lXG4gICAgICBAZWxlbWVudC5kYXRhc2V0LnR5cGUgPSBpdGVtQ2xhc3NcbiAgICBlbHNlXG4gICAgICBkZWxldGUgQGVsZW1lbnQuZGF0YXNldC50eXBlXG5cbiAgdXBkYXRlVGl0bGU6ICh7dXBkYXRlU2libGluZ3MsIHVzZUxvbmdUaXRsZX09e30pIC0+XG4gICAgcmV0dXJuIGlmIEB1cGRhdGluZ1RpdGxlXG4gICAgQHVwZGF0aW5nVGl0bGUgPSB0cnVlXG5cbiAgICBpZiB1cGRhdGVTaWJsaW5ncyBpcyBmYWxzZVxuICAgICAgdGl0bGUgPSBAaXRlbS5nZXRUaXRsZSgpXG4gICAgICB0aXRsZSA9IEBpdGVtLmdldExvbmdUaXRsZT8oKSA/IHRpdGxlIGlmIHVzZUxvbmdUaXRsZVxuICAgICAgQGl0ZW1UaXRsZS50ZXh0Q29udGVudCA9IHRpdGxlXG4gICAgZWxzZVxuICAgICAgdGl0bGUgPSBAaXRlbS5nZXRUaXRsZSgpXG4gICAgICB1c2VMb25nVGl0bGUgPSBmYWxzZVxuICAgICAgZm9yIHRhYiBpbiBAdGFicyB3aGVuIHRhYiBpc250IHRoaXNcbiAgICAgICAgaWYgdGFiLml0ZW0uZ2V0VGl0bGUoKSBpcyB0aXRsZVxuICAgICAgICAgIHRhYi51cGRhdGVUaXRsZSh1cGRhdGVTaWJsaW5nczogZmFsc2UsIHVzZUxvbmdUaXRsZTogdHJ1ZSlcbiAgICAgICAgICB1c2VMb25nVGl0bGUgPSB0cnVlXG4gICAgICB0aXRsZSA9IEBpdGVtLmdldExvbmdUaXRsZT8oKSA/IHRpdGxlIGlmIHVzZUxvbmdUaXRsZVxuXG4gICAgICBAaXRlbVRpdGxlLnRleHRDb250ZW50ID0gdGl0bGVcblxuICAgIEB1cGRhdGluZ1RpdGxlID0gZmFsc2VcblxuICB1cGRhdGVJY29uOiAtPlxuICAgIGdldEljb25TZXJ2aWNlcygpLnVwZGF0ZVRhYkljb24odGhpcylcblxuICBpc0l0ZW1QZW5kaW5nOiAtPlxuICAgIGlmIEBwYW5lLmdldFBlbmRpbmdJdGVtP1xuICAgICAgQHBhbmUuZ2V0UGVuZGluZ0l0ZW0oKSBpcyBAaXRlbVxuICAgIGVsc2UgaWYgQGl0ZW0uaXNQZW5kaW5nP1xuICAgICAgQGl0ZW0uaXNQZW5kaW5nKClcblxuICB0ZXJtaW5hdGVQZW5kaW5nU3RhdGU6IC0+XG4gICAgaWYgQHBhbmUuY2xlYXJQZW5kaW5nSXRlbT9cbiAgICAgIEBwYW5lLmNsZWFyUGVuZGluZ0l0ZW0oKSBpZiBAcGFuZS5nZXRQZW5kaW5nSXRlbSgpIGlzIEBpdGVtXG4gICAgZWxzZSBpZiBAaXRlbS50ZXJtaW5hdGVQZW5kaW5nU3RhdGU/XG4gICAgICBAaXRlbS50ZXJtaW5hdGVQZW5kaW5nU3RhdGUoKVxuXG4gIGNsZWFyUGVuZGluZzogLT5cbiAgICBAaXRlbVRpdGxlLmNsYXNzTGlzdC5yZW1vdmUoJ3RlbXAnKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ3BlbmRpbmctdGFiJylcblxuICB1cGRhdGVJY29uVmlzaWJpbGl0eTogLT5cbiAgICBpZiBhdG9tLmNvbmZpZy5nZXQgJ3RhYnMuc2hvd0ljb25zJ1xuICAgICAgQGl0ZW1UaXRsZS5jbGFzc0xpc3QucmVtb3ZlKCdoaWRlLWljb24nKVxuICAgIGVsc2VcbiAgICAgIEBpdGVtVGl0bGUuY2xhc3NMaXN0LmFkZCgnaGlkZS1pY29uJylcblxuICB1cGRhdGVNb2RpZmllZFN0YXR1czogLT5cbiAgICBpZiBAaXRlbS5pc01vZGlmaWVkPygpXG4gICAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdtb2RpZmllZCcpIHVubGVzcyBAaXNNb2RpZmllZFxuICAgICAgQGlzTW9kaWZpZWQgPSB0cnVlXG4gICAgZWxzZVxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnbW9kaWZpZWQnKSBpZiBAaXNNb2RpZmllZFxuICAgICAgQGlzTW9kaWZpZWQgPSBmYWxzZVxuXG4gIHNldHVwVmNzU3RhdHVzOiAtPlxuICAgIHJldHVybiB1bmxlc3MgQHBhdGg/XG4gICAgQHJlcG9Gb3JQYXRoKEBwYXRoKS50aGVuIChyZXBvKSA9PlxuICAgICAgQHN1YnNjcmliZVRvUmVwbyhyZXBvKVxuICAgICAgQHVwZGF0ZVZjc1N0YXR1cyhyZXBvKVxuXG4gICMgU3Vic2NyaWJlIHRvIHRoZSBwcm9qZWN0J3MgcmVwbyBmb3IgY2hhbmdlcyB0byB0aGUgVkNTIHN0YXR1cyBvZiB0aGUgZmlsZS5cbiAgc3Vic2NyaWJlVG9SZXBvOiAocmVwbykgLT5cbiAgICByZXR1cm4gdW5sZXNzIHJlcG8/XG5cbiAgICAjIFJlbW92ZSBwcmV2aW91cyByZXBvIHN1YnNjcmlwdGlvbnMuXG4gICAgQHJlcG9TdWJzY3JpcHRpb25zPy5kaXNwb3NlKClcbiAgICBAcmVwb1N1YnNjcmlwdGlvbnMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZSgpXG5cbiAgICBAcmVwb1N1YnNjcmlwdGlvbnMuYWRkIHJlcG8ub25EaWRDaGFuZ2VTdGF0dXMgKGV2ZW50KSA9PlxuICAgICAgQHVwZGF0ZVZjc1N0YXR1cyhyZXBvLCBldmVudC5wYXRoU3RhdHVzKSBpZiBldmVudC5wYXRoIGlzIEBwYXRoXG4gICAgQHJlcG9TdWJzY3JpcHRpb25zLmFkZCByZXBvLm9uRGlkQ2hhbmdlU3RhdHVzZXMgPT5cbiAgICAgIEB1cGRhdGVWY3NTdGF0dXMocmVwbylcblxuICByZXBvRm9yUGF0aDogLT5cbiAgICBmb3IgZGlyIGluIGF0b20ucHJvamVjdC5nZXREaXJlY3RvcmllcygpXG4gICAgICByZXR1cm4gYXRvbS5wcm9qZWN0LnJlcG9zaXRvcnlGb3JEaXJlY3RvcnkoZGlyKSBpZiBkaXIuY29udGFpbnMgQHBhdGhcbiAgICBQcm9taXNlLnJlc29sdmUobnVsbClcblxuICAjIFVwZGF0ZSB0aGUgVkNTIHN0YXR1cyBwcm9wZXJ0eSBvZiB0aGlzIHRhYiB1c2luZyB0aGUgcmVwby5cbiAgdXBkYXRlVmNzU3RhdHVzOiAocmVwbywgc3RhdHVzKSAtPlxuICAgIHJldHVybiB1bmxlc3MgcmVwbz9cblxuICAgIG5ld1N0YXR1cyA9IG51bGxcbiAgICBpZiByZXBvLmlzUGF0aElnbm9yZWQoQHBhdGgpXG4gICAgICBuZXdTdGF0dXMgPSAnaWdub3JlZCdcbiAgICBlbHNlXG4gICAgICBzdGF0dXMgPSByZXBvLmdldENhY2hlZFBhdGhTdGF0dXMoQHBhdGgpIHVubGVzcyBzdGF0dXM/XG4gICAgICBpZiByZXBvLmlzU3RhdHVzTW9kaWZpZWQoc3RhdHVzKVxuICAgICAgICBuZXdTdGF0dXMgPSAnbW9kaWZpZWQnXG4gICAgICBlbHNlIGlmIHJlcG8uaXNTdGF0dXNOZXcoc3RhdHVzKVxuICAgICAgICBuZXdTdGF0dXMgPSAnYWRkZWQnXG5cbiAgICBpZiBuZXdTdGF0dXMgaXNudCBAc3RhdHVzXG4gICAgICBAc3RhdHVzID0gbmV3U3RhdHVzXG4gICAgICBAdXBkYXRlVmNzQ29sb3JpbmcoKVxuXG4gIHVwZGF0ZVZjc0NvbG9yaW5nOiAtPlxuICAgIEBpdGVtVGl0bGUuY2xhc3NMaXN0LnJlbW92ZSgnc3RhdHVzLWlnbm9yZWQnLCAnc3RhdHVzLW1vZGlmaWVkJywgICdzdGF0dXMtYWRkZWQnKVxuICAgIGlmIEBzdGF0dXMgYW5kIGF0b20uY29uZmlnLmdldCAndGFicy5lbmFibGVWY3NDb2xvcmluZydcbiAgICAgIEBpdGVtVGl0bGUuY2xhc3NMaXN0LmFkZChcInN0YXR1cy0je0BzdGF0dXN9XCIpXG5cbiAgdW5zZXRWY3NTdGF0dXM6IC0+XG4gICAgQHJlcG9TdWJzY3JpcHRpb25zPy5kaXNwb3NlKClcbiAgICBkZWxldGUgQHN0YXR1c1xuICAgIEB1cGRhdGVWY3NDb2xvcmluZygpXG4iXX0=
