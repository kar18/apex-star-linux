(function() {
  var CompositeDisposable, Range, SelectNext, _, ref;

  _ = require('underscore-plus');

  ref = require('atom'), CompositeDisposable = ref.CompositeDisposable, Range = ref.Range;

  module.exports = SelectNext = (function() {
    SelectNext.prototype.selectionRanges = null;

    function SelectNext(editor) {
      this.editor = editor;
      this.selectionRanges = [];
    }

    SelectNext.prototype.findAndSelectNext = function() {
      if (this.editor.getLastSelection().isEmpty()) {
        return this.selectWord();
      } else {
        return this.selectNextOccurrence();
      }
    };

    SelectNext.prototype.findAndSelectAll = function() {
      if (this.editor.getLastSelection().isEmpty()) {
        this.selectWord();
      }
      return this.selectAllOccurrences();
    };

    SelectNext.prototype.undoLastSelection = function() {
      this.updateSavedSelections();
      if (this.selectionRanges.length < 1) {
        return;
      }
      if (this.selectionRanges.length > 1) {
        this.selectionRanges.pop();
        this.editor.setSelectedBufferRanges(this.selectionRanges);
      } else {
        this.editor.clearSelections();
      }
      return this.editor.scrollToCursorPosition();
    };

    SelectNext.prototype.skipCurrentSelection = function() {
      var lastSelection;
      this.updateSavedSelections();
      if (this.selectionRanges.length < 1) {
        return;
      }
      if (this.selectionRanges.length > 1) {
        lastSelection = this.selectionRanges.pop();
        this.editor.setSelectedBufferRanges(this.selectionRanges);
        return this.selectNextOccurrence({
          start: lastSelection.end
        });
      } else {
        this.selectNextOccurrence();
        this.selectionRanges.shift();
        if (this.selectionRanges.length < 1) {
          return;
        }
        return this.editor.setSelectedBufferRanges(this.selectionRanges);
      }
    };

    SelectNext.prototype.selectWord = function() {
      var clearWordSelected, disposables, lastSelection;
      this.editor.selectWordsContainingCursors();
      lastSelection = this.editor.getLastSelection();
      if (this.wordSelected = this.isWordSelected(lastSelection)) {
        disposables = new CompositeDisposable;
        clearWordSelected = (function(_this) {
          return function() {
            _this.wordSelected = null;
            return disposables.dispose();
          };
        })(this);
        disposables.add(lastSelection.onDidChangeRange(clearWordSelected));
        return disposables.add(lastSelection.onDidDestroy(clearWordSelected));
      }
    };

    SelectNext.prototype.selectAllOccurrences = function() {
      var range;
      range = [[0, 0], this.editor.getEofBufferPosition()];
      return this.scanForNextOccurrence(range, (function(_this) {
        return function(arg) {
          var range, stop;
          range = arg.range, stop = arg.stop;
          return _this.addSelection(range);
        };
      })(this));
    };

    SelectNext.prototype.selectNextOccurrence = function(options) {
      var range, ref1, startingRange;
      if (options == null) {
        options = {};
      }
      startingRange = (ref1 = options.start) != null ? ref1 : this.editor.getSelectedBufferRange().end;
      range = this.findNextOccurrence([startingRange, this.editor.getEofBufferPosition()]);
      if (range == null) {
        range = this.findNextOccurrence([[0, 0], this.editor.getSelections()[0].getBufferRange().start]);
      }
      if (range != null) {
        return this.addSelection(range);
      }
    };

    SelectNext.prototype.findNextOccurrence = function(scanRange) {
      var foundRange;
      foundRange = null;
      this.scanForNextOccurrence(scanRange, function(arg) {
        var range, stop;
        range = arg.range, stop = arg.stop;
        foundRange = range;
        return stop();
      });
      return foundRange;
    };

    SelectNext.prototype.addSelection = function(range) {
      var reversed, selection;
      reversed = this.editor.getLastSelection().isReversed();
      selection = this.editor.addSelectionForBufferRange(range, {
        reversed: reversed
      });
      return this.updateSavedSelections(selection);
    };

    SelectNext.prototype.scanForNextOccurrence = function(range, callback) {
      var nonWordCharacters, selection, text;
      selection = this.editor.getLastSelection();
      text = _.escapeRegExp(selection.getText());
      if (this.wordSelected) {
        nonWordCharacters = atom.config.get('editor.nonWordCharacters');
        text = "(^|[ \t" + (_.escapeRegExp(nonWordCharacters)) + "]+)" + text + "(?=$|[\\s" + (_.escapeRegExp(nonWordCharacters)) + "]+)";
      }
      return this.editor.scanInBufferRange(new RegExp(text, 'g'), range, function(result) {
        var prefix;
        if (prefix = result.match[1]) {
          result.range = result.range.translate([0, prefix.length], [0, 0]);
        }
        return callback(result);
      });
    };

    SelectNext.prototype.updateSavedSelections = function(selection) {
      var i, len, results, s, selectionRange, selections;
      if (selection == null) {
        selection = null;
      }
      selections = this.editor.getSelections();
      if (selections.length < 3) {
        this.selectionRanges = [];
      }
      if (this.selectionRanges.length === 0) {
        results = [];
        for (i = 0, len = selections.length; i < len; i++) {
          s = selections[i];
          results.push(this.selectionRanges.push(s.getBufferRange()));
        }
        return results;
      } else if (selection) {
        selectionRange = selection.getBufferRange();
        if (this.selectionRanges.some(function(existingRange) {
          return existingRange.isEqual(selectionRange);
        })) {
          return;
        }
        return this.selectionRanges.push(selectionRange);
      }
    };

    SelectNext.prototype.isNonWordCharacter = function(character) {
      var nonWordCharacters;
      nonWordCharacters = atom.config.get('editor.nonWordCharacters');
      return new RegExp("[ \t" + (_.escapeRegExp(nonWordCharacters)) + "]").test(character);
    };

    SelectNext.prototype.isNonWordCharacterToTheLeft = function(selection) {
      var range, selectionStart;
      selectionStart = selection.getBufferRange().start;
      range = Range.fromPointWithDelta(selectionStart, 0, -1);
      return this.isNonWordCharacter(this.editor.getTextInBufferRange(range));
    };

    SelectNext.prototype.isNonWordCharacterToTheRight = function(selection) {
      var range, selectionEnd;
      selectionEnd = selection.getBufferRange().end;
      range = Range.fromPointWithDelta(selectionEnd, 0, 1);
      return this.isNonWordCharacter(this.editor.getTextInBufferRange(range));
    };

    SelectNext.prototype.isWordSelected = function(selection) {
      var containsOnlyWordCharacters, lineRange, nonWordCharacterToTheLeft, nonWordCharacterToTheRight, selectionRange;
      if (selection.getBufferRange().isSingleLine()) {
        selectionRange = selection.getBufferRange();
        lineRange = this.editor.bufferRangeForBufferRow(selectionRange.start.row);
        nonWordCharacterToTheLeft = _.isEqual(selectionRange.start, lineRange.start) || this.isNonWordCharacterToTheLeft(selection);
        nonWordCharacterToTheRight = _.isEqual(selectionRange.end, lineRange.end) || this.isNonWordCharacterToTheRight(selection);
        containsOnlyWordCharacters = !this.isNonWordCharacter(selection.getText());
        return nonWordCharacterToTheLeft && nonWordCharacterToTheRight && containsOnlyWordCharacters;
      } else {
        return false;
      }
    };

    return SelectNext;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9maW5kLWFuZC1yZXBsYWNlL2xpYi9zZWxlY3QtbmV4dC5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLENBQUEsR0FBSSxPQUFBLENBQVEsaUJBQVI7O0VBQ0osTUFBK0IsT0FBQSxDQUFRLE1BQVIsQ0FBL0IsRUFBQyw2Q0FBRCxFQUFzQjs7RUFLdEIsTUFBTSxDQUFDLE9BQVAsR0FDTTt5QkFDSixlQUFBLEdBQWlCOztJQUVKLG9CQUFDLE1BQUQ7TUFBQyxJQUFDLENBQUEsU0FBRDtNQUNaLElBQUMsQ0FBQSxlQUFELEdBQW1CO0lBRFI7O3lCQUdiLGlCQUFBLEdBQW1CLFNBQUE7TUFDakIsSUFBRyxJQUFDLENBQUEsTUFBTSxDQUFDLGdCQUFSLENBQUEsQ0FBMEIsQ0FBQyxPQUEzQixDQUFBLENBQUg7ZUFDRSxJQUFDLENBQUEsVUFBRCxDQUFBLEVBREY7T0FBQSxNQUFBO2VBR0UsSUFBQyxDQUFBLG9CQUFELENBQUEsRUFIRjs7SUFEaUI7O3lCQU1uQixnQkFBQSxHQUFrQixTQUFBO01BQ2hCLElBQWlCLElBQUMsQ0FBQSxNQUFNLENBQUMsZ0JBQVIsQ0FBQSxDQUEwQixDQUFDLE9BQTNCLENBQUEsQ0FBakI7UUFBQSxJQUFDLENBQUEsVUFBRCxDQUFBLEVBQUE7O2FBQ0EsSUFBQyxDQUFBLG9CQUFELENBQUE7SUFGZ0I7O3lCQUlsQixpQkFBQSxHQUFtQixTQUFBO01BQ2pCLElBQUMsQ0FBQSxxQkFBRCxDQUFBO01BRUEsSUFBVSxJQUFDLENBQUEsZUFBZSxDQUFDLE1BQWpCLEdBQTBCLENBQXBDO0FBQUEsZUFBQTs7TUFFQSxJQUFHLElBQUMsQ0FBQSxlQUFlLENBQUMsTUFBakIsR0FBMEIsQ0FBN0I7UUFDRSxJQUFDLENBQUEsZUFBZSxDQUFDLEdBQWpCLENBQUE7UUFDQSxJQUFDLENBQUEsTUFBTSxDQUFDLHVCQUFSLENBQWdDLElBQUMsQ0FBQSxlQUFqQyxFQUZGO09BQUEsTUFBQTtRQUlFLElBQUMsQ0FBQSxNQUFNLENBQUMsZUFBUixDQUFBLEVBSkY7O2FBTUEsSUFBQyxDQUFBLE1BQU0sQ0FBQyxzQkFBUixDQUFBO0lBWGlCOzt5QkFhbkIsb0JBQUEsR0FBc0IsU0FBQTtBQUNwQixVQUFBO01BQUEsSUFBQyxDQUFBLHFCQUFELENBQUE7TUFFQSxJQUFVLElBQUMsQ0FBQSxlQUFlLENBQUMsTUFBakIsR0FBMEIsQ0FBcEM7QUFBQSxlQUFBOztNQUVBLElBQUcsSUFBQyxDQUFBLGVBQWUsQ0FBQyxNQUFqQixHQUEwQixDQUE3QjtRQUNFLGFBQUEsR0FBZ0IsSUFBQyxDQUFBLGVBQWUsQ0FBQyxHQUFqQixDQUFBO1FBQ2hCLElBQUMsQ0FBQSxNQUFNLENBQUMsdUJBQVIsQ0FBZ0MsSUFBQyxDQUFBLGVBQWpDO2VBQ0EsSUFBQyxDQUFBLG9CQUFELENBQXNCO1VBQUEsS0FBQSxFQUFPLGFBQWEsQ0FBQyxHQUFyQjtTQUF0QixFQUhGO09BQUEsTUFBQTtRQUtFLElBQUMsQ0FBQSxvQkFBRCxDQUFBO1FBQ0EsSUFBQyxDQUFBLGVBQWUsQ0FBQyxLQUFqQixDQUFBO1FBQ0EsSUFBVSxJQUFDLENBQUEsZUFBZSxDQUFDLE1BQWpCLEdBQTBCLENBQXBDO0FBQUEsaUJBQUE7O2VBQ0EsSUFBQyxDQUFBLE1BQU0sQ0FBQyx1QkFBUixDQUFnQyxJQUFDLENBQUEsZUFBakMsRUFSRjs7SUFMb0I7O3lCQWV0QixVQUFBLEdBQVksU0FBQTtBQUNWLFVBQUE7TUFBQSxJQUFDLENBQUEsTUFBTSxDQUFDLDRCQUFSLENBQUE7TUFDQSxhQUFBLEdBQWdCLElBQUMsQ0FBQSxNQUFNLENBQUMsZ0JBQVIsQ0FBQTtNQUNoQixJQUFHLElBQUMsQ0FBQSxZQUFELEdBQWdCLElBQUMsQ0FBQSxjQUFELENBQWdCLGFBQWhCLENBQW5CO1FBQ0UsV0FBQSxHQUFjLElBQUk7UUFDbEIsaUJBQUEsR0FBb0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTtZQUNsQixLQUFDLENBQUEsWUFBRCxHQUFnQjttQkFDaEIsV0FBVyxDQUFDLE9BQVosQ0FBQTtVQUZrQjtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7UUFHcEIsV0FBVyxDQUFDLEdBQVosQ0FBZ0IsYUFBYSxDQUFDLGdCQUFkLENBQStCLGlCQUEvQixDQUFoQjtlQUNBLFdBQVcsQ0FBQyxHQUFaLENBQWdCLGFBQWEsQ0FBQyxZQUFkLENBQTJCLGlCQUEzQixDQUFoQixFQU5GOztJQUhVOzt5QkFXWixvQkFBQSxHQUFzQixTQUFBO0FBQ3BCLFVBQUE7TUFBQSxLQUFBLEdBQVEsQ0FBQyxDQUFDLENBQUQsRUFBSSxDQUFKLENBQUQsRUFBUyxJQUFDLENBQUEsTUFBTSxDQUFDLG9CQUFSLENBQUEsQ0FBVDthQUNSLElBQUMsQ0FBQSxxQkFBRCxDQUF1QixLQUF2QixFQUE4QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsR0FBRDtBQUM1QixjQUFBO1VBRDhCLG1CQUFPO2lCQUNyQyxLQUFDLENBQUEsWUFBRCxDQUFjLEtBQWQ7UUFENEI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTlCO0lBRm9COzt5QkFLdEIsb0JBQUEsR0FBc0IsU0FBQyxPQUFEO0FBQ3BCLFVBQUE7O1FBRHFCLFVBQVE7O01BQzdCLGFBQUEsMkNBQWdDLElBQUMsQ0FBQSxNQUFNLENBQUMsc0JBQVIsQ0FBQSxDQUFnQyxDQUFDO01BQ2pFLEtBQUEsR0FBUSxJQUFDLENBQUEsa0JBQUQsQ0FBb0IsQ0FBQyxhQUFELEVBQWdCLElBQUMsQ0FBQSxNQUFNLENBQUMsb0JBQVIsQ0FBQSxDQUFoQixDQUFwQjs7UUFDUixRQUFTLElBQUMsQ0FBQSxrQkFBRCxDQUFvQixDQUFDLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FBRCxFQUFTLElBQUMsQ0FBQSxNQUFNLENBQUMsYUFBUixDQUFBLENBQXdCLENBQUEsQ0FBQSxDQUFFLENBQUMsY0FBM0IsQ0FBQSxDQUEyQyxDQUFDLEtBQXJELENBQXBCOztNQUNULElBQXdCLGFBQXhCO2VBQUEsSUFBQyxDQUFBLFlBQUQsQ0FBYyxLQUFkLEVBQUE7O0lBSm9COzt5QkFNdEIsa0JBQUEsR0FBb0IsU0FBQyxTQUFEO0FBQ2xCLFVBQUE7TUFBQSxVQUFBLEdBQWE7TUFDYixJQUFDLENBQUEscUJBQUQsQ0FBdUIsU0FBdkIsRUFBa0MsU0FBQyxHQUFEO0FBQ2hDLFlBQUE7UUFEa0MsbUJBQU87UUFDekMsVUFBQSxHQUFhO2VBQ2IsSUFBQSxDQUFBO01BRmdDLENBQWxDO2FBR0E7SUFMa0I7O3lCQU9wQixZQUFBLEdBQWMsU0FBQyxLQUFEO0FBQ1osVUFBQTtNQUFBLFFBQUEsR0FBVyxJQUFDLENBQUEsTUFBTSxDQUFDLGdCQUFSLENBQUEsQ0FBMEIsQ0FBQyxVQUEzQixDQUFBO01BQ1gsU0FBQSxHQUFZLElBQUMsQ0FBQSxNQUFNLENBQUMsMEJBQVIsQ0FBbUMsS0FBbkMsRUFBMEM7UUFBQyxVQUFBLFFBQUQ7T0FBMUM7YUFDWixJQUFDLENBQUEscUJBQUQsQ0FBdUIsU0FBdkI7SUFIWTs7eUJBS2QscUJBQUEsR0FBdUIsU0FBQyxLQUFELEVBQVEsUUFBUjtBQUNyQixVQUFBO01BQUEsU0FBQSxHQUFZLElBQUMsQ0FBQSxNQUFNLENBQUMsZ0JBQVIsQ0FBQTtNQUNaLElBQUEsR0FBTyxDQUFDLENBQUMsWUFBRixDQUFlLFNBQVMsQ0FBQyxPQUFWLENBQUEsQ0FBZjtNQUVQLElBQUcsSUFBQyxDQUFBLFlBQUo7UUFDRSxpQkFBQSxHQUFvQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsMEJBQWhCO1FBQ3BCLElBQUEsR0FBTyxTQUFBLEdBQVMsQ0FBQyxDQUFDLENBQUMsWUFBRixDQUFlLGlCQUFmLENBQUQsQ0FBVCxHQUE0QyxLQUE1QyxHQUFpRCxJQUFqRCxHQUFzRCxXQUF0RCxHQUFnRSxDQUFDLENBQUMsQ0FBQyxZQUFGLENBQWUsaUJBQWYsQ0FBRCxDQUFoRSxHQUFtRyxNQUY1Rzs7YUFJQSxJQUFDLENBQUEsTUFBTSxDQUFDLGlCQUFSLENBQTBCLElBQUksTUFBSixDQUFXLElBQVgsRUFBaUIsR0FBakIsQ0FBMUIsRUFBaUQsS0FBakQsRUFBd0QsU0FBQyxNQUFEO0FBQ3RELFlBQUE7UUFBQSxJQUFHLE1BQUEsR0FBUyxNQUFNLENBQUMsS0FBTSxDQUFBLENBQUEsQ0FBekI7VUFDRSxNQUFNLENBQUMsS0FBUCxHQUFlLE1BQU0sQ0FBQyxLQUFLLENBQUMsU0FBYixDQUF1QixDQUFDLENBQUQsRUFBSSxNQUFNLENBQUMsTUFBWCxDQUF2QixFQUEyQyxDQUFDLENBQUQsRUFBSSxDQUFKLENBQTNDLEVBRGpCOztlQUVBLFFBQUEsQ0FBUyxNQUFUO01BSHNELENBQXhEO0lBUnFCOzt5QkFhdkIscUJBQUEsR0FBdUIsU0FBQyxTQUFEO0FBQ3JCLFVBQUE7O1FBRHNCLFlBQVU7O01BQ2hDLFVBQUEsR0FBYSxJQUFDLENBQUEsTUFBTSxDQUFDLGFBQVIsQ0FBQTtNQUNiLElBQXlCLFVBQVUsQ0FBQyxNQUFYLEdBQW9CLENBQTdDO1FBQUEsSUFBQyxDQUFBLGVBQUQsR0FBbUIsR0FBbkI7O01BQ0EsSUFBRyxJQUFDLENBQUEsZUFBZSxDQUFDLE1BQWpCLEtBQTJCLENBQTlCO0FBQ0U7YUFBQSw0Q0FBQTs7dUJBQUEsSUFBQyxDQUFBLGVBQWUsQ0FBQyxJQUFqQixDQUFzQixDQUFDLENBQUMsY0FBRixDQUFBLENBQXRCO0FBQUE7dUJBREY7T0FBQSxNQUVLLElBQUcsU0FBSDtRQUNILGNBQUEsR0FBaUIsU0FBUyxDQUFDLGNBQVYsQ0FBQTtRQUNqQixJQUFVLElBQUMsQ0FBQSxlQUFlLENBQUMsSUFBakIsQ0FBc0IsU0FBQyxhQUFEO2lCQUFtQixhQUFhLENBQUMsT0FBZCxDQUFzQixjQUF0QjtRQUFuQixDQUF0QixDQUFWO0FBQUEsaUJBQUE7O2VBQ0EsSUFBQyxDQUFBLGVBQWUsQ0FBQyxJQUFqQixDQUFzQixjQUF0QixFQUhHOztJQUxnQjs7eUJBVXZCLGtCQUFBLEdBQW9CLFNBQUMsU0FBRDtBQUNsQixVQUFBO01BQUEsaUJBQUEsR0FBb0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLDBCQUFoQjthQUNwQixJQUFJLE1BQUosQ0FBVyxNQUFBLEdBQU0sQ0FBQyxDQUFDLENBQUMsWUFBRixDQUFlLGlCQUFmLENBQUQsQ0FBTixHQUF5QyxHQUFwRCxDQUF1RCxDQUFDLElBQXhELENBQTZELFNBQTdEO0lBRmtCOzt5QkFJcEIsMkJBQUEsR0FBNkIsU0FBQyxTQUFEO0FBQzNCLFVBQUE7TUFBQSxjQUFBLEdBQWlCLFNBQVMsQ0FBQyxjQUFWLENBQUEsQ0FBMEIsQ0FBQztNQUM1QyxLQUFBLEdBQVEsS0FBSyxDQUFDLGtCQUFOLENBQXlCLGNBQXpCLEVBQXlDLENBQXpDLEVBQTRDLENBQUMsQ0FBN0M7YUFDUixJQUFDLENBQUEsa0JBQUQsQ0FBb0IsSUFBQyxDQUFBLE1BQU0sQ0FBQyxvQkFBUixDQUE2QixLQUE3QixDQUFwQjtJQUgyQjs7eUJBSzdCLDRCQUFBLEdBQThCLFNBQUMsU0FBRDtBQUM1QixVQUFBO01BQUEsWUFBQSxHQUFlLFNBQVMsQ0FBQyxjQUFWLENBQUEsQ0FBMEIsQ0FBQztNQUMxQyxLQUFBLEdBQVEsS0FBSyxDQUFDLGtCQUFOLENBQXlCLFlBQXpCLEVBQXVDLENBQXZDLEVBQTBDLENBQTFDO2FBQ1IsSUFBQyxDQUFBLGtCQUFELENBQW9CLElBQUMsQ0FBQSxNQUFNLENBQUMsb0JBQVIsQ0FBNkIsS0FBN0IsQ0FBcEI7SUFINEI7O3lCQUs5QixjQUFBLEdBQWdCLFNBQUMsU0FBRDtBQUNkLFVBQUE7TUFBQSxJQUFHLFNBQVMsQ0FBQyxjQUFWLENBQUEsQ0FBMEIsQ0FBQyxZQUEzQixDQUFBLENBQUg7UUFDRSxjQUFBLEdBQWlCLFNBQVMsQ0FBQyxjQUFWLENBQUE7UUFDakIsU0FBQSxHQUFZLElBQUMsQ0FBQSxNQUFNLENBQUMsdUJBQVIsQ0FBZ0MsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFyRDtRQUNaLHlCQUFBLEdBQTRCLENBQUMsQ0FBQyxPQUFGLENBQVUsY0FBYyxDQUFDLEtBQXpCLEVBQWdDLFNBQVMsQ0FBQyxLQUExQyxDQUFBLElBQzFCLElBQUMsQ0FBQSwyQkFBRCxDQUE2QixTQUE3QjtRQUNGLDBCQUFBLEdBQTZCLENBQUMsQ0FBQyxPQUFGLENBQVUsY0FBYyxDQUFDLEdBQXpCLEVBQThCLFNBQVMsQ0FBQyxHQUF4QyxDQUFBLElBQzNCLElBQUMsQ0FBQSw0QkFBRCxDQUE4QixTQUE5QjtRQUNGLDBCQUFBLEdBQTZCLENBQUksSUFBQyxDQUFBLGtCQUFELENBQW9CLFNBQVMsQ0FBQyxPQUFWLENBQUEsQ0FBcEI7ZUFFakMseUJBQUEsSUFBOEIsMEJBQTlCLElBQTZELDJCQVQvRDtPQUFBLE1BQUE7ZUFXRSxNQVhGOztJQURjOzs7OztBQTFIbEIiLCJzb3VyY2VzQ29udGVudCI6WyJfID0gcmVxdWlyZSAndW5kZXJzY29yZS1wbHVzJ1xue0NvbXBvc2l0ZURpc3Bvc2FibGUsIFJhbmdlfSA9IHJlcXVpcmUgJ2F0b20nXG5cbiMgRmluZCBhbmQgc2VsZWN0IHRoZSBuZXh0IG9jY3VycmVuY2Ugb2YgdGhlIGN1cnJlbnRseSBzZWxlY3RlZCB0ZXh0LlxuI1xuIyBUaGUgd29yZCB1bmRlciB0aGUgY3Vyc29yIHdpbGwgYmUgc2VsZWN0ZWQgaWYgdGhlIHNlbGVjdGlvbiBpcyBlbXB0eS5cbm1vZHVsZS5leHBvcnRzID1cbmNsYXNzIFNlbGVjdE5leHRcbiAgc2VsZWN0aW9uUmFuZ2VzOiBudWxsXG5cbiAgY29uc3RydWN0b3I6IChAZWRpdG9yKSAtPlxuICAgIEBzZWxlY3Rpb25SYW5nZXMgPSBbXVxuXG4gIGZpbmRBbmRTZWxlY3ROZXh0OiAtPlxuICAgIGlmIEBlZGl0b3IuZ2V0TGFzdFNlbGVjdGlvbigpLmlzRW1wdHkoKVxuICAgICAgQHNlbGVjdFdvcmQoKVxuICAgIGVsc2VcbiAgICAgIEBzZWxlY3ROZXh0T2NjdXJyZW5jZSgpXG5cbiAgZmluZEFuZFNlbGVjdEFsbDogLT5cbiAgICBAc2VsZWN0V29yZCgpIGlmIEBlZGl0b3IuZ2V0TGFzdFNlbGVjdGlvbigpLmlzRW1wdHkoKVxuICAgIEBzZWxlY3RBbGxPY2N1cnJlbmNlcygpXG5cbiAgdW5kb0xhc3RTZWxlY3Rpb246IC0+XG4gICAgQHVwZGF0ZVNhdmVkU2VsZWN0aW9ucygpXG5cbiAgICByZXR1cm4gaWYgQHNlbGVjdGlvblJhbmdlcy5sZW5ndGggPCAxXG5cbiAgICBpZiBAc2VsZWN0aW9uUmFuZ2VzLmxlbmd0aCA+IDFcbiAgICAgIEBzZWxlY3Rpb25SYW5nZXMucG9wKClcbiAgICAgIEBlZGl0b3Iuc2V0U2VsZWN0ZWRCdWZmZXJSYW5nZXMgQHNlbGVjdGlvblJhbmdlc1xuICAgIGVsc2VcbiAgICAgIEBlZGl0b3IuY2xlYXJTZWxlY3Rpb25zKClcblxuICAgIEBlZGl0b3Iuc2Nyb2xsVG9DdXJzb3JQb3NpdGlvbigpXG5cbiAgc2tpcEN1cnJlbnRTZWxlY3Rpb246IC0+XG4gICAgQHVwZGF0ZVNhdmVkU2VsZWN0aW9ucygpXG5cbiAgICByZXR1cm4gaWYgQHNlbGVjdGlvblJhbmdlcy5sZW5ndGggPCAxXG5cbiAgICBpZiBAc2VsZWN0aW9uUmFuZ2VzLmxlbmd0aCA+IDFcbiAgICAgIGxhc3RTZWxlY3Rpb24gPSBAc2VsZWN0aW9uUmFuZ2VzLnBvcCgpXG4gICAgICBAZWRpdG9yLnNldFNlbGVjdGVkQnVmZmVyUmFuZ2VzIEBzZWxlY3Rpb25SYW5nZXNcbiAgICAgIEBzZWxlY3ROZXh0T2NjdXJyZW5jZShzdGFydDogbGFzdFNlbGVjdGlvbi5lbmQpXG4gICAgZWxzZVxuICAgICAgQHNlbGVjdE5leHRPY2N1cnJlbmNlKClcbiAgICAgIEBzZWxlY3Rpb25SYW5nZXMuc2hpZnQoKVxuICAgICAgcmV0dXJuIGlmIEBzZWxlY3Rpb25SYW5nZXMubGVuZ3RoIDwgMVxuICAgICAgQGVkaXRvci5zZXRTZWxlY3RlZEJ1ZmZlclJhbmdlcyBAc2VsZWN0aW9uUmFuZ2VzXG5cbiAgc2VsZWN0V29yZDogLT5cbiAgICBAZWRpdG9yLnNlbGVjdFdvcmRzQ29udGFpbmluZ0N1cnNvcnMoKVxuICAgIGxhc3RTZWxlY3Rpb24gPSBAZWRpdG9yLmdldExhc3RTZWxlY3Rpb24oKVxuICAgIGlmIEB3b3JkU2VsZWN0ZWQgPSBAaXNXb3JkU2VsZWN0ZWQobGFzdFNlbGVjdGlvbilcbiAgICAgIGRpc3Bvc2FibGVzID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcbiAgICAgIGNsZWFyV29yZFNlbGVjdGVkID0gPT5cbiAgICAgICAgQHdvcmRTZWxlY3RlZCA9IG51bGxcbiAgICAgICAgZGlzcG9zYWJsZXMuZGlzcG9zZSgpXG4gICAgICBkaXNwb3NhYmxlcy5hZGQgbGFzdFNlbGVjdGlvbi5vbkRpZENoYW5nZVJhbmdlIGNsZWFyV29yZFNlbGVjdGVkXG4gICAgICBkaXNwb3NhYmxlcy5hZGQgbGFzdFNlbGVjdGlvbi5vbkRpZERlc3Ryb3kgY2xlYXJXb3JkU2VsZWN0ZWRcblxuICBzZWxlY3RBbGxPY2N1cnJlbmNlczogLT5cbiAgICByYW5nZSA9IFtbMCwgMF0sIEBlZGl0b3IuZ2V0RW9mQnVmZmVyUG9zaXRpb24oKV1cbiAgICBAc2NhbkZvck5leHRPY2N1cnJlbmNlIHJhbmdlLCAoe3JhbmdlLCBzdG9wfSkgPT5cbiAgICAgIEBhZGRTZWxlY3Rpb24ocmFuZ2UpXG5cbiAgc2VsZWN0TmV4dE9jY3VycmVuY2U6IChvcHRpb25zPXt9KSAtPlxuICAgIHN0YXJ0aW5nUmFuZ2UgPSBvcHRpb25zLnN0YXJ0ID8gQGVkaXRvci5nZXRTZWxlY3RlZEJ1ZmZlclJhbmdlKCkuZW5kXG4gICAgcmFuZ2UgPSBAZmluZE5leHRPY2N1cnJlbmNlKFtzdGFydGluZ1JhbmdlLCBAZWRpdG9yLmdldEVvZkJ1ZmZlclBvc2l0aW9uKCldKVxuICAgIHJhbmdlID89IEBmaW5kTmV4dE9jY3VycmVuY2UoW1swLCAwXSwgQGVkaXRvci5nZXRTZWxlY3Rpb25zKClbMF0uZ2V0QnVmZmVyUmFuZ2UoKS5zdGFydF0pXG4gICAgQGFkZFNlbGVjdGlvbihyYW5nZSkgaWYgcmFuZ2U/XG5cbiAgZmluZE5leHRPY2N1cnJlbmNlOiAoc2NhblJhbmdlKSAtPlxuICAgIGZvdW5kUmFuZ2UgPSBudWxsXG4gICAgQHNjYW5Gb3JOZXh0T2NjdXJyZW5jZSBzY2FuUmFuZ2UsICh7cmFuZ2UsIHN0b3B9KSAtPlxuICAgICAgZm91bmRSYW5nZSA9IHJhbmdlXG4gICAgICBzdG9wKClcbiAgICBmb3VuZFJhbmdlXG5cbiAgYWRkU2VsZWN0aW9uOiAocmFuZ2UpIC0+XG4gICAgcmV2ZXJzZWQgPSBAZWRpdG9yLmdldExhc3RTZWxlY3Rpb24oKS5pc1JldmVyc2VkKClcbiAgICBzZWxlY3Rpb24gPSBAZWRpdG9yLmFkZFNlbGVjdGlvbkZvckJ1ZmZlclJhbmdlKHJhbmdlLCB7cmV2ZXJzZWR9KVxuICAgIEB1cGRhdGVTYXZlZFNlbGVjdGlvbnMgc2VsZWN0aW9uXG5cbiAgc2NhbkZvck5leHRPY2N1cnJlbmNlOiAocmFuZ2UsIGNhbGxiYWNrKSAtPlxuICAgIHNlbGVjdGlvbiA9IEBlZGl0b3IuZ2V0TGFzdFNlbGVjdGlvbigpXG4gICAgdGV4dCA9IF8uZXNjYXBlUmVnRXhwKHNlbGVjdGlvbi5nZXRUZXh0KCkpXG5cbiAgICBpZiBAd29yZFNlbGVjdGVkXG4gICAgICBub25Xb3JkQ2hhcmFjdGVycyA9IGF0b20uY29uZmlnLmdldCgnZWRpdG9yLm5vbldvcmRDaGFyYWN0ZXJzJylcbiAgICAgIHRleHQgPSBcIihefFsgXFx0I3tfLmVzY2FwZVJlZ0V4cChub25Xb3JkQ2hhcmFjdGVycyl9XSspI3t0ZXh0fSg/PSR8W1xcXFxzI3tfLmVzY2FwZVJlZ0V4cChub25Xb3JkQ2hhcmFjdGVycyl9XSspXCJcblxuICAgIEBlZGl0b3Iuc2NhbkluQnVmZmVyUmFuZ2UgbmV3IFJlZ0V4cCh0ZXh0LCAnZycpLCByYW5nZSwgKHJlc3VsdCkgLT5cbiAgICAgIGlmIHByZWZpeCA9IHJlc3VsdC5tYXRjaFsxXVxuICAgICAgICByZXN1bHQucmFuZ2UgPSByZXN1bHQucmFuZ2UudHJhbnNsYXRlKFswLCBwcmVmaXgubGVuZ3RoXSwgWzAsIDBdKVxuICAgICAgY2FsbGJhY2socmVzdWx0KVxuXG4gIHVwZGF0ZVNhdmVkU2VsZWN0aW9uczogKHNlbGVjdGlvbj1udWxsKSAtPlxuICAgIHNlbGVjdGlvbnMgPSBAZWRpdG9yLmdldFNlbGVjdGlvbnMoKVxuICAgIEBzZWxlY3Rpb25SYW5nZXMgPSBbXSBpZiBzZWxlY3Rpb25zLmxlbmd0aCA8IDNcbiAgICBpZiBAc2VsZWN0aW9uUmFuZ2VzLmxlbmd0aCBpcyAwXG4gICAgICBAc2VsZWN0aW9uUmFuZ2VzLnB1c2ggcy5nZXRCdWZmZXJSYW5nZSgpIGZvciBzIGluIHNlbGVjdGlvbnNcbiAgICBlbHNlIGlmIHNlbGVjdGlvblxuICAgICAgc2VsZWN0aW9uUmFuZ2UgPSBzZWxlY3Rpb24uZ2V0QnVmZmVyUmFuZ2UoKVxuICAgICAgcmV0dXJuIGlmIEBzZWxlY3Rpb25SYW5nZXMuc29tZSAoZXhpc3RpbmdSYW5nZSkgLT4gZXhpc3RpbmdSYW5nZS5pc0VxdWFsKHNlbGVjdGlvblJhbmdlKVxuICAgICAgQHNlbGVjdGlvblJhbmdlcy5wdXNoIHNlbGVjdGlvblJhbmdlXG5cbiAgaXNOb25Xb3JkQ2hhcmFjdGVyOiAoY2hhcmFjdGVyKSAtPlxuICAgIG5vbldvcmRDaGFyYWN0ZXJzID0gYXRvbS5jb25maWcuZ2V0KCdlZGl0b3Iubm9uV29yZENoYXJhY3RlcnMnKVxuICAgIG5ldyBSZWdFeHAoXCJbIFxcdCN7Xy5lc2NhcGVSZWdFeHAobm9uV29yZENoYXJhY3RlcnMpfV1cIikudGVzdChjaGFyYWN0ZXIpXG5cbiAgaXNOb25Xb3JkQ2hhcmFjdGVyVG9UaGVMZWZ0OiAoc2VsZWN0aW9uKSAtPlxuICAgIHNlbGVjdGlvblN0YXJ0ID0gc2VsZWN0aW9uLmdldEJ1ZmZlclJhbmdlKCkuc3RhcnRcbiAgICByYW5nZSA9IFJhbmdlLmZyb21Qb2ludFdpdGhEZWx0YShzZWxlY3Rpb25TdGFydCwgMCwgLTEpXG4gICAgQGlzTm9uV29yZENoYXJhY3RlcihAZWRpdG9yLmdldFRleHRJbkJ1ZmZlclJhbmdlKHJhbmdlKSlcblxuICBpc05vbldvcmRDaGFyYWN0ZXJUb1RoZVJpZ2h0OiAoc2VsZWN0aW9uKSAtPlxuICAgIHNlbGVjdGlvbkVuZCA9IHNlbGVjdGlvbi5nZXRCdWZmZXJSYW5nZSgpLmVuZFxuICAgIHJhbmdlID0gUmFuZ2UuZnJvbVBvaW50V2l0aERlbHRhKHNlbGVjdGlvbkVuZCwgMCwgMSlcbiAgICBAaXNOb25Xb3JkQ2hhcmFjdGVyKEBlZGl0b3IuZ2V0VGV4dEluQnVmZmVyUmFuZ2UocmFuZ2UpKVxuXG4gIGlzV29yZFNlbGVjdGVkOiAoc2VsZWN0aW9uKSAtPlxuICAgIGlmIHNlbGVjdGlvbi5nZXRCdWZmZXJSYW5nZSgpLmlzU2luZ2xlTGluZSgpXG4gICAgICBzZWxlY3Rpb25SYW5nZSA9IHNlbGVjdGlvbi5nZXRCdWZmZXJSYW5nZSgpXG4gICAgICBsaW5lUmFuZ2UgPSBAZWRpdG9yLmJ1ZmZlclJhbmdlRm9yQnVmZmVyUm93KHNlbGVjdGlvblJhbmdlLnN0YXJ0LnJvdylcbiAgICAgIG5vbldvcmRDaGFyYWN0ZXJUb1RoZUxlZnQgPSBfLmlzRXF1YWwoc2VsZWN0aW9uUmFuZ2Uuc3RhcnQsIGxpbmVSYW5nZS5zdGFydCkgb3JcbiAgICAgICAgQGlzTm9uV29yZENoYXJhY3RlclRvVGhlTGVmdChzZWxlY3Rpb24pXG4gICAgICBub25Xb3JkQ2hhcmFjdGVyVG9UaGVSaWdodCA9IF8uaXNFcXVhbChzZWxlY3Rpb25SYW5nZS5lbmQsIGxpbmVSYW5nZS5lbmQpIG9yXG4gICAgICAgIEBpc05vbldvcmRDaGFyYWN0ZXJUb1RoZVJpZ2h0KHNlbGVjdGlvbilcbiAgICAgIGNvbnRhaW5zT25seVdvcmRDaGFyYWN0ZXJzID0gbm90IEBpc05vbldvcmRDaGFyYWN0ZXIoc2VsZWN0aW9uLmdldFRleHQoKSlcblxuICAgICAgbm9uV29yZENoYXJhY3RlclRvVGhlTGVmdCBhbmQgbm9uV29yZENoYXJhY3RlclRvVGhlUmlnaHQgYW5kIGNvbnRhaW5zT25seVdvcmRDaGFyYWN0ZXJzXG4gICAgZWxzZVxuICAgICAgZmFsc2VcbiJdfQ==
