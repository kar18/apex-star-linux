(function() {
  var AddDialog, Dialog, fs, path, repoForPath,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  path = require('path');

  fs = require('fs-plus');

  Dialog = require('./dialog');

  repoForPath = require('./helpers').repoForPath;

  module.exports = AddDialog = (function(superClass) {
    extend(AddDialog, superClass);

    function AddDialog(initialPath, isCreatingFile) {
      var directoryPath, ref, relativeDirectoryPath;
      this.isCreatingFile = isCreatingFile;
      if (fs.isFileSync(initialPath)) {
        directoryPath = path.dirname(initialPath);
      } else {
        directoryPath = initialPath;
      }
      relativeDirectoryPath = directoryPath;
      ref = atom.project.relativizePath(directoryPath), this.rootProjectPath = ref[0], relativeDirectoryPath = ref[1];
      if (relativeDirectoryPath.length > 0) {
        relativeDirectoryPath += path.sep;
      }
      AddDialog.__super__.constructor.call(this, {
        prompt: "Enter the path for the new " + (isCreatingFile ? "file." : "folder."),
        initialPath: relativeDirectoryPath,
        select: false,
        iconClass: isCreatingFile ? 'icon-file-add' : 'icon-file-directory-create'
      });
    }

    AddDialog.prototype.onDidCreateFile = function(callback) {
      return this.emitter.on('did-create-file', callback);
    };

    AddDialog.prototype.onDidCreateDirectory = function(callback) {
      return this.emitter.on('did-create-directory', callback);
    };

    AddDialog.prototype.onConfirm = function(newPath) {
      var endsWithDirectorySeparator, error, ref;
      newPath = newPath.replace(/\s+$/, '');
      endsWithDirectorySeparator = newPath[newPath.length - 1] === path.sep;
      if (!path.isAbsolute(newPath)) {
        if (this.rootProjectPath == null) {
          this.showError("You must open a directory to create a file with a relative path");
          return;
        }
        newPath = path.join(this.rootProjectPath, newPath);
      }
      if (!newPath) {
        return;
      }
      try {
        if (fs.existsSync(newPath)) {
          return this.showError("'" + newPath + "' already exists.");
        } else if (this.isCreatingFile) {
          if (endsWithDirectorySeparator) {
            return this.showError("File names must not end with a '" + path.sep + "' character.");
          } else {
            fs.writeFileSync(newPath, '');
            if ((ref = repoForPath(newPath)) != null) {
              ref.getPathStatus(newPath);
            }
            this.emitter.emit('did-create-file', newPath);
            return this.close();
          }
        } else {
          fs.makeTreeSync(newPath);
          this.emitter.emit('did-create-directory', newPath);
          return this.cancel();
        }
      } catch (error1) {
        error = error1;
        return this.showError(error.message + ".");
      }
    };

    return AddDialog;

  })(Dialog);

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90cmVlLXZpZXcvbGliL2FkZC1kaWFsb2cuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQSx3Q0FBQTtJQUFBOzs7RUFBQSxJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBQ1AsRUFBQSxHQUFLLE9BQUEsQ0FBUSxTQUFSOztFQUNMLE1BQUEsR0FBUyxPQUFBLENBQVEsVUFBUjs7RUFDUixjQUFlLE9BQUEsQ0FBUSxXQUFSOztFQUVoQixNQUFNLENBQUMsT0FBUCxHQUNNOzs7SUFDUyxtQkFBQyxXQUFELEVBQWMsY0FBZDtBQUNYLFVBQUE7TUFBQSxJQUFDLENBQUEsY0FBRCxHQUFrQjtNQUVsQixJQUFHLEVBQUUsQ0FBQyxVQUFILENBQWMsV0FBZCxDQUFIO1FBQ0UsYUFBQSxHQUFnQixJQUFJLENBQUMsT0FBTCxDQUFhLFdBQWIsRUFEbEI7T0FBQSxNQUFBO1FBR0UsYUFBQSxHQUFnQixZQUhsQjs7TUFLQSxxQkFBQSxHQUF3QjtNQUN4QixNQUE0QyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWIsQ0FBNEIsYUFBNUIsQ0FBNUMsRUFBQyxJQUFDLENBQUEsd0JBQUYsRUFBbUI7TUFDbkIsSUFBcUMscUJBQXFCLENBQUMsTUFBdEIsR0FBK0IsQ0FBcEU7UUFBQSxxQkFBQSxJQUF5QixJQUFJLENBQUMsSUFBOUI7O01BRUEsMkNBQ0U7UUFBQSxNQUFBLEVBQVEsNkJBQUEsR0FBZ0MsQ0FBRyxjQUFILEdBQXVCLE9BQXZCLEdBQW9DLFNBQXBDLENBQXhDO1FBQ0EsV0FBQSxFQUFhLHFCQURiO1FBRUEsTUFBQSxFQUFRLEtBRlI7UUFHQSxTQUFBLEVBQWMsY0FBSCxHQUF1QixlQUF2QixHQUE0Qyw0QkFIdkQ7T0FERjtJQVpXOzt3QkFrQmIsZUFBQSxHQUFpQixTQUFDLFFBQUQ7YUFDZixJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxpQkFBWixFQUErQixRQUEvQjtJQURlOzt3QkFHakIsb0JBQUEsR0FBc0IsU0FBQyxRQUFEO2FBQ3BCLElBQUMsQ0FBQSxPQUFPLENBQUMsRUFBVCxDQUFZLHNCQUFaLEVBQW9DLFFBQXBDO0lBRG9COzt3QkFHdEIsU0FBQSxHQUFXLFNBQUMsT0FBRDtBQUNULFVBQUE7TUFBQSxPQUFBLEdBQVUsT0FBTyxDQUFDLE9BQVIsQ0FBZ0IsTUFBaEIsRUFBd0IsRUFBeEI7TUFDViwwQkFBQSxHQUE2QixPQUFRLENBQUEsT0FBTyxDQUFDLE1BQVIsR0FBaUIsQ0FBakIsQ0FBUixLQUErQixJQUFJLENBQUM7TUFDakUsSUFBQSxDQUFPLElBQUksQ0FBQyxVQUFMLENBQWdCLE9BQWhCLENBQVA7UUFDRSxJQUFPLDRCQUFQO1VBQ0UsSUFBQyxDQUFBLFNBQUQsQ0FBVyxpRUFBWDtBQUNBLGlCQUZGOztRQUlBLE9BQUEsR0FBVSxJQUFJLENBQUMsSUFBTCxDQUFVLElBQUMsQ0FBQSxlQUFYLEVBQTRCLE9BQTVCLEVBTFo7O01BT0EsSUFBQSxDQUFjLE9BQWQ7QUFBQSxlQUFBOztBQUVBO1FBQ0UsSUFBRyxFQUFFLENBQUMsVUFBSCxDQUFjLE9BQWQsQ0FBSDtpQkFDRSxJQUFDLENBQUEsU0FBRCxDQUFXLEdBQUEsR0FBSSxPQUFKLEdBQVksbUJBQXZCLEVBREY7U0FBQSxNQUVLLElBQUcsSUFBQyxDQUFBLGNBQUo7VUFDSCxJQUFHLDBCQUFIO21CQUNFLElBQUMsQ0FBQSxTQUFELENBQVcsa0NBQUEsR0FBbUMsSUFBSSxDQUFDLEdBQXhDLEdBQTRDLGNBQXZELEVBREY7V0FBQSxNQUFBO1lBR0UsRUFBRSxDQUFDLGFBQUgsQ0FBaUIsT0FBakIsRUFBMEIsRUFBMUI7O2lCQUNvQixDQUFFLGFBQXRCLENBQW9DLE9BQXBDOztZQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsSUFBVCxDQUFjLGlCQUFkLEVBQWlDLE9BQWpDO21CQUNBLElBQUMsQ0FBQSxLQUFELENBQUEsRUFORjtXQURHO1NBQUEsTUFBQTtVQVNILEVBQUUsQ0FBQyxZQUFILENBQWdCLE9BQWhCO1VBQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsc0JBQWQsRUFBc0MsT0FBdEM7aUJBQ0EsSUFBQyxDQUFBLE1BQUQsQ0FBQSxFQVhHO1NBSFA7T0FBQSxjQUFBO1FBZU07ZUFDSixJQUFDLENBQUEsU0FBRCxDQUFjLEtBQUssQ0FBQyxPQUFQLEdBQWUsR0FBNUIsRUFoQkY7O0lBWlM7Ozs7S0F6Qlc7QUFOeEIiLCJzb3VyY2VzQ29udGVudCI6WyJwYXRoID0gcmVxdWlyZSAncGF0aCdcbmZzID0gcmVxdWlyZSAnZnMtcGx1cydcbkRpYWxvZyA9IHJlcXVpcmUgJy4vZGlhbG9nJ1xue3JlcG9Gb3JQYXRofSA9IHJlcXVpcmUgJy4vaGVscGVycydcblxubW9kdWxlLmV4cG9ydHMgPVxuY2xhc3MgQWRkRGlhbG9nIGV4dGVuZHMgRGlhbG9nXG4gIGNvbnN0cnVjdG9yOiAoaW5pdGlhbFBhdGgsIGlzQ3JlYXRpbmdGaWxlKSAtPlxuICAgIEBpc0NyZWF0aW5nRmlsZSA9IGlzQ3JlYXRpbmdGaWxlXG5cbiAgICBpZiBmcy5pc0ZpbGVTeW5jKGluaXRpYWxQYXRoKVxuICAgICAgZGlyZWN0b3J5UGF0aCA9IHBhdGguZGlybmFtZShpbml0aWFsUGF0aClcbiAgICBlbHNlXG4gICAgICBkaXJlY3RvcnlQYXRoID0gaW5pdGlhbFBhdGhcblxuICAgIHJlbGF0aXZlRGlyZWN0b3J5UGF0aCA9IGRpcmVjdG9yeVBhdGhcbiAgICBbQHJvb3RQcm9qZWN0UGF0aCwgcmVsYXRpdmVEaXJlY3RvcnlQYXRoXSA9IGF0b20ucHJvamVjdC5yZWxhdGl2aXplUGF0aChkaXJlY3RvcnlQYXRoKVxuICAgIHJlbGF0aXZlRGlyZWN0b3J5UGF0aCArPSBwYXRoLnNlcCBpZiByZWxhdGl2ZURpcmVjdG9yeVBhdGgubGVuZ3RoID4gMFxuXG4gICAgc3VwZXJcbiAgICAgIHByb21wdDogXCJFbnRlciB0aGUgcGF0aCBmb3IgdGhlIG5ldyBcIiArIGlmIGlzQ3JlYXRpbmdGaWxlIHRoZW4gXCJmaWxlLlwiIGVsc2UgXCJmb2xkZXIuXCJcbiAgICAgIGluaXRpYWxQYXRoOiByZWxhdGl2ZURpcmVjdG9yeVBhdGhcbiAgICAgIHNlbGVjdDogZmFsc2VcbiAgICAgIGljb25DbGFzczogaWYgaXNDcmVhdGluZ0ZpbGUgdGhlbiAnaWNvbi1maWxlLWFkZCcgZWxzZSAnaWNvbi1maWxlLWRpcmVjdG9yeS1jcmVhdGUnXG5cbiAgb25EaWRDcmVhdGVGaWxlOiAoY2FsbGJhY2spIC0+XG4gICAgQGVtaXR0ZXIub24oJ2RpZC1jcmVhdGUtZmlsZScsIGNhbGxiYWNrKVxuXG4gIG9uRGlkQ3JlYXRlRGlyZWN0b3J5OiAoY2FsbGJhY2spIC0+XG4gICAgQGVtaXR0ZXIub24oJ2RpZC1jcmVhdGUtZGlyZWN0b3J5JywgY2FsbGJhY2spXG5cbiAgb25Db25maXJtOiAobmV3UGF0aCkgLT5cbiAgICBuZXdQYXRoID0gbmV3UGF0aC5yZXBsYWNlKC9cXHMrJC8sICcnKSAjIFJlbW92ZSB0cmFpbGluZyB3aGl0ZXNwYWNlXG4gICAgZW5kc1dpdGhEaXJlY3RvcnlTZXBhcmF0b3IgPSBuZXdQYXRoW25ld1BhdGgubGVuZ3RoIC0gMV0gaXMgcGF0aC5zZXBcbiAgICB1bmxlc3MgcGF0aC5pc0Fic29sdXRlKG5ld1BhdGgpXG4gICAgICB1bmxlc3MgQHJvb3RQcm9qZWN0UGF0aD9cbiAgICAgICAgQHNob3dFcnJvcihcIllvdSBtdXN0IG9wZW4gYSBkaXJlY3RvcnkgdG8gY3JlYXRlIGEgZmlsZSB3aXRoIGEgcmVsYXRpdmUgcGF0aFwiKVxuICAgICAgICByZXR1cm5cblxuICAgICAgbmV3UGF0aCA9IHBhdGguam9pbihAcm9vdFByb2plY3RQYXRoLCBuZXdQYXRoKVxuXG4gICAgcmV0dXJuIHVubGVzcyBuZXdQYXRoXG5cbiAgICB0cnlcbiAgICAgIGlmIGZzLmV4aXN0c1N5bmMobmV3UGF0aClcbiAgICAgICAgQHNob3dFcnJvcihcIicje25ld1BhdGh9JyBhbHJlYWR5IGV4aXN0cy5cIilcbiAgICAgIGVsc2UgaWYgQGlzQ3JlYXRpbmdGaWxlXG4gICAgICAgIGlmIGVuZHNXaXRoRGlyZWN0b3J5U2VwYXJhdG9yXG4gICAgICAgICAgQHNob3dFcnJvcihcIkZpbGUgbmFtZXMgbXVzdCBub3QgZW5kIHdpdGggYSAnI3twYXRoLnNlcH0nIGNoYXJhY3Rlci5cIilcbiAgICAgICAgZWxzZVxuICAgICAgICAgIGZzLndyaXRlRmlsZVN5bmMobmV3UGF0aCwgJycpXG4gICAgICAgICAgcmVwb0ZvclBhdGgobmV3UGF0aCk/LmdldFBhdGhTdGF0dXMobmV3UGF0aClcbiAgICAgICAgICBAZW1pdHRlci5lbWl0KCdkaWQtY3JlYXRlLWZpbGUnLCBuZXdQYXRoKVxuICAgICAgICAgIEBjbG9zZSgpXG4gICAgICBlbHNlXG4gICAgICAgIGZzLm1ha2VUcmVlU3luYyhuZXdQYXRoKVxuICAgICAgICBAZW1pdHRlci5lbWl0KCdkaWQtY3JlYXRlLWRpcmVjdG9yeScsIG5ld1BhdGgpXG4gICAgICAgIEBjYW5jZWwoKVxuICAgIGNhdGNoIGVycm9yXG4gICAgICBAc2hvd0Vycm9yKFwiI3tlcnJvci5tZXNzYWdlfS5cIilcbiJdfQ==
