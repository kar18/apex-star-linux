(function() {
  var CLASSES, clone, firstCharsEqual, fs, path, propertyPrefixPattern;

  fs = require('fs');

  path = require('path');

  CLASSES = require('../completions.json');

  propertyPrefixPattern = /(?:^|\[|\(|,|=|:|\s)\s*(atom\.(?:[a-zA-Z]+\.?){0,2})$/;

  module.exports = {
    selector: '.source.coffee, .source.js',
    filterSuggestions: true,
    getSuggestions: function(arg) {
      var bufferPosition, editor, line;
      bufferPosition = arg.bufferPosition, editor = arg.editor;
      if (!this.isEditingAnAtomPackageFile(editor)) {
        return;
      }
      line = editor.getTextInRange([[bufferPosition.row, 0], bufferPosition]);
      return this.getCompletions(line);
    },
    load: function() {
      this.loadCompletions();
      atom.project.onDidChangePaths((function(_this) {
        return function() {
          return _this.scanProjectDirectories();
        };
      })(this));
      return this.scanProjectDirectories();
    },
    scanProjectDirectories: function() {
      this.packageDirectories = [];
      return atom.project.getDirectories().forEach((function(_this) {
        return function(directory) {
          if (directory == null) {
            return;
          }
          return _this.readMetadata(directory, function(error, metadata) {
            if (_this.isAtomPackage(metadata) || _this.isAtomCore(metadata)) {
              return _this.packageDirectories.push(directory);
            }
          });
        };
      })(this));
    },
    readMetadata: function(directory, callback) {
      return fs.readFile(path.join(directory.getPath(), 'package.json'), function(error, contents) {
        var metadata, parseError;
        if (error == null) {
          try {
            metadata = JSON.parse(contents);
          } catch (error1) {
            parseError = error1;
            error = parseError;
          }
        }
        return callback(error, metadata);
      });
    },
    isAtomPackage: function(metadata) {
      var ref, ref1;
      return (metadata != null ? (ref = metadata.engines) != null ? (ref1 = ref.atom) != null ? ref1.length : void 0 : void 0 : void 0) > 0;
    },
    isAtomCore: function(metadata) {
      return (metadata != null ? metadata.name : void 0) === 'atom';
    },
    isEditingAnAtomPackageFile: function(editor) {
      var directory, editorPath, i, len, parsedPath, ref, ref1;
      editorPath = editor.getPath();
      if (editorPath != null) {
        parsedPath = path.parse(editorPath);
        if (path.basename(parsedPath.dir) === '.atom') {
          if (parsedPath.base === 'init.coffee' || parsedPath.base === 'init.js') {
            return true;
          }
        }
      }
      ref1 = (ref = this.packageDirectories) != null ? ref : [];
      for (i = 0, len = ref1.length; i < len; i++) {
        directory = ref1[i];
        if (directory.contains(editorPath)) {
          return true;
        }
      }
      return false;
    },
    loadCompletions: function() {
      if (this.completions == null) {
        this.completions = {};
      }
      return this.loadProperty('atom', 'AtomEnvironment', CLASSES);
    },
    getCompletions: function(line) {
      var completion, completions, i, len, match, prefix, property, propertyCompletions, ref, ref1, ref2, ref3, segments;
      completions = [];
      match = (ref = propertyPrefixPattern.exec(line)) != null ? ref[1] : void 0;
      if (!match) {
        return completions;
      }
      segments = match.split('.');
      prefix = (ref1 = segments.pop()) != null ? ref1 : '';
      segments = segments.filter(function(segment) {
        return segment;
      });
      property = segments[segments.length - 1];
      propertyCompletions = (ref2 = (ref3 = this.completions[property]) != null ? ref3.completions : void 0) != null ? ref2 : [];
      for (i = 0, len = propertyCompletions.length; i < len; i++) {
        completion = propertyCompletions[i];
        if (!prefix || firstCharsEqual(completion.name, prefix)) {
          completions.push(clone(completion));
        }
      }
      return completions;
    },
    getPropertyClass: function(name) {
      var ref, ref1;
      return (ref = atom[name]) != null ? (ref1 = ref.constructor) != null ? ref1.name : void 0 : void 0;
    },
    loadProperty: function(propertyName, className, classes, parent) {
      var classCompletions, completion, i, len, propertyClass;
      classCompletions = classes[className];
      if (classCompletions == null) {
        return;
      }
      this.completions[propertyName] = {
        completions: []
      };
      for (i = 0, len = classCompletions.length; i < len; i++) {
        completion = classCompletions[i];
        this.completions[propertyName].completions.push(completion);
        if (completion.type === 'property') {
          propertyClass = this.getPropertyClass(completion.name);
          this.loadProperty(completion.name, propertyClass, classes);
        }
      }
    }
  };

  clone = function(obj) {
    var k, newObj, v;
    newObj = {};
    for (k in obj) {
      v = obj[k];
      newObj[k] = v;
    }
    return newObj;
  };

  firstCharsEqual = function(str1, str2) {
    return str1[0].toLowerCase() === str2[0].toLowerCase();
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9hdXRvY29tcGxldGUtYXRvbS1hcGkvbGliL3Byb3ZpZGVyLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsRUFBQSxHQUFLLE9BQUEsQ0FBUSxJQUFSOztFQUNMLElBQUEsR0FBTyxPQUFBLENBQVEsTUFBUjs7RUFFUCxPQUFBLEdBQVUsT0FBQSxDQUFRLHFCQUFSOztFQUVWLHFCQUFBLEdBQXdCOztFQUV4QixNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsUUFBQSxFQUFVLDRCQUFWO0lBQ0EsaUJBQUEsRUFBbUIsSUFEbkI7SUFHQSxjQUFBLEVBQWdCLFNBQUMsR0FBRDtBQUNkLFVBQUE7TUFEZ0IscUNBQWdCO01BQ2hDLElBQUEsQ0FBYyxJQUFDLENBQUEsMEJBQUQsQ0FBNEIsTUFBNUIsQ0FBZDtBQUFBLGVBQUE7O01BQ0EsSUFBQSxHQUFPLE1BQU0sQ0FBQyxjQUFQLENBQXNCLENBQUMsQ0FBQyxjQUFjLENBQUMsR0FBaEIsRUFBcUIsQ0FBckIsQ0FBRCxFQUEwQixjQUExQixDQUF0QjthQUNQLElBQUMsQ0FBQSxjQUFELENBQWdCLElBQWhCO0lBSGMsQ0FIaEI7SUFRQSxJQUFBLEVBQU0sU0FBQTtNQUNKLElBQUMsQ0FBQSxlQUFELENBQUE7TUFDQSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFiLENBQThCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsc0JBQUQsQ0FBQTtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE5QjthQUNBLElBQUMsQ0FBQSxzQkFBRCxDQUFBO0lBSEksQ0FSTjtJQWFBLHNCQUFBLEVBQXdCLFNBQUE7TUFDdEIsSUFBQyxDQUFBLGtCQUFELEdBQXNCO2FBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYixDQUFBLENBQTZCLENBQUMsT0FBOUIsQ0FBc0MsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLFNBQUQ7VUFDcEMsSUFBYyxpQkFBZDtBQUFBLG1CQUFBOztpQkFDQSxLQUFDLENBQUEsWUFBRCxDQUFjLFNBQWQsRUFBeUIsU0FBQyxLQUFELEVBQVEsUUFBUjtZQUN2QixJQUFHLEtBQUMsQ0FBQSxhQUFELENBQWUsUUFBZixDQUFBLElBQTRCLEtBQUMsQ0FBQSxVQUFELENBQVksUUFBWixDQUEvQjtxQkFDRSxLQUFDLENBQUEsa0JBQWtCLENBQUMsSUFBcEIsQ0FBeUIsU0FBekIsRUFERjs7VUFEdUIsQ0FBekI7UUFGb0M7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXRDO0lBRnNCLENBYnhCO0lBcUJBLFlBQUEsRUFBYyxTQUFDLFNBQUQsRUFBWSxRQUFaO2FBQ1osRUFBRSxDQUFDLFFBQUgsQ0FBWSxJQUFJLENBQUMsSUFBTCxDQUFVLFNBQVMsQ0FBQyxPQUFWLENBQUEsQ0FBVixFQUErQixjQUEvQixDQUFaLEVBQTRELFNBQUMsS0FBRCxFQUFRLFFBQVI7QUFDMUQsWUFBQTtRQUFBLElBQU8sYUFBUDtBQUNFO1lBQ0UsUUFBQSxHQUFXLElBQUksQ0FBQyxLQUFMLENBQVcsUUFBWCxFQURiO1dBQUEsY0FBQTtZQUVNO1lBQ0osS0FBQSxHQUFRLFdBSFY7V0FERjs7ZUFLQSxRQUFBLENBQVMsS0FBVCxFQUFnQixRQUFoQjtNQU4wRCxDQUE1RDtJQURZLENBckJkO0lBOEJBLGFBQUEsRUFBZSxTQUFDLFFBQUQ7QUFDYixVQUFBO29HQUF1QixDQUFFLGtDQUF6QixHQUFrQztJQURyQixDQTlCZjtJQWlDQSxVQUFBLEVBQVksU0FBQyxRQUFEO2lDQUNWLFFBQVEsQ0FBRSxjQUFWLEtBQWtCO0lBRFIsQ0FqQ1o7SUFvQ0EsMEJBQUEsRUFBNEIsU0FBQyxNQUFEO0FBQzFCLFVBQUE7TUFBQSxVQUFBLEdBQWEsTUFBTSxDQUFDLE9BQVAsQ0FBQTtNQUNiLElBQUcsa0JBQUg7UUFDRSxVQUFBLEdBQWEsSUFBSSxDQUFDLEtBQUwsQ0FBVyxVQUFYO1FBQ2IsSUFBRyxJQUFJLENBQUMsUUFBTCxDQUFjLFVBQVUsQ0FBQyxHQUF6QixDQUFBLEtBQWlDLE9BQXBDO1VBQ0UsSUFBRyxVQUFVLENBQUMsSUFBWCxLQUFtQixhQUFuQixJQUFvQyxVQUFVLENBQUMsSUFBWCxLQUFtQixTQUExRDtBQUNFLG1CQUFPLEtBRFQ7V0FERjtTQUZGOztBQUtBO0FBQUEsV0FBQSxzQ0FBQTs7UUFDRSxJQUFlLFNBQVMsQ0FBQyxRQUFWLENBQW1CLFVBQW5CLENBQWY7QUFBQSxpQkFBTyxLQUFQOztBQURGO2FBRUE7SUFUMEIsQ0FwQzVCO0lBK0NBLGVBQUEsRUFBaUIsU0FBQTs7UUFDZixJQUFDLENBQUEsY0FBZTs7YUFDaEIsSUFBQyxDQUFBLFlBQUQsQ0FBYyxNQUFkLEVBQXNCLGlCQUF0QixFQUF5QyxPQUF6QztJQUZlLENBL0NqQjtJQW1EQSxjQUFBLEVBQWdCLFNBQUMsSUFBRDtBQUNkLFVBQUE7TUFBQSxXQUFBLEdBQWM7TUFDZCxLQUFBLHlEQUEyQyxDQUFBLENBQUE7TUFDM0MsSUFBQSxDQUEwQixLQUExQjtBQUFBLGVBQU8sWUFBUDs7TUFFQSxRQUFBLEdBQVcsS0FBSyxDQUFDLEtBQU4sQ0FBWSxHQUFaO01BQ1gsTUFBQSw0Q0FBMEI7TUFDMUIsUUFBQSxHQUFXLFFBQVEsQ0FBQyxNQUFULENBQWdCLFNBQUMsT0FBRDtlQUFhO01BQWIsQ0FBaEI7TUFDWCxRQUFBLEdBQVcsUUFBUyxDQUFBLFFBQVEsQ0FBQyxNQUFULEdBQWtCLENBQWxCO01BQ3BCLG1CQUFBLHFHQUE0RDtBQUM1RCxXQUFBLHFEQUFBOztZQUEyQyxDQUFJLE1BQUosSUFBYyxlQUFBLENBQWdCLFVBQVUsQ0FBQyxJQUEzQixFQUFpQyxNQUFqQztVQUN2RCxXQUFXLENBQUMsSUFBWixDQUFpQixLQUFBLENBQU0sVUFBTixDQUFqQjs7QUFERjthQUVBO0lBWmMsQ0FuRGhCO0lBaUVBLGdCQUFBLEVBQWtCLFNBQUMsSUFBRDtBQUNoQixVQUFBO2lGQUF1QixDQUFFO0lBRFQsQ0FqRWxCO0lBb0VBLFlBQUEsRUFBYyxTQUFDLFlBQUQsRUFBZSxTQUFmLEVBQTBCLE9BQTFCLEVBQW1DLE1BQW5DO0FBQ1osVUFBQTtNQUFBLGdCQUFBLEdBQW1CLE9BQVEsQ0FBQSxTQUFBO01BQzNCLElBQWMsd0JBQWQ7QUFBQSxlQUFBOztNQUVBLElBQUMsQ0FBQSxXQUFZLENBQUEsWUFBQSxDQUFiLEdBQTZCO1FBQUEsV0FBQSxFQUFhLEVBQWI7O0FBRTdCLFdBQUEsa0RBQUE7O1FBQ0UsSUFBQyxDQUFBLFdBQVksQ0FBQSxZQUFBLENBQWEsQ0FBQyxXQUFXLENBQUMsSUFBdkMsQ0FBNEMsVUFBNUM7UUFDQSxJQUFHLFVBQVUsQ0FBQyxJQUFYLEtBQW1CLFVBQXRCO1VBQ0UsYUFBQSxHQUFnQixJQUFDLENBQUEsZ0JBQUQsQ0FBa0IsVUFBVSxDQUFDLElBQTdCO1VBQ2hCLElBQUMsQ0FBQSxZQUFELENBQWMsVUFBVSxDQUFDLElBQXpCLEVBQStCLGFBQS9CLEVBQThDLE9BQTlDLEVBRkY7O0FBRkY7SUFOWSxDQXBFZDs7O0VBaUZGLEtBQUEsR0FBUSxTQUFDLEdBQUQ7QUFDTixRQUFBO0lBQUEsTUFBQSxHQUFTO0FBQ1QsU0FBQSxRQUFBOztNQUFBLE1BQU8sQ0FBQSxDQUFBLENBQVAsR0FBWTtBQUFaO1dBQ0E7RUFITTs7RUFLUixlQUFBLEdBQWtCLFNBQUMsSUFBRCxFQUFPLElBQVA7V0FDaEIsSUFBSyxDQUFBLENBQUEsQ0FBRSxDQUFDLFdBQVIsQ0FBQSxDQUFBLEtBQXlCLElBQUssQ0FBQSxDQUFBLENBQUUsQ0FBQyxXQUFSLENBQUE7RUFEVDtBQTlGbEIiLCJzb3VyY2VzQ29udGVudCI6WyJmcyA9IHJlcXVpcmUgJ2ZzJ1xucGF0aCA9IHJlcXVpcmUgJ3BhdGgnXG5cbkNMQVNTRVMgPSByZXF1aXJlKCcuLi9jb21wbGV0aW9ucy5qc29uJylcblxucHJvcGVydHlQcmVmaXhQYXR0ZXJuID0gLyg/Ol58XFxbfFxcKHwsfD18OnxcXHMpXFxzKihhdG9tXFwuKD86W2EtekEtWl0rXFwuPyl7MCwyfSkkL1xuXG5tb2R1bGUuZXhwb3J0cyA9XG4gIHNlbGVjdG9yOiAnLnNvdXJjZS5jb2ZmZWUsIC5zb3VyY2UuanMnXG4gIGZpbHRlclN1Z2dlc3Rpb25zOiB0cnVlXG5cbiAgZ2V0U3VnZ2VzdGlvbnM6ICh7YnVmZmVyUG9zaXRpb24sIGVkaXRvcn0pIC0+XG4gICAgcmV0dXJuIHVubGVzcyBAaXNFZGl0aW5nQW5BdG9tUGFja2FnZUZpbGUoZWRpdG9yKVxuICAgIGxpbmUgPSBlZGl0b3IuZ2V0VGV4dEluUmFuZ2UoW1tidWZmZXJQb3NpdGlvbi5yb3csIDBdLCBidWZmZXJQb3NpdGlvbl0pXG4gICAgQGdldENvbXBsZXRpb25zKGxpbmUpXG5cbiAgbG9hZDogLT5cbiAgICBAbG9hZENvbXBsZXRpb25zKClcbiAgICBhdG9tLnByb2plY3Qub25EaWRDaGFuZ2VQYXRocyA9PiBAc2NhblByb2plY3REaXJlY3RvcmllcygpXG4gICAgQHNjYW5Qcm9qZWN0RGlyZWN0b3JpZXMoKVxuXG4gIHNjYW5Qcm9qZWN0RGlyZWN0b3JpZXM6IC0+XG4gICAgQHBhY2thZ2VEaXJlY3RvcmllcyA9IFtdXG4gICAgYXRvbS5wcm9qZWN0LmdldERpcmVjdG9yaWVzKCkuZm9yRWFjaCAoZGlyZWN0b3J5KSA9PlxuICAgICAgcmV0dXJuIHVubGVzcyBkaXJlY3Rvcnk/XG4gICAgICBAcmVhZE1ldGFkYXRhIGRpcmVjdG9yeSwgKGVycm9yLCBtZXRhZGF0YSkgPT5cbiAgICAgICAgaWYgQGlzQXRvbVBhY2thZ2UobWV0YWRhdGEpIG9yIEBpc0F0b21Db3JlKG1ldGFkYXRhKVxuICAgICAgICAgIEBwYWNrYWdlRGlyZWN0b3JpZXMucHVzaChkaXJlY3RvcnkpXG5cbiAgcmVhZE1ldGFkYXRhOiAoZGlyZWN0b3J5LCBjYWxsYmFjaykgLT5cbiAgICBmcy5yZWFkRmlsZSBwYXRoLmpvaW4oZGlyZWN0b3J5LmdldFBhdGgoKSwgJ3BhY2thZ2UuanNvbicpLCAoZXJyb3IsIGNvbnRlbnRzKSAtPlxuICAgICAgdW5sZXNzIGVycm9yP1xuICAgICAgICB0cnlcbiAgICAgICAgICBtZXRhZGF0YSA9IEpTT04ucGFyc2UoY29udGVudHMpXG4gICAgICAgIGNhdGNoIHBhcnNlRXJyb3JcbiAgICAgICAgICBlcnJvciA9IHBhcnNlRXJyb3JcbiAgICAgIGNhbGxiYWNrKGVycm9yLCBtZXRhZGF0YSlcblxuICBpc0F0b21QYWNrYWdlOiAobWV0YWRhdGEpIC0+XG4gICAgbWV0YWRhdGE/LmVuZ2luZXM/LmF0b20/Lmxlbmd0aCA+IDBcblxuICBpc0F0b21Db3JlOiAobWV0YWRhdGEpIC0+XG4gICAgbWV0YWRhdGE/Lm5hbWUgaXMgJ2F0b20nXG5cbiAgaXNFZGl0aW5nQW5BdG9tUGFja2FnZUZpbGU6IChlZGl0b3IpIC0+XG4gICAgZWRpdG9yUGF0aCA9IGVkaXRvci5nZXRQYXRoKClcbiAgICBpZiBlZGl0b3JQYXRoP1xuICAgICAgcGFyc2VkUGF0aCA9IHBhdGgucGFyc2UoZWRpdG9yUGF0aClcbiAgICAgIGlmIHBhdGguYmFzZW5hbWUocGFyc2VkUGF0aC5kaXIpIGlzICcuYXRvbSdcbiAgICAgICAgaWYgcGFyc2VkUGF0aC5iYXNlIGlzICdpbml0LmNvZmZlZScgb3IgcGFyc2VkUGF0aC5iYXNlIGlzICdpbml0LmpzJ1xuICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgZm9yIGRpcmVjdG9yeSBpbiBAcGFja2FnZURpcmVjdG9yaWVzID8gW11cbiAgICAgIHJldHVybiB0cnVlIGlmIGRpcmVjdG9yeS5jb250YWlucyhlZGl0b3JQYXRoKVxuICAgIGZhbHNlXG5cbiAgbG9hZENvbXBsZXRpb25zOiAtPlxuICAgIEBjb21wbGV0aW9ucyA/PSB7fVxuICAgIEBsb2FkUHJvcGVydHkoJ2F0b20nLCAnQXRvbUVudmlyb25tZW50JywgQ0xBU1NFUylcblxuICBnZXRDb21wbGV0aW9uczogKGxpbmUpIC0+XG4gICAgY29tcGxldGlvbnMgPSBbXVxuICAgIG1hdGNoID0gIHByb3BlcnR5UHJlZml4UGF0dGVybi5leGVjKGxpbmUpP1sxXVxuICAgIHJldHVybiBjb21wbGV0aW9ucyB1bmxlc3MgbWF0Y2hcblxuICAgIHNlZ21lbnRzID0gbWF0Y2guc3BsaXQoJy4nKVxuICAgIHByZWZpeCA9IHNlZ21lbnRzLnBvcCgpID8gJydcbiAgICBzZWdtZW50cyA9IHNlZ21lbnRzLmZpbHRlciAoc2VnbWVudCkgLT4gc2VnbWVudFxuICAgIHByb3BlcnR5ID0gc2VnbWVudHNbc2VnbWVudHMubGVuZ3RoIC0gMV1cbiAgICBwcm9wZXJ0eUNvbXBsZXRpb25zID0gQGNvbXBsZXRpb25zW3Byb3BlcnR5XT8uY29tcGxldGlvbnMgPyBbXVxuICAgIGZvciBjb21wbGV0aW9uIGluIHByb3BlcnR5Q29tcGxldGlvbnMgd2hlbiBub3QgcHJlZml4IG9yIGZpcnN0Q2hhcnNFcXVhbChjb21wbGV0aW9uLm5hbWUsIHByZWZpeClcbiAgICAgIGNvbXBsZXRpb25zLnB1c2goY2xvbmUoY29tcGxldGlvbikpXG4gICAgY29tcGxldGlvbnNcblxuICBnZXRQcm9wZXJ0eUNsYXNzOiAobmFtZSkgLT5cbiAgICBhdG9tW25hbWVdPy5jb25zdHJ1Y3Rvcj8ubmFtZVxuXG4gIGxvYWRQcm9wZXJ0eTogKHByb3BlcnR5TmFtZSwgY2xhc3NOYW1lLCBjbGFzc2VzLCBwYXJlbnQpIC0+XG4gICAgY2xhc3NDb21wbGV0aW9ucyA9IGNsYXNzZXNbY2xhc3NOYW1lXVxuICAgIHJldHVybiB1bmxlc3MgY2xhc3NDb21wbGV0aW9ucz9cblxuICAgIEBjb21wbGV0aW9uc1twcm9wZXJ0eU5hbWVdID0gY29tcGxldGlvbnM6IFtdXG5cbiAgICBmb3IgY29tcGxldGlvbiBpbiBjbGFzc0NvbXBsZXRpb25zXG4gICAgICBAY29tcGxldGlvbnNbcHJvcGVydHlOYW1lXS5jb21wbGV0aW9ucy5wdXNoKGNvbXBsZXRpb24pXG4gICAgICBpZiBjb21wbGV0aW9uLnR5cGUgaXMgJ3Byb3BlcnR5J1xuICAgICAgICBwcm9wZXJ0eUNsYXNzID0gQGdldFByb3BlcnR5Q2xhc3MoY29tcGxldGlvbi5uYW1lKVxuICAgICAgICBAbG9hZFByb3BlcnR5KGNvbXBsZXRpb24ubmFtZSwgcHJvcGVydHlDbGFzcywgY2xhc3NlcylcbiAgICByZXR1cm5cblxuY2xvbmUgPSAob2JqKSAtPlxuICBuZXdPYmogPSB7fVxuICBuZXdPYmpba10gPSB2IGZvciBrLCB2IG9mIG9ialxuICBuZXdPYmpcblxuZmlyc3RDaGFyc0VxdWFsID0gKHN0cjEsIHN0cjIpIC0+XG4gIHN0cjFbMF0udG9Mb3dlckNhc2UoKSBpcyBzdHIyWzBdLnRvTG93ZXJDYXNlKClcbiJdfQ==
