(function() {
  var AddDialog, AddProjectsView, BufferedProcess, CompositeDisposable, CopyDialog, Directory, DirectoryView, Emitter, IgnoredNames, MoveDialog, RootDragAndDrop, TREE_VIEW_URI, TreeView, _, fs, getFullExtension, getStyleObject, nextId, path, ref, ref1, repoForPath, shell, toggleConfig,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    slice = [].slice,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  path = require('path');

  shell = require('electron').shell;

  _ = require('underscore-plus');

  ref = require('atom'), BufferedProcess = ref.BufferedProcess, CompositeDisposable = ref.CompositeDisposable, Emitter = ref.Emitter;

  ref1 = require("./helpers"), repoForPath = ref1.repoForPath, getStyleObject = ref1.getStyleObject, getFullExtension = ref1.getFullExtension;

  fs = require('fs-plus');

  AddDialog = require('./add-dialog');

  MoveDialog = require('./move-dialog');

  CopyDialog = require('./copy-dialog');

  IgnoredNames = null;

  AddProjectsView = require('./add-projects-view');

  Directory = require('./directory');

  DirectoryView = require('./directory-view');

  RootDragAndDrop = require('./root-drag-and-drop');

  TREE_VIEW_URI = 'atom://tree-view';

  toggleConfig = function(keyPath) {
    return atom.config.set(keyPath, !atom.config.get(keyPath));
  };

  nextId = 1;

  module.exports = TreeView = (function() {
    function TreeView(state) {
      this.onDragLeave = bind(this.onDragLeave, this);
      this.onDragEnter = bind(this.onDragEnter, this);
      this.onStylesheetsChanged = bind(this.onStylesheetsChanged, this);
      this.moveConflictingEntry = bind(this.moveConflictingEntry, this);
      var j, len, observer, ref2, ref3, selectedPath;
      this.id = nextId++;
      this.element = document.createElement('div');
      this.element.classList.add('tool-panel', 'tree-view');
      this.element.tabIndex = -1;
      this.list = document.createElement('ol');
      this.list.classList.add('tree-view-root', 'full-menu', 'list-tree', 'has-collapsable-children', 'focusable-panel');
      this.disposables = new CompositeDisposable;
      this.emitter = new Emitter;
      this.roots = [];
      this.selectedPath = null;
      this.selectOnMouseUp = null;
      this.lastFocusedEntry = null;
      this.ignoredPatterns = [];
      this.useSyncFS = false;
      this.currentlyOpening = new Map;
      this.editorsToMove = [];
      this.editorsToDestroy = [];
      this.dragEventCounts = new WeakMap;
      this.rootDragAndDrop = new RootDragAndDrop(this);
      this.handleEvents();
      process.nextTick((function(_this) {
        return function() {
          var onStylesheetsChanged;
          _this.onStylesheetsChanged();
          onStylesheetsChanged = _.debounce(_this.onStylesheetsChanged, 100);
          _this.disposables.add(atom.styles.onDidAddStyleElement(onStylesheetsChanged));
          _this.disposables.add(atom.styles.onDidRemoveStyleElement(onStylesheetsChanged));
          return _this.disposables.add(atom.styles.onDidUpdateStyleElement(onStylesheetsChanged));
        };
      })(this));
      this.updateRoots(state.directoryExpansionStates);
      if (((ref2 = state.selectedPaths) != null ? ref2.length : void 0) > 0) {
        ref3 = state.selectedPaths;
        for (j = 0, len = ref3.length; j < len; j++) {
          selectedPath = ref3[j];
          this.selectMultipleEntries(this.entryForPath(selectedPath));
        }
      } else {
        this.selectEntry(this.roots[0]);
      }
      if ((state.scrollTop != null) || (state.scrollLeft != null)) {
        observer = new IntersectionObserver((function(_this) {
          return function() {
            if (_this.isVisible()) {
              _this.element.scrollTop = state.scrollTop;
              _this.element.scrollLeft = state.scrollLeft;
              return observer.disconnect();
            }
          };
        })(this));
        observer.observe(this.element);
      }
      if (state.width > 0) {
        this.element.style.width = state.width + "px";
      }
      this.disposables.add(this.onWillMoveEntry((function(_this) {
        return function(arg) {
          var editor, editors, filePath, initialPath, k, l, len1, len2, newPath, results, results1;
          initialPath = arg.initialPath, newPath = arg.newPath;
          editors = atom.workspace.getTextEditors();
          if (fs.isDirectorySync(initialPath)) {
            initialPath += path.sep;
            results = [];
            for (k = 0, len1 = editors.length; k < len1; k++) {
              editor = editors[k];
              filePath = editor.getPath();
              if (filePath != null ? filePath.startsWith(initialPath) : void 0) {
                results.push(_this.editorsToMove.push(filePath));
              } else {
                results.push(void 0);
              }
            }
            return results;
          } else {
            results1 = [];
            for (l = 0, len2 = editors.length; l < len2; l++) {
              editor = editors[l];
              filePath = editor.getPath();
              if (filePath === initialPath) {
                results1.push(_this.editorsToMove.push(filePath));
              } else {
                results1.push(void 0);
              }
            }
            return results1;
          }
        };
      })(this)));
      this.disposables.add(this.onEntryMoved((function(_this) {
        return function(arg) {
          var editor, filePath, index, initialPath, k, len1, newPath, ref4, results;
          initialPath = arg.initialPath, newPath = arg.newPath;
          ref4 = atom.workspace.getTextEditors();
          results = [];
          for (k = 0, len1 = ref4.length; k < len1; k++) {
            editor = ref4[k];
            filePath = editor.getPath();
            index = _this.editorsToMove.indexOf(filePath);
            if (index !== -1) {
              editor.getBuffer().setPath(filePath.replace(initialPath, newPath));
              results.push(_this.editorsToMove.splice(index, 1));
            } else {
              results.push(void 0);
            }
          }
          return results;
        };
      })(this)));
      this.disposables.add(this.onMoveEntryFailed((function(_this) {
        return function(arg) {
          var index, initialPath, newPath;
          initialPath = arg.initialPath, newPath = arg.newPath;
          index = _this.editorsToMove.indexOf(initialPath);
          if (index !== -1) {
            return _this.editorsToMove.splice(index, 1);
          }
        };
      })(this)));
      this.disposables.add(this.onWillDeleteEntry((function(_this) {
        return function(arg) {
          var editor, editors, filePath, k, l, len1, len2, pathToDelete, results, results1;
          pathToDelete = arg.pathToDelete;
          editors = atom.workspace.getTextEditors();
          if (fs.isDirectorySync(pathToDelete)) {
            pathToDelete += path.sep;
            results = [];
            for (k = 0, len1 = editors.length; k < len1; k++) {
              editor = editors[k];
              filePath = editor.getPath();
              if ((filePath != null ? filePath.startsWith(pathToDelete) : void 0) && !editor.isModified()) {
                results.push(_this.editorsToDestroy.push(filePath));
              } else {
                results.push(void 0);
              }
            }
            return results;
          } else {
            results1 = [];
            for (l = 0, len2 = editors.length; l < len2; l++) {
              editor = editors[l];
              filePath = editor.getPath();
              if (filePath === pathToDelete && !editor.isModified()) {
                results1.push(_this.editorsToDestroy.push(filePath));
              } else {
                results1.push(void 0);
              }
            }
            return results1;
          }
        };
      })(this)));
      this.disposables.add(this.onEntryDeleted((function(_this) {
        return function(arg) {
          var editor, index, k, len1, pathToDelete, ref4, results;
          pathToDelete = arg.pathToDelete;
          ref4 = atom.workspace.getTextEditors();
          results = [];
          for (k = 0, len1 = ref4.length; k < len1; k++) {
            editor = ref4[k];
            index = _this.editorsToDestroy.indexOf(editor.getPath());
            if (index !== -1) {
              editor.destroy();
              results.push(_this.editorsToDestroy.splice(index, 1));
            } else {
              results.push(void 0);
            }
          }
          return results;
        };
      })(this)));
      this.disposables.add(this.onDeleteEntryFailed((function(_this) {
        return function(arg) {
          var index, pathToDelete;
          pathToDelete = arg.pathToDelete;
          index = _this.editorsToDestroy.indexOf(pathToDelete);
          if (index !== -1) {
            return _this.editorsToDestroy.splice(index, 1);
          }
        };
      })(this)));
    }

    TreeView.prototype.serialize = function() {
      return {
        directoryExpansionStates: new (function(roots) {
          var j, len, root;
          for (j = 0, len = roots.length; j < len; j++) {
            root = roots[j];
            this[root.directory.path] = root.directory.serializeExpansionState();
          }
          return this;
        })(this.roots),
        deserializer: 'TreeView',
        selectedPaths: Array.from(this.getSelectedEntries(), function(entry) {
          return entry.getPath();
        }),
        scrollLeft: this.element.scrollLeft,
        scrollTop: this.element.scrollTop,
        width: parseInt(this.element.style.width || 0)
      };
    };

    TreeView.prototype.destroy = function() {
      var j, len, ref2, root;
      ref2 = this.roots;
      for (j = 0, len = ref2.length; j < len; j++) {
        root = ref2[j];
        root.directory.destroy();
      }
      this.disposables.dispose();
      this.rootDragAndDrop.dispose();
      return this.emitter.emit('did-destroy');
    };

    TreeView.prototype.onDidDestroy = function(callback) {
      return this.emitter.on('did-destroy', callback);
    };

    TreeView.prototype.getTitle = function() {
      return "Project";
    };

    TreeView.prototype.getURI = function() {
      return TREE_VIEW_URI;
    };

    TreeView.prototype.getPreferredLocation = function() {
      if (atom.config.get('tree-view.showOnRightSide')) {
        return 'right';
      } else {
        return 'left';
      }
    };

    TreeView.prototype.getAllowedLocations = function() {
      return ["left", "right"];
    };

    TreeView.prototype.isPermanentDockItem = function() {
      return true;
    };

    TreeView.prototype.getPreferredWidth = function() {
      var result;
      this.list.style.width = 'min-content';
      result = this.list.offsetWidth;
      this.list.style.width = '';
      return result;
    };

    TreeView.prototype.onDirectoryCreated = function(callback) {
      return this.emitter.on('directory-created', callback);
    };

    TreeView.prototype.onEntryCopied = function(callback) {
      return this.emitter.on('entry-copied', callback);
    };

    TreeView.prototype.onWillDeleteEntry = function(callback) {
      return this.emitter.on('will-delete-entry', callback);
    };

    TreeView.prototype.onEntryDeleted = function(callback) {
      return this.emitter.on('entry-deleted', callback);
    };

    TreeView.prototype.onDeleteEntryFailed = function(callback) {
      return this.emitter.on('delete-entry-failed', callback);
    };

    TreeView.prototype.onWillMoveEntry = function(callback) {
      return this.emitter.on('will-move-entry', callback);
    };

    TreeView.prototype.onEntryMoved = function(callback) {
      return this.emitter.on('entry-moved', callback);
    };

    TreeView.prototype.onMoveEntryFailed = function(callback) {
      return this.emitter.on('move-entry-failed', callback);
    };

    TreeView.prototype.onFileCreated = function(callback) {
      return this.emitter.on('file-created', callback);
    };

    TreeView.prototype.handleEvents = function() {
      this.element.addEventListener('click', (function(_this) {
        return function(e) {
          if (e.target.classList.contains('entries')) {
            return;
          }
          if (!(e.shiftKey || e.metaKey || e.ctrlKey)) {
            return _this.entryClicked(e);
          }
        };
      })(this));
      this.element.addEventListener('mousedown', (function(_this) {
        return function(e) {
          return _this.onMouseDown(e);
        };
      })(this));
      this.element.addEventListener('mouseup', (function(_this) {
        return function(e) {
          return _this.onMouseUp(e);
        };
      })(this));
      this.element.addEventListener('dragstart', (function(_this) {
        return function(e) {
          return _this.onDragStart(e);
        };
      })(this));
      this.element.addEventListener('dragenter', (function(_this) {
        return function(e) {
          return _this.onDragEnter(e);
        };
      })(this));
      this.element.addEventListener('dragleave', (function(_this) {
        return function(e) {
          return _this.onDragLeave(e);
        };
      })(this));
      this.element.addEventListener('dragover', (function(_this) {
        return function(e) {
          return _this.onDragOver(e);
        };
      })(this));
      this.element.addEventListener('drop', (function(_this) {
        return function(e) {
          return _this.onDrop(e);
        };
      })(this));
      atom.commands.add(this.element, {
        'core:move-up': (function(_this) {
          return function(e) {
            return _this.moveUp(e);
          };
        })(this),
        'core:move-down': (function(_this) {
          return function(e) {
            return _this.moveDown(e);
          };
        })(this),
        'core:page-up': (function(_this) {
          return function() {
            return _this.pageUp();
          };
        })(this),
        'core:page-down': (function(_this) {
          return function() {
            return _this.pageDown();
          };
        })(this),
        'core:move-to-top': (function(_this) {
          return function() {
            return _this.scrollToTop();
          };
        })(this),
        'core:move-to-bottom': (function(_this) {
          return function() {
            return _this.scrollToBottom();
          };
        })(this),
        'tree-view:expand-item': (function(_this) {
          return function() {
            return _this.openSelectedEntry({
              pending: true
            }, true);
          };
        })(this),
        'tree-view:recursive-expand-directory': (function(_this) {
          return function() {
            return _this.expandDirectory(true);
          };
        })(this),
        'tree-view:collapse-directory': (function(_this) {
          return function() {
            return _this.collapseDirectory();
          };
        })(this),
        'tree-view:recursive-collapse-directory': (function(_this) {
          return function() {
            return _this.collapseDirectory(true);
          };
        })(this),
        'tree-view:collapse-all': (function(_this) {
          return function() {
            return _this.collapseDirectory(true, true);
          };
        })(this),
        'tree-view:open-selected-entry': (function(_this) {
          return function() {
            return _this.openSelectedEntry();
          };
        })(this),
        'tree-view:open-selected-entry-right': (function(_this) {
          return function() {
            return _this.openSelectedEntryRight();
          };
        })(this),
        'tree-view:open-selected-entry-left': (function(_this) {
          return function() {
            return _this.openSelectedEntryLeft();
          };
        })(this),
        'tree-view:open-selected-entry-up': (function(_this) {
          return function() {
            return _this.openSelectedEntryUp();
          };
        })(this),
        'tree-view:open-selected-entry-down': (function(_this) {
          return function() {
            return _this.openSelectedEntryDown();
          };
        })(this),
        'tree-view:move': (function(_this) {
          return function() {
            return _this.moveSelectedEntry();
          };
        })(this),
        'tree-view:copy': (function(_this) {
          return function() {
            return _this.copySelectedEntries();
          };
        })(this),
        'tree-view:cut': (function(_this) {
          return function() {
            return _this.cutSelectedEntries();
          };
        })(this),
        'tree-view:paste': (function(_this) {
          return function() {
            return _this.pasteEntries();
          };
        })(this),
        'tree-view:copy-full-path': (function(_this) {
          return function() {
            return _this.copySelectedEntryPath(false);
          };
        })(this),
        'tree-view:show-in-file-manager': (function(_this) {
          return function() {
            return _this.showSelectedEntryInFileManager();
          };
        })(this),
        'tree-view:open-in-new-window': (function(_this) {
          return function() {
            return _this.openSelectedEntryInNewWindow();
          };
        })(this),
        'tree-view:copy-project-path': (function(_this) {
          return function() {
            return _this.copySelectedEntryPath(true);
          };
        })(this),
        'tree-view:unfocus': (function(_this) {
          return function() {
            return _this.unfocus();
          };
        })(this),
        'tree-view:toggle-vcs-ignored-files': function() {
          return toggleConfig('tree-view.hideVcsIgnoredFiles');
        },
        'tree-view:toggle-ignored-names': function() {
          return toggleConfig('tree-view.hideIgnoredNames');
        },
        'tree-view:remove-project-folder': (function(_this) {
          return function(e) {
            return _this.removeProjectFolder(e);
          };
        })(this)
      });
      [0, 1, 2, 3, 4, 5, 6, 7, 8].forEach((function(_this) {
        return function(index) {
          return atom.commands.add(_this.element, "tree-view:open-selected-entry-in-pane-" + (index + 1), function() {
            return _this.openSelectedEntryInPane(index);
          });
        };
      })(this));
      this.disposables.add(atom.workspace.getCenter().onDidChangeActivePaneItem((function(_this) {
        return function() {
          _this.selectActiveFile();
          if (atom.config.get('tree-view.autoReveal')) {
            return _this.revealActiveFile({
              show: false,
              focus: false
            });
          }
        };
      })(this)));
      this.disposables.add(atom.project.onDidChangePaths((function(_this) {
        return function() {
          return _this.updateRoots();
        };
      })(this)));
      this.disposables.add(atom.config.onDidChange('tree-view.hideVcsIgnoredFiles', (function(_this) {
        return function() {
          return _this.updateRoots();
        };
      })(this)));
      this.disposables.add(atom.config.onDidChange('tree-view.hideIgnoredNames', (function(_this) {
        return function() {
          return _this.updateRoots();
        };
      })(this)));
      this.disposables.add(atom.config.onDidChange('core.ignoredNames', (function(_this) {
        return function() {
          if (atom.config.get('tree-view.hideIgnoredNames')) {
            return _this.updateRoots();
          }
        };
      })(this)));
      this.disposables.add(atom.config.onDidChange('tree-view.sortFoldersBeforeFiles', (function(_this) {
        return function() {
          return _this.updateRoots();
        };
      })(this)));
      return this.disposables.add(atom.config.onDidChange('tree-view.squashDirectoryNames', (function(_this) {
        return function() {
          return _this.updateRoots();
        };
      })(this)));
    };

    TreeView.prototype.toggle = function() {
      return atom.workspace.toggle(this);
    };

    TreeView.prototype.show = function(focus) {
      return atom.workspace.open(this, {
        searchAllPanes: true,
        activatePane: false,
        activateItem: false
      }).then((function(_this) {
        return function() {
          atom.workspace.paneContainerForURI(_this.getURI()).show();
          if (focus) {
            return _this.focus();
          }
        };
      })(this));
    };

    TreeView.prototype.hide = function() {
      return atom.workspace.hide(this);
    };

    TreeView.prototype.focus = function() {
      return this.element.focus();
    };

    TreeView.prototype.unfocus = function() {
      return atom.workspace.getCenter().activate();
    };

    TreeView.prototype.hasFocus = function() {
      return document.activeElement === this.element;
    };

    TreeView.prototype.toggleFocus = function() {
      if (this.hasFocus()) {
        return this.unfocus();
      } else {
        return this.show(true);
      }
    };

    TreeView.prototype.entryClicked = function(e) {
      var entry, isRecursive;
      if (entry = e.target.closest('.entry')) {
        isRecursive = e.altKey || false;
        this.selectEntry(entry);
        if (entry.classList.contains('directory')) {
          return entry.toggleExpansion(isRecursive);
        } else if (entry.classList.contains('file')) {
          return this.fileViewEntryClicked(e);
        }
      }
    };

    TreeView.prototype.fileViewEntryClicked = function(e) {
      var alwaysOpenExisting, detail, filePath, openPromise, ref2;
      filePath = e.target.closest('.entry').getPath();
      detail = (ref2 = e.detail) != null ? ref2 : 1;
      alwaysOpenExisting = atom.config.get('tree-view.alwaysOpenExisting');
      if (detail === 1) {
        if (atom.config.get('core.allowPendingPaneItems')) {
          openPromise = atom.workspace.open(filePath, {
            pending: true,
            activatePane: false,
            searchAllPanes: alwaysOpenExisting
          });
          this.currentlyOpening.set(filePath, openPromise);
          return openPromise.then((function(_this) {
            return function() {
              return _this.currentlyOpening["delete"](filePath);
            };
          })(this));
        }
      } else if (detail === 2) {
        return this.openAfterPromise(filePath, {
          searchAllPanes: alwaysOpenExisting
        });
      }
    };

    TreeView.prototype.openAfterPromise = function(uri, options) {
      var promise;
      if (promise = this.currentlyOpening.get(uri)) {
        return promise.then(function() {
          return atom.workspace.open(uri, options);
        });
      } else {
        return atom.workspace.open(uri, options);
      }
    };

    TreeView.prototype.updateRoots = function(expansionStates) {
      var addProjectsViewElement, directory, j, k, key, len, len1, oldExpansionStates, projectPath, projectPaths, ref2, results, root, selectedPath, selectedPaths, stats;
      if (expansionStates == null) {
        expansionStates = {};
      }
      selectedPaths = this.selectedPaths();
      oldExpansionStates = {};
      ref2 = this.roots;
      for (j = 0, len = ref2.length; j < len; j++) {
        root = ref2[j];
        oldExpansionStates[root.directory.path] = root.directory.serializeExpansionState();
        root.directory.destroy();
        root.remove();
      }
      this.roots = [];
      projectPaths = atom.project.getPaths();
      if (projectPaths.length > 0) {
        if (!this.element.querySelector('tree-view-root')) {
          this.element.appendChild(this.list);
        }
        addProjectsViewElement = this.element.querySelector('#add-projects-view');
        if (addProjectsViewElement) {
          this.element.removeChild(addProjectsViewElement);
        }
        if (IgnoredNames == null) {
          IgnoredNames = require('./ignored-names');
        }
        this.roots = (function() {
          var k, l, len1, len2, ref3, ref4, ref5, results;
          results = [];
          for (k = 0, len1 = projectPaths.length; k < len1; k++) {
            projectPath = projectPaths[k];
            stats = fs.lstatSyncNoException(projectPath);
            if (!stats) {
              continue;
            }
            stats = _.pick.apply(_, [stats].concat(slice.call(_.keys(stats))));
            ref3 = ["atime", "birthtime", "ctime", "mtime"];
            for (l = 0, len2 = ref3.length; l < len2; l++) {
              key = ref3[l];
              stats[key] = stats[key].getTime();
            }
            directory = new Directory({
              name: path.basename(projectPath),
              fullPath: projectPath,
              symlink: false,
              isRoot: true,
              expansionState: (ref4 = (ref5 = expansionStates[projectPath]) != null ? ref5 : oldExpansionStates[projectPath]) != null ? ref4 : {
                isExpanded: true
              },
              ignoredNames: new IgnoredNames(),
              useSyncFS: this.useSyncFS,
              stats: stats
            });
            root = new DirectoryView(directory).element;
            this.list.appendChild(root);
            results.push(root);
          }
          return results;
        }).call(this);
        results = [];
        for (k = 0, len1 = selectedPaths.length; k < len1; k++) {
          selectedPath = selectedPaths[k];
          results.push(this.selectMultipleEntries(this.entryForPath(selectedPath)));
        }
        return results;
      } else {
        if (this.element.querySelector('.tree-view-root')) {
          this.element.removeChild(this.list);
        }
        if (!this.element.querySelector('#add-projects-view')) {
          return this.element.appendChild(new AddProjectsView().element);
        }
      }
    };

    TreeView.prototype.getActivePath = function() {
      var ref2;
      return (ref2 = atom.workspace.getCenter().getActivePaneItem()) != null ? typeof ref2.getPath === "function" ? ref2.getPath() : void 0 : void 0;
    };

    TreeView.prototype.selectActiveFile = function() {
      var activeFilePath;
      activeFilePath = this.getActivePath();
      if (this.entryForPath(activeFilePath)) {
        return this.selectEntryForPath(activeFilePath);
      } else {
        return this.deselect();
      }
    };

    TreeView.prototype.revealActiveFile = function(options) {
      var focus, promise, show;
      if (options == null) {
        options = {};
      }
      if (!atom.project.getPaths().length) {
        return Promise.resolve();
      }
      show = options.show, focus = options.focus;
      if (focus == null) {
        focus = atom.config.get('tree-view.focusOnReveal');
      }
      promise = show || focus ? this.show(focus) : Promise.resolve();
      return promise.then((function(_this) {
        return function() {
          var activeFilePath, activePathComponents, currentPath, entry, j, len, pathComponent, ref2, relativePath, results, rootPath;
          if (!(activeFilePath = _this.getActivePath())) {
            return;
          }
          ref2 = atom.project.relativizePath(activeFilePath), rootPath = ref2[0], relativePath = ref2[1];
          if (rootPath == null) {
            return;
          }
          activePathComponents = relativePath.split(path.sep);
          activePathComponents.unshift(rootPath.substr(rootPath.lastIndexOf(path.sep) + 1));
          currentPath = rootPath.substr(0, rootPath.lastIndexOf(path.sep));
          results = [];
          for (j = 0, len = activePathComponents.length; j < len; j++) {
            pathComponent = activePathComponents[j];
            currentPath += path.sep + pathComponent;
            entry = _this.entryForPath(currentPath);
            if (entry.classList.contains('directory')) {
              results.push(entry.expand());
            } else {
              _this.selectEntry(entry);
              results.push(_this.scrollToEntry(entry));
            }
          }
          return results;
        };
      })(this));
    };

    TreeView.prototype.copySelectedEntryPath = function(relativePath) {
      var pathToCopy;
      if (relativePath == null) {
        relativePath = false;
      }
      if (pathToCopy = this.selectedPath) {
        if (relativePath) {
          pathToCopy = atom.project.relativize(pathToCopy);
        }
        return atom.clipboard.write(pathToCopy);
      }
    };

    TreeView.prototype.entryForPath = function(entryPath) {
      var bestMatchEntry, bestMatchLength, entry, entryLength, j, len, ref2, ref3;
      bestMatchEntry = null;
      bestMatchLength = 0;
      ref2 = this.list.querySelectorAll('.entry');
      for (j = 0, len = ref2.length; j < len; j++) {
        entry = ref2[j];
        if (entry.isPathEqual(entryPath)) {
          return entry;
        }
        entryLength = entry.getPath().length;
        if (((ref3 = entry.directory) != null ? ref3.contains(entryPath) : void 0) && entryLength > bestMatchLength) {
          bestMatchEntry = entry;
          bestMatchLength = entryLength;
        }
      }
      return bestMatchEntry;
    };

    TreeView.prototype.selectEntryForPath = function(entryPath) {
      return this.selectEntry(this.entryForPath(entryPath));
    };

    TreeView.prototype.moveDown = function(event) {
      var nextEntry, selectedEntry;
      if (event != null) {
        event.stopImmediatePropagation();
      }
      selectedEntry = this.selectedEntry();
      if (selectedEntry != null) {
        if (selectedEntry.classList.contains('directory')) {
          if (this.selectEntry(selectedEntry.entries.children[0])) {
            this.scrollToEntry(this.selectedEntry(), false);
            return;
          }
        }
        if (nextEntry = this.nextEntry(selectedEntry)) {
          this.selectEntry(nextEntry);
        }
      } else {
        this.selectEntry(this.roots[0]);
      }
      return this.scrollToEntry(this.selectedEntry(), false);
    };

    TreeView.prototype.moveUp = function(event) {
      var entries, previousEntry, selectedEntry;
      event.stopImmediatePropagation();
      selectedEntry = this.selectedEntry();
      if (selectedEntry != null) {
        if (previousEntry = this.previousEntry(selectedEntry)) {
          this.selectEntry(previousEntry);
        } else {
          this.selectEntry(selectedEntry.parentElement.closest('.directory'));
        }
      } else {
        entries = this.list.querySelectorAll('.entry');
        this.selectEntry(entries[entries.length - 1]);
      }
      return this.scrollToEntry(this.selectedEntry(), false);
    };

    TreeView.prototype.nextEntry = function(entry) {
      var currentEntry;
      currentEntry = entry;
      while (currentEntry != null) {
        if (currentEntry.nextSibling != null) {
          currentEntry = currentEntry.nextSibling;
          if (currentEntry.matches('.entry')) {
            return currentEntry;
          }
        } else {
          currentEntry = currentEntry.parentElement.closest('.directory');
        }
      }
      return null;
    };

    TreeView.prototype.previousEntry = function(entry) {
      var entries, previousEntry;
      previousEntry = entry.previousSibling;
      while ((previousEntry != null) && !previousEntry.matches('.entry')) {
        previousEntry = previousEntry.previousSibling;
      }
      if (previousEntry == null) {
        return null;
      }
      if (previousEntry.matches('.directory.expanded')) {
        entries = previousEntry.querySelectorAll('.entry');
        if (entries.length > 0) {
          return entries[entries.length - 1];
        }
      }
      return previousEntry;
    };

    TreeView.prototype.expandDirectory = function(isRecursive) {
      var directory, selectedEntry;
      if (isRecursive == null) {
        isRecursive = false;
      }
      selectedEntry = this.selectedEntry();
      if (selectedEntry == null) {
        return;
      }
      directory = selectedEntry.closest('.directory');
      if (isRecursive === false && directory.isExpanded) {
        if (directory.directory.getEntries().length > 0) {
          return this.moveDown();
        }
      } else {
        return directory.expand(isRecursive);
      }
    };

    TreeView.prototype.collapseDirectory = function(isRecursive, allDirectories) {
      var directory, j, len, ref2, root, selectedEntry;
      if (isRecursive == null) {
        isRecursive = false;
      }
      if (allDirectories == null) {
        allDirectories = false;
      }
      if (allDirectories) {
        ref2 = this.roots;
        for (j = 0, len = ref2.length; j < len; j++) {
          root = ref2[j];
          root.collapse(true);
        }
        return;
      }
      selectedEntry = this.selectedEntry();
      if (selectedEntry == null) {
        return;
      }
      if (directory = selectedEntry.closest('.expanded.directory')) {
        directory.collapse(isRecursive);
        return this.selectEntry(directory);
      }
    };

    TreeView.prototype.openSelectedEntry = function(options, expandDirectory) {
      var selectedEntry;
      if (options == null) {
        options = {};
      }
      if (expandDirectory == null) {
        expandDirectory = false;
      }
      selectedEntry = this.selectedEntry();
      if (selectedEntry == null) {
        return;
      }
      if (selectedEntry.classList.contains('directory')) {
        if (expandDirectory) {
          return this.expandDirectory(false);
        } else {
          return selectedEntry.toggleExpansion();
        }
      } else if (selectedEntry.classList.contains('file')) {
        if (atom.config.get('tree-view.alwaysOpenExisting')) {
          options = Object.assign({
            searchAllPanes: true
          }, options);
        }
        return this.openAfterPromise(selectedEntry.getPath(), options);
      }
    };

    TreeView.prototype.openSelectedEntrySplit = function(orientation, side) {
      var pane, selectedEntry, split;
      selectedEntry = this.selectedEntry();
      if (selectedEntry == null) {
        return;
      }
      pane = atom.workspace.getCenter().getActivePane();
      if (pane && selectedEntry.classList.contains('file')) {
        if (atom.workspace.getCenter().getActivePaneItem()) {
          split = pane.split(orientation, side);
          return atom.workspace.openURIInPane(selectedEntry.getPath(), split);
        } else {
          return this.openSelectedEntry(true);
        }
      }
    };

    TreeView.prototype.openSelectedEntryRight = function() {
      return this.openSelectedEntrySplit('horizontal', 'after');
    };

    TreeView.prototype.openSelectedEntryLeft = function() {
      return this.openSelectedEntrySplit('horizontal', 'before');
    };

    TreeView.prototype.openSelectedEntryUp = function() {
      return this.openSelectedEntrySplit('vertical', 'before');
    };

    TreeView.prototype.openSelectedEntryDown = function() {
      return this.openSelectedEntrySplit('vertical', 'after');
    };

    TreeView.prototype.openSelectedEntryInPane = function(index) {
      var pane, selectedEntry;
      selectedEntry = this.selectedEntry();
      if (selectedEntry == null) {
        return;
      }
      pane = atom.workspace.getCenter().getPanes()[index];
      if (pane && selectedEntry.classList.contains('file')) {
        return atom.workspace.openURIInPane(selectedEntry.getPath(), pane);
      }
    };

    TreeView.prototype.moveSelectedEntry = function() {
      var dialog, entry, oldPath;
      if (this.hasFocus()) {
        entry = this.selectedEntry();
        if ((entry == null) || indexOf.call(this.roots, entry) >= 0) {
          return;
        }
        oldPath = entry.getPath();
      } else {
        oldPath = this.getActivePath();
      }
      if (oldPath) {
        dialog = new MoveDialog(oldPath, {
          willMove: (function(_this) {
            return function(arg) {
              var initialPath, newPath;
              initialPath = arg.initialPath, newPath = arg.newPath;
              return _this.emitter.emit('will-move-entry', {
                initialPath: initialPath,
                newPath: newPath
              });
            };
          })(this),
          onMove: (function(_this) {
            return function(arg) {
              var initialPath, newPath;
              initialPath = arg.initialPath, newPath = arg.newPath;
              return _this.emitter.emit('entry-moved', {
                initialPath: initialPath,
                newPath: newPath
              });
            };
          })(this),
          onMoveFailed: (function(_this) {
            return function(arg) {
              var initialPath, newPath;
              initialPath = arg.initialPath, newPath = arg.newPath;
              return _this.emitter.emit('move-entry-failed', {
                initialPath: initialPath,
                newPath: newPath
              });
            };
          })(this)
        });
        return dialog.attach();
      }
    };

    TreeView.prototype.showSelectedEntryInFileManager = function() {
      var filePath, ref2;
      if (!(filePath = (ref2 = this.selectedEntry()) != null ? ref2.getPath() : void 0)) {
        return;
      }
      if (!fs.existsSync(filePath)) {
        return atom.notifications.addWarning("Unable to show " + filePath + " in " + (this.getFileManagerName()));
      }
      return shell.showItemInFolder(filePath);
    };

    TreeView.prototype.showCurrentFileInFileManager = function() {
      var filePath, ref2;
      if (!(filePath = (ref2 = atom.workspace.getCenter().getActiveTextEditor()) != null ? ref2.getPath() : void 0)) {
        return;
      }
      if (!fs.existsSync(filePath)) {
        return atom.notifications.addWarning("Unable to show " + filePath + " in " + (this.getFileManagerName()));
      }
      return shell.showItemInFolder(filePath);
    };

    TreeView.prototype.getFileManagerName = function() {
      switch (process.platform) {
        case 'darwin':
          return 'Finder';
        case 'win32':
          return 'Explorer';
        default:
          return 'File Manager';
      }
    };

    TreeView.prototype.openSelectedEntryInNewWindow = function() {
      var pathToOpen, ref2;
      if (pathToOpen = (ref2 = this.selectedEntry()) != null ? ref2.getPath() : void 0) {
        return atom.open({
          pathsToOpen: [pathToOpen],
          newWindow: true
        });
      }
    };

    TreeView.prototype.copySelectedEntry = function() {
      var dialog, entry, oldPath;
      if (this.hasFocus()) {
        entry = this.selectedEntry();
        if (indexOf.call(this.roots, entry) >= 0) {
          return;
        }
        oldPath = entry != null ? entry.getPath() : void 0;
      } else {
        oldPath = this.getActivePath();
      }
      if (!oldPath) {
        return;
      }
      dialog = new CopyDialog(oldPath, {
        onCopy: (function(_this) {
          return function(arg) {
            var initialPath, newPath;
            initialPath = arg.initialPath, newPath = arg.newPath;
            return _this.emitter.emit('entry-copied', {
              initialPath: initialPath,
              newPath: newPath
            });
          };
        })(this)
      });
      return dialog.attach();
    };

    TreeView.prototype.removeSelectedEntries = function() {
      var activePath, j, len, ref2, ref3, root, selectedEntries, selectedPaths;
      if (this.hasFocus()) {
        selectedPaths = this.selectedPaths();
        selectedEntries = this.getSelectedEntries();
      } else if (activePath = this.getActivePath()) {
        selectedPaths = [activePath];
        selectedEntries = [this.entryForPath(activePath)];
      }
      if (!((selectedPaths != null ? selectedPaths.length : void 0) > 0)) {
        return;
      }
      ref2 = this.roots;
      for (j = 0, len = ref2.length; j < len; j++) {
        root = ref2[j];
        if (ref3 = root.getPath(), indexOf.call(selectedPaths, ref3) >= 0) {
          atom.confirm({
            message: "The root directory '" + root.directory.name + "' can't be removed.",
            buttons: ['OK']
          }, function() {});
          return;
        }
      }
      return atom.confirm({
        message: "Are you sure you want to delete the selected " + (selectedPaths.length > 1 ? 'items' : 'item') + "?",
        detailedMessage: "You are deleting:\n" + (selectedPaths.join('\n')),
        buttons: ['Move to Trash', 'Cancel']
      }, (function(_this) {
        return function(response) {
          var failedDeletions, firstSelectedEntry, k, len1, repo, selectedPath;
          if (response === 0) {
            failedDeletions = [];
            for (k = 0, len1 = selectedPaths.length; k < len1; k++) {
              selectedPath = selectedPaths[k];
              if (!fs.existsSync(selectedPath)) {
                continue;
              }
              _this.emitter.emit('will-delete-entry', {
                pathToDelete: selectedPath
              });
              if (shell.moveItemToTrash(selectedPath)) {
                _this.emitter.emit('entry-deleted', {
                  pathToDelete: selectedPath
                });
              } else {
                _this.emitter.emit('delete-entry-failed', {
                  pathToDelete: selectedPath
                });
                failedDeletions.push(selectedPath);
              }
              if (repo = repoForPath(selectedPath)) {
                repo.getPathStatus(selectedPath);
              }
            }
            if (failedDeletions.length > 0) {
              atom.notifications.addError(_this.formatTrashFailureMessage(failedDeletions), {
                description: _this.formatTrashEnabledMessage(),
                detail: "" + (failedDeletions.join('\n')),
                dismissable: true
              });
            }
            if (firstSelectedEntry = selectedEntries[0]) {
              _this.selectEntry(firstSelectedEntry.closest('.directory:not(.selected)'));
            }
            if (atom.config.get('tree-view.squashDirectoryNames')) {
              return _this.updateRoots();
            }
          }
        };
      })(this));
    };

    TreeView.prototype.formatTrashFailureMessage = function(failedDeletions) {
      var fileText;
      fileText = failedDeletions.length > 1 ? 'files' : 'file';
      return "The following " + fileText + " couldn't be moved to the trash.";
    };

    TreeView.prototype.formatTrashEnabledMessage = function() {
      switch (process.platform) {
        case 'linux':
          return 'Is `gvfs-trash` installed?';
        case 'darwin':
          return 'Is Trash enabled on the volume where the files are stored?';
        case 'win32':
          return 'Is there a Recycle Bin on the drive where the files are stored?';
      }
    };

    TreeView.prototype.copySelectedEntries = function() {
      var selectedPaths;
      selectedPaths = this.selectedPaths();
      if (!(selectedPaths && selectedPaths.length > 0)) {
        return;
      }
      window.localStorage.removeItem('tree-view:cutPath');
      return window.localStorage['tree-view:copyPath'] = JSON.stringify(selectedPaths);
    };

    TreeView.prototype.cutSelectedEntries = function() {
      var selectedPaths;
      selectedPaths = this.selectedPaths();
      if (!(selectedPaths && selectedPaths.length > 0)) {
        return;
      }
      window.localStorage.removeItem('tree-view:copyPath');
      return window.localStorage['tree-view:cutPath'] = JSON.stringify(selectedPaths);
    };

    TreeView.prototype.pasteEntries = function() {
      var copiedPaths, cutPaths, initialPath, initialPaths, j, len, newDirectoryPath, results, selectedEntry;
      selectedEntry = this.selectedEntry();
      if (!selectedEntry) {
        return;
      }
      cutPaths = window.localStorage['tree-view:cutPath'] ? JSON.parse(window.localStorage['tree-view:cutPath']) : null;
      copiedPaths = window.localStorage['tree-view:copyPath'] ? JSON.parse(window.localStorage['tree-view:copyPath']) : null;
      initialPaths = copiedPaths || cutPaths;
      if (!(initialPaths != null ? initialPaths.length : void 0)) {
        return;
      }
      newDirectoryPath = selectedEntry.getPath();
      if (selectedEntry.classList.contains('file')) {
        newDirectoryPath = path.dirname(newDirectoryPath);
      }
      results = [];
      for (j = 0, len = initialPaths.length; j < len; j++) {
        initialPath = initialPaths[j];
        if (fs.existsSync(initialPath)) {
          if (copiedPaths) {
            results.push(this.copyEntry(initialPath, newDirectoryPath));
          } else if (cutPaths) {
            if (!this.moveEntry(initialPath, newDirectoryPath)) {
              break;
            } else {
              results.push(void 0);
            }
          } else {
            results.push(void 0);
          }
        } else {
          results.push(void 0);
        }
      }
      return results;
    };

    TreeView.prototype.add = function(isCreatingFile) {
      var dialog, ref2, ref3, selectedEntry, selectedPath;
      selectedEntry = (ref2 = this.selectedEntry()) != null ? ref2 : this.roots[0];
      selectedPath = (ref3 = selectedEntry != null ? selectedEntry.getPath() : void 0) != null ? ref3 : '';
      dialog = new AddDialog(selectedPath, isCreatingFile);
      dialog.onDidCreateDirectory((function(_this) {
        return function(createdPath) {
          var ref4;
          if ((ref4 = _this.entryForPath(createdPath)) != null) {
            ref4.reload();
          }
          _this.selectEntryForPath(createdPath);
          if (atom.config.get('tree-view.squashDirectoryNames')) {
            _this.updateRoots();
          }
          return _this.emitter.emit('directory-created', {
            path: createdPath
          });
        };
      })(this));
      dialog.onDidCreateFile((function(_this) {
        return function(createdPath) {
          var ref4;
          if ((ref4 = _this.entryForPath(createdPath)) != null) {
            ref4.reload();
          }
          atom.workspace.open(createdPath);
          if (atom.config.get('tree-view.squashDirectoryNames')) {
            _this.updateRoots();
          }
          return _this.emitter.emit('file-created', {
            path: createdPath
          });
        };
      })(this));
      return dialog.attach();
    };

    TreeView.prototype.removeProjectFolder = function(e) {
      var pathToRemove, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
      pathToRemove = (ref2 = e.target.closest(".project-root > .header")) != null ? (ref3 = ref2.querySelector(".name")) != null ? ref3.dataset.path : void 0 : void 0;
      if (pathToRemove == null) {
        pathToRemove = (ref4 = this.selectedEntry()) != null ? (ref5 = ref4.closest(".project-root")) != null ? (ref6 = ref5.querySelector(".header")) != null ? (ref7 = ref6.querySelector(".name")) != null ? ref7.dataset.path : void 0 : void 0 : void 0 : void 0;
      }
      if (this.roots.length === 1) {
        if (pathToRemove == null) {
          pathToRemove = (ref8 = this.roots[0].querySelector(".header")) != null ? (ref9 = ref8.querySelector(".name")) != null ? ref9.dataset.path : void 0 : void 0;
        }
      }
      if (pathToRemove != null) {
        return atom.project.removePath(pathToRemove);
      }
    };

    TreeView.prototype.selectedEntry = function() {
      return this.list.querySelector('.selected');
    };

    TreeView.prototype.selectEntry = function(entry) {
      var selectedEntries;
      if (entry == null) {
        return;
      }
      this.selectedPath = entry.getPath();
      this.lastFocusedEntry = entry;
      selectedEntries = this.getSelectedEntries();
      if (selectedEntries.length > 1 || selectedEntries[0] !== entry) {
        this.deselect(selectedEntries);
        entry.classList.add('selected');
      }
      return entry;
    };

    TreeView.prototype.getSelectedEntries = function() {
      return this.list.querySelectorAll('.selected');
    };

    TreeView.prototype.deselect = function(elementsToDeselect) {
      var j, len, selected;
      if (elementsToDeselect == null) {
        elementsToDeselect = this.getSelectedEntries();
      }
      for (j = 0, len = elementsToDeselect.length; j < len; j++) {
        selected = elementsToDeselect[j];
        selected.classList.remove('selected');
      }
      return void 0;
    };

    TreeView.prototype.scrollTop = function(top) {
      if (top != null) {
        return this.element.scrollTop = top;
      } else {
        return this.element.scrollTop;
      }
    };

    TreeView.prototype.scrollBottom = function(bottom) {
      if (bottom != null) {
        return this.element.scrollTop = bottom - this.element.offsetHeight;
      } else {
        return this.element.scrollTop + this.element.offsetHeight;
      }
    };

    TreeView.prototype.scrollToEntry = function(entry, center) {
      var element;
      if (center == null) {
        center = true;
      }
      element = (entry != null ? entry.classList.contains('directory') : void 0) ? entry.header : entry;
      return element != null ? element.scrollIntoViewIfNeeded(center) : void 0;
    };

    TreeView.prototype.scrollToBottom = function() {
      var lastEntry;
      if (lastEntry = _.last(this.list.querySelectorAll('.entry'))) {
        this.selectEntry(lastEntry);
        return this.scrollToEntry(lastEntry);
      }
    };

    TreeView.prototype.scrollToTop = function() {
      if (this.roots[0] != null) {
        this.selectEntry(this.roots[0]);
      }
      return this.element.scrollTop = 0;
    };

    TreeView.prototype.pageUp = function() {
      return this.element.scrollTop -= this.element.offsetHeight;
    };

    TreeView.prototype.pageDown = function() {
      return this.element.scrollTop += this.element.offsetHeight;
    };

    TreeView.prototype.copyEntry = function(initialPath, newDirectoryPath) {
      var error, extension, fileCounter, filePath, initialPathIsDirectory, newPath, originalNewPath, realInitialPath, realNewDirectoryPath, repo;
      initialPathIsDirectory = fs.isDirectorySync(initialPath);
      realNewDirectoryPath = fs.realpathSync(newDirectoryPath) + path.sep;
      realInitialPath = fs.realpathSync(initialPath) + path.sep;
      if (initialPathIsDirectory && realNewDirectoryPath.startsWith(realInitialPath)) {
        if (!fs.isSymbolicLinkSync(initialPath)) {
          atom.notifications.addWarning('Cannot copy a folder into itself');
          return;
        }
      }
      newPath = path.join(newDirectoryPath, path.basename(initialPath));
      fileCounter = 0;
      originalNewPath = newPath;
      while (fs.existsSync(newPath)) {
        if (initialPathIsDirectory) {
          newPath = "" + originalNewPath + fileCounter;
        } else {
          extension = getFullExtension(originalNewPath);
          filePath = path.join(path.dirname(originalNewPath), path.basename(originalNewPath, extension));
          newPath = "" + filePath + fileCounter + extension;
        }
        fileCounter += 1;
      }
      try {
        this.emitter.emit('will-copy-entry', {
          initialPath: initialPath,
          newPath: newPath
        });
        if (initialPathIsDirectory) {
          fs.copySync(initialPath, newPath);
        } else {
          fs.writeFileSync(newPath, fs.readFileSync(initialPath));
        }
        this.emitter.emit('entry-copied', {
          initialPath: initialPath,
          newPath: newPath
        });
        if (repo = repoForPath(newPath)) {
          repo.getPathStatus(initialPath);
          return repo.getPathStatus(newPath);
        }
      } catch (error1) {
        error = error1;
        this.emitter.emit('copy-entry-failed', {
          initialPath: initialPath,
          newPath: newPath
        });
        return atom.notifications.addWarning("Failed to copy entry " + initialPath + " to " + newDirectoryPath, {
          detail: error.message
        });
      }
    };

    TreeView.prototype.moveEntry = function(initialPath, newDirectoryPath) {
      var error, newPath, realInitialPath, realNewDirectoryPath, repo;
      try {
        realNewDirectoryPath = fs.realpathSync(newDirectoryPath) + path.sep;
        realInitialPath = fs.realpathSync(initialPath) + path.sep;
        if (fs.isDirectorySync(initialPath) && realNewDirectoryPath.startsWith(realInitialPath)) {
          if (!fs.isSymbolicLinkSync(initialPath)) {
            atom.notifications.addWarning('Cannot move a folder into itself');
            return;
          }
        }
      } catch (error1) {
        error = error1;
        atom.notifications.addWarning("Failed to move entry " + initialPath + " to " + newDirectoryPath, {
          detail: error.message
        });
      }
      newPath = path.join(newDirectoryPath, path.basename(initialPath));
      try {
        this.emitter.emit('will-move-entry', {
          initialPath: initialPath,
          newPath: newPath
        });
        fs.moveSync(initialPath, newPath);
        this.emitter.emit('entry-moved', {
          initialPath: initialPath,
          newPath: newPath
        });
        if (repo = repoForPath(newPath)) {
          repo.getPathStatus(initialPath);
          repo.getPathStatus(newPath);
        }
      } catch (error1) {
        error = error1;
        if (error.code === 'EEXIST') {
          return this.moveConflictingEntry(initialPath, newPath, newDirectoryPath);
        } else {
          this.emitter.emit('move-entry-failed', {
            initialPath: initialPath,
            newPath: newPath
          });
          atom.notifications.addWarning("Failed to move entry " + initialPath + " to " + newDirectoryPath, {
            detail: error.message
          });
        }
      }
      return true;
    };

    TreeView.prototype.moveConflictingEntry = function(initialPath, newPath, newDirectoryPath) {
      var chosen, entries, entry, error, j, len, repo;
      try {
        if (!fs.isDirectorySync(initialPath)) {
          chosen = atom.confirm({
            message: "'" + (path.relative(newDirectoryPath, newPath)) + "' already exists",
            detailedMessage: 'Do you want to replace it?',
            buttons: ['Replace file', 'Skip', 'Cancel']
          });
          switch (chosen) {
            case 0:
              fs.renameSync(initialPath, newPath);
              this.emitter.emit('entry-moved', {
                initialPath: initialPath,
                newPath: newPath
              });
              if (repo = repoForPath(newPath)) {
                repo.getPathStatus(initialPath);
                repo.getPathStatus(newPath);
              }
              break;
            case 2:
              return false;
          }
        } else {
          entries = fs.readdirSync(initialPath);
          for (j = 0, len = entries.length; j < len; j++) {
            entry = entries[j];
            if (fs.existsSync(path.join(newPath, entry))) {
              if (!this.moveConflictingEntry(path.join(initialPath, entry), path.join(newPath, entry), newDirectoryPath)) {
                return false;
              }
            } else {
              this.moveEntry(path.join(initialPath, entry), newPath);
            }
          }
          if (!fs.readdirSync(initialPath).length) {
            fs.rmdirSync(initialPath);
          }
        }
      } catch (error1) {
        error = error1;
        this.emitter.emit('move-entry-failed', {
          initialPath: initialPath,
          newPath: newPath
        });
        atom.notifications.addWarning("Failed to move entry " + initialPath + " to " + newPath, {
          detail: error.message
        });
      }
      return true;
    };

    TreeView.prototype.onStylesheetsChanged = function() {
      if (!this.isVisible()) {
        return;
      }
      this.element.style.display = 'none';
      this.element.offsetWidth;
      return this.element.style.display = '';
    };

    TreeView.prototype.onMouseDown = function(e) {
      var cmdKey, entryToSelect, shiftKey;
      if (!(entryToSelect = e.target.closest('.entry'))) {
        return;
      }
      e.stopPropagation();
      cmdKey = e.metaKey || (e.ctrlKey && process.platform !== 'darwin');
      if (entryToSelect.classList.contains('selected')) {
        if (e.button === 2 || (e.ctrlKey && process.platform === 'darwin')) {
          return;
        } else {
          shiftKey = e.shiftKey;
          this.selectOnMouseUp = {
            shiftKey: shiftKey,
            cmdKey: cmdKey
          };
          return;
        }
      }
      if (e.shiftKey && cmdKey) {
        this.selectContinuousEntries(entryToSelect, false);
        return this.showMultiSelectMenuIfNecessary();
      } else if (e.shiftKey) {
        this.selectContinuousEntries(entryToSelect);
        return this.showMultiSelectMenuIfNecessary();
      } else if (cmdKey) {
        this.selectMultipleEntries(entryToSelect);
        this.lastFocusedEntry = entryToSelect;
        return this.showMultiSelectMenuIfNecessary();
      } else {
        this.selectEntry(entryToSelect);
        return this.showFullMenu();
      }
    };

    TreeView.prototype.onMouseUp = function(e) {
      var cmdKey, entryToSelect, ref2, shiftKey;
      if (this.selectOnMouseUp == null) {
        return;
      }
      ref2 = this.selectOnMouseUp, shiftKey = ref2.shiftKey, cmdKey = ref2.cmdKey;
      this.selectOnMouseUp = null;
      if (!(entryToSelect = e.target.closest('.entry'))) {
        return;
      }
      e.stopPropagation();
      if (shiftKey && cmdKey) {
        this.selectContinuousEntries(entryToSelect, false);
        return this.showMultiSelectMenuIfNecessary();
      } else if (shiftKey) {
        this.selectContinuousEntries(entryToSelect);
        return this.showMultiSelectMenuIfNecessary();
      } else if (cmdKey) {
        this.deselect([entryToSelect]);
        this.lastFocusedEntry = entryToSelect;
        return this.showMultiSelectMenuIfNecessary();
      } else {
        this.selectEntry(entryToSelect);
        return this.showFullMenu();
      }
    };

    TreeView.prototype.selectedPaths = function() {
      var entry, j, len, ref2, results;
      ref2 = this.getSelectedEntries();
      results = [];
      for (j = 0, len = ref2.length; j < len; j++) {
        entry = ref2[j];
        results.push(entry.getPath());
      }
      return results;
    };

    TreeView.prototype.selectContinuousEntries = function(entry, deselectOthers) {
      var currentSelectedEntry, element, elements, entries, entryIndex, i, j, len, parentContainer, ref2, selectedIndex;
      if (deselectOthers == null) {
        deselectOthers = true;
      }
      currentSelectedEntry = (ref2 = this.lastFocusedEntry) != null ? ref2 : this.selectedEntry();
      parentContainer = entry.parentElement;
      elements = [];
      if (parentContainer === currentSelectedEntry.parentElement) {
        entries = Array.from(parentContainer.querySelectorAll('.entry'));
        entryIndex = entries.indexOf(entry);
        selectedIndex = entries.indexOf(currentSelectedEntry);
        elements = (function() {
          var j, ref3, ref4, results;
          results = [];
          for (i = j = ref3 = entryIndex, ref4 = selectedIndex; ref3 <= ref4 ? j <= ref4 : j >= ref4; i = ref3 <= ref4 ? ++j : --j) {
            results.push(entries[i]);
          }
          return results;
        })();
        if (deselectOthers) {
          this.deselect();
        }
        for (j = 0, len = elements.length; j < len; j++) {
          element = elements[j];
          element.classList.add('selected');
        }
      }
      return elements;
    };

    TreeView.prototype.selectMultipleEntries = function(entry) {
      if (entry != null) {
        entry.classList.toggle('selected');
      }
      return entry;
    };

    TreeView.prototype.showFullMenu = function() {
      this.list.classList.remove('multi-select');
      return this.list.classList.add('full-menu');
    };

    TreeView.prototype.showMultiSelectMenu = function() {
      this.list.classList.remove('full-menu');
      return this.list.classList.add('multi-select');
    };

    TreeView.prototype.showMultiSelectMenuIfNecessary = function() {
      if (this.getSelectedEntries().length > 1) {
        return this.showMultiSelectMenu();
      } else {
        return this.showFullMenu();
      }
    };

    TreeView.prototype.multiSelectEnabled = function() {
      return this.list.classList.contains('multi-select');
    };

    TreeView.prototype.onDragEnter = function(e) {
      var entry;
      if (entry = e.target.closest('.entry.directory')) {
        if (this.rootDragAndDrop.isDragging(e)) {
          return;
        }
        if (!this.isAtomTreeViewEvent(e)) {
          return;
        }
        e.stopPropagation();
        if (!this.dragEventCounts.get(entry)) {
          this.dragEventCounts.set(entry, 0);
        }
        if (!(this.dragEventCounts.get(entry) !== 0 || entry.classList.contains('selected'))) {
          entry.classList.add('drag-over', 'selected');
        }
        return this.dragEventCounts.set(entry, this.dragEventCounts.get(entry) + 1);
      }
    };

    TreeView.prototype.onDragLeave = function(e) {
      var entry;
      if (entry = e.target.closest('.entry.directory')) {
        if (this.rootDragAndDrop.isDragging(e)) {
          return;
        }
        if (!this.isAtomTreeViewEvent(e)) {
          return;
        }
        e.stopPropagation();
        this.dragEventCounts.set(entry, this.dragEventCounts.get(entry) - 1);
        if (this.dragEventCounts.get(entry) === 0 && entry.classList.contains('drag-over')) {
          return entry.classList.remove('drag-over', 'selected');
        }
      }
    };

    TreeView.prototype.onDragStart = function(e) {
      var dragImage, entry, entryPath, initialPaths, j, key, len, newElement, parentSelected, ref2, ref3, target, value;
      this.dragEventCounts = new WeakMap;
      this.selectOnMouseUp = null;
      if (entry = e.target.closest('.entry')) {
        e.stopPropagation();
        if (this.rootDragAndDrop.canDragStart(e)) {
          return this.rootDragAndDrop.onDragStart(e);
        }
        dragImage = document.createElement("ol");
        dragImage.classList.add("entries", "list-tree");
        dragImage.style.position = "absolute";
        dragImage.style.top = 0;
        dragImage.style.left = 0;
        dragImage.style.willChange = "transform";
        initialPaths = [];
        ref2 = this.getSelectedEntries();
        for (j = 0, len = ref2.length; j < len; j++) {
          target = ref2[j];
          entryPath = target.querySelector(".name").dataset.path;
          parentSelected = target.parentNode.closest(".entry.selected");
          if (!parentSelected) {
            initialPaths.push(entryPath);
            newElement = target.cloneNode(true);
            if (newElement.classList.contains("directory")) {
              newElement.querySelector(".entries").remove();
            }
            ref3 = getStyleObject(target);
            for (key in ref3) {
              value = ref3[key];
              newElement.style[key] = value;
            }
            newElement.style.paddingLeft = "1em";
            newElement.style.paddingRight = "1em";
            dragImage.append(newElement);
          }
        }
        document.body.appendChild(dragImage);
        e.dataTransfer.effectAllowed = "move";
        e.dataTransfer.setDragImage(dragImage, 0, 0);
        e.dataTransfer.setData("initialPaths", JSON.stringify(initialPaths));
        e.dataTransfer.setData("atom-tree-view-event", "true");
        return window.requestAnimationFrame(function() {
          return dragImage.remove();
        });
      }
    };

    TreeView.prototype.onDragOver = function(e) {
      var entry;
      if (entry = e.target.closest('.entry.directory')) {
        if (this.rootDragAndDrop.isDragging(e)) {
          return;
        }
        if (!this.isAtomTreeViewEvent(e)) {
          return;
        }
        e.preventDefault();
        e.stopPropagation();
        if (this.dragEventCounts.get(entry) > 0 && !entry.classList.contains('selected')) {
          return entry.classList.add('drag-over', 'selected');
        }
      }
    };

    TreeView.prototype.onDrop = function(e) {
      var entry, file, initialPath, initialPaths, j, k, l, len, len1, newDirectoryPath, ref2, ref3, ref4, ref5, results, results1, results2;
      this.dragEventCounts = new WeakMap;
      if (entry = e.target.closest('.entry.directory')) {
        if (this.rootDragAndDrop.isDragging(e)) {
          return;
        }
        if (!this.isAtomTreeViewEvent(e)) {
          return;
        }
        e.preventDefault();
        e.stopPropagation();
        newDirectoryPath = (ref2 = entry.querySelector('.name')) != null ? ref2.dataset.path : void 0;
        if (!newDirectoryPath) {
          return false;
        }
        initialPaths = e.dataTransfer.getData('initialPaths');
        if (initialPaths) {
          initialPaths = JSON.parse(initialPaths);
          if (initialPaths.includes(newDirectoryPath)) {
            return;
          }
          entry.classList.remove('drag-over', 'selected');
          results = [];
          for (j = initialPaths.length - 1; j >= 0; j += -1) {
            initialPath = initialPaths[j];
            if ((ref3 = this.entryForPath(initialPath)) != null) {
              if (typeof ref3.collapse === "function") {
                ref3.collapse();
              }
            }
            if ((process.platform === 'darwin' && e.metaKey) || e.ctrlKey) {
              results.push(this.copyEntry(initialPath, newDirectoryPath));
            } else {
              if (!this.moveEntry(initialPath, newDirectoryPath)) {
                break;
              } else {
                results.push(void 0);
              }
            }
          }
          return results;
        } else {
          entry.classList.remove('selected');
          ref4 = e.dataTransfer.files;
          results1 = [];
          for (k = 0, len = ref4.length; k < len; k++) {
            file = ref4[k];
            if ((process.platform === 'darwin' && e.metaKey) || e.ctrlKey) {
              results1.push(this.copyEntry(file.path, newDirectoryPath));
            } else {
              if (!this.moveEntry(file.path, newDirectoryPath)) {
                break;
              } else {
                results1.push(void 0);
              }
            }
          }
          return results1;
        }
      } else if (e.dataTransfer.files.length) {
        ref5 = e.dataTransfer.files;
        results2 = [];
        for (l = 0, len1 = ref5.length; l < len1; l++) {
          entry = ref5[l];
          results2.push(atom.project.addPath(entry.path));
        }
        return results2;
      }
    };

    TreeView.prototype.isAtomTreeViewEvent = function(e) {
      var item, j, len, ref2;
      ref2 = e.dataTransfer.items;
      for (j = 0, len = ref2.length; j < len; j++) {
        item = ref2[j];
        if (item.type === 'atom-tree-view-event' || item.kind === 'file') {
          return true;
        }
      }
      return false;
    };

    TreeView.prototype.isVisible = function() {
      return this.element.offsetWidth !== 0 || this.element.offsetHeight !== 0;
    };

    return TreeView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90cmVlLXZpZXcvbGliL3RyZWUtdmlldy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBLHVSQUFBO0lBQUE7Ozs7RUFBQSxJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBQ04sUUFBUyxPQUFBLENBQVEsVUFBUjs7RUFFVixDQUFBLEdBQUksT0FBQSxDQUFRLGlCQUFSOztFQUNKLE1BQWtELE9BQUEsQ0FBUSxNQUFSLENBQWxELEVBQUMscUNBQUQsRUFBa0IsNkNBQWxCLEVBQXVDOztFQUN2QyxPQUFrRCxPQUFBLENBQVEsV0FBUixDQUFsRCxFQUFDLDhCQUFELEVBQWMsb0NBQWQsRUFBOEI7O0VBQzlCLEVBQUEsR0FBSyxPQUFBLENBQVEsU0FBUjs7RUFFTCxTQUFBLEdBQVksT0FBQSxDQUFRLGNBQVI7O0VBQ1osVUFBQSxHQUFhLE9BQUEsQ0FBUSxlQUFSOztFQUNiLFVBQUEsR0FBYSxPQUFBLENBQVEsZUFBUjs7RUFDYixZQUFBLEdBQWU7O0VBRWYsZUFBQSxHQUFrQixPQUFBLENBQVEscUJBQVI7O0VBRWxCLFNBQUEsR0FBWSxPQUFBLENBQVEsYUFBUjs7RUFDWixhQUFBLEdBQWdCLE9BQUEsQ0FBUSxrQkFBUjs7RUFDaEIsZUFBQSxHQUFrQixPQUFBLENBQVEsc0JBQVI7O0VBRWxCLGFBQUEsR0FBZ0I7O0VBRWhCLFlBQUEsR0FBZSxTQUFDLE9BQUQ7V0FDYixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsT0FBaEIsRUFBeUIsQ0FBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsT0FBaEIsQ0FBN0I7RUFEYTs7RUFHZixNQUFBLEdBQVM7O0VBRVQsTUFBTSxDQUFDLE9BQVAsR0FDTTtJQUNTLGtCQUFDLEtBQUQ7Ozs7O0FBQ1gsVUFBQTtNQUFBLElBQUMsQ0FBQSxFQUFELEdBQU0sTUFBQTtNQUNOLElBQUMsQ0FBQSxPQUFELEdBQVcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDWCxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixZQUF2QixFQUFxQyxXQUFyQztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsUUFBVCxHQUFvQixDQUFDO01BRXJCLElBQUMsQ0FBQSxJQUFELEdBQVEsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsSUFBdkI7TUFDUixJQUFDLENBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFoQixDQUFvQixnQkFBcEIsRUFBc0MsV0FBdEMsRUFBbUQsV0FBbkQsRUFBZ0UsMEJBQWhFLEVBQTRGLGlCQUE1RjtNQUVBLElBQUMsQ0FBQSxXQUFELEdBQWUsSUFBSTtNQUNuQixJQUFDLENBQUEsT0FBRCxHQUFXLElBQUk7TUFDZixJQUFDLENBQUEsS0FBRCxHQUFTO01BQ1QsSUFBQyxDQUFBLFlBQUQsR0FBZ0I7TUFDaEIsSUFBQyxDQUFBLGVBQUQsR0FBbUI7TUFDbkIsSUFBQyxDQUFBLGdCQUFELEdBQW9CO01BQ3BCLElBQUMsQ0FBQSxlQUFELEdBQW1CO01BQ25CLElBQUMsQ0FBQSxTQUFELEdBQWE7TUFDYixJQUFDLENBQUEsZ0JBQUQsR0FBb0IsSUFBSTtNQUN4QixJQUFDLENBQUEsYUFBRCxHQUFpQjtNQUNqQixJQUFDLENBQUEsZ0JBQUQsR0FBb0I7TUFFcEIsSUFBQyxDQUFBLGVBQUQsR0FBbUIsSUFBSTtNQUN2QixJQUFDLENBQUEsZUFBRCxHQUFtQixJQUFJLGVBQUosQ0FBb0IsSUFBcEI7TUFFbkIsSUFBQyxDQUFBLFlBQUQsQ0FBQTtNQUVBLE9BQU8sQ0FBQyxRQUFSLENBQWlCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtBQUNmLGNBQUE7VUFBQSxLQUFDLENBQUEsb0JBQUQsQ0FBQTtVQUNBLG9CQUFBLEdBQXVCLENBQUMsQ0FBQyxRQUFGLENBQVcsS0FBQyxDQUFBLG9CQUFaLEVBQWtDLEdBQWxDO1VBQ3ZCLEtBQUMsQ0FBQSxXQUFXLENBQUMsR0FBYixDQUFpQixJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFaLENBQWlDLG9CQUFqQyxDQUFqQjtVQUNBLEtBQUMsQ0FBQSxXQUFXLENBQUMsR0FBYixDQUFpQixJQUFJLENBQUMsTUFBTSxDQUFDLHVCQUFaLENBQW9DLG9CQUFwQyxDQUFqQjtpQkFDQSxLQUFDLENBQUEsV0FBVyxDQUFDLEdBQWIsQ0FBaUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBWixDQUFvQyxvQkFBcEMsQ0FBakI7UUFMZTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBakI7TUFPQSxJQUFDLENBQUEsV0FBRCxDQUFhLEtBQUssQ0FBQyx3QkFBbkI7TUFFQSxnREFBc0IsQ0FBRSxnQkFBckIsR0FBOEIsQ0FBakM7QUFDRTtBQUFBLGFBQUEsc0NBQUE7O1VBQUEsSUFBQyxDQUFBLHFCQUFELENBQXVCLElBQUMsQ0FBQSxZQUFELENBQWMsWUFBZCxDQUF2QjtBQUFBLFNBREY7T0FBQSxNQUFBO1FBR0UsSUFBQyxDQUFBLFdBQUQsQ0FBYSxJQUFDLENBQUEsS0FBTSxDQUFBLENBQUEsQ0FBcEIsRUFIRjs7TUFLQSxJQUFHLHlCQUFBLElBQW9CLDBCQUF2QjtRQUNFLFFBQUEsR0FBVyxJQUFJLG9CQUFKLENBQXlCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7WUFDbEMsSUFBRyxLQUFDLENBQUEsU0FBRCxDQUFBLENBQUg7Y0FDRSxLQUFDLENBQUEsT0FBTyxDQUFDLFNBQVQsR0FBcUIsS0FBSyxDQUFDO2NBQzNCLEtBQUMsQ0FBQSxPQUFPLENBQUMsVUFBVCxHQUFzQixLQUFLLENBQUM7cUJBQzVCLFFBQVEsQ0FBQyxVQUFULENBQUEsRUFIRjs7VUFEa0M7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpCO1FBTVgsUUFBUSxDQUFDLE9BQVQsQ0FBaUIsSUFBQyxDQUFBLE9BQWxCLEVBUEY7O01BU0EsSUFBNkMsS0FBSyxDQUFDLEtBQU4sR0FBYyxDQUEzRDtRQUFBLElBQUMsQ0FBQSxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQWYsR0FBMEIsS0FBSyxDQUFDLEtBQVAsR0FBYSxLQUF0Qzs7TUFFQSxJQUFDLENBQUEsV0FBVyxDQUFDLEdBQWIsQ0FBaUIsSUFBQyxDQUFBLGVBQUQsQ0FBaUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEdBQUQ7QUFDaEMsY0FBQTtVQURrQywrQkFBYTtVQUMvQyxPQUFBLEdBQVUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFmLENBQUE7VUFDVixJQUFHLEVBQUUsQ0FBQyxlQUFILENBQW1CLFdBQW5CLENBQUg7WUFDRSxXQUFBLElBQWUsSUFBSSxDQUFDO0FBQ3BCO2lCQUFBLDJDQUFBOztjQUNFLFFBQUEsR0FBVyxNQUFNLENBQUMsT0FBUCxDQUFBO2NBQ1gsdUJBQUcsUUFBUSxDQUFFLFVBQVYsQ0FBcUIsV0FBckIsVUFBSDs2QkFDRSxLQUFDLENBQUEsYUFBYSxDQUFDLElBQWYsQ0FBb0IsUUFBcEIsR0FERjtlQUFBLE1BQUE7cUNBQUE7O0FBRkY7MkJBRkY7V0FBQSxNQUFBO0FBT0U7aUJBQUEsMkNBQUE7O2NBQ0UsUUFBQSxHQUFXLE1BQU0sQ0FBQyxPQUFQLENBQUE7Y0FDWCxJQUFHLFFBQUEsS0FBWSxXQUFmOzhCQUNFLEtBQUMsQ0FBQSxhQUFhLENBQUMsSUFBZixDQUFvQixRQUFwQixHQURGO2VBQUEsTUFBQTtzQ0FBQTs7QUFGRjs0QkFQRjs7UUFGZ0M7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWpCLENBQWpCO01BY0EsSUFBQyxDQUFBLFdBQVcsQ0FBQyxHQUFiLENBQWlCLElBQUMsQ0FBQSxZQUFELENBQWMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEdBQUQ7QUFDN0IsY0FBQTtVQUQrQiwrQkFBYTtBQUM1QztBQUFBO2VBQUEsd0NBQUE7O1lBQ0UsUUFBQSxHQUFXLE1BQU0sQ0FBQyxPQUFQLENBQUE7WUFDWCxLQUFBLEdBQVEsS0FBQyxDQUFBLGFBQWEsQ0FBQyxPQUFmLENBQXVCLFFBQXZCO1lBQ1IsSUFBRyxLQUFBLEtBQVcsQ0FBQyxDQUFmO2NBQ0UsTUFBTSxDQUFDLFNBQVAsQ0FBQSxDQUFrQixDQUFDLE9BQW5CLENBQTJCLFFBQVEsQ0FBQyxPQUFULENBQWlCLFdBQWpCLEVBQThCLE9BQTlCLENBQTNCOzJCQUNBLEtBQUMsQ0FBQSxhQUFhLENBQUMsTUFBZixDQUFzQixLQUF0QixFQUE2QixDQUE3QixHQUZGO2FBQUEsTUFBQTttQ0FBQTs7QUFIRjs7UUFENkI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWQsQ0FBakI7TUFRQSxJQUFDLENBQUEsV0FBVyxDQUFDLEdBQWIsQ0FBaUIsSUFBQyxDQUFBLGlCQUFELENBQW1CLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxHQUFEO0FBQ2xDLGNBQUE7VUFEb0MsK0JBQWE7VUFDakQsS0FBQSxHQUFRLEtBQUMsQ0FBQSxhQUFhLENBQUMsT0FBZixDQUF1QixXQUF2QjtVQUNSLElBQW1DLEtBQUEsS0FBVyxDQUFDLENBQS9DO21CQUFBLEtBQUMsQ0FBQSxhQUFhLENBQUMsTUFBZixDQUFzQixLQUF0QixFQUE2QixDQUE3QixFQUFBOztRQUZrQztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBbkIsQ0FBakI7TUFJQSxJQUFDLENBQUEsV0FBVyxDQUFDLEdBQWIsQ0FBaUIsSUFBQyxDQUFBLGlCQUFELENBQW1CLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxHQUFEO0FBQ2xDLGNBQUE7VUFEb0MsZUFBRDtVQUNuQyxPQUFBLEdBQVUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFmLENBQUE7VUFDVixJQUFHLEVBQUUsQ0FBQyxlQUFILENBQW1CLFlBQW5CLENBQUg7WUFDRSxZQUFBLElBQWdCLElBQUksQ0FBQztBQUNyQjtpQkFBQSwyQ0FBQTs7Y0FDRSxRQUFBLEdBQVcsTUFBTSxDQUFDLE9BQVAsQ0FBQTtjQUNYLHdCQUFHLFFBQVEsQ0FBRSxVQUFWLENBQXFCLFlBQXJCLFdBQUEsSUFBdUMsQ0FBSSxNQUFNLENBQUMsVUFBUCxDQUFBLENBQTlDOzZCQUNFLEtBQUMsQ0FBQSxnQkFBZ0IsQ0FBQyxJQUFsQixDQUF1QixRQUF2QixHQURGO2VBQUEsTUFBQTtxQ0FBQTs7QUFGRjsyQkFGRjtXQUFBLE1BQUE7QUFPRTtpQkFBQSwyQ0FBQTs7Y0FDRSxRQUFBLEdBQVcsTUFBTSxDQUFDLE9BQVAsQ0FBQTtjQUNYLElBQUcsUUFBQSxLQUFZLFlBQVosSUFBNkIsQ0FBSSxNQUFNLENBQUMsVUFBUCxDQUFBLENBQXBDOzhCQUNFLEtBQUMsQ0FBQSxnQkFBZ0IsQ0FBQyxJQUFsQixDQUF1QixRQUF2QixHQURGO2VBQUEsTUFBQTtzQ0FBQTs7QUFGRjs0QkFQRjs7UUFGa0M7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQW5CLENBQWpCO01BY0EsSUFBQyxDQUFBLFdBQVcsQ0FBQyxHQUFiLENBQWlCLElBQUMsQ0FBQSxjQUFELENBQWdCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxHQUFEO0FBQy9CLGNBQUE7VUFEaUMsZUFBRDtBQUNoQztBQUFBO2VBQUEsd0NBQUE7O1lBQ0UsS0FBQSxHQUFRLEtBQUMsQ0FBQSxnQkFBZ0IsQ0FBQyxPQUFsQixDQUEwQixNQUFNLENBQUMsT0FBUCxDQUFBLENBQTFCO1lBQ1IsSUFBRyxLQUFBLEtBQVcsQ0FBQyxDQUFmO2NBQ0UsTUFBTSxDQUFDLE9BQVAsQ0FBQTsyQkFDQSxLQUFDLENBQUEsZ0JBQWdCLENBQUMsTUFBbEIsQ0FBeUIsS0FBekIsRUFBZ0MsQ0FBaEMsR0FGRjthQUFBLE1BQUE7bUNBQUE7O0FBRkY7O1FBRCtCO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFoQixDQUFqQjtNQU9BLElBQUMsQ0FBQSxXQUFXLENBQUMsR0FBYixDQUFpQixJQUFDLENBQUEsbUJBQUQsQ0FBcUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEdBQUQ7QUFDcEMsY0FBQTtVQURzQyxlQUFEO1VBQ3JDLEtBQUEsR0FBUSxLQUFDLENBQUEsZ0JBQWdCLENBQUMsT0FBbEIsQ0FBMEIsWUFBMUI7VUFDUixJQUFzQyxLQUFBLEtBQVcsQ0FBQyxDQUFsRDttQkFBQSxLQUFDLENBQUEsZ0JBQWdCLENBQUMsTUFBbEIsQ0FBeUIsS0FBekIsRUFBZ0MsQ0FBaEMsRUFBQTs7UUFGb0M7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXJCLENBQWpCO0lBbEdXOzt1QkFzR2IsU0FBQSxHQUFXLFNBQUE7YUFDVDtRQUFBLHdCQUFBLEVBQTBCLElBQUksQ0FBQyxTQUFDLEtBQUQ7QUFDN0IsY0FBQTtBQUFBLGVBQUEsdUNBQUE7O1lBQUEsSUFBRSxDQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBZixDQUFGLEdBQXlCLElBQUksQ0FBQyxTQUFTLENBQUMsdUJBQWYsQ0FBQTtBQUF6QjtpQkFDQTtRQUY2QixDQUFELENBQUosQ0FFbEIsSUFBQyxDQUFBLEtBRmlCLENBQTFCO1FBR0EsWUFBQSxFQUFjLFVBSGQ7UUFJQSxhQUFBLEVBQWUsS0FBSyxDQUFDLElBQU4sQ0FBVyxJQUFDLENBQUEsa0JBQUQsQ0FBQSxDQUFYLEVBQWtDLFNBQUMsS0FBRDtpQkFBVyxLQUFLLENBQUMsT0FBTixDQUFBO1FBQVgsQ0FBbEMsQ0FKZjtRQUtBLFVBQUEsRUFBWSxJQUFDLENBQUEsT0FBTyxDQUFDLFVBTHJCO1FBTUEsU0FBQSxFQUFXLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FOcEI7UUFPQSxLQUFBLEVBQU8sUUFBQSxDQUFTLElBQUMsQ0FBQSxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQWYsSUFBd0IsQ0FBakMsQ0FQUDs7SUFEUzs7dUJBVVgsT0FBQSxHQUFTLFNBQUE7QUFDUCxVQUFBO0FBQUE7QUFBQSxXQUFBLHNDQUFBOztRQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBZixDQUFBO0FBQUE7TUFDQSxJQUFDLENBQUEsV0FBVyxDQUFDLE9BQWIsQ0FBQTtNQUNBLElBQUMsQ0FBQSxlQUFlLENBQUMsT0FBakIsQ0FBQTthQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsSUFBVCxDQUFjLGFBQWQ7SUFKTzs7dUJBTVQsWUFBQSxHQUFjLFNBQUMsUUFBRDthQUNaLElBQUMsQ0FBQSxPQUFPLENBQUMsRUFBVCxDQUFZLGFBQVosRUFBMkIsUUFBM0I7SUFEWTs7dUJBR2QsUUFBQSxHQUFVLFNBQUE7YUFBRztJQUFIOzt1QkFFVixNQUFBLEdBQVEsU0FBQTthQUFHO0lBQUg7O3VCQUVSLG9CQUFBLEdBQXNCLFNBQUE7TUFDcEIsSUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsMkJBQWhCLENBQUg7ZUFDRSxRQURGO09BQUEsTUFBQTtlQUdFLE9BSEY7O0lBRG9COzt1QkFNdEIsbUJBQUEsR0FBcUIsU0FBQTthQUFHLENBQUMsTUFBRCxFQUFTLE9BQVQ7SUFBSDs7dUJBRXJCLG1CQUFBLEdBQXFCLFNBQUE7YUFBRztJQUFIOzt1QkFFckIsaUJBQUEsR0FBbUIsU0FBQTtBQUNqQixVQUFBO01BQUEsSUFBQyxDQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBWixHQUFvQjtNQUNwQixNQUFBLEdBQVMsSUFBQyxDQUFBLElBQUksQ0FBQztNQUNmLElBQUMsQ0FBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQVosR0FBb0I7YUFDcEI7SUFKaUI7O3VCQU1uQixrQkFBQSxHQUFvQixTQUFDLFFBQUQ7YUFDbEIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxFQUFULENBQVksbUJBQVosRUFBaUMsUUFBakM7SUFEa0I7O3VCQUdwQixhQUFBLEdBQWUsU0FBQyxRQUFEO2FBQ2IsSUFBQyxDQUFBLE9BQU8sQ0FBQyxFQUFULENBQVksY0FBWixFQUE0QixRQUE1QjtJQURhOzt1QkFHZixpQkFBQSxHQUFtQixTQUFDLFFBQUQ7YUFDakIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxFQUFULENBQVksbUJBQVosRUFBaUMsUUFBakM7SUFEaUI7O3VCQUduQixjQUFBLEdBQWdCLFNBQUMsUUFBRDthQUNkLElBQUMsQ0FBQSxPQUFPLENBQUMsRUFBVCxDQUFZLGVBQVosRUFBNkIsUUFBN0I7SUFEYzs7dUJBR2hCLG1CQUFBLEdBQXFCLFNBQUMsUUFBRDthQUNuQixJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxxQkFBWixFQUFtQyxRQUFuQztJQURtQjs7dUJBR3JCLGVBQUEsR0FBaUIsU0FBQyxRQUFEO2FBQ2YsSUFBQyxDQUFBLE9BQU8sQ0FBQyxFQUFULENBQVksaUJBQVosRUFBK0IsUUFBL0I7SUFEZTs7dUJBR2pCLFlBQUEsR0FBYyxTQUFDLFFBQUQ7YUFDWixJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxhQUFaLEVBQTJCLFFBQTNCO0lBRFk7O3VCQUdkLGlCQUFBLEdBQW1CLFNBQUMsUUFBRDthQUNqQixJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxtQkFBWixFQUFpQyxRQUFqQztJQURpQjs7dUJBR25CLGFBQUEsR0FBZSxTQUFDLFFBQUQ7YUFDYixJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxjQUFaLEVBQTRCLFFBQTVCO0lBRGE7O3VCQUdmLFlBQUEsR0FBYyxTQUFBO01BQ1osSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsQ0FBRDtVQUVqQyxJQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQW5CLENBQTRCLFNBQTVCLENBQVY7QUFBQSxtQkFBQTs7VUFFQSxJQUFBLENBQUEsQ0FBd0IsQ0FBQyxDQUFDLFFBQUYsSUFBYyxDQUFDLENBQUMsT0FBaEIsSUFBMkIsQ0FBQyxDQUFDLE9BQXJELENBQUE7bUJBQUEsS0FBQyxDQUFBLFlBQUQsQ0FBYyxDQUFkLEVBQUE7O1FBSmlDO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFuQztNQUtBLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsV0FBMUIsRUFBdUMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLENBQUQ7aUJBQU8sS0FBQyxDQUFBLFdBQUQsQ0FBYSxDQUFiO1FBQVA7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXZDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixTQUExQixFQUFxQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsQ0FBRDtpQkFBTyxLQUFDLENBQUEsU0FBRCxDQUFXLENBQVg7UUFBUDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBckM7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLGdCQUFULENBQTBCLFdBQTFCLEVBQXVDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxDQUFEO2lCQUFPLEtBQUMsQ0FBQSxXQUFELENBQWEsQ0FBYjtRQUFQO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF2QztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsV0FBMUIsRUFBdUMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLENBQUQ7aUJBQU8sS0FBQyxDQUFBLFdBQUQsQ0FBYSxDQUFiO1FBQVA7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXZDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixXQUExQixFQUF1QyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsQ0FBRDtpQkFBTyxLQUFDLENBQUEsV0FBRCxDQUFhLENBQWI7UUFBUDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBdkM7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLGdCQUFULENBQTBCLFVBQTFCLEVBQXNDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxDQUFEO2lCQUFPLEtBQUMsQ0FBQSxVQUFELENBQVksQ0FBWjtRQUFQO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF0QztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsTUFBMUIsRUFBa0MsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLENBQUQ7aUJBQU8sS0FBQyxDQUFBLE1BQUQsQ0FBUSxDQUFSO1FBQVA7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWxDO01BRUEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLElBQUMsQ0FBQSxPQUFuQixFQUNDO1FBQUEsY0FBQSxFQUFnQixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFDLENBQUQ7bUJBQU8sS0FBQyxDQUFBLE1BQUQsQ0FBUSxDQUFSO1VBQVA7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWhCO1FBQ0EsZ0JBQUEsRUFBa0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQyxDQUFEO21CQUFPLEtBQUMsQ0FBQSxRQUFELENBQVUsQ0FBVjtVQUFQO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQURsQjtRQUVBLGNBQUEsRUFBZ0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsTUFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBRmhCO1FBR0EsZ0JBQUEsRUFBa0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsUUFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBSGxCO1FBSUEsa0JBQUEsRUFBb0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsV0FBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBSnBCO1FBS0EscUJBQUEsRUFBdUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsY0FBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBTHZCO1FBTUEsdUJBQUEsRUFBeUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsaUJBQUQsQ0FBbUI7Y0FBQSxPQUFBLEVBQVMsSUFBVDthQUFuQixFQUFrQyxJQUFsQztVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQU56QjtRQU9BLHNDQUFBLEVBQXdDLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLGVBQUQsQ0FBaUIsSUFBakI7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FQeEM7UUFRQSw4QkFBQSxFQUFnQyxDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxpQkFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBUmhDO1FBU0Esd0NBQUEsRUFBMEMsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsaUJBQUQsQ0FBbUIsSUFBbkI7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FUMUM7UUFVQSx3QkFBQSxFQUEwQixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxpQkFBRCxDQUFtQixJQUFuQixFQUF5QixJQUF6QjtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQVYxQjtRQVdBLCtCQUFBLEVBQWlDLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLGlCQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FYakM7UUFZQSxxQ0FBQSxFQUF1QyxDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxzQkFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBWnZDO1FBYUEsb0NBQUEsRUFBc0MsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEscUJBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQWJ0QztRQWNBLGtDQUFBLEVBQW9DLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLG1CQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FkcEM7UUFlQSxvQ0FBQSxFQUFzQyxDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxxQkFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBZnRDO1FBZ0JBLGdCQUFBLEVBQWtCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLGlCQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FoQmxCO1FBaUJBLGdCQUFBLEVBQWtCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLG1CQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FqQmxCO1FBa0JBLGVBQUEsRUFBaUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsa0JBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQWxCakI7UUFtQkEsaUJBQUEsRUFBbUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsWUFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBbkJuQjtRQW9CQSwwQkFBQSxFQUE0QixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxxQkFBRCxDQUF1QixLQUF2QjtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQXBCNUI7UUFxQkEsZ0NBQUEsRUFBa0MsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsOEJBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQXJCbEM7UUFzQkEsOEJBQUEsRUFBZ0MsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsNEJBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQXRCaEM7UUF1QkEsNkJBQUEsRUFBK0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEscUJBQUQsQ0FBdUIsSUFBdkI7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0F2Qi9CO1FBd0JBLG1CQUFBLEVBQXFCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQXhCckI7UUF5QkEsb0NBQUEsRUFBc0MsU0FBQTtpQkFBRyxZQUFBLENBQWEsK0JBQWI7UUFBSCxDQXpCdEM7UUEwQkEsZ0NBQUEsRUFBa0MsU0FBQTtpQkFBRyxZQUFBLENBQWEsNEJBQWI7UUFBSCxDQTFCbEM7UUEyQkEsaUNBQUEsRUFBbUMsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQyxDQUFEO21CQUFPLEtBQUMsQ0FBQSxtQkFBRCxDQUFxQixDQUFyQjtVQUFQO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQTNCbkM7T0FERDtNQThCQSwyQkFBTSxDQUFDLE9BQVAsQ0FBZSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsS0FBRDtpQkFDYixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsS0FBQyxDQUFBLE9BQW5CLEVBQTRCLHdDQUFBLEdBQXdDLENBQUMsS0FBQSxHQUFRLENBQVQsQ0FBcEUsRUFBa0YsU0FBQTttQkFDaEYsS0FBQyxDQUFBLHVCQUFELENBQXlCLEtBQXpCO1VBRGdGLENBQWxGO1FBRGE7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWY7TUFJQSxJQUFDLENBQUEsV0FBVyxDQUFDLEdBQWIsQ0FBaUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFmLENBQUEsQ0FBMEIsQ0FBQyx5QkFBM0IsQ0FBcUQsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO1VBQ3BFLEtBQUMsQ0FBQSxnQkFBRCxDQUFBO1VBQ0EsSUFBa0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLHNCQUFoQixDQUFsRDttQkFBQSxLQUFDLENBQUEsZ0JBQUQsQ0FBa0I7Y0FBQyxJQUFBLEVBQU0sS0FBUDtjQUFjLEtBQUEsRUFBTyxLQUFyQjthQUFsQixFQUFBOztRQUZvRTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBckQsQ0FBakI7TUFHQSxJQUFDLENBQUEsV0FBVyxDQUFDLEdBQWIsQ0FBaUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBYixDQUE4QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQzdDLEtBQUMsQ0FBQSxXQUFELENBQUE7UUFENkM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTlCLENBQWpCO01BRUEsSUFBQyxDQUFBLFdBQVcsQ0FBQyxHQUFiLENBQWlCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBWixDQUF3QiwrQkFBeEIsRUFBeUQsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUN4RSxLQUFDLENBQUEsV0FBRCxDQUFBO1FBRHdFO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6RCxDQUFqQjtNQUVBLElBQUMsQ0FBQSxXQUFXLENBQUMsR0FBYixDQUFpQixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVosQ0FBd0IsNEJBQXhCLEVBQXNELENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFDckUsS0FBQyxDQUFBLFdBQUQsQ0FBQTtRQURxRTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBdEQsQ0FBakI7TUFFQSxJQUFDLENBQUEsV0FBVyxDQUFDLEdBQWIsQ0FBaUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFaLENBQXdCLG1CQUF4QixFQUE2QyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDNUQsSUFBa0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLDRCQUFoQixDQUFsQjttQkFBQSxLQUFDLENBQUEsV0FBRCxDQUFBLEVBQUE7O1FBRDREO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE3QyxDQUFqQjtNQUVBLElBQUMsQ0FBQSxXQUFXLENBQUMsR0FBYixDQUFpQixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVosQ0FBd0Isa0NBQXhCLEVBQTRELENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFDM0UsS0FBQyxDQUFBLFdBQUQsQ0FBQTtRQUQyRTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBNUQsQ0FBakI7YUFFQSxJQUFDLENBQUEsV0FBVyxDQUFDLEdBQWIsQ0FBaUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFaLENBQXdCLGdDQUF4QixFQUEwRCxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQ3pFLEtBQUMsQ0FBQSxXQUFELENBQUE7UUFEeUU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTFELENBQWpCO0lBN0RZOzt1QkFnRWQsTUFBQSxHQUFRLFNBQUE7YUFDTixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQWYsQ0FBc0IsSUFBdEI7SUFETTs7dUJBR1IsSUFBQSxHQUFNLFNBQUMsS0FBRDthQUNKLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBZixDQUFvQixJQUFwQixFQUEwQjtRQUN4QixjQUFBLEVBQWdCLElBRFE7UUFFeEIsWUFBQSxFQUFjLEtBRlU7UUFHeEIsWUFBQSxFQUFjLEtBSFU7T0FBMUIsQ0FJRSxDQUFDLElBSkgsQ0FJUSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDTixJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFmLENBQW1DLEtBQUMsQ0FBQSxNQUFELENBQUEsQ0FBbkMsQ0FBNkMsQ0FBQyxJQUE5QyxDQUFBO1VBQ0EsSUFBWSxLQUFaO21CQUFBLEtBQUMsQ0FBQSxLQUFELENBQUEsRUFBQTs7UUFGTTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FKUjtJQURJOzt1QkFTTixJQUFBLEdBQU0sU0FBQTthQUNKLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBZixDQUFvQixJQUFwQjtJQURJOzt1QkFHTixLQUFBLEdBQU8sU0FBQTthQUNMLElBQUMsQ0FBQSxPQUFPLENBQUMsS0FBVCxDQUFBO0lBREs7O3VCQUdQLE9BQUEsR0FBUyxTQUFBO2FBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFmLENBQUEsQ0FBMEIsQ0FBQyxRQUEzQixDQUFBO0lBRE87O3VCQUdULFFBQUEsR0FBVSxTQUFBO2FBQ1IsUUFBUSxDQUFDLGFBQVQsS0FBMEIsSUFBQyxDQUFBO0lBRG5COzt1QkFHVixXQUFBLEdBQWEsU0FBQTtNQUNYLElBQUcsSUFBQyxDQUFBLFFBQUQsQ0FBQSxDQUFIO2VBQ0UsSUFBQyxDQUFBLE9BQUQsQ0FBQSxFQURGO09BQUEsTUFBQTtlQUdFLElBQUMsQ0FBQSxJQUFELENBQU0sSUFBTixFQUhGOztJQURXOzt1QkFNYixZQUFBLEdBQWMsU0FBQyxDQUFEO0FBQ1osVUFBQTtNQUFBLElBQUcsS0FBQSxHQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBVCxDQUFpQixRQUFqQixDQUFYO1FBQ0UsV0FBQSxHQUFjLENBQUMsQ0FBQyxNQUFGLElBQVk7UUFDMUIsSUFBQyxDQUFBLFdBQUQsQ0FBYSxLQUFiO1FBQ0EsSUFBRyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQWhCLENBQXlCLFdBQXpCLENBQUg7aUJBQ0UsS0FBSyxDQUFDLGVBQU4sQ0FBc0IsV0FBdEIsRUFERjtTQUFBLE1BRUssSUFBRyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQWhCLENBQXlCLE1BQXpCLENBQUg7aUJBQ0gsSUFBQyxDQUFBLG9CQUFELENBQXNCLENBQXRCLEVBREc7U0FMUDs7SUFEWTs7dUJBU2Qsb0JBQUEsR0FBc0IsU0FBQyxDQUFEO0FBQ3BCLFVBQUE7TUFBQSxRQUFBLEdBQVcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFULENBQWlCLFFBQWpCLENBQTBCLENBQUMsT0FBM0IsQ0FBQTtNQUNYLE1BQUEsc0NBQW9CO01BQ3BCLGtCQUFBLEdBQXFCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQiw4QkFBaEI7TUFDckIsSUFBRyxNQUFBLEtBQVUsQ0FBYjtRQUNFLElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLDRCQUFoQixDQUFIO1VBQ0UsV0FBQSxHQUFjLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBZixDQUFvQixRQUFwQixFQUE4QjtZQUFBLE9BQUEsRUFBUyxJQUFUO1lBQWUsWUFBQSxFQUFjLEtBQTdCO1lBQW9DLGNBQUEsRUFBZ0Isa0JBQXBEO1dBQTlCO1VBQ2QsSUFBQyxDQUFBLGdCQUFnQixDQUFDLEdBQWxCLENBQXNCLFFBQXRCLEVBQWdDLFdBQWhDO2lCQUNBLFdBQVcsQ0FBQyxJQUFaLENBQWlCLENBQUEsU0FBQSxLQUFBO21CQUFBLFNBQUE7cUJBQUcsS0FBQyxDQUFBLGdCQUFnQixFQUFDLE1BQUQsRUFBakIsQ0FBeUIsUUFBekI7WUFBSDtVQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBakIsRUFIRjtTQURGO09BQUEsTUFLSyxJQUFHLE1BQUEsS0FBVSxDQUFiO2VBQ0gsSUFBQyxDQUFBLGdCQUFELENBQWtCLFFBQWxCLEVBQTRCO1VBQUEsY0FBQSxFQUFnQixrQkFBaEI7U0FBNUIsRUFERzs7SUFUZTs7dUJBWXRCLGdCQUFBLEdBQWtCLFNBQUMsR0FBRCxFQUFNLE9BQU47QUFDaEIsVUFBQTtNQUFBLElBQUcsT0FBQSxHQUFVLElBQUMsQ0FBQSxnQkFBZ0IsQ0FBQyxHQUFsQixDQUFzQixHQUF0QixDQUFiO2VBQ0UsT0FBTyxDQUFDLElBQVIsQ0FBYSxTQUFBO2lCQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBZixDQUFvQixHQUFwQixFQUF5QixPQUF6QjtRQUFILENBQWIsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQWYsQ0FBb0IsR0FBcEIsRUFBeUIsT0FBekIsRUFIRjs7SUFEZ0I7O3VCQU1sQixXQUFBLEdBQWEsU0FBQyxlQUFEO0FBQ1gsVUFBQTs7UUFEWSxrQkFBZ0I7O01BQzVCLGFBQUEsR0FBZ0IsSUFBQyxDQUFBLGFBQUQsQ0FBQTtNQUVoQixrQkFBQSxHQUFxQjtBQUNyQjtBQUFBLFdBQUEsc0NBQUE7O1FBQ0Usa0JBQW1CLENBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFmLENBQW5CLEdBQTBDLElBQUksQ0FBQyxTQUFTLENBQUMsdUJBQWYsQ0FBQTtRQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQWYsQ0FBQTtRQUNBLElBQUksQ0FBQyxNQUFMLENBQUE7QUFIRjtNQUtBLElBQUMsQ0FBQSxLQUFELEdBQVM7TUFFVCxZQUFBLEdBQWUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFiLENBQUE7TUFDZixJQUFHLFlBQVksQ0FBQyxNQUFiLEdBQXNCLENBQXpCO1FBQ0UsSUFBQSxDQUFtQyxJQUFDLENBQUEsT0FBTyxDQUFDLGFBQVQsQ0FBdUIsZ0JBQXZCLENBQW5DO1VBQUEsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLElBQUMsQ0FBQSxJQUF0QixFQUFBOztRQUVBLHNCQUFBLEdBQXlCLElBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxDQUF1QixvQkFBdkI7UUFDekIsSUFBZ0Qsc0JBQWhEO1VBQUEsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLHNCQUFyQixFQUFBOzs7VUFFQSxlQUFnQixPQUFBLENBQVEsaUJBQVI7O1FBRWhCLElBQUMsQ0FBQSxLQUFEOztBQUFTO2VBQUEsZ0RBQUE7O1lBQ1AsS0FBQSxHQUFRLEVBQUUsQ0FBQyxvQkFBSCxDQUF3QixXQUF4QjtZQUNSLElBQUEsQ0FBZ0IsS0FBaEI7QUFBQSx1QkFBQTs7WUFDQSxLQUFBLEdBQVEsQ0FBQyxDQUFDLElBQUYsVUFBTyxDQUFBLEtBQU8sU0FBQSxXQUFBLENBQUMsQ0FBQyxJQUFGLENBQU8sS0FBUCxDQUFBLENBQUEsQ0FBZDtBQUNSO0FBQUEsaUJBQUEsd0NBQUE7O2NBQ0UsS0FBTSxDQUFBLEdBQUEsQ0FBTixHQUFhLEtBQU0sQ0FBQSxHQUFBLENBQUksQ0FBQyxPQUFYLENBQUE7QUFEZjtZQUdBLFNBQUEsR0FBWSxJQUFJLFNBQUosQ0FBYztjQUN4QixJQUFBLEVBQU0sSUFBSSxDQUFDLFFBQUwsQ0FBYyxXQUFkLENBRGtCO2NBRXhCLFFBQUEsRUFBVSxXQUZjO2NBR3hCLE9BQUEsRUFBUyxLQUhlO2NBSXhCLE1BQUEsRUFBUSxJQUpnQjtjQUt4QixjQUFBLG1IQUVnQjtnQkFBQyxVQUFBLEVBQVksSUFBYjtlQVBRO2NBUXhCLFlBQUEsRUFBYyxJQUFJLFlBQUosQ0FBQSxDQVJVO2NBU3ZCLFdBQUQsSUFBQyxDQUFBLFNBVHVCO2NBVXhCLE9BQUEsS0FWd0I7YUFBZDtZQVlaLElBQUEsR0FBTyxJQUFJLGFBQUEsQ0FBYyxTQUFkLENBQXdCLENBQUM7WUFDcEMsSUFBQyxDQUFBLElBQUksQ0FBQyxXQUFOLENBQWtCLElBQWxCO3lCQUNBO0FBckJPOzs7QUF3QlQ7YUFBQSxpREFBQTs7dUJBQUEsSUFBQyxDQUFBLHFCQUFELENBQXVCLElBQUMsQ0FBQSxZQUFELENBQWMsWUFBZCxDQUF2QjtBQUFBO3VCQWhDRjtPQUFBLE1BQUE7UUFrQ0UsSUFBK0IsSUFBQyxDQUFBLE9BQU8sQ0FBQyxhQUFULENBQXVCLGlCQUF2QixDQUEvQjtVQUFBLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixJQUFDLENBQUEsSUFBdEIsRUFBQTs7UUFDQSxJQUFBLENBQTJELElBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxDQUF1QixvQkFBdkIsQ0FBM0Q7aUJBQUEsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLElBQUksZUFBQSxDQUFBLENBQWlCLENBQUMsT0FBM0MsRUFBQTtTQW5DRjs7SUFaVzs7dUJBaURiLGFBQUEsR0FBZSxTQUFBO0FBQUcsVUFBQTt3SEFBOEMsQ0FBRTtJQUFuRDs7dUJBRWYsZ0JBQUEsR0FBa0IsU0FBQTtBQUNoQixVQUFBO01BQUEsY0FBQSxHQUFpQixJQUFDLENBQUEsYUFBRCxDQUFBO01BQ2pCLElBQUcsSUFBQyxDQUFBLFlBQUQsQ0FBYyxjQUFkLENBQUg7ZUFDRSxJQUFDLENBQUEsa0JBQUQsQ0FBb0IsY0FBcEIsRUFERjtPQUFBLE1BQUE7ZUFJRSxJQUFDLENBQUEsUUFBRCxDQUFBLEVBSkY7O0lBRmdCOzt1QkFRbEIsZ0JBQUEsR0FBa0IsU0FBQyxPQUFEO0FBQ2hCLFVBQUE7O1FBRGlCLFVBQVU7O01BQzNCLElBQUEsQ0FBZ0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFiLENBQUEsQ0FBdUIsQ0FBQyxNQUF4RDtBQUFBLGVBQU8sT0FBTyxDQUFDLE9BQVIsQ0FBQSxFQUFQOztNQUVDLG1CQUFELEVBQU87O1FBRVAsUUFBUyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IseUJBQWhCOztNQUNULE9BQUEsR0FBYSxJQUFBLElBQVEsS0FBWCxHQUFzQixJQUFDLENBQUEsSUFBRCxDQUFNLEtBQU4sQ0FBdEIsR0FBd0MsT0FBTyxDQUFDLE9BQVIsQ0FBQTthQUNsRCxPQUFPLENBQUMsSUFBUixDQUFhLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtBQUNYLGNBQUE7VUFBQSxJQUFBLENBQWMsQ0FBQSxjQUFBLEdBQWlCLEtBQUMsQ0FBQSxhQUFELENBQUEsQ0FBakIsQ0FBZDtBQUFBLG1CQUFBOztVQUVBLE9BQTJCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYixDQUE0QixjQUE1QixDQUEzQixFQUFDLGtCQUFELEVBQVc7VUFDWCxJQUFjLGdCQUFkO0FBQUEsbUJBQUE7O1VBRUEsb0JBQUEsR0FBdUIsWUFBWSxDQUFDLEtBQWIsQ0FBbUIsSUFBSSxDQUFDLEdBQXhCO1VBRXZCLG9CQUFvQixDQUFDLE9BQXJCLENBQTZCLFFBQVEsQ0FBQyxNQUFULENBQWdCLFFBQVEsQ0FBQyxXQUFULENBQXFCLElBQUksQ0FBQyxHQUExQixDQUFBLEdBQWlDLENBQWpELENBQTdCO1VBRUEsV0FBQSxHQUFjLFFBQVEsQ0FBQyxNQUFULENBQWdCLENBQWhCLEVBQW1CLFFBQVEsQ0FBQyxXQUFULENBQXFCLElBQUksQ0FBQyxHQUExQixDQUFuQjtBQUNkO2VBQUEsc0RBQUE7O1lBQ0UsV0FBQSxJQUFlLElBQUksQ0FBQyxHQUFMLEdBQVc7WUFDMUIsS0FBQSxHQUFRLEtBQUMsQ0FBQSxZQUFELENBQWMsV0FBZDtZQUNSLElBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxRQUFoQixDQUF5QixXQUF6QixDQUFIOzJCQUNFLEtBQUssQ0FBQyxNQUFOLENBQUEsR0FERjthQUFBLE1BQUE7Y0FHRSxLQUFDLENBQUEsV0FBRCxDQUFhLEtBQWI7MkJBQ0EsS0FBQyxDQUFBLGFBQUQsQ0FBZSxLQUFmLEdBSkY7O0FBSEY7O1FBWFc7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWI7SUFQZ0I7O3VCQTJCbEIscUJBQUEsR0FBdUIsU0FBQyxZQUFEO0FBQ3JCLFVBQUE7O1FBRHNCLGVBQWU7O01BQ3JDLElBQUcsVUFBQSxHQUFhLElBQUMsQ0FBQSxZQUFqQjtRQUNFLElBQW9ELFlBQXBEO1VBQUEsVUFBQSxHQUFhLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBYixDQUF3QixVQUF4QixFQUFiOztlQUNBLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBZixDQUFxQixVQUFyQixFQUZGOztJQURxQjs7dUJBS3ZCLFlBQUEsR0FBYyxTQUFDLFNBQUQ7QUFDWixVQUFBO01BQUEsY0FBQSxHQUFpQjtNQUNqQixlQUFBLEdBQWtCO0FBRWxCO0FBQUEsV0FBQSxzQ0FBQTs7UUFDRSxJQUFHLEtBQUssQ0FBQyxXQUFOLENBQWtCLFNBQWxCLENBQUg7QUFDRSxpQkFBTyxNQURUOztRQUdBLFdBQUEsR0FBYyxLQUFLLENBQUMsT0FBTixDQUFBLENBQWUsQ0FBQztRQUM5Qiw0Q0FBa0IsQ0FBRSxRQUFqQixDQUEwQixTQUExQixXQUFBLElBQXlDLFdBQUEsR0FBYyxlQUExRDtVQUNFLGNBQUEsR0FBaUI7VUFDakIsZUFBQSxHQUFrQixZQUZwQjs7QUFMRjthQVNBO0lBYlk7O3VCQWVkLGtCQUFBLEdBQW9CLFNBQUMsU0FBRDthQUNsQixJQUFDLENBQUEsV0FBRCxDQUFhLElBQUMsQ0FBQSxZQUFELENBQWMsU0FBZCxDQUFiO0lBRGtCOzt1QkFHcEIsUUFBQSxHQUFVLFNBQUMsS0FBRDtBQUNSLFVBQUE7O1FBQUEsS0FBSyxDQUFFLHdCQUFQLENBQUE7O01BQ0EsYUFBQSxHQUFnQixJQUFDLENBQUEsYUFBRCxDQUFBO01BQ2hCLElBQUcscUJBQUg7UUFDRSxJQUFHLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBeEIsQ0FBaUMsV0FBakMsQ0FBSDtVQUNFLElBQUcsSUFBQyxDQUFBLFdBQUQsQ0FBYSxhQUFhLENBQUMsT0FBTyxDQUFDLFFBQVMsQ0FBQSxDQUFBLENBQTVDLENBQUg7WUFDRSxJQUFDLENBQUEsYUFBRCxDQUFlLElBQUMsQ0FBQSxhQUFELENBQUEsQ0FBZixFQUFpQyxLQUFqQztBQUNBLG1CQUZGO1dBREY7O1FBS0EsSUFBRyxTQUFBLEdBQVksSUFBQyxDQUFBLFNBQUQsQ0FBVyxhQUFYLENBQWY7VUFDRSxJQUFDLENBQUEsV0FBRCxDQUFhLFNBQWIsRUFERjtTQU5GO09BQUEsTUFBQTtRQVNFLElBQUMsQ0FBQSxXQUFELENBQWEsSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLENBQXBCLEVBVEY7O2FBV0EsSUFBQyxDQUFBLGFBQUQsQ0FBZSxJQUFDLENBQUEsYUFBRCxDQUFBLENBQWYsRUFBaUMsS0FBakM7SUFkUTs7dUJBZ0JWLE1BQUEsR0FBUSxTQUFDLEtBQUQ7QUFDTixVQUFBO01BQUEsS0FBSyxDQUFDLHdCQUFOLENBQUE7TUFDQSxhQUFBLEdBQWdCLElBQUMsQ0FBQSxhQUFELENBQUE7TUFDaEIsSUFBRyxxQkFBSDtRQUNFLElBQUcsYUFBQSxHQUFnQixJQUFDLENBQUEsYUFBRCxDQUFlLGFBQWYsQ0FBbkI7VUFDRSxJQUFDLENBQUEsV0FBRCxDQUFhLGFBQWIsRUFERjtTQUFBLE1BQUE7VUFHRSxJQUFDLENBQUEsV0FBRCxDQUFhLGFBQWEsQ0FBQyxhQUFhLENBQUMsT0FBNUIsQ0FBb0MsWUFBcEMsQ0FBYixFQUhGO1NBREY7T0FBQSxNQUFBO1FBTUUsT0FBQSxHQUFVLElBQUMsQ0FBQSxJQUFJLENBQUMsZ0JBQU4sQ0FBdUIsUUFBdkI7UUFDVixJQUFDLENBQUEsV0FBRCxDQUFhLE9BQVEsQ0FBQSxPQUFPLENBQUMsTUFBUixHQUFpQixDQUFqQixDQUFyQixFQVBGOzthQVNBLElBQUMsQ0FBQSxhQUFELENBQWUsSUFBQyxDQUFBLGFBQUQsQ0FBQSxDQUFmLEVBQWlDLEtBQWpDO0lBWk07O3VCQWNSLFNBQUEsR0FBVyxTQUFDLEtBQUQ7QUFDVCxVQUFBO01BQUEsWUFBQSxHQUFlO0FBQ2YsYUFBTSxvQkFBTjtRQUNFLElBQUcsZ0NBQUg7VUFDRSxZQUFBLEdBQWUsWUFBWSxDQUFDO1VBQzVCLElBQUcsWUFBWSxDQUFDLE9BQWIsQ0FBcUIsUUFBckIsQ0FBSDtBQUNFLG1CQUFPLGFBRFQ7V0FGRjtTQUFBLE1BQUE7VUFLRSxZQUFBLEdBQWUsWUFBWSxDQUFDLGFBQWEsQ0FBQyxPQUEzQixDQUFtQyxZQUFuQyxFQUxqQjs7TUFERjtBQVFBLGFBQU87SUFWRTs7dUJBWVgsYUFBQSxHQUFlLFNBQUMsS0FBRDtBQUNiLFVBQUE7TUFBQSxhQUFBLEdBQWdCLEtBQUssQ0FBQztBQUN0QixhQUFNLHVCQUFBLElBQW1CLENBQUksYUFBYSxDQUFDLE9BQWQsQ0FBc0IsUUFBdEIsQ0FBN0I7UUFDRSxhQUFBLEdBQWdCLGFBQWEsQ0FBQztNQURoQztNQUdBLElBQW1CLHFCQUFuQjtBQUFBLGVBQU8sS0FBUDs7TUFLQSxJQUFHLGFBQWEsQ0FBQyxPQUFkLENBQXNCLHFCQUF0QixDQUFIO1FBQ0UsT0FBQSxHQUFVLGFBQWEsQ0FBQyxnQkFBZCxDQUErQixRQUEvQjtRQUNWLElBQXNDLE9BQU8sQ0FBQyxNQUFSLEdBQWlCLENBQXZEO0FBQUEsaUJBQU8sT0FBUSxDQUFBLE9BQU8sQ0FBQyxNQUFSLEdBQWlCLENBQWpCLEVBQWY7U0FGRjs7QUFJQSxhQUFPO0lBZE07O3VCQWdCZixlQUFBLEdBQWlCLFNBQUMsV0FBRDtBQUNmLFVBQUE7O1FBRGdCLGNBQVk7O01BQzVCLGFBQUEsR0FBZ0IsSUFBQyxDQUFBLGFBQUQsQ0FBQTtNQUNoQixJQUFjLHFCQUFkO0FBQUEsZUFBQTs7TUFFQSxTQUFBLEdBQVksYUFBYSxDQUFDLE9BQWQsQ0FBc0IsWUFBdEI7TUFDWixJQUFHLFdBQUEsS0FBZSxLQUFmLElBQXlCLFNBQVMsQ0FBQyxVQUF0QztRQUVFLElBQWUsU0FBUyxDQUFDLFNBQVMsQ0FBQyxVQUFwQixDQUFBLENBQWdDLENBQUMsTUFBakMsR0FBMEMsQ0FBekQ7aUJBQUEsSUFBQyxDQUFBLFFBQUQsQ0FBQSxFQUFBO1NBRkY7T0FBQSxNQUFBO2VBSUUsU0FBUyxDQUFDLE1BQVYsQ0FBaUIsV0FBakIsRUFKRjs7SUFMZTs7dUJBV2pCLGlCQUFBLEdBQW1CLFNBQUMsV0FBRCxFQUFvQixjQUFwQjtBQUNqQixVQUFBOztRQURrQixjQUFZOzs7UUFBTyxpQkFBZTs7TUFDcEQsSUFBRyxjQUFIO0FBQ0U7QUFBQSxhQUFBLHNDQUFBOztVQUFBLElBQUksQ0FBQyxRQUFMLENBQWMsSUFBZDtBQUFBO0FBQ0EsZUFGRjs7TUFJQSxhQUFBLEdBQWdCLElBQUMsQ0FBQSxhQUFELENBQUE7TUFDaEIsSUFBYyxxQkFBZDtBQUFBLGVBQUE7O01BRUEsSUFBRyxTQUFBLEdBQVksYUFBYSxDQUFDLE9BQWQsQ0FBc0IscUJBQXRCLENBQWY7UUFDRSxTQUFTLENBQUMsUUFBVixDQUFtQixXQUFuQjtlQUNBLElBQUMsQ0FBQSxXQUFELENBQWEsU0FBYixFQUZGOztJQVJpQjs7dUJBWW5CLGlCQUFBLEdBQW1CLFNBQUMsT0FBRCxFQUFhLGVBQWI7QUFDakIsVUFBQTs7UUFEa0IsVUFBUTs7O1FBQUksa0JBQWdCOztNQUM5QyxhQUFBLEdBQWdCLElBQUMsQ0FBQSxhQUFELENBQUE7TUFDaEIsSUFBYyxxQkFBZDtBQUFBLGVBQUE7O01BRUEsSUFBRyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQXhCLENBQWlDLFdBQWpDLENBQUg7UUFDRSxJQUFHLGVBQUg7aUJBQ0UsSUFBQyxDQUFBLGVBQUQsQ0FBaUIsS0FBakIsRUFERjtTQUFBLE1BQUE7aUJBR0UsYUFBYSxDQUFDLGVBQWQsQ0FBQSxFQUhGO1NBREY7T0FBQSxNQUtLLElBQUcsYUFBYSxDQUFDLFNBQVMsQ0FBQyxRQUF4QixDQUFpQyxNQUFqQyxDQUFIO1FBQ0gsSUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsOEJBQWhCLENBQUg7VUFDRSxPQUFBLEdBQVUsTUFBTSxDQUFDLE1BQVAsQ0FBYztZQUFBLGNBQUEsRUFBZ0IsSUFBaEI7V0FBZCxFQUFvQyxPQUFwQyxFQURaOztlQUVBLElBQUMsQ0FBQSxnQkFBRCxDQUFrQixhQUFhLENBQUMsT0FBZCxDQUFBLENBQWxCLEVBQTJDLE9BQTNDLEVBSEc7O0lBVFk7O3VCQWNuQixzQkFBQSxHQUF3QixTQUFDLFdBQUQsRUFBYyxJQUFkO0FBQ3RCLFVBQUE7TUFBQSxhQUFBLEdBQWdCLElBQUMsQ0FBQSxhQUFELENBQUE7TUFDaEIsSUFBYyxxQkFBZDtBQUFBLGVBQUE7O01BRUEsSUFBQSxHQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBZixDQUFBLENBQTBCLENBQUMsYUFBM0IsQ0FBQTtNQUNQLElBQUcsSUFBQSxJQUFTLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBeEIsQ0FBaUMsTUFBakMsQ0FBWjtRQUNFLElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFmLENBQUEsQ0FBMEIsQ0FBQyxpQkFBM0IsQ0FBQSxDQUFIO1VBQ0UsS0FBQSxHQUFRLElBQUksQ0FBQyxLQUFMLENBQVcsV0FBWCxFQUF3QixJQUF4QjtpQkFDUixJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWYsQ0FBNkIsYUFBYSxDQUFDLE9BQWQsQ0FBQSxDQUE3QixFQUFzRCxLQUF0RCxFQUZGO1NBQUEsTUFBQTtpQkFJRSxJQUFDLENBQUEsaUJBQUQsQ0FBbUIsSUFBbkIsRUFKRjtTQURGOztJQUxzQjs7dUJBWXhCLHNCQUFBLEdBQXdCLFNBQUE7YUFDdEIsSUFBQyxDQUFBLHNCQUFELENBQXdCLFlBQXhCLEVBQXNDLE9BQXRDO0lBRHNCOzt1QkFHeEIscUJBQUEsR0FBdUIsU0FBQTthQUNyQixJQUFDLENBQUEsc0JBQUQsQ0FBd0IsWUFBeEIsRUFBc0MsUUFBdEM7SUFEcUI7O3VCQUd2QixtQkFBQSxHQUFxQixTQUFBO2FBQ25CLElBQUMsQ0FBQSxzQkFBRCxDQUF3QixVQUF4QixFQUFvQyxRQUFwQztJQURtQjs7dUJBR3JCLHFCQUFBLEdBQXVCLFNBQUE7YUFDckIsSUFBQyxDQUFBLHNCQUFELENBQXdCLFVBQXhCLEVBQW9DLE9BQXBDO0lBRHFCOzt1QkFHdkIsdUJBQUEsR0FBeUIsU0FBQyxLQUFEO0FBQ3ZCLFVBQUE7TUFBQSxhQUFBLEdBQWdCLElBQUMsQ0FBQSxhQUFELENBQUE7TUFDaEIsSUFBYyxxQkFBZDtBQUFBLGVBQUE7O01BRUEsSUFBQSxHQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBZixDQUFBLENBQTBCLENBQUMsUUFBM0IsQ0FBQSxDQUFzQyxDQUFBLEtBQUE7TUFDN0MsSUFBRyxJQUFBLElBQVMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxRQUF4QixDQUFpQyxNQUFqQyxDQUFaO2VBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFmLENBQTZCLGFBQWEsQ0FBQyxPQUFkLENBQUEsQ0FBN0IsRUFBc0QsSUFBdEQsRUFERjs7SUFMdUI7O3VCQVF6QixpQkFBQSxHQUFtQixTQUFBO0FBQ2pCLFVBQUE7TUFBQSxJQUFHLElBQUMsQ0FBQSxRQUFELENBQUEsQ0FBSDtRQUNFLEtBQUEsR0FBUSxJQUFDLENBQUEsYUFBRCxDQUFBO1FBQ1IsSUFBYyxlQUFKLElBQWMsYUFBUyxJQUFDLENBQUEsS0FBVixFQUFBLEtBQUEsTUFBeEI7QUFBQSxpQkFBQTs7UUFDQSxPQUFBLEdBQVUsS0FBSyxDQUFDLE9BQU4sQ0FBQSxFQUhaO09BQUEsTUFBQTtRQUtFLE9BQUEsR0FBVSxJQUFDLENBQUEsYUFBRCxDQUFBLEVBTFo7O01BT0EsSUFBRyxPQUFIO1FBQ0UsTUFBQSxHQUFTLElBQUksVUFBSixDQUFlLE9BQWYsRUFDUDtVQUFBLFFBQUEsRUFBVSxDQUFBLFNBQUEsS0FBQTttQkFBQSxTQUFDLEdBQUQ7QUFDUixrQkFBQTtjQURVLCtCQUFhO3FCQUN2QixLQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxpQkFBZCxFQUFpQztnQkFBQyxhQUFBLFdBQUQ7Z0JBQWMsU0FBQSxPQUFkO2VBQWpDO1lBRFE7VUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVY7VUFFQSxNQUFBLEVBQVEsQ0FBQSxTQUFBLEtBQUE7bUJBQUEsU0FBQyxHQUFEO0FBQ04sa0JBQUE7Y0FEUSwrQkFBYTtxQkFDckIsS0FBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsYUFBZCxFQUE2QjtnQkFBQyxhQUFBLFdBQUQ7Z0JBQWMsU0FBQSxPQUFkO2VBQTdCO1lBRE07VUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBRlI7VUFJQSxZQUFBLEVBQWMsQ0FBQSxTQUFBLEtBQUE7bUJBQUEsU0FBQyxHQUFEO0FBQ1osa0JBQUE7Y0FEYywrQkFBYTtxQkFDM0IsS0FBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsbUJBQWQsRUFBbUM7Z0JBQUMsYUFBQSxXQUFEO2dCQUFjLFNBQUEsT0FBZDtlQUFuQztZQURZO1VBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUpkO1NBRE87ZUFPVCxNQUFNLENBQUMsTUFBUCxDQUFBLEVBUkY7O0lBUmlCOzt1QkFrQm5CLDhCQUFBLEdBQWdDLFNBQUE7QUFDOUIsVUFBQTtNQUFBLElBQUEsQ0FBYyxDQUFBLFFBQUEsK0NBQTJCLENBQUUsT0FBbEIsQ0FBQSxVQUFYLENBQWQ7QUFBQSxlQUFBOztNQUVPLElBQUEsQ0FBTyxFQUFFLENBQUMsVUFBSCxDQUFjLFFBQWQsQ0FBUDtlQUNMLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBbkIsQ0FBOEIsaUJBQUEsR0FBa0IsUUFBbEIsR0FBMkIsTUFBM0IsR0FBZ0MsQ0FBQyxJQUFDLENBQUEsa0JBQUQsQ0FBQSxDQUFELENBQTlELEVBREs7O2FBR1AsS0FBSyxDQUFDLGdCQUFOLENBQXVCLFFBQXZCO0lBTjhCOzt1QkFRaEMsNEJBQUEsR0FBOEIsU0FBQTtBQUM1QixVQUFBO01BQUEsSUFBQSxDQUFjLENBQUEsUUFBQSwyRUFBMkQsQ0FBRSxPQUFsRCxDQUFBLFVBQVgsQ0FBZDtBQUFBLGVBQUE7O01BRU8sSUFBQSxDQUFPLEVBQUUsQ0FBQyxVQUFILENBQWMsUUFBZCxDQUFQO2VBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixpQkFBQSxHQUFrQixRQUFsQixHQUEyQixNQUEzQixHQUFnQyxDQUFDLElBQUMsQ0FBQSxrQkFBRCxDQUFBLENBQUQsQ0FBOUQsRUFESzs7YUFHUCxLQUFLLENBQUMsZ0JBQU4sQ0FBdUIsUUFBdkI7SUFONEI7O3VCQVE5QixrQkFBQSxHQUFvQixTQUFBO0FBQ2xCLGNBQU8sT0FBTyxDQUFDLFFBQWY7QUFBQSxhQUNPLFFBRFA7QUFFSSxpQkFBTztBQUZYLGFBR08sT0FIUDtBQUlJLGlCQUFPO0FBSlg7QUFNSSxpQkFBTztBQU5YO0lBRGtCOzt1QkFTcEIsNEJBQUEsR0FBOEIsU0FBQTtBQUM1QixVQUFBO01BQUEsSUFBRyxVQUFBLCtDQUE2QixDQUFFLE9BQWxCLENBQUEsVUFBaEI7ZUFDRSxJQUFJLENBQUMsSUFBTCxDQUFVO1VBQUMsV0FBQSxFQUFhLENBQUMsVUFBRCxDQUFkO1VBQTRCLFNBQUEsRUFBVyxJQUF2QztTQUFWLEVBREY7O0lBRDRCOzt1QkFJOUIsaUJBQUEsR0FBbUIsU0FBQTtBQUNqQixVQUFBO01BQUEsSUFBRyxJQUFDLENBQUEsUUFBRCxDQUFBLENBQUg7UUFDRSxLQUFBLEdBQVEsSUFBQyxDQUFBLGFBQUQsQ0FBQTtRQUNSLElBQVUsYUFBUyxJQUFDLENBQUEsS0FBVixFQUFBLEtBQUEsTUFBVjtBQUFBLGlCQUFBOztRQUNBLE9BQUEsbUJBQVUsS0FBSyxDQUFFLE9BQVAsQ0FBQSxXQUhaO09BQUEsTUFBQTtRQUtFLE9BQUEsR0FBVSxJQUFDLENBQUEsYUFBRCxDQUFBLEVBTFo7O01BTUEsSUFBQSxDQUFjLE9BQWQ7QUFBQSxlQUFBOztNQUVBLE1BQUEsR0FBUyxJQUFJLFVBQUosQ0FBZSxPQUFmLEVBQ1A7UUFBQSxNQUFBLEVBQVEsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQyxHQUFEO0FBQ04sZ0JBQUE7WUFEUSwrQkFBYTttQkFDckIsS0FBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsY0FBZCxFQUE4QjtjQUFDLGFBQUEsV0FBRDtjQUFjLFNBQUEsT0FBZDthQUE5QjtVQURNO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFSO09BRE87YUFHVCxNQUFNLENBQUMsTUFBUCxDQUFBO0lBWmlCOzt1QkFjbkIscUJBQUEsR0FBdUIsU0FBQTtBQUNyQixVQUFBO01BQUEsSUFBRyxJQUFDLENBQUEsUUFBRCxDQUFBLENBQUg7UUFDRSxhQUFBLEdBQWdCLElBQUMsQ0FBQSxhQUFELENBQUE7UUFDaEIsZUFBQSxHQUFrQixJQUFDLENBQUEsa0JBQUQsQ0FBQSxFQUZwQjtPQUFBLE1BR0ssSUFBRyxVQUFBLEdBQWEsSUFBQyxDQUFBLGFBQUQsQ0FBQSxDQUFoQjtRQUNILGFBQUEsR0FBZ0IsQ0FBQyxVQUFEO1FBQ2hCLGVBQUEsR0FBa0IsQ0FBQyxJQUFDLENBQUEsWUFBRCxDQUFjLFVBQWQsQ0FBRCxFQUZmOztNQUlMLElBQUEsQ0FBQSwwQkFBYyxhQUFhLENBQUUsZ0JBQWYsR0FBd0IsQ0FBdEMsQ0FBQTtBQUFBLGVBQUE7O0FBRUE7QUFBQSxXQUFBLHNDQUFBOztRQUNFLFdBQUcsSUFBSSxDQUFDLE9BQUwsQ0FBQSxDQUFBLEVBQUEsYUFBa0IsYUFBbEIsRUFBQSxJQUFBLE1BQUg7VUFDRSxJQUFJLENBQUMsT0FBTCxDQUFhO1lBQ1gsT0FBQSxFQUFTLHNCQUFBLEdBQXVCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBdEMsR0FBMkMscUJBRHpDO1lBRVgsT0FBQSxFQUFTLENBQUMsSUFBRCxDQUZFO1dBQWIsRUFHRyxTQUFBLEdBQUEsQ0FISDtBQUtBLGlCQU5GOztBQURGO2FBU0EsSUFBSSxDQUFDLE9BQUwsQ0FBYTtRQUNYLE9BQUEsRUFBUywrQ0FBQSxHQUErQyxDQUFJLGFBQWEsQ0FBQyxNQUFkLEdBQXVCLENBQTFCLEdBQWlDLE9BQWpDLEdBQThDLE1BQS9DLENBQS9DLEdBQXFHLEdBRG5HO1FBRVgsZUFBQSxFQUFpQixxQkFBQSxHQUFxQixDQUFDLGFBQWEsQ0FBQyxJQUFkLENBQW1CLElBQW5CLENBQUQsQ0FGM0I7UUFHWCxPQUFBLEVBQVMsQ0FBQyxlQUFELEVBQWtCLFFBQWxCLENBSEU7T0FBYixFQUlHLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxRQUFEO0FBQ0QsY0FBQTtVQUFBLElBQUcsUUFBQSxLQUFZLENBQWY7WUFDRSxlQUFBLEdBQWtCO0FBQ2xCLGlCQUFBLGlEQUFBOztjQUtFLElBQUEsQ0FBZ0IsRUFBRSxDQUFDLFVBQUgsQ0FBYyxZQUFkLENBQWhCO0FBQUEseUJBQUE7O2NBRUEsS0FBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsbUJBQWQsRUFBbUM7Z0JBQUMsWUFBQSxFQUFjLFlBQWY7ZUFBbkM7Y0FDQSxJQUFHLEtBQUssQ0FBQyxlQUFOLENBQXNCLFlBQXRCLENBQUg7Z0JBQ0UsS0FBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsZUFBZCxFQUErQjtrQkFBQyxZQUFBLEVBQWMsWUFBZjtpQkFBL0IsRUFERjtlQUFBLE1BQUE7Z0JBR0UsS0FBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMscUJBQWQsRUFBcUM7a0JBQUMsWUFBQSxFQUFjLFlBQWY7aUJBQXJDO2dCQUNBLGVBQWUsQ0FBQyxJQUFoQixDQUFxQixZQUFyQixFQUpGOztjQU1BLElBQUcsSUFBQSxHQUFPLFdBQUEsQ0FBWSxZQUFaLENBQVY7Z0JBQ0UsSUFBSSxDQUFDLGFBQUwsQ0FBbUIsWUFBbkIsRUFERjs7QUFkRjtZQWlCQSxJQUFHLGVBQWUsQ0FBQyxNQUFoQixHQUF5QixDQUE1QjtjQUNFLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBbkIsQ0FBNEIsS0FBQyxDQUFBLHlCQUFELENBQTJCLGVBQTNCLENBQTVCLEVBQ0U7Z0JBQUEsV0FBQSxFQUFhLEtBQUMsQ0FBQSx5QkFBRCxDQUFBLENBQWI7Z0JBQ0EsTUFBQSxFQUFRLEVBQUEsR0FBRSxDQUFDLGVBQWUsQ0FBQyxJQUFoQixDQUFxQixJQUFyQixDQUFELENBRFY7Z0JBRUEsV0FBQSxFQUFhLElBRmI7ZUFERixFQURGOztZQU9BLElBQUcsa0JBQUEsR0FBcUIsZUFBZ0IsQ0FBQSxDQUFBLENBQXhDO2NBQ0UsS0FBQyxDQUFBLFdBQUQsQ0FBYSxrQkFBa0IsQ0FBQyxPQUFuQixDQUEyQiwyQkFBM0IsQ0FBYixFQURGOztZQUVBLElBQWtCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQixnQ0FBaEIsQ0FBbEI7cUJBQUEsS0FBQyxDQUFBLFdBQUQsQ0FBQSxFQUFBO2FBNUJGOztRQURDO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUpIO0lBbkJxQjs7dUJBdUR2Qix5QkFBQSxHQUEyQixTQUFDLGVBQUQ7QUFDekIsVUFBQTtNQUFBLFFBQUEsR0FBYyxlQUFlLENBQUMsTUFBaEIsR0FBeUIsQ0FBNUIsR0FBbUMsT0FBbkMsR0FBZ0Q7YUFFM0QsZ0JBQUEsR0FBaUIsUUFBakIsR0FBMEI7SUFIRDs7dUJBSzNCLHlCQUFBLEdBQTJCLFNBQUE7QUFDekIsY0FBTyxPQUFPLENBQUMsUUFBZjtBQUFBLGFBQ08sT0FEUDtpQkFDb0I7QUFEcEIsYUFFTyxRQUZQO2lCQUVxQjtBQUZyQixhQUdPLE9BSFA7aUJBR29CO0FBSHBCO0lBRHlCOzt1QkFZM0IsbUJBQUEsR0FBcUIsU0FBQTtBQUNuQixVQUFBO01BQUEsYUFBQSxHQUFnQixJQUFDLENBQUEsYUFBRCxDQUFBO01BQ2hCLElBQUEsQ0FBQSxDQUFjLGFBQUEsSUFBa0IsYUFBYSxDQUFDLE1BQWQsR0FBdUIsQ0FBdkQsQ0FBQTtBQUFBLGVBQUE7O01BRUEsTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFwQixDQUErQixtQkFBL0I7YUFDQSxNQUFNLENBQUMsWUFBYSxDQUFBLG9CQUFBLENBQXBCLEdBQTRDLElBQUksQ0FBQyxTQUFMLENBQWUsYUFBZjtJQUx6Qjs7dUJBYXJCLGtCQUFBLEdBQW9CLFNBQUE7QUFDbEIsVUFBQTtNQUFBLGFBQUEsR0FBZ0IsSUFBQyxDQUFBLGFBQUQsQ0FBQTtNQUNoQixJQUFBLENBQUEsQ0FBYyxhQUFBLElBQWtCLGFBQWEsQ0FBQyxNQUFkLEdBQXVCLENBQXZELENBQUE7QUFBQSxlQUFBOztNQUVBLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBcEIsQ0FBK0Isb0JBQS9CO2FBQ0EsTUFBTSxDQUFDLFlBQWEsQ0FBQSxtQkFBQSxDQUFwQixHQUEyQyxJQUFJLENBQUMsU0FBTCxDQUFlLGFBQWY7SUFMekI7O3VCQVVwQixZQUFBLEdBQWMsU0FBQTtBQUNaLFVBQUE7TUFBQSxhQUFBLEdBQWdCLElBQUMsQ0FBQSxhQUFELENBQUE7TUFDaEIsSUFBQSxDQUFjLGFBQWQ7QUFBQSxlQUFBOztNQUVBLFFBQUEsR0FBYyxNQUFNLENBQUMsWUFBYSxDQUFBLG1CQUFBLENBQXZCLEdBQWlELElBQUksQ0FBQyxLQUFMLENBQVcsTUFBTSxDQUFDLFlBQWEsQ0FBQSxtQkFBQSxDQUEvQixDQUFqRCxHQUEyRztNQUN0SCxXQUFBLEdBQWlCLE1BQU0sQ0FBQyxZQUFhLENBQUEsb0JBQUEsQ0FBdkIsR0FBa0QsSUFBSSxDQUFDLEtBQUwsQ0FBVyxNQUFNLENBQUMsWUFBYSxDQUFBLG9CQUFBLENBQS9CLENBQWxELEdBQTZHO01BQzNILFlBQUEsR0FBZSxXQUFBLElBQWU7TUFDOUIsSUFBQSx5QkFBYyxZQUFZLENBQUUsZ0JBQTVCO0FBQUEsZUFBQTs7TUFFQSxnQkFBQSxHQUFtQixhQUFhLENBQUMsT0FBZCxDQUFBO01BQ25CLElBQXFELGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBeEIsQ0FBaUMsTUFBakMsQ0FBckQ7UUFBQSxnQkFBQSxHQUFtQixJQUFJLENBQUMsT0FBTCxDQUFhLGdCQUFiLEVBQW5COztBQUVBO1dBQUEsOENBQUE7O1FBQ0UsSUFBRyxFQUFFLENBQUMsVUFBSCxDQUFjLFdBQWQsQ0FBSDtVQUNFLElBQUcsV0FBSDt5QkFDRSxJQUFDLENBQUEsU0FBRCxDQUFXLFdBQVgsRUFBd0IsZ0JBQXhCLEdBREY7V0FBQSxNQUVLLElBQUcsUUFBSDtZQUNILElBQUEsQ0FBYSxJQUFDLENBQUEsU0FBRCxDQUFXLFdBQVgsRUFBd0IsZ0JBQXhCLENBQWI7QUFBQSxvQkFBQTthQUFBLE1BQUE7bUNBQUE7YUFERztXQUFBLE1BQUE7aUNBQUE7V0FIUDtTQUFBLE1BQUE7K0JBQUE7O0FBREY7O0lBWlk7O3VCQW1CZCxHQUFBLEdBQUssU0FBQyxjQUFEO0FBQ0gsVUFBQTtNQUFBLGFBQUEsa0RBQW1DLElBQUMsQ0FBQSxLQUFNLENBQUEsQ0FBQTtNQUMxQyxZQUFBLHNGQUEwQztNQUUxQyxNQUFBLEdBQVMsSUFBSSxTQUFKLENBQWMsWUFBZCxFQUE0QixjQUE1QjtNQUNULE1BQU0sQ0FBQyxvQkFBUCxDQUE0QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsV0FBRDtBQUMxQixjQUFBOztnQkFBMEIsQ0FBRSxNQUE1QixDQUFBOztVQUNBLEtBQUMsQ0FBQSxrQkFBRCxDQUFvQixXQUFwQjtVQUNBLElBQWtCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQixnQ0FBaEIsQ0FBbEI7WUFBQSxLQUFDLENBQUEsV0FBRCxDQUFBLEVBQUE7O2lCQUNBLEtBQUMsQ0FBQSxPQUFPLENBQUMsSUFBVCxDQUFjLG1CQUFkLEVBQW1DO1lBQUMsSUFBQSxFQUFNLFdBQVA7V0FBbkM7UUFKMEI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTVCO01BS0EsTUFBTSxDQUFDLGVBQVAsQ0FBdUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLFdBQUQ7QUFDckIsY0FBQTs7Z0JBQTBCLENBQUUsTUFBNUIsQ0FBQTs7VUFDQSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQWYsQ0FBb0IsV0FBcEI7VUFDQSxJQUFrQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsZ0NBQWhCLENBQWxCO1lBQUEsS0FBQyxDQUFBLFdBQUQsQ0FBQSxFQUFBOztpQkFDQSxLQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxjQUFkLEVBQThCO1lBQUMsSUFBQSxFQUFNLFdBQVA7V0FBOUI7UUFKcUI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXZCO2FBS0EsTUFBTSxDQUFDLE1BQVAsQ0FBQTtJQWZHOzt1QkFpQkwsbUJBQUEsR0FBcUIsU0FBQyxDQUFEO0FBRW5CLFVBQUE7TUFBQSxZQUFBLHFIQUFrRixDQUFFLE9BQU8sQ0FBQzs7UUFFNUYsNE1BQTRHLENBQUUsT0FBTyxDQUFDOztNQUV0SCxJQUE0RixJQUFDLENBQUEsS0FBSyxDQUFDLE1BQVAsS0FBaUIsQ0FBN0c7O1VBQUEsNEhBQTBFLENBQUUsT0FBTyxDQUFDO1NBQXBGOztNQUNBLElBQXlDLG9CQUF6QztlQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBYixDQUF3QixZQUF4QixFQUFBOztJQVBtQjs7dUJBU3JCLGFBQUEsR0FBZSxTQUFBO2FBQ2IsSUFBQyxDQUFBLElBQUksQ0FBQyxhQUFOLENBQW9CLFdBQXBCO0lBRGE7O3VCQUdmLFdBQUEsR0FBYSxTQUFDLEtBQUQ7QUFDWCxVQUFBO01BQUEsSUFBYyxhQUFkO0FBQUEsZUFBQTs7TUFFQSxJQUFDLENBQUEsWUFBRCxHQUFnQixLQUFLLENBQUMsT0FBTixDQUFBO01BQ2hCLElBQUMsQ0FBQSxnQkFBRCxHQUFvQjtNQUVwQixlQUFBLEdBQWtCLElBQUMsQ0FBQSxrQkFBRCxDQUFBO01BQ2xCLElBQUcsZUFBZSxDQUFDLE1BQWhCLEdBQXlCLENBQXpCLElBQThCLGVBQWdCLENBQUEsQ0FBQSxDQUFoQixLQUF3QixLQUF6RDtRQUNFLElBQUMsQ0FBQSxRQUFELENBQVUsZUFBVjtRQUNBLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBaEIsQ0FBb0IsVUFBcEIsRUFGRjs7YUFHQTtJQVZXOzt1QkFZYixrQkFBQSxHQUFvQixTQUFBO2FBQ2xCLElBQUMsQ0FBQSxJQUFJLENBQUMsZ0JBQU4sQ0FBdUIsV0FBdkI7SUFEa0I7O3VCQUdwQixRQUFBLEdBQVUsU0FBQyxrQkFBRDtBQUNSLFVBQUE7O1FBRFMscUJBQW1CLElBQUMsQ0FBQSxrQkFBRCxDQUFBOztBQUM1QixXQUFBLG9EQUFBOztRQUFBLFFBQVEsQ0FBQyxTQUFTLENBQUMsTUFBbkIsQ0FBMEIsVUFBMUI7QUFBQTthQUNBO0lBRlE7O3VCQUlWLFNBQUEsR0FBVyxTQUFDLEdBQUQ7TUFDVCxJQUFHLFdBQUg7ZUFDRSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVQsR0FBcUIsSUFEdkI7T0FBQSxNQUFBO2VBR0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxVQUhYOztJQURTOzt1QkFNWCxZQUFBLEdBQWMsU0FBQyxNQUFEO01BQ1osSUFBRyxjQUFIO2VBQ0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFULEdBQXFCLE1BQUEsR0FBUyxJQUFDLENBQUEsT0FBTyxDQUFDLGFBRHpDO09BQUEsTUFBQTtlQUdFLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBVCxHQUFxQixJQUFDLENBQUEsT0FBTyxDQUFDLGFBSGhDOztJQURZOzt1QkFNZCxhQUFBLEdBQWUsU0FBQyxLQUFELEVBQVEsTUFBUjtBQUNiLFVBQUE7O1FBRHFCLFNBQU87O01BQzVCLE9BQUEsb0JBQWEsS0FBSyxDQUFFLFNBQVMsQ0FBQyxRQUFqQixDQUEwQixXQUExQixXQUFILEdBQStDLEtBQUssQ0FBQyxNQUFyRCxHQUFpRTsrQkFDM0UsT0FBTyxDQUFFLHNCQUFULENBQWdDLE1BQWhDO0lBRmE7O3VCQUlmLGNBQUEsR0FBZ0IsU0FBQTtBQUNkLFVBQUE7TUFBQSxJQUFHLFNBQUEsR0FBWSxDQUFDLENBQUMsSUFBRixDQUFPLElBQUMsQ0FBQSxJQUFJLENBQUMsZ0JBQU4sQ0FBdUIsUUFBdkIsQ0FBUCxDQUFmO1FBQ0UsSUFBQyxDQUFBLFdBQUQsQ0FBYSxTQUFiO2VBQ0EsSUFBQyxDQUFBLGFBQUQsQ0FBZSxTQUFmLEVBRkY7O0lBRGM7O3VCQUtoQixXQUFBLEdBQWEsU0FBQTtNQUNYLElBQTJCLHFCQUEzQjtRQUFBLElBQUMsQ0FBQSxXQUFELENBQWEsSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLENBQXBCLEVBQUE7O2FBQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFULEdBQXFCO0lBRlY7O3VCQUliLE1BQUEsR0FBUSxTQUFBO2FBQ04sSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFULElBQXNCLElBQUMsQ0FBQSxPQUFPLENBQUM7SUFEekI7O3VCQUdSLFFBQUEsR0FBVSxTQUFBO2FBQ1IsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFULElBQXNCLElBQUMsQ0FBQSxPQUFPLENBQUM7SUFEdkI7O3VCQUtWLFNBQUEsR0FBVyxTQUFDLFdBQUQsRUFBYyxnQkFBZDtBQUNULFVBQUE7TUFBQSxzQkFBQSxHQUF5QixFQUFFLENBQUMsZUFBSCxDQUFtQixXQUFuQjtNQUl6QixvQkFBQSxHQUF1QixFQUFFLENBQUMsWUFBSCxDQUFnQixnQkFBaEIsQ0FBQSxHQUFvQyxJQUFJLENBQUM7TUFDaEUsZUFBQSxHQUFrQixFQUFFLENBQUMsWUFBSCxDQUFnQixXQUFoQixDQUFBLEdBQStCLElBQUksQ0FBQztNQUN0RCxJQUFHLHNCQUFBLElBQTJCLG9CQUFvQixDQUFDLFVBQXJCLENBQWdDLGVBQWhDLENBQTlCO1FBQ0UsSUFBQSxDQUFPLEVBQUUsQ0FBQyxrQkFBSCxDQUFzQixXQUF0QixDQUFQO1VBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixrQ0FBOUI7QUFDQSxpQkFGRjtTQURGOztNQUtBLE9BQUEsR0FBVSxJQUFJLENBQUMsSUFBTCxDQUFVLGdCQUFWLEVBQTRCLElBQUksQ0FBQyxRQUFMLENBQWMsV0FBZCxDQUE1QjtNQUdWLFdBQUEsR0FBYztNQUNkLGVBQUEsR0FBa0I7QUFDbEIsYUFBTSxFQUFFLENBQUMsVUFBSCxDQUFjLE9BQWQsQ0FBTjtRQUNFLElBQUcsc0JBQUg7VUFDRSxPQUFBLEdBQVUsRUFBQSxHQUFHLGVBQUgsR0FBcUIsWUFEakM7U0FBQSxNQUFBO1VBR0UsU0FBQSxHQUFZLGdCQUFBLENBQWlCLGVBQWpCO1VBQ1osUUFBQSxHQUFXLElBQUksQ0FBQyxJQUFMLENBQVUsSUFBSSxDQUFDLE9BQUwsQ0FBYSxlQUFiLENBQVYsRUFBeUMsSUFBSSxDQUFDLFFBQUwsQ0FBYyxlQUFkLEVBQStCLFNBQS9CLENBQXpDO1VBQ1gsT0FBQSxHQUFVLEVBQUEsR0FBRyxRQUFILEdBQWMsV0FBZCxHQUE0QixVQUx4Qzs7UUFNQSxXQUFBLElBQWU7TUFQakI7QUFTQTtRQUNFLElBQUMsQ0FBQSxPQUFPLENBQUMsSUFBVCxDQUFjLGlCQUFkLEVBQWlDO1VBQUMsYUFBQSxXQUFEO1VBQWMsU0FBQSxPQUFkO1NBQWpDO1FBQ0EsSUFBRyxzQkFBSDtVQUVFLEVBQUUsQ0FBQyxRQUFILENBQVksV0FBWixFQUF5QixPQUF6QixFQUZGO1NBQUEsTUFBQTtVQU1FLEVBQUUsQ0FBQyxhQUFILENBQWlCLE9BQWpCLEVBQTBCLEVBQUUsQ0FBQyxZQUFILENBQWdCLFdBQWhCLENBQTFCLEVBTkY7O1FBT0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsY0FBZCxFQUE4QjtVQUFDLGFBQUEsV0FBRDtVQUFjLFNBQUEsT0FBZDtTQUE5QjtRQUVBLElBQUcsSUFBQSxHQUFPLFdBQUEsQ0FBWSxPQUFaLENBQVY7VUFDRSxJQUFJLENBQUMsYUFBTCxDQUFtQixXQUFuQjtpQkFDQSxJQUFJLENBQUMsYUFBTCxDQUFtQixPQUFuQixFQUZGO1NBWEY7T0FBQSxjQUFBO1FBZU07UUFDSixJQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxtQkFBZCxFQUFtQztVQUFDLGFBQUEsV0FBRDtVQUFjLFNBQUEsT0FBZDtTQUFuQztlQUNBLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBbkIsQ0FBOEIsdUJBQUEsR0FBd0IsV0FBeEIsR0FBb0MsTUFBcEMsR0FBMEMsZ0JBQXhFLEVBQTRGO1VBQUEsTUFBQSxFQUFRLEtBQUssQ0FBQyxPQUFkO1NBQTVGLEVBakJGOztJQTFCUzs7dUJBOENYLFNBQUEsR0FBVyxTQUFDLFdBQUQsRUFBYyxnQkFBZDtBQUdULFVBQUE7QUFBQTtRQUNFLG9CQUFBLEdBQXVCLEVBQUUsQ0FBQyxZQUFILENBQWdCLGdCQUFoQixDQUFBLEdBQW9DLElBQUksQ0FBQztRQUNoRSxlQUFBLEdBQWtCLEVBQUUsQ0FBQyxZQUFILENBQWdCLFdBQWhCLENBQUEsR0FBK0IsSUFBSSxDQUFDO1FBQ3RELElBQUcsRUFBRSxDQUFDLGVBQUgsQ0FBbUIsV0FBbkIsQ0FBQSxJQUFvQyxvQkFBb0IsQ0FBQyxVQUFyQixDQUFnQyxlQUFoQyxDQUF2QztVQUNFLElBQUEsQ0FBTyxFQUFFLENBQUMsa0JBQUgsQ0FBc0IsV0FBdEIsQ0FBUDtZQUNFLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBbkIsQ0FBOEIsa0NBQTlCO0FBQ0EsbUJBRkY7V0FERjtTQUhGO09BQUEsY0FBQTtRQU9NO1FBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4Qix1QkFBQSxHQUF3QixXQUF4QixHQUFvQyxNQUFwQyxHQUEwQyxnQkFBeEUsRUFBNEY7VUFBQSxNQUFBLEVBQVEsS0FBSyxDQUFDLE9BQWQ7U0FBNUYsRUFSRjs7TUFVQSxPQUFBLEdBQVUsSUFBSSxDQUFDLElBQUwsQ0FBVSxnQkFBVixFQUE0QixJQUFJLENBQUMsUUFBTCxDQUFjLFdBQWQsQ0FBNUI7QUFFVjtRQUNFLElBQUMsQ0FBQSxPQUFPLENBQUMsSUFBVCxDQUFjLGlCQUFkLEVBQWlDO1VBQUMsYUFBQSxXQUFEO1VBQWMsU0FBQSxPQUFkO1NBQWpDO1FBQ0EsRUFBRSxDQUFDLFFBQUgsQ0FBWSxXQUFaLEVBQXlCLE9BQXpCO1FBQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsYUFBZCxFQUE2QjtVQUFDLGFBQUEsV0FBRDtVQUFjLFNBQUEsT0FBZDtTQUE3QjtRQUVBLElBQUcsSUFBQSxHQUFPLFdBQUEsQ0FBWSxPQUFaLENBQVY7VUFDRSxJQUFJLENBQUMsYUFBTCxDQUFtQixXQUFuQjtVQUNBLElBQUksQ0FBQyxhQUFMLENBQW1CLE9BQW5CLEVBRkY7U0FMRjtPQUFBLGNBQUE7UUFTTTtRQUNKLElBQUcsS0FBSyxDQUFDLElBQU4sS0FBYyxRQUFqQjtBQUNFLGlCQUFPLElBQUMsQ0FBQSxvQkFBRCxDQUFzQixXQUF0QixFQUFtQyxPQUFuQyxFQUE0QyxnQkFBNUMsRUFEVDtTQUFBLE1BQUE7VUFHRSxJQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxtQkFBZCxFQUFtQztZQUFDLGFBQUEsV0FBRDtZQUFjLFNBQUEsT0FBZDtXQUFuQztVQUNBLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBbkIsQ0FBOEIsdUJBQUEsR0FBd0IsV0FBeEIsR0FBb0MsTUFBcEMsR0FBMEMsZ0JBQXhFLEVBQTRGO1lBQUEsTUFBQSxFQUFRLEtBQUssQ0FBQyxPQUFkO1dBQTVGLEVBSkY7U0FWRjs7QUFnQkEsYUFBTztJQS9CRTs7dUJBaUNYLG9CQUFBLEdBQXNCLFNBQUMsV0FBRCxFQUFjLE9BQWQsRUFBdUIsZ0JBQXZCO0FBQ3BCLFVBQUE7QUFBQTtRQUNFLElBQUEsQ0FBTyxFQUFFLENBQUMsZUFBSCxDQUFtQixXQUFuQixDQUFQO1VBRUUsTUFBQSxHQUFTLElBQUksQ0FBQyxPQUFMLENBQ1A7WUFBQSxPQUFBLEVBQVMsR0FBQSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQUwsQ0FBYyxnQkFBZCxFQUFnQyxPQUFoQyxDQUFELENBQUgsR0FBNkMsa0JBQXREO1lBQ0EsZUFBQSxFQUFpQiw0QkFEakI7WUFFQSxPQUFBLEVBQVMsQ0FBQyxjQUFELEVBQWlCLE1BQWpCLEVBQXlCLFFBQXpCLENBRlQ7V0FETztBQUtULGtCQUFPLE1BQVA7QUFBQSxpQkFDTyxDQURQO2NBRUksRUFBRSxDQUFDLFVBQUgsQ0FBYyxXQUFkLEVBQTJCLE9BQTNCO2NBQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsYUFBZCxFQUE2QjtnQkFBQyxhQUFBLFdBQUQ7Z0JBQWMsU0FBQSxPQUFkO2VBQTdCO2NBRUEsSUFBRyxJQUFBLEdBQU8sV0FBQSxDQUFZLE9BQVosQ0FBVjtnQkFDRSxJQUFJLENBQUMsYUFBTCxDQUFtQixXQUFuQjtnQkFDQSxJQUFJLENBQUMsYUFBTCxDQUFtQixPQUFuQixFQUZGOztBQUdBO0FBUkosaUJBU08sQ0FUUDtBQVVJLHFCQUFPO0FBVlgsV0FQRjtTQUFBLE1BQUE7VUFtQkUsT0FBQSxHQUFVLEVBQUUsQ0FBQyxXQUFILENBQWUsV0FBZjtBQUNWLGVBQUEseUNBQUE7O1lBQ0UsSUFBRyxFQUFFLENBQUMsVUFBSCxDQUFjLElBQUksQ0FBQyxJQUFMLENBQVUsT0FBVixFQUFtQixLQUFuQixDQUFkLENBQUg7Y0FDRSxJQUFBLENBQW9CLElBQUMsQ0FBQSxvQkFBRCxDQUFzQixJQUFJLENBQUMsSUFBTCxDQUFVLFdBQVYsRUFBdUIsS0FBdkIsQ0FBdEIsRUFBcUQsSUFBSSxDQUFDLElBQUwsQ0FBVSxPQUFWLEVBQW1CLEtBQW5CLENBQXJELEVBQWdGLGdCQUFoRixDQUFwQjtBQUFBLHVCQUFPLE1BQVA7ZUFERjthQUFBLE1BQUE7Y0FHRSxJQUFDLENBQUEsU0FBRCxDQUFXLElBQUksQ0FBQyxJQUFMLENBQVUsV0FBVixFQUF1QixLQUF2QixDQUFYLEVBQTBDLE9BQTFDLEVBSEY7O0FBREY7VUFPQSxJQUFBLENBQWlDLEVBQUUsQ0FBQyxXQUFILENBQWUsV0FBZixDQUEyQixDQUFDLE1BQTdEO1lBQUEsRUFBRSxDQUFDLFNBQUgsQ0FBYSxXQUFiLEVBQUE7V0EzQkY7U0FERjtPQUFBLGNBQUE7UUE2Qk07UUFDSixJQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxtQkFBZCxFQUFtQztVQUFDLGFBQUEsV0FBRDtVQUFjLFNBQUEsT0FBZDtTQUFuQztRQUNBLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBbkIsQ0FBOEIsdUJBQUEsR0FBd0IsV0FBeEIsR0FBb0MsTUFBcEMsR0FBMEMsT0FBeEUsRUFBbUY7VUFBQSxNQUFBLEVBQVEsS0FBSyxDQUFDLE9BQWQ7U0FBbkYsRUEvQkY7O0FBaUNBLGFBQU87SUFsQ2E7O3VCQW9DdEIsb0JBQUEsR0FBc0IsU0FBQTtNQUdwQixJQUFBLENBQWMsSUFBQyxDQUFBLFNBQUQsQ0FBQSxDQUFkO0FBQUEsZUFBQTs7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFmLEdBQXlCO01BQ3pCLElBQUMsQ0FBQSxPQUFPLENBQUM7YUFDVCxJQUFDLENBQUEsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFmLEdBQXlCO0lBTkw7O3VCQVF0QixXQUFBLEdBQWEsU0FBQyxDQUFEO0FBQ1gsVUFBQTtNQUFBLElBQUEsQ0FBYyxDQUFBLGFBQUEsR0FBZ0IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFULENBQWlCLFFBQWpCLENBQWhCLENBQWQ7QUFBQSxlQUFBOztNQUVBLENBQUMsQ0FBQyxlQUFGLENBQUE7TUFNQSxNQUFBLEdBQVMsQ0FBQyxDQUFDLE9BQUYsSUFBYSxDQUFDLENBQUMsQ0FBQyxPQUFGLElBQWMsT0FBTyxDQUFDLFFBQVIsS0FBc0IsUUFBckM7TUFHdEIsSUFBRyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQXhCLENBQWlDLFVBQWpDLENBQUg7UUFFRSxJQUFHLENBQUMsQ0FBQyxNQUFGLEtBQVksQ0FBWixJQUFpQixDQUFDLENBQUMsQ0FBQyxPQUFGLElBQWMsT0FBTyxDQUFDLFFBQVIsS0FBb0IsUUFBbkMsQ0FBcEI7QUFDRSxpQkFERjtTQUFBLE1BQUE7VUFJRyxXQUFZO1VBQ2IsSUFBQyxDQUFBLGVBQUQsR0FBbUI7WUFBQyxVQUFBLFFBQUQ7WUFBVyxRQUFBLE1BQVg7O0FBQ25CLGlCQU5GO1NBRkY7O01BVUEsSUFBRyxDQUFDLENBQUMsUUFBRixJQUFlLE1BQWxCO1FBRUUsSUFBQyxDQUFBLHVCQUFELENBQXlCLGFBQXpCLEVBQXdDLEtBQXhDO2VBQ0EsSUFBQyxDQUFBLDhCQUFELENBQUEsRUFIRjtPQUFBLE1BSUssSUFBRyxDQUFDLENBQUMsUUFBTDtRQUVILElBQUMsQ0FBQSx1QkFBRCxDQUF5QixhQUF6QjtlQUNBLElBQUMsQ0FBQSw4QkFBRCxDQUFBLEVBSEc7T0FBQSxNQUtBLElBQUcsTUFBSDtRQUNILElBQUMsQ0FBQSxxQkFBRCxDQUF1QixhQUF2QjtRQUNBLElBQUMsQ0FBQSxnQkFBRCxHQUFvQjtlQUNwQixJQUFDLENBQUEsOEJBQUQsQ0FBQSxFQUhHO09BQUEsTUFBQTtRQUtILElBQUMsQ0FBQSxXQUFELENBQWEsYUFBYjtlQUNBLElBQUMsQ0FBQSxZQUFELENBQUEsRUFORzs7SUEvQk07O3VCQXVDYixTQUFBLEdBQVcsU0FBQyxDQUFEO0FBQ1QsVUFBQTtNQUFBLElBQWMsNEJBQWQ7QUFBQSxlQUFBOztNQUVBLE9BQXFCLElBQUMsQ0FBQSxlQUF0QixFQUFDLHdCQUFELEVBQVc7TUFDWCxJQUFDLENBQUEsZUFBRCxHQUFtQjtNQUVuQixJQUFBLENBQWMsQ0FBQSxhQUFBLEdBQWdCLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBVCxDQUFpQixRQUFqQixDQUFoQixDQUFkO0FBQUEsZUFBQTs7TUFFQSxDQUFDLENBQUMsZUFBRixDQUFBO01BRUEsSUFBRyxRQUFBLElBQWEsTUFBaEI7UUFFRSxJQUFDLENBQUEsdUJBQUQsQ0FBeUIsYUFBekIsRUFBd0MsS0FBeEM7ZUFDQSxJQUFDLENBQUEsOEJBQUQsQ0FBQSxFQUhGO09BQUEsTUFJSyxJQUFHLFFBQUg7UUFFSCxJQUFDLENBQUEsdUJBQUQsQ0FBeUIsYUFBekI7ZUFDQSxJQUFDLENBQUEsOEJBQUQsQ0FBQSxFQUhHO09BQUEsTUFLQSxJQUFHLE1BQUg7UUFDSCxJQUFDLENBQUEsUUFBRCxDQUFVLENBQUMsYUFBRCxDQUFWO1FBQ0EsSUFBQyxDQUFBLGdCQUFELEdBQW9CO2VBQ3BCLElBQUMsQ0FBQSw4QkFBRCxDQUFBLEVBSEc7T0FBQSxNQUFBO1FBS0gsSUFBQyxDQUFBLFdBQUQsQ0FBYSxhQUFiO2VBQ0EsSUFBQyxDQUFBLFlBQUQsQ0FBQSxFQU5HOztJQW5CSTs7dUJBZ0NYLGFBQUEsR0FBZSxTQUFBO0FBQ2IsVUFBQTtBQUFBO0FBQUE7V0FBQSxzQ0FBQTs7cUJBQUEsS0FBSyxDQUFDLE9BQU4sQ0FBQTtBQUFBOztJQURhOzt1QkFPZix1QkFBQSxHQUF5QixTQUFDLEtBQUQsRUFBUSxjQUFSO0FBQ3ZCLFVBQUE7O1FBRCtCLGlCQUFpQjs7TUFDaEQsb0JBQUEsbURBQTJDLElBQUMsQ0FBQSxhQUFELENBQUE7TUFDM0MsZUFBQSxHQUFrQixLQUFLLENBQUM7TUFDeEIsUUFBQSxHQUFXO01BQ1gsSUFBRyxlQUFBLEtBQW1CLG9CQUFvQixDQUFDLGFBQTNDO1FBQ0UsT0FBQSxHQUFVLEtBQUssQ0FBQyxJQUFOLENBQVcsZUFBZSxDQUFDLGdCQUFoQixDQUFpQyxRQUFqQyxDQUFYO1FBQ1YsVUFBQSxHQUFhLE9BQU8sQ0FBQyxPQUFSLENBQWdCLEtBQWhCO1FBQ2IsYUFBQSxHQUFnQixPQUFPLENBQUMsT0FBUixDQUFnQixvQkFBaEI7UUFDaEIsUUFBQTs7QUFBWTtlQUFvQixtSEFBcEI7eUJBQUEsT0FBUSxDQUFBLENBQUE7QUFBUjs7O1FBRVosSUFBZSxjQUFmO1VBQUEsSUFBQyxDQUFBLFFBQUQsQ0FBQSxFQUFBOztBQUNBLGFBQUEsMENBQUE7O1VBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFsQixDQUFzQixVQUF0QjtBQUFBLFNBUEY7O2FBU0E7SUFidUI7O3VCQW1CekIscUJBQUEsR0FBdUIsU0FBQyxLQUFEOztRQUNyQixLQUFLLENBQUUsU0FBUyxDQUFDLE1BQWpCLENBQXdCLFVBQXhCOzthQUNBO0lBRnFCOzt1QkFNdkIsWUFBQSxHQUFjLFNBQUE7TUFDWixJQUFDLENBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFoQixDQUF1QixjQUF2QjthQUNBLElBQUMsQ0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQWhCLENBQW9CLFdBQXBCO0lBRlk7O3VCQU1kLG1CQUFBLEdBQXFCLFNBQUE7TUFDbkIsSUFBQyxDQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBaEIsQ0FBdUIsV0FBdkI7YUFDQSxJQUFDLENBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFoQixDQUFvQixjQUFwQjtJQUZtQjs7dUJBSXJCLDhCQUFBLEdBQWdDLFNBQUE7TUFDOUIsSUFBRyxJQUFDLENBQUEsa0JBQUQsQ0FBQSxDQUFxQixDQUFDLE1BQXRCLEdBQStCLENBQWxDO2VBQ0UsSUFBQyxDQUFBLG1CQUFELENBQUEsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsWUFBRCxDQUFBLEVBSEY7O0lBRDhCOzt1QkFTaEMsa0JBQUEsR0FBb0IsU0FBQTthQUNsQixJQUFDLENBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFoQixDQUF5QixjQUF6QjtJQURrQjs7dUJBR3BCLFdBQUEsR0FBYSxTQUFDLENBQUQ7QUFDWCxVQUFBO01BQUEsSUFBRyxLQUFBLEdBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFULENBQWlCLGtCQUFqQixDQUFYO1FBQ0UsSUFBVSxJQUFDLENBQUEsZUFBZSxDQUFDLFVBQWpCLENBQTRCLENBQTVCLENBQVY7QUFBQSxpQkFBQTs7UUFDQSxJQUFBLENBQWMsSUFBQyxDQUFBLG1CQUFELENBQXFCLENBQXJCLENBQWQ7QUFBQSxpQkFBQTs7UUFFQSxDQUFDLENBQUMsZUFBRixDQUFBO1FBRUEsSUFBQSxDQUFzQyxJQUFDLENBQUEsZUFBZSxDQUFDLEdBQWpCLENBQXFCLEtBQXJCLENBQXRDO1VBQUEsSUFBQyxDQUFBLGVBQWUsQ0FBQyxHQUFqQixDQUFxQixLQUFyQixFQUE0QixDQUE1QixFQUFBOztRQUNBLElBQUEsQ0FBQSxDQUFPLElBQUMsQ0FBQSxlQUFlLENBQUMsR0FBakIsQ0FBcUIsS0FBckIsQ0FBQSxLQUFpQyxDQUFqQyxJQUFzQyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQWhCLENBQXlCLFVBQXpCLENBQTdDLENBQUE7VUFDRSxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQWhCLENBQW9CLFdBQXBCLEVBQWlDLFVBQWpDLEVBREY7O2VBR0EsSUFBQyxDQUFBLGVBQWUsQ0FBQyxHQUFqQixDQUFxQixLQUFyQixFQUE0QixJQUFDLENBQUEsZUFBZSxDQUFDLEdBQWpCLENBQXFCLEtBQXJCLENBQUEsR0FBOEIsQ0FBMUQsRUFWRjs7SUFEVzs7dUJBYWIsV0FBQSxHQUFhLFNBQUMsQ0FBRDtBQUNYLFVBQUE7TUFBQSxJQUFHLEtBQUEsR0FBUSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQVQsQ0FBaUIsa0JBQWpCLENBQVg7UUFDRSxJQUFVLElBQUMsQ0FBQSxlQUFlLENBQUMsVUFBakIsQ0FBNEIsQ0FBNUIsQ0FBVjtBQUFBLGlCQUFBOztRQUNBLElBQUEsQ0FBYyxJQUFDLENBQUEsbUJBQUQsQ0FBcUIsQ0FBckIsQ0FBZDtBQUFBLGlCQUFBOztRQUVBLENBQUMsQ0FBQyxlQUFGLENBQUE7UUFFQSxJQUFDLENBQUEsZUFBZSxDQUFDLEdBQWpCLENBQXFCLEtBQXJCLEVBQTRCLElBQUMsQ0FBQSxlQUFlLENBQUMsR0FBakIsQ0FBcUIsS0FBckIsQ0FBQSxHQUE4QixDQUExRDtRQUNBLElBQUcsSUFBQyxDQUFBLGVBQWUsQ0FBQyxHQUFqQixDQUFxQixLQUFyQixDQUFBLEtBQStCLENBQS9CLElBQXFDLEtBQUssQ0FBQyxTQUFTLENBQUMsUUFBaEIsQ0FBeUIsV0FBekIsQ0FBeEM7aUJBQ0UsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFoQixDQUF1QixXQUF2QixFQUFvQyxVQUFwQyxFQURGO1NBUEY7O0lBRFc7O3VCQVliLFdBQUEsR0FBYSxTQUFDLENBQUQ7QUFDWCxVQUFBO01BQUEsSUFBQyxDQUFBLGVBQUQsR0FBbUIsSUFBSTtNQUN2QixJQUFDLENBQUEsZUFBRCxHQUFtQjtNQUNuQixJQUFHLEtBQUEsR0FBUSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQVQsQ0FBaUIsUUFBakIsQ0FBWDtRQUNFLENBQUMsQ0FBQyxlQUFGLENBQUE7UUFFQSxJQUFHLElBQUMsQ0FBQSxlQUFlLENBQUMsWUFBakIsQ0FBOEIsQ0FBOUIsQ0FBSDtBQUNFLGlCQUFPLElBQUMsQ0FBQSxlQUFlLENBQUMsV0FBakIsQ0FBNkIsQ0FBN0IsRUFEVDs7UUFHQSxTQUFBLEdBQVksUUFBUSxDQUFDLGFBQVQsQ0FBdUIsSUFBdkI7UUFDWixTQUFTLENBQUMsU0FBUyxDQUFDLEdBQXBCLENBQXdCLFNBQXhCLEVBQW1DLFdBQW5DO1FBQ0EsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFoQixHQUEyQjtRQUMzQixTQUFTLENBQUMsS0FBSyxDQUFDLEdBQWhCLEdBQXNCO1FBQ3RCLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBaEIsR0FBdUI7UUFJdkIsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFoQixHQUE2QjtRQUU3QixZQUFBLEdBQWU7QUFDZjtBQUFBLGFBQUEsc0NBQUE7O1VBQ0UsU0FBQSxHQUFZLE1BQU0sQ0FBQyxhQUFQLENBQXFCLE9BQXJCLENBQTZCLENBQUMsT0FBTyxDQUFDO1VBQ2xELGNBQUEsR0FBaUIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFsQixDQUEwQixpQkFBMUI7VUFDakIsSUFBQSxDQUFPLGNBQVA7WUFDRSxZQUFZLENBQUMsSUFBYixDQUFrQixTQUFsQjtZQUNBLFVBQUEsR0FBYSxNQUFNLENBQUMsU0FBUCxDQUFpQixJQUFqQjtZQUNiLElBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxRQUFyQixDQUE4QixXQUE5QixDQUFIO2NBQ0UsVUFBVSxDQUFDLGFBQVgsQ0FBeUIsVUFBekIsQ0FBb0MsQ0FBQyxNQUFyQyxDQUFBLEVBREY7O0FBRUE7QUFBQSxpQkFBQSxXQUFBOztjQUNFLFVBQVUsQ0FBQyxLQUFNLENBQUEsR0FBQSxDQUFqQixHQUF3QjtBQUQxQjtZQUVBLFVBQVUsQ0FBQyxLQUFLLENBQUMsV0FBakIsR0FBK0I7WUFDL0IsVUFBVSxDQUFDLEtBQUssQ0FBQyxZQUFqQixHQUFnQztZQUNoQyxTQUFTLENBQUMsTUFBVixDQUFpQixVQUFqQixFQVRGOztBQUhGO1FBY0EsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFkLENBQTBCLFNBQTFCO1FBRUEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxhQUFmLEdBQStCO1FBQy9CLENBQUMsQ0FBQyxZQUFZLENBQUMsWUFBZixDQUE0QixTQUE1QixFQUF1QyxDQUF2QyxFQUEwQyxDQUExQztRQUNBLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBZixDQUF1QixjQUF2QixFQUF1QyxJQUFJLENBQUMsU0FBTCxDQUFlLFlBQWYsQ0FBdkM7UUFDQSxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQWYsQ0FBdUIsc0JBQXZCLEVBQStDLE1BQS9DO2VBRUEsTUFBTSxDQUFDLHFCQUFQLENBQTZCLFNBQUE7aUJBQzNCLFNBQVMsQ0FBQyxNQUFWLENBQUE7UUFEMkIsQ0FBN0IsRUF0Q0Y7O0lBSFc7O3VCQTZDYixVQUFBLEdBQVksU0FBQyxDQUFEO0FBQ1YsVUFBQTtNQUFBLElBQUcsS0FBQSxHQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBVCxDQUFpQixrQkFBakIsQ0FBWDtRQUNFLElBQVUsSUFBQyxDQUFBLGVBQWUsQ0FBQyxVQUFqQixDQUE0QixDQUE1QixDQUFWO0FBQUEsaUJBQUE7O1FBQ0EsSUFBQSxDQUFjLElBQUMsQ0FBQSxtQkFBRCxDQUFxQixDQUFyQixDQUFkO0FBQUEsaUJBQUE7O1FBRUEsQ0FBQyxDQUFDLGNBQUYsQ0FBQTtRQUNBLENBQUMsQ0FBQyxlQUFGLENBQUE7UUFFQSxJQUFHLElBQUMsQ0FBQSxlQUFlLENBQUMsR0FBakIsQ0FBcUIsS0FBckIsQ0FBQSxHQUE4QixDQUE5QixJQUFvQyxDQUFJLEtBQUssQ0FBQyxTQUFTLENBQUMsUUFBaEIsQ0FBeUIsVUFBekIsQ0FBM0M7aUJBQ0UsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFoQixDQUFvQixXQUFwQixFQUFpQyxVQUFqQyxFQURGO1NBUEY7O0lBRFU7O3VCQVlaLE1BQUEsR0FBUSxTQUFDLENBQUQ7QUFDTixVQUFBO01BQUEsSUFBQyxDQUFBLGVBQUQsR0FBbUIsSUFBSTtNQUN2QixJQUFHLEtBQUEsR0FBUSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQVQsQ0FBaUIsa0JBQWpCLENBQVg7UUFDRSxJQUFVLElBQUMsQ0FBQSxlQUFlLENBQUMsVUFBakIsQ0FBNEIsQ0FBNUIsQ0FBVjtBQUFBLGlCQUFBOztRQUNBLElBQUEsQ0FBYyxJQUFDLENBQUEsbUJBQUQsQ0FBcUIsQ0FBckIsQ0FBZDtBQUFBLGlCQUFBOztRQUVBLENBQUMsQ0FBQyxjQUFGLENBQUE7UUFDQSxDQUFDLENBQUMsZUFBRixDQUFBO1FBRUEsZ0JBQUEsdURBQStDLENBQUUsT0FBTyxDQUFDO1FBQ3pELElBQUEsQ0FBb0IsZ0JBQXBCO0FBQUEsaUJBQU8sTUFBUDs7UUFFQSxZQUFBLEdBQWUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFmLENBQXVCLGNBQXZCO1FBRWYsSUFBRyxZQUFIO1VBRUUsWUFBQSxHQUFlLElBQUksQ0FBQyxLQUFMLENBQVcsWUFBWDtVQUNmLElBQVUsWUFBWSxDQUFDLFFBQWIsQ0FBc0IsZ0JBQXRCLENBQVY7QUFBQSxtQkFBQTs7VUFFQSxLQUFLLENBQUMsU0FBUyxDQUFDLE1BQWhCLENBQXVCLFdBQXZCLEVBQW9DLFVBQXBDO0FBR0E7ZUFBQSw0Q0FBQTs7OztvQkFLNEIsQ0FBRTs7O1lBQzVCLElBQUcsQ0FBQyxPQUFPLENBQUMsUUFBUixLQUFvQixRQUFwQixJQUFpQyxDQUFDLENBQUMsT0FBcEMsQ0FBQSxJQUFnRCxDQUFDLENBQUMsT0FBckQ7MkJBQ0UsSUFBQyxDQUFBLFNBQUQsQ0FBVyxXQUFYLEVBQXdCLGdCQUF4QixHQURGO2FBQUEsTUFBQTtjQUdFLElBQUEsQ0FBYSxJQUFDLENBQUEsU0FBRCxDQUFXLFdBQVgsRUFBd0IsZ0JBQXhCLENBQWI7QUFBQSxzQkFBQTtlQUFBLE1BQUE7cUNBQUE7ZUFIRjs7QUFORjt5QkFSRjtTQUFBLE1BQUE7VUFvQkUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFoQixDQUF1QixVQUF2QjtBQUNBO0FBQUE7ZUFBQSxzQ0FBQTs7WUFDRSxJQUFHLENBQUMsT0FBTyxDQUFDLFFBQVIsS0FBb0IsUUFBcEIsSUFBaUMsQ0FBQyxDQUFDLE9BQXBDLENBQUEsSUFBZ0QsQ0FBQyxDQUFDLE9BQXJEOzRCQUNFLElBQUMsQ0FBQSxTQUFELENBQVcsSUFBSSxDQUFDLElBQWhCLEVBQXNCLGdCQUF0QixHQURGO2FBQUEsTUFBQTtjQUdFLElBQUEsQ0FBYSxJQUFDLENBQUEsU0FBRCxDQUFXLElBQUksQ0FBQyxJQUFoQixFQUFzQixnQkFBdEIsQ0FBYjtBQUFBLHNCQUFBO2VBQUEsTUFBQTtzQ0FBQTtlQUhGOztBQURGOzBCQXJCRjtTQVpGO09BQUEsTUFzQ0ssSUFBRyxDQUFDLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUF4QjtBQUVIO0FBQUE7YUFBQSx3Q0FBQTs7d0JBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFiLENBQXFCLEtBQUssQ0FBQyxJQUEzQjtBQUFBO3dCQUZHOztJQXhDQzs7dUJBNENSLG1CQUFBLEdBQXFCLFNBQUMsQ0FBRDtBQUNuQixVQUFBO0FBQUE7QUFBQSxXQUFBLHNDQUFBOztRQUNFLElBQUcsSUFBSSxDQUFDLElBQUwsS0FBYSxzQkFBYixJQUF1QyxJQUFJLENBQUMsSUFBTCxLQUFhLE1BQXZEO0FBQ0UsaUJBQU8sS0FEVDs7QUFERjtBQUlBLGFBQU87SUFMWTs7dUJBT3JCLFNBQUEsR0FBVyxTQUFBO2FBQ1QsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULEtBQTBCLENBQTFCLElBQStCLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFBVCxLQUEyQjtJQURqRDs7Ozs7QUF0cUNiIiwic291cmNlc0NvbnRlbnQiOlsicGF0aCA9IHJlcXVpcmUgJ3BhdGgnXG57c2hlbGx9ID0gcmVxdWlyZSAnZWxlY3Ryb24nXG5cbl8gPSByZXF1aXJlICd1bmRlcnNjb3JlLXBsdXMnXG57QnVmZmVyZWRQcm9jZXNzLCBDb21wb3NpdGVEaXNwb3NhYmxlLCBFbWl0dGVyfSA9IHJlcXVpcmUgJ2F0b20nXG57cmVwb0ZvclBhdGgsIGdldFN0eWxlT2JqZWN0LCBnZXRGdWxsRXh0ZW5zaW9ufSA9IHJlcXVpcmUgXCIuL2hlbHBlcnNcIlxuZnMgPSByZXF1aXJlICdmcy1wbHVzJ1xuXG5BZGREaWFsb2cgPSByZXF1aXJlICcuL2FkZC1kaWFsb2cnXG5Nb3ZlRGlhbG9nID0gcmVxdWlyZSAnLi9tb3ZlLWRpYWxvZydcbkNvcHlEaWFsb2cgPSByZXF1aXJlICcuL2NvcHktZGlhbG9nJ1xuSWdub3JlZE5hbWVzID0gbnVsbCAjIERlZmVyIHJlcXVpcmluZyB1bnRpbCBhY3R1YWxseSBuZWVkZWRcblxuQWRkUHJvamVjdHNWaWV3ID0gcmVxdWlyZSAnLi9hZGQtcHJvamVjdHMtdmlldydcblxuRGlyZWN0b3J5ID0gcmVxdWlyZSAnLi9kaXJlY3RvcnknXG5EaXJlY3RvcnlWaWV3ID0gcmVxdWlyZSAnLi9kaXJlY3RvcnktdmlldydcblJvb3REcmFnQW5kRHJvcCA9IHJlcXVpcmUgJy4vcm9vdC1kcmFnLWFuZC1kcm9wJ1xuXG5UUkVFX1ZJRVdfVVJJID0gJ2F0b206Ly90cmVlLXZpZXcnXG5cbnRvZ2dsZUNvbmZpZyA9IChrZXlQYXRoKSAtPlxuICBhdG9tLmNvbmZpZy5zZXQoa2V5UGF0aCwgbm90IGF0b20uY29uZmlnLmdldChrZXlQYXRoKSlcblxubmV4dElkID0gMVxuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBUcmVlVmlld1xuICBjb25zdHJ1Y3RvcjogKHN0YXRlKSAtPlxuICAgIEBpZCA9IG5leHRJZCsrXG4gICAgQGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3Rvb2wtcGFuZWwnLCAndHJlZS12aWV3JylcbiAgICBAZWxlbWVudC50YWJJbmRleCA9IC0xXG5cbiAgICBAbGlzdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ29sJylcbiAgICBAbGlzdC5jbGFzc0xpc3QuYWRkKCd0cmVlLXZpZXctcm9vdCcsICdmdWxsLW1lbnUnLCAnbGlzdC10cmVlJywgJ2hhcy1jb2xsYXBzYWJsZS1jaGlsZHJlbicsICdmb2N1c2FibGUtcGFuZWwnKVxuXG4gICAgQGRpc3Bvc2FibGVzID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcbiAgICBAZW1pdHRlciA9IG5ldyBFbWl0dGVyXG4gICAgQHJvb3RzID0gW11cbiAgICBAc2VsZWN0ZWRQYXRoID0gbnVsbFxuICAgIEBzZWxlY3RPbk1vdXNlVXAgPSBudWxsXG4gICAgQGxhc3RGb2N1c2VkRW50cnkgPSBudWxsXG4gICAgQGlnbm9yZWRQYXR0ZXJucyA9IFtdXG4gICAgQHVzZVN5bmNGUyA9IGZhbHNlXG4gICAgQGN1cnJlbnRseU9wZW5pbmcgPSBuZXcgTWFwXG4gICAgQGVkaXRvcnNUb01vdmUgPSBbXVxuICAgIEBlZGl0b3JzVG9EZXN0cm95ID0gW11cblxuICAgIEBkcmFnRXZlbnRDb3VudHMgPSBuZXcgV2Vha01hcFxuICAgIEByb290RHJhZ0FuZERyb3AgPSBuZXcgUm9vdERyYWdBbmREcm9wKHRoaXMpXG5cbiAgICBAaGFuZGxlRXZlbnRzKClcblxuICAgIHByb2Nlc3MubmV4dFRpY2sgPT5cbiAgICAgIEBvblN0eWxlc2hlZXRzQ2hhbmdlZCgpXG4gICAgICBvblN0eWxlc2hlZXRzQ2hhbmdlZCA9IF8uZGVib3VuY2UoQG9uU3R5bGVzaGVldHNDaGFuZ2VkLCAxMDApXG4gICAgICBAZGlzcG9zYWJsZXMuYWRkIGF0b20uc3R5bGVzLm9uRGlkQWRkU3R5bGVFbGVtZW50KG9uU3R5bGVzaGVldHNDaGFuZ2VkKVxuICAgICAgQGRpc3Bvc2FibGVzLmFkZCBhdG9tLnN0eWxlcy5vbkRpZFJlbW92ZVN0eWxlRWxlbWVudChvblN0eWxlc2hlZXRzQ2hhbmdlZClcbiAgICAgIEBkaXNwb3NhYmxlcy5hZGQgYXRvbS5zdHlsZXMub25EaWRVcGRhdGVTdHlsZUVsZW1lbnQob25TdHlsZXNoZWV0c0NoYW5nZWQpXG5cbiAgICBAdXBkYXRlUm9vdHMoc3RhdGUuZGlyZWN0b3J5RXhwYW5zaW9uU3RhdGVzKVxuXG4gICAgaWYgc3RhdGUuc2VsZWN0ZWRQYXRocz8ubGVuZ3RoID4gMFxuICAgICAgQHNlbGVjdE11bHRpcGxlRW50cmllcyhAZW50cnlGb3JQYXRoKHNlbGVjdGVkUGF0aCkpIGZvciBzZWxlY3RlZFBhdGggaW4gc3RhdGUuc2VsZWN0ZWRQYXRoc1xuICAgIGVsc2VcbiAgICAgIEBzZWxlY3RFbnRyeShAcm9vdHNbMF0pXG5cbiAgICBpZiBzdGF0ZS5zY3JvbGxUb3A/IG9yIHN0YXRlLnNjcm9sbExlZnQ/XG4gICAgICBvYnNlcnZlciA9IG5ldyBJbnRlcnNlY3Rpb25PYnNlcnZlcig9PlxuICAgICAgICBpZiBAaXNWaXNpYmxlKClcbiAgICAgICAgICBAZWxlbWVudC5zY3JvbGxUb3AgPSBzdGF0ZS5zY3JvbGxUb3BcbiAgICAgICAgICBAZWxlbWVudC5zY3JvbGxMZWZ0ID0gc3RhdGUuc2Nyb2xsTGVmdFxuICAgICAgICAgIG9ic2VydmVyLmRpc2Nvbm5lY3QoKVxuICAgICAgKVxuICAgICAgb2JzZXJ2ZXIub2JzZXJ2ZShAZWxlbWVudClcblxuICAgIEBlbGVtZW50LnN0eWxlLndpZHRoID0gXCIje3N0YXRlLndpZHRofXB4XCIgaWYgc3RhdGUud2lkdGggPiAwXG5cbiAgICBAZGlzcG9zYWJsZXMuYWRkIEBvbldpbGxNb3ZlRW50cnkgKHtpbml0aWFsUGF0aCwgbmV3UGF0aH0pID0+XG4gICAgICBlZGl0b3JzID0gYXRvbS53b3Jrc3BhY2UuZ2V0VGV4dEVkaXRvcnMoKVxuICAgICAgaWYgZnMuaXNEaXJlY3RvcnlTeW5jKGluaXRpYWxQYXRoKVxuICAgICAgICBpbml0aWFsUGF0aCArPSBwYXRoLnNlcCAjIEF2b2lkIG1vdmluZyBsaWIyJ3MgZWRpdG9ycyB3aGVuIGxpYiB3YXMgbW92ZWRcbiAgICAgICAgZm9yIGVkaXRvciBpbiBlZGl0b3JzXG4gICAgICAgICAgZmlsZVBhdGggPSBlZGl0b3IuZ2V0UGF0aCgpXG4gICAgICAgICAgaWYgZmlsZVBhdGg/LnN0YXJ0c1dpdGgoaW5pdGlhbFBhdGgpXG4gICAgICAgICAgICBAZWRpdG9yc1RvTW92ZS5wdXNoKGZpbGVQYXRoKVxuICAgICAgZWxzZVxuICAgICAgICBmb3IgZWRpdG9yIGluIGVkaXRvcnNcbiAgICAgICAgICBmaWxlUGF0aCA9IGVkaXRvci5nZXRQYXRoKClcbiAgICAgICAgICBpZiBmaWxlUGF0aCBpcyBpbml0aWFsUGF0aFxuICAgICAgICAgICAgQGVkaXRvcnNUb01vdmUucHVzaChmaWxlUGF0aClcblxuICAgIEBkaXNwb3NhYmxlcy5hZGQgQG9uRW50cnlNb3ZlZCAoe2luaXRpYWxQYXRoLCBuZXdQYXRofSkgPT5cbiAgICAgIGZvciBlZGl0b3IgaW4gYXRvbS53b3Jrc3BhY2UuZ2V0VGV4dEVkaXRvcnMoKVxuICAgICAgICBmaWxlUGF0aCA9IGVkaXRvci5nZXRQYXRoKClcbiAgICAgICAgaW5kZXggPSBAZWRpdG9yc1RvTW92ZS5pbmRleE9mKGZpbGVQYXRoKVxuICAgICAgICBpZiBpbmRleCBpc250IC0xXG4gICAgICAgICAgZWRpdG9yLmdldEJ1ZmZlcigpLnNldFBhdGgoZmlsZVBhdGgucmVwbGFjZShpbml0aWFsUGF0aCwgbmV3UGF0aCkpXG4gICAgICAgICAgQGVkaXRvcnNUb01vdmUuc3BsaWNlKGluZGV4LCAxKVxuXG4gICAgQGRpc3Bvc2FibGVzLmFkZCBAb25Nb3ZlRW50cnlGYWlsZWQgKHtpbml0aWFsUGF0aCwgbmV3UGF0aH0pID0+XG4gICAgICBpbmRleCA9IEBlZGl0b3JzVG9Nb3ZlLmluZGV4T2YoaW5pdGlhbFBhdGgpXG4gICAgICBAZWRpdG9yc1RvTW92ZS5zcGxpY2UoaW5kZXgsIDEpIGlmIGluZGV4IGlzbnQgLTFcblxuICAgIEBkaXNwb3NhYmxlcy5hZGQgQG9uV2lsbERlbGV0ZUVudHJ5ICh7cGF0aFRvRGVsZXRlfSkgPT5cbiAgICAgIGVkaXRvcnMgPSBhdG9tLndvcmtzcGFjZS5nZXRUZXh0RWRpdG9ycygpXG4gICAgICBpZiBmcy5pc0RpcmVjdG9yeVN5bmMocGF0aFRvRGVsZXRlKVxuICAgICAgICBwYXRoVG9EZWxldGUgKz0gcGF0aC5zZXAgIyBBdm9pZCBkZXN0cm95aW5nIGxpYjIncyBlZGl0b3JzIHdoZW4gbGliIHdhcyBkZWxldGVkXG4gICAgICAgIGZvciBlZGl0b3IgaW4gZWRpdG9yc1xuICAgICAgICAgIGZpbGVQYXRoID0gZWRpdG9yLmdldFBhdGgoKVxuICAgICAgICAgIGlmIGZpbGVQYXRoPy5zdGFydHNXaXRoKHBhdGhUb0RlbGV0ZSkgYW5kIG5vdCBlZGl0b3IuaXNNb2RpZmllZCgpXG4gICAgICAgICAgICBAZWRpdG9yc1RvRGVzdHJveS5wdXNoKGZpbGVQYXRoKVxuICAgICAgZWxzZVxuICAgICAgICBmb3IgZWRpdG9yIGluIGVkaXRvcnNcbiAgICAgICAgICBmaWxlUGF0aCA9IGVkaXRvci5nZXRQYXRoKClcbiAgICAgICAgICBpZiBmaWxlUGF0aCBpcyBwYXRoVG9EZWxldGUgYW5kIG5vdCBlZGl0b3IuaXNNb2RpZmllZCgpXG4gICAgICAgICAgICBAZWRpdG9yc1RvRGVzdHJveS5wdXNoKGZpbGVQYXRoKVxuXG4gICAgQGRpc3Bvc2FibGVzLmFkZCBAb25FbnRyeURlbGV0ZWQgKHtwYXRoVG9EZWxldGV9KSA9PlxuICAgICAgZm9yIGVkaXRvciBpbiBhdG9tLndvcmtzcGFjZS5nZXRUZXh0RWRpdG9ycygpXG4gICAgICAgIGluZGV4ID0gQGVkaXRvcnNUb0Rlc3Ryb3kuaW5kZXhPZihlZGl0b3IuZ2V0UGF0aCgpKVxuICAgICAgICBpZiBpbmRleCBpc250IC0xXG4gICAgICAgICAgZWRpdG9yLmRlc3Ryb3koKVxuICAgICAgICAgIEBlZGl0b3JzVG9EZXN0cm95LnNwbGljZShpbmRleCwgMSlcblxuICAgIEBkaXNwb3NhYmxlcy5hZGQgQG9uRGVsZXRlRW50cnlGYWlsZWQgKHtwYXRoVG9EZWxldGV9KSA9PlxuICAgICAgaW5kZXggPSBAZWRpdG9yc1RvRGVzdHJveS5pbmRleE9mKHBhdGhUb0RlbGV0ZSlcbiAgICAgIEBlZGl0b3JzVG9EZXN0cm95LnNwbGljZShpbmRleCwgMSkgaWYgaW5kZXggaXNudCAtMVxuXG4gIHNlcmlhbGl6ZTogLT5cbiAgICBkaXJlY3RvcnlFeHBhbnNpb25TdGF0ZXM6IG5ldyAoKHJvb3RzKSAtPlxuICAgICAgQFtyb290LmRpcmVjdG9yeS5wYXRoXSA9IHJvb3QuZGlyZWN0b3J5LnNlcmlhbGl6ZUV4cGFuc2lvblN0YXRlKCkgZm9yIHJvb3QgaW4gcm9vdHNcbiAgICAgIHRoaXMpKEByb290cylcbiAgICBkZXNlcmlhbGl6ZXI6ICdUcmVlVmlldydcbiAgICBzZWxlY3RlZFBhdGhzOiBBcnJheS5mcm9tKEBnZXRTZWxlY3RlZEVudHJpZXMoKSwgKGVudHJ5KSAtPiBlbnRyeS5nZXRQYXRoKCkpXG4gICAgc2Nyb2xsTGVmdDogQGVsZW1lbnQuc2Nyb2xsTGVmdFxuICAgIHNjcm9sbFRvcDogQGVsZW1lbnQuc2Nyb2xsVG9wXG4gICAgd2lkdGg6IHBhcnNlSW50KEBlbGVtZW50LnN0eWxlLndpZHRoIG9yIDApXG5cbiAgZGVzdHJveTogLT5cbiAgICByb290LmRpcmVjdG9yeS5kZXN0cm95KCkgZm9yIHJvb3QgaW4gQHJvb3RzXG4gICAgQGRpc3Bvc2FibGVzLmRpc3Bvc2UoKVxuICAgIEByb290RHJhZ0FuZERyb3AuZGlzcG9zZSgpXG4gICAgQGVtaXR0ZXIuZW1pdCgnZGlkLWRlc3Ryb3knKVxuXG4gIG9uRGlkRGVzdHJveTogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdkaWQtZGVzdHJveScsIGNhbGxiYWNrKVxuXG4gIGdldFRpdGxlOiAtPiBcIlByb2plY3RcIlxuXG4gIGdldFVSSTogLT4gVFJFRV9WSUVXX1VSSVxuXG4gIGdldFByZWZlcnJlZExvY2F0aW9uOiAtPlxuICAgIGlmIGF0b20uY29uZmlnLmdldCgndHJlZS12aWV3LnNob3dPblJpZ2h0U2lkZScpXG4gICAgICAncmlnaHQnXG4gICAgZWxzZVxuICAgICAgJ2xlZnQnXG5cbiAgZ2V0QWxsb3dlZExvY2F0aW9uczogLT4gW1wibGVmdFwiLCBcInJpZ2h0XCJdXG5cbiAgaXNQZXJtYW5lbnREb2NrSXRlbTogLT4gdHJ1ZVxuXG4gIGdldFByZWZlcnJlZFdpZHRoOiAtPlxuICAgIEBsaXN0LnN0eWxlLndpZHRoID0gJ21pbi1jb250ZW50J1xuICAgIHJlc3VsdCA9IEBsaXN0Lm9mZnNldFdpZHRoXG4gICAgQGxpc3Quc3R5bGUud2lkdGggPSAnJ1xuICAgIHJlc3VsdFxuXG4gIG9uRGlyZWN0b3J5Q3JlYXRlZDogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdkaXJlY3RvcnktY3JlYXRlZCcsIGNhbGxiYWNrKVxuXG4gIG9uRW50cnlDb3BpZWQ6IChjYWxsYmFjaykgLT5cbiAgICBAZW1pdHRlci5vbignZW50cnktY29waWVkJywgY2FsbGJhY2spXG5cbiAgb25XaWxsRGVsZXRlRW50cnk6IChjYWxsYmFjaykgLT5cbiAgICBAZW1pdHRlci5vbignd2lsbC1kZWxldGUtZW50cnknLCBjYWxsYmFjaylcblxuICBvbkVudHJ5RGVsZXRlZDogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdlbnRyeS1kZWxldGVkJywgY2FsbGJhY2spXG5cbiAgb25EZWxldGVFbnRyeUZhaWxlZDogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdkZWxldGUtZW50cnktZmFpbGVkJywgY2FsbGJhY2spXG5cbiAgb25XaWxsTW92ZUVudHJ5OiAoY2FsbGJhY2spIC0+XG4gICAgQGVtaXR0ZXIub24oJ3dpbGwtbW92ZS1lbnRyeScsIGNhbGxiYWNrKVxuXG4gIG9uRW50cnlNb3ZlZDogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdlbnRyeS1tb3ZlZCcsIGNhbGxiYWNrKVxuXG4gIG9uTW92ZUVudHJ5RmFpbGVkOiAoY2FsbGJhY2spIC0+XG4gICAgQGVtaXR0ZXIub24oJ21vdmUtZW50cnktZmFpbGVkJywgY2FsbGJhY2spXG5cbiAgb25GaWxlQ3JlYXRlZDogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdmaWxlLWNyZWF0ZWQnLCBjYWxsYmFjaylcblxuICBoYW5kbGVFdmVudHM6IC0+XG4gICAgQGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lciAnY2xpY2snLCAoZSkgPT5cbiAgICAgICMgVGhpcyBwcmV2ZW50cyBhY2NpZGVudGFsIGNvbGxhcHNpbmcgd2hlbiBhIC5lbnRyaWVzIGVsZW1lbnQgaXMgdGhlIGV2ZW50IHRhcmdldFxuICAgICAgcmV0dXJuIGlmIGUudGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygnZW50cmllcycpXG5cbiAgICAgIEBlbnRyeUNsaWNrZWQoZSkgdW5sZXNzIGUuc2hpZnRLZXkgb3IgZS5tZXRhS2V5IG9yIGUuY3RybEtleVxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgJ21vdXNlZG93bicsIChlKSA9PiBAb25Nb3VzZURvd24oZSlcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyICdtb3VzZXVwJywgKGUpID0+IEBvbk1vdXNlVXAoZSlcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyICdkcmFnc3RhcnQnLCAoZSkgPT4gQG9uRHJhZ1N0YXJ0KGUpXG4gICAgQGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lciAnZHJhZ2VudGVyJywgKGUpID0+IEBvbkRyYWdFbnRlcihlKVxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgJ2RyYWdsZWF2ZScsIChlKSA9PiBAb25EcmFnTGVhdmUoZSlcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyICdkcmFnb3ZlcicsIChlKSA9PiBAb25EcmFnT3ZlcihlKVxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgJ2Ryb3AnLCAoZSkgPT4gQG9uRHJvcChlKVxuXG4gICAgYXRvbS5jb21tYW5kcy5hZGQgQGVsZW1lbnQsXG4gICAgICdjb3JlOm1vdmUtdXAnOiAoZSkgPT4gQG1vdmVVcChlKVxuICAgICAnY29yZTptb3ZlLWRvd24nOiAoZSkgPT4gQG1vdmVEb3duKGUpXG4gICAgICdjb3JlOnBhZ2UtdXAnOiA9PiBAcGFnZVVwKClcbiAgICAgJ2NvcmU6cGFnZS1kb3duJzogPT4gQHBhZ2VEb3duKClcbiAgICAgJ2NvcmU6bW92ZS10by10b3AnOiA9PiBAc2Nyb2xsVG9Ub3AoKVxuICAgICAnY29yZTptb3ZlLXRvLWJvdHRvbSc6ID0+IEBzY3JvbGxUb0JvdHRvbSgpXG4gICAgICd0cmVlLXZpZXc6ZXhwYW5kLWl0ZW0nOiA9PiBAb3BlblNlbGVjdGVkRW50cnkocGVuZGluZzogdHJ1ZSwgdHJ1ZSlcbiAgICAgJ3RyZWUtdmlldzpyZWN1cnNpdmUtZXhwYW5kLWRpcmVjdG9yeSc6ID0+IEBleHBhbmREaXJlY3RvcnkodHJ1ZSlcbiAgICAgJ3RyZWUtdmlldzpjb2xsYXBzZS1kaXJlY3RvcnknOiA9PiBAY29sbGFwc2VEaXJlY3RvcnkoKVxuICAgICAndHJlZS12aWV3OnJlY3Vyc2l2ZS1jb2xsYXBzZS1kaXJlY3RvcnknOiA9PiBAY29sbGFwc2VEaXJlY3RvcnkodHJ1ZSlcbiAgICAgJ3RyZWUtdmlldzpjb2xsYXBzZS1hbGwnOiA9PiBAY29sbGFwc2VEaXJlY3RvcnkodHJ1ZSwgdHJ1ZSlcbiAgICAgJ3RyZWUtdmlldzpvcGVuLXNlbGVjdGVkLWVudHJ5JzogPT4gQG9wZW5TZWxlY3RlZEVudHJ5KClcbiAgICAgJ3RyZWUtdmlldzpvcGVuLXNlbGVjdGVkLWVudHJ5LXJpZ2h0JzogPT4gQG9wZW5TZWxlY3RlZEVudHJ5UmlnaHQoKVxuICAgICAndHJlZS12aWV3Om9wZW4tc2VsZWN0ZWQtZW50cnktbGVmdCc6ID0+IEBvcGVuU2VsZWN0ZWRFbnRyeUxlZnQoKVxuICAgICAndHJlZS12aWV3Om9wZW4tc2VsZWN0ZWQtZW50cnktdXAnOiA9PiBAb3BlblNlbGVjdGVkRW50cnlVcCgpXG4gICAgICd0cmVlLXZpZXc6b3Blbi1zZWxlY3RlZC1lbnRyeS1kb3duJzogPT4gQG9wZW5TZWxlY3RlZEVudHJ5RG93bigpXG4gICAgICd0cmVlLXZpZXc6bW92ZSc6ID0+IEBtb3ZlU2VsZWN0ZWRFbnRyeSgpXG4gICAgICd0cmVlLXZpZXc6Y29weSc6ID0+IEBjb3B5U2VsZWN0ZWRFbnRyaWVzKClcbiAgICAgJ3RyZWUtdmlldzpjdXQnOiA9PiBAY3V0U2VsZWN0ZWRFbnRyaWVzKClcbiAgICAgJ3RyZWUtdmlldzpwYXN0ZSc6ID0+IEBwYXN0ZUVudHJpZXMoKVxuICAgICAndHJlZS12aWV3OmNvcHktZnVsbC1wYXRoJzogPT4gQGNvcHlTZWxlY3RlZEVudHJ5UGF0aChmYWxzZSlcbiAgICAgJ3RyZWUtdmlldzpzaG93LWluLWZpbGUtbWFuYWdlcic6ID0+IEBzaG93U2VsZWN0ZWRFbnRyeUluRmlsZU1hbmFnZXIoKVxuICAgICAndHJlZS12aWV3Om9wZW4taW4tbmV3LXdpbmRvdyc6ID0+IEBvcGVuU2VsZWN0ZWRFbnRyeUluTmV3V2luZG93KClcbiAgICAgJ3RyZWUtdmlldzpjb3B5LXByb2plY3QtcGF0aCc6ID0+IEBjb3B5U2VsZWN0ZWRFbnRyeVBhdGgodHJ1ZSlcbiAgICAgJ3RyZWUtdmlldzp1bmZvY3VzJzogPT4gQHVuZm9jdXMoKVxuICAgICAndHJlZS12aWV3OnRvZ2dsZS12Y3MtaWdub3JlZC1maWxlcyc6IC0+IHRvZ2dsZUNvbmZpZyAndHJlZS12aWV3LmhpZGVWY3NJZ25vcmVkRmlsZXMnXG4gICAgICd0cmVlLXZpZXc6dG9nZ2xlLWlnbm9yZWQtbmFtZXMnOiAtPiB0b2dnbGVDb25maWcgJ3RyZWUtdmlldy5oaWRlSWdub3JlZE5hbWVzJ1xuICAgICAndHJlZS12aWV3OnJlbW92ZS1wcm9qZWN0LWZvbGRlcic6IChlKSA9PiBAcmVtb3ZlUHJvamVjdEZvbGRlcihlKVxuXG4gICAgWzAuLjhdLmZvckVhY2ggKGluZGV4KSA9PlxuICAgICAgYXRvbS5jb21tYW5kcy5hZGQgQGVsZW1lbnQsIFwidHJlZS12aWV3Om9wZW4tc2VsZWN0ZWQtZW50cnktaW4tcGFuZS0je2luZGV4ICsgMX1cIiwgPT5cbiAgICAgICAgQG9wZW5TZWxlY3RlZEVudHJ5SW5QYW5lIGluZGV4XG5cbiAgICBAZGlzcG9zYWJsZXMuYWRkIGF0b20ud29ya3NwYWNlLmdldENlbnRlcigpLm9uRGlkQ2hhbmdlQWN0aXZlUGFuZUl0ZW0gPT5cbiAgICAgIEBzZWxlY3RBY3RpdmVGaWxlKClcbiAgICAgIEByZXZlYWxBY3RpdmVGaWxlKHtzaG93OiBmYWxzZSwgZm9jdXM6IGZhbHNlfSkgaWYgYXRvbS5jb25maWcuZ2V0KCd0cmVlLXZpZXcuYXV0b1JldmVhbCcpXG4gICAgQGRpc3Bvc2FibGVzLmFkZCBhdG9tLnByb2plY3Qub25EaWRDaGFuZ2VQYXRocyA9PlxuICAgICAgQHVwZGF0ZVJvb3RzKClcbiAgICBAZGlzcG9zYWJsZXMuYWRkIGF0b20uY29uZmlnLm9uRGlkQ2hhbmdlICd0cmVlLXZpZXcuaGlkZVZjc0lnbm9yZWRGaWxlcycsID0+XG4gICAgICBAdXBkYXRlUm9vdHMoKVxuICAgIEBkaXNwb3NhYmxlcy5hZGQgYXRvbS5jb25maWcub25EaWRDaGFuZ2UgJ3RyZWUtdmlldy5oaWRlSWdub3JlZE5hbWVzJywgPT5cbiAgICAgIEB1cGRhdGVSb290cygpXG4gICAgQGRpc3Bvc2FibGVzLmFkZCBhdG9tLmNvbmZpZy5vbkRpZENoYW5nZSAnY29yZS5pZ25vcmVkTmFtZXMnLCA9PlxuICAgICAgQHVwZGF0ZVJvb3RzKCkgaWYgYXRvbS5jb25maWcuZ2V0KCd0cmVlLXZpZXcuaGlkZUlnbm9yZWROYW1lcycpXG4gICAgQGRpc3Bvc2FibGVzLmFkZCBhdG9tLmNvbmZpZy5vbkRpZENoYW5nZSAndHJlZS12aWV3LnNvcnRGb2xkZXJzQmVmb3JlRmlsZXMnLCA9PlxuICAgICAgQHVwZGF0ZVJvb3RzKClcbiAgICBAZGlzcG9zYWJsZXMuYWRkIGF0b20uY29uZmlnLm9uRGlkQ2hhbmdlICd0cmVlLXZpZXcuc3F1YXNoRGlyZWN0b3J5TmFtZXMnLCA9PlxuICAgICAgQHVwZGF0ZVJvb3RzKClcblxuICB0b2dnbGU6IC0+XG4gICAgYXRvbS53b3Jrc3BhY2UudG9nZ2xlKHRoaXMpXG5cbiAgc2hvdzogKGZvY3VzKSAtPlxuICAgIGF0b20ud29ya3NwYWNlLm9wZW4odGhpcywge1xuICAgICAgc2VhcmNoQWxsUGFuZXM6IHRydWUsXG4gICAgICBhY3RpdmF0ZVBhbmU6IGZhbHNlLFxuICAgICAgYWN0aXZhdGVJdGVtOiBmYWxzZSxcbiAgICB9KS50aGVuID0+XG4gICAgICBhdG9tLndvcmtzcGFjZS5wYW5lQ29udGFpbmVyRm9yVVJJKEBnZXRVUkkoKSkuc2hvdygpXG4gICAgICBAZm9jdXMoKSBpZiBmb2N1c1xuXG4gIGhpZGU6IC0+XG4gICAgYXRvbS53b3Jrc3BhY2UuaGlkZSh0aGlzKVxuXG4gIGZvY3VzOiAtPlxuICAgIEBlbGVtZW50LmZvY3VzKClcblxuICB1bmZvY3VzOiAtPlxuICAgIGF0b20ud29ya3NwYWNlLmdldENlbnRlcigpLmFjdGl2YXRlKClcblxuICBoYXNGb2N1czogLT5cbiAgICBkb2N1bWVudC5hY3RpdmVFbGVtZW50IGlzIEBlbGVtZW50XG5cbiAgdG9nZ2xlRm9jdXM6IC0+XG4gICAgaWYgQGhhc0ZvY3VzKClcbiAgICAgIEB1bmZvY3VzKClcbiAgICBlbHNlXG4gICAgICBAc2hvdyh0cnVlKVxuXG4gIGVudHJ5Q2xpY2tlZDogKGUpIC0+XG4gICAgaWYgZW50cnkgPSBlLnRhcmdldC5jbG9zZXN0KCcuZW50cnknKVxuICAgICAgaXNSZWN1cnNpdmUgPSBlLmFsdEtleSBvciBmYWxzZVxuICAgICAgQHNlbGVjdEVudHJ5KGVudHJ5KVxuICAgICAgaWYgZW50cnkuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXJlY3RvcnknKVxuICAgICAgICBlbnRyeS50b2dnbGVFeHBhbnNpb24oaXNSZWN1cnNpdmUpXG4gICAgICBlbHNlIGlmIGVudHJ5LmNsYXNzTGlzdC5jb250YWlucygnZmlsZScpXG4gICAgICAgIEBmaWxlVmlld0VudHJ5Q2xpY2tlZChlKVxuXG4gIGZpbGVWaWV3RW50cnlDbGlja2VkOiAoZSkgLT5cbiAgICBmaWxlUGF0aCA9IGUudGFyZ2V0LmNsb3Nlc3QoJy5lbnRyeScpLmdldFBhdGgoKVxuICAgIGRldGFpbCA9IGUuZGV0YWlsID8gMVxuICAgIGFsd2F5c09wZW5FeGlzdGluZyA9IGF0b20uY29uZmlnLmdldCgndHJlZS12aWV3LmFsd2F5c09wZW5FeGlzdGluZycpXG4gICAgaWYgZGV0YWlsIGlzIDFcbiAgICAgIGlmIGF0b20uY29uZmlnLmdldCgnY29yZS5hbGxvd1BlbmRpbmdQYW5lSXRlbXMnKVxuICAgICAgICBvcGVuUHJvbWlzZSA9IGF0b20ud29ya3NwYWNlLm9wZW4oZmlsZVBhdGgsIHBlbmRpbmc6IHRydWUsIGFjdGl2YXRlUGFuZTogZmFsc2UsIHNlYXJjaEFsbFBhbmVzOiBhbHdheXNPcGVuRXhpc3RpbmcpXG4gICAgICAgIEBjdXJyZW50bHlPcGVuaW5nLnNldChmaWxlUGF0aCwgb3BlblByb21pc2UpXG4gICAgICAgIG9wZW5Qcm9taXNlLnRoZW4gPT4gQGN1cnJlbnRseU9wZW5pbmcuZGVsZXRlKGZpbGVQYXRoKVxuICAgIGVsc2UgaWYgZGV0YWlsIGlzIDJcbiAgICAgIEBvcGVuQWZ0ZXJQcm9taXNlKGZpbGVQYXRoLCBzZWFyY2hBbGxQYW5lczogYWx3YXlzT3BlbkV4aXN0aW5nKVxuXG4gIG9wZW5BZnRlclByb21pc2U6ICh1cmksIG9wdGlvbnMpIC0+XG4gICAgaWYgcHJvbWlzZSA9IEBjdXJyZW50bHlPcGVuaW5nLmdldCh1cmkpXG4gICAgICBwcm9taXNlLnRoZW4gLT4gYXRvbS53b3Jrc3BhY2Uub3Blbih1cmksIG9wdGlvbnMpXG4gICAgZWxzZVxuICAgICAgYXRvbS53b3Jrc3BhY2Uub3Blbih1cmksIG9wdGlvbnMpXG5cbiAgdXBkYXRlUm9vdHM6IChleHBhbnNpb25TdGF0ZXM9e30pIC0+XG4gICAgc2VsZWN0ZWRQYXRocyA9IEBzZWxlY3RlZFBhdGhzKClcblxuICAgIG9sZEV4cGFuc2lvblN0YXRlcyA9IHt9XG4gICAgZm9yIHJvb3QgaW4gQHJvb3RzXG4gICAgICBvbGRFeHBhbnNpb25TdGF0ZXNbcm9vdC5kaXJlY3RvcnkucGF0aF0gPSByb290LmRpcmVjdG9yeS5zZXJpYWxpemVFeHBhbnNpb25TdGF0ZSgpXG4gICAgICByb290LmRpcmVjdG9yeS5kZXN0cm95KClcbiAgICAgIHJvb3QucmVtb3ZlKClcblxuICAgIEByb290cyA9IFtdXG5cbiAgICBwcm9qZWN0UGF0aHMgPSBhdG9tLnByb2plY3QuZ2V0UGF0aHMoKVxuICAgIGlmIHByb2plY3RQYXRocy5sZW5ndGggPiAwXG4gICAgICBAZWxlbWVudC5hcHBlbmRDaGlsZChAbGlzdCkgdW5sZXNzIEBlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ3RyZWUtdmlldy1yb290JylcblxuICAgICAgYWRkUHJvamVjdHNWaWV3RWxlbWVudCA9IEBlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJyNhZGQtcHJvamVjdHMtdmlldycpXG4gICAgICBAZWxlbWVudC5yZW1vdmVDaGlsZChhZGRQcm9qZWN0c1ZpZXdFbGVtZW50KSBpZiBhZGRQcm9qZWN0c1ZpZXdFbGVtZW50XG5cbiAgICAgIElnbm9yZWROYW1lcyA/PSByZXF1aXJlKCcuL2lnbm9yZWQtbmFtZXMnKVxuXG4gICAgICBAcm9vdHMgPSBmb3IgcHJvamVjdFBhdGggaW4gcHJvamVjdFBhdGhzXG4gICAgICAgIHN0YXRzID0gZnMubHN0YXRTeW5jTm9FeGNlcHRpb24ocHJvamVjdFBhdGgpXG4gICAgICAgIGNvbnRpbnVlIHVubGVzcyBzdGF0c1xuICAgICAgICBzdGF0cyA9IF8ucGljayBzdGF0cywgXy5rZXlzKHN0YXRzKS4uLlxuICAgICAgICBmb3Iga2V5IGluIFtcImF0aW1lXCIsIFwiYmlydGh0aW1lXCIsIFwiY3RpbWVcIiwgXCJtdGltZVwiXVxuICAgICAgICAgIHN0YXRzW2tleV0gPSBzdGF0c1trZXldLmdldFRpbWUoKVxuXG4gICAgICAgIGRpcmVjdG9yeSA9IG5ldyBEaXJlY3Rvcnkoe1xuICAgICAgICAgIG5hbWU6IHBhdGguYmFzZW5hbWUocHJvamVjdFBhdGgpXG4gICAgICAgICAgZnVsbFBhdGg6IHByb2plY3RQYXRoXG4gICAgICAgICAgc3ltbGluazogZmFsc2VcbiAgICAgICAgICBpc1Jvb3Q6IHRydWVcbiAgICAgICAgICBleHBhbnNpb25TdGF0ZTogZXhwYW5zaW9uU3RhdGVzW3Byb2plY3RQYXRoXSA/XG4gICAgICAgICAgICAgICAgICAgICAgICAgIG9sZEV4cGFuc2lvblN0YXRlc1twcm9qZWN0UGF0aF0gP1xuICAgICAgICAgICAgICAgICAgICAgICAgICB7aXNFeHBhbmRlZDogdHJ1ZX1cbiAgICAgICAgICBpZ25vcmVkTmFtZXM6IG5ldyBJZ25vcmVkTmFtZXMoKVxuICAgICAgICAgIEB1c2VTeW5jRlNcbiAgICAgICAgICBzdGF0c1xuICAgICAgICB9KVxuICAgICAgICByb290ID0gbmV3IERpcmVjdG9yeVZpZXcoZGlyZWN0b3J5KS5lbGVtZW50XG4gICAgICAgIEBsaXN0LmFwcGVuZENoaWxkKHJvb3QpXG4gICAgICAgIHJvb3RcblxuICAgICAgIyBUaGUgRE9NIGhhcyBiZWVuIHJlY3JlYXRlZDsgcmVzZWxlY3QgZXZlcnl0aGluZ1xuICAgICAgQHNlbGVjdE11bHRpcGxlRW50cmllcyhAZW50cnlGb3JQYXRoKHNlbGVjdGVkUGF0aCkpIGZvciBzZWxlY3RlZFBhdGggaW4gc2VsZWN0ZWRQYXRoc1xuICAgIGVsc2VcbiAgICAgIEBlbGVtZW50LnJlbW92ZUNoaWxkKEBsaXN0KSBpZiBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcudHJlZS12aWV3LXJvb3QnKVxuICAgICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQobmV3IEFkZFByb2plY3RzVmlldygpLmVsZW1lbnQpIHVubGVzcyBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcjYWRkLXByb2plY3RzLXZpZXcnKVxuXG4gIGdldEFjdGl2ZVBhdGg6IC0+IGF0b20ud29ya3NwYWNlLmdldENlbnRlcigpLmdldEFjdGl2ZVBhbmVJdGVtKCk/LmdldFBhdGg/KClcblxuICBzZWxlY3RBY3RpdmVGaWxlOiAtPlxuICAgIGFjdGl2ZUZpbGVQYXRoID0gQGdldEFjdGl2ZVBhdGgoKVxuICAgIGlmIEBlbnRyeUZvclBhdGgoYWN0aXZlRmlsZVBhdGgpXG4gICAgICBAc2VsZWN0RW50cnlGb3JQYXRoKGFjdGl2ZUZpbGVQYXRoKVxuICAgIGVsc2VcbiAgICAgICMgSWYgdGhlIGFjdGl2ZSBmaWxlIGlzIG5vdCBwYXJ0IG9mIHRoZSBwcm9qZWN0LCBkZXNlbGVjdCBhbGwgZW50cmllc1xuICAgICAgQGRlc2VsZWN0KClcblxuICByZXZlYWxBY3RpdmVGaWxlOiAob3B0aW9ucyA9IHt9KSAtPlxuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKSB1bmxlc3MgYXRvbS5wcm9qZWN0LmdldFBhdGhzKCkubGVuZ3RoXG5cbiAgICB7c2hvdywgZm9jdXN9ID0gb3B0aW9uc1xuXG4gICAgZm9jdXMgPz0gYXRvbS5jb25maWcuZ2V0KCd0cmVlLXZpZXcuZm9jdXNPblJldmVhbCcpXG4gICAgcHJvbWlzZSA9IGlmIHNob3cgb3IgZm9jdXMgdGhlbiBAc2hvdyhmb2N1cykgZWxzZSBQcm9taXNlLnJlc29sdmUoKVxuICAgIHByb21pc2UudGhlbiA9PlxuICAgICAgcmV0dXJuIHVubGVzcyBhY3RpdmVGaWxlUGF0aCA9IEBnZXRBY3RpdmVQYXRoKClcblxuICAgICAgW3Jvb3RQYXRoLCByZWxhdGl2ZVBhdGhdID0gYXRvbS5wcm9qZWN0LnJlbGF0aXZpemVQYXRoKGFjdGl2ZUZpbGVQYXRoKVxuICAgICAgcmV0dXJuIHVubGVzcyByb290UGF0aD9cblxuICAgICAgYWN0aXZlUGF0aENvbXBvbmVudHMgPSByZWxhdGl2ZVBhdGguc3BsaXQocGF0aC5zZXApXG4gICAgICAjIEFkZCB0aGUgcm9vdCBmb2xkZXIgdG8gdGhlIHBhdGggY29tcG9uZW50c1xuICAgICAgYWN0aXZlUGF0aENvbXBvbmVudHMudW5zaGlmdChyb290UGF0aC5zdWJzdHIocm9vdFBhdGgubGFzdEluZGV4T2YocGF0aC5zZXApICsgMSkpXG4gICAgICAjIEFuZCByZW1vdmUgaXQgZnJvbSB0aGUgY3VycmVudCBwYXRoXG4gICAgICBjdXJyZW50UGF0aCA9IHJvb3RQYXRoLnN1YnN0cigwLCByb290UGF0aC5sYXN0SW5kZXhPZihwYXRoLnNlcCkpXG4gICAgICBmb3IgcGF0aENvbXBvbmVudCBpbiBhY3RpdmVQYXRoQ29tcG9uZW50c1xuICAgICAgICBjdXJyZW50UGF0aCArPSBwYXRoLnNlcCArIHBhdGhDb21wb25lbnRcbiAgICAgICAgZW50cnkgPSBAZW50cnlGb3JQYXRoKGN1cnJlbnRQYXRoKVxuICAgICAgICBpZiBlbnRyeS5jbGFzc0xpc3QuY29udGFpbnMoJ2RpcmVjdG9yeScpXG4gICAgICAgICAgZW50cnkuZXhwYW5kKClcbiAgICAgICAgZWxzZVxuICAgICAgICAgIEBzZWxlY3RFbnRyeShlbnRyeSlcbiAgICAgICAgICBAc2Nyb2xsVG9FbnRyeShlbnRyeSlcblxuICBjb3B5U2VsZWN0ZWRFbnRyeVBhdGg6IChyZWxhdGl2ZVBhdGggPSBmYWxzZSkgLT5cbiAgICBpZiBwYXRoVG9Db3B5ID0gQHNlbGVjdGVkUGF0aFxuICAgICAgcGF0aFRvQ29weSA9IGF0b20ucHJvamVjdC5yZWxhdGl2aXplKHBhdGhUb0NvcHkpIGlmIHJlbGF0aXZlUGF0aFxuICAgICAgYXRvbS5jbGlwYm9hcmQud3JpdGUocGF0aFRvQ29weSlcblxuICBlbnRyeUZvclBhdGg6IChlbnRyeVBhdGgpIC0+XG4gICAgYmVzdE1hdGNoRW50cnkgPSBudWxsXG4gICAgYmVzdE1hdGNoTGVuZ3RoID0gMFxuXG4gICAgZm9yIGVudHJ5IGluIEBsaXN0LnF1ZXJ5U2VsZWN0b3JBbGwoJy5lbnRyeScpXG4gICAgICBpZiBlbnRyeS5pc1BhdGhFcXVhbChlbnRyeVBhdGgpXG4gICAgICAgIHJldHVybiBlbnRyeVxuXG4gICAgICBlbnRyeUxlbmd0aCA9IGVudHJ5LmdldFBhdGgoKS5sZW5ndGhcbiAgICAgIGlmIGVudHJ5LmRpcmVjdG9yeT8uY29udGFpbnMoZW50cnlQYXRoKSBhbmQgZW50cnlMZW5ndGggPiBiZXN0TWF0Y2hMZW5ndGhcbiAgICAgICAgYmVzdE1hdGNoRW50cnkgPSBlbnRyeVxuICAgICAgICBiZXN0TWF0Y2hMZW5ndGggPSBlbnRyeUxlbmd0aFxuXG4gICAgYmVzdE1hdGNoRW50cnlcblxuICBzZWxlY3RFbnRyeUZvclBhdGg6IChlbnRyeVBhdGgpIC0+XG4gICAgQHNlbGVjdEVudHJ5KEBlbnRyeUZvclBhdGgoZW50cnlQYXRoKSlcblxuICBtb3ZlRG93bjogKGV2ZW50KSAtPlxuICAgIGV2ZW50Py5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKVxuICAgIHNlbGVjdGVkRW50cnkgPSBAc2VsZWN0ZWRFbnRyeSgpXG4gICAgaWYgc2VsZWN0ZWRFbnRyeT9cbiAgICAgIGlmIHNlbGVjdGVkRW50cnkuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXJlY3RvcnknKVxuICAgICAgICBpZiBAc2VsZWN0RW50cnkoc2VsZWN0ZWRFbnRyeS5lbnRyaWVzLmNoaWxkcmVuWzBdKVxuICAgICAgICAgIEBzY3JvbGxUb0VudHJ5KEBzZWxlY3RlZEVudHJ5KCksIGZhbHNlKVxuICAgICAgICAgIHJldHVyblxuXG4gICAgICBpZiBuZXh0RW50cnkgPSBAbmV4dEVudHJ5KHNlbGVjdGVkRW50cnkpXG4gICAgICAgIEBzZWxlY3RFbnRyeShuZXh0RW50cnkpXG4gICAgZWxzZVxuICAgICAgQHNlbGVjdEVudHJ5KEByb290c1swXSlcblxuICAgIEBzY3JvbGxUb0VudHJ5KEBzZWxlY3RlZEVudHJ5KCksIGZhbHNlKVxuXG4gIG1vdmVVcDogKGV2ZW50KSAtPlxuICAgIGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpXG4gICAgc2VsZWN0ZWRFbnRyeSA9IEBzZWxlY3RlZEVudHJ5KClcbiAgICBpZiBzZWxlY3RlZEVudHJ5P1xuICAgICAgaWYgcHJldmlvdXNFbnRyeSA9IEBwcmV2aW91c0VudHJ5KHNlbGVjdGVkRW50cnkpXG4gICAgICAgIEBzZWxlY3RFbnRyeShwcmV2aW91c0VudHJ5KVxuICAgICAgZWxzZVxuICAgICAgICBAc2VsZWN0RW50cnkoc2VsZWN0ZWRFbnRyeS5wYXJlbnRFbGVtZW50LmNsb3Nlc3QoJy5kaXJlY3RvcnknKSlcbiAgICBlbHNlXG4gICAgICBlbnRyaWVzID0gQGxpc3QucXVlcnlTZWxlY3RvckFsbCgnLmVudHJ5JylcbiAgICAgIEBzZWxlY3RFbnRyeShlbnRyaWVzW2VudHJpZXMubGVuZ3RoIC0gMV0pXG5cbiAgICBAc2Nyb2xsVG9FbnRyeShAc2VsZWN0ZWRFbnRyeSgpLCBmYWxzZSlcblxuICBuZXh0RW50cnk6IChlbnRyeSkgLT5cbiAgICBjdXJyZW50RW50cnkgPSBlbnRyeVxuICAgIHdoaWxlIGN1cnJlbnRFbnRyeT9cbiAgICAgIGlmIGN1cnJlbnRFbnRyeS5uZXh0U2libGluZz9cbiAgICAgICAgY3VycmVudEVudHJ5ID0gY3VycmVudEVudHJ5Lm5leHRTaWJsaW5nXG4gICAgICAgIGlmIGN1cnJlbnRFbnRyeS5tYXRjaGVzKCcuZW50cnknKVxuICAgICAgICAgIHJldHVybiBjdXJyZW50RW50cnlcbiAgICAgIGVsc2VcbiAgICAgICAgY3VycmVudEVudHJ5ID0gY3VycmVudEVudHJ5LnBhcmVudEVsZW1lbnQuY2xvc2VzdCgnLmRpcmVjdG9yeScpXG5cbiAgICByZXR1cm4gbnVsbFxuXG4gIHByZXZpb3VzRW50cnk6IChlbnRyeSkgLT5cbiAgICBwcmV2aW91c0VudHJ5ID0gZW50cnkucHJldmlvdXNTaWJsaW5nXG4gICAgd2hpbGUgcHJldmlvdXNFbnRyeT8gYW5kIG5vdCBwcmV2aW91c0VudHJ5Lm1hdGNoZXMoJy5lbnRyeScpXG4gICAgICBwcmV2aW91c0VudHJ5ID0gcHJldmlvdXNFbnRyeS5wcmV2aW91c1NpYmxpbmdcblxuICAgIHJldHVybiBudWxsIHVubGVzcyBwcmV2aW91c0VudHJ5P1xuXG4gICAgIyBJZiB0aGUgcHJldmlvdXMgZW50cnkgaXMgYW4gZXhwYW5kZWQgZGlyZWN0b3J5LFxuICAgICMgd2UgbmVlZCB0byBzZWxlY3QgdGhlIGxhc3QgZW50cnkgaW4gdGhhdCBkaXJlY3RvcnksXG4gICAgIyBub3QgdGhlIGRpcmVjdG9yeSBpdHNlbGZcbiAgICBpZiBwcmV2aW91c0VudHJ5Lm1hdGNoZXMoJy5kaXJlY3RvcnkuZXhwYW5kZWQnKVxuICAgICAgZW50cmllcyA9IHByZXZpb3VzRW50cnkucXVlcnlTZWxlY3RvckFsbCgnLmVudHJ5JylcbiAgICAgIHJldHVybiBlbnRyaWVzW2VudHJpZXMubGVuZ3RoIC0gMV0gaWYgZW50cmllcy5sZW5ndGggPiAwXG5cbiAgICByZXR1cm4gcHJldmlvdXNFbnRyeVxuXG4gIGV4cGFuZERpcmVjdG9yeTogKGlzUmVjdXJzaXZlPWZhbHNlKSAtPlxuICAgIHNlbGVjdGVkRW50cnkgPSBAc2VsZWN0ZWRFbnRyeSgpXG4gICAgcmV0dXJuIHVubGVzcyBzZWxlY3RlZEVudHJ5P1xuXG4gICAgZGlyZWN0b3J5ID0gc2VsZWN0ZWRFbnRyeS5jbG9zZXN0KCcuZGlyZWN0b3J5JylcbiAgICBpZiBpc1JlY3Vyc2l2ZSBpcyBmYWxzZSBhbmQgZGlyZWN0b3J5LmlzRXhwYW5kZWRcbiAgICAgICMgU2VsZWN0IHRoZSBmaXJzdCBlbnRyeSBpbiB0aGUgZXhwYW5kZWQgZm9sZGVyIGlmIGl0IGV4aXN0c1xuICAgICAgQG1vdmVEb3duKCkgaWYgZGlyZWN0b3J5LmRpcmVjdG9yeS5nZXRFbnRyaWVzKCkubGVuZ3RoID4gMFxuICAgIGVsc2VcbiAgICAgIGRpcmVjdG9yeS5leHBhbmQoaXNSZWN1cnNpdmUpXG5cbiAgY29sbGFwc2VEaXJlY3Rvcnk6IChpc1JlY3Vyc2l2ZT1mYWxzZSwgYWxsRGlyZWN0b3JpZXM9ZmFsc2UpIC0+XG4gICAgaWYgYWxsRGlyZWN0b3JpZXNcbiAgICAgIHJvb3QuY29sbGFwc2UodHJ1ZSkgZm9yIHJvb3QgaW4gQHJvb3RzXG4gICAgICByZXR1cm5cblxuICAgIHNlbGVjdGVkRW50cnkgPSBAc2VsZWN0ZWRFbnRyeSgpXG4gICAgcmV0dXJuIHVubGVzcyBzZWxlY3RlZEVudHJ5P1xuXG4gICAgaWYgZGlyZWN0b3J5ID0gc2VsZWN0ZWRFbnRyeS5jbG9zZXN0KCcuZXhwYW5kZWQuZGlyZWN0b3J5JylcbiAgICAgIGRpcmVjdG9yeS5jb2xsYXBzZShpc1JlY3Vyc2l2ZSlcbiAgICAgIEBzZWxlY3RFbnRyeShkaXJlY3RvcnkpXG5cbiAgb3BlblNlbGVjdGVkRW50cnk6IChvcHRpb25zPXt9LCBleHBhbmREaXJlY3Rvcnk9ZmFsc2UpIC0+XG4gICAgc2VsZWN0ZWRFbnRyeSA9IEBzZWxlY3RlZEVudHJ5KClcbiAgICByZXR1cm4gdW5sZXNzIHNlbGVjdGVkRW50cnk/XG5cbiAgICBpZiBzZWxlY3RlZEVudHJ5LmNsYXNzTGlzdC5jb250YWlucygnZGlyZWN0b3J5JylcbiAgICAgIGlmIGV4cGFuZERpcmVjdG9yeVxuICAgICAgICBAZXhwYW5kRGlyZWN0b3J5KGZhbHNlKVxuICAgICAgZWxzZVxuICAgICAgICBzZWxlY3RlZEVudHJ5LnRvZ2dsZUV4cGFuc2lvbigpXG4gICAgZWxzZSBpZiBzZWxlY3RlZEVudHJ5LmNsYXNzTGlzdC5jb250YWlucygnZmlsZScpXG4gICAgICBpZiBhdG9tLmNvbmZpZy5nZXQoJ3RyZWUtdmlldy5hbHdheXNPcGVuRXhpc3RpbmcnKVxuICAgICAgICBvcHRpb25zID0gT2JqZWN0LmFzc2lnbiBzZWFyY2hBbGxQYW5lczogdHJ1ZSwgb3B0aW9uc1xuICAgICAgQG9wZW5BZnRlclByb21pc2Uoc2VsZWN0ZWRFbnRyeS5nZXRQYXRoKCksIG9wdGlvbnMpXG5cbiAgb3BlblNlbGVjdGVkRW50cnlTcGxpdDogKG9yaWVudGF0aW9uLCBzaWRlKSAtPlxuICAgIHNlbGVjdGVkRW50cnkgPSBAc2VsZWN0ZWRFbnRyeSgpXG4gICAgcmV0dXJuIHVubGVzcyBzZWxlY3RlZEVudHJ5P1xuXG4gICAgcGFuZSA9IGF0b20ud29ya3NwYWNlLmdldENlbnRlcigpLmdldEFjdGl2ZVBhbmUoKVxuICAgIGlmIHBhbmUgYW5kIHNlbGVjdGVkRW50cnkuY2xhc3NMaXN0LmNvbnRhaW5zKCdmaWxlJylcbiAgICAgIGlmIGF0b20ud29ya3NwYWNlLmdldENlbnRlcigpLmdldEFjdGl2ZVBhbmVJdGVtKClcbiAgICAgICAgc3BsaXQgPSBwYW5lLnNwbGl0IG9yaWVudGF0aW9uLCBzaWRlXG4gICAgICAgIGF0b20ud29ya3NwYWNlLm9wZW5VUklJblBhbmUgc2VsZWN0ZWRFbnRyeS5nZXRQYXRoKCksIHNwbGl0XG4gICAgICBlbHNlXG4gICAgICAgIEBvcGVuU2VsZWN0ZWRFbnRyeSB5ZXNcblxuICBvcGVuU2VsZWN0ZWRFbnRyeVJpZ2h0OiAtPlxuICAgIEBvcGVuU2VsZWN0ZWRFbnRyeVNwbGl0ICdob3Jpem9udGFsJywgJ2FmdGVyJ1xuXG4gIG9wZW5TZWxlY3RlZEVudHJ5TGVmdDogLT5cbiAgICBAb3BlblNlbGVjdGVkRW50cnlTcGxpdCAnaG9yaXpvbnRhbCcsICdiZWZvcmUnXG5cbiAgb3BlblNlbGVjdGVkRW50cnlVcDogLT5cbiAgICBAb3BlblNlbGVjdGVkRW50cnlTcGxpdCAndmVydGljYWwnLCAnYmVmb3JlJ1xuXG4gIG9wZW5TZWxlY3RlZEVudHJ5RG93bjogLT5cbiAgICBAb3BlblNlbGVjdGVkRW50cnlTcGxpdCAndmVydGljYWwnLCAnYWZ0ZXInXG5cbiAgb3BlblNlbGVjdGVkRW50cnlJblBhbmU6IChpbmRleCkgLT5cbiAgICBzZWxlY3RlZEVudHJ5ID0gQHNlbGVjdGVkRW50cnkoKVxuICAgIHJldHVybiB1bmxlc3Mgc2VsZWN0ZWRFbnRyeT9cblxuICAgIHBhbmUgPSBhdG9tLndvcmtzcGFjZS5nZXRDZW50ZXIoKS5nZXRQYW5lcygpW2luZGV4XVxuICAgIGlmIHBhbmUgYW5kIHNlbGVjdGVkRW50cnkuY2xhc3NMaXN0LmNvbnRhaW5zKCdmaWxlJylcbiAgICAgIGF0b20ud29ya3NwYWNlLm9wZW5VUklJblBhbmUgc2VsZWN0ZWRFbnRyeS5nZXRQYXRoKCksIHBhbmVcblxuICBtb3ZlU2VsZWN0ZWRFbnRyeTogLT5cbiAgICBpZiBAaGFzRm9jdXMoKVxuICAgICAgZW50cnkgPSBAc2VsZWN0ZWRFbnRyeSgpXG4gICAgICByZXR1cm4gaWYgbm90IGVudHJ5PyBvciBlbnRyeSBpbiBAcm9vdHNcbiAgICAgIG9sZFBhdGggPSBlbnRyeS5nZXRQYXRoKClcbiAgICBlbHNlXG4gICAgICBvbGRQYXRoID0gQGdldEFjdGl2ZVBhdGgoKVxuXG4gICAgaWYgb2xkUGF0aFxuICAgICAgZGlhbG9nID0gbmV3IE1vdmVEaWFsb2cgb2xkUGF0aCxcbiAgICAgICAgd2lsbE1vdmU6ICh7aW5pdGlhbFBhdGgsIG5ld1BhdGh9KSA9PlxuICAgICAgICAgIEBlbWl0dGVyLmVtaXQgJ3dpbGwtbW92ZS1lbnRyeScsIHtpbml0aWFsUGF0aCwgbmV3UGF0aH1cbiAgICAgICAgb25Nb3ZlOiAoe2luaXRpYWxQYXRoLCBuZXdQYXRofSkgPT5cbiAgICAgICAgICBAZW1pdHRlci5lbWl0ICdlbnRyeS1tb3ZlZCcsIHtpbml0aWFsUGF0aCwgbmV3UGF0aH1cbiAgICAgICAgb25Nb3ZlRmFpbGVkOiAoe2luaXRpYWxQYXRoLCBuZXdQYXRofSkgPT5cbiAgICAgICAgICBAZW1pdHRlci5lbWl0ICdtb3ZlLWVudHJ5LWZhaWxlZCcsIHtpbml0aWFsUGF0aCwgbmV3UGF0aH1cbiAgICAgIGRpYWxvZy5hdHRhY2goKVxuXG4gIHNob3dTZWxlY3RlZEVudHJ5SW5GaWxlTWFuYWdlcjogLT5cbiAgICByZXR1cm4gdW5sZXNzIGZpbGVQYXRoID0gQHNlbGVjdGVkRW50cnkoKT8uZ2V0UGF0aCgpXG5cbiAgICByZXR1cm4gdW5sZXNzIGZzLmV4aXN0c1N5bmMoZmlsZVBhdGgpXG4gICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkV2FybmluZyhcIlVuYWJsZSB0byBzaG93ICN7ZmlsZVBhdGh9IGluICN7QGdldEZpbGVNYW5hZ2VyTmFtZSgpfVwiKVxuXG4gICAgc2hlbGwuc2hvd0l0ZW1JbkZvbGRlcihmaWxlUGF0aClcblxuICBzaG93Q3VycmVudEZpbGVJbkZpbGVNYW5hZ2VyOiAtPlxuICAgIHJldHVybiB1bmxlc3MgZmlsZVBhdGggPSBhdG9tLndvcmtzcGFjZS5nZXRDZW50ZXIoKS5nZXRBY3RpdmVUZXh0RWRpdG9yKCk/LmdldFBhdGgoKVxuXG4gICAgcmV0dXJuIHVubGVzcyBmcy5leGlzdHNTeW5jKGZpbGVQYXRoKVxuICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZFdhcm5pbmcoXCJVbmFibGUgdG8gc2hvdyAje2ZpbGVQYXRofSBpbiAje0BnZXRGaWxlTWFuYWdlck5hbWUoKX1cIilcblxuICAgIHNoZWxsLnNob3dJdGVtSW5Gb2xkZXIoZmlsZVBhdGgpXG5cbiAgZ2V0RmlsZU1hbmFnZXJOYW1lOiAtPlxuICAgIHN3aXRjaCBwcm9jZXNzLnBsYXRmb3JtXG4gICAgICB3aGVuICdkYXJ3aW4nXG4gICAgICAgIHJldHVybiAnRmluZGVyJ1xuICAgICAgd2hlbiAnd2luMzInXG4gICAgICAgIHJldHVybiAnRXhwbG9yZXInXG4gICAgICBlbHNlXG4gICAgICAgIHJldHVybiAnRmlsZSBNYW5hZ2VyJ1xuXG4gIG9wZW5TZWxlY3RlZEVudHJ5SW5OZXdXaW5kb3c6IC0+XG4gICAgaWYgcGF0aFRvT3BlbiA9IEBzZWxlY3RlZEVudHJ5KCk/LmdldFBhdGgoKVxuICAgICAgYXRvbS5vcGVuKHtwYXRoc1RvT3BlbjogW3BhdGhUb09wZW5dLCBuZXdXaW5kb3c6IHRydWV9KVxuXG4gIGNvcHlTZWxlY3RlZEVudHJ5OiAtPlxuICAgIGlmIEBoYXNGb2N1cygpXG4gICAgICBlbnRyeSA9IEBzZWxlY3RlZEVudHJ5KClcbiAgICAgIHJldHVybiBpZiBlbnRyeSBpbiBAcm9vdHNcbiAgICAgIG9sZFBhdGggPSBlbnRyeT8uZ2V0UGF0aCgpXG4gICAgZWxzZVxuICAgICAgb2xkUGF0aCA9IEBnZXRBY3RpdmVQYXRoKClcbiAgICByZXR1cm4gdW5sZXNzIG9sZFBhdGhcblxuICAgIGRpYWxvZyA9IG5ldyBDb3B5RGlhbG9nIG9sZFBhdGgsXG4gICAgICBvbkNvcHk6ICh7aW5pdGlhbFBhdGgsIG5ld1BhdGh9KSA9PlxuICAgICAgICBAZW1pdHRlci5lbWl0ICdlbnRyeS1jb3BpZWQnLCB7aW5pdGlhbFBhdGgsIG5ld1BhdGh9XG4gICAgZGlhbG9nLmF0dGFjaCgpXG5cbiAgcmVtb3ZlU2VsZWN0ZWRFbnRyaWVzOiAtPlxuICAgIGlmIEBoYXNGb2N1cygpXG4gICAgICBzZWxlY3RlZFBhdGhzID0gQHNlbGVjdGVkUGF0aHMoKVxuICAgICAgc2VsZWN0ZWRFbnRyaWVzID0gQGdldFNlbGVjdGVkRW50cmllcygpXG4gICAgZWxzZSBpZiBhY3RpdmVQYXRoID0gQGdldEFjdGl2ZVBhdGgoKVxuICAgICAgc2VsZWN0ZWRQYXRocyA9IFthY3RpdmVQYXRoXVxuICAgICAgc2VsZWN0ZWRFbnRyaWVzID0gW0BlbnRyeUZvclBhdGgoYWN0aXZlUGF0aCldXG5cbiAgICByZXR1cm4gdW5sZXNzIHNlbGVjdGVkUGF0aHM/Lmxlbmd0aCA+IDBcblxuICAgIGZvciByb290IGluIEByb290c1xuICAgICAgaWYgcm9vdC5nZXRQYXRoKCkgaW4gc2VsZWN0ZWRQYXRoc1xuICAgICAgICBhdG9tLmNvbmZpcm0oe1xuICAgICAgICAgIG1lc3NhZ2U6IFwiVGhlIHJvb3QgZGlyZWN0b3J5ICcje3Jvb3QuZGlyZWN0b3J5Lm5hbWV9JyBjYW4ndCBiZSByZW1vdmVkLlwiLFxuICAgICAgICAgIGJ1dHRvbnM6IFsnT0snXVxuICAgICAgICB9LCAtPiAjIG5vb3BcbiAgICAgICAgKVxuICAgICAgICByZXR1cm5cblxuICAgIGF0b20uY29uZmlybSh7XG4gICAgICBtZXNzYWdlOiBcIkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhlIHNlbGVjdGVkICN7aWYgc2VsZWN0ZWRQYXRocy5sZW5ndGggPiAxIHRoZW4gJ2l0ZW1zJyBlbHNlICdpdGVtJ30/XCIsXG4gICAgICBkZXRhaWxlZE1lc3NhZ2U6IFwiWW91IGFyZSBkZWxldGluZzpcXG4je3NlbGVjdGVkUGF0aHMuam9pbignXFxuJyl9XCIsXG4gICAgICBidXR0b25zOiBbJ01vdmUgdG8gVHJhc2gnLCAnQ2FuY2VsJ11cbiAgICB9LCAocmVzcG9uc2UpID0+XG4gICAgICBpZiByZXNwb25zZSBpcyAwICMgTW92ZSB0byBUcmFzaFxuICAgICAgICBmYWlsZWREZWxldGlvbnMgPSBbXVxuICAgICAgICBmb3Igc2VsZWN0ZWRQYXRoIGluIHNlbGVjdGVkUGF0aHNcbiAgICAgICAgICAjIERvbid0IGRlbGV0ZSBlbnRyaWVzIHdoaWNoIG5vIGxvbmdlciBleGlzdC4gVGhpcyBjYW4gaGFwcGVuLCBmb3IgZXhhbXBsZSwgd2hlbjpcbiAgICAgICAgICAjICogVGhlIGVudHJ5IGlzIGRlbGV0ZWQgb3V0c2lkZSBvZiBBdG9tIGJlZm9yZSBcIk1vdmUgdG8gVHJhc2hcIiBpcyBzZWxlY3RlZFxuICAgICAgICAgICMgKiBBIGZvbGRlciBhbmQgb25lIG9mIGl0cyBjaGlsZHJlbiBhcmUgYm90aCBzZWxlY3RlZCBmb3IgZGVsZXRpb24sXG4gICAgICAgICAgIyAgIGJ1dCB0aGUgcGFyZW50IGZvbGRlciBpcyBkZWxldGVkIGZpcnN0XG4gICAgICAgICAgY29udGludWUgdW5sZXNzIGZzLmV4aXN0c1N5bmMoc2VsZWN0ZWRQYXRoKVxuXG4gICAgICAgICAgQGVtaXR0ZXIuZW1pdCAnd2lsbC1kZWxldGUtZW50cnknLCB7cGF0aFRvRGVsZXRlOiBzZWxlY3RlZFBhdGh9XG4gICAgICAgICAgaWYgc2hlbGwubW92ZUl0ZW1Ub1RyYXNoKHNlbGVjdGVkUGF0aClcbiAgICAgICAgICAgIEBlbWl0dGVyLmVtaXQgJ2VudHJ5LWRlbGV0ZWQnLCB7cGF0aFRvRGVsZXRlOiBzZWxlY3RlZFBhdGh9XG4gICAgICAgICAgZWxzZVxuICAgICAgICAgICAgQGVtaXR0ZXIuZW1pdCAnZGVsZXRlLWVudHJ5LWZhaWxlZCcsIHtwYXRoVG9EZWxldGU6IHNlbGVjdGVkUGF0aH1cbiAgICAgICAgICAgIGZhaWxlZERlbGV0aW9ucy5wdXNoIHNlbGVjdGVkUGF0aFxuXG4gICAgICAgICAgaWYgcmVwbyA9IHJlcG9Gb3JQYXRoKHNlbGVjdGVkUGF0aClcbiAgICAgICAgICAgIHJlcG8uZ2V0UGF0aFN0YXR1cyhzZWxlY3RlZFBhdGgpXG5cbiAgICAgICAgaWYgZmFpbGVkRGVsZXRpb25zLmxlbmd0aCA+IDBcbiAgICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRXJyb3IgQGZvcm1hdFRyYXNoRmFpbHVyZU1lc3NhZ2UoZmFpbGVkRGVsZXRpb25zKSxcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBAZm9ybWF0VHJhc2hFbmFibGVkTWVzc2FnZSgpXG4gICAgICAgICAgICBkZXRhaWw6IFwiI3tmYWlsZWREZWxldGlvbnMuam9pbignXFxuJyl9XCJcbiAgICAgICAgICAgIGRpc21pc3NhYmxlOiB0cnVlXG5cbiAgICAgICAgIyBGb2N1cyB0aGUgZmlyc3QgcGFyZW50IGZvbGRlclxuICAgICAgICBpZiBmaXJzdFNlbGVjdGVkRW50cnkgPSBzZWxlY3RlZEVudHJpZXNbMF1cbiAgICAgICAgICBAc2VsZWN0RW50cnkoZmlyc3RTZWxlY3RlZEVudHJ5LmNsb3Nlc3QoJy5kaXJlY3Rvcnk6bm90KC5zZWxlY3RlZCknKSlcbiAgICAgICAgQHVwZGF0ZVJvb3RzKCkgaWYgYXRvbS5jb25maWcuZ2V0KCd0cmVlLXZpZXcuc3F1YXNoRGlyZWN0b3J5TmFtZXMnKVxuICAgIClcblxuICBmb3JtYXRUcmFzaEZhaWx1cmVNZXNzYWdlOiAoZmFpbGVkRGVsZXRpb25zKSAtPlxuICAgIGZpbGVUZXh0ID0gaWYgZmFpbGVkRGVsZXRpb25zLmxlbmd0aCA+IDEgdGhlbiAnZmlsZXMnIGVsc2UgJ2ZpbGUnXG5cbiAgICBcIlRoZSBmb2xsb3dpbmcgI3tmaWxlVGV4dH0gY291bGRuJ3QgYmUgbW92ZWQgdG8gdGhlIHRyYXNoLlwiXG5cbiAgZm9ybWF0VHJhc2hFbmFibGVkTWVzc2FnZTogLT5cbiAgICBzd2l0Y2ggcHJvY2Vzcy5wbGF0Zm9ybVxuICAgICAgd2hlbiAnbGludXgnIHRoZW4gJ0lzIGBndmZzLXRyYXNoYCBpbnN0YWxsZWQ/J1xuICAgICAgd2hlbiAnZGFyd2luJyB0aGVuICdJcyBUcmFzaCBlbmFibGVkIG9uIHRoZSB2b2x1bWUgd2hlcmUgdGhlIGZpbGVzIGFyZSBzdG9yZWQ/J1xuICAgICAgd2hlbiAnd2luMzInIHRoZW4gJ0lzIHRoZXJlIGEgUmVjeWNsZSBCaW4gb24gdGhlIGRyaXZlIHdoZXJlIHRoZSBmaWxlcyBhcmUgc3RvcmVkPydcblxuICAjIFB1YmxpYzogQ29weSB0aGUgcGF0aCBvZiB0aGUgc2VsZWN0ZWQgZW50cnkgZWxlbWVudC5cbiAgIyAgICAgICAgIFNhdmUgdGhlIHBhdGggaW4gbG9jYWxTdG9yYWdlLCBzbyB0aGF0IGNvcHlpbmcgZnJvbSAyIGRpZmZlcmVudFxuICAjICAgICAgICAgaW5zdGFuY2VzIG9mIGF0b20gd29ya3MgYXMgaW50ZW5kZWRcbiAgI1xuICAjXG4gICMgUmV0dXJucyBgY29weVBhdGhgLlxuICBjb3B5U2VsZWN0ZWRFbnRyaWVzOiAtPlxuICAgIHNlbGVjdGVkUGF0aHMgPSBAc2VsZWN0ZWRQYXRocygpXG4gICAgcmV0dXJuIHVubGVzcyBzZWxlY3RlZFBhdGhzIGFuZCBzZWxlY3RlZFBhdGhzLmxlbmd0aCA+IDBcbiAgICAjIHNhdmUgdG8gbG9jYWxTdG9yYWdlIHNvIHdlIGNhbiBwYXN0ZSBhY3Jvc3MgbXVsdGlwbGUgb3BlbiBhcHBzXG4gICAgd2luZG93LmxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCd0cmVlLXZpZXc6Y3V0UGF0aCcpXG4gICAgd2luZG93LmxvY2FsU3RvcmFnZVsndHJlZS12aWV3OmNvcHlQYXRoJ10gPSBKU09OLnN0cmluZ2lmeShzZWxlY3RlZFBhdGhzKVxuXG4gICMgUHVibGljOiBDdXQgdGhlIHBhdGggb2YgdGhlIHNlbGVjdGVkIGVudHJ5IGVsZW1lbnQuXG4gICMgICAgICAgICBTYXZlIHRoZSBwYXRoIGluIGxvY2FsU3RvcmFnZSwgc28gdGhhdCBjdXR0aW5nIGZyb20gMiBkaWZmZXJlbnRcbiAgIyAgICAgICAgIGluc3RhbmNlcyBvZiBhdG9tIHdvcmtzIGFzIGludGVuZGVkXG4gICNcbiAgI1xuICAjIFJldHVybnMgYGN1dFBhdGhgXG4gIGN1dFNlbGVjdGVkRW50cmllczogLT5cbiAgICBzZWxlY3RlZFBhdGhzID0gQHNlbGVjdGVkUGF0aHMoKVxuICAgIHJldHVybiB1bmxlc3Mgc2VsZWN0ZWRQYXRocyBhbmQgc2VsZWN0ZWRQYXRocy5sZW5ndGggPiAwXG4gICAgIyBzYXZlIHRvIGxvY2FsU3RvcmFnZSBzbyB3ZSBjYW4gcGFzdGUgYWNyb3NzIG11bHRpcGxlIG9wZW4gYXBwc1xuICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgndHJlZS12aWV3OmNvcHlQYXRoJylcbiAgICB3aW5kb3cubG9jYWxTdG9yYWdlWyd0cmVlLXZpZXc6Y3V0UGF0aCddID0gSlNPTi5zdHJpbmdpZnkoc2VsZWN0ZWRQYXRocylcblxuICAjIFB1YmxpYzogUGFzdGUgYSBjb3BpZWQgb3IgY3V0IGl0ZW0uXG4gICMgICAgICAgICBJZiBhIGZpbGUgaXMgc2VsZWN0ZWQsIHRoZSBmaWxlJ3MgcGFyZW50IGRpcmVjdG9yeSBpcyB1c2VkIGFzIHRoZVxuICAjICAgICAgICAgcGFzdGUgZGVzdGluYXRpb24uXG4gIHBhc3RlRW50cmllczogLT5cbiAgICBzZWxlY3RlZEVudHJ5ID0gQHNlbGVjdGVkRW50cnkoKVxuICAgIHJldHVybiB1bmxlc3Mgc2VsZWN0ZWRFbnRyeVxuXG4gICAgY3V0UGF0aHMgPSBpZiB3aW5kb3cubG9jYWxTdG9yYWdlWyd0cmVlLXZpZXc6Y3V0UGF0aCddIHRoZW4gSlNPTi5wYXJzZSh3aW5kb3cubG9jYWxTdG9yYWdlWyd0cmVlLXZpZXc6Y3V0UGF0aCddKSBlbHNlIG51bGxcbiAgICBjb3BpZWRQYXRocyA9IGlmIHdpbmRvdy5sb2NhbFN0b3JhZ2VbJ3RyZWUtdmlldzpjb3B5UGF0aCddIHRoZW4gSlNPTi5wYXJzZSh3aW5kb3cubG9jYWxTdG9yYWdlWyd0cmVlLXZpZXc6Y29weVBhdGgnXSkgZWxzZSBudWxsXG4gICAgaW5pdGlhbFBhdGhzID0gY29waWVkUGF0aHMgb3IgY3V0UGF0aHNcbiAgICByZXR1cm4gdW5sZXNzIGluaXRpYWxQYXRocz8ubGVuZ3RoXG5cbiAgICBuZXdEaXJlY3RvcnlQYXRoID0gc2VsZWN0ZWRFbnRyeS5nZXRQYXRoKClcbiAgICBuZXdEaXJlY3RvcnlQYXRoID0gcGF0aC5kaXJuYW1lKG5ld0RpcmVjdG9yeVBhdGgpIGlmIHNlbGVjdGVkRW50cnkuY2xhc3NMaXN0LmNvbnRhaW5zKCdmaWxlJylcblxuICAgIGZvciBpbml0aWFsUGF0aCBpbiBpbml0aWFsUGF0aHNcbiAgICAgIGlmIGZzLmV4aXN0c1N5bmMoaW5pdGlhbFBhdGgpXG4gICAgICAgIGlmIGNvcGllZFBhdGhzXG4gICAgICAgICAgQGNvcHlFbnRyeShpbml0aWFsUGF0aCwgbmV3RGlyZWN0b3J5UGF0aClcbiAgICAgICAgZWxzZSBpZiBjdXRQYXRoc1xuICAgICAgICAgIGJyZWFrIHVubGVzcyBAbW92ZUVudHJ5KGluaXRpYWxQYXRoLCBuZXdEaXJlY3RvcnlQYXRoKVxuXG4gIGFkZDogKGlzQ3JlYXRpbmdGaWxlKSAtPlxuICAgIHNlbGVjdGVkRW50cnkgPSBAc2VsZWN0ZWRFbnRyeSgpID8gQHJvb3RzWzBdXG4gICAgc2VsZWN0ZWRQYXRoID0gc2VsZWN0ZWRFbnRyeT8uZ2V0UGF0aCgpID8gJydcblxuICAgIGRpYWxvZyA9IG5ldyBBZGREaWFsb2coc2VsZWN0ZWRQYXRoLCBpc0NyZWF0aW5nRmlsZSlcbiAgICBkaWFsb2cub25EaWRDcmVhdGVEaXJlY3RvcnkgKGNyZWF0ZWRQYXRoKSA9PlxuICAgICAgQGVudHJ5Rm9yUGF0aChjcmVhdGVkUGF0aCk/LnJlbG9hZCgpXG4gICAgICBAc2VsZWN0RW50cnlGb3JQYXRoKGNyZWF0ZWRQYXRoKVxuICAgICAgQHVwZGF0ZVJvb3RzKCkgaWYgYXRvbS5jb25maWcuZ2V0KCd0cmVlLXZpZXcuc3F1YXNoRGlyZWN0b3J5TmFtZXMnKVxuICAgICAgQGVtaXR0ZXIuZW1pdCAnZGlyZWN0b3J5LWNyZWF0ZWQnLCB7cGF0aDogY3JlYXRlZFBhdGh9XG4gICAgZGlhbG9nLm9uRGlkQ3JlYXRlRmlsZSAoY3JlYXRlZFBhdGgpID0+XG4gICAgICBAZW50cnlGb3JQYXRoKGNyZWF0ZWRQYXRoKT8ucmVsb2FkKClcbiAgICAgIGF0b20ud29ya3NwYWNlLm9wZW4oY3JlYXRlZFBhdGgpXG4gICAgICBAdXBkYXRlUm9vdHMoKSBpZiBhdG9tLmNvbmZpZy5nZXQoJ3RyZWUtdmlldy5zcXVhc2hEaXJlY3RvcnlOYW1lcycpXG4gICAgICBAZW1pdHRlci5lbWl0ICdmaWxlLWNyZWF0ZWQnLCB7cGF0aDogY3JlYXRlZFBhdGh9XG4gICAgZGlhbG9nLmF0dGFjaCgpXG5cbiAgcmVtb3ZlUHJvamVjdEZvbGRlcjogKGUpIC0+XG4gICAgIyBSZW1vdmUgdGhlIHRhcmdldGVkIHByb2plY3QgZm9sZGVyIChnZW5lcmFsbHkgdGhpcyBvbmx5IGhhcHBlbnMgdGhyb3VnaCB0aGUgY29udGV4dCBtZW51KVxuICAgIHBhdGhUb1JlbW92ZSA9IGUudGFyZ2V0LmNsb3Nlc3QoXCIucHJvamVjdC1yb290ID4gLmhlYWRlclwiKT8ucXVlcnlTZWxlY3RvcihcIi5uYW1lXCIpPy5kYXRhc2V0LnBhdGhcbiAgICAjIElmIGFuIGVudHJ5IGlzIHNlbGVjdGVkLCByZW1vdmUgdGhhdCBlbnRyeSdzIHByb2plY3QgZm9sZGVyXG4gICAgcGF0aFRvUmVtb3ZlID89IEBzZWxlY3RlZEVudHJ5KCk/LmNsb3Nlc3QoXCIucHJvamVjdC1yb290XCIpPy5xdWVyeVNlbGVjdG9yKFwiLmhlYWRlclwiKT8ucXVlcnlTZWxlY3RvcihcIi5uYW1lXCIpPy5kYXRhc2V0LnBhdGhcbiAgICAjIEZpbmFsbHksIGlmIG9ubHkgb25lIHByb2plY3QgZm9sZGVyIGV4aXN0cyBhbmQgbm90aGluZyBpcyBzZWxlY3RlZCwgcmVtb3ZlIHRoYXQgZm9sZGVyXG4gICAgcGF0aFRvUmVtb3ZlID89IEByb290c1swXS5xdWVyeVNlbGVjdG9yKFwiLmhlYWRlclwiKT8ucXVlcnlTZWxlY3RvcihcIi5uYW1lXCIpPy5kYXRhc2V0LnBhdGggaWYgQHJvb3RzLmxlbmd0aCBpcyAxXG4gICAgYXRvbS5wcm9qZWN0LnJlbW92ZVBhdGgocGF0aFRvUmVtb3ZlKSBpZiBwYXRoVG9SZW1vdmU/XG5cbiAgc2VsZWN0ZWRFbnRyeTogLT5cbiAgICBAbGlzdC5xdWVyeVNlbGVjdG9yKCcuc2VsZWN0ZWQnKVxuXG4gIHNlbGVjdEVudHJ5OiAoZW50cnkpIC0+XG4gICAgcmV0dXJuIHVubGVzcyBlbnRyeT9cblxuICAgIEBzZWxlY3RlZFBhdGggPSBlbnRyeS5nZXRQYXRoKClcbiAgICBAbGFzdEZvY3VzZWRFbnRyeSA9IGVudHJ5XG5cbiAgICBzZWxlY3RlZEVudHJpZXMgPSBAZ2V0U2VsZWN0ZWRFbnRyaWVzKClcbiAgICBpZiBzZWxlY3RlZEVudHJpZXMubGVuZ3RoID4gMSBvciBzZWxlY3RlZEVudHJpZXNbMF0gaXNudCBlbnRyeVxuICAgICAgQGRlc2VsZWN0KHNlbGVjdGVkRW50cmllcylcbiAgICAgIGVudHJ5LmNsYXNzTGlzdC5hZGQoJ3NlbGVjdGVkJylcbiAgICBlbnRyeVxuXG4gIGdldFNlbGVjdGVkRW50cmllczogLT5cbiAgICBAbGlzdC5xdWVyeVNlbGVjdG9yQWxsKCcuc2VsZWN0ZWQnKVxuXG4gIGRlc2VsZWN0OiAoZWxlbWVudHNUb0Rlc2VsZWN0PUBnZXRTZWxlY3RlZEVudHJpZXMoKSkgLT5cbiAgICBzZWxlY3RlZC5jbGFzc0xpc3QucmVtb3ZlKCdzZWxlY3RlZCcpIGZvciBzZWxlY3RlZCBpbiBlbGVtZW50c1RvRGVzZWxlY3RcbiAgICB1bmRlZmluZWRcblxuICBzY3JvbGxUb3A6ICh0b3ApIC0+XG4gICAgaWYgdG9wP1xuICAgICAgQGVsZW1lbnQuc2Nyb2xsVG9wID0gdG9wXG4gICAgZWxzZVxuICAgICAgQGVsZW1lbnQuc2Nyb2xsVG9wXG5cbiAgc2Nyb2xsQm90dG9tOiAoYm90dG9tKSAtPlxuICAgIGlmIGJvdHRvbT9cbiAgICAgIEBlbGVtZW50LnNjcm9sbFRvcCA9IGJvdHRvbSAtIEBlbGVtZW50Lm9mZnNldEhlaWdodFxuICAgIGVsc2VcbiAgICAgIEBlbGVtZW50LnNjcm9sbFRvcCArIEBlbGVtZW50Lm9mZnNldEhlaWdodFxuXG4gIHNjcm9sbFRvRW50cnk6IChlbnRyeSwgY2VudGVyPXRydWUpIC0+XG4gICAgZWxlbWVudCA9IGlmIGVudHJ5Py5jbGFzc0xpc3QuY29udGFpbnMoJ2RpcmVjdG9yeScpIHRoZW4gZW50cnkuaGVhZGVyIGVsc2UgZW50cnlcbiAgICBlbGVtZW50Py5zY3JvbGxJbnRvVmlld0lmTmVlZGVkKGNlbnRlcilcblxuICBzY3JvbGxUb0JvdHRvbTogLT5cbiAgICBpZiBsYXN0RW50cnkgPSBfLmxhc3QoQGxpc3QucXVlcnlTZWxlY3RvckFsbCgnLmVudHJ5JykpXG4gICAgICBAc2VsZWN0RW50cnkobGFzdEVudHJ5KVxuICAgICAgQHNjcm9sbFRvRW50cnkobGFzdEVudHJ5KVxuXG4gIHNjcm9sbFRvVG9wOiAtPlxuICAgIEBzZWxlY3RFbnRyeShAcm9vdHNbMF0pIGlmIEByb290c1swXT9cbiAgICBAZWxlbWVudC5zY3JvbGxUb3AgPSAwXG5cbiAgcGFnZVVwOiAtPlxuICAgIEBlbGVtZW50LnNjcm9sbFRvcCAtPSBAZWxlbWVudC5vZmZzZXRIZWlnaHRcblxuICBwYWdlRG93bjogLT5cbiAgICBAZWxlbWVudC5zY3JvbGxUb3AgKz0gQGVsZW1lbnQub2Zmc2V0SGVpZ2h0XG5cbiAgIyBDb3BpZXMgYW4gZW50cnkgZnJvbSBgaW5pdGlhbFBhdGhgIHRvIGBuZXdEaXJlY3RvcnlQYXRoYFxuICAjIElmIHRoZSBlbnRyeSBhbHJlYWR5IGV4aXN0cyBpbiBgbmV3RGlyZWN0b3J5UGF0aGAsIGEgbnVtYmVyIGlzIGFwcGVuZGVkIHRvIHRoZSBiYXNlbmFtZVxuICBjb3B5RW50cnk6IChpbml0aWFsUGF0aCwgbmV3RGlyZWN0b3J5UGF0aCkgLT5cbiAgICBpbml0aWFsUGF0aElzRGlyZWN0b3J5ID0gZnMuaXNEaXJlY3RvcnlTeW5jKGluaXRpYWxQYXRoKVxuXG4gICAgIyBEbyBub3QgYWxsb3cgY29weWluZyB0ZXN0L2EvIGludG8gdGVzdC9hL2IvXG4gICAgIyBOb3RlOiBBIHRyYWlsaW5nIHBhdGguc2VwIGlzIGFkZGVkIHRvIHByZXZlbnQgZmFsc2UgcG9zaXRpdmVzLCBzdWNoIGFzIHRlc3QvYSAtPiB0ZXN0L2FiXG4gICAgcmVhbE5ld0RpcmVjdG9yeVBhdGggPSBmcy5yZWFscGF0aFN5bmMobmV3RGlyZWN0b3J5UGF0aCkgKyBwYXRoLnNlcFxuICAgIHJlYWxJbml0aWFsUGF0aCA9IGZzLnJlYWxwYXRoU3luYyhpbml0aWFsUGF0aCkgKyBwYXRoLnNlcFxuICAgIGlmIGluaXRpYWxQYXRoSXNEaXJlY3RvcnkgYW5kIHJlYWxOZXdEaXJlY3RvcnlQYXRoLnN0YXJ0c1dpdGgocmVhbEluaXRpYWxQYXRoKVxuICAgICAgdW5sZXNzIGZzLmlzU3ltYm9saWNMaW5rU3luYyhpbml0aWFsUGF0aClcbiAgICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZFdhcm5pbmcoJ0Nhbm5vdCBjb3B5IGEgZm9sZGVyIGludG8gaXRzZWxmJylcbiAgICAgICAgcmV0dXJuXG5cbiAgICBuZXdQYXRoID0gcGF0aC5qb2luKG5ld0RpcmVjdG9yeVBhdGgsIHBhdGguYmFzZW5hbWUoaW5pdGlhbFBhdGgpKVxuXG4gICAgIyBhcHBlbmQgYSBudW1iZXIgdG8gdGhlIGZpbGUgaWYgYW4gaXRlbSB3aXRoIHRoZSBzYW1lIG5hbWUgZXhpc3RzXG4gICAgZmlsZUNvdW50ZXIgPSAwXG4gICAgb3JpZ2luYWxOZXdQYXRoID0gbmV3UGF0aFxuICAgIHdoaWxlIGZzLmV4aXN0c1N5bmMobmV3UGF0aClcbiAgICAgIGlmIGluaXRpYWxQYXRoSXNEaXJlY3RvcnlcbiAgICAgICAgbmV3UGF0aCA9IFwiI3tvcmlnaW5hbE5ld1BhdGh9I3tmaWxlQ291bnRlcn1cIlxuICAgICAgZWxzZVxuICAgICAgICBleHRlbnNpb24gPSBnZXRGdWxsRXh0ZW5zaW9uKG9yaWdpbmFsTmV3UGF0aClcbiAgICAgICAgZmlsZVBhdGggPSBwYXRoLmpvaW4ocGF0aC5kaXJuYW1lKG9yaWdpbmFsTmV3UGF0aCksIHBhdGguYmFzZW5hbWUob3JpZ2luYWxOZXdQYXRoLCBleHRlbnNpb24pKVxuICAgICAgICBuZXdQYXRoID0gXCIje2ZpbGVQYXRofSN7ZmlsZUNvdW50ZXJ9I3tleHRlbnNpb259XCJcbiAgICAgIGZpbGVDb3VudGVyICs9IDFcblxuICAgIHRyeVxuICAgICAgQGVtaXR0ZXIuZW1pdCAnd2lsbC1jb3B5LWVudHJ5Jywge2luaXRpYWxQYXRoLCBuZXdQYXRofVxuICAgICAgaWYgaW5pdGlhbFBhdGhJc0RpcmVjdG9yeVxuICAgICAgICAjIHVzZSBmcy5jb3B5IHRvIGNvcHkgZGlyZWN0b3JpZXMgc2luY2UgcmVhZC93cml0ZSB3aWxsIGZhaWwgZm9yIGRpcmVjdG9yaWVzXG4gICAgICAgIGZzLmNvcHlTeW5jKGluaXRpYWxQYXRoLCBuZXdQYXRoKVxuICAgICAgZWxzZVxuICAgICAgICAjIHJlYWQgdGhlIG9sZCBmaWxlIGFuZCB3cml0ZSBhIG5ldyBvbmUgYXQgdGFyZ2V0IGxvY2F0aW9uXG4gICAgICAgICMgVE9ETzogUmVwbGFjZSB3aXRoIGZzLmNvcHlGaWxlU3luY1xuICAgICAgICBmcy53cml0ZUZpbGVTeW5jKG5ld1BhdGgsIGZzLnJlYWRGaWxlU3luYyhpbml0aWFsUGF0aCkpXG4gICAgICBAZW1pdHRlci5lbWl0ICdlbnRyeS1jb3BpZWQnLCB7aW5pdGlhbFBhdGgsIG5ld1BhdGh9XG5cbiAgICAgIGlmIHJlcG8gPSByZXBvRm9yUGF0aChuZXdQYXRoKVxuICAgICAgICByZXBvLmdldFBhdGhTdGF0dXMoaW5pdGlhbFBhdGgpXG4gICAgICAgIHJlcG8uZ2V0UGF0aFN0YXR1cyhuZXdQYXRoKVxuXG4gICAgY2F0Y2ggZXJyb3JcbiAgICAgIEBlbWl0dGVyLmVtaXQgJ2NvcHktZW50cnktZmFpbGVkJywge2luaXRpYWxQYXRoLCBuZXdQYXRofVxuICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZFdhcm5pbmcoXCJGYWlsZWQgdG8gY29weSBlbnRyeSAje2luaXRpYWxQYXRofSB0byAje25ld0RpcmVjdG9yeVBhdGh9XCIsIGRldGFpbDogZXJyb3IubWVzc2FnZSlcblxuICAjIE1vdmVzIGFuIGVudHJ5IGZyb20gYGluaXRpYWxQYXRoYCB0byBgbmV3RGlyZWN0b3J5UGF0aGBcbiAgbW92ZUVudHJ5OiAoaW5pdGlhbFBhdGgsIG5ld0RpcmVjdG9yeVBhdGgpIC0+XG4gICAgIyBEbyBub3QgYWxsb3cgbW92aW5nIHRlc3QvYS8gaW50byB0ZXN0L2EvYi9cbiAgICAjIE5vdGU6IEEgdHJhaWxpbmcgcGF0aC5zZXAgaXMgYWRkZWQgdG8gcHJldmVudCBmYWxzZSBwb3NpdGl2ZXMsIHN1Y2ggYXMgdGVzdC9hIC0+IHRlc3QvYWJcbiAgICB0cnlcbiAgICAgIHJlYWxOZXdEaXJlY3RvcnlQYXRoID0gZnMucmVhbHBhdGhTeW5jKG5ld0RpcmVjdG9yeVBhdGgpICsgcGF0aC5zZXBcbiAgICAgIHJlYWxJbml0aWFsUGF0aCA9IGZzLnJlYWxwYXRoU3luYyhpbml0aWFsUGF0aCkgKyBwYXRoLnNlcFxuICAgICAgaWYgZnMuaXNEaXJlY3RvcnlTeW5jKGluaXRpYWxQYXRoKSBhbmQgcmVhbE5ld0RpcmVjdG9yeVBhdGguc3RhcnRzV2l0aChyZWFsSW5pdGlhbFBhdGgpXG4gICAgICAgIHVubGVzcyBmcy5pc1N5bWJvbGljTGlua1N5bmMoaW5pdGlhbFBhdGgpXG4gICAgICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZFdhcm5pbmcoJ0Nhbm5vdCBtb3ZlIGEgZm9sZGVyIGludG8gaXRzZWxmJylcbiAgICAgICAgICByZXR1cm5cbiAgICBjYXRjaCBlcnJvclxuICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZFdhcm5pbmcoXCJGYWlsZWQgdG8gbW92ZSBlbnRyeSAje2luaXRpYWxQYXRofSB0byAje25ld0RpcmVjdG9yeVBhdGh9XCIsIGRldGFpbDogZXJyb3IubWVzc2FnZSlcblxuICAgIG5ld1BhdGggPSBwYXRoLmpvaW4obmV3RGlyZWN0b3J5UGF0aCwgcGF0aC5iYXNlbmFtZShpbml0aWFsUGF0aCkpXG5cbiAgICB0cnlcbiAgICAgIEBlbWl0dGVyLmVtaXQgJ3dpbGwtbW92ZS1lbnRyeScsIHtpbml0aWFsUGF0aCwgbmV3UGF0aH1cbiAgICAgIGZzLm1vdmVTeW5jKGluaXRpYWxQYXRoLCBuZXdQYXRoKVxuICAgICAgQGVtaXR0ZXIuZW1pdCAnZW50cnktbW92ZWQnLCB7aW5pdGlhbFBhdGgsIG5ld1BhdGh9XG5cbiAgICAgIGlmIHJlcG8gPSByZXBvRm9yUGF0aChuZXdQYXRoKVxuICAgICAgICByZXBvLmdldFBhdGhTdGF0dXMoaW5pdGlhbFBhdGgpXG4gICAgICAgIHJlcG8uZ2V0UGF0aFN0YXR1cyhuZXdQYXRoKVxuXG4gICAgY2F0Y2ggZXJyb3JcbiAgICAgIGlmIGVycm9yLmNvZGUgaXMgJ0VFWElTVCdcbiAgICAgICAgcmV0dXJuIEBtb3ZlQ29uZmxpY3RpbmdFbnRyeShpbml0aWFsUGF0aCwgbmV3UGF0aCwgbmV3RGlyZWN0b3J5UGF0aClcbiAgICAgIGVsc2VcbiAgICAgICAgQGVtaXR0ZXIuZW1pdCAnbW92ZS1lbnRyeS1mYWlsZWQnLCB7aW5pdGlhbFBhdGgsIG5ld1BhdGh9XG4gICAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRXYXJuaW5nKFwiRmFpbGVkIHRvIG1vdmUgZW50cnkgI3tpbml0aWFsUGF0aH0gdG8gI3tuZXdEaXJlY3RvcnlQYXRofVwiLCBkZXRhaWw6IGVycm9yLm1lc3NhZ2UpXG5cbiAgICByZXR1cm4gdHJ1ZVxuXG4gIG1vdmVDb25mbGljdGluZ0VudHJ5OiAoaW5pdGlhbFBhdGgsIG5ld1BhdGgsIG5ld0RpcmVjdG9yeVBhdGgpID0+XG4gICAgdHJ5XG4gICAgICB1bmxlc3MgZnMuaXNEaXJlY3RvcnlTeW5jKGluaXRpYWxQYXRoKVxuICAgICAgICAjIEZpbGVzLCBzeW1saW5rcywgYW55dGhpbmcgYnV0IGEgZGlyZWN0b3J5XG4gICAgICAgIGNob3NlbiA9IGF0b20uY29uZmlybVxuICAgICAgICAgIG1lc3NhZ2U6IFwiJyN7cGF0aC5yZWxhdGl2ZShuZXdEaXJlY3RvcnlQYXRoLCBuZXdQYXRoKX0nIGFscmVhZHkgZXhpc3RzXCJcbiAgICAgICAgICBkZXRhaWxlZE1lc3NhZ2U6ICdEbyB5b3Ugd2FudCB0byByZXBsYWNlIGl0PydcbiAgICAgICAgICBidXR0b25zOiBbJ1JlcGxhY2UgZmlsZScsICdTa2lwJywgJ0NhbmNlbCddXG5cbiAgICAgICAgc3dpdGNoIGNob3NlblxuICAgICAgICAgIHdoZW4gMCAjIFJlcGxhY2VcbiAgICAgICAgICAgIGZzLnJlbmFtZVN5bmMoaW5pdGlhbFBhdGgsIG5ld1BhdGgpXG4gICAgICAgICAgICBAZW1pdHRlci5lbWl0ICdlbnRyeS1tb3ZlZCcsIHtpbml0aWFsUGF0aCwgbmV3UGF0aH1cblxuICAgICAgICAgICAgaWYgcmVwbyA9IHJlcG9Gb3JQYXRoKG5ld1BhdGgpXG4gICAgICAgICAgICAgIHJlcG8uZ2V0UGF0aFN0YXR1cyhpbml0aWFsUGF0aClcbiAgICAgICAgICAgICAgcmVwby5nZXRQYXRoU3RhdHVzKG5ld1BhdGgpXG4gICAgICAgICAgICBicmVha1xuICAgICAgICAgIHdoZW4gMiAjIENhbmNlbFxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICBlbHNlXG4gICAgICAgIGVudHJpZXMgPSBmcy5yZWFkZGlyU3luYyhpbml0aWFsUGF0aClcbiAgICAgICAgZm9yIGVudHJ5IGluIGVudHJpZXNcbiAgICAgICAgICBpZiBmcy5leGlzdHNTeW5jKHBhdGguam9pbihuZXdQYXRoLCBlbnRyeSkpXG4gICAgICAgICAgICByZXR1cm4gZmFsc2UgdW5sZXNzIEBtb3ZlQ29uZmxpY3RpbmdFbnRyeShwYXRoLmpvaW4oaW5pdGlhbFBhdGgsIGVudHJ5KSwgcGF0aC5qb2luKG5ld1BhdGgsIGVudHJ5KSwgbmV3RGlyZWN0b3J5UGF0aClcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICBAbW92ZUVudHJ5KHBhdGguam9pbihpbml0aWFsUGF0aCwgZW50cnkpLCBuZXdQYXRoKVxuXG4gICAgICAgICMgXCJNb3ZlXCIgdGhlIGNvbnRhaW5pbmcgZm9sZGVyIGJ5IGRlbGV0aW5nIGl0LCBzaW5jZSB3ZSd2ZSBhbHJlYWR5IG1vdmVkIGV2ZXJ5dGhpbmcgaW4gaXRcbiAgICAgICAgZnMucm1kaXJTeW5jKGluaXRpYWxQYXRoKSB1bmxlc3MgZnMucmVhZGRpclN5bmMoaW5pdGlhbFBhdGgpLmxlbmd0aFxuICAgIGNhdGNoIGVycm9yXG4gICAgICBAZW1pdHRlci5lbWl0ICdtb3ZlLWVudHJ5LWZhaWxlZCcsIHtpbml0aWFsUGF0aCwgbmV3UGF0aH1cbiAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRXYXJuaW5nKFwiRmFpbGVkIHRvIG1vdmUgZW50cnkgI3tpbml0aWFsUGF0aH0gdG8gI3tuZXdQYXRofVwiLCBkZXRhaWw6IGVycm9yLm1lc3NhZ2UpXG5cbiAgICByZXR1cm4gdHJ1ZVxuXG4gIG9uU3R5bGVzaGVldHNDaGFuZ2VkOiA9PlxuICAgICMgSWYgdmlzaWJsZSwgZm9yY2UgYSByZWRyYXcgc28gdGhlIHNjcm9sbGJhcnMgYXJlIHN0eWxlZCBjb3JyZWN0bHkgYmFzZWQgb25cbiAgICAjIHRoZSB0aGVtZVxuICAgIHJldHVybiB1bmxlc3MgQGlzVmlzaWJsZSgpXG4gICAgQGVsZW1lbnQuc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgIEBlbGVtZW50Lm9mZnNldFdpZHRoXG4gICAgQGVsZW1lbnQuc3R5bGUuZGlzcGxheSA9ICcnXG5cbiAgb25Nb3VzZURvd246IChlKSAtPlxuICAgIHJldHVybiB1bmxlc3MgZW50cnlUb1NlbGVjdCA9IGUudGFyZ2V0LmNsb3Nlc3QoJy5lbnRyeScpXG5cbiAgICBlLnN0b3BQcm9wYWdhdGlvbigpXG5cbiAgICAjIFRPRE86IG1ldGErY2xpY2sgYW5kIGN0cmwrY2xpY2sgc2hvdWxkIG5vdCBkbyB0aGUgc2FtZSB0aGluZyBvbiBXaW5kb3dzLlxuICAgICMgICAgICAgUmlnaHQgbm93IHJlbW92aW5nIG1ldGFLZXkgaWYgcGxhdGZvcm0gaXMgbm90IGRhcndpbiBicmVha3MgdGVzdHNcbiAgICAjICAgICAgIHRoYXQgc2V0IHRoZSBtZXRhS2V5IHRvIHRydWUgd2hlbiBzaW11bGF0aW5nIGEgY21kK2NsaWNrIG9uIG1hY29zXG4gICAgIyAgICAgICBhbmQgY3RybCtjbGljayBvbiB3aW5kb3dzIGFuZCBsaW51eC5cbiAgICBjbWRLZXkgPSBlLm1ldGFLZXkgb3IgKGUuY3RybEtleSBhbmQgcHJvY2Vzcy5wbGF0Zm9ybSBpc250ICdkYXJ3aW4nKVxuXG4gICAgIyByZXR1cm4gZWFybHkgaWYgY2xpY2tpbmcgb24gYSBzZWxlY3RlZCBlbnRyeVxuICAgIGlmIGVudHJ5VG9TZWxlY3QuY2xhc3NMaXN0LmNvbnRhaW5zKCdzZWxlY3RlZCcpXG4gICAgICAjIG1vdXNlIHJpZ2h0IGNsaWNrIG9yIGN0cmwgY2xpY2sgYXMgcmlnaHQgY2xpY2sgb24gZGFyd2luIHBsYXRmb3Jtc1xuICAgICAgaWYgZS5idXR0b24gaXMgMiBvciAoZS5jdHJsS2V5IGFuZCBwcm9jZXNzLnBsYXRmb3JtIGlzICdkYXJ3aW4nKVxuICAgICAgICByZXR1cm5cbiAgICAgIGVsc2VcbiAgICAgICAgIyBhbGxvdyBjbGljayBvbiBtb3VzZXVwIGlmIG5vdCBkcmFnZ2luZ1xuICAgICAgICB7c2hpZnRLZXl9ID0gZVxuICAgICAgICBAc2VsZWN0T25Nb3VzZVVwID0ge3NoaWZ0S2V5LCBjbWRLZXl9XG4gICAgICAgIHJldHVyblxuXG4gICAgaWYgZS5zaGlmdEtleSBhbmQgY21kS2V5XG4gICAgICAjIHNlbGVjdCBjb250aW51b3VzIGZyb20gQGxhc3RGb2N1c2VkRW50cnkgYnV0IGxlYXZlIG90aGVyc1xuICAgICAgQHNlbGVjdENvbnRpbnVvdXNFbnRyaWVzKGVudHJ5VG9TZWxlY3QsIGZhbHNlKVxuICAgICAgQHNob3dNdWx0aVNlbGVjdE1lbnVJZk5lY2Vzc2FyeSgpXG4gICAgZWxzZSBpZiBlLnNoaWZ0S2V5XG4gICAgICAjIHNlbGVjdCBjb250aW51b3VzIGZyb20gQGxhc3RGb2N1c2VkRW50cnkgYW5kIGRlc2VsZWN0IHJlc3RcbiAgICAgIEBzZWxlY3RDb250aW51b3VzRW50cmllcyhlbnRyeVRvU2VsZWN0KVxuICAgICAgQHNob3dNdWx0aVNlbGVjdE1lbnVJZk5lY2Vzc2FyeSgpXG4gICAgIyBvbmx5IGFsbG93IGN0cmwgY2xpY2sgZm9yIG11bHRpIHNlbGVjdGlvbiBvbiBub24gZGFyd2luIHN5c3RlbXNcbiAgICBlbHNlIGlmIGNtZEtleVxuICAgICAgQHNlbGVjdE11bHRpcGxlRW50cmllcyhlbnRyeVRvU2VsZWN0KVxuICAgICAgQGxhc3RGb2N1c2VkRW50cnkgPSBlbnRyeVRvU2VsZWN0XG4gICAgICBAc2hvd011bHRpU2VsZWN0TWVudUlmTmVjZXNzYXJ5KClcbiAgICBlbHNlXG4gICAgICBAc2VsZWN0RW50cnkoZW50cnlUb1NlbGVjdClcbiAgICAgIEBzaG93RnVsbE1lbnUoKVxuXG4gIG9uTW91c2VVcDogKGUpIC0+XG4gICAgcmV0dXJuIHVubGVzcyBAc2VsZWN0T25Nb3VzZVVwP1xuXG4gICAge3NoaWZ0S2V5LCBjbWRLZXl9ID0gQHNlbGVjdE9uTW91c2VVcFxuICAgIEBzZWxlY3RPbk1vdXNlVXAgPSBudWxsXG5cbiAgICByZXR1cm4gdW5sZXNzIGVudHJ5VG9TZWxlY3QgPSBlLnRhcmdldC5jbG9zZXN0KCcuZW50cnknKVxuXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuXG4gICAgaWYgc2hpZnRLZXkgYW5kIGNtZEtleVxuICAgICAgIyBzZWxlY3QgY29udGludW91cyBmcm9tIEBsYXN0Rm9jdXNlZEVudHJ5IGJ1dCBsZWF2ZSBvdGhlcnNcbiAgICAgIEBzZWxlY3RDb250aW51b3VzRW50cmllcyhlbnRyeVRvU2VsZWN0LCBmYWxzZSlcbiAgICAgIEBzaG93TXVsdGlTZWxlY3RNZW51SWZOZWNlc3NhcnkoKVxuICAgIGVsc2UgaWYgc2hpZnRLZXlcbiAgICAgICMgc2VsZWN0IGNvbnRpbnVvdXMgZnJvbSBAbGFzdEZvY3VzZWRFbnRyeSBhbmQgZGVzZWxlY3QgcmVzdFxuICAgICAgQHNlbGVjdENvbnRpbnVvdXNFbnRyaWVzKGVudHJ5VG9TZWxlY3QpXG4gICAgICBAc2hvd011bHRpU2VsZWN0TWVudUlmTmVjZXNzYXJ5KClcbiAgICAjIG9ubHkgYWxsb3cgY3RybCBjbGljayBmb3IgbXVsdGkgc2VsZWN0aW9uIG9uIG5vbiBkYXJ3aW4gc3lzdGVtc1xuICAgIGVsc2UgaWYgY21kS2V5XG4gICAgICBAZGVzZWxlY3QoW2VudHJ5VG9TZWxlY3RdKVxuICAgICAgQGxhc3RGb2N1c2VkRW50cnkgPSBlbnRyeVRvU2VsZWN0XG4gICAgICBAc2hvd011bHRpU2VsZWN0TWVudUlmTmVjZXNzYXJ5KClcbiAgICBlbHNlXG4gICAgICBAc2VsZWN0RW50cnkoZW50cnlUb1NlbGVjdClcbiAgICAgIEBzaG93RnVsbE1lbnUoKVxuXG4gICMgUHVibGljOiBSZXR1cm4gYW4gYXJyYXkgb2YgcGF0aHMgZnJvbSBhbGwgc2VsZWN0ZWQgaXRlbXNcbiAgI1xuICAjIEV4YW1wbGU6IEBzZWxlY3RlZFBhdGhzKClcbiAgIyA9PiBbJ3NlbGVjdGVkL3BhdGgvb25lJywgJ3NlbGVjdGVkL3BhdGgvdHdvJywgJ3NlbGVjdGVkL3BhdGgvdGhyZWUnXVxuICAjIFJldHVybnMgQXJyYXkgb2Ygc2VsZWN0ZWQgaXRlbSBwYXRoc1xuICBzZWxlY3RlZFBhdGhzOiAtPlxuICAgIGVudHJ5LmdldFBhdGgoKSBmb3IgZW50cnkgaW4gQGdldFNlbGVjdGVkRW50cmllcygpXG5cbiAgIyBQdWJsaWM6IFNlbGVjdHMgaXRlbXMgd2l0aGluIGEgcmFuZ2UgZGVmaW5lZCBieSBhIGN1cnJlbnRseSBzZWxlY3RlZCBlbnRyeSBhbmRcbiAgIyAgICAgICAgIGEgbmV3IGdpdmVuIGVudHJ5LiBUaGlzIGlzIHNoaWZ0K2NsaWNrIGZ1bmN0aW9uYWxpdHlcbiAgI1xuICAjIFJldHVybnMgYXJyYXkgb2Ygc2VsZWN0ZWQgZWxlbWVudHNcbiAgc2VsZWN0Q29udGludW91c0VudHJpZXM6IChlbnRyeSwgZGVzZWxlY3RPdGhlcnMgPSB0cnVlKSAtPlxuICAgIGN1cnJlbnRTZWxlY3RlZEVudHJ5ID0gQGxhc3RGb2N1c2VkRW50cnkgPyBAc2VsZWN0ZWRFbnRyeSgpXG4gICAgcGFyZW50Q29udGFpbmVyID0gZW50cnkucGFyZW50RWxlbWVudFxuICAgIGVsZW1lbnRzID0gW11cbiAgICBpZiBwYXJlbnRDb250YWluZXIgaXMgY3VycmVudFNlbGVjdGVkRW50cnkucGFyZW50RWxlbWVudFxuICAgICAgZW50cmllcyA9IEFycmF5LmZyb20ocGFyZW50Q29udGFpbmVyLnF1ZXJ5U2VsZWN0b3JBbGwoJy5lbnRyeScpKVxuICAgICAgZW50cnlJbmRleCA9IGVudHJpZXMuaW5kZXhPZihlbnRyeSlcbiAgICAgIHNlbGVjdGVkSW5kZXggPSBlbnRyaWVzLmluZGV4T2YoY3VycmVudFNlbGVjdGVkRW50cnkpXG4gICAgICBlbGVtZW50cyA9IChlbnRyaWVzW2ldIGZvciBpIGluIFtlbnRyeUluZGV4Li5zZWxlY3RlZEluZGV4XSlcblxuICAgICAgQGRlc2VsZWN0KCkgaWYgZGVzZWxlY3RPdGhlcnNcbiAgICAgIGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnc2VsZWN0ZWQnKSBmb3IgZWxlbWVudCBpbiBlbGVtZW50c1xuXG4gICAgZWxlbWVudHNcblxuICAjIFB1YmxpYzogU2VsZWN0cyBjb25zZWN1dGl2ZSBnaXZlbiBlbnRyaWVzIHdpdGhvdXQgY2xlYXJpbmcgcHJldmlvdXNseSBzZWxlY3RlZFxuICAjICAgICAgICAgaXRlbXMuIFRoaXMgaXMgY21kK2NsaWNrIGZ1bmN0aW9uYWxpdHlcbiAgI1xuICAjIFJldHVybnMgZ2l2ZW4gZW50cnlcbiAgc2VsZWN0TXVsdGlwbGVFbnRyaWVzOiAoZW50cnkpIC0+XG4gICAgZW50cnk/LmNsYXNzTGlzdC50b2dnbGUoJ3NlbGVjdGVkJylcbiAgICBlbnRyeVxuXG4gICMgUHVibGljOiBUb2dnbGUgZnVsbC1tZW51IGNsYXNzIG9uIHRoZSBtYWluIGxpc3QgZWxlbWVudCB0byBkaXNwbGF5IHRoZSBmdWxsIGNvbnRleHRcbiAgIyAgICAgICAgIG1lbnUuXG4gIHNob3dGdWxsTWVudTogLT5cbiAgICBAbGlzdC5jbGFzc0xpc3QucmVtb3ZlKCdtdWx0aS1zZWxlY3QnKVxuICAgIEBsaXN0LmNsYXNzTGlzdC5hZGQoJ2Z1bGwtbWVudScpXG5cbiAgIyBQdWJsaWM6IFRvZ2dsZSBtdWx0aS1zZWxlY3QgY2xhc3Mgb24gdGhlIG1haW4gbGlzdCBlbGVtZW50IHRvIGRpc3BsYXkgdGhlXG4gICMgICAgICAgICBtZW51IHdpdGggb25seSBpdGVtcyB0aGF0IG1ha2Ugc2Vuc2UgZm9yIG11bHRpIHNlbGVjdCBmdW5jdGlvbmFsaXR5XG4gIHNob3dNdWx0aVNlbGVjdE1lbnU6IC0+XG4gICAgQGxpc3QuY2xhc3NMaXN0LnJlbW92ZSgnZnVsbC1tZW51JylcbiAgICBAbGlzdC5jbGFzc0xpc3QuYWRkKCdtdWx0aS1zZWxlY3QnKVxuXG4gIHNob3dNdWx0aVNlbGVjdE1lbnVJZk5lY2Vzc2FyeTogLT5cbiAgICBpZiBAZ2V0U2VsZWN0ZWRFbnRyaWVzKCkubGVuZ3RoID4gMVxuICAgICAgQHNob3dNdWx0aVNlbGVjdE1lbnUoKVxuICAgIGVsc2VcbiAgICAgIEBzaG93RnVsbE1lbnUoKVxuXG4gICMgUHVibGljOiBDaGVjayBmb3IgbXVsdGktc2VsZWN0IGNsYXNzIG9uIHRoZSBtYWluIGxpc3RcbiAgI1xuICAjIFJldHVybnMgYm9vbGVhblxuICBtdWx0aVNlbGVjdEVuYWJsZWQ6IC0+XG4gICAgQGxpc3QuY2xhc3NMaXN0LmNvbnRhaW5zKCdtdWx0aS1zZWxlY3QnKVxuXG4gIG9uRHJhZ0VudGVyOiAoZSkgPT5cbiAgICBpZiBlbnRyeSA9IGUudGFyZ2V0LmNsb3Nlc3QoJy5lbnRyeS5kaXJlY3RvcnknKVxuICAgICAgcmV0dXJuIGlmIEByb290RHJhZ0FuZERyb3AuaXNEcmFnZ2luZyhlKVxuICAgICAgcmV0dXJuIHVubGVzcyBAaXNBdG9tVHJlZVZpZXdFdmVudChlKVxuXG4gICAgICBlLnN0b3BQcm9wYWdhdGlvbigpXG5cbiAgICAgIEBkcmFnRXZlbnRDb3VudHMuc2V0KGVudHJ5LCAwKSB1bmxlc3MgQGRyYWdFdmVudENvdW50cy5nZXQoZW50cnkpXG4gICAgICB1bmxlc3MgQGRyYWdFdmVudENvdW50cy5nZXQoZW50cnkpIGlzbnQgMCBvciBlbnRyeS5jbGFzc0xpc3QuY29udGFpbnMoJ3NlbGVjdGVkJylcbiAgICAgICAgZW50cnkuY2xhc3NMaXN0LmFkZCgnZHJhZy1vdmVyJywgJ3NlbGVjdGVkJylcblxuICAgICAgQGRyYWdFdmVudENvdW50cy5zZXQoZW50cnksIEBkcmFnRXZlbnRDb3VudHMuZ2V0KGVudHJ5KSArIDEpXG5cbiAgb25EcmFnTGVhdmU6IChlKSA9PlxuICAgIGlmIGVudHJ5ID0gZS50YXJnZXQuY2xvc2VzdCgnLmVudHJ5LmRpcmVjdG9yeScpXG4gICAgICByZXR1cm4gaWYgQHJvb3REcmFnQW5kRHJvcC5pc0RyYWdnaW5nKGUpXG4gICAgICByZXR1cm4gdW5sZXNzIEBpc0F0b21UcmVlVmlld0V2ZW50KGUpXG5cbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKClcblxuICAgICAgQGRyYWdFdmVudENvdW50cy5zZXQoZW50cnksIEBkcmFnRXZlbnRDb3VudHMuZ2V0KGVudHJ5KSAtIDEpXG4gICAgICBpZiBAZHJhZ0V2ZW50Q291bnRzLmdldChlbnRyeSkgaXMgMCBhbmQgZW50cnkuY2xhc3NMaXN0LmNvbnRhaW5zKCdkcmFnLW92ZXInKVxuICAgICAgICBlbnRyeS5jbGFzc0xpc3QucmVtb3ZlKCdkcmFnLW92ZXInLCAnc2VsZWN0ZWQnKVxuXG4gICMgSGFuZGxlIGVudHJ5IG5hbWUgb2JqZWN0IGRyYWdzdGFydCBldmVudFxuICBvbkRyYWdTdGFydDogKGUpIC0+XG4gICAgQGRyYWdFdmVudENvdW50cyA9IG5ldyBXZWFrTWFwXG4gICAgQHNlbGVjdE9uTW91c2VVcCA9IG51bGxcbiAgICBpZiBlbnRyeSA9IGUudGFyZ2V0LmNsb3Nlc3QoJy5lbnRyeScpXG4gICAgICBlLnN0b3BQcm9wYWdhdGlvbigpXG5cbiAgICAgIGlmIEByb290RHJhZ0FuZERyb3AuY2FuRHJhZ1N0YXJ0KGUpXG4gICAgICAgIHJldHVybiBAcm9vdERyYWdBbmREcm9wLm9uRHJhZ1N0YXJ0KGUpXG5cbiAgICAgIGRyYWdJbWFnZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJvbFwiKVxuICAgICAgZHJhZ0ltYWdlLmNsYXNzTGlzdC5hZGQoXCJlbnRyaWVzXCIsIFwibGlzdC10cmVlXCIpXG4gICAgICBkcmFnSW1hZ2Uuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCJcbiAgICAgIGRyYWdJbWFnZS5zdHlsZS50b3AgPSAwXG4gICAgICBkcmFnSW1hZ2Uuc3R5bGUubGVmdCA9IDBcbiAgICAgICMgRW5zdXJlIHRoZSBjbG9uZWQgZmlsZSBuYW1lIGVsZW1lbnQgaXMgcmVuZGVyZWQgb24gYSBzZXBhcmF0ZSBHUFUgbGF5ZXJcbiAgICAgICMgdG8gcHJldmVudCBvdmVybGFwcGluZyBlbGVtZW50cyBsb2NhdGVkIGF0ICgwcHgsIDBweCkgZnJvbSBiZWluZyB1c2VkIGFzXG4gICAgICAjIHRoZSBkcmFnIGltYWdlLlxuICAgICAgZHJhZ0ltYWdlLnN0eWxlLndpbGxDaGFuZ2UgPSBcInRyYW5zZm9ybVwiXG5cbiAgICAgIGluaXRpYWxQYXRocyA9IFtdXG4gICAgICBmb3IgdGFyZ2V0IGluIEBnZXRTZWxlY3RlZEVudHJpZXMoKVxuICAgICAgICBlbnRyeVBhdGggPSB0YXJnZXQucXVlcnlTZWxlY3RvcihcIi5uYW1lXCIpLmRhdGFzZXQucGF0aFxuICAgICAgICBwYXJlbnRTZWxlY3RlZCA9IHRhcmdldC5wYXJlbnROb2RlLmNsb3Nlc3QoXCIuZW50cnkuc2VsZWN0ZWRcIilcbiAgICAgICAgdW5sZXNzIHBhcmVudFNlbGVjdGVkXG4gICAgICAgICAgaW5pdGlhbFBhdGhzLnB1c2goZW50cnlQYXRoKVxuICAgICAgICAgIG5ld0VsZW1lbnQgPSB0YXJnZXQuY2xvbmVOb2RlKHRydWUpXG4gICAgICAgICAgaWYgbmV3RWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoXCJkaXJlY3RvcnlcIilcbiAgICAgICAgICAgIG5ld0VsZW1lbnQucXVlcnlTZWxlY3RvcihcIi5lbnRyaWVzXCIpLnJlbW92ZSgpXG4gICAgICAgICAgZm9yIGtleSwgdmFsdWUgb2YgZ2V0U3R5bGVPYmplY3QodGFyZ2V0KVxuICAgICAgICAgICAgbmV3RWxlbWVudC5zdHlsZVtrZXldID0gdmFsdWVcbiAgICAgICAgICBuZXdFbGVtZW50LnN0eWxlLnBhZGRpbmdMZWZ0ID0gXCIxZW1cIlxuICAgICAgICAgIG5ld0VsZW1lbnQuc3R5bGUucGFkZGluZ1JpZ2h0ID0gXCIxZW1cIlxuICAgICAgICAgIGRyYWdJbWFnZS5hcHBlbmQobmV3RWxlbWVudClcblxuICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChkcmFnSW1hZ2UpXG5cbiAgICAgIGUuZGF0YVRyYW5zZmVyLmVmZmVjdEFsbG93ZWQgPSBcIm1vdmVcIlxuICAgICAgZS5kYXRhVHJhbnNmZXIuc2V0RHJhZ0ltYWdlKGRyYWdJbWFnZSwgMCwgMClcbiAgICAgIGUuZGF0YVRyYW5zZmVyLnNldERhdGEoXCJpbml0aWFsUGF0aHNcIiwgSlNPTi5zdHJpbmdpZnkoaW5pdGlhbFBhdGhzKSlcbiAgICAgIGUuZGF0YVRyYW5zZmVyLnNldERhdGEoXCJhdG9tLXRyZWUtdmlldy1ldmVudFwiLCBcInRydWVcIilcblxuICAgICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSAtPlxuICAgICAgICBkcmFnSW1hZ2UucmVtb3ZlKClcblxuICAjIEhhbmRsZSBlbnRyeSBkcmFnb3ZlciBldmVudDsgcmVzZXQgZGVmYXVsdCBkcmFnb3ZlciBhY3Rpb25zXG4gIG9uRHJhZ092ZXI6IChlKSAtPlxuICAgIGlmIGVudHJ5ID0gZS50YXJnZXQuY2xvc2VzdCgnLmVudHJ5LmRpcmVjdG9yeScpXG4gICAgICByZXR1cm4gaWYgQHJvb3REcmFnQW5kRHJvcC5pc0RyYWdnaW5nKGUpXG4gICAgICByZXR1cm4gdW5sZXNzIEBpc0F0b21UcmVlVmlld0V2ZW50KGUpXG5cbiAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuXG4gICAgICBpZiBAZHJhZ0V2ZW50Q291bnRzLmdldChlbnRyeSkgPiAwIGFuZCBub3QgZW50cnkuY2xhc3NMaXN0LmNvbnRhaW5zKCdzZWxlY3RlZCcpXG4gICAgICAgIGVudHJ5LmNsYXNzTGlzdC5hZGQoJ2RyYWctb3ZlcicsICdzZWxlY3RlZCcpXG5cbiAgIyBIYW5kbGUgZW50cnkgZHJvcCBldmVudFxuICBvbkRyb3A6IChlKSAtPlxuICAgIEBkcmFnRXZlbnRDb3VudHMgPSBuZXcgV2Vha01hcFxuICAgIGlmIGVudHJ5ID0gZS50YXJnZXQuY2xvc2VzdCgnLmVudHJ5LmRpcmVjdG9yeScpXG4gICAgICByZXR1cm4gaWYgQHJvb3REcmFnQW5kRHJvcC5pc0RyYWdnaW5nKGUpXG4gICAgICByZXR1cm4gdW5sZXNzIEBpc0F0b21UcmVlVmlld0V2ZW50KGUpXG5cbiAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuXG4gICAgICBuZXdEaXJlY3RvcnlQYXRoID0gZW50cnkucXVlcnlTZWxlY3RvcignLm5hbWUnKT8uZGF0YXNldC5wYXRoXG4gICAgICByZXR1cm4gZmFsc2UgdW5sZXNzIG5ld0RpcmVjdG9yeVBhdGhcblxuICAgICAgaW5pdGlhbFBhdGhzID0gZS5kYXRhVHJhbnNmZXIuZ2V0RGF0YSgnaW5pdGlhbFBhdGhzJylcblxuICAgICAgaWYgaW5pdGlhbFBhdGhzXG4gICAgICAgICMgRHJvcCBldmVudCBmcm9tIEF0b21cbiAgICAgICAgaW5pdGlhbFBhdGhzID0gSlNPTi5wYXJzZShpbml0aWFsUGF0aHMpXG4gICAgICAgIHJldHVybiBpZiBpbml0aWFsUGF0aHMuaW5jbHVkZXMobmV3RGlyZWN0b3J5UGF0aClcblxuICAgICAgICBlbnRyeS5jbGFzc0xpc3QucmVtb3ZlKCdkcmFnLW92ZXInLCAnc2VsZWN0ZWQnKVxuXG4gICAgICAgICMgaXRlcmF0ZSBiYWNrd2FyZHMgc28gZmlsZXMgaW4gYSBkaXIgYXJlIG1vdmVkIGJlZm9yZSB0aGUgZGlyIGl0c2VsZlxuICAgICAgICBmb3IgaW5pdGlhbFBhdGggaW4gaW5pdGlhbFBhdGhzIGJ5IC0xXG4gICAgICAgICAgIyBOb3RlOiB0aGlzIGlzIG5lY2Vzc2FyeSBvbiBXaW5kb3dzIHRvIGNpcmN1bXZlbnQgbm9kZS1wYXRod2F0Y2hlclxuICAgICAgICAgICMgaG9sZGluZyBhIGxvY2sgb24gZXhwYW5kZWQgZm9sZGVycyBhbmQgcHJldmVudGluZyB0aGVtIGZyb21cbiAgICAgICAgICAjIGJlaW5nIG1vdmVkIG9yIGRlbGV0ZWRcbiAgICAgICAgICAjIFRPRE86IFRoaXMgY2FuIGJlIHJlbW92ZWQgd2hlbiB0cmVlLXZpZXcgaXMgc3dpdGNoZWQgdG8gQGF0b20vd2F0Y2hlclxuICAgICAgICAgIEBlbnRyeUZvclBhdGgoaW5pdGlhbFBhdGgpPy5jb2xsYXBzZT8oKVxuICAgICAgICAgIGlmIChwcm9jZXNzLnBsYXRmb3JtIGlzICdkYXJ3aW4nIGFuZCBlLm1ldGFLZXkpIG9yIGUuY3RybEtleVxuICAgICAgICAgICAgQGNvcHlFbnRyeShpbml0aWFsUGF0aCwgbmV3RGlyZWN0b3J5UGF0aClcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICBicmVhayB1bmxlc3MgQG1vdmVFbnRyeShpbml0aWFsUGF0aCwgbmV3RGlyZWN0b3J5UGF0aClcbiAgICAgIGVsc2VcbiAgICAgICAgIyBEcm9wIGV2ZW50IGZyb20gT1NcbiAgICAgICAgZW50cnkuY2xhc3NMaXN0LnJlbW92ZSgnc2VsZWN0ZWQnKVxuICAgICAgICBmb3IgZmlsZSBpbiBlLmRhdGFUcmFuc2Zlci5maWxlc1xuICAgICAgICAgIGlmIChwcm9jZXNzLnBsYXRmb3JtIGlzICdkYXJ3aW4nIGFuZCBlLm1ldGFLZXkpIG9yIGUuY3RybEtleVxuICAgICAgICAgICAgQGNvcHlFbnRyeShmaWxlLnBhdGgsIG5ld0RpcmVjdG9yeVBhdGgpXG4gICAgICAgICAgZWxzZVxuICAgICAgICAgICAgYnJlYWsgdW5sZXNzIEBtb3ZlRW50cnkoZmlsZS5wYXRoLCBuZXdEaXJlY3RvcnlQYXRoKVxuICAgIGVsc2UgaWYgZS5kYXRhVHJhbnNmZXIuZmlsZXMubGVuZ3RoXG4gICAgICAjIERyb3AgZXZlbnQgZnJvbSBPUyB0aGF0IGlzbid0IHRhcmdldGluZyBhIGZvbGRlcjogYWRkIGEgbmV3IHByb2plY3QgZm9sZGVyXG4gICAgICBhdG9tLnByb2plY3QuYWRkUGF0aChlbnRyeS5wYXRoKSBmb3IgZW50cnkgaW4gZS5kYXRhVHJhbnNmZXIuZmlsZXNcblxuICBpc0F0b21UcmVlVmlld0V2ZW50OiAoZSkgLT5cbiAgICBmb3IgaXRlbSBpbiBlLmRhdGFUcmFuc2Zlci5pdGVtc1xuICAgICAgaWYgaXRlbS50eXBlIGlzICdhdG9tLXRyZWUtdmlldy1ldmVudCcgb3IgaXRlbS5raW5kIGlzICdmaWxlJ1xuICAgICAgICByZXR1cm4gdHJ1ZVxuXG4gICAgcmV0dXJuIGZhbHNlXG5cbiAgaXNWaXNpYmxlOiAtPlxuICAgIEBlbGVtZW50Lm9mZnNldFdpZHRoIGlzbnQgMCBvciBAZWxlbWVudC5vZmZzZXRIZWlnaHQgaXNudCAwXG4iXX0=
