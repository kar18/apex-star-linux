(function() {
  var Disposable, FileInfoView, fs, url;

  Disposable = require('atom').Disposable;

  url = require('url');

  fs = require('fs-plus');

  module.exports = FileInfoView = (function() {
    function FileInfoView() {
      var clickHandler;
      this.element = document.createElement('status-bar-file');
      this.element.classList.add('file-info', 'inline-block');
      this.currentPath = document.createElement('a');
      this.currentPath.classList.add('current-path');
      this.element.appendChild(this.currentPath);
      this.element.currentPath = this.currentPath;
      this.element.getActiveItem = this.getActiveItem.bind(this);
      this.activeItemSubscription = atom.workspace.getCenter().onDidChangeActivePaneItem((function(_this) {
        return function() {
          return _this.subscribeToActiveItem();
        };
      })(this));
      this.subscribeToActiveItem();
      this.registerTooltip();
      clickHandler = (function(_this) {
        return function(event) {
          var isShiftClick, text;
          isShiftClick = event.shiftKey;
          _this.showCopiedTooltip(isShiftClick);
          text = _this.getActiveItemCopyText(isShiftClick);
          atom.clipboard.write(text);
          return setTimeout(function() {
            return _this.clearCopiedTooltip();
          }, 2000);
        };
      })(this);
      this.element.addEventListener('click', clickHandler);
      this.clickSubscription = new Disposable((function(_this) {
        return function() {
          return _this.element.removeEventListener('click', clickHandler);
        };
      })(this));
    }

    FileInfoView.prototype.registerTooltip = function() {
      return this.tooltip = atom.tooltips.add(this.element, {
        title: function() {
          return "Click to copy absolute file path (Shift + Click to copy relative path)";
        }
      });
    };

    FileInfoView.prototype.clearCopiedTooltip = function() {
      var ref;
      if ((ref = this.copiedTooltip) != null) {
        ref.dispose();
      }
      return this.registerTooltip();
    };

    FileInfoView.prototype.showCopiedTooltip = function(copyRelativePath) {
      var ref, ref1, text;
      if ((ref = this.tooltip) != null) {
        ref.dispose();
      }
      if ((ref1 = this.copiedTooltip) != null) {
        ref1.dispose();
      }
      text = this.getActiveItemCopyText(copyRelativePath);
      return this.copiedTooltip = atom.tooltips.add(this.element, {
        title: "Copied: " + text,
        trigger: 'manual',
        delay: {
          show: 0
        }
      });
    };

    FileInfoView.prototype.getActiveItemCopyText = function(copyRelativePath) {
      var activeItem, path, relativized;
      activeItem = this.getActiveItem();
      path = activeItem != null ? typeof activeItem.getPath === "function" ? activeItem.getPath() : void 0 : void 0;
      if (path == null) {
        return (activeItem != null ? typeof activeItem.getTitle === "function" ? activeItem.getTitle() : void 0 : void 0) || '';
      }
      if (copyRelativePath) {
        relativized = atom.project.relativize(path);
        if (relativized !== path) {
          return relativized;
        }
      }
      if ((path != null ? path.indexOf('://') : void 0) > 0) {
        path = url.parse(path).path;
      }
      return path;
    };

    FileInfoView.prototype.subscribeToActiveItem = function() {
      var activeItem, ref, ref1;
      if ((ref = this.modifiedSubscription) != null) {
        ref.dispose();
      }
      if ((ref1 = this.titleSubscription) != null) {
        ref1.dispose();
      }
      if (activeItem = this.getActiveItem()) {
        if (this.updateCallback == null) {
          this.updateCallback = (function(_this) {
            return function() {
              return _this.update();
            };
          })(this);
        }
        if (typeof activeItem.onDidChangeTitle === 'function') {
          this.titleSubscription = activeItem.onDidChangeTitle(this.updateCallback);
        } else if (typeof activeItem.on === 'function') {
          activeItem.on('title-changed', this.updateCallback);
          this.titleSubscription = {
            dispose: (function(_this) {
              return function() {
                return typeof activeItem.off === "function" ? activeItem.off('title-changed', _this.updateCallback) : void 0;
              };
            })(this)
          };
        }
        this.modifiedSubscription = typeof activeItem.onDidChangeModified === "function" ? activeItem.onDidChangeModified(this.updateCallback) : void 0;
      }
      return this.update();
    };

    FileInfoView.prototype.destroy = function() {
      var ref, ref1, ref2, ref3, ref4;
      this.activeItemSubscription.dispose();
      if ((ref = this.titleSubscription) != null) {
        ref.dispose();
      }
      if ((ref1 = this.modifiedSubscription) != null) {
        ref1.dispose();
      }
      if ((ref2 = this.clickSubscription) != null) {
        ref2.dispose();
      }
      if ((ref3 = this.copiedTooltip) != null) {
        ref3.dispose();
      }
      return (ref4 = this.tooltip) != null ? ref4.dispose() : void 0;
    };

    FileInfoView.prototype.getActiveItem = function() {
      return atom.workspace.getCenter().getActivePaneItem();
    };

    FileInfoView.prototype.update = function() {
      var ref;
      this.updatePathText();
      return this.updateBufferHasModifiedText((ref = this.getActiveItem()) != null ? typeof ref.isModified === "function" ? ref.isModified() : void 0 : void 0);
    };

    FileInfoView.prototype.updateBufferHasModifiedText = function(isModified) {
      if (isModified) {
        this.element.classList.add('buffer-modified');
        return this.isModified = true;
      } else {
        this.element.classList.remove('buffer-modified');
        return this.isModified = false;
      }
    };

    FileInfoView.prototype.updatePathText = function() {
      var path, ref, ref1, relativized, title;
      if (path = (ref = this.getActiveItem()) != null ? typeof ref.getPath === "function" ? ref.getPath() : void 0 : void 0) {
        relativized = atom.project.relativize(path);
        return this.currentPath.textContent = relativized != null ? fs.tildify(relativized) : path;
      } else if (title = (ref1 = this.getActiveItem()) != null ? typeof ref1.getTitle === "function" ? ref1.getTitle() : void 0 : void 0) {
        return this.currentPath.textContent = title;
      } else {
        return this.currentPath.textContent = '';
      }
    };

    return FileInfoView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zdGF0dXMtYmFyL2xpYi9maWxlLWluZm8tdmlldy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFDLGFBQWMsT0FBQSxDQUFRLE1BQVI7O0VBQ2YsR0FBQSxHQUFNLE9BQUEsQ0FBUSxLQUFSOztFQUNOLEVBQUEsR0FBSyxPQUFBLENBQVEsU0FBUjs7RUFFTCxNQUFNLENBQUMsT0FBUCxHQUNNO0lBQ1Msc0JBQUE7QUFDWCxVQUFBO01BQUEsSUFBQyxDQUFBLE9BQUQsR0FBVyxRQUFRLENBQUMsYUFBVCxDQUF1QixpQkFBdkI7TUFDWCxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixXQUF2QixFQUFvQyxjQUFwQztNQUVBLElBQUMsQ0FBQSxXQUFELEdBQWUsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsR0FBdkI7TUFDZixJQUFDLENBQUEsV0FBVyxDQUFDLFNBQVMsQ0FBQyxHQUF2QixDQUEyQixjQUEzQjtNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixJQUFDLENBQUEsV0FBdEI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsR0FBdUIsSUFBQyxDQUFBO01BRXhCLElBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxHQUF5QixJQUFDLENBQUEsYUFBYSxDQUFDLElBQWYsQ0FBb0IsSUFBcEI7TUFFekIsSUFBQyxDQUFBLHNCQUFELEdBQTBCLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBZixDQUFBLENBQTBCLENBQUMseUJBQTNCLENBQXFELENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFDN0UsS0FBQyxDQUFBLHFCQUFELENBQUE7UUFENkU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXJEO01BRTFCLElBQUMsQ0FBQSxxQkFBRCxDQUFBO01BRUEsSUFBQyxDQUFBLGVBQUQsQ0FBQTtNQUNBLFlBQUEsR0FBZSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsS0FBRDtBQUNiLGNBQUE7VUFBQSxZQUFBLEdBQWUsS0FBSyxDQUFDO1VBQ3JCLEtBQUMsQ0FBQSxpQkFBRCxDQUFtQixZQUFuQjtVQUNBLElBQUEsR0FBTyxLQUFDLENBQUEscUJBQUQsQ0FBdUIsWUFBdkI7VUFDUCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQWYsQ0FBcUIsSUFBckI7aUJBQ0EsVUFBQSxDQUFXLFNBQUE7bUJBQ1QsS0FBQyxDQUFBLGtCQUFELENBQUE7VUFEUyxDQUFYLEVBRUUsSUFGRjtRQUxhO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQTtNQVNmLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsWUFBbkM7TUFDQSxJQUFDLENBQUEsaUJBQUQsR0FBcUIsSUFBSSxVQUFKLENBQWUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFPLENBQUMsbUJBQVQsQ0FBNkIsT0FBN0IsRUFBc0MsWUFBdEM7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBZjtJQTFCVjs7MkJBNEJiLGVBQUEsR0FBaUIsU0FBQTthQUNmLElBQUMsQ0FBQSxPQUFELEdBQVcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLElBQUMsQ0FBQSxPQUFuQixFQUE0QjtRQUFBLEtBQUEsRUFBTyxTQUFBO2lCQUM1QztRQUQ0QyxDQUFQO09BQTVCO0lBREk7OzJCQUlqQixrQkFBQSxHQUFvQixTQUFBO0FBQ2xCLFVBQUE7O1dBQWMsQ0FBRSxPQUFoQixDQUFBOzthQUNBLElBQUMsQ0FBQSxlQUFELENBQUE7SUFGa0I7OzJCQUlwQixpQkFBQSxHQUFtQixTQUFDLGdCQUFEO0FBQ2pCLFVBQUE7O1dBQVEsQ0FBRSxPQUFWLENBQUE7OztZQUNjLENBQUUsT0FBaEIsQ0FBQTs7TUFDQSxJQUFBLEdBQU8sSUFBQyxDQUFBLHFCQUFELENBQXVCLGdCQUF2QjthQUNQLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixJQUFDLENBQUEsT0FBbkIsRUFDZjtRQUFBLEtBQUEsRUFBTyxVQUFBLEdBQVcsSUFBbEI7UUFDQSxPQUFBLEVBQVMsUUFEVDtRQUVBLEtBQUEsRUFDRTtVQUFBLElBQUEsRUFBTSxDQUFOO1NBSEY7T0FEZTtJQUpBOzsyQkFVbkIscUJBQUEsR0FBdUIsU0FBQyxnQkFBRDtBQUNyQixVQUFBO01BQUEsVUFBQSxHQUFhLElBQUMsQ0FBQSxhQUFELENBQUE7TUFDYixJQUFBLG1FQUFPLFVBQVUsQ0FBRTtNQUNuQixJQUE0QyxZQUE1QztBQUFBLGlGQUFPLFVBQVUsQ0FBRSw2QkFBWixJQUEyQixHQUFsQzs7TUFHQSxJQUFHLGdCQUFIO1FBQ0UsV0FBQSxHQUFjLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBYixDQUF3QixJQUF4QjtRQUNkLElBQUcsV0FBQSxLQUFpQixJQUFwQjtBQUNFLGlCQUFPLFlBRFQ7U0FGRjs7TUFNQSxvQkFBRyxJQUFJLENBQUUsT0FBTixDQUFjLEtBQWQsV0FBQSxHQUF1QixDQUExQjtRQUNFLElBQUEsR0FBTyxHQUFHLENBQUMsS0FBSixDQUFVLElBQVYsQ0FBZSxDQUFDLEtBRHpCOzthQUVBO0lBZHFCOzsyQkFnQnZCLHFCQUFBLEdBQXVCLFNBQUE7QUFDckIsVUFBQTs7V0FBcUIsQ0FBRSxPQUF2QixDQUFBOzs7WUFDa0IsQ0FBRSxPQUFwQixDQUFBOztNQUVBLElBQUcsVUFBQSxHQUFhLElBQUMsQ0FBQSxhQUFELENBQUEsQ0FBaEI7O1VBQ0UsSUFBQyxDQUFBLGlCQUFrQixDQUFBLFNBQUEsS0FBQTttQkFBQSxTQUFBO3FCQUFHLEtBQUMsQ0FBQSxNQUFELENBQUE7WUFBSDtVQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7O1FBRW5CLElBQUcsT0FBTyxVQUFVLENBQUMsZ0JBQWxCLEtBQXNDLFVBQXpDO1VBQ0UsSUFBQyxDQUFBLGlCQUFELEdBQXFCLFVBQVUsQ0FBQyxnQkFBWCxDQUE0QixJQUFDLENBQUEsY0FBN0IsRUFEdkI7U0FBQSxNQUVLLElBQUcsT0FBTyxVQUFVLENBQUMsRUFBbEIsS0FBd0IsVUFBM0I7VUFFSCxVQUFVLENBQUMsRUFBWCxDQUFjLGVBQWQsRUFBK0IsSUFBQyxDQUFBLGNBQWhDO1VBQ0EsSUFBQyxDQUFBLGlCQUFELEdBQXFCO1lBQUEsT0FBQSxFQUFTLENBQUEsU0FBQSxLQUFBO3FCQUFBLFNBQUE7OERBQzVCLFVBQVUsQ0FBQyxJQUFLLGlCQUFpQixLQUFDLENBQUE7Y0FETjtZQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBVDtZQUhsQjs7UUFNTCxJQUFDLENBQUEsb0JBQUQsMERBQXdCLFVBQVUsQ0FBQyxvQkFBcUIsSUFBQyxDQUFBLHlCQVgzRDs7YUFhQSxJQUFDLENBQUEsTUFBRCxDQUFBO0lBakJxQjs7MkJBbUJ2QixPQUFBLEdBQVMsU0FBQTtBQUNQLFVBQUE7TUFBQSxJQUFDLENBQUEsc0JBQXNCLENBQUMsT0FBeEIsQ0FBQTs7V0FDa0IsQ0FBRSxPQUFwQixDQUFBOzs7WUFDcUIsQ0FBRSxPQUF2QixDQUFBOzs7WUFDa0IsQ0FBRSxPQUFwQixDQUFBOzs7WUFDYyxDQUFFLE9BQWhCLENBQUE7O2lEQUNRLENBQUUsT0FBVixDQUFBO0lBTk87OzJCQVFULGFBQUEsR0FBZSxTQUFBO2FBQ2IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFmLENBQUEsQ0FBMEIsQ0FBQyxpQkFBM0IsQ0FBQTtJQURhOzsyQkFHZixNQUFBLEdBQVEsU0FBQTtBQUNOLFVBQUE7TUFBQSxJQUFDLENBQUEsY0FBRCxDQUFBO2FBQ0EsSUFBQyxDQUFBLDJCQUFELGtGQUE2QyxDQUFFLDhCQUEvQztJQUZNOzsyQkFJUiwyQkFBQSxHQUE2QixTQUFDLFVBQUQ7TUFDM0IsSUFBRyxVQUFIO1FBQ0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsaUJBQXZCO2VBQ0EsSUFBQyxDQUFBLFVBQUQsR0FBYyxLQUZoQjtPQUFBLE1BQUE7UUFJRSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFuQixDQUEwQixpQkFBMUI7ZUFDQSxJQUFDLENBQUEsVUFBRCxHQUFjLE1BTGhCOztJQUQyQjs7MkJBUTdCLGNBQUEsR0FBZ0IsU0FBQTtBQUNkLFVBQUE7TUFBQSxJQUFHLElBQUEsaUZBQXVCLENBQUUsMkJBQTVCO1FBQ0UsV0FBQSxHQUFjLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBYixDQUF3QixJQUF4QjtlQUNkLElBQUMsQ0FBQSxXQUFXLENBQUMsV0FBYixHQUE4QixtQkFBSCxHQUFxQixFQUFFLENBQUMsT0FBSCxDQUFXLFdBQVgsQ0FBckIsR0FBa0QsS0FGL0U7T0FBQSxNQUdLLElBQUcsS0FBQSxxRkFBd0IsQ0FBRSw0QkFBN0I7ZUFDSCxJQUFDLENBQUEsV0FBVyxDQUFDLFdBQWIsR0FBMkIsTUFEeEI7T0FBQSxNQUFBO2VBR0gsSUFBQyxDQUFBLFdBQVcsQ0FBQyxXQUFiLEdBQTJCLEdBSHhCOztJQUpTOzs7OztBQTlHbEIiLCJzb3VyY2VzQ29udGVudCI6WyJ7RGlzcG9zYWJsZX0gPSByZXF1aXJlICdhdG9tJ1xudXJsID0gcmVxdWlyZSAndXJsJ1xuZnMgPSByZXF1aXJlICdmcy1wbHVzJ1xuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBGaWxlSW5mb1ZpZXdcbiAgY29uc3RydWN0b3I6IC0+XG4gICAgQGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdGF0dXMtYmFyLWZpbGUnKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2ZpbGUtaW5mbycsICdpbmxpbmUtYmxvY2snKVxuXG4gICAgQGN1cnJlbnRQYXRoID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpXG4gICAgQGN1cnJlbnRQYXRoLmNsYXNzTGlzdC5hZGQoJ2N1cnJlbnQtcGF0aCcpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQGN1cnJlbnRQYXRoKVxuICAgIEBlbGVtZW50LmN1cnJlbnRQYXRoID0gQGN1cnJlbnRQYXRoXG5cbiAgICBAZWxlbWVudC5nZXRBY3RpdmVJdGVtID0gQGdldEFjdGl2ZUl0ZW0uYmluZCh0aGlzKVxuXG4gICAgQGFjdGl2ZUl0ZW1TdWJzY3JpcHRpb24gPSBhdG9tLndvcmtzcGFjZS5nZXRDZW50ZXIoKS5vbkRpZENoYW5nZUFjdGl2ZVBhbmVJdGVtID0+XG4gICAgICBAc3Vic2NyaWJlVG9BY3RpdmVJdGVtKClcbiAgICBAc3Vic2NyaWJlVG9BY3RpdmVJdGVtKClcblxuICAgIEByZWdpc3RlclRvb2x0aXAoKVxuICAgIGNsaWNrSGFuZGxlciA9IChldmVudCkgPT5cbiAgICAgIGlzU2hpZnRDbGljayA9IGV2ZW50LnNoaWZ0S2V5XG4gICAgICBAc2hvd0NvcGllZFRvb2x0aXAoaXNTaGlmdENsaWNrKVxuICAgICAgdGV4dCA9IEBnZXRBY3RpdmVJdGVtQ29weVRleHQoaXNTaGlmdENsaWNrKVxuICAgICAgYXRvbS5jbGlwYm9hcmQud3JpdGUodGV4dClcbiAgICAgIHNldFRpbWVvdXQgPT5cbiAgICAgICAgQGNsZWFyQ29waWVkVG9vbHRpcCgpXG4gICAgICAsIDIwMDBcblxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2xpY2tIYW5kbGVyKVxuICAgIEBjbGlja1N1YnNjcmlwdGlvbiA9IG5ldyBEaXNwb3NhYmxlID0+IEBlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2xpY2tIYW5kbGVyKVxuXG4gIHJlZ2lzdGVyVG9vbHRpcDogLT5cbiAgICBAdG9vbHRpcCA9IGF0b20udG9vbHRpcHMuYWRkKEBlbGVtZW50LCB0aXRsZTogLT5cbiAgICAgIFwiQ2xpY2sgdG8gY29weSBhYnNvbHV0ZSBmaWxlIHBhdGggKFNoaWZ0ICsgQ2xpY2sgdG8gY29weSByZWxhdGl2ZSBwYXRoKVwiKVxuXG4gIGNsZWFyQ29waWVkVG9vbHRpcDogLT5cbiAgICBAY29waWVkVG9vbHRpcD8uZGlzcG9zZSgpXG4gICAgQHJlZ2lzdGVyVG9vbHRpcCgpXG5cbiAgc2hvd0NvcGllZFRvb2x0aXA6IChjb3B5UmVsYXRpdmVQYXRoKSAtPlxuICAgIEB0b29sdGlwPy5kaXNwb3NlKClcbiAgICBAY29waWVkVG9vbHRpcD8uZGlzcG9zZSgpXG4gICAgdGV4dCA9IEBnZXRBY3RpdmVJdGVtQ29weVRleHQoY29weVJlbGF0aXZlUGF0aClcbiAgICBAY29waWVkVG9vbHRpcCA9IGF0b20udG9vbHRpcHMuYWRkIEBlbGVtZW50LFxuICAgICAgdGl0bGU6IFwiQ29waWVkOiAje3RleHR9XCJcbiAgICAgIHRyaWdnZXI6ICdtYW51YWwnXG4gICAgICBkZWxheTpcbiAgICAgICAgc2hvdzogMFxuXG4gIGdldEFjdGl2ZUl0ZW1Db3B5VGV4dDogKGNvcHlSZWxhdGl2ZVBhdGgpIC0+XG4gICAgYWN0aXZlSXRlbSA9IEBnZXRBY3RpdmVJdGVtKClcbiAgICBwYXRoID0gYWN0aXZlSXRlbT8uZ2V0UGF0aD8oKVxuICAgIHJldHVybiBhY3RpdmVJdGVtPy5nZXRUaXRsZT8oKSBvciAnJyBpZiBub3QgcGF0aD9cblxuICAgICMgTWFrZSBzdXJlIHdlIHRyeSB0byByZWxhdGl2aXplIGJlZm9yZSBwYXJzaW5nIFVSTHMuXG4gICAgaWYgY29weVJlbGF0aXZlUGF0aFxuICAgICAgcmVsYXRpdml6ZWQgPSBhdG9tLnByb2plY3QucmVsYXRpdml6ZShwYXRoKVxuICAgICAgaWYgcmVsYXRpdml6ZWQgaXNudCBwYXRoXG4gICAgICAgIHJldHVybiByZWxhdGl2aXplZFxuXG4gICAgIyBBbiBpdGVtIHBhdGggY291bGQgYmUgYSB1cmwsIHdlIG9ubHkgd2FudCB0byBjb3B5IHRoZSBgcGF0aGAgcGFydFxuICAgIGlmIHBhdGg/LmluZGV4T2YoJzovLycpID4gMFxuICAgICAgcGF0aCA9IHVybC5wYXJzZShwYXRoKS5wYXRoXG4gICAgcGF0aFxuXG4gIHN1YnNjcmliZVRvQWN0aXZlSXRlbTogLT5cbiAgICBAbW9kaWZpZWRTdWJzY3JpcHRpb24/LmRpc3Bvc2UoKVxuICAgIEB0aXRsZVN1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG5cbiAgICBpZiBhY3RpdmVJdGVtID0gQGdldEFjdGl2ZUl0ZW0oKVxuICAgICAgQHVwZGF0ZUNhbGxiYWNrID89ID0+IEB1cGRhdGUoKVxuXG4gICAgICBpZiB0eXBlb2YgYWN0aXZlSXRlbS5vbkRpZENoYW5nZVRpdGxlIGlzICdmdW5jdGlvbidcbiAgICAgICAgQHRpdGxlU3Vic2NyaXB0aW9uID0gYWN0aXZlSXRlbS5vbkRpZENoYW5nZVRpdGxlKEB1cGRhdGVDYWxsYmFjaylcbiAgICAgIGVsc2UgaWYgdHlwZW9mIGFjdGl2ZUl0ZW0ub24gaXMgJ2Z1bmN0aW9uJ1xuICAgICAgICAjVE9ETyBSZW1vdmUgb25jZSB0aXRsZS1jaGFuZ2VkIGV2ZW50IHN1cHBvcnQgaXMgcmVtb3ZlZFxuICAgICAgICBhY3RpdmVJdGVtLm9uKCd0aXRsZS1jaGFuZ2VkJywgQHVwZGF0ZUNhbGxiYWNrKVxuICAgICAgICBAdGl0bGVTdWJzY3JpcHRpb24gPSBkaXNwb3NlOiA9PlxuICAgICAgICAgIGFjdGl2ZUl0ZW0ub2ZmPygndGl0bGUtY2hhbmdlZCcsIEB1cGRhdGVDYWxsYmFjaylcblxuICAgICAgQG1vZGlmaWVkU3Vic2NyaXB0aW9uID0gYWN0aXZlSXRlbS5vbkRpZENoYW5nZU1vZGlmaWVkPyhAdXBkYXRlQ2FsbGJhY2spXG5cbiAgICBAdXBkYXRlKClcblxuICBkZXN0cm95OiAtPlxuICAgIEBhY3RpdmVJdGVtU3Vic2NyaXB0aW9uLmRpc3Bvc2UoKVxuICAgIEB0aXRsZVN1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG4gICAgQG1vZGlmaWVkU3Vic2NyaXB0aW9uPy5kaXNwb3NlKClcbiAgICBAY2xpY2tTdWJzY3JpcHRpb24/LmRpc3Bvc2UoKVxuICAgIEBjb3BpZWRUb29sdGlwPy5kaXNwb3NlKClcbiAgICBAdG9vbHRpcD8uZGlzcG9zZSgpXG5cbiAgZ2V0QWN0aXZlSXRlbTogLT5cbiAgICBhdG9tLndvcmtzcGFjZS5nZXRDZW50ZXIoKS5nZXRBY3RpdmVQYW5lSXRlbSgpXG5cbiAgdXBkYXRlOiAtPlxuICAgIEB1cGRhdGVQYXRoVGV4dCgpXG4gICAgQHVwZGF0ZUJ1ZmZlckhhc01vZGlmaWVkVGV4dChAZ2V0QWN0aXZlSXRlbSgpPy5pc01vZGlmaWVkPygpKVxuXG4gIHVwZGF0ZUJ1ZmZlckhhc01vZGlmaWVkVGV4dDogKGlzTW9kaWZpZWQpIC0+XG4gICAgaWYgaXNNb2RpZmllZFxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnYnVmZmVyLW1vZGlmaWVkJylcbiAgICAgIEBpc01vZGlmaWVkID0gdHJ1ZVxuICAgIGVsc2VcbiAgICAgIEBlbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2J1ZmZlci1tb2RpZmllZCcpXG4gICAgICBAaXNNb2RpZmllZCA9IGZhbHNlXG5cbiAgdXBkYXRlUGF0aFRleHQ6IC0+XG4gICAgaWYgcGF0aCA9IEBnZXRBY3RpdmVJdGVtKCk/LmdldFBhdGg/KClcbiAgICAgIHJlbGF0aXZpemVkID0gYXRvbS5wcm9qZWN0LnJlbGF0aXZpemUocGF0aClcbiAgICAgIEBjdXJyZW50UGF0aC50ZXh0Q29udGVudCA9IGlmIHJlbGF0aXZpemVkPyB0aGVuIGZzLnRpbGRpZnkocmVsYXRpdml6ZWQpIGVsc2UgcGF0aFxuICAgIGVsc2UgaWYgdGl0bGUgPSBAZ2V0QWN0aXZlSXRlbSgpPy5nZXRUaXRsZT8oKVxuICAgICAgQGN1cnJlbnRQYXRoLnRleHRDb250ZW50ID0gdGl0bGVcbiAgICBlbHNlXG4gICAgICBAY3VycmVudFBhdGgudGV4dENvbnRlbnQgPSAnJ1xuIl19
