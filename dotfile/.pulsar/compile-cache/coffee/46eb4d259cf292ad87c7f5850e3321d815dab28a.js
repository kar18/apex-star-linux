(function() {
  var BufferedProcess, DEV_PACKAGE_PATH, fs, os, path, semver,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  os = require('os');

  fs = require('fs');

  path = require('path');

  semver = require('semver');

  BufferedProcess = require('atom').BufferedProcess;


  /*
  A collection of methods for retrieving information about the user's system for
  bug report purposes.
   */

  DEV_PACKAGE_PATH = path.join('dev', 'packages');

  module.exports = {

    /*
    Section: System Information
     */
    getPlatform: function() {
      return os.platform();
    },
    getOSVersion: function() {
      return new Promise((function(_this) {
        return function(resolve, reject) {
          switch (_this.getPlatform()) {
            case 'darwin':
              return resolve(_this.macVersionText());
            case 'win32':
              return resolve(_this.winVersionText());
            case 'linux':
              return resolve(_this.linuxVersionText());
            default:
              return resolve((os.platform()) + " " + (os.release()));
          }
        };
      })(this));
    },
    macVersionText: function() {
      return this.macVersionInfo().then(function(info) {
        if (!(info.ProductName && info.ProductVersion)) {
          return 'Unknown macOS version';
        }
        return info.ProductName + " " + info.ProductVersion;
      });
    },
    macVersionInfo: function() {
      return new Promise(function(resolve, reject) {
        var plistBuddy, stdout;
        stdout = '';
        plistBuddy = new BufferedProcess({
          command: '/usr/libexec/PlistBuddy',
          args: ['-c', 'Print ProductVersion', '-c', 'Print ProductName', '/System/Library/CoreServices/SystemVersion.plist'],
          stdout: function(output) {
            return stdout += output;
          },
          exit: function() {
            var ProductName, ProductVersion, ref;
            ref = stdout.trim().split('\n'), ProductVersion = ref[0], ProductName = ref[1];
            return resolve({
              ProductVersion: ProductVersion,
              ProductName: ProductName
            });
          }
        });
        return plistBuddy.onWillThrowError(function(arg) {
          var handle;
          handle = arg.handle;
          handle();
          return resolve({});
        });
      });
    },
    linuxVersionText: function() {
      return this.linuxVersionInfo().then(function(info) {
        if (info.DistroName && info.DistroVersion) {
          return info.DistroName + " " + info.DistroVersion;
        } else {
          return (os.platform()) + " " + (os.release());
        }
      });
    },
    linuxVersionInfo: function() {
      return new Promise(function(resolve, reject) {
        var lsbRelease, stdout;
        stdout = '';
        lsbRelease = new BufferedProcess({
          command: 'lsb_release',
          args: ['-ds'],
          stdout: function(output) {
            return stdout += output;
          },
          exit: function(exitCode) {
            var DistroName, DistroVersion, ref;
            ref = stdout.trim().split(' '), DistroName = ref[0], DistroVersion = ref[1];
            return resolve({
              DistroName: DistroName,
              DistroVersion: DistroVersion
            });
          }
        });
        return lsbRelease.onWillThrowError(function(arg) {
          var handle;
          handle = arg.handle;
          handle();
          return resolve({});
        });
      });
    },
    winVersionText: function() {
      return new Promise(function(resolve, reject) {
        var data, systemInfo;
        data = [];
        systemInfo = new BufferedProcess({
          command: 'systeminfo',
          stdout: function(oneLine) {
            return data.push(oneLine);
          },
          exit: function() {
            var info, res;
            info = data.join('\n');
            info = (res = /OS.Name.\s+(.*)$/im.exec(info)) ? res[1] : 'Unknown Windows version';
            return resolve(info);
          }
        });
        return systemInfo.onWillThrowError(function(arg) {
          var handle;
          handle = arg.handle;
          handle();
          return resolve('Unknown Windows version');
        });
      });
    },

    /*
    Section: Installed Packages
     */
    getNonCorePackages: function() {
      return new Promise(function(resolve, reject) {
        var devPackageNames, nonCorePackages, pack;
        nonCorePackages = atom.packages.getAvailablePackageMetadata().filter(function(p) {
          return !atom.packages.isBundledPackage(p.name);
        });
        devPackageNames = atom.packages.getAvailablePackagePaths().filter(function(p) {
          return p.includes(DEV_PACKAGE_PATH);
        }).map(function(p) {
          return path.basename(p);
        });
        return resolve((function() {
          var i, len, ref, results;
          results = [];
          for (i = 0, len = nonCorePackages.length; i < len; i++) {
            pack = nonCorePackages[i];
            results.push(pack.name + " " + pack.version + " " + ((ref = pack.name, indexOf.call(devPackageNames, ref) >= 0) ? '(dev)' : ''));
          }
          return results;
        })());
      });
    },
    getLatestAtomData: function() {
      var githubHeaders;
      githubHeaders = new Headers({
        accept: 'application/vnd.github.v3+json',
        contentType: "application/json"
      });
      return fetch('https://atom.io/api/updates', {
        headers: githubHeaders
      }).then(function(r) {
        if (r.ok) {
          return r.json();
        } else {
          return Promise.reject(r.statusCode);
        }
      });
    },
    checkAtomUpToDate: function() {
      return this.getLatestAtomData().then(function(latestAtomData) {
        var installedVersion, latestVersion, ref, upToDate;
        installedVersion = (ref = atom.getVersion()) != null ? ref.replace(/-.*$/, '') : void 0;
        latestVersion = latestAtomData.name;
        upToDate = (installedVersion != null) && semver.gte(installedVersion, latestVersion);
        return {
          upToDate: upToDate,
          latestVersion: latestVersion,
          installedVersion: installedVersion
        };
      });
    },
    getPackageVersion: function(packageName) {
      var pack;
      pack = atom.packages.getLoadedPackage(packageName);
      return pack != null ? pack.metadata.version : void 0;
    },
    getPackageVersionShippedWithAtom: function(packageName) {
      return require(path.join(atom.getLoadSettings().resourcePath, 'package.json')).packageDependencies[packageName];
    },
    getLatestPackageData: function(packageName) {
      var githubHeaders;
      githubHeaders = new Headers({
        accept: 'application/vnd.github.v3+json',
        contentType: "application/json"
      });
      return fetch("https://atom.io/api/packages/" + packageName, {
        headers: githubHeaders
      }).then(function(r) {
        if (r.ok) {
          return r.json();
        } else {
          return Promise.reject(r.statusCode);
        }
      });
    },
    checkPackageUpToDate: function(packageName) {
      return this.getLatestPackageData(packageName).then((function(_this) {
        return function(latestPackageData) {
          var installedVersion, isCore, latestVersion, upToDate, versionShippedWithAtom;
          installedVersion = _this.getPackageVersion(packageName);
          upToDate = (installedVersion != null) && semver.gte(installedVersion, latestPackageData.releases.latest);
          latestVersion = latestPackageData.releases.latest;
          versionShippedWithAtom = _this.getPackageVersionShippedWithAtom(packageName);
          if (isCore = versionShippedWithAtom != null) {
            upToDate = (installedVersion != null) && semver.gte(installedVersion, versionShippedWithAtom);
          }
          return {
            isCore: isCore,
            upToDate: upToDate,
            latestVersion: latestVersion,
            installedVersion: installedVersion,
            versionShippedWithAtom: versionShippedWithAtom
          };
        };
      })(this));
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9ub3RpZmljYXRpb25zL2xpYi91c2VyLXV0aWxpdGllcy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBLHVEQUFBO0lBQUE7O0VBQUEsRUFBQSxHQUFLLE9BQUEsQ0FBUSxJQUFSOztFQUNMLEVBQUEsR0FBSyxPQUFBLENBQVEsSUFBUjs7RUFDTCxJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBQ1AsTUFBQSxHQUFTLE9BQUEsQ0FBUSxRQUFSOztFQUNSLGtCQUFtQixPQUFBLENBQVEsTUFBUjs7O0FBRXBCOzs7OztFQUtBLGdCQUFBLEdBQW1CLElBQUksQ0FBQyxJQUFMLENBQVUsS0FBVixFQUFpQixVQUFqQjs7RUFFbkIsTUFBTSxDQUFDLE9BQVAsR0FFRTs7QUFBQTs7O0lBSUEsV0FBQSxFQUFhLFNBQUE7YUFDWCxFQUFFLENBQUMsUUFBSCxDQUFBO0lBRFcsQ0FKYjtJQVFBLFlBQUEsRUFBYyxTQUFBO2FBQ1osSUFBSSxPQUFKLENBQVksQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLE9BQUQsRUFBVSxNQUFWO0FBQ1Ysa0JBQU8sS0FBQyxDQUFBLFdBQUQsQ0FBQSxDQUFQO0FBQUEsaUJBQ08sUUFEUDtxQkFDcUIsT0FBQSxDQUFRLEtBQUMsQ0FBQSxjQUFELENBQUEsQ0FBUjtBQURyQixpQkFFTyxPQUZQO3FCQUVvQixPQUFBLENBQVEsS0FBQyxDQUFBLGNBQUQsQ0FBQSxDQUFSO0FBRnBCLGlCQUdPLE9BSFA7cUJBR29CLE9BQUEsQ0FBUSxLQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFSO0FBSHBCO3FCQUlPLE9BQUEsQ0FBVSxDQUFDLEVBQUUsQ0FBQyxRQUFILENBQUEsQ0FBRCxDQUFBLEdBQWUsR0FBZixHQUFpQixDQUFDLEVBQUUsQ0FBQyxPQUFILENBQUEsQ0FBRCxDQUEzQjtBQUpQO1FBRFU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVo7SUFEWSxDQVJkO0lBZ0JBLGNBQUEsRUFBZ0IsU0FBQTthQUNkLElBQUMsQ0FBQSxjQUFELENBQUEsQ0FBaUIsQ0FBQyxJQUFsQixDQUF1QixTQUFDLElBQUQ7UUFDckIsSUFBQSxDQUFBLENBQXNDLElBQUksQ0FBQyxXQUFMLElBQXFCLElBQUksQ0FBQyxjQUFoRSxDQUFBO0FBQUEsaUJBQU8sd0JBQVA7O2VBQ0csSUFBSSxDQUFDLFdBQU4sR0FBa0IsR0FBbEIsR0FBcUIsSUFBSSxDQUFDO01BRlAsQ0FBdkI7SUFEYyxDQWhCaEI7SUFxQkEsY0FBQSxFQUFnQixTQUFBO2FBQ2QsSUFBSSxPQUFKLENBQVksU0FBQyxPQUFELEVBQVUsTUFBVjtBQUNWLFlBQUE7UUFBQSxNQUFBLEdBQVM7UUFDVCxVQUFBLEdBQWEsSUFBSSxlQUFKLENBQ1g7VUFBQSxPQUFBLEVBQVMseUJBQVQ7VUFDQSxJQUFBLEVBQU0sQ0FDSixJQURJLEVBRUosc0JBRkksRUFHSixJQUhJLEVBSUosbUJBSkksRUFLSixrREFMSSxDQUROO1VBUUEsTUFBQSxFQUFRLFNBQUMsTUFBRDttQkFBWSxNQUFBLElBQVU7VUFBdEIsQ0FSUjtVQVNBLElBQUEsRUFBTSxTQUFBO0FBQ0osZ0JBQUE7WUFBQSxNQUFnQyxNQUFNLENBQUMsSUFBUCxDQUFBLENBQWEsQ0FBQyxLQUFkLENBQW9CLElBQXBCLENBQWhDLEVBQUMsdUJBQUQsRUFBaUI7bUJBQ2pCLE9BQUEsQ0FBUTtjQUFDLGdCQUFBLGNBQUQ7Y0FBaUIsYUFBQSxXQUFqQjthQUFSO1VBRkksQ0FUTjtTQURXO2VBY2IsVUFBVSxDQUFDLGdCQUFYLENBQTRCLFNBQUMsR0FBRDtBQUMxQixjQUFBO1VBRDRCLFNBQUQ7VUFDM0IsTUFBQSxDQUFBO2lCQUNBLE9BQUEsQ0FBUSxFQUFSO1FBRjBCLENBQTVCO01BaEJVLENBQVo7SUFEYyxDQXJCaEI7SUEwQ0EsZ0JBQUEsRUFBa0IsU0FBQTthQUNoQixJQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFtQixDQUFDLElBQXBCLENBQXlCLFNBQUMsSUFBRDtRQUN2QixJQUFHLElBQUksQ0FBQyxVQUFMLElBQW9CLElBQUksQ0FBQyxhQUE1QjtpQkFDSyxJQUFJLENBQUMsVUFBTixHQUFpQixHQUFqQixHQUFvQixJQUFJLENBQUMsY0FEN0I7U0FBQSxNQUFBO2lCQUdJLENBQUMsRUFBRSxDQUFDLFFBQUgsQ0FBQSxDQUFELENBQUEsR0FBZSxHQUFmLEdBQWlCLENBQUMsRUFBRSxDQUFDLE9BQUgsQ0FBQSxDQUFELEVBSHJCOztNQUR1QixDQUF6QjtJQURnQixDQTFDbEI7SUFpREEsZ0JBQUEsRUFBa0IsU0FBQTthQUNoQixJQUFJLE9BQUosQ0FBWSxTQUFDLE9BQUQsRUFBVSxNQUFWO0FBQ1YsWUFBQTtRQUFBLE1BQUEsR0FBUztRQUVULFVBQUEsR0FBYSxJQUFJLGVBQUosQ0FDWDtVQUFBLE9BQUEsRUFBUyxhQUFUO1VBQ0EsSUFBQSxFQUFNLENBQUMsS0FBRCxDQUROO1VBRUEsTUFBQSxFQUFRLFNBQUMsTUFBRDttQkFBWSxNQUFBLElBQVU7VUFBdEIsQ0FGUjtVQUdBLElBQUEsRUFBTSxTQUFDLFFBQUQ7QUFDSixnQkFBQTtZQUFBLE1BQThCLE1BQU0sQ0FBQyxJQUFQLENBQUEsQ0FBYSxDQUFDLEtBQWQsQ0FBb0IsR0FBcEIsQ0FBOUIsRUFBQyxtQkFBRCxFQUFhO21CQUNiLE9BQUEsQ0FBUTtjQUFDLFlBQUEsVUFBRDtjQUFhLGVBQUEsYUFBYjthQUFSO1VBRkksQ0FITjtTQURXO2VBUWIsVUFBVSxDQUFDLGdCQUFYLENBQTRCLFNBQUMsR0FBRDtBQUMxQixjQUFBO1VBRDRCLFNBQUQ7VUFDM0IsTUFBQSxDQUFBO2lCQUNBLE9BQUEsQ0FBUSxFQUFSO1FBRjBCLENBQTVCO01BWFUsQ0FBWjtJQURnQixDQWpEbEI7SUFpRUEsY0FBQSxFQUFnQixTQUFBO2FBQ2QsSUFBSSxPQUFKLENBQVksU0FBQyxPQUFELEVBQVUsTUFBVjtBQUNWLFlBQUE7UUFBQSxJQUFBLEdBQU87UUFDUCxVQUFBLEdBQWEsSUFBSSxlQUFKLENBQ1g7VUFBQSxPQUFBLEVBQVMsWUFBVDtVQUNBLE1BQUEsRUFBUSxTQUFDLE9BQUQ7bUJBQWEsSUFBSSxDQUFDLElBQUwsQ0FBVSxPQUFWO1VBQWIsQ0FEUjtVQUVBLElBQUEsRUFBTSxTQUFBO0FBQ0osZ0JBQUE7WUFBQSxJQUFBLEdBQU8sSUFBSSxDQUFDLElBQUwsQ0FBVSxJQUFWO1lBQ1AsSUFBQSxHQUFVLENBQUMsR0FBQSxHQUFNLG9CQUFvQixDQUFDLElBQXJCLENBQTBCLElBQTFCLENBQVAsQ0FBSCxHQUFnRCxHQUFJLENBQUEsQ0FBQSxDQUFwRCxHQUE0RDttQkFDbkUsT0FBQSxDQUFRLElBQVI7VUFISSxDQUZOO1NBRFc7ZUFRYixVQUFVLENBQUMsZ0JBQVgsQ0FBNEIsU0FBQyxHQUFEO0FBQzFCLGNBQUE7VUFENEIsU0FBRDtVQUMzQixNQUFBLENBQUE7aUJBQ0EsT0FBQSxDQUFRLHlCQUFSO1FBRjBCLENBQTVCO01BVlUsQ0FBWjtJQURjLENBakVoQjs7QUFnRkE7OztJQUlBLGtCQUFBLEVBQW9CLFNBQUE7YUFDbEIsSUFBSSxPQUFKLENBQVksU0FBQyxPQUFELEVBQVUsTUFBVjtBQUNWLFlBQUE7UUFBQSxlQUFBLEdBQWtCLElBQUksQ0FBQyxRQUFRLENBQUMsMkJBQWQsQ0FBQSxDQUEyQyxDQUFDLE1BQTVDLENBQW1ELFNBQUMsQ0FBRDtpQkFBTyxDQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWQsQ0FBK0IsQ0FBQyxDQUFDLElBQWpDO1FBQVgsQ0FBbkQ7UUFDbEIsZUFBQSxHQUFrQixJQUFJLENBQUMsUUFBUSxDQUFDLHdCQUFkLENBQUEsQ0FBd0MsQ0FBQyxNQUF6QyxDQUFnRCxTQUFDLENBQUQ7aUJBQU8sQ0FBQyxDQUFDLFFBQUYsQ0FBVyxnQkFBWDtRQUFQLENBQWhELENBQW9GLENBQUMsR0FBckYsQ0FBeUYsU0FBQyxDQUFEO2lCQUFPLElBQUksQ0FBQyxRQUFMLENBQWMsQ0FBZDtRQUFQLENBQXpGO2VBQ2xCLE9BQUE7O0FBQVE7ZUFBQSxpREFBQTs7eUJBQUcsSUFBSSxDQUFDLElBQU4sR0FBVyxHQUFYLEdBQWMsSUFBSSxDQUFDLE9BQW5CLEdBQTJCLEdBQTNCLEdBQTZCLENBQUksT0FBQSxJQUFJLENBQUMsSUFBTCxFQUFBLGFBQWEsZUFBYixFQUFBLEdBQUEsTUFBQSxDQUFILEdBQXFDLE9BQXJDLEdBQWtELEVBQW5EO0FBQS9COztZQUFSO01BSFUsQ0FBWjtJQURrQixDQXBGcEI7SUEwRkEsaUJBQUEsRUFBbUIsU0FBQTtBQUNqQixVQUFBO01BQUEsYUFBQSxHQUFnQixJQUFJLE9BQUosQ0FBWTtRQUMxQixNQUFBLEVBQVEsZ0NBRGtCO1FBRTFCLFdBQUEsRUFBYSxrQkFGYTtPQUFaO2FBSWhCLEtBQUEsQ0FBTSw2QkFBTixFQUFxQztRQUFDLE9BQUEsRUFBUyxhQUFWO09BQXJDLENBQ0UsQ0FBQyxJQURILENBQ1EsU0FBQyxDQUFEO1FBQU8sSUFBRyxDQUFDLENBQUMsRUFBTDtpQkFBYSxDQUFDLENBQUMsSUFBRixDQUFBLEVBQWI7U0FBQSxNQUFBO2lCQUEyQixPQUFPLENBQUMsTUFBUixDQUFlLENBQUMsQ0FBQyxVQUFqQixFQUEzQjs7TUFBUCxDQURSO0lBTGlCLENBMUZuQjtJQWtHQSxpQkFBQSxFQUFtQixTQUFBO2FBQ2pCLElBQUMsQ0FBQSxpQkFBRCxDQUFBLENBQW9CLENBQUMsSUFBckIsQ0FBMEIsU0FBQyxjQUFEO0FBQ3hCLFlBQUE7UUFBQSxnQkFBQSwwQ0FBb0MsQ0FBRSxPQUFuQixDQUEyQixNQUEzQixFQUFtQyxFQUFuQztRQUNuQixhQUFBLEdBQWdCLGNBQWMsQ0FBQztRQUMvQixRQUFBLEdBQVcsMEJBQUEsSUFBc0IsTUFBTSxDQUFDLEdBQVAsQ0FBVyxnQkFBWCxFQUE2QixhQUE3QjtlQUNqQztVQUFDLFVBQUEsUUFBRDtVQUFXLGVBQUEsYUFBWDtVQUEwQixrQkFBQSxnQkFBMUI7O01BSndCLENBQTFCO0lBRGlCLENBbEduQjtJQXlHQSxpQkFBQSxFQUFtQixTQUFDLFdBQUQ7QUFDakIsVUFBQTtNQUFBLElBQUEsR0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFkLENBQStCLFdBQS9COzRCQUNQLElBQUksQ0FBRSxRQUFRLENBQUM7SUFGRSxDQXpHbkI7SUE2R0EsZ0NBQUEsRUFBa0MsU0FBQyxXQUFEO2FBQ2hDLE9BQUEsQ0FBUSxJQUFJLENBQUMsSUFBTCxDQUFVLElBQUksQ0FBQyxlQUFMLENBQUEsQ0FBc0IsQ0FBQyxZQUFqQyxFQUErQyxjQUEvQyxDQUFSLENBQXVFLENBQUMsbUJBQW9CLENBQUEsV0FBQTtJQUQ1RCxDQTdHbEM7SUFnSEEsb0JBQUEsRUFBc0IsU0FBQyxXQUFEO0FBQ3BCLFVBQUE7TUFBQSxhQUFBLEdBQWdCLElBQUksT0FBSixDQUFZO1FBQzFCLE1BQUEsRUFBUSxnQ0FEa0I7UUFFMUIsV0FBQSxFQUFhLGtCQUZhO09BQVo7YUFJaEIsS0FBQSxDQUFNLCtCQUFBLEdBQWdDLFdBQXRDLEVBQXFEO1FBQUMsT0FBQSxFQUFTLGFBQVY7T0FBckQsQ0FDRSxDQUFDLElBREgsQ0FDUSxTQUFDLENBQUQ7UUFBTyxJQUFHLENBQUMsQ0FBQyxFQUFMO2lCQUFhLENBQUMsQ0FBQyxJQUFGLENBQUEsRUFBYjtTQUFBLE1BQUE7aUJBQTJCLE9BQU8sQ0FBQyxNQUFSLENBQWUsQ0FBQyxDQUFDLFVBQWpCLEVBQTNCOztNQUFQLENBRFI7SUFMb0IsQ0FoSHRCO0lBd0hBLG9CQUFBLEVBQXNCLFNBQUMsV0FBRDthQUNwQixJQUFDLENBQUEsb0JBQUQsQ0FBc0IsV0FBdEIsQ0FBa0MsQ0FBQyxJQUFuQyxDQUF3QyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsaUJBQUQ7QUFDdEMsY0FBQTtVQUFBLGdCQUFBLEdBQW1CLEtBQUMsQ0FBQSxpQkFBRCxDQUFtQixXQUFuQjtVQUNuQixRQUFBLEdBQVcsMEJBQUEsSUFBc0IsTUFBTSxDQUFDLEdBQVAsQ0FBVyxnQkFBWCxFQUE2QixpQkFBaUIsQ0FBQyxRQUFRLENBQUMsTUFBeEQ7VUFDakMsYUFBQSxHQUFnQixpQkFBaUIsQ0FBQyxRQUFRLENBQUM7VUFDM0Msc0JBQUEsR0FBeUIsS0FBQyxDQUFBLGdDQUFELENBQWtDLFdBQWxDO1VBRXpCLElBQUcsTUFBQSxHQUFTLDhCQUFaO1lBS0UsUUFBQSxHQUFXLDBCQUFBLElBQXNCLE1BQU0sQ0FBQyxHQUFQLENBQVcsZ0JBQVgsRUFBNkIsc0JBQTdCLEVBTG5DOztpQkFPQTtZQUFDLFFBQUEsTUFBRDtZQUFTLFVBQUEsUUFBVDtZQUFtQixlQUFBLGFBQW5CO1lBQWtDLGtCQUFBLGdCQUFsQztZQUFvRCx3QkFBQSxzQkFBcEQ7O1FBYnNDO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF4QztJQURvQixDQXhIdEI7O0FBZkYiLCJzb3VyY2VzQ29udGVudCI6WyJvcyA9IHJlcXVpcmUgJ29zJ1xuZnMgPSByZXF1aXJlICdmcydcbnBhdGggPSByZXF1aXJlICdwYXRoJ1xuc2VtdmVyID0gcmVxdWlyZSAnc2VtdmVyJ1xue0J1ZmZlcmVkUHJvY2Vzc30gPSByZXF1aXJlICdhdG9tJ1xuXG4jIyNcbkEgY29sbGVjdGlvbiBvZiBtZXRob2RzIGZvciByZXRyaWV2aW5nIGluZm9ybWF0aW9uIGFib3V0IHRoZSB1c2VyJ3Mgc3lzdGVtIGZvclxuYnVnIHJlcG9ydCBwdXJwb3Nlcy5cbiMjI1xuXG5ERVZfUEFDS0FHRV9QQVRIID0gcGF0aC5qb2luKCdkZXYnLCAncGFja2FnZXMnKVxuXG5tb2R1bGUuZXhwb3J0cyA9XG5cbiAgIyMjXG4gIFNlY3Rpb246IFN5c3RlbSBJbmZvcm1hdGlvblxuICAjIyNcblxuICBnZXRQbGF0Zm9ybTogLT5cbiAgICBvcy5wbGF0Zm9ybSgpXG5cbiAgIyBPUyB2ZXJzaW9uIHN0cmluZ3MgbGlmdGVkIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL2xlZS1kb2htL2J1Zy1yZXBvcnRcbiAgZ2V0T1NWZXJzaW9uOiAtPlxuICAgIG5ldyBQcm9taXNlIChyZXNvbHZlLCByZWplY3QpID0+XG4gICAgICBzd2l0Y2ggQGdldFBsYXRmb3JtKClcbiAgICAgICAgd2hlbiAnZGFyd2luJyB0aGVuIHJlc29sdmUoQG1hY1ZlcnNpb25UZXh0KCkpXG4gICAgICAgIHdoZW4gJ3dpbjMyJyB0aGVuIHJlc29sdmUoQHdpblZlcnNpb25UZXh0KCkpXG4gICAgICAgIHdoZW4gJ2xpbnV4JyB0aGVuIHJlc29sdmUoQGxpbnV4VmVyc2lvblRleHQoKSlcbiAgICAgICAgZWxzZSByZXNvbHZlKFwiI3tvcy5wbGF0Zm9ybSgpfSAje29zLnJlbGVhc2UoKX1cIilcblxuICBtYWNWZXJzaW9uVGV4dDogLT5cbiAgICBAbWFjVmVyc2lvbkluZm8oKS50aGVuIChpbmZvKSAtPlxuICAgICAgcmV0dXJuICdVbmtub3duIG1hY09TIHZlcnNpb24nIHVubGVzcyBpbmZvLlByb2R1Y3ROYW1lIGFuZCBpbmZvLlByb2R1Y3RWZXJzaW9uXG4gICAgICBcIiN7aW5mby5Qcm9kdWN0TmFtZX0gI3tpbmZvLlByb2R1Y3RWZXJzaW9ufVwiXG5cbiAgbWFjVmVyc2lvbkluZm86IC0+XG4gICAgbmV3IFByb21pc2UgKHJlc29sdmUsIHJlamVjdCkgLT5cbiAgICAgIHN0ZG91dCA9ICcnXG4gICAgICBwbGlzdEJ1ZGR5ID0gbmV3IEJ1ZmZlcmVkUHJvY2Vzc1xuICAgICAgICBjb21tYW5kOiAnL3Vzci9saWJleGVjL1BsaXN0QnVkZHknXG4gICAgICAgIGFyZ3M6IFtcbiAgICAgICAgICAnLWMnXG4gICAgICAgICAgJ1ByaW50IFByb2R1Y3RWZXJzaW9uJ1xuICAgICAgICAgICctYydcbiAgICAgICAgICAnUHJpbnQgUHJvZHVjdE5hbWUnXG4gICAgICAgICAgJy9TeXN0ZW0vTGlicmFyeS9Db3JlU2VydmljZXMvU3lzdGVtVmVyc2lvbi5wbGlzdCdcbiAgICAgICAgXVxuICAgICAgICBzdGRvdXQ6IChvdXRwdXQpIC0+IHN0ZG91dCArPSBvdXRwdXRcbiAgICAgICAgZXhpdDogLT5cbiAgICAgICAgICBbUHJvZHVjdFZlcnNpb24sIFByb2R1Y3ROYW1lXSA9IHN0ZG91dC50cmltKCkuc3BsaXQoJ1xcbicpXG4gICAgICAgICAgcmVzb2x2ZSh7UHJvZHVjdFZlcnNpb24sIFByb2R1Y3ROYW1lfSlcblxuICAgICAgcGxpc3RCdWRkeS5vbldpbGxUaHJvd0Vycm9yICh7aGFuZGxlfSkgLT5cbiAgICAgICAgaGFuZGxlKClcbiAgICAgICAgcmVzb2x2ZSh7fSlcblxuICBsaW51eFZlcnNpb25UZXh0OiAtPlxuICAgIEBsaW51eFZlcnNpb25JbmZvKCkudGhlbiAoaW5mbykgLT5cbiAgICAgIGlmIGluZm8uRGlzdHJvTmFtZSBhbmQgaW5mby5EaXN0cm9WZXJzaW9uXG4gICAgICAgIFwiI3tpbmZvLkRpc3Ryb05hbWV9ICN7aW5mby5EaXN0cm9WZXJzaW9ufVwiXG4gICAgICBlbHNlXG4gICAgICAgIFwiI3tvcy5wbGF0Zm9ybSgpfSAje29zLnJlbGVhc2UoKX1cIlxuXG4gIGxpbnV4VmVyc2lvbkluZm86IC0+XG4gICAgbmV3IFByb21pc2UgKHJlc29sdmUsIHJlamVjdCkgLT5cbiAgICAgIHN0ZG91dCA9ICcnXG5cbiAgICAgIGxzYlJlbGVhc2UgPSBuZXcgQnVmZmVyZWRQcm9jZXNzXG4gICAgICAgIGNvbW1hbmQ6ICdsc2JfcmVsZWFzZSdcbiAgICAgICAgYXJnczogWyctZHMnXVxuICAgICAgICBzdGRvdXQ6IChvdXRwdXQpIC0+IHN0ZG91dCArPSBvdXRwdXRcbiAgICAgICAgZXhpdDogKGV4aXRDb2RlKSAtPlxuICAgICAgICAgIFtEaXN0cm9OYW1lLCBEaXN0cm9WZXJzaW9uXSA9IHN0ZG91dC50cmltKCkuc3BsaXQoJyAnKVxuICAgICAgICAgIHJlc29sdmUoe0Rpc3Ryb05hbWUsIERpc3Ryb1ZlcnNpb259KVxuXG4gICAgICBsc2JSZWxlYXNlLm9uV2lsbFRocm93RXJyb3IgKHtoYW5kbGV9KSAtPlxuICAgICAgICBoYW5kbGUoKVxuICAgICAgICByZXNvbHZlKHt9KVxuXG4gIHdpblZlcnNpb25UZXh0OiAtPlxuICAgIG5ldyBQcm9taXNlIChyZXNvbHZlLCByZWplY3QpIC0+XG4gICAgICBkYXRhID0gW11cbiAgICAgIHN5c3RlbUluZm8gPSBuZXcgQnVmZmVyZWRQcm9jZXNzXG4gICAgICAgIGNvbW1hbmQ6ICdzeXN0ZW1pbmZvJ1xuICAgICAgICBzdGRvdXQ6IChvbmVMaW5lKSAtPiBkYXRhLnB1c2gob25lTGluZSlcbiAgICAgICAgZXhpdDogLT5cbiAgICAgICAgICBpbmZvID0gZGF0YS5qb2luKCdcXG4nKVxuICAgICAgICAgIGluZm8gPSBpZiAocmVzID0gL09TLk5hbWUuXFxzKyguKikkL2ltLmV4ZWMoaW5mbykpIHRoZW4gcmVzWzFdIGVsc2UgJ1Vua25vd24gV2luZG93cyB2ZXJzaW9uJ1xuICAgICAgICAgIHJlc29sdmUoaW5mbylcblxuICAgICAgc3lzdGVtSW5mby5vbldpbGxUaHJvd0Vycm9yICh7aGFuZGxlfSkgLT5cbiAgICAgICAgaGFuZGxlKClcbiAgICAgICAgcmVzb2x2ZSgnVW5rbm93biBXaW5kb3dzIHZlcnNpb24nKVxuXG4gICMjI1xuICBTZWN0aW9uOiBJbnN0YWxsZWQgUGFja2FnZXNcbiAgIyMjXG5cbiAgZ2V0Tm9uQ29yZVBhY2thZ2VzOiAtPlxuICAgIG5ldyBQcm9taXNlIChyZXNvbHZlLCByZWplY3QpIC0+XG4gICAgICBub25Db3JlUGFja2FnZXMgPSBhdG9tLnBhY2thZ2VzLmdldEF2YWlsYWJsZVBhY2thZ2VNZXRhZGF0YSgpLmZpbHRlcigocCkgLT4gbm90IGF0b20ucGFja2FnZXMuaXNCdW5kbGVkUGFja2FnZShwLm5hbWUpKVxuICAgICAgZGV2UGFja2FnZU5hbWVzID0gYXRvbS5wYWNrYWdlcy5nZXRBdmFpbGFibGVQYWNrYWdlUGF0aHMoKS5maWx0ZXIoKHApIC0+IHAuaW5jbHVkZXMoREVWX1BBQ0tBR0VfUEFUSCkpLm1hcCgocCkgLT4gcGF0aC5iYXNlbmFtZShwKSlcbiAgICAgIHJlc29sdmUoXCIje3BhY2submFtZX0gI3twYWNrLnZlcnNpb259ICN7aWYgcGFjay5uYW1lIGluIGRldlBhY2thZ2VOYW1lcyB0aGVuICcoZGV2KScgZWxzZSAnJ31cIiBmb3IgcGFjayBpbiBub25Db3JlUGFja2FnZXMpXG5cbiAgZ2V0TGF0ZXN0QXRvbURhdGE6IC0+XG4gICAgZ2l0aHViSGVhZGVycyA9IG5ldyBIZWFkZXJzKHtcbiAgICAgIGFjY2VwdDogJ2FwcGxpY2F0aW9uL3ZuZC5naXRodWIudjMranNvbicsXG4gICAgICBjb250ZW50VHlwZTogXCJhcHBsaWNhdGlvbi9qc29uXCJcbiAgICB9KVxuICAgIGZldGNoICdodHRwczovL2F0b20uaW8vYXBpL3VwZGF0ZXMnLCB7aGVhZGVyczogZ2l0aHViSGVhZGVyc31cbiAgICAgIC50aGVuIChyKSAtPiBpZiByLm9rIHRoZW4gci5qc29uKCkgZWxzZSBQcm9taXNlLnJlamVjdCByLnN0YXR1c0NvZGVcblxuICBjaGVja0F0b21VcFRvRGF0ZTogLT5cbiAgICBAZ2V0TGF0ZXN0QXRvbURhdGEoKS50aGVuIChsYXRlc3RBdG9tRGF0YSkgLT5cbiAgICAgIGluc3RhbGxlZFZlcnNpb24gPSBhdG9tLmdldFZlcnNpb24oKT8ucmVwbGFjZSgvLS4qJC8sICcnKVxuICAgICAgbGF0ZXN0VmVyc2lvbiA9IGxhdGVzdEF0b21EYXRhLm5hbWVcbiAgICAgIHVwVG9EYXRlID0gaW5zdGFsbGVkVmVyc2lvbj8gYW5kIHNlbXZlci5ndGUoaW5zdGFsbGVkVmVyc2lvbiwgbGF0ZXN0VmVyc2lvbilcbiAgICAgIHt1cFRvRGF0ZSwgbGF0ZXN0VmVyc2lvbiwgaW5zdGFsbGVkVmVyc2lvbn1cblxuICBnZXRQYWNrYWdlVmVyc2lvbjogKHBhY2thZ2VOYW1lKSAtPlxuICAgIHBhY2sgPSBhdG9tLnBhY2thZ2VzLmdldExvYWRlZFBhY2thZ2UocGFja2FnZU5hbWUpXG4gICAgcGFjaz8ubWV0YWRhdGEudmVyc2lvblxuXG4gIGdldFBhY2thZ2VWZXJzaW9uU2hpcHBlZFdpdGhBdG9tOiAocGFja2FnZU5hbWUpIC0+XG4gICAgcmVxdWlyZShwYXRoLmpvaW4oYXRvbS5nZXRMb2FkU2V0dGluZ3MoKS5yZXNvdXJjZVBhdGgsICdwYWNrYWdlLmpzb24nKSkucGFja2FnZURlcGVuZGVuY2llc1twYWNrYWdlTmFtZV1cblxuICBnZXRMYXRlc3RQYWNrYWdlRGF0YTogKHBhY2thZ2VOYW1lKSAtPlxuICAgIGdpdGh1YkhlYWRlcnMgPSBuZXcgSGVhZGVycyh7XG4gICAgICBhY2NlcHQ6ICdhcHBsaWNhdGlvbi92bmQuZ2l0aHViLnYzK2pzb24nLFxuICAgICAgY29udGVudFR5cGU6IFwiYXBwbGljYXRpb24vanNvblwiXG4gICAgfSlcbiAgICBmZXRjaCBcImh0dHBzOi8vYXRvbS5pby9hcGkvcGFja2FnZXMvI3twYWNrYWdlTmFtZX1cIiwge2hlYWRlcnM6IGdpdGh1YkhlYWRlcnN9XG4gICAgICAudGhlbiAocikgLT4gaWYgci5vayB0aGVuIHIuanNvbigpIGVsc2UgUHJvbWlzZS5yZWplY3Qgci5zdGF0dXNDb2RlXG5cbiAgY2hlY2tQYWNrYWdlVXBUb0RhdGU6IChwYWNrYWdlTmFtZSkgLT5cbiAgICBAZ2V0TGF0ZXN0UGFja2FnZURhdGEocGFja2FnZU5hbWUpLnRoZW4gKGxhdGVzdFBhY2thZ2VEYXRhKSA9PlxuICAgICAgaW5zdGFsbGVkVmVyc2lvbiA9IEBnZXRQYWNrYWdlVmVyc2lvbihwYWNrYWdlTmFtZSlcbiAgICAgIHVwVG9EYXRlID0gaW5zdGFsbGVkVmVyc2lvbj8gYW5kIHNlbXZlci5ndGUoaW5zdGFsbGVkVmVyc2lvbiwgbGF0ZXN0UGFja2FnZURhdGEucmVsZWFzZXMubGF0ZXN0KVxuICAgICAgbGF0ZXN0VmVyc2lvbiA9IGxhdGVzdFBhY2thZ2VEYXRhLnJlbGVhc2VzLmxhdGVzdFxuICAgICAgdmVyc2lvblNoaXBwZWRXaXRoQXRvbSA9IEBnZXRQYWNrYWdlVmVyc2lvblNoaXBwZWRXaXRoQXRvbShwYWNrYWdlTmFtZSlcblxuICAgICAgaWYgaXNDb3JlID0gdmVyc2lvblNoaXBwZWRXaXRoQXRvbT9cbiAgICAgICAgIyBBIGNvcmUgcGFja2FnZSBpcyBvdXQgb2YgZGF0ZSBpZiB0aGUgdmVyc2lvbiB3aGljaCBpcyBiZWluZyB1c2VkXG4gICAgICAgICMgaXMgbG93ZXIgdGhhbiB0aGUgdmVyc2lvbiB3aGljaCBub3JtYWxseSBzaGlwcyB3aXRoIHRoZSB2ZXJzaW9uXG4gICAgICAgICMgb2YgQXRvbSB3aGljaCBpcyBydW5uaW5nLiBUaGlzIHdpbGwgaGFwcGVuIHdoZW4gdGhlcmUncyBhIGxvY2FsbHlcbiAgICAgICAgIyBpbnN0YWxsZWQgdmVyc2lvbiBvZiB0aGUgcGFja2FnZSB3aXRoIGEgbG93ZXIgdmVyc2lvbiB0aGFuIEF0b20ncy5cbiAgICAgICAgdXBUb0RhdGUgPSBpbnN0YWxsZWRWZXJzaW9uPyBhbmQgc2VtdmVyLmd0ZShpbnN0YWxsZWRWZXJzaW9uLCB2ZXJzaW9uU2hpcHBlZFdpdGhBdG9tKVxuXG4gICAgICB7aXNDb3JlLCB1cFRvRGF0ZSwgbGF0ZXN0VmVyc2lvbiwgaW5zdGFsbGVkVmVyc2lvbiwgdmVyc2lvblNoaXBwZWRXaXRoQXRvbX1cbiJdfQ==
