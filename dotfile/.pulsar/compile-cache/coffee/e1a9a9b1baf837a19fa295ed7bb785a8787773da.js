(function() {
  var BrowserWindow, CompositeDisposable, TabBarView, TabView, ipcRenderer;

  BrowserWindow = null;

  ipcRenderer = require('electron').ipcRenderer;

  CompositeDisposable = require('atom').CompositeDisposable;

  TabView = require('./tab-view');

  module.exports = TabBarView = (function() {
    function TabBarView(pane1, location1) {
      var addElementCommands, item, j, len, ref;
      this.pane = pane1;
      this.location = location1;
      this.element = document.createElement('ul');
      this.element.classList.add("list-inline");
      this.element.classList.add("tab-bar");
      this.element.classList.add("inset-panel");
      this.element.setAttribute('is', 'atom-tabs');
      this.element.setAttribute("tabindex", -1);
      this.element.setAttribute("location", this.location);
      this.tabs = [];
      this.tabsByElement = new WeakMap;
      this.subscriptions = new CompositeDisposable;
      this.paneElement = this.pane.getElement();
      this.subscriptions.add(atom.commands.add(this.paneElement, {
        'tabs:keep-pending-tab': (function(_this) {
          return function() {
            return _this.terminatePendingStates();
          };
        })(this),
        'tabs:close-tab': (function(_this) {
          return function() {
            return _this.closeTab(_this.getActiveTab());
          };
        })(this),
        'tabs:close-other-tabs': (function(_this) {
          return function() {
            return _this.closeOtherTabs(_this.getActiveTab());
          };
        })(this),
        'tabs:close-tabs-to-right': (function(_this) {
          return function() {
            return _this.closeTabsToRight(_this.getActiveTab());
          };
        })(this),
        'tabs:close-tabs-to-left': (function(_this) {
          return function() {
            return _this.closeTabsToLeft(_this.getActiveTab());
          };
        })(this),
        'tabs:close-saved-tabs': (function(_this) {
          return function() {
            return _this.closeSavedTabs();
          };
        })(this),
        'tabs:close-all-tabs': (function(_this) {
          return function(event) {
            event.stopPropagation();
            return _this.closeAllTabs();
          };
        })(this),
        'tabs:open-in-new-window': (function(_this) {
          return function() {
            return _this.openInNewWindow();
          };
        })(this)
      }));
      addElementCommands = (function(_this) {
        return function(commands) {
          var commandsWithPropagationStopped;
          commandsWithPropagationStopped = {};
          Object.keys(commands).forEach(function(name) {
            return commandsWithPropagationStopped[name] = function(event) {
              event.stopPropagation();
              return commands[name]();
            };
          });
          return _this.subscriptions.add(atom.commands.add(_this.element, commandsWithPropagationStopped));
        };
      })(this);
      addElementCommands({
        'tabs:close-tab': (function(_this) {
          return function() {
            return _this.closeTab();
          };
        })(this),
        'tabs:close-other-tabs': (function(_this) {
          return function() {
            return _this.closeOtherTabs();
          };
        })(this),
        'tabs:close-tabs-to-right': (function(_this) {
          return function() {
            return _this.closeTabsToRight();
          };
        })(this),
        'tabs:close-tabs-to-left': (function(_this) {
          return function() {
            return _this.closeTabsToLeft();
          };
        })(this),
        'tabs:close-saved-tabs': (function(_this) {
          return function() {
            return _this.closeSavedTabs();
          };
        })(this),
        'tabs:close-all-tabs': (function(_this) {
          return function() {
            return _this.closeAllTabs();
          };
        })(this),
        'tabs:split-up': (function(_this) {
          return function() {
            return _this.splitTab('splitUp');
          };
        })(this),
        'tabs:split-down': (function(_this) {
          return function() {
            return _this.splitTab('splitDown');
          };
        })(this),
        'tabs:split-left': (function(_this) {
          return function() {
            return _this.splitTab('splitLeft');
          };
        })(this),
        'tabs:split-right': (function(_this) {
          return function() {
            return _this.splitTab('splitRight');
          };
        })(this)
      });
      this.element.addEventListener("mouseenter", this.onMouseEnter.bind(this));
      this.element.addEventListener("mouseleave", this.onMouseLeave.bind(this));
      this.element.addEventListener("mousewheel", this.onMouseWheel.bind(this));
      this.element.addEventListener("dragstart", this.onDragStart.bind(this));
      this.element.addEventListener("dragend", this.onDragEnd.bind(this));
      this.element.addEventListener("dragleave", this.onDragLeave.bind(this));
      this.element.addEventListener("dragover", this.onDragOver.bind(this));
      this.element.addEventListener("drop", this.onDrop.bind(this));
      this.paneElement.addEventListener('dragenter', this.onPaneDragEnter.bind(this));
      this.paneElement.addEventListener('dragleave', this.onPaneDragLeave.bind(this));
      this.paneContainer = this.pane.getContainer();
      ref = this.pane.getItems();
      for (j = 0, len = ref.length; j < len; j++) {
        item = ref[j];
        this.addTabForItem(item);
      }
      this.subscriptions.add(this.pane.onDidDestroy((function(_this) {
        return function() {
          return _this.destroy();
        };
      })(this)));
      this.subscriptions.add(this.pane.onDidAddItem((function(_this) {
        return function(arg) {
          var index, item;
          item = arg.item, index = arg.index;
          return _this.addTabForItem(item, index);
        };
      })(this)));
      this.subscriptions.add(this.pane.onDidMoveItem((function(_this) {
        return function(arg) {
          var item, newIndex;
          item = arg.item, newIndex = arg.newIndex;
          return _this.moveItemTabToIndex(item, newIndex);
        };
      })(this)));
      this.subscriptions.add(this.pane.onDidRemoveItem((function(_this) {
        return function(arg) {
          var item;
          item = arg.item;
          return _this.removeTabForItem(item);
        };
      })(this)));
      this.subscriptions.add(this.pane.onDidChangeActiveItem((function(_this) {
        return function(item) {
          return _this.updateActiveTab();
        };
      })(this)));
      this.subscriptions.add(atom.config.observe('tabs.tabScrolling', (function(_this) {
        return function(value) {
          return _this.updateTabScrolling(value);
        };
      })(this)));
      this.subscriptions.add(atom.config.observe('tabs.tabScrollingThreshold', (function(_this) {
        return function(value) {
          return _this.updateTabScrollingThreshold(value);
        };
      })(this)));
      this.subscriptions.add(atom.config.observe('tabs.alwaysShowTabBar', (function(_this) {
        return function() {
          return _this.updateTabBarVisibility();
        };
      })(this)));
      this.updateActiveTab();
      this.element.addEventListener("mousedown", this.onMouseDown.bind(this));
      this.element.addEventListener("click", this.onClick.bind(this));
      this.element.addEventListener("auxclick", this.onClick.bind(this));
      this.element.addEventListener("dblclick", this.onDoubleClick.bind(this));
      this.onDropOnOtherWindow = this.onDropOnOtherWindow.bind(this);
      ipcRenderer.on('tab:dropped', this.onDropOnOtherWindow);
    }

    TabBarView.prototype.destroy = function() {
      ipcRenderer.removeListener('tab:dropped', this.onDropOnOtherWindow);
      this.subscriptions.dispose();
      return this.element.remove();
    };

    TabBarView.prototype.terminatePendingStates = function() {
      var j, len, ref, tab;
      ref = this.getTabs();
      for (j = 0, len = ref.length; j < len; j++) {
        tab = ref[j];
        if (typeof tab.terminatePendingState === "function") {
          tab.terminatePendingState();
        }
      }
    };

    TabBarView.prototype.addTabForItem = function(item, index) {
      var tabView;
      tabView = new TabView({
        item: item,
        pane: this.pane,
        tabs: this.tabs,
        didClickCloseIcon: (function(_this) {
          return function() {
            _this.closeTab(tabView);
          };
        })(this),
        location: this.location
      });
      if (this.isItemMovingBetweenPanes) {
        tabView.terminatePendingState();
      }
      this.tabsByElement.set(tabView.element, tabView);
      this.insertTabAtIndex(tabView, index);
      if (atom.config.get('tabs.addNewTabsAtEnd')) {
        if (!this.isItemMovingBetweenPanes) {
          return this.pane.moveItem(item, this.pane.getItems().length - 1);
        }
      }
    };

    TabBarView.prototype.moveItemTabToIndex = function(item, index) {
      var tab, tabIndex;
      tabIndex = this.tabs.findIndex(function(t) {
        return t.item === item;
      });
      if (tabIndex !== -1) {
        tab = this.tabs[tabIndex];
        tab.element.remove();
        this.tabs.splice(tabIndex, 1);
        return this.insertTabAtIndex(tab, index);
      }
    };

    TabBarView.prototype.insertTabAtIndex = function(tab, index) {
      var followingTab;
      if (index != null) {
        followingTab = this.tabs[index];
      }
      if (followingTab) {
        this.element.insertBefore(tab.element, followingTab.element);
        this.tabs.splice(index, 0, tab);
      } else {
        this.element.appendChild(tab.element);
        this.tabs.push(tab);
      }
      tab.updateTitle();
      return this.updateTabBarVisibility();
    };

    TabBarView.prototype.removeTabForItem = function(item) {
      var j, len, ref, tab, tabIndex;
      tabIndex = this.tabs.findIndex(function(t) {
        return t.item === item;
      });
      if (tabIndex !== -1) {
        tab = this.tabs[tabIndex];
        this.tabs.splice(tabIndex, 1);
        this.tabsByElement["delete"](tab);
        tab.destroy();
      }
      ref = this.getTabs();
      for (j = 0, len = ref.length; j < len; j++) {
        tab = ref[j];
        tab.updateTitle();
      }
      return this.updateTabBarVisibility();
    };

    TabBarView.prototype.updateTabBarVisibility = function() {
      if (atom.config.get('tabs.alwaysShowTabBar') || this.pane.getItems().length > 1) {
        return this.element.classList.remove('hidden');
      } else {
        return this.element.classList.add('hidden');
      }
    };

    TabBarView.prototype.getTabs = function() {
      return this.tabs.slice();
    };

    TabBarView.prototype.tabAtIndex = function(index) {
      return this.tabs[index];
    };

    TabBarView.prototype.tabForItem = function(item) {
      return this.tabs.find(function(t) {
        return t.item === item;
      });
    };

    TabBarView.prototype.setActiveTab = function(tabView) {
      var ref;
      if ((tabView != null) && tabView !== this.activeTab) {
        if ((ref = this.activeTab) != null) {
          ref.element.classList.remove('active');
        }
        this.activeTab = tabView;
        this.activeTab.element.classList.add('active');
        return this.activeTab.element.scrollIntoView(false);
      }
    };

    TabBarView.prototype.getActiveTab = function() {
      return this.tabForItem(this.pane.getActiveItem());
    };

    TabBarView.prototype.updateActiveTab = function() {
      return this.setActiveTab(this.tabForItem(this.pane.getActiveItem()));
    };

    TabBarView.prototype.closeTab = function(tab) {
      if (tab == null) {
        tab = this.rightClickedTab;
      }
      if (tab != null) {
        return this.pane.destroyItem(tab.item);
      }
    };

    TabBarView.prototype.openInNewWindow = function(tab) {
      var item, itemURI, j, len, pathsToOpen, ref;
      if (tab == null) {
        tab = this.rightClickedTab;
      }
      item = tab != null ? tab.item : void 0;
      if (item == null) {
        return;
      }
      if (typeof item.getURI === 'function') {
        itemURI = item.getURI();
      } else if (typeof item.getPath === 'function') {
        itemURI = item.getPath();
      } else if (typeof item.getUri === 'function') {
        itemURI = item.getUri();
      }
      if (itemURI == null) {
        return;
      }
      this.closeTab(tab);
      ref = this.getTabs();
      for (j = 0, len = ref.length; j < len; j++) {
        tab = ref[j];
        tab.element.style.maxWidth = '';
      }
      pathsToOpen = [atom.project.getPaths(), itemURI].reduce((function(a, b) {
        return a.concat(b);
      }), []);
      return atom.open({
        pathsToOpen: pathsToOpen,
        newWindow: true,
        devMode: atom.devMode,
        safeMode: atom.safeMode
      });
    };

    TabBarView.prototype.splitTab = function(fn) {
      var copiedItem, item, ref;
      if (item = (ref = this.rightClickedTab) != null ? ref.item : void 0) {
        if (copiedItem = typeof item.copy === "function" ? item.copy() : void 0) {
          return this.pane[fn]({
            items: [copiedItem]
          });
        }
      }
    };

    TabBarView.prototype.closeOtherTabs = function(active) {
      var j, len, results, tab, tabs;
      tabs = this.getTabs();
      if (active == null) {
        active = this.rightClickedTab;
      }
      if (active == null) {
        return;
      }
      results = [];
      for (j = 0, len = tabs.length; j < len; j++) {
        tab = tabs[j];
        if (tab !== active) {
          results.push(this.closeTab(tab));
        }
      }
      return results;
    };

    TabBarView.prototype.closeTabsToRight = function(active) {
      var i, index, j, len, results, tab, tabs;
      tabs = this.getTabs();
      if (active == null) {
        active = this.rightClickedTab;
      }
      index = tabs.indexOf(active);
      if (index === -1) {
        return;
      }
      results = [];
      for (i = j = 0, len = tabs.length; j < len; i = ++j) {
        tab = tabs[i];
        if (i > index) {
          results.push(this.closeTab(tab));
        }
      }
      return results;
    };

    TabBarView.prototype.closeTabsToLeft = function(active) {
      var i, index, j, len, results, tab, tabs;
      tabs = this.getTabs();
      if (active == null) {
        active = this.rightClickedTab;
      }
      index = tabs.indexOf(active);
      if (index === -1) {
        return;
      }
      results = [];
      for (i = j = 0, len = tabs.length; j < len; i = ++j) {
        tab = tabs[i];
        if (i < index) {
          results.push(this.closeTab(tab));
        }
      }
      return results;
    };

    TabBarView.prototype.closeSavedTabs = function() {
      var base, j, len, ref, results, tab;
      ref = this.getTabs();
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        tab = ref[j];
        if (!(typeof (base = tab.item).isModified === "function" ? base.isModified() : void 0)) {
          results.push(this.closeTab(tab));
        } else {
          results.push(void 0);
        }
      }
      return results;
    };

    TabBarView.prototype.closeAllTabs = function() {
      var j, len, ref, results, tab;
      ref = this.getTabs();
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        tab = ref[j];
        results.push(this.closeTab(tab));
      }
      return results;
    };

    TabBarView.prototype.getWindowId = function() {
      return this.windowId != null ? this.windowId : this.windowId = atom.getCurrentWindow().id;
    };

    TabBarView.prototype.onDragStart = function(event) {
      var item, itemURI, j, len, location, paneIndex, ref, ref1, ref2, ref3, tabIndex;
      this.draggedTab = this.tabForElement(event.target);
      if (!this.draggedTab) {
        return;
      }
      this.lastDropTargetIndex = null;
      event.dataTransfer.setData('atom-tab-event', 'true');
      this.draggedTab.element.classList.add('is-dragging');
      this.draggedTab.destroyTooltip();
      tabIndex = this.tabs.indexOf(this.draggedTab);
      event.dataTransfer.setData('sortable-index', tabIndex);
      paneIndex = this.paneContainer.getPanes().indexOf(this.pane);
      event.dataTransfer.setData('from-pane-index', paneIndex);
      event.dataTransfer.setData('from-pane-id', this.pane.id);
      event.dataTransfer.setData('from-window-id', this.getWindowId());
      item = this.pane.getItems()[this.tabs.indexOf(this.draggedTab)];
      if (item == null) {
        return;
      }
      if (typeof item.getURI === 'function') {
        itemURI = (ref = item.getURI()) != null ? ref : '';
      } else if (typeof item.getPath === 'function') {
        itemURI = (ref1 = item.getPath()) != null ? ref1 : '';
      } else if (typeof item.getUri === 'function') {
        itemURI = (ref2 = item.getUri()) != null ? ref2 : '';
      }
      if (typeof item.getAllowedLocations === 'function') {
        ref3 = item.getAllowedLocations();
        for (j = 0, len = ref3.length; j < len; j++) {
          location = ref3[j];
          event.dataTransfer.setData("allowed-location-" + location, 'true');
        }
      } else {
        event.dataTransfer.setData('allow-all-locations', 'true');
      }
      if (itemURI != null) {
        event.dataTransfer.setData('text/plain', itemURI);
        if (process.platform === 'darwin') {
          if (!this.uriHasProtocol(itemURI)) {
            itemURI = "file://" + itemURI;
          }
          event.dataTransfer.setData('text/uri-list', itemURI);
        }
        if ((typeof item.isModified === "function" ? item.isModified() : void 0) && (item.getText != null)) {
          event.dataTransfer.setData('has-unsaved-changes', 'true');
          return event.dataTransfer.setData('modified-text', item.getText());
        }
      }
    };

    TabBarView.prototype.uriHasProtocol = function(uri) {
      var error;
      try {
        return require('url').parse(uri).protocol != null;
      } catch (error1) {
        error = error1;
        return false;
      }
    };

    TabBarView.prototype.onDragLeave = function(event) {
      var j, len, ref, results, tab;
      if (!event.currentTarget.contains(event.relatedTarget)) {
        this.removePlaceholder();
        this.lastDropTargetIndex = null;
        ref = this.getTabs();
        results = [];
        for (j = 0, len = ref.length; j < len; j++) {
          tab = ref[j];
          results.push(tab.element.style.maxWidth = '');
        }
        return results;
      }
    };

    TabBarView.prototype.onDragEnd = function(event) {
      if (!this.tabForElement(event.target)) {
        return;
      }
      return this.clearDropTarget();
    };

    TabBarView.prototype.onDragOver = function(event) {
      var newDropTargetIndex, placeholder, sibling, tab, tabs;
      if (!this.isAtomTabEvent(event)) {
        return;
      }
      if (!this.itemIsAllowed(event, this.location)) {
        return;
      }
      event.preventDefault();
      event.stopPropagation();
      newDropTargetIndex = this.getDropTargetIndex(event);
      if (newDropTargetIndex == null) {
        return;
      }
      if (this.lastDropTargetIndex === newDropTargetIndex) {
        return;
      }
      this.lastDropTargetIndex = newDropTargetIndex;
      this.removeDropTargetClasses();
      tabs = this.getTabs();
      placeholder = this.getPlaceholder();
      if (placeholder == null) {
        return;
      }
      if (newDropTargetIndex < tabs.length) {
        tab = tabs[newDropTargetIndex];
        tab.element.classList.add('is-drop-target');
        return tab.element.parentElement.insertBefore(placeholder, tab.element);
      } else {
        if (tab = tabs[newDropTargetIndex - 1]) {
          tab.element.classList.add('drop-target-is-after');
          if (sibling = tab.element.nextSibling) {
            return tab.element.parentElement.insertBefore(placeholder, sibling);
          } else {
            return tab.element.parentElement.appendChild(placeholder);
          }
        }
      }
    };

    TabBarView.prototype.onDropOnOtherWindow = function(event, fromPaneId, fromItemIndex) {
      var itemToRemove;
      if (this.pane.id === fromPaneId) {
        if (itemToRemove = this.pane.getItems()[fromItemIndex]) {
          this.pane.destroyItem(itemToRemove);
        }
      }
      return this.clearDropTarget();
    };

    TabBarView.prototype.clearDropTarget = function() {
      var ref, ref1;
      if ((ref = this.draggedTab) != null) {
        ref.element.classList.remove('is-dragging');
      }
      if ((ref1 = this.draggedTab) != null) {
        ref1.updateTooltip();
      }
      this.draggedTab = null;
      this.removeDropTargetClasses();
      return this.removePlaceholder();
    };

    TabBarView.prototype.onDrop = function(event) {
      var droppedURI, fromIndex, fromPane, fromPaneId, fromPaneIndex, fromWindowId, hasUnsavedChanges, item, modifiedText, toIndex, toPane;
      if (!this.isAtomTabEvent(event)) {
        return;
      }
      event.preventDefault();
      fromWindowId = parseInt(event.dataTransfer.getData('from-window-id'));
      fromPaneId = parseInt(event.dataTransfer.getData('from-pane-id'));
      fromIndex = parseInt(event.dataTransfer.getData('sortable-index'));
      fromPaneIndex = parseInt(event.dataTransfer.getData('from-pane-index'));
      hasUnsavedChanges = event.dataTransfer.getData('has-unsaved-changes') === 'true';
      modifiedText = event.dataTransfer.getData('modified-text');
      toIndex = this.getDropTargetIndex(event);
      toPane = this.pane;
      this.clearDropTarget();
      if (!this.itemIsAllowed(event, this.location)) {
        return;
      }
      if (fromWindowId === this.getWindowId()) {
        fromPane = this.paneContainer.getPanes()[fromPaneIndex];
        if ((fromPane != null ? fromPane.id : void 0) !== fromPaneId) {
          fromPane = Array.from(document.querySelectorAll('atom-pane')).map(function(paneEl) {
            return paneEl.model;
          }).find(function(pane) {
            return pane.id === fromPaneId;
          });
        }
        item = fromPane.getItems()[fromIndex];
        if (item != null) {
          return this.moveItemBetweenPanes(fromPane, fromIndex, toPane, toIndex, item);
        }
      } else {
        droppedURI = event.dataTransfer.getData('text/plain');
        atom.workspace.open(droppedURI).then((function(_this) {
          return function(item) {
            var activeItemIndex, activePane, browserWindow;
            activePane = atom.workspace.getActivePane();
            activeItemIndex = activePane.getItems().indexOf(item);
            _this.moveItemBetweenPanes(activePane, activeItemIndex, toPane, toIndex, item);
            if (hasUnsavedChanges) {
              if (typeof item.setText === "function") {
                item.setText(modifiedText);
              }
            }
            if (!isNaN(fromWindowId)) {
              browserWindow = _this.browserWindowForId(fromWindowId);
              return browserWindow != null ? browserWindow.webContents.send('tab:dropped', fromPaneId, fromIndex) : void 0;
            }
          };
        })(this));
        return atom.focus();
      }
    };

    TabBarView.prototype.onPaneDragEnter = function(event) {
      if (!this.isAtomTabEvent(event)) {
        return;
      }
      if (!this.itemIsAllowed(event, this.location)) {
        return;
      }
      if (this.pane.getItems().length > 1 || atom.config.get('tabs.alwaysShowTabBar')) {
        return;
      }
      if (this.paneElement.contains(event.relatedTarget)) {
        return this.element.classList.remove('hidden');
      }
    };

    TabBarView.prototype.onPaneDragLeave = function(event) {
      if (!this.isAtomTabEvent(event)) {
        return;
      }
      if (!this.itemIsAllowed(event, this.location)) {
        return;
      }
      if (this.pane.getItems().length > 1 || atom.config.get('tabs.alwaysShowTabBar')) {
        return;
      }
      if (!this.paneElement.contains(event.relatedTarget)) {
        return this.element.classList.add('hidden');
      }
    };

    TabBarView.prototype.onMouseWheel = function(event) {
      if (event.shiftKey || !this.tabScrolling) {
        return;
      }
      if (this.wheelDelta == null) {
        this.wheelDelta = 0;
      }
      this.wheelDelta += event.wheelDeltaY;
      if (this.wheelDelta <= -this.tabScrollingThreshold) {
        this.wheelDelta = 0;
        return this.pane.activateNextItem();
      } else if (this.wheelDelta >= this.tabScrollingThreshold) {
        this.wheelDelta = 0;
        return this.pane.activatePreviousItem();
      }
    };

    TabBarView.prototype.onMouseDown = function(event) {
      var tab;
      if (!this.pane.isDestroyed()) {
        this.pane.activate();
      }
      tab = this.tabForElement(event.target);
      if (!tab) {
        return;
      }
      if (event.button === 2 || (event.button === 0 && event.ctrlKey === true)) {
        this.rightClickedTab = tab;
        return event.preventDefault();
      } else if (event.button === 1) {
        return event.preventDefault();
      }
    };

    TabBarView.prototype.onClick = function(event) {
      var tab;
      tab = this.tabForElement(event.target);
      if (!tab) {
        return;
      }
      event.preventDefault();
      if (event.button === 2 || (event.button === 0 && event.ctrlKey === true)) {

      } else if (event.button === 0 && !event.target.classList.contains('close-icon')) {
        return this.pane.activateItem(tab.item);
      } else if (event.button === 1) {
        return this.pane.destroyItem(tab.item);
      }
    };

    TabBarView.prototype.onDoubleClick = function(event) {
      var base, tab;
      if (tab = this.tabForElement(event.target)) {
        return typeof (base = tab.item).terminatePendingState === "function" ? base.terminatePendingState() : void 0;
      } else if (event.target === this.element) {
        atom.commands.dispatch(this.element, 'application:new-file');
        return event.preventDefault();
      }
    };

    TabBarView.prototype.updateTabScrollingThreshold = function(value) {
      return this.tabScrollingThreshold = value;
    };

    TabBarView.prototype.updateTabScrolling = function(value) {
      if (value === 'platform') {
        return this.tabScrolling = process.platform === 'linux';
      } else {
        return this.tabScrolling = value;
      }
    };

    TabBarView.prototype.browserWindowForId = function(id) {
      if (BrowserWindow == null) {
        BrowserWindow = require('electron').remote.BrowserWindow;
      }
      return BrowserWindow.fromId(id);
    };

    TabBarView.prototype.moveItemBetweenPanes = function(fromPane, fromIndex, toPane, toIndex, item) {
      try {
        if (toPane === fromPane) {
          if (fromIndex < toIndex) {
            toIndex--;
          }
          toPane.moveItem(item, toIndex);
        } else {
          this.isItemMovingBetweenPanes = true;
          fromPane.moveItemToPane(item, toPane, toIndex--);
        }
        toPane.activateItem(item);
        return toPane.activate();
      } finally {
        this.isItemMovingBetweenPanes = false;
      }
    };

    TabBarView.prototype.removeDropTargetClasses = function() {
      var dropTarget, j, k, len, len1, ref, ref1, results, workspaceElement;
      workspaceElement = atom.workspace.getElement();
      ref = workspaceElement.querySelectorAll('.tab-bar .is-drop-target');
      for (j = 0, len = ref.length; j < len; j++) {
        dropTarget = ref[j];
        dropTarget.classList.remove('is-drop-target');
      }
      ref1 = workspaceElement.querySelectorAll('.tab-bar .drop-target-is-after');
      results = [];
      for (k = 0, len1 = ref1.length; k < len1; k++) {
        dropTarget = ref1[k];
        results.push(dropTarget.classList.remove('drop-target-is-after'));
      }
      return results;
    };

    TabBarView.prototype.getDropTargetIndex = function(event) {
      var elementCenter, elementIndex, left, ref, tab, tabs, target, width;
      target = event.target;
      if (this.isPlaceholder(target)) {
        return;
      }
      tabs = this.getTabs();
      tab = this.tabForElement(target);
      if (tab == null) {
        tab = tabs[tabs.length - 1];
      }
      if (tab == null) {
        return 0;
      }
      ref = tab.element.getBoundingClientRect(), left = ref.left, width = ref.width;
      elementCenter = left + width / 2;
      elementIndex = tabs.indexOf(tab);
      if (event.pageX < elementCenter) {
        return elementIndex;
      } else {
        return elementIndex + 1;
      }
    };

    TabBarView.prototype.getPlaceholder = function() {
      if (this.placeholderEl != null) {
        return this.placeholderEl;
      }
      this.placeholderEl = document.createElement("li");
      this.placeholderEl.classList.add("placeholder");
      return this.placeholderEl;
    };

    TabBarView.prototype.removePlaceholder = function() {
      var ref;
      if ((ref = this.placeholderEl) != null) {
        ref.remove();
      }
      return this.placeholderEl = null;
    };

    TabBarView.prototype.isPlaceholder = function(element) {
      return element.classList.contains('placeholder');
    };

    TabBarView.prototype.onMouseEnter = function() {
      var j, len, ref, tab, width;
      ref = this.getTabs();
      for (j = 0, len = ref.length; j < len; j++) {
        tab = ref[j];
        width = tab.element.getBoundingClientRect().width;
        tab.element.style.maxWidth = width.toFixed(2) + 'px';
      }
    };

    TabBarView.prototype.onMouseLeave = function() {
      var j, len, ref, tab;
      ref = this.getTabs();
      for (j = 0, len = ref.length; j < len; j++) {
        tab = ref[j];
        tab.element.style.maxWidth = '';
      }
    };

    TabBarView.prototype.tabForElement = function(element) {
      var currentElement, tab;
      currentElement = element;
      while (currentElement != null) {
        if (tab = this.tabsByElement.get(currentElement)) {
          return tab;
        } else {
          currentElement = currentElement.parentElement;
        }
      }
    };

    TabBarView.prototype.isAtomTabEvent = function(event) {
      var item, j, len, ref;
      ref = event.dataTransfer.items;
      for (j = 0, len = ref.length; j < len; j++) {
        item = ref[j];
        if (item.type === 'atom-tab-event') {
          return true;
        }
      }
      return false;
    };

    TabBarView.prototype.itemIsAllowed = function(event, location) {
      var item, j, len, ref;
      ref = event.dataTransfer.items;
      for (j = 0, len = ref.length; j < len; j++) {
        item = ref[j];
        if (item.type === 'allow-all-locations' || item.type === ("allowed-location-" + location)) {
          return true;
        }
      }
      return false;
    };

    return TabBarView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90YWJzL2xpYi90YWItYmFyLXZpZXcuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQSxhQUFBLEdBQWdCOztFQUNmLGNBQWUsT0FBQSxDQUFRLFVBQVI7O0VBRWYsc0JBQXVCLE9BQUEsQ0FBUSxNQUFSOztFQUN4QixPQUFBLEdBQVUsT0FBQSxDQUFRLFlBQVI7O0VBRVYsTUFBTSxDQUFDLE9BQVAsR0FDTTtJQUNTLG9CQUFDLEtBQUQsRUFBUSxTQUFSO0FBQ1gsVUFBQTtNQURZLElBQUMsQ0FBQSxPQUFEO01BQU8sSUFBQyxDQUFBLFdBQUQ7TUFDbkIsSUFBQyxDQUFBLE9BQUQsR0FBVyxRQUFRLENBQUMsYUFBVCxDQUF1QixJQUF2QjtNQUNYLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLGFBQXZCO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsU0FBdkI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixhQUF2QjtNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFBVCxDQUFzQixJQUF0QixFQUE0QixXQUE1QjtNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFBVCxDQUFzQixVQUF0QixFQUFrQyxDQUFDLENBQW5DO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxZQUFULENBQXNCLFVBQXRCLEVBQWtDLElBQUMsQ0FBQSxRQUFuQztNQUVBLElBQUMsQ0FBQSxJQUFELEdBQVE7TUFDUixJQUFDLENBQUEsYUFBRCxHQUFpQixJQUFJO01BQ3JCLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUk7TUFFckIsSUFBQyxDQUFBLFdBQUQsR0FBZSxJQUFDLENBQUEsSUFBSSxDQUFDLFVBQU4sQ0FBQTtNQUVmLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsSUFBQyxDQUFBLFdBQW5CLEVBQ2pCO1FBQUEsdUJBQUEsRUFBeUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsc0JBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6QjtRQUNBLGdCQUFBLEVBQWtCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLFFBQUQsQ0FBVSxLQUFDLENBQUEsWUFBRCxDQUFBLENBQVY7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FEbEI7UUFFQSx1QkFBQSxFQUF5QixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxjQUFELENBQWdCLEtBQUMsQ0FBQSxZQUFELENBQUEsQ0FBaEI7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FGekI7UUFHQSwwQkFBQSxFQUE0QixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxnQkFBRCxDQUFrQixLQUFDLENBQUEsWUFBRCxDQUFBLENBQWxCO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBSDVCO1FBSUEseUJBQUEsRUFBMkIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsZUFBRCxDQUFpQixLQUFDLENBQUEsWUFBRCxDQUFBLENBQWpCO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBSjNCO1FBS0EsdUJBQUEsRUFBeUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsY0FBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBTHpCO1FBTUEscUJBQUEsRUFBdUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQyxLQUFEO1lBQ3JCLEtBQUssQ0FBQyxlQUFOLENBQUE7bUJBQ0EsS0FBQyxDQUFBLFlBQUQsQ0FBQTtVQUZxQjtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FOdkI7UUFTQSx5QkFBQSxFQUEyQixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxlQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FUM0I7T0FEaUIsQ0FBbkI7TUFZQSxrQkFBQSxHQUFxQixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsUUFBRDtBQUNuQixjQUFBO1VBQUEsOEJBQUEsR0FBaUM7VUFDakMsTUFBTSxDQUFDLElBQVAsQ0FBWSxRQUFaLENBQXFCLENBQUMsT0FBdEIsQ0FBOEIsU0FBQyxJQUFEO21CQUM1Qiw4QkFBK0IsQ0FBQSxJQUFBLENBQS9CLEdBQXVDLFNBQUMsS0FBRDtjQUNyQyxLQUFLLENBQUMsZUFBTixDQUFBO3FCQUNBLFFBQVMsQ0FBQSxJQUFBLENBQVQsQ0FBQTtZQUZxQztVQURYLENBQTlCO2lCQUtBLEtBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsS0FBQyxDQUFBLE9BQW5CLEVBQTRCLDhCQUE1QixDQUFuQjtRQVBtQjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFTckIsa0JBQUEsQ0FDRTtRQUFBLGdCQUFBLEVBQWtCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLFFBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFsQjtRQUNBLHVCQUFBLEVBQXlCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLGNBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUR6QjtRQUVBLDBCQUFBLEVBQTRCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLGdCQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FGNUI7UUFHQSx5QkFBQSxFQUEyQixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxlQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FIM0I7UUFJQSx1QkFBQSxFQUF5QixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxjQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FKekI7UUFLQSxxQkFBQSxFQUF1QixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxZQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FMdkI7UUFNQSxlQUFBLEVBQWlCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLFFBQUQsQ0FBVSxTQUFWO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBTmpCO1FBT0EsaUJBQUEsRUFBbUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsUUFBRCxDQUFVLFdBQVY7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FQbkI7UUFRQSxpQkFBQSxFQUFtQixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxRQUFELENBQVUsV0FBVjtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQVJuQjtRQVNBLGtCQUFBLEVBQW9CLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLFFBQUQsQ0FBVSxZQUFWO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBVHBCO09BREY7TUFZQSxJQUFDLENBQUEsT0FBTyxDQUFDLGdCQUFULENBQTBCLFlBQTFCLEVBQXdDLElBQUMsQ0FBQSxZQUFZLENBQUMsSUFBZCxDQUFtQixJQUFuQixDQUF4QztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsWUFBMUIsRUFBd0MsSUFBQyxDQUFBLFlBQVksQ0FBQyxJQUFkLENBQW1CLElBQW5CLENBQXhDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixZQUExQixFQUF3QyxJQUFDLENBQUEsWUFBWSxDQUFDLElBQWQsQ0FBbUIsSUFBbkIsQ0FBeEM7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLGdCQUFULENBQTBCLFdBQTFCLEVBQXVDLElBQUMsQ0FBQSxXQUFXLENBQUMsSUFBYixDQUFrQixJQUFsQixDQUF2QztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsU0FBMUIsRUFBcUMsSUFBQyxDQUFBLFNBQVMsQ0FBQyxJQUFYLENBQWdCLElBQWhCLENBQXJDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixXQUExQixFQUF1QyxJQUFDLENBQUEsV0FBVyxDQUFDLElBQWIsQ0FBa0IsSUFBbEIsQ0FBdkM7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLGdCQUFULENBQTBCLFVBQTFCLEVBQXNDLElBQUMsQ0FBQSxVQUFVLENBQUMsSUFBWixDQUFpQixJQUFqQixDQUF0QztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsTUFBMUIsRUFBa0MsSUFBQyxDQUFBLE1BQU0sQ0FBQyxJQUFSLENBQWEsSUFBYixDQUFsQztNQUdBLElBQUMsQ0FBQSxXQUFXLENBQUMsZ0JBQWIsQ0FBOEIsV0FBOUIsRUFBMkMsSUFBQyxDQUFBLGVBQWUsQ0FBQyxJQUFqQixDQUFzQixJQUF0QixDQUEzQztNQUNBLElBQUMsQ0FBQSxXQUFXLENBQUMsZ0JBQWIsQ0FBOEIsV0FBOUIsRUFBMkMsSUFBQyxDQUFBLGVBQWUsQ0FBQyxJQUFqQixDQUFzQixJQUF0QixDQUEzQztNQUVBLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUMsQ0FBQSxJQUFJLENBQUMsWUFBTixDQUFBO0FBQ2pCO0FBQUEsV0FBQSxxQ0FBQTs7UUFBQSxJQUFDLENBQUEsYUFBRCxDQUFlLElBQWY7QUFBQTtNQUVBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsSUFBSSxDQUFDLFlBQU4sQ0FBbUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUNwQyxLQUFDLENBQUEsT0FBRCxDQUFBO1FBRG9DO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFuQixDQUFuQjtNQUdBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsSUFBSSxDQUFDLFlBQU4sQ0FBbUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEdBQUQ7QUFDcEMsY0FBQTtVQURzQyxpQkFBTTtpQkFDNUMsS0FBQyxDQUFBLGFBQUQsQ0FBZSxJQUFmLEVBQXFCLEtBQXJCO1FBRG9DO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFuQixDQUFuQjtNQUdBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsSUFBSSxDQUFDLGFBQU4sQ0FBb0IsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEdBQUQ7QUFDckMsY0FBQTtVQUR1QyxpQkFBTTtpQkFDN0MsS0FBQyxDQUFBLGtCQUFELENBQW9CLElBQXBCLEVBQTBCLFFBQTFCO1FBRHFDO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFwQixDQUFuQjtNQUdBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsSUFBSSxDQUFDLGVBQU4sQ0FBc0IsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEdBQUQ7QUFDdkMsY0FBQTtVQUR5QyxPQUFEO2lCQUN4QyxLQUFDLENBQUEsZ0JBQUQsQ0FBa0IsSUFBbEI7UUFEdUM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXRCLENBQW5CO01BR0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUMsQ0FBQSxJQUFJLENBQUMscUJBQU4sQ0FBNEIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLElBQUQ7aUJBQzdDLEtBQUMsQ0FBQSxlQUFELENBQUE7UUFENkM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTVCLENBQW5CO01BR0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBWixDQUFvQixtQkFBcEIsRUFBeUMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEtBQUQ7aUJBQVcsS0FBQyxDQUFBLGtCQUFELENBQW9CLEtBQXBCO1FBQVg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpDLENBQW5CO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBWixDQUFvQiw0QkFBcEIsRUFBa0QsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEtBQUQ7aUJBQVcsS0FBQyxDQUFBLDJCQUFELENBQTZCLEtBQTdCO1FBQVg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWxELENBQW5CO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBWixDQUFvQix1QkFBcEIsRUFBNkMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxzQkFBRCxDQUFBO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTdDLENBQW5CO01BRUEsSUFBQyxDQUFBLGVBQUQsQ0FBQTtNQUVBLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsV0FBMUIsRUFBdUMsSUFBQyxDQUFBLFdBQVcsQ0FBQyxJQUFiLENBQWtCLElBQWxCLENBQXZDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxJQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxJQUFkLENBQW5DO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixVQUExQixFQUFzQyxJQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxJQUFkLENBQXRDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixVQUExQixFQUFzQyxJQUFDLENBQUEsYUFBYSxDQUFDLElBQWYsQ0FBb0IsSUFBcEIsQ0FBdEM7TUFFQSxJQUFDLENBQUEsbUJBQUQsR0FBdUIsSUFBQyxDQUFBLG1CQUFtQixDQUFDLElBQXJCLENBQTBCLElBQTFCO01BQ3ZCLFdBQVcsQ0FBQyxFQUFaLENBQWUsYUFBZixFQUE4QixJQUFDLENBQUEsbUJBQS9CO0lBM0ZXOzt5QkE2RmIsT0FBQSxHQUFTLFNBQUE7TUFDUCxXQUFXLENBQUMsY0FBWixDQUEyQixhQUEzQixFQUEwQyxJQUFDLENBQUEsbUJBQTNDO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxPQUFmLENBQUE7YUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLE1BQVQsQ0FBQTtJQUhPOzt5QkFLVCxzQkFBQSxHQUF3QixTQUFBO0FBQ3RCLFVBQUE7QUFBQTtBQUFBLFdBQUEscUNBQUE7OztVQUFBLEdBQUcsQ0FBQzs7QUFBSjtJQURzQjs7eUJBSXhCLGFBQUEsR0FBZSxTQUFDLElBQUQsRUFBTyxLQUFQO0FBQ2IsVUFBQTtNQUFBLE9BQUEsR0FBVSxJQUFJLE9BQUosQ0FBWTtRQUNwQixNQUFBLElBRG9CO1FBRW5CLE1BQUQsSUFBQyxDQUFBLElBRm1CO1FBR25CLE1BQUQsSUFBQyxDQUFBLElBSG1CO1FBSXBCLGlCQUFBLEVBQW1CLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7WUFDakIsS0FBQyxDQUFBLFFBQUQsQ0FBVSxPQUFWO1VBRGlCO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUpDO1FBT25CLFVBQUQsSUFBQyxDQUFBLFFBUG1CO09BQVo7TUFTVixJQUFtQyxJQUFDLENBQUEsd0JBQXBDO1FBQUEsT0FBTyxDQUFDLHFCQUFSLENBQUEsRUFBQTs7TUFDQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsT0FBTyxDQUFDLE9BQTNCLEVBQW9DLE9BQXBDO01BQ0EsSUFBQyxDQUFBLGdCQUFELENBQWtCLE9BQWxCLEVBQTJCLEtBQTNCO01BQ0EsSUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0Isc0JBQWhCLENBQUg7UUFDRSxJQUFBLENBQXlELElBQUMsQ0FBQSx3QkFBMUQ7aUJBQUEsSUFBQyxDQUFBLElBQUksQ0FBQyxRQUFOLENBQWUsSUFBZixFQUFxQixJQUFDLENBQUEsSUFBSSxDQUFDLFFBQU4sQ0FBQSxDQUFnQixDQUFDLE1BQWpCLEdBQTBCLENBQS9DLEVBQUE7U0FERjs7SUFiYTs7eUJBZ0JmLGtCQUFBLEdBQW9CLFNBQUMsSUFBRCxFQUFPLEtBQVA7QUFDbEIsVUFBQTtNQUFBLFFBQUEsR0FBVyxJQUFDLENBQUEsSUFBSSxDQUFDLFNBQU4sQ0FBZ0IsU0FBQyxDQUFEO2VBQU8sQ0FBQyxDQUFDLElBQUYsS0FBVTtNQUFqQixDQUFoQjtNQUNYLElBQUcsUUFBQSxLQUFjLENBQUMsQ0FBbEI7UUFDRSxHQUFBLEdBQU0sSUFBQyxDQUFBLElBQUssQ0FBQSxRQUFBO1FBQ1osR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFaLENBQUE7UUFDQSxJQUFDLENBQUEsSUFBSSxDQUFDLE1BQU4sQ0FBYSxRQUFiLEVBQXVCLENBQXZCO2VBQ0EsSUFBQyxDQUFBLGdCQUFELENBQWtCLEdBQWxCLEVBQXVCLEtBQXZCLEVBSkY7O0lBRmtCOzt5QkFRcEIsZ0JBQUEsR0FBa0IsU0FBQyxHQUFELEVBQU0sS0FBTjtBQUNoQixVQUFBO01BQUEsSUFBK0IsYUFBL0I7UUFBQSxZQUFBLEdBQWUsSUFBQyxDQUFBLElBQUssQ0FBQSxLQUFBLEVBQXJCOztNQUNBLElBQUcsWUFBSDtRQUNFLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFBVCxDQUFzQixHQUFHLENBQUMsT0FBMUIsRUFBbUMsWUFBWSxDQUFDLE9BQWhEO1FBQ0EsSUFBQyxDQUFBLElBQUksQ0FBQyxNQUFOLENBQWEsS0FBYixFQUFvQixDQUFwQixFQUF1QixHQUF2QixFQUZGO09BQUEsTUFBQTtRQUlFLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixHQUFHLENBQUMsT0FBekI7UUFDQSxJQUFDLENBQUEsSUFBSSxDQUFDLElBQU4sQ0FBVyxHQUFYLEVBTEY7O01BT0EsR0FBRyxDQUFDLFdBQUosQ0FBQTthQUNBLElBQUMsQ0FBQSxzQkFBRCxDQUFBO0lBVmdCOzt5QkFZbEIsZ0JBQUEsR0FBa0IsU0FBQyxJQUFEO0FBQ2hCLFVBQUE7TUFBQSxRQUFBLEdBQVcsSUFBQyxDQUFBLElBQUksQ0FBQyxTQUFOLENBQWdCLFNBQUMsQ0FBRDtlQUFPLENBQUMsQ0FBQyxJQUFGLEtBQVU7TUFBakIsQ0FBaEI7TUFDWCxJQUFHLFFBQUEsS0FBYyxDQUFDLENBQWxCO1FBQ0UsR0FBQSxHQUFNLElBQUMsQ0FBQSxJQUFLLENBQUEsUUFBQTtRQUNaLElBQUMsQ0FBQSxJQUFJLENBQUMsTUFBTixDQUFhLFFBQWIsRUFBdUIsQ0FBdkI7UUFDQSxJQUFDLENBQUEsYUFBYSxFQUFDLE1BQUQsRUFBZCxDQUFzQixHQUF0QjtRQUNBLEdBQUcsQ0FBQyxPQUFKLENBQUEsRUFKRjs7QUFLQTtBQUFBLFdBQUEscUNBQUE7O1FBQUEsR0FBRyxDQUFDLFdBQUosQ0FBQTtBQUFBO2FBQ0EsSUFBQyxDQUFBLHNCQUFELENBQUE7SUFSZ0I7O3lCQVVsQixzQkFBQSxHQUF3QixTQUFBO01BRXRCLElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLHVCQUFoQixDQUFBLElBQTRDLElBQUMsQ0FBQSxJQUFJLENBQUMsUUFBTixDQUFBLENBQWdCLENBQUMsTUFBakIsR0FBMEIsQ0FBekU7ZUFDRSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFuQixDQUEwQixRQUExQixFQURGO09BQUEsTUFBQTtlQUdFLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLFFBQXZCLEVBSEY7O0lBRnNCOzt5QkFPeEIsT0FBQSxHQUFTLFNBQUE7YUFDUCxJQUFDLENBQUEsSUFBSSxDQUFDLEtBQU4sQ0FBQTtJQURPOzt5QkFHVCxVQUFBLEdBQVksU0FBQyxLQUFEO2FBQ1YsSUFBQyxDQUFBLElBQUssQ0FBQSxLQUFBO0lBREk7O3lCQUdaLFVBQUEsR0FBWSxTQUFDLElBQUQ7YUFDVixJQUFDLENBQUEsSUFBSSxDQUFDLElBQU4sQ0FBVyxTQUFDLENBQUQ7ZUFBTyxDQUFDLENBQUMsSUFBRixLQUFVO01BQWpCLENBQVg7SUFEVTs7eUJBR1osWUFBQSxHQUFjLFNBQUMsT0FBRDtBQUNaLFVBQUE7TUFBQSxJQUFHLGlCQUFBLElBQWEsT0FBQSxLQUFhLElBQUMsQ0FBQSxTQUE5Qjs7YUFDWSxDQUFFLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBOUIsQ0FBcUMsUUFBckM7O1FBQ0EsSUFBQyxDQUFBLFNBQUQsR0FBYTtRQUNiLElBQUMsQ0FBQSxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUE3QixDQUFpQyxRQUFqQztlQUNBLElBQUMsQ0FBQSxTQUFTLENBQUMsT0FBTyxDQUFDLGNBQW5CLENBQWtDLEtBQWxDLEVBSkY7O0lBRFk7O3lCQU9kLFlBQUEsR0FBYyxTQUFBO2FBQ1osSUFBQyxDQUFBLFVBQUQsQ0FBWSxJQUFDLENBQUEsSUFBSSxDQUFDLGFBQU4sQ0FBQSxDQUFaO0lBRFk7O3lCQUdkLGVBQUEsR0FBaUIsU0FBQTthQUNmLElBQUMsQ0FBQSxZQUFELENBQWMsSUFBQyxDQUFBLFVBQUQsQ0FBWSxJQUFDLENBQUEsSUFBSSxDQUFDLGFBQU4sQ0FBQSxDQUFaLENBQWQ7SUFEZTs7eUJBR2pCLFFBQUEsR0FBVSxTQUFDLEdBQUQ7O1FBQ1IsTUFBTyxJQUFDLENBQUE7O01BQ1IsSUFBK0IsV0FBL0I7ZUFBQSxJQUFDLENBQUEsSUFBSSxDQUFDLFdBQU4sQ0FBa0IsR0FBRyxDQUFDLElBQXRCLEVBQUE7O0lBRlE7O3lCQUlWLGVBQUEsR0FBaUIsU0FBQyxHQUFEO0FBQ2YsVUFBQTs7UUFBQSxNQUFPLElBQUMsQ0FBQTs7TUFDUixJQUFBLGlCQUFPLEdBQUcsQ0FBRTtNQUNaLElBQWMsWUFBZDtBQUFBLGVBQUE7O01BQ0EsSUFBRyxPQUFPLElBQUksQ0FBQyxNQUFaLEtBQXNCLFVBQXpCO1FBQ0UsT0FBQSxHQUFVLElBQUksQ0FBQyxNQUFMLENBQUEsRUFEWjtPQUFBLE1BRUssSUFBRyxPQUFPLElBQUksQ0FBQyxPQUFaLEtBQXVCLFVBQTFCO1FBQ0gsT0FBQSxHQUFVLElBQUksQ0FBQyxPQUFMLENBQUEsRUFEUDtPQUFBLE1BRUEsSUFBRyxPQUFPLElBQUksQ0FBQyxNQUFaLEtBQXNCLFVBQXpCO1FBQ0gsT0FBQSxHQUFVLElBQUksQ0FBQyxNQUFMLENBQUEsRUFEUDs7TUFFTCxJQUFjLGVBQWQ7QUFBQSxlQUFBOztNQUNBLElBQUMsQ0FBQSxRQUFELENBQVUsR0FBVjtBQUNBO0FBQUEsV0FBQSxxQ0FBQTs7UUFBQSxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFsQixHQUE2QjtBQUE3QjtNQUNBLFdBQUEsR0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBYixDQUFBLENBQUQsRUFBMEIsT0FBMUIsQ0FBa0MsQ0FBQyxNQUFuQyxDQUEwQyxDQUFDLFNBQUMsQ0FBRCxFQUFJLENBQUo7ZUFBVSxDQUFDLENBQUMsTUFBRixDQUFTLENBQVQ7TUFBVixDQUFELENBQTFDLEVBQW1FLEVBQW5FO2FBQ2QsSUFBSSxDQUFDLElBQUwsQ0FBVTtRQUFDLFdBQUEsRUFBYSxXQUFkO1FBQTJCLFNBQUEsRUFBVyxJQUF0QztRQUE0QyxPQUFBLEVBQVMsSUFBSSxDQUFDLE9BQTFEO1FBQW1FLFFBQUEsRUFBVSxJQUFJLENBQUMsUUFBbEY7T0FBVjtJQWRlOzt5QkFnQmpCLFFBQUEsR0FBVSxTQUFDLEVBQUQ7QUFDUixVQUFBO01BQUEsSUFBRyxJQUFBLDZDQUF1QixDQUFFLGFBQTVCO1FBQ0UsSUFBRyxVQUFBLHFDQUFhLElBQUksQ0FBQyxlQUFyQjtpQkFDRSxJQUFDLENBQUEsSUFBSyxDQUFBLEVBQUEsQ0FBTixDQUFVO1lBQUEsS0FBQSxFQUFPLENBQUMsVUFBRCxDQUFQO1dBQVYsRUFERjtTQURGOztJQURROzt5QkFLVixjQUFBLEdBQWdCLFNBQUMsTUFBRDtBQUNkLFVBQUE7TUFBQSxJQUFBLEdBQU8sSUFBQyxDQUFBLE9BQUQsQ0FBQTs7UUFDUCxTQUFVLElBQUMsQ0FBQTs7TUFDWCxJQUFjLGNBQWQ7QUFBQSxlQUFBOztBQUNBO1dBQUEsc0NBQUE7O1lBQW1DLEdBQUEsS0FBUzt1QkFBNUMsSUFBQyxDQUFBLFFBQUQsQ0FBVSxHQUFWOztBQUFBOztJQUpjOzt5QkFNaEIsZ0JBQUEsR0FBa0IsU0FBQyxNQUFEO0FBQ2hCLFVBQUE7TUFBQSxJQUFBLEdBQU8sSUFBQyxDQUFBLE9BQUQsQ0FBQTs7UUFDUCxTQUFVLElBQUMsQ0FBQTs7TUFDWCxLQUFBLEdBQVEsSUFBSSxDQUFDLE9BQUwsQ0FBYSxNQUFiO01BQ1IsSUFBVSxLQUFBLEtBQVMsQ0FBQyxDQUFwQjtBQUFBLGVBQUE7O0FBQ0E7V0FBQSw4Q0FBQTs7WUFBc0MsQ0FBQSxHQUFJO3VCQUExQyxJQUFDLENBQUEsUUFBRCxDQUFVLEdBQVY7O0FBQUE7O0lBTGdCOzt5QkFPbEIsZUFBQSxHQUFpQixTQUFDLE1BQUQ7QUFDZixVQUFBO01BQUEsSUFBQSxHQUFPLElBQUMsQ0FBQSxPQUFELENBQUE7O1FBQ1AsU0FBVSxJQUFDLENBQUE7O01BQ1gsS0FBQSxHQUFRLElBQUksQ0FBQyxPQUFMLENBQWEsTUFBYjtNQUNSLElBQVUsS0FBQSxLQUFTLENBQUMsQ0FBcEI7QUFBQSxlQUFBOztBQUNBO1dBQUEsOENBQUE7O1lBQXNDLENBQUEsR0FBSTt1QkFBMUMsSUFBQyxDQUFBLFFBQUQsQ0FBVSxHQUFWOztBQUFBOztJQUxlOzt5QkFPakIsY0FBQSxHQUFnQixTQUFBO0FBQ2QsVUFBQTtBQUFBO0FBQUE7V0FBQSxxQ0FBQTs7UUFDRSxJQUFBLDJEQUE4QixDQUFDLHNCQUEvQjt1QkFBQSxJQUFDLENBQUEsUUFBRCxDQUFVLEdBQVYsR0FBQTtTQUFBLE1BQUE7K0JBQUE7O0FBREY7O0lBRGM7O3lCQUloQixZQUFBLEdBQWMsU0FBQTtBQUNaLFVBQUE7QUFBQTtBQUFBO1dBQUEscUNBQUE7O3FCQUFBLElBQUMsQ0FBQSxRQUFELENBQVUsR0FBVjtBQUFBOztJQURZOzt5QkFHZCxXQUFBLEdBQWEsU0FBQTtxQ0FDWCxJQUFDLENBQUEsV0FBRCxJQUFDLENBQUEsV0FBWSxJQUFJLENBQUMsZ0JBQUwsQ0FBQSxDQUF1QixDQUFDO0lBRDFCOzt5QkFHYixXQUFBLEdBQWEsU0FBQyxLQUFEO0FBQ1gsVUFBQTtNQUFBLElBQUMsQ0FBQSxVQUFELEdBQWMsSUFBQyxDQUFBLGFBQUQsQ0FBZSxLQUFLLENBQUMsTUFBckI7TUFDZCxJQUFBLENBQWMsSUFBQyxDQUFBLFVBQWY7QUFBQSxlQUFBOztNQUNBLElBQUMsQ0FBQSxtQkFBRCxHQUF1QjtNQUV2QixLQUFLLENBQUMsWUFBWSxDQUFDLE9BQW5CLENBQTJCLGdCQUEzQixFQUE2QyxNQUE3QztNQUVBLElBQUMsQ0FBQSxVQUFVLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUE5QixDQUFrQyxhQUFsQztNQUNBLElBQUMsQ0FBQSxVQUFVLENBQUMsY0FBWixDQUFBO01BRUEsUUFBQSxHQUFXLElBQUMsQ0FBQSxJQUFJLENBQUMsT0FBTixDQUFjLElBQUMsQ0FBQSxVQUFmO01BQ1gsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFuQixDQUEyQixnQkFBM0IsRUFBNkMsUUFBN0M7TUFFQSxTQUFBLEdBQVksSUFBQyxDQUFBLGFBQWEsQ0FBQyxRQUFmLENBQUEsQ0FBeUIsQ0FBQyxPQUExQixDQUFrQyxJQUFDLENBQUEsSUFBbkM7TUFDWixLQUFLLENBQUMsWUFBWSxDQUFDLE9BQW5CLENBQTJCLGlCQUEzQixFQUE4QyxTQUE5QztNQUNBLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBbkIsQ0FBMkIsY0FBM0IsRUFBMkMsSUFBQyxDQUFBLElBQUksQ0FBQyxFQUFqRDtNQUNBLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBbkIsQ0FBMkIsZ0JBQTNCLEVBQTZDLElBQUMsQ0FBQSxXQUFELENBQUEsQ0FBN0M7TUFFQSxJQUFBLEdBQU8sSUFBQyxDQUFBLElBQUksQ0FBQyxRQUFOLENBQUEsQ0FBaUIsQ0FBQSxJQUFDLENBQUEsSUFBSSxDQUFDLE9BQU4sQ0FBYyxJQUFDLENBQUEsVUFBZixDQUFBO01BQ3hCLElBQWMsWUFBZDtBQUFBLGVBQUE7O01BRUEsSUFBRyxPQUFPLElBQUksQ0FBQyxNQUFaLEtBQXNCLFVBQXpCO1FBQ0UsT0FBQSx5Q0FBMEIsR0FENUI7T0FBQSxNQUVLLElBQUcsT0FBTyxJQUFJLENBQUMsT0FBWixLQUF1QixVQUExQjtRQUNILE9BQUEsNENBQTJCLEdBRHhCO09BQUEsTUFFQSxJQUFHLE9BQU8sSUFBSSxDQUFDLE1BQVosS0FBc0IsVUFBekI7UUFDSCxPQUFBLDJDQUEwQixHQUR2Qjs7TUFHTCxJQUFHLE9BQU8sSUFBSSxDQUFDLG1CQUFaLEtBQW1DLFVBQXRDO0FBQ0U7QUFBQSxhQUFBLHNDQUFBOztVQUNFLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBbkIsQ0FBMkIsbUJBQUEsR0FBb0IsUUFBL0MsRUFBMkQsTUFBM0Q7QUFERixTQURGO09BQUEsTUFBQTtRQUlFLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBbkIsQ0FBMkIscUJBQTNCLEVBQWtELE1BQWxELEVBSkY7O01BTUEsSUFBRyxlQUFIO1FBQ0UsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFuQixDQUEyQixZQUEzQixFQUF5QyxPQUF6QztRQUVBLElBQUcsT0FBTyxDQUFDLFFBQVIsS0FBb0IsUUFBdkI7VUFDRSxJQUFBLENBQXFDLElBQUMsQ0FBQSxjQUFELENBQWdCLE9BQWhCLENBQXJDO1lBQUEsT0FBQSxHQUFVLFNBQUEsR0FBVSxRQUFwQjs7VUFDQSxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQW5CLENBQTJCLGVBQTNCLEVBQTRDLE9BQTVDLEVBRkY7O1FBSUEsNkNBQUcsSUFBSSxDQUFDLHNCQUFMLElBQXVCLHNCQUExQjtVQUNFLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBbkIsQ0FBMkIscUJBQTNCLEVBQWtELE1BQWxEO2lCQUNBLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBbkIsQ0FBMkIsZUFBM0IsRUFBNEMsSUFBSSxDQUFDLE9BQUwsQ0FBQSxDQUE1QyxFQUZGO1NBUEY7O0lBbENXOzt5QkE2Q2IsY0FBQSxHQUFnQixTQUFDLEdBQUQ7QUFDZCxVQUFBO0FBQUE7ZUFDRSwyQ0FERjtPQUFBLGNBQUE7UUFFTTtlQUNKLE1BSEY7O0lBRGM7O3lCQU1oQixXQUFBLEdBQWEsU0FBQyxLQUFEO0FBRVgsVUFBQTtNQUFBLElBQUEsQ0FBTyxLQUFLLENBQUMsYUFBYSxDQUFDLFFBQXBCLENBQTZCLEtBQUssQ0FBQyxhQUFuQyxDQUFQO1FBQ0UsSUFBQyxDQUFBLGlCQUFELENBQUE7UUFDQSxJQUFDLENBQUEsbUJBQUQsR0FBdUI7QUFDdkI7QUFBQTthQUFBLHFDQUFBOzt1QkFBQSxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFsQixHQUE2QjtBQUE3Qjt1QkFIRjs7SUFGVzs7eUJBT2IsU0FBQSxHQUFXLFNBQUMsS0FBRDtNQUNULElBQUEsQ0FBYyxJQUFDLENBQUEsYUFBRCxDQUFlLEtBQUssQ0FBQyxNQUFyQixDQUFkO0FBQUEsZUFBQTs7YUFFQSxJQUFDLENBQUEsZUFBRCxDQUFBO0lBSFM7O3lCQUtYLFVBQUEsR0FBWSxTQUFDLEtBQUQ7QUFDVixVQUFBO01BQUEsSUFBQSxDQUFjLElBQUMsQ0FBQSxjQUFELENBQWdCLEtBQWhCLENBQWQ7QUFBQSxlQUFBOztNQUNBLElBQUEsQ0FBYyxJQUFDLENBQUEsYUFBRCxDQUFlLEtBQWYsRUFBc0IsSUFBQyxDQUFBLFFBQXZCLENBQWQ7QUFBQSxlQUFBOztNQUVBLEtBQUssQ0FBQyxjQUFOLENBQUE7TUFDQSxLQUFLLENBQUMsZUFBTixDQUFBO01BRUEsa0JBQUEsR0FBcUIsSUFBQyxDQUFBLGtCQUFELENBQW9CLEtBQXBCO01BQ3JCLElBQWMsMEJBQWQ7QUFBQSxlQUFBOztNQUNBLElBQVUsSUFBQyxDQUFBLG1CQUFELEtBQXdCLGtCQUFsQztBQUFBLGVBQUE7O01BQ0EsSUFBQyxDQUFBLG1CQUFELEdBQXVCO01BRXZCLElBQUMsQ0FBQSx1QkFBRCxDQUFBO01BRUEsSUFBQSxHQUFPLElBQUMsQ0FBQSxPQUFELENBQUE7TUFDUCxXQUFBLEdBQWMsSUFBQyxDQUFBLGNBQUQsQ0FBQTtNQUNkLElBQWMsbUJBQWQ7QUFBQSxlQUFBOztNQUVBLElBQUcsa0JBQUEsR0FBcUIsSUFBSSxDQUFDLE1BQTdCO1FBQ0UsR0FBQSxHQUFNLElBQUssQ0FBQSxrQkFBQTtRQUNYLEdBQUcsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQXRCLENBQTBCLGdCQUExQjtlQUNBLEdBQUcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFlBQTFCLENBQXVDLFdBQXZDLEVBQW9ELEdBQUcsQ0FBQyxPQUF4RCxFQUhGO09BQUEsTUFBQTtRQUtFLElBQUcsR0FBQSxHQUFNLElBQUssQ0FBQSxrQkFBQSxHQUFxQixDQUFyQixDQUFkO1VBQ0UsR0FBRyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBdEIsQ0FBMEIsc0JBQTFCO1VBQ0EsSUFBRyxPQUFBLEdBQVUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUF6QjttQkFDRSxHQUFHLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxZQUExQixDQUF1QyxXQUF2QyxFQUFvRCxPQUFwRCxFQURGO1dBQUEsTUFBQTttQkFHRSxHQUFHLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxXQUExQixDQUFzQyxXQUF0QyxFQUhGO1dBRkY7U0FMRjs7SUFsQlU7O3lCQThCWixtQkFBQSxHQUFxQixTQUFDLEtBQUQsRUFBUSxVQUFSLEVBQW9CLGFBQXBCO0FBQ25CLFVBQUE7TUFBQSxJQUFHLElBQUMsQ0FBQSxJQUFJLENBQUMsRUFBTixLQUFZLFVBQWY7UUFDRSxJQUFHLFlBQUEsR0FBZSxJQUFDLENBQUEsSUFBSSxDQUFDLFFBQU4sQ0FBQSxDQUFpQixDQUFBLGFBQUEsQ0FBbkM7VUFDRSxJQUFDLENBQUEsSUFBSSxDQUFDLFdBQU4sQ0FBa0IsWUFBbEIsRUFERjtTQURGOzthQUlBLElBQUMsQ0FBQSxlQUFELENBQUE7SUFMbUI7O3lCQU9yQixlQUFBLEdBQWlCLFNBQUE7QUFDZixVQUFBOztXQUFXLENBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUEvQixDQUFzQyxhQUF0Qzs7O1lBQ1csQ0FBRSxhQUFiLENBQUE7O01BQ0EsSUFBQyxDQUFBLFVBQUQsR0FBYztNQUNkLElBQUMsQ0FBQSx1QkFBRCxDQUFBO2FBQ0EsSUFBQyxDQUFBLGlCQUFELENBQUE7SUFMZTs7eUJBT2pCLE1BQUEsR0FBUSxTQUFDLEtBQUQ7QUFDTixVQUFBO01BQUEsSUFBQSxDQUFjLElBQUMsQ0FBQSxjQUFELENBQWdCLEtBQWhCLENBQWQ7QUFBQSxlQUFBOztNQUVBLEtBQUssQ0FBQyxjQUFOLENBQUE7TUFFQSxZQUFBLEdBQWdCLFFBQUEsQ0FBUyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQW5CLENBQTJCLGdCQUEzQixDQUFUO01BQ2hCLFVBQUEsR0FBZ0IsUUFBQSxDQUFTLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBbkIsQ0FBMkIsY0FBM0IsQ0FBVDtNQUNoQixTQUFBLEdBQWdCLFFBQUEsQ0FBUyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQW5CLENBQTJCLGdCQUEzQixDQUFUO01BQ2hCLGFBQUEsR0FBZ0IsUUFBQSxDQUFTLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBbkIsQ0FBMkIsaUJBQTNCLENBQVQ7TUFFaEIsaUJBQUEsR0FBb0IsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFuQixDQUEyQixxQkFBM0IsQ0FBQSxLQUFxRDtNQUN6RSxZQUFBLEdBQWUsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFuQixDQUEyQixlQUEzQjtNQUVmLE9BQUEsR0FBVSxJQUFDLENBQUEsa0JBQUQsQ0FBb0IsS0FBcEI7TUFDVixNQUFBLEdBQVMsSUFBQyxDQUFBO01BRVYsSUFBQyxDQUFBLGVBQUQsQ0FBQTtNQUVBLElBQUEsQ0FBYyxJQUFDLENBQUEsYUFBRCxDQUFlLEtBQWYsRUFBc0IsSUFBQyxDQUFBLFFBQXZCLENBQWQ7QUFBQSxlQUFBOztNQUVBLElBQUcsWUFBQSxLQUFnQixJQUFDLENBQUEsV0FBRCxDQUFBLENBQW5CO1FBQ0UsUUFBQSxHQUFXLElBQUMsQ0FBQSxhQUFhLENBQUMsUUFBZixDQUFBLENBQTBCLENBQUEsYUFBQTtRQUNyQyx3QkFBRyxRQUFRLENBQUUsWUFBVixLQUFrQixVQUFyQjtVQUdFLFFBQUEsR0FBVyxLQUFLLENBQUMsSUFBTixDQUFXLFFBQVEsQ0FBQyxnQkFBVCxDQUEwQixXQUExQixDQUFYLENBQ1QsQ0FBQyxHQURRLENBQ0osU0FBQyxNQUFEO21CQUFZLE1BQU0sQ0FBQztVQUFuQixDQURJLENBRVQsQ0FBQyxJQUZRLENBRUgsU0FBQyxJQUFEO21CQUFVLElBQUksQ0FBQyxFQUFMLEtBQVc7VUFBckIsQ0FGRyxFQUhiOztRQU1BLElBQUEsR0FBTyxRQUFRLENBQUMsUUFBVCxDQUFBLENBQW9CLENBQUEsU0FBQTtRQUMzQixJQUFxRSxZQUFyRTtpQkFBQSxJQUFDLENBQUEsb0JBQUQsQ0FBc0IsUUFBdEIsRUFBZ0MsU0FBaEMsRUFBMkMsTUFBM0MsRUFBbUQsT0FBbkQsRUFBNEQsSUFBNUQsRUFBQTtTQVRGO09BQUEsTUFBQTtRQVdFLFVBQUEsR0FBYSxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQW5CLENBQTJCLFlBQTNCO1FBQ2IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFmLENBQW9CLFVBQXBCLENBQStCLENBQUMsSUFBaEMsQ0FBcUMsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQyxJQUFEO0FBR25DLGdCQUFBO1lBQUEsVUFBQSxHQUFhLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBZixDQUFBO1lBQ2IsZUFBQSxHQUFrQixVQUFVLENBQUMsUUFBWCxDQUFBLENBQXFCLENBQUMsT0FBdEIsQ0FBOEIsSUFBOUI7WUFDbEIsS0FBQyxDQUFBLG9CQUFELENBQXNCLFVBQXRCLEVBQWtDLGVBQWxDLEVBQW1ELE1BQW5ELEVBQTJELE9BQTNELEVBQW9FLElBQXBFO1lBQ0EsSUFBK0IsaUJBQS9COztnQkFBQSxJQUFJLENBQUMsUUFBUztlQUFkOztZQUVBLElBQUcsQ0FBSSxLQUFBLENBQU0sWUFBTixDQUFQO2NBRUUsYUFBQSxHQUFnQixLQUFDLENBQUEsa0JBQUQsQ0FBb0IsWUFBcEI7NkNBQ2hCLGFBQWEsQ0FBRSxXQUFXLENBQUMsSUFBM0IsQ0FBZ0MsYUFBaEMsRUFBK0MsVUFBL0MsRUFBMkQsU0FBM0QsV0FIRjs7VUFSbUM7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXJDO2VBYUEsSUFBSSxDQUFDLEtBQUwsQ0FBQSxFQXpCRjs7SUFwQk07O3lCQWdEUixlQUFBLEdBQWlCLFNBQUMsS0FBRDtNQUNmLElBQUEsQ0FBYyxJQUFDLENBQUEsY0FBRCxDQUFnQixLQUFoQixDQUFkO0FBQUEsZUFBQTs7TUFDQSxJQUFBLENBQWMsSUFBQyxDQUFBLGFBQUQsQ0FBZSxLQUFmLEVBQXNCLElBQUMsQ0FBQSxRQUF2QixDQUFkO0FBQUEsZUFBQTs7TUFDQSxJQUFVLElBQUMsQ0FBQSxJQUFJLENBQUMsUUFBTixDQUFBLENBQWdCLENBQUMsTUFBakIsR0FBMEIsQ0FBMUIsSUFBK0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLHVCQUFoQixDQUF6QztBQUFBLGVBQUE7O01BQ0EsSUFBRyxJQUFDLENBQUEsV0FBVyxDQUFDLFFBQWIsQ0FBc0IsS0FBSyxDQUFDLGFBQTVCLENBQUg7ZUFDRSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFuQixDQUEwQixRQUExQixFQURGOztJQUplOzt5QkFRakIsZUFBQSxHQUFpQixTQUFDLEtBQUQ7TUFDZixJQUFBLENBQWMsSUFBQyxDQUFBLGNBQUQsQ0FBZ0IsS0FBaEIsQ0FBZDtBQUFBLGVBQUE7O01BQ0EsSUFBQSxDQUFjLElBQUMsQ0FBQSxhQUFELENBQWUsS0FBZixFQUFzQixJQUFDLENBQUEsUUFBdkIsQ0FBZDtBQUFBLGVBQUE7O01BQ0EsSUFBVSxJQUFDLENBQUEsSUFBSSxDQUFDLFFBQU4sQ0FBQSxDQUFnQixDQUFDLE1BQWpCLEdBQTBCLENBQTFCLElBQStCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQix1QkFBaEIsQ0FBekM7QUFBQSxlQUFBOztNQUNBLElBQUEsQ0FBTyxJQUFDLENBQUEsV0FBVyxDQUFDLFFBQWIsQ0FBc0IsS0FBSyxDQUFDLGFBQTVCLENBQVA7ZUFDRSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixRQUF2QixFQURGOztJQUplOzt5QkFPakIsWUFBQSxHQUFjLFNBQUMsS0FBRDtNQUNaLElBQVUsS0FBSyxDQUFDLFFBQU4sSUFBa0IsQ0FBSSxJQUFDLENBQUEsWUFBakM7QUFBQSxlQUFBOzs7UUFFQSxJQUFDLENBQUEsYUFBYzs7TUFDZixJQUFDLENBQUEsVUFBRCxJQUFlLEtBQUssQ0FBQztNQUVyQixJQUFHLElBQUMsQ0FBQSxVQUFELElBQWUsQ0FBQyxJQUFDLENBQUEscUJBQXBCO1FBQ0UsSUFBQyxDQUFBLFVBQUQsR0FBYztlQUNkLElBQUMsQ0FBQSxJQUFJLENBQUMsZ0JBQU4sQ0FBQSxFQUZGO09BQUEsTUFHSyxJQUFHLElBQUMsQ0FBQSxVQUFELElBQWUsSUFBQyxDQUFBLHFCQUFuQjtRQUNILElBQUMsQ0FBQSxVQUFELEdBQWM7ZUFDZCxJQUFDLENBQUEsSUFBSSxDQUFDLG9CQUFOLENBQUEsRUFGRzs7SUFUTzs7eUJBYWQsV0FBQSxHQUFhLFNBQUMsS0FBRDtBQUNYLFVBQUE7TUFBQSxJQUFBLENBQXdCLElBQUMsQ0FBQSxJQUFJLENBQUMsV0FBTixDQUFBLENBQXhCO1FBQUEsSUFBQyxDQUFBLElBQUksQ0FBQyxRQUFOLENBQUEsRUFBQTs7TUFFQSxHQUFBLEdBQU0sSUFBQyxDQUFBLGFBQUQsQ0FBZSxLQUFLLENBQUMsTUFBckI7TUFDTixJQUFBLENBQWMsR0FBZDtBQUFBLGVBQUE7O01BRUEsSUFBRyxLQUFLLENBQUMsTUFBTixLQUFnQixDQUFoQixJQUFxQixDQUFDLEtBQUssQ0FBQyxNQUFOLEtBQWdCLENBQWhCLElBQXNCLEtBQUssQ0FBQyxPQUFOLEtBQWlCLElBQXhDLENBQXhCO1FBQ0UsSUFBQyxDQUFBLGVBQUQsR0FBbUI7ZUFDbkIsS0FBSyxDQUFDLGNBQU4sQ0FBQSxFQUZGO09BQUEsTUFHSyxJQUFHLEtBQUssQ0FBQyxNQUFOLEtBQWdCLENBQW5CO2VBR0gsS0FBSyxDQUFDLGNBQU4sQ0FBQSxFQUhHOztJQVRNOzt5QkFjYixPQUFBLEdBQVMsU0FBQyxLQUFEO0FBQ1AsVUFBQTtNQUFBLEdBQUEsR0FBTSxJQUFDLENBQUEsYUFBRCxDQUFlLEtBQUssQ0FBQyxNQUFyQjtNQUNOLElBQUEsQ0FBYyxHQUFkO0FBQUEsZUFBQTs7TUFFQSxLQUFLLENBQUMsY0FBTixDQUFBO01BQ0EsSUFBRyxLQUFLLENBQUMsTUFBTixLQUFnQixDQUFoQixJQUFxQixDQUFDLEtBQUssQ0FBQyxNQUFOLEtBQWdCLENBQWhCLElBQXNCLEtBQUssQ0FBQyxPQUFOLEtBQWlCLElBQXhDLENBQXhCO0FBQUE7T0FBQSxNQUlLLElBQUcsS0FBSyxDQUFDLE1BQU4sS0FBZ0IsQ0FBaEIsSUFBc0IsQ0FBSSxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUF2QixDQUFnQyxZQUFoQyxDQUE3QjtlQUNILElBQUMsQ0FBQSxJQUFJLENBQUMsWUFBTixDQUFtQixHQUFHLENBQUMsSUFBdkIsRUFERztPQUFBLE1BRUEsSUFBRyxLQUFLLENBQUMsTUFBTixLQUFnQixDQUFuQjtlQUNILElBQUMsQ0FBQSxJQUFJLENBQUMsV0FBTixDQUFrQixHQUFHLENBQUMsSUFBdEIsRUFERzs7SUFYRTs7eUJBY1QsYUFBQSxHQUFlLFNBQUMsS0FBRDtBQUNiLFVBQUE7TUFBQSxJQUFHLEdBQUEsR0FBTSxJQUFDLENBQUEsYUFBRCxDQUFlLEtBQUssQ0FBQyxNQUFyQixDQUFUO21GQUNVLENBQUMsaUNBRFg7T0FBQSxNQUVLLElBQUcsS0FBSyxDQUFDLE1BQU4sS0FBZ0IsSUFBQyxDQUFBLE9BQXBCO1FBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFkLENBQXVCLElBQUMsQ0FBQSxPQUF4QixFQUFpQyxzQkFBakM7ZUFDQSxLQUFLLENBQUMsY0FBTixDQUFBLEVBRkc7O0lBSFE7O3lCQU9mLDJCQUFBLEdBQTZCLFNBQUMsS0FBRDthQUMzQixJQUFDLENBQUEscUJBQUQsR0FBeUI7SUFERTs7eUJBRzdCLGtCQUFBLEdBQW9CLFNBQUMsS0FBRDtNQUNsQixJQUFHLEtBQUEsS0FBUyxVQUFaO2VBQ0UsSUFBQyxDQUFBLFlBQUQsR0FBaUIsT0FBTyxDQUFDLFFBQVIsS0FBb0IsUUFEdkM7T0FBQSxNQUFBO2VBR0UsSUFBQyxDQUFBLFlBQUQsR0FBZ0IsTUFIbEI7O0lBRGtCOzt5QkFNcEIsa0JBQUEsR0FBb0IsU0FBQyxFQUFEOztRQUNsQixnQkFBaUIsT0FBQSxDQUFRLFVBQVIsQ0FBbUIsQ0FBQyxNQUFNLENBQUM7O2FBRTVDLGFBQWEsQ0FBQyxNQUFkLENBQXFCLEVBQXJCO0lBSGtCOzt5QkFLcEIsb0JBQUEsR0FBc0IsU0FBQyxRQUFELEVBQVcsU0FBWCxFQUFzQixNQUF0QixFQUE4QixPQUE5QixFQUF1QyxJQUF2QztBQUNwQjtRQUNFLElBQUcsTUFBQSxLQUFVLFFBQWI7VUFDRSxJQUFhLFNBQUEsR0FBWSxPQUF6QjtZQUFBLE9BQUEsR0FBQTs7VUFDQSxNQUFNLENBQUMsUUFBUCxDQUFnQixJQUFoQixFQUFzQixPQUF0QixFQUZGO1NBQUEsTUFBQTtVQUlFLElBQUMsQ0FBQSx3QkFBRCxHQUE0QjtVQUM1QixRQUFRLENBQUMsY0FBVCxDQUF3QixJQUF4QixFQUE4QixNQUE5QixFQUFzQyxPQUFBLEVBQXRDLEVBTEY7O1FBTUEsTUFBTSxDQUFDLFlBQVAsQ0FBb0IsSUFBcEI7ZUFDQSxNQUFNLENBQUMsUUFBUCxDQUFBLEVBUkY7T0FBQTtRQVVFLElBQUMsQ0FBQSx3QkFBRCxHQUE0QixNQVY5Qjs7SUFEb0I7O3lCQWF0Qix1QkFBQSxHQUF5QixTQUFBO0FBQ3ZCLFVBQUE7TUFBQSxnQkFBQSxHQUFtQixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQWYsQ0FBQTtBQUNuQjtBQUFBLFdBQUEscUNBQUE7O1FBQ0UsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFyQixDQUE0QixnQkFBNUI7QUFERjtBQUdBO0FBQUE7V0FBQSx3Q0FBQTs7cUJBQ0UsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFyQixDQUE0QixzQkFBNUI7QUFERjs7SUFMdUI7O3lCQVF6QixrQkFBQSxHQUFvQixTQUFDLEtBQUQ7QUFDbEIsVUFBQTtNQUFBLE1BQUEsR0FBUyxLQUFLLENBQUM7TUFFZixJQUFVLElBQUMsQ0FBQSxhQUFELENBQWUsTUFBZixDQUFWO0FBQUEsZUFBQTs7TUFFQSxJQUFBLEdBQU8sSUFBQyxDQUFBLE9BQUQsQ0FBQTtNQUNQLEdBQUEsR0FBTSxJQUFDLENBQUEsYUFBRCxDQUFlLE1BQWY7O1FBQ04sTUFBTyxJQUFLLENBQUEsSUFBSSxDQUFDLE1BQUwsR0FBYyxDQUFkOztNQUVaLElBQWdCLFdBQWhCO0FBQUEsZUFBTyxFQUFQOztNQUVBLE1BQWdCLEdBQUcsQ0FBQyxPQUFPLENBQUMscUJBQVosQ0FBQSxDQUFoQixFQUFDLGVBQUQsRUFBTztNQUNQLGFBQUEsR0FBZ0IsSUFBQSxHQUFPLEtBQUEsR0FBUTtNQUMvQixZQUFBLEdBQWUsSUFBSSxDQUFDLE9BQUwsQ0FBYSxHQUFiO01BRWYsSUFBRyxLQUFLLENBQUMsS0FBTixHQUFjLGFBQWpCO2VBQ0UsYUFERjtPQUFBLE1BQUE7ZUFHRSxZQUFBLEdBQWUsRUFIakI7O0lBZmtCOzt5QkFvQnBCLGNBQUEsR0FBZ0IsU0FBQTtNQUNkLElBQXlCLDBCQUF6QjtBQUFBLGVBQU8sSUFBQyxDQUFBLGNBQVI7O01BRUEsSUFBQyxDQUFBLGFBQUQsR0FBaUIsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsSUFBdkI7TUFDakIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxTQUFTLENBQUMsR0FBekIsQ0FBNkIsYUFBN0I7YUFDQSxJQUFDLENBQUE7SUFMYTs7eUJBT2hCLGlCQUFBLEdBQW1CLFNBQUE7QUFDakIsVUFBQTs7V0FBYyxDQUFFLE1BQWhCLENBQUE7O2FBQ0EsSUFBQyxDQUFBLGFBQUQsR0FBaUI7SUFGQTs7eUJBSW5CLGFBQUEsR0FBZSxTQUFDLE9BQUQ7YUFDYixPQUFPLENBQUMsU0FBUyxDQUFDLFFBQWxCLENBQTJCLGFBQTNCO0lBRGE7O3lCQUdmLFlBQUEsR0FBYyxTQUFBO0FBQ1osVUFBQTtBQUFBO0FBQUEsV0FBQSxxQ0FBQTs7UUFDRyxRQUFTLEdBQUcsQ0FBQyxPQUFPLENBQUMscUJBQVosQ0FBQTtRQUNWLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFFBQWxCLEdBQTZCLEtBQUssQ0FBQyxPQUFOLENBQWMsQ0FBZCxDQUFBLEdBQW1CO0FBRmxEO0lBRFk7O3lCQU1kLFlBQUEsR0FBYyxTQUFBO0FBQ1osVUFBQTtBQUFBO0FBQUEsV0FBQSxxQ0FBQTs7UUFBQSxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFsQixHQUE2QjtBQUE3QjtJQURZOzt5QkFJZCxhQUFBLEdBQWUsU0FBQyxPQUFEO0FBQ2IsVUFBQTtNQUFBLGNBQUEsR0FBaUI7QUFDakIsYUFBTSxzQkFBTjtRQUNFLElBQUcsR0FBQSxHQUFNLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixjQUFuQixDQUFUO0FBQ0UsaUJBQU8sSUFEVDtTQUFBLE1BQUE7VUFHRSxjQUFBLEdBQWlCLGNBQWMsQ0FBQyxjQUhsQzs7TUFERjtJQUZhOzt5QkFRZixjQUFBLEdBQWdCLFNBQUMsS0FBRDtBQUNkLFVBQUE7QUFBQTtBQUFBLFdBQUEscUNBQUE7O1FBQ0UsSUFBRyxJQUFJLENBQUMsSUFBTCxLQUFhLGdCQUFoQjtBQUNFLGlCQUFPLEtBRFQ7O0FBREY7QUFJQSxhQUFPO0lBTE87O3lCQU9oQixhQUFBLEdBQWUsU0FBQyxLQUFELEVBQVEsUUFBUjtBQUNiLFVBQUE7QUFBQTtBQUFBLFdBQUEscUNBQUE7O1FBQ0UsSUFBRyxJQUFJLENBQUMsSUFBTCxLQUFhLHFCQUFiLElBQXNDLElBQUksQ0FBQyxJQUFMLEtBQWEsQ0FBQSxtQkFBQSxHQUFvQixRQUFwQixDQUF0RDtBQUNFLGlCQUFPLEtBRFQ7O0FBREY7QUFJQSxhQUFPO0lBTE07Ozs7O0FBeGlCakIiLCJzb3VyY2VzQ29udGVudCI6WyJCcm93c2VyV2luZG93ID0gbnVsbCAjIERlZmVyIHJlcXVpcmUgdW50aWwgYWN0dWFsbHkgdXNlZFxue2lwY1JlbmRlcmVyfSA9IHJlcXVpcmUgJ2VsZWN0cm9uJ1xuXG57Q29tcG9zaXRlRGlzcG9zYWJsZX0gPSByZXF1aXJlICdhdG9tJ1xuVGFiVmlldyA9IHJlcXVpcmUgJy4vdGFiLXZpZXcnXG5cbm1vZHVsZS5leHBvcnRzID1cbmNsYXNzIFRhYkJhclZpZXdcbiAgY29uc3RydWN0b3I6IChAcGFuZSwgQGxvY2F0aW9uKSAtPlxuICAgIEBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgndWwnKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJsaXN0LWlubGluZVwiKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJ0YWItYmFyXCIpXG4gICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZChcImluc2V0LXBhbmVsXCIpXG4gICAgQGVsZW1lbnQuc2V0QXR0cmlidXRlKCdpcycsICdhdG9tLXRhYnMnKVxuICAgIEBlbGVtZW50LnNldEF0dHJpYnV0ZShcInRhYmluZGV4XCIsIC0xKVxuICAgIEBlbGVtZW50LnNldEF0dHJpYnV0ZShcImxvY2F0aW9uXCIsIEBsb2NhdGlvbilcblxuICAgIEB0YWJzID0gW11cbiAgICBAdGFic0J5RWxlbWVudCA9IG5ldyBXZWFrTWFwXG4gICAgQHN1YnNjcmlwdGlvbnMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZVxuXG4gICAgQHBhbmVFbGVtZW50ID0gQHBhbmUuZ2V0RWxlbWVudCgpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb21tYW5kcy5hZGQgQHBhbmVFbGVtZW50LFxuICAgICAgJ3RhYnM6a2VlcC1wZW5kaW5nLXRhYic6ID0+IEB0ZXJtaW5hdGVQZW5kaW5nU3RhdGVzKClcbiAgICAgICd0YWJzOmNsb3NlLXRhYic6ID0+IEBjbG9zZVRhYihAZ2V0QWN0aXZlVGFiKCkpXG4gICAgICAndGFiczpjbG9zZS1vdGhlci10YWJzJzogPT4gQGNsb3NlT3RoZXJUYWJzKEBnZXRBY3RpdmVUYWIoKSlcbiAgICAgICd0YWJzOmNsb3NlLXRhYnMtdG8tcmlnaHQnOiA9PiBAY2xvc2VUYWJzVG9SaWdodChAZ2V0QWN0aXZlVGFiKCkpXG4gICAgICAndGFiczpjbG9zZS10YWJzLXRvLWxlZnQnOiA9PiBAY2xvc2VUYWJzVG9MZWZ0KEBnZXRBY3RpdmVUYWIoKSlcbiAgICAgICd0YWJzOmNsb3NlLXNhdmVkLXRhYnMnOiA9PiBAY2xvc2VTYXZlZFRhYnMoKVxuICAgICAgJ3RhYnM6Y2xvc2UtYWxsLXRhYnMnOiAoZXZlbnQpID0+XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgIEBjbG9zZUFsbFRhYnMoKVxuICAgICAgJ3RhYnM6b3Blbi1pbi1uZXctd2luZG93JzogPT4gQG9wZW5Jbk5ld1dpbmRvdygpXG5cbiAgICBhZGRFbGVtZW50Q29tbWFuZHMgPSAoY29tbWFuZHMpID0+XG4gICAgICBjb21tYW5kc1dpdGhQcm9wYWdhdGlvblN0b3BwZWQgPSB7fVxuICAgICAgT2JqZWN0LmtleXMoY29tbWFuZHMpLmZvckVhY2ggKG5hbWUpIC0+XG4gICAgICAgIGNvbW1hbmRzV2l0aFByb3BhZ2F0aW9uU3RvcHBlZFtuYW1lXSA9IChldmVudCkgLT5cbiAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgICAgIGNvbW1hbmRzW25hbWVdKClcblxuICAgICAgQHN1YnNjcmlwdGlvbnMuYWRkKGF0b20uY29tbWFuZHMuYWRkKEBlbGVtZW50LCBjb21tYW5kc1dpdGhQcm9wYWdhdGlvblN0b3BwZWQpKVxuXG4gICAgYWRkRWxlbWVudENvbW1hbmRzXG4gICAgICAndGFiczpjbG9zZS10YWInOiA9PiBAY2xvc2VUYWIoKVxuICAgICAgJ3RhYnM6Y2xvc2Utb3RoZXItdGFicyc6ID0+IEBjbG9zZU90aGVyVGFicygpXG4gICAgICAndGFiczpjbG9zZS10YWJzLXRvLXJpZ2h0JzogPT4gQGNsb3NlVGFic1RvUmlnaHQoKVxuICAgICAgJ3RhYnM6Y2xvc2UtdGFicy10by1sZWZ0JzogPT4gQGNsb3NlVGFic1RvTGVmdCgpXG4gICAgICAndGFiczpjbG9zZS1zYXZlZC10YWJzJzogPT4gQGNsb3NlU2F2ZWRUYWJzKClcbiAgICAgICd0YWJzOmNsb3NlLWFsbC10YWJzJzogPT4gQGNsb3NlQWxsVGFicygpXG4gICAgICAndGFiczpzcGxpdC11cCc6ID0+IEBzcGxpdFRhYignc3BsaXRVcCcpXG4gICAgICAndGFiczpzcGxpdC1kb3duJzogPT4gQHNwbGl0VGFiKCdzcGxpdERvd24nKVxuICAgICAgJ3RhYnM6c3BsaXQtbGVmdCc6ID0+IEBzcGxpdFRhYignc3BsaXRMZWZ0JylcbiAgICAgICd0YWJzOnNwbGl0LXJpZ2h0JzogPT4gQHNwbGl0VGFiKCdzcGxpdFJpZ2h0JylcblxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgXCJtb3VzZWVudGVyXCIsIEBvbk1vdXNlRW50ZXIuYmluZCh0aGlzKVxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgXCJtb3VzZWxlYXZlXCIsIEBvbk1vdXNlTGVhdmUuYmluZCh0aGlzKVxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgXCJtb3VzZXdoZWVsXCIsIEBvbk1vdXNlV2hlZWwuYmluZCh0aGlzKVxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgXCJkcmFnc3RhcnRcIiwgQG9uRHJhZ1N0YXJ0LmJpbmQodGhpcylcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyIFwiZHJhZ2VuZFwiLCBAb25EcmFnRW5kLmJpbmQodGhpcylcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyIFwiZHJhZ2xlYXZlXCIsIEBvbkRyYWdMZWF2ZS5iaW5kKHRoaXMpXG4gICAgQGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lciBcImRyYWdvdmVyXCIsIEBvbkRyYWdPdmVyLmJpbmQodGhpcylcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyIFwiZHJvcFwiLCBAb25Ecm9wLmJpbmQodGhpcylcblxuICAgICMgVG9nZ2xlIHRoZSB0YWIgYmFyIHdoZW4gYSB0YWIgaXMgZHJhZ2dlZCBvdmVyIHRoZSBwYW5lIHdpdGggYWx3YXlzU2hvd1RhYkJhciA9IGZhbHNlXG4gICAgQHBhbmVFbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgJ2RyYWdlbnRlcicsIEBvblBhbmVEcmFnRW50ZXIuYmluZCh0aGlzKVxuICAgIEBwYW5lRWxlbWVudC5hZGRFdmVudExpc3RlbmVyICdkcmFnbGVhdmUnLCBAb25QYW5lRHJhZ0xlYXZlLmJpbmQodGhpcylcblxuICAgIEBwYW5lQ29udGFpbmVyID0gQHBhbmUuZ2V0Q29udGFpbmVyKClcbiAgICBAYWRkVGFiRm9ySXRlbShpdGVtKSBmb3IgaXRlbSBpbiBAcGFuZS5nZXRJdGVtcygpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgQHBhbmUub25EaWREZXN0cm95ID0+XG4gICAgICBAZGVzdHJveSgpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgQHBhbmUub25EaWRBZGRJdGVtICh7aXRlbSwgaW5kZXh9KSA9PlxuICAgICAgQGFkZFRhYkZvckl0ZW0oaXRlbSwgaW5kZXgpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgQHBhbmUub25EaWRNb3ZlSXRlbSAoe2l0ZW0sIG5ld0luZGV4fSkgPT5cbiAgICAgIEBtb3ZlSXRlbVRhYlRvSW5kZXgoaXRlbSwgbmV3SW5kZXgpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgQHBhbmUub25EaWRSZW1vdmVJdGVtICh7aXRlbX0pID0+XG4gICAgICBAcmVtb3ZlVGFiRm9ySXRlbShpdGVtKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIEBwYW5lLm9uRGlkQ2hhbmdlQWN0aXZlSXRlbSAoaXRlbSkgPT5cbiAgICAgIEB1cGRhdGVBY3RpdmVUYWIoKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29uZmlnLm9ic2VydmUgJ3RhYnMudGFiU2Nyb2xsaW5nJywgKHZhbHVlKSA9PiBAdXBkYXRlVGFiU2Nyb2xsaW5nKHZhbHVlKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbmZpZy5vYnNlcnZlICd0YWJzLnRhYlNjcm9sbGluZ1RocmVzaG9sZCcsICh2YWx1ZSkgPT4gQHVwZGF0ZVRhYlNjcm9sbGluZ1RocmVzaG9sZCh2YWx1ZSlcbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb25maWcub2JzZXJ2ZSAndGFicy5hbHdheXNTaG93VGFiQmFyJywgPT4gQHVwZGF0ZVRhYkJhclZpc2liaWxpdHkoKVxuXG4gICAgQHVwZGF0ZUFjdGl2ZVRhYigpXG5cbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyIFwibW91c2Vkb3duXCIsIEBvbk1vdXNlRG93bi5iaW5kKHRoaXMpXG4gICAgQGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lciBcImNsaWNrXCIsIEBvbkNsaWNrLmJpbmQodGhpcylcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyIFwiYXV4Y2xpY2tcIiwgQG9uQ2xpY2suYmluZCh0aGlzKVxuICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgXCJkYmxjbGlja1wiLCBAb25Eb3VibGVDbGljay5iaW5kKHRoaXMpXG5cbiAgICBAb25Ecm9wT25PdGhlcldpbmRvdyA9IEBvbkRyb3BPbk90aGVyV2luZG93LmJpbmQodGhpcylcbiAgICBpcGNSZW5kZXJlci5vbigndGFiOmRyb3BwZWQnLCBAb25Ecm9wT25PdGhlcldpbmRvdylcblxuICBkZXN0cm95OiAtPlxuICAgIGlwY1JlbmRlcmVyLnJlbW92ZUxpc3RlbmVyKCd0YWI6ZHJvcHBlZCcsIEBvbkRyb3BPbk90aGVyV2luZG93KVxuICAgIEBzdWJzY3JpcHRpb25zLmRpc3Bvc2UoKVxuICAgIEBlbGVtZW50LnJlbW92ZSgpXG5cbiAgdGVybWluYXRlUGVuZGluZ1N0YXRlczogLT5cbiAgICB0YWIudGVybWluYXRlUGVuZGluZ1N0YXRlPygpIGZvciB0YWIgaW4gQGdldFRhYnMoKVxuICAgIHJldHVyblxuXG4gIGFkZFRhYkZvckl0ZW06IChpdGVtLCBpbmRleCkgLT5cbiAgICB0YWJWaWV3ID0gbmV3IFRhYlZpZXcoe1xuICAgICAgaXRlbSxcbiAgICAgIEBwYW5lLFxuICAgICAgQHRhYnMsXG4gICAgICBkaWRDbGlja0Nsb3NlSWNvbjogPT5cbiAgICAgICAgQGNsb3NlVGFiKHRhYlZpZXcpXG4gICAgICAgIHJldHVyblxuICAgICAgQGxvY2F0aW9uXG4gICAgfSlcbiAgICB0YWJWaWV3LnRlcm1pbmF0ZVBlbmRpbmdTdGF0ZSgpIGlmIEBpc0l0ZW1Nb3ZpbmdCZXR3ZWVuUGFuZXNcbiAgICBAdGFic0J5RWxlbWVudC5zZXQodGFiVmlldy5lbGVtZW50LCB0YWJWaWV3KVxuICAgIEBpbnNlcnRUYWJBdEluZGV4KHRhYlZpZXcsIGluZGV4KVxuICAgIGlmIGF0b20uY29uZmlnLmdldCgndGFicy5hZGROZXdUYWJzQXRFbmQnKVxuICAgICAgQHBhbmUubW92ZUl0ZW0oaXRlbSwgQHBhbmUuZ2V0SXRlbXMoKS5sZW5ndGggLSAxKSB1bmxlc3MgQGlzSXRlbU1vdmluZ0JldHdlZW5QYW5lc1xuXG4gIG1vdmVJdGVtVGFiVG9JbmRleDogKGl0ZW0sIGluZGV4KSAtPlxuICAgIHRhYkluZGV4ID0gQHRhYnMuZmluZEluZGV4KCh0KSAtPiB0Lml0ZW0gaXMgaXRlbSlcbiAgICBpZiB0YWJJbmRleCBpc250IC0xXG4gICAgICB0YWIgPSBAdGFic1t0YWJJbmRleF1cbiAgICAgIHRhYi5lbGVtZW50LnJlbW92ZSgpXG4gICAgICBAdGFicy5zcGxpY2UodGFiSW5kZXgsIDEpXG4gICAgICBAaW5zZXJ0VGFiQXRJbmRleCh0YWIsIGluZGV4KVxuXG4gIGluc2VydFRhYkF0SW5kZXg6ICh0YWIsIGluZGV4KSAtPlxuICAgIGZvbGxvd2luZ1RhYiA9IEB0YWJzW2luZGV4XSBpZiBpbmRleD9cbiAgICBpZiBmb2xsb3dpbmdUYWJcbiAgICAgIEBlbGVtZW50Lmluc2VydEJlZm9yZSh0YWIuZWxlbWVudCwgZm9sbG93aW5nVGFiLmVsZW1lbnQpXG4gICAgICBAdGFicy5zcGxpY2UoaW5kZXgsIDAsIHRhYilcbiAgICBlbHNlXG4gICAgICBAZWxlbWVudC5hcHBlbmRDaGlsZCh0YWIuZWxlbWVudClcbiAgICAgIEB0YWJzLnB1c2godGFiKVxuXG4gICAgdGFiLnVwZGF0ZVRpdGxlKClcbiAgICBAdXBkYXRlVGFiQmFyVmlzaWJpbGl0eSgpXG5cbiAgcmVtb3ZlVGFiRm9ySXRlbTogKGl0ZW0pIC0+XG4gICAgdGFiSW5kZXggPSBAdGFicy5maW5kSW5kZXgoKHQpIC0+IHQuaXRlbSBpcyBpdGVtKVxuICAgIGlmIHRhYkluZGV4IGlzbnQgLTFcbiAgICAgIHRhYiA9IEB0YWJzW3RhYkluZGV4XVxuICAgICAgQHRhYnMuc3BsaWNlKHRhYkluZGV4LCAxKVxuICAgICAgQHRhYnNCeUVsZW1lbnQuZGVsZXRlKHRhYilcbiAgICAgIHRhYi5kZXN0cm95KClcbiAgICB0YWIudXBkYXRlVGl0bGUoKSBmb3IgdGFiIGluIEBnZXRUYWJzKClcbiAgICBAdXBkYXRlVGFiQmFyVmlzaWJpbGl0eSgpXG5cbiAgdXBkYXRlVGFiQmFyVmlzaWJpbGl0eTogLT5cbiAgICAjIFNob3cgdGFiIGJhciBpZiB0aGUgc2V0dGluZyBpcyB0cnVlIG9yIHRoZXJlIGlzIG1vcmUgdGhhbiBvbmUgdGFiXG4gICAgaWYgYXRvbS5jb25maWcuZ2V0KCd0YWJzLmFsd2F5c1Nob3dUYWJCYXInKSBvciBAcGFuZS5nZXRJdGVtcygpLmxlbmd0aCA+IDFcbiAgICAgIEBlbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2hpZGRlbicpXG4gICAgZWxzZVxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJylcblxuICBnZXRUYWJzOiAtPlxuICAgIEB0YWJzLnNsaWNlKClcblxuICB0YWJBdEluZGV4OiAoaW5kZXgpIC0+XG4gICAgQHRhYnNbaW5kZXhdXG5cbiAgdGFiRm9ySXRlbTogKGl0ZW0pIC0+XG4gICAgQHRhYnMuZmluZCgodCkgLT4gdC5pdGVtIGlzIGl0ZW0pXG5cbiAgc2V0QWN0aXZlVGFiOiAodGFiVmlldykgLT5cbiAgICBpZiB0YWJWaWV3PyBhbmQgdGFiVmlldyBpc250IEBhY3RpdmVUYWJcbiAgICAgIEBhY3RpdmVUYWI/LmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJylcbiAgICAgIEBhY3RpdmVUYWIgPSB0YWJWaWV3XG4gICAgICBAYWN0aXZlVGFiLmVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcbiAgICAgIEBhY3RpdmVUYWIuZWxlbWVudC5zY3JvbGxJbnRvVmlldyhmYWxzZSlcblxuICBnZXRBY3RpdmVUYWI6IC0+XG4gICAgQHRhYkZvckl0ZW0oQHBhbmUuZ2V0QWN0aXZlSXRlbSgpKVxuXG4gIHVwZGF0ZUFjdGl2ZVRhYjogLT5cbiAgICBAc2V0QWN0aXZlVGFiKEB0YWJGb3JJdGVtKEBwYW5lLmdldEFjdGl2ZUl0ZW0oKSkpXG5cbiAgY2xvc2VUYWI6ICh0YWIpIC0+XG4gICAgdGFiID89IEByaWdodENsaWNrZWRUYWJcbiAgICBAcGFuZS5kZXN0cm95SXRlbSh0YWIuaXRlbSkgaWYgdGFiP1xuXG4gIG9wZW5Jbk5ld1dpbmRvdzogKHRhYikgLT5cbiAgICB0YWIgPz0gQHJpZ2h0Q2xpY2tlZFRhYlxuICAgIGl0ZW0gPSB0YWI/Lml0ZW1cbiAgICByZXR1cm4gdW5sZXNzIGl0ZW0/XG4gICAgaWYgdHlwZW9mIGl0ZW0uZ2V0VVJJIGlzICdmdW5jdGlvbidcbiAgICAgIGl0ZW1VUkkgPSBpdGVtLmdldFVSSSgpXG4gICAgZWxzZSBpZiB0eXBlb2YgaXRlbS5nZXRQYXRoIGlzICdmdW5jdGlvbidcbiAgICAgIGl0ZW1VUkkgPSBpdGVtLmdldFBhdGgoKVxuICAgIGVsc2UgaWYgdHlwZW9mIGl0ZW0uZ2V0VXJpIGlzICdmdW5jdGlvbidcbiAgICAgIGl0ZW1VUkkgPSBpdGVtLmdldFVyaSgpXG4gICAgcmV0dXJuIHVubGVzcyBpdGVtVVJJP1xuICAgIEBjbG9zZVRhYih0YWIpXG4gICAgdGFiLmVsZW1lbnQuc3R5bGUubWF4V2lkdGggPSAnJyBmb3IgdGFiIGluIEBnZXRUYWJzKClcbiAgICBwYXRoc1RvT3BlbiA9IFthdG9tLnByb2plY3QuZ2V0UGF0aHMoKSwgaXRlbVVSSV0ucmVkdWNlICgoYSwgYikgLT4gYS5jb25jYXQoYikpLCBbXVxuICAgIGF0b20ub3Blbih7cGF0aHNUb09wZW46IHBhdGhzVG9PcGVuLCBuZXdXaW5kb3c6IHRydWUsIGRldk1vZGU6IGF0b20uZGV2TW9kZSwgc2FmZU1vZGU6IGF0b20uc2FmZU1vZGV9KVxuXG4gIHNwbGl0VGFiOiAoZm4pIC0+XG4gICAgaWYgaXRlbSA9IEByaWdodENsaWNrZWRUYWI/Lml0ZW1cbiAgICAgIGlmIGNvcGllZEl0ZW0gPSBpdGVtLmNvcHk/KClcbiAgICAgICAgQHBhbmVbZm5dKGl0ZW1zOiBbY29waWVkSXRlbV0pXG5cbiAgY2xvc2VPdGhlclRhYnM6IChhY3RpdmUpIC0+XG4gICAgdGFicyA9IEBnZXRUYWJzKClcbiAgICBhY3RpdmUgPz0gQHJpZ2h0Q2xpY2tlZFRhYlxuICAgIHJldHVybiB1bmxlc3MgYWN0aXZlP1xuICAgIEBjbG9zZVRhYiB0YWIgZm9yIHRhYiBpbiB0YWJzIHdoZW4gdGFiIGlzbnQgYWN0aXZlXG5cbiAgY2xvc2VUYWJzVG9SaWdodDogKGFjdGl2ZSkgLT5cbiAgICB0YWJzID0gQGdldFRhYnMoKVxuICAgIGFjdGl2ZSA/PSBAcmlnaHRDbGlja2VkVGFiXG4gICAgaW5kZXggPSB0YWJzLmluZGV4T2YoYWN0aXZlKVxuICAgIHJldHVybiBpZiBpbmRleCBpcyAtMVxuICAgIEBjbG9zZVRhYiB0YWIgZm9yIHRhYiwgaSBpbiB0YWJzIHdoZW4gaSA+IGluZGV4XG5cbiAgY2xvc2VUYWJzVG9MZWZ0OiAoYWN0aXZlKSAtPlxuICAgIHRhYnMgPSBAZ2V0VGFicygpXG4gICAgYWN0aXZlID89IEByaWdodENsaWNrZWRUYWJcbiAgICBpbmRleCA9IHRhYnMuaW5kZXhPZihhY3RpdmUpXG4gICAgcmV0dXJuIGlmIGluZGV4IGlzIC0xXG4gICAgQGNsb3NlVGFiIHRhYiBmb3IgdGFiLCBpIGluIHRhYnMgd2hlbiBpIDwgaW5kZXhcblxuICBjbG9zZVNhdmVkVGFiczogLT5cbiAgICBmb3IgdGFiIGluIEBnZXRUYWJzKClcbiAgICAgIEBjbG9zZVRhYih0YWIpIHVubGVzcyB0YWIuaXRlbS5pc01vZGlmaWVkPygpXG5cbiAgY2xvc2VBbGxUYWJzOiAtPlxuICAgIEBjbG9zZVRhYih0YWIpIGZvciB0YWIgaW4gQGdldFRhYnMoKVxuXG4gIGdldFdpbmRvd0lkOiAtPlxuICAgIEB3aW5kb3dJZCA/PSBhdG9tLmdldEN1cnJlbnRXaW5kb3coKS5pZFxuXG4gIG9uRHJhZ1N0YXJ0OiAoZXZlbnQpIC0+XG4gICAgQGRyYWdnZWRUYWIgPSBAdGFiRm9yRWxlbWVudChldmVudC50YXJnZXQpXG4gICAgcmV0dXJuIHVubGVzcyBAZHJhZ2dlZFRhYlxuICAgIEBsYXN0RHJvcFRhcmdldEluZGV4ID0gbnVsbFxuXG4gICAgZXZlbnQuZGF0YVRyYW5zZmVyLnNldERhdGEgJ2F0b20tdGFiLWV2ZW50JywgJ3RydWUnXG5cbiAgICBAZHJhZ2dlZFRhYi5lbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2lzLWRyYWdnaW5nJylcbiAgICBAZHJhZ2dlZFRhYi5kZXN0cm95VG9vbHRpcCgpXG5cbiAgICB0YWJJbmRleCA9IEB0YWJzLmluZGV4T2YoQGRyYWdnZWRUYWIpXG4gICAgZXZlbnQuZGF0YVRyYW5zZmVyLnNldERhdGEgJ3NvcnRhYmxlLWluZGV4JywgdGFiSW5kZXhcblxuICAgIHBhbmVJbmRleCA9IEBwYW5lQ29udGFpbmVyLmdldFBhbmVzKCkuaW5kZXhPZihAcGFuZSlcbiAgICBldmVudC5kYXRhVHJhbnNmZXIuc2V0RGF0YSAnZnJvbS1wYW5lLWluZGV4JywgcGFuZUluZGV4XG4gICAgZXZlbnQuZGF0YVRyYW5zZmVyLnNldERhdGEgJ2Zyb20tcGFuZS1pZCcsIEBwYW5lLmlkXG4gICAgZXZlbnQuZGF0YVRyYW5zZmVyLnNldERhdGEgJ2Zyb20td2luZG93LWlkJywgQGdldFdpbmRvd0lkKClcblxuICAgIGl0ZW0gPSBAcGFuZS5nZXRJdGVtcygpW0B0YWJzLmluZGV4T2YoQGRyYWdnZWRUYWIpXVxuICAgIHJldHVybiB1bmxlc3MgaXRlbT9cblxuICAgIGlmIHR5cGVvZiBpdGVtLmdldFVSSSBpcyAnZnVuY3Rpb24nXG4gICAgICBpdGVtVVJJID0gaXRlbS5nZXRVUkkoKSA/ICcnXG4gICAgZWxzZSBpZiB0eXBlb2YgaXRlbS5nZXRQYXRoIGlzICdmdW5jdGlvbidcbiAgICAgIGl0ZW1VUkkgPSBpdGVtLmdldFBhdGgoKSA/ICcnXG4gICAgZWxzZSBpZiB0eXBlb2YgaXRlbS5nZXRVcmkgaXMgJ2Z1bmN0aW9uJ1xuICAgICAgaXRlbVVSSSA9IGl0ZW0uZ2V0VXJpKCkgPyAnJ1xuXG4gICAgaWYgdHlwZW9mIGl0ZW0uZ2V0QWxsb3dlZExvY2F0aW9ucyBpcyAnZnVuY3Rpb24nXG4gICAgICBmb3IgbG9jYXRpb24gaW4gaXRlbS5nZXRBbGxvd2VkTG9jYXRpb25zKClcbiAgICAgICAgZXZlbnQuZGF0YVRyYW5zZmVyLnNldERhdGEoXCJhbGxvd2VkLWxvY2F0aW9uLSN7bG9jYXRpb259XCIsICd0cnVlJylcbiAgICBlbHNlXG4gICAgICBldmVudC5kYXRhVHJhbnNmZXIuc2V0RGF0YSAnYWxsb3ctYWxsLWxvY2F0aW9ucycsICd0cnVlJ1xuXG4gICAgaWYgaXRlbVVSST9cbiAgICAgIGV2ZW50LmRhdGFUcmFuc2Zlci5zZXREYXRhICd0ZXh0L3BsYWluJywgaXRlbVVSSVxuXG4gICAgICBpZiBwcm9jZXNzLnBsYXRmb3JtIGlzICdkYXJ3aW4nICMgc2VlICM2OVxuICAgICAgICBpdGVtVVJJID0gXCJmaWxlOi8vI3tpdGVtVVJJfVwiIHVubGVzcyBAdXJpSGFzUHJvdG9jb2woaXRlbVVSSSlcbiAgICAgICAgZXZlbnQuZGF0YVRyYW5zZmVyLnNldERhdGEgJ3RleHQvdXJpLWxpc3QnLCBpdGVtVVJJXG5cbiAgICAgIGlmIGl0ZW0uaXNNb2RpZmllZD8oKSBhbmQgaXRlbS5nZXRUZXh0P1xuICAgICAgICBldmVudC5kYXRhVHJhbnNmZXIuc2V0RGF0YSAnaGFzLXVuc2F2ZWQtY2hhbmdlcycsICd0cnVlJ1xuICAgICAgICBldmVudC5kYXRhVHJhbnNmZXIuc2V0RGF0YSAnbW9kaWZpZWQtdGV4dCcsIGl0ZW0uZ2V0VGV4dCgpXG5cbiAgdXJpSGFzUHJvdG9jb2w6ICh1cmkpIC0+XG4gICAgdHJ5XG4gICAgICByZXF1aXJlKCd1cmwnKS5wYXJzZSh1cmkpLnByb3RvY29sP1xuICAgIGNhdGNoIGVycm9yXG4gICAgICBmYWxzZVxuXG4gIG9uRHJhZ0xlYXZlOiAoZXZlbnQpIC0+XG4gICAgIyBEbyBub3QgZG8gYW55dGhpbmcgdW5sZXNzIHRoZSBkcmFnIGdvZXMgb3V0c2lkZSB0aGUgdGFiIGJhclxuICAgIHVubGVzcyBldmVudC5jdXJyZW50VGFyZ2V0LmNvbnRhaW5zKGV2ZW50LnJlbGF0ZWRUYXJnZXQpXG4gICAgICBAcmVtb3ZlUGxhY2Vob2xkZXIoKVxuICAgICAgQGxhc3REcm9wVGFyZ2V0SW5kZXggPSBudWxsXG4gICAgICB0YWIuZWxlbWVudC5zdHlsZS5tYXhXaWR0aCA9ICcnIGZvciB0YWIgaW4gQGdldFRhYnMoKVxuXG4gIG9uRHJhZ0VuZDogKGV2ZW50KSAtPlxuICAgIHJldHVybiB1bmxlc3MgQHRhYkZvckVsZW1lbnQoZXZlbnQudGFyZ2V0KVxuXG4gICAgQGNsZWFyRHJvcFRhcmdldCgpXG5cbiAgb25EcmFnT3ZlcjogKGV2ZW50KSAtPlxuICAgIHJldHVybiB1bmxlc3MgQGlzQXRvbVRhYkV2ZW50KGV2ZW50KVxuICAgIHJldHVybiB1bmxlc3MgQGl0ZW1Jc0FsbG93ZWQoZXZlbnQsIEBsb2NhdGlvbilcblxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKVxuXG4gICAgbmV3RHJvcFRhcmdldEluZGV4ID0gQGdldERyb3BUYXJnZXRJbmRleChldmVudClcbiAgICByZXR1cm4gdW5sZXNzIG5ld0Ryb3BUYXJnZXRJbmRleD9cbiAgICByZXR1cm4gaWYgQGxhc3REcm9wVGFyZ2V0SW5kZXggaXMgbmV3RHJvcFRhcmdldEluZGV4XG4gICAgQGxhc3REcm9wVGFyZ2V0SW5kZXggPSBuZXdEcm9wVGFyZ2V0SW5kZXhcblxuICAgIEByZW1vdmVEcm9wVGFyZ2V0Q2xhc3NlcygpXG5cbiAgICB0YWJzID0gQGdldFRhYnMoKVxuICAgIHBsYWNlaG9sZGVyID0gQGdldFBsYWNlaG9sZGVyKClcbiAgICByZXR1cm4gdW5sZXNzIHBsYWNlaG9sZGVyP1xuXG4gICAgaWYgbmV3RHJvcFRhcmdldEluZGV4IDwgdGFicy5sZW5ndGhcbiAgICAgIHRhYiA9IHRhYnNbbmV3RHJvcFRhcmdldEluZGV4XVxuICAgICAgdGFiLmVsZW1lbnQuY2xhc3NMaXN0LmFkZCAnaXMtZHJvcC10YXJnZXQnXG4gICAgICB0YWIuZWxlbWVudC5wYXJlbnRFbGVtZW50Lmluc2VydEJlZm9yZShwbGFjZWhvbGRlciwgdGFiLmVsZW1lbnQpXG4gICAgZWxzZVxuICAgICAgaWYgdGFiID0gdGFic1tuZXdEcm9wVGFyZ2V0SW5kZXggLSAxXVxuICAgICAgICB0YWIuZWxlbWVudC5jbGFzc0xpc3QuYWRkICdkcm9wLXRhcmdldC1pcy1hZnRlcidcbiAgICAgICAgaWYgc2libGluZyA9IHRhYi5lbGVtZW50Lm5leHRTaWJsaW5nXG4gICAgICAgICAgdGFiLmVsZW1lbnQucGFyZW50RWxlbWVudC5pbnNlcnRCZWZvcmUocGxhY2Vob2xkZXIsIHNpYmxpbmcpXG4gICAgICAgIGVsc2VcbiAgICAgICAgICB0YWIuZWxlbWVudC5wYXJlbnRFbGVtZW50LmFwcGVuZENoaWxkKHBsYWNlaG9sZGVyKVxuXG4gIG9uRHJvcE9uT3RoZXJXaW5kb3c6IChldmVudCwgZnJvbVBhbmVJZCwgZnJvbUl0ZW1JbmRleCkgLT5cbiAgICBpZiBAcGFuZS5pZCBpcyBmcm9tUGFuZUlkXG4gICAgICBpZiBpdGVtVG9SZW1vdmUgPSBAcGFuZS5nZXRJdGVtcygpW2Zyb21JdGVtSW5kZXhdXG4gICAgICAgIEBwYW5lLmRlc3Ryb3lJdGVtKGl0ZW1Ub1JlbW92ZSlcblxuICAgIEBjbGVhckRyb3BUYXJnZXQoKVxuXG4gIGNsZWFyRHJvcFRhcmdldDogLT5cbiAgICBAZHJhZ2dlZFRhYj8uZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKCdpcy1kcmFnZ2luZycpXG4gICAgQGRyYWdnZWRUYWI/LnVwZGF0ZVRvb2x0aXAoKVxuICAgIEBkcmFnZ2VkVGFiID0gbnVsbFxuICAgIEByZW1vdmVEcm9wVGFyZ2V0Q2xhc3NlcygpXG4gICAgQHJlbW92ZVBsYWNlaG9sZGVyKClcblxuICBvbkRyb3A6IChldmVudCkgLT5cbiAgICByZXR1cm4gdW5sZXNzIEBpc0F0b21UYWJFdmVudChldmVudClcblxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcblxuICAgIGZyb21XaW5kb3dJZCAgPSBwYXJzZUludChldmVudC5kYXRhVHJhbnNmZXIuZ2V0RGF0YSgnZnJvbS13aW5kb3ctaWQnKSlcbiAgICBmcm9tUGFuZUlkICAgID0gcGFyc2VJbnQoZXZlbnQuZGF0YVRyYW5zZmVyLmdldERhdGEoJ2Zyb20tcGFuZS1pZCcpKVxuICAgIGZyb21JbmRleCAgICAgPSBwYXJzZUludChldmVudC5kYXRhVHJhbnNmZXIuZ2V0RGF0YSgnc29ydGFibGUtaW5kZXgnKSlcbiAgICBmcm9tUGFuZUluZGV4ID0gcGFyc2VJbnQoZXZlbnQuZGF0YVRyYW5zZmVyLmdldERhdGEoJ2Zyb20tcGFuZS1pbmRleCcpKVxuXG4gICAgaGFzVW5zYXZlZENoYW5nZXMgPSBldmVudC5kYXRhVHJhbnNmZXIuZ2V0RGF0YSgnaGFzLXVuc2F2ZWQtY2hhbmdlcycpIGlzICd0cnVlJ1xuICAgIG1vZGlmaWVkVGV4dCA9IGV2ZW50LmRhdGFUcmFuc2Zlci5nZXREYXRhKCdtb2RpZmllZC10ZXh0JylcblxuICAgIHRvSW5kZXggPSBAZ2V0RHJvcFRhcmdldEluZGV4KGV2ZW50KVxuICAgIHRvUGFuZSA9IEBwYW5lXG5cbiAgICBAY2xlYXJEcm9wVGFyZ2V0KClcblxuICAgIHJldHVybiB1bmxlc3MgQGl0ZW1Jc0FsbG93ZWQoZXZlbnQsIEBsb2NhdGlvbilcblxuICAgIGlmIGZyb21XaW5kb3dJZCBpcyBAZ2V0V2luZG93SWQoKVxuICAgICAgZnJvbVBhbmUgPSBAcGFuZUNvbnRhaW5lci5nZXRQYW5lcygpW2Zyb21QYW5lSW5kZXhdXG4gICAgICBpZiBmcm9tUGFuZT8uaWQgaXNudCBmcm9tUGFuZUlkXG4gICAgICAgICMgSWYgZHJhZ2dpbmcgZnJvbSBhIGRpZmZlcmVudCBwYW5lIGNvbnRhaW5lciwgd2UgaGF2ZSB0byBiZSBtb3JlXG4gICAgICAgICMgZXhoYXVzdGl2ZSBpbiBvdXIgc2VhcmNoLlxuICAgICAgICBmcm9tUGFuZSA9IEFycmF5LmZyb20gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnYXRvbS1wYW5lJylcbiAgICAgICAgICAubWFwIChwYW5lRWwpIC0+IHBhbmVFbC5tb2RlbFxuICAgICAgICAgIC5maW5kIChwYW5lKSAtPiBwYW5lLmlkIGlzIGZyb21QYW5lSWRcbiAgICAgIGl0ZW0gPSBmcm9tUGFuZS5nZXRJdGVtcygpW2Zyb21JbmRleF1cbiAgICAgIEBtb3ZlSXRlbUJldHdlZW5QYW5lcyhmcm9tUGFuZSwgZnJvbUluZGV4LCB0b1BhbmUsIHRvSW5kZXgsIGl0ZW0pIGlmIGl0ZW0/XG4gICAgZWxzZVxuICAgICAgZHJvcHBlZFVSSSA9IGV2ZW50LmRhdGFUcmFuc2Zlci5nZXREYXRhKCd0ZXh0L3BsYWluJylcbiAgICAgIGF0b20ud29ya3NwYWNlLm9wZW4oZHJvcHBlZFVSSSkudGhlbiAoaXRlbSkgPT5cbiAgICAgICAgIyBNb3ZlIHRoZSBpdGVtIGZyb20gdGhlIHBhbmUgaXQgd2FzIG9wZW5lZCBvbiB0byB0aGUgdGFyZ2V0IHBhbmVcbiAgICAgICAgIyB3aGVyZSBpdCB3YXMgZHJvcHBlZCBvbnRvXG4gICAgICAgIGFjdGl2ZVBhbmUgPSBhdG9tLndvcmtzcGFjZS5nZXRBY3RpdmVQYW5lKClcbiAgICAgICAgYWN0aXZlSXRlbUluZGV4ID0gYWN0aXZlUGFuZS5nZXRJdGVtcygpLmluZGV4T2YoaXRlbSlcbiAgICAgICAgQG1vdmVJdGVtQmV0d2VlblBhbmVzKGFjdGl2ZVBhbmUsIGFjdGl2ZUl0ZW1JbmRleCwgdG9QYW5lLCB0b0luZGV4LCBpdGVtKVxuICAgICAgICBpdGVtLnNldFRleHQ/KG1vZGlmaWVkVGV4dCkgaWYgaGFzVW5zYXZlZENoYW5nZXNcblxuICAgICAgICBpZiBub3QgaXNOYU4oZnJvbVdpbmRvd0lkKVxuICAgICAgICAgICMgTGV0IHRoZSB3aW5kb3cgd2hlcmUgdGhlIGRyYWcgc3RhcnRlZCBrbm93IHRoYXQgdGhlIHRhYiB3YXMgZHJvcHBlZFxuICAgICAgICAgIGJyb3dzZXJXaW5kb3cgPSBAYnJvd3NlcldpbmRvd0ZvcklkKGZyb21XaW5kb3dJZClcbiAgICAgICAgICBicm93c2VyV2luZG93Py53ZWJDb250ZW50cy5zZW5kKCd0YWI6ZHJvcHBlZCcsIGZyb21QYW5lSWQsIGZyb21JbmRleClcblxuICAgICAgYXRvbS5mb2N1cygpXG5cbiAgIyBTaG93IHRoZSB0YWIgYmFyIHdoZW4gYSB0YWIgaXMgYmVpbmcgZHJhZ2dlZCBpbiB0aGlzIHBhbmUgd2hlbiBhbHdheXNTaG93VGFiQmFyID0gZmFsc2VcbiAgb25QYW5lRHJhZ0VudGVyOiAoZXZlbnQpIC0+XG4gICAgcmV0dXJuIHVubGVzcyBAaXNBdG9tVGFiRXZlbnQoZXZlbnQpXG4gICAgcmV0dXJuIHVubGVzcyBAaXRlbUlzQWxsb3dlZChldmVudCwgQGxvY2F0aW9uKVxuICAgIHJldHVybiBpZiBAcGFuZS5nZXRJdGVtcygpLmxlbmd0aCA+IDEgb3IgYXRvbS5jb25maWcuZ2V0KCd0YWJzLmFsd2F5c1Nob3dUYWJCYXInKVxuICAgIGlmIEBwYW5lRWxlbWVudC5jb250YWlucyhldmVudC5yZWxhdGVkVGFyZ2V0KVxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJylcblxuICAjIEhpZGUgdGhlIHRhYiBiYXIgd2hlbiB0aGUgZHJhZ2dlZCB0YWIgbGVhdmVzIHRoaXMgcGFuZSB3aGVuIGFsd2F5c1Nob3dUYWJCYXIgPSBmYWxzZVxuICBvblBhbmVEcmFnTGVhdmU6IChldmVudCkgLT5cbiAgICByZXR1cm4gdW5sZXNzIEBpc0F0b21UYWJFdmVudChldmVudClcbiAgICByZXR1cm4gdW5sZXNzIEBpdGVtSXNBbGxvd2VkKGV2ZW50LCBAbG9jYXRpb24pXG4gICAgcmV0dXJuIGlmIEBwYW5lLmdldEl0ZW1zKCkubGVuZ3RoID4gMSBvciBhdG9tLmNvbmZpZy5nZXQoJ3RhYnMuYWx3YXlzU2hvd1RhYkJhcicpXG4gICAgdW5sZXNzIEBwYW5lRWxlbWVudC5jb250YWlucyhldmVudC5yZWxhdGVkVGFyZ2V0KVxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJylcblxuICBvbk1vdXNlV2hlZWw6IChldmVudCkgLT5cbiAgICByZXR1cm4gaWYgZXZlbnQuc2hpZnRLZXkgb3Igbm90IEB0YWJTY3JvbGxpbmdcblxuICAgIEB3aGVlbERlbHRhID89IDBcbiAgICBAd2hlZWxEZWx0YSArPSBldmVudC53aGVlbERlbHRhWVxuXG4gICAgaWYgQHdoZWVsRGVsdGEgPD0gLUB0YWJTY3JvbGxpbmdUaHJlc2hvbGRcbiAgICAgIEB3aGVlbERlbHRhID0gMFxuICAgICAgQHBhbmUuYWN0aXZhdGVOZXh0SXRlbSgpXG4gICAgZWxzZSBpZiBAd2hlZWxEZWx0YSA+PSBAdGFiU2Nyb2xsaW5nVGhyZXNob2xkXG4gICAgICBAd2hlZWxEZWx0YSA9IDBcbiAgICAgIEBwYW5lLmFjdGl2YXRlUHJldmlvdXNJdGVtKClcblxuICBvbk1vdXNlRG93bjogKGV2ZW50KSAtPlxuICAgIEBwYW5lLmFjdGl2YXRlKCkgdW5sZXNzIEBwYW5lLmlzRGVzdHJveWVkKClcblxuICAgIHRhYiA9IEB0YWJGb3JFbGVtZW50KGV2ZW50LnRhcmdldClcbiAgICByZXR1cm4gdW5sZXNzIHRhYlxuXG4gICAgaWYgZXZlbnQuYnV0dG9uIGlzIDIgb3IgKGV2ZW50LmJ1dHRvbiBpcyAwIGFuZCBldmVudC5jdHJsS2V5IGlzIHRydWUpXG4gICAgICBAcmlnaHRDbGlja2VkVGFiID0gdGFiXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgZWxzZSBpZiBldmVudC5idXR0b24gaXMgMVxuICAgICAgIyBUaGlzIHByZXZlbnRzIENocm9taXVtIGZyb20gYWN0aXZhdGluZyBcInNjcm9sbCBtb2RlXCIgd2hlblxuICAgICAgIyBtaWRkbGUtY2xpY2tpbmcgd2hpbGUgc29tZSB0YWJzIGFyZSBvZmYtc2NyZWVuLlxuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKVxuXG4gIG9uQ2xpY2s6IChldmVudCkgLT5cbiAgICB0YWIgPSBAdGFiRm9yRWxlbWVudChldmVudC50YXJnZXQpXG4gICAgcmV0dXJuIHVubGVzcyB0YWJcblxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICBpZiBldmVudC5idXR0b24gaXMgMiBvciAoZXZlbnQuYnV0dG9uIGlzIDAgYW5kIGV2ZW50LmN0cmxLZXkgaXMgdHJ1ZSlcbiAgICAgICMgQmFpbCBvdXQgZWFybHkgd2hlbiByZWNlaXZpbmcgdGhpcyBldmVudCwgYmVjYXVzZSB3ZSBoYXZlIGFscmVhZHlcbiAgICAgICMgaGFuZGxlZCBpdCBpbiB0aGUgbW91c2Vkb3duIGhhbmRsZXIuXG4gICAgICByZXR1cm5cbiAgICBlbHNlIGlmIGV2ZW50LmJ1dHRvbiBpcyAwIGFuZCBub3QgZXZlbnQudGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygnY2xvc2UtaWNvbicpXG4gICAgICBAcGFuZS5hY3RpdmF0ZUl0ZW0odGFiLml0ZW0pXG4gICAgZWxzZSBpZiBldmVudC5idXR0b24gaXMgMVxuICAgICAgQHBhbmUuZGVzdHJveUl0ZW0odGFiLml0ZW0pXG5cbiAgb25Eb3VibGVDbGljazogKGV2ZW50KSAtPlxuICAgIGlmIHRhYiA9IEB0YWJGb3JFbGVtZW50KGV2ZW50LnRhcmdldClcbiAgICAgIHRhYi5pdGVtLnRlcm1pbmF0ZVBlbmRpbmdTdGF0ZT8oKVxuICAgIGVsc2UgaWYgZXZlbnQudGFyZ2V0IGlzIEBlbGVtZW50XG4gICAgICBhdG9tLmNvbW1hbmRzLmRpc3BhdGNoKEBlbGVtZW50LCAnYXBwbGljYXRpb246bmV3LWZpbGUnKVxuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKVxuXG4gIHVwZGF0ZVRhYlNjcm9sbGluZ1RocmVzaG9sZDogKHZhbHVlKSAtPlxuICAgIEB0YWJTY3JvbGxpbmdUaHJlc2hvbGQgPSB2YWx1ZVxuXG4gIHVwZGF0ZVRhYlNjcm9sbGluZzogKHZhbHVlKSAtPlxuICAgIGlmIHZhbHVlIGlzICdwbGF0Zm9ybSdcbiAgICAgIEB0YWJTY3JvbGxpbmcgPSAocHJvY2Vzcy5wbGF0Zm9ybSBpcyAnbGludXgnKVxuICAgIGVsc2VcbiAgICAgIEB0YWJTY3JvbGxpbmcgPSB2YWx1ZVxuXG4gIGJyb3dzZXJXaW5kb3dGb3JJZDogKGlkKSAtPlxuICAgIEJyb3dzZXJXaW5kb3cgPz0gcmVxdWlyZSgnZWxlY3Ryb24nKS5yZW1vdGUuQnJvd3NlcldpbmRvd1xuXG4gICAgQnJvd3NlcldpbmRvdy5mcm9tSWQgaWRcblxuICBtb3ZlSXRlbUJldHdlZW5QYW5lczogKGZyb21QYW5lLCBmcm9tSW5kZXgsIHRvUGFuZSwgdG9JbmRleCwgaXRlbSkgLT5cbiAgICB0cnlcbiAgICAgIGlmIHRvUGFuZSBpcyBmcm9tUGFuZVxuICAgICAgICB0b0luZGV4LS0gaWYgZnJvbUluZGV4IDwgdG9JbmRleFxuICAgICAgICB0b1BhbmUubW92ZUl0ZW0oaXRlbSwgdG9JbmRleClcbiAgICAgIGVsc2VcbiAgICAgICAgQGlzSXRlbU1vdmluZ0JldHdlZW5QYW5lcyA9IHRydWVcbiAgICAgICAgZnJvbVBhbmUubW92ZUl0ZW1Ub1BhbmUoaXRlbSwgdG9QYW5lLCB0b0luZGV4LS0pXG4gICAgICB0b1BhbmUuYWN0aXZhdGVJdGVtKGl0ZW0pXG4gICAgICB0b1BhbmUuYWN0aXZhdGUoKVxuICAgIGZpbmFsbHlcbiAgICAgIEBpc0l0ZW1Nb3ZpbmdCZXR3ZWVuUGFuZXMgPSBmYWxzZVxuXG4gIHJlbW92ZURyb3BUYXJnZXRDbGFzc2VzOiAtPlxuICAgIHdvcmtzcGFjZUVsZW1lbnQgPSBhdG9tLndvcmtzcGFjZS5nZXRFbGVtZW50KClcbiAgICBmb3IgZHJvcFRhcmdldCBpbiB3b3Jrc3BhY2VFbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50YWItYmFyIC5pcy1kcm9wLXRhcmdldCcpXG4gICAgICBkcm9wVGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoJ2lzLWRyb3AtdGFyZ2V0JylcblxuICAgIGZvciBkcm9wVGFyZ2V0IGluIHdvcmtzcGFjZUVsZW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnRhYi1iYXIgLmRyb3AtdGFyZ2V0LWlzLWFmdGVyJylcbiAgICAgIGRyb3BUYXJnZXQuY2xhc3NMaXN0LnJlbW92ZSgnZHJvcC10YXJnZXQtaXMtYWZ0ZXInKVxuXG4gIGdldERyb3BUYXJnZXRJbmRleDogKGV2ZW50KSAtPlxuICAgIHRhcmdldCA9IGV2ZW50LnRhcmdldFxuXG4gICAgcmV0dXJuIGlmIEBpc1BsYWNlaG9sZGVyKHRhcmdldClcblxuICAgIHRhYnMgPSBAZ2V0VGFicygpXG4gICAgdGFiID0gQHRhYkZvckVsZW1lbnQodGFyZ2V0KVxuICAgIHRhYiA/PSB0YWJzW3RhYnMubGVuZ3RoIC0gMV1cblxuICAgIHJldHVybiAwIHVubGVzcyB0YWI/XG5cbiAgICB7bGVmdCwgd2lkdGh9ID0gdGFiLmVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KClcbiAgICBlbGVtZW50Q2VudGVyID0gbGVmdCArIHdpZHRoIC8gMlxuICAgIGVsZW1lbnRJbmRleCA9IHRhYnMuaW5kZXhPZih0YWIpXG5cbiAgICBpZiBldmVudC5wYWdlWCA8IGVsZW1lbnRDZW50ZXJcbiAgICAgIGVsZW1lbnRJbmRleFxuICAgIGVsc2VcbiAgICAgIGVsZW1lbnRJbmRleCArIDFcblxuICBnZXRQbGFjZWhvbGRlcjogLT5cbiAgICByZXR1cm4gQHBsYWNlaG9sZGVyRWwgaWYgQHBsYWNlaG9sZGVyRWw/XG5cbiAgICBAcGxhY2Vob2xkZXJFbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsaVwiKVxuICAgIEBwbGFjZWhvbGRlckVsLmNsYXNzTGlzdC5hZGQoXCJwbGFjZWhvbGRlclwiKVxuICAgIEBwbGFjZWhvbGRlckVsXG5cbiAgcmVtb3ZlUGxhY2Vob2xkZXI6IC0+XG4gICAgQHBsYWNlaG9sZGVyRWw/LnJlbW92ZSgpXG4gICAgQHBsYWNlaG9sZGVyRWwgPSBudWxsXG5cbiAgaXNQbGFjZWhvbGRlcjogKGVsZW1lbnQpIC0+XG4gICAgZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoJ3BsYWNlaG9sZGVyJylcblxuICBvbk1vdXNlRW50ZXI6IC0+XG4gICAgZm9yIHRhYiBpbiBAZ2V0VGFicygpXG4gICAgICB7d2lkdGh9ID0gdGFiLmVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KClcbiAgICAgIHRhYi5lbGVtZW50LnN0eWxlLm1heFdpZHRoID0gd2lkdGgudG9GaXhlZCgyKSArICdweCdcbiAgICByZXR1cm5cblxuICBvbk1vdXNlTGVhdmU6IC0+XG4gICAgdGFiLmVsZW1lbnQuc3R5bGUubWF4V2lkdGggPSAnJyBmb3IgdGFiIGluIEBnZXRUYWJzKClcbiAgICByZXR1cm5cblxuICB0YWJGb3JFbGVtZW50OiAoZWxlbWVudCkgLT5cbiAgICBjdXJyZW50RWxlbWVudCA9IGVsZW1lbnRcbiAgICB3aGlsZSBjdXJyZW50RWxlbWVudD9cbiAgICAgIGlmIHRhYiA9IEB0YWJzQnlFbGVtZW50LmdldChjdXJyZW50RWxlbWVudClcbiAgICAgICAgcmV0dXJuIHRhYlxuICAgICAgZWxzZVxuICAgICAgICBjdXJyZW50RWxlbWVudCA9IGN1cnJlbnRFbGVtZW50LnBhcmVudEVsZW1lbnRcblxuICBpc0F0b21UYWJFdmVudDogKGV2ZW50KSAtPlxuICAgIGZvciBpdGVtIGluIGV2ZW50LmRhdGFUcmFuc2Zlci5pdGVtc1xuICAgICAgaWYgaXRlbS50eXBlIGlzICdhdG9tLXRhYi1ldmVudCdcbiAgICAgICAgcmV0dXJuIHRydWVcblxuICAgIHJldHVybiBmYWxzZVxuXG4gIGl0ZW1Jc0FsbG93ZWQ6IChldmVudCwgbG9jYXRpb24pIC0+XG4gICAgZm9yIGl0ZW0gaW4gZXZlbnQuZGF0YVRyYW5zZmVyLml0ZW1zXG4gICAgICBpZiBpdGVtLnR5cGUgaXMgJ2FsbG93LWFsbC1sb2NhdGlvbnMnIG9yIGl0ZW0udHlwZSBpcyBcImFsbG93ZWQtbG9jYXRpb24tI3tsb2NhdGlvbn1cIlxuICAgICAgICByZXR1cm4gdHJ1ZVxuXG4gICAgcmV0dXJuIGZhbHNlXG4iXX0=
