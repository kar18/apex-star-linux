(function() {
  var Disposable, StatusBarView, Tile;

  Disposable = require('atom').Disposable;

  Tile = require('./tile');

  module.exports = StatusBarView = (function() {
    function StatusBarView() {
      var flexboxHackElement;
      this.element = document.createElement('status-bar');
      this.element.classList.add('status-bar');
      flexboxHackElement = document.createElement('div');
      flexboxHackElement.classList.add('flexbox-repaint-hack');
      this.element.appendChild(flexboxHackElement);
      this.leftPanel = document.createElement('div');
      this.leftPanel.classList.add('status-bar-left');
      flexboxHackElement.appendChild(this.leftPanel);
      this.element.leftPanel = this.leftPanel;
      this.rightPanel = document.createElement('div');
      this.rightPanel.classList.add('status-bar-right');
      flexboxHackElement.appendChild(this.rightPanel);
      this.element.rightPanel = this.rightPanel;
      this.leftTiles = [];
      this.rightTiles = [];
      this.element.getLeftTiles = this.getLeftTiles.bind(this);
      this.element.getRightTiles = this.getRightTiles.bind(this);
      this.element.addLeftTile = this.addLeftTile.bind(this);
      this.element.addRightTile = this.addRightTile.bind(this);
      this.bufferSubscriptions = [];
      this.activeItemSubscription = atom.workspace.getCenter().onDidChangeActivePaneItem((function(_this) {
        return function() {
          _this.unsubscribeAllFromBuffer();
          _this.storeActiveBuffer();
          _this.subscribeAllToBuffer();
          return _this.element.dispatchEvent(new CustomEvent('active-buffer-changed', {
            bubbles: true
          }));
        };
      })(this));
      this.storeActiveBuffer();
    }

    StatusBarView.prototype.destroy = function() {
      this.activeItemSubscription.dispose();
      this.unsubscribeAllFromBuffer();
      return this.element.remove();
    };

    StatusBarView.prototype.addLeftTile = function(options) {
      var i, index, item, len, newElement, newItem, newPriority, newTile, nextElement, nextItem, priority, ref, ref1, ref2;
      newItem = options.item;
      newPriority = (ref = options != null ? options.priority : void 0) != null ? ref : this.leftTiles[this.leftTiles.length - 1].priority + 1;
      nextItem = null;
      ref1 = this.leftTiles;
      for (index = i = 0, len = ref1.length; i < len; index = ++i) {
        ref2 = ref1[index], priority = ref2.priority, item = ref2.item;
        if (priority > newPriority) {
          nextItem = item;
          break;
        }
      }
      newTile = new Tile(newItem, newPriority, this.leftTiles);
      this.leftTiles.splice(index, 0, newTile);
      newElement = atom.views.getView(newItem);
      nextElement = atom.views.getView(nextItem);
      this.leftPanel.insertBefore(newElement, nextElement);
      return newTile;
    };

    StatusBarView.prototype.addRightTile = function(options) {
      var i, index, item, len, newElement, newItem, newPriority, newTile, nextElement, nextItem, priority, ref, ref1, ref2;
      newItem = options.item;
      newPriority = (ref = options != null ? options.priority : void 0) != null ? ref : this.rightTiles[0].priority + 1;
      nextItem = null;
      ref1 = this.rightTiles;
      for (index = i = 0, len = ref1.length; i < len; index = ++i) {
        ref2 = ref1[index], priority = ref2.priority, item = ref2.item;
        if (priority < newPriority) {
          nextItem = item;
          break;
        }
      }
      newTile = new Tile(newItem, newPriority, this.rightTiles);
      this.rightTiles.splice(index, 0, newTile);
      newElement = atom.views.getView(newItem);
      nextElement = atom.views.getView(nextItem);
      this.rightPanel.insertBefore(newElement, nextElement);
      return newTile;
    };

    StatusBarView.prototype.getLeftTiles = function() {
      return this.leftTiles;
    };

    StatusBarView.prototype.getRightTiles = function() {
      return this.rightTiles;
    };

    StatusBarView.prototype.getActiveBuffer = function() {
      return this.buffer;
    };

    StatusBarView.prototype.getActiveItem = function() {
      return atom.workspace.getCenter().getActivePaneItem();
    };

    StatusBarView.prototype.storeActiveBuffer = function() {
      var ref;
      return this.buffer = (ref = this.getActiveItem()) != null ? typeof ref.getBuffer === "function" ? ref.getBuffer() : void 0 : void 0;
    };

    StatusBarView.prototype.subscribeToBuffer = function(event, callback) {
      this.bufferSubscriptions.push([event, callback]);
      if (this.buffer) {
        return this.buffer.on(event, callback);
      }
    };

    StatusBarView.prototype.subscribeAllToBuffer = function() {
      var callback, event, i, len, ref, ref1, results;
      if (!this.buffer) {
        return;
      }
      ref = this.bufferSubscriptions;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        ref1 = ref[i], event = ref1[0], callback = ref1[1];
        results.push(this.buffer.on(event, callback));
      }
      return results;
    };

    StatusBarView.prototype.unsubscribeAllFromBuffer = function() {
      var callback, event, i, len, ref, ref1, results;
      if (!this.buffer) {
        return;
      }
      ref = this.bufferSubscriptions;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        ref1 = ref[i], event = ref1[0], callback = ref1[1];
        results.push(this.buffer.off(event, callback));
      }
      return results;
    };

    return StatusBarView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zdGF0dXMtYmFyL2xpYi9zdGF0dXMtYmFyLXZpZXcuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQyxhQUFjLE9BQUEsQ0FBUSxNQUFSOztFQUNmLElBQUEsR0FBTyxPQUFBLENBQVEsUUFBUjs7RUFFUCxNQUFNLENBQUMsT0FBUCxHQUNNO0lBQ1MsdUJBQUE7QUFDWCxVQUFBO01BQUEsSUFBQyxDQUFBLE9BQUQsR0FBVyxRQUFRLENBQUMsYUFBVCxDQUF1QixZQUF2QjtNQUNYLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLFlBQXZCO01BRUEsa0JBQUEsR0FBcUIsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDckIsa0JBQWtCLENBQUMsU0FBUyxDQUFDLEdBQTdCLENBQWlDLHNCQUFqQztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixrQkFBckI7TUFFQSxJQUFDLENBQUEsU0FBRCxHQUFhLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO01BQ2IsSUFBQyxDQUFBLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBckIsQ0FBeUIsaUJBQXpCO01BQ0Esa0JBQWtCLENBQUMsV0FBbkIsQ0FBK0IsSUFBQyxDQUFBLFNBQWhDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFULEdBQXFCLElBQUMsQ0FBQTtNQUV0QixJQUFDLENBQUEsVUFBRCxHQUFjLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO01BQ2QsSUFBQyxDQUFBLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBdEIsQ0FBMEIsa0JBQTFCO01BQ0Esa0JBQWtCLENBQUMsV0FBbkIsQ0FBK0IsSUFBQyxDQUFBLFVBQWhDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxVQUFULEdBQXNCLElBQUMsQ0FBQTtNQUV2QixJQUFDLENBQUEsU0FBRCxHQUFhO01BQ2IsSUFBQyxDQUFBLFVBQUQsR0FBYztNQUVkLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFBVCxHQUF3QixJQUFDLENBQUEsWUFBWSxDQUFDLElBQWQsQ0FBbUIsSUFBbkI7TUFDeEIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxhQUFULEdBQXlCLElBQUMsQ0FBQSxhQUFhLENBQUMsSUFBZixDQUFvQixJQUFwQjtNQUN6QixJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsR0FBdUIsSUFBQyxDQUFBLFdBQVcsQ0FBQyxJQUFiLENBQWtCLElBQWxCO01BQ3ZCLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFBVCxHQUF3QixJQUFDLENBQUEsWUFBWSxDQUFDLElBQWQsQ0FBbUIsSUFBbkI7TUFFeEIsSUFBQyxDQUFBLG1CQUFELEdBQXVCO01BRXZCLElBQUMsQ0FBQSxzQkFBRCxHQUEwQixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQWYsQ0FBQSxDQUEwQixDQUFDLHlCQUEzQixDQUFxRCxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDN0UsS0FBQyxDQUFBLHdCQUFELENBQUE7VUFDQSxLQUFDLENBQUEsaUJBQUQsQ0FBQTtVQUNBLEtBQUMsQ0FBQSxvQkFBRCxDQUFBO2lCQUVBLEtBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxDQUF1QixJQUFJLFdBQUosQ0FBZ0IsdUJBQWhCLEVBQXlDO1lBQUEsT0FBQSxFQUFTLElBQVQ7V0FBekMsQ0FBdkI7UUFMNkU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXJEO01BTzFCLElBQUMsQ0FBQSxpQkFBRCxDQUFBO0lBbkNXOzs0QkFxQ2IsT0FBQSxHQUFTLFNBQUE7TUFDUCxJQUFDLENBQUEsc0JBQXNCLENBQUMsT0FBeEIsQ0FBQTtNQUNBLElBQUMsQ0FBQSx3QkFBRCxDQUFBO2FBQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxNQUFULENBQUE7SUFITzs7NEJBS1QsV0FBQSxHQUFhLFNBQUMsT0FBRDtBQUNYLFVBQUE7TUFBQSxPQUFBLEdBQVUsT0FBTyxDQUFDO01BQ2xCLFdBQUEsdUVBQWtDLElBQUMsQ0FBQSxTQUFVLENBQUEsSUFBQyxDQUFBLFNBQVMsQ0FBQyxNQUFYLEdBQW9CLENBQXBCLENBQXNCLENBQUMsUUFBbEMsR0FBNkM7TUFDL0UsUUFBQSxHQUFXO0FBQ1g7QUFBQSxXQUFBLHNEQUFBOzRCQUFLLDBCQUFVO1FBQ2IsSUFBRyxRQUFBLEdBQVcsV0FBZDtVQUNFLFFBQUEsR0FBVztBQUNYLGdCQUZGOztBQURGO01BS0EsT0FBQSxHQUFVLElBQUksSUFBSixDQUFTLE9BQVQsRUFBa0IsV0FBbEIsRUFBK0IsSUFBQyxDQUFBLFNBQWhDO01BQ1YsSUFBQyxDQUFBLFNBQVMsQ0FBQyxNQUFYLENBQWtCLEtBQWxCLEVBQXlCLENBQXpCLEVBQTRCLE9BQTVCO01BQ0EsVUFBQSxHQUFhLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBWCxDQUFtQixPQUFuQjtNQUNiLFdBQUEsR0FBYyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVgsQ0FBbUIsUUFBbkI7TUFDZCxJQUFDLENBQUEsU0FBUyxDQUFDLFlBQVgsQ0FBd0IsVUFBeEIsRUFBb0MsV0FBcEM7YUFDQTtJQWRXOzs0QkFnQmIsWUFBQSxHQUFjLFNBQUMsT0FBRDtBQUNaLFVBQUE7TUFBQSxPQUFBLEdBQVUsT0FBTyxDQUFDO01BQ2xCLFdBQUEsdUVBQWtDLElBQUMsQ0FBQSxVQUFXLENBQUEsQ0FBQSxDQUFFLENBQUMsUUFBZixHQUEwQjtNQUM1RCxRQUFBLEdBQVc7QUFDWDtBQUFBLFdBQUEsc0RBQUE7NEJBQUssMEJBQVU7UUFDYixJQUFHLFFBQUEsR0FBVyxXQUFkO1VBQ0UsUUFBQSxHQUFXO0FBQ1gsZ0JBRkY7O0FBREY7TUFLQSxPQUFBLEdBQVUsSUFBSSxJQUFKLENBQVMsT0FBVCxFQUFrQixXQUFsQixFQUErQixJQUFDLENBQUEsVUFBaEM7TUFDVixJQUFDLENBQUEsVUFBVSxDQUFDLE1BQVosQ0FBbUIsS0FBbkIsRUFBMEIsQ0FBMUIsRUFBNkIsT0FBN0I7TUFDQSxVQUFBLEdBQWEsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFYLENBQW1CLE9BQW5CO01BQ2IsV0FBQSxHQUFjLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBWCxDQUFtQixRQUFuQjtNQUNkLElBQUMsQ0FBQSxVQUFVLENBQUMsWUFBWixDQUF5QixVQUF6QixFQUFxQyxXQUFyQzthQUNBO0lBZFk7OzRCQWdCZCxZQUFBLEdBQWMsU0FBQTthQUNaLElBQUMsQ0FBQTtJQURXOzs0QkFHZCxhQUFBLEdBQWUsU0FBQTthQUNiLElBQUMsQ0FBQTtJQURZOzs0QkFHZixlQUFBLEdBQWlCLFNBQUE7YUFDZixJQUFDLENBQUE7SUFEYzs7NEJBR2pCLGFBQUEsR0FBZSxTQUFBO2FBQ2IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFmLENBQUEsQ0FBMEIsQ0FBQyxpQkFBM0IsQ0FBQTtJQURhOzs0QkFHZixpQkFBQSxHQUFtQixTQUFBO0FBQ2pCLFVBQUE7YUFBQSxJQUFDLENBQUEsTUFBRCxtRkFBMEIsQ0FBRTtJQURYOzs0QkFHbkIsaUJBQUEsR0FBbUIsU0FBQyxLQUFELEVBQVEsUUFBUjtNQUNqQixJQUFDLENBQUEsbUJBQW1CLENBQUMsSUFBckIsQ0FBMEIsQ0FBQyxLQUFELEVBQVEsUUFBUixDQUExQjtNQUNBLElBQStCLElBQUMsQ0FBQSxNQUFoQztlQUFBLElBQUMsQ0FBQSxNQUFNLENBQUMsRUFBUixDQUFXLEtBQVgsRUFBa0IsUUFBbEIsRUFBQTs7SUFGaUI7OzRCQUluQixvQkFBQSxHQUFzQixTQUFBO0FBQ3BCLFVBQUE7TUFBQSxJQUFBLENBQWMsSUFBQyxDQUFBLE1BQWY7QUFBQSxlQUFBOztBQUNBO0FBQUE7V0FBQSxxQ0FBQTt1QkFBSyxpQkFBTztxQkFDVixJQUFDLENBQUEsTUFBTSxDQUFDLEVBQVIsQ0FBVyxLQUFYLEVBQWtCLFFBQWxCO0FBREY7O0lBRm9COzs0QkFLdEIsd0JBQUEsR0FBMEIsU0FBQTtBQUN4QixVQUFBO01BQUEsSUFBQSxDQUFjLElBQUMsQ0FBQSxNQUFmO0FBQUEsZUFBQTs7QUFDQTtBQUFBO1dBQUEscUNBQUE7dUJBQUssaUJBQU87cUJBQ1YsSUFBQyxDQUFBLE1BQU0sQ0FBQyxHQUFSLENBQVksS0FBWixFQUFtQixRQUFuQjtBQURGOztJQUZ3Qjs7Ozs7QUF2RzVCIiwic291cmNlc0NvbnRlbnQiOlsie0Rpc3Bvc2FibGV9ID0gcmVxdWlyZSAnYXRvbSdcblRpbGUgPSByZXF1aXJlICcuL3RpbGUnXG5cbm1vZHVsZS5leHBvcnRzID1cbmNsYXNzIFN0YXR1c0JhclZpZXdcbiAgY29uc3RydWN0b3I6IC0+XG4gICAgQGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdGF0dXMtYmFyJylcbiAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdzdGF0dXMtYmFyJylcblxuICAgIGZsZXhib3hIYWNrRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgZmxleGJveEhhY2tFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2ZsZXhib3gtcmVwYWludC1oYWNrJylcbiAgICBAZWxlbWVudC5hcHBlbmRDaGlsZChmbGV4Ym94SGFja0VsZW1lbnQpXG5cbiAgICBAbGVmdFBhbmVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICBAbGVmdFBhbmVsLmNsYXNzTGlzdC5hZGQoJ3N0YXR1cy1iYXItbGVmdCcpXG4gICAgZmxleGJveEhhY2tFbGVtZW50LmFwcGVuZENoaWxkKEBsZWZ0UGFuZWwpXG4gICAgQGVsZW1lbnQubGVmdFBhbmVsID0gQGxlZnRQYW5lbFxuXG4gICAgQHJpZ2h0UGFuZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIEByaWdodFBhbmVsLmNsYXNzTGlzdC5hZGQoJ3N0YXR1cy1iYXItcmlnaHQnKVxuICAgIGZsZXhib3hIYWNrRWxlbWVudC5hcHBlbmRDaGlsZChAcmlnaHRQYW5lbClcbiAgICBAZWxlbWVudC5yaWdodFBhbmVsID0gQHJpZ2h0UGFuZWxcblxuICAgIEBsZWZ0VGlsZXMgPSBbXVxuICAgIEByaWdodFRpbGVzID0gW11cblxuICAgIEBlbGVtZW50LmdldExlZnRUaWxlcyA9IEBnZXRMZWZ0VGlsZXMuYmluZCh0aGlzKVxuICAgIEBlbGVtZW50LmdldFJpZ2h0VGlsZXMgPSBAZ2V0UmlnaHRUaWxlcy5iaW5kKHRoaXMpXG4gICAgQGVsZW1lbnQuYWRkTGVmdFRpbGUgPSBAYWRkTGVmdFRpbGUuYmluZCh0aGlzKVxuICAgIEBlbGVtZW50LmFkZFJpZ2h0VGlsZSA9IEBhZGRSaWdodFRpbGUuYmluZCh0aGlzKVxuXG4gICAgQGJ1ZmZlclN1YnNjcmlwdGlvbnMgPSBbXVxuXG4gICAgQGFjdGl2ZUl0ZW1TdWJzY3JpcHRpb24gPSBhdG9tLndvcmtzcGFjZS5nZXRDZW50ZXIoKS5vbkRpZENoYW5nZUFjdGl2ZVBhbmVJdGVtID0+XG4gICAgICBAdW5zdWJzY3JpYmVBbGxGcm9tQnVmZmVyKClcbiAgICAgIEBzdG9yZUFjdGl2ZUJ1ZmZlcigpXG4gICAgICBAc3Vic2NyaWJlQWxsVG9CdWZmZXIoKVxuXG4gICAgICBAZWxlbWVudC5kaXNwYXRjaEV2ZW50KG5ldyBDdXN0b21FdmVudCgnYWN0aXZlLWJ1ZmZlci1jaGFuZ2VkJywgYnViYmxlczogdHJ1ZSkpXG5cbiAgICBAc3RvcmVBY3RpdmVCdWZmZXIoKVxuXG4gIGRlc3Ryb3k6IC0+XG4gICAgQGFjdGl2ZUl0ZW1TdWJzY3JpcHRpb24uZGlzcG9zZSgpXG4gICAgQHVuc3Vic2NyaWJlQWxsRnJvbUJ1ZmZlcigpXG4gICAgQGVsZW1lbnQucmVtb3ZlKClcblxuICBhZGRMZWZ0VGlsZTogKG9wdGlvbnMpIC0+XG4gICAgbmV3SXRlbSA9IG9wdGlvbnMuaXRlbVxuICAgIG5ld1ByaW9yaXR5ID0gb3B0aW9ucz8ucHJpb3JpdHkgPyBAbGVmdFRpbGVzW0BsZWZ0VGlsZXMubGVuZ3RoIC0gMV0ucHJpb3JpdHkgKyAxXG4gICAgbmV4dEl0ZW0gPSBudWxsXG4gICAgZm9yIHtwcmlvcml0eSwgaXRlbX0sIGluZGV4IGluIEBsZWZ0VGlsZXNcbiAgICAgIGlmIHByaW9yaXR5ID4gbmV3UHJpb3JpdHlcbiAgICAgICAgbmV4dEl0ZW0gPSBpdGVtXG4gICAgICAgIGJyZWFrXG5cbiAgICBuZXdUaWxlID0gbmV3IFRpbGUobmV3SXRlbSwgbmV3UHJpb3JpdHksIEBsZWZ0VGlsZXMpXG4gICAgQGxlZnRUaWxlcy5zcGxpY2UoaW5kZXgsIDAsIG5ld1RpbGUpXG4gICAgbmV3RWxlbWVudCA9IGF0b20udmlld3MuZ2V0VmlldyhuZXdJdGVtKVxuICAgIG5leHRFbGVtZW50ID0gYXRvbS52aWV3cy5nZXRWaWV3KG5leHRJdGVtKVxuICAgIEBsZWZ0UGFuZWwuaW5zZXJ0QmVmb3JlKG5ld0VsZW1lbnQsIG5leHRFbGVtZW50KVxuICAgIG5ld1RpbGVcblxuICBhZGRSaWdodFRpbGU6IChvcHRpb25zKSAtPlxuICAgIG5ld0l0ZW0gPSBvcHRpb25zLml0ZW1cbiAgICBuZXdQcmlvcml0eSA9IG9wdGlvbnM/LnByaW9yaXR5ID8gQHJpZ2h0VGlsZXNbMF0ucHJpb3JpdHkgKyAxXG4gICAgbmV4dEl0ZW0gPSBudWxsXG4gICAgZm9yIHtwcmlvcml0eSwgaXRlbX0sIGluZGV4IGluIEByaWdodFRpbGVzXG4gICAgICBpZiBwcmlvcml0eSA8IG5ld1ByaW9yaXR5XG4gICAgICAgIG5leHRJdGVtID0gaXRlbVxuICAgICAgICBicmVha1xuXG4gICAgbmV3VGlsZSA9IG5ldyBUaWxlKG5ld0l0ZW0sIG5ld1ByaW9yaXR5LCBAcmlnaHRUaWxlcylcbiAgICBAcmlnaHRUaWxlcy5zcGxpY2UoaW5kZXgsIDAsIG5ld1RpbGUpXG4gICAgbmV3RWxlbWVudCA9IGF0b20udmlld3MuZ2V0VmlldyhuZXdJdGVtKVxuICAgIG5leHRFbGVtZW50ID0gYXRvbS52aWV3cy5nZXRWaWV3KG5leHRJdGVtKVxuICAgIEByaWdodFBhbmVsLmluc2VydEJlZm9yZShuZXdFbGVtZW50LCBuZXh0RWxlbWVudClcbiAgICBuZXdUaWxlXG5cbiAgZ2V0TGVmdFRpbGVzOiAtPlxuICAgIEBsZWZ0VGlsZXNcblxuICBnZXRSaWdodFRpbGVzOiAtPlxuICAgIEByaWdodFRpbGVzXG5cbiAgZ2V0QWN0aXZlQnVmZmVyOiAtPlxuICAgIEBidWZmZXJcblxuICBnZXRBY3RpdmVJdGVtOiAtPlxuICAgIGF0b20ud29ya3NwYWNlLmdldENlbnRlcigpLmdldEFjdGl2ZVBhbmVJdGVtKClcblxuICBzdG9yZUFjdGl2ZUJ1ZmZlcjogLT5cbiAgICBAYnVmZmVyID0gQGdldEFjdGl2ZUl0ZW0oKT8uZ2V0QnVmZmVyPygpXG5cbiAgc3Vic2NyaWJlVG9CdWZmZXI6IChldmVudCwgY2FsbGJhY2spIC0+XG4gICAgQGJ1ZmZlclN1YnNjcmlwdGlvbnMucHVzaChbZXZlbnQsIGNhbGxiYWNrXSlcbiAgICBAYnVmZmVyLm9uKGV2ZW50LCBjYWxsYmFjaykgaWYgQGJ1ZmZlclxuXG4gIHN1YnNjcmliZUFsbFRvQnVmZmVyOiAtPlxuICAgIHJldHVybiB1bmxlc3MgQGJ1ZmZlclxuICAgIGZvciBbZXZlbnQsIGNhbGxiYWNrXSBpbiBAYnVmZmVyU3Vic2NyaXB0aW9uc1xuICAgICAgQGJ1ZmZlci5vbihldmVudCwgY2FsbGJhY2spXG5cbiAgdW5zdWJzY3JpYmVBbGxGcm9tQnVmZmVyOiAtPlxuICAgIHJldHVybiB1bmxlc3MgQGJ1ZmZlclxuICAgIGZvciBbZXZlbnQsIGNhbGxiYWNrXSBpbiBAYnVmZmVyU3Vic2NyaXB0aW9uc1xuICAgICAgQGJ1ZmZlci5vZmYoZXZlbnQsIGNhbGxiYWNrKVxuIl19
