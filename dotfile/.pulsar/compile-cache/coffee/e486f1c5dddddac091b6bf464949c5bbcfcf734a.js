(function() {
  var CompositeDisposable, DeprecationCopStatusBarView, Disposable, Grim, _, ref,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  ref = require('atom'), CompositeDisposable = ref.CompositeDisposable, Disposable = ref.Disposable;

  _ = require('underscore-plus');

  Grim = require('grim');

  module.exports = DeprecationCopStatusBarView = (function() {
    DeprecationCopStatusBarView.prototype.lastLength = null;

    DeprecationCopStatusBarView.prototype.toolTipDisposable = null;

    function DeprecationCopStatusBarView() {
      this.update = bind(this.update, this);
      var clickHandler, debouncedUpdateDeprecatedSelectorCount;
      this.subscriptions = new CompositeDisposable;
      this.element = document.createElement('div');
      this.element.classList.add('deprecation-cop-status', 'inline-block', 'text-warning');
      this.element.setAttribute('tabindex', -1);
      this.icon = document.createElement('span');
      this.icon.classList.add('icon', 'icon-alert');
      this.element.appendChild(this.icon);
      this.deprecationNumber = document.createElement('span');
      this.deprecationNumber.classList.add('deprecation-number');
      this.deprecationNumber.textContent = '0';
      this.element.appendChild(this.deprecationNumber);
      clickHandler = function() {
        var workspaceElement;
        workspaceElement = atom.views.getView(atom.workspace);
        return atom.commands.dispatch(workspaceElement, 'deprecation-cop:view');
      };
      this.element.addEventListener('click', clickHandler);
      this.subscriptions.add(new Disposable((function(_this) {
        return function() {
          return _this.element.removeEventListener('click', clickHandler);
        };
      })(this)));
      this.update();
      debouncedUpdateDeprecatedSelectorCount = _.debounce(this.update, 1000);
      this.subscriptions.add(Grim.on('updated', this.update));
      if (atom.styles.onDidUpdateDeprecations != null) {
        this.subscriptions.add(atom.styles.onDidUpdateDeprecations(debouncedUpdateDeprecatedSelectorCount));
      }
    }

    DeprecationCopStatusBarView.prototype.destroy = function() {
      this.subscriptions.dispose();
      return this.element.remove();
    };

    DeprecationCopStatusBarView.prototype.getDeprecatedCallCount = function() {
      return Grim.getDeprecations().map(function(d) {
        return d.getStackCount();
      }).reduce((function(a, b) {
        return a + b;
      }), 0);
    };

    DeprecationCopStatusBarView.prototype.getDeprecatedStyleSheetsCount = function() {
      if (atom.styles.getDeprecations != null) {
        return Object.keys(atom.styles.getDeprecations()).length;
      } else {
        return 0;
      }
    };

    DeprecationCopStatusBarView.prototype.update = function() {
      var length, ref1;
      length = this.getDeprecatedCallCount() + this.getDeprecatedStyleSheetsCount();
      if (this.lastLength === length) {
        return;
      }
      this.lastLength = length;
      this.deprecationNumber.textContent = "" + (_.pluralize(length, 'deprecation'));
      if ((ref1 = this.toolTipDisposable) != null) {
        ref1.dispose();
      }
      this.toolTipDisposable = atom.tooltips.add(this.element, {
        title: (_.pluralize(length, 'call')) + " to deprecated methods"
      });
      if (length === 0) {
        return this.element.style.display = 'none';
      } else {
        return this.element.style.display = '';
      }
    };

    return DeprecationCopStatusBarView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9kZXByZWNhdGlvbi1jb3AvbGliL2RlcHJlY2F0aW9uLWNvcC1zdGF0dXMtYmFyLXZpZXcuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQSwwRUFBQTtJQUFBOztFQUFBLE1BQW9DLE9BQUEsQ0FBUSxNQUFSLENBQXBDLEVBQUMsNkNBQUQsRUFBc0I7O0VBQ3RCLENBQUEsR0FBSSxPQUFBLENBQVEsaUJBQVI7O0VBQ0osSUFBQSxHQUFPLE9BQUEsQ0FBUSxNQUFSOztFQUVQLE1BQU0sQ0FBQyxPQUFQLEdBQ007MENBQ0osVUFBQSxHQUFZOzswQ0FDWixpQkFBQSxHQUFtQjs7SUFFTixxQ0FBQTs7QUFDWCxVQUFBO01BQUEsSUFBQyxDQUFBLGFBQUQsR0FBaUIsSUFBSTtNQUVyQixJQUFDLENBQUEsT0FBRCxHQUFXLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO01BQ1gsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsd0JBQXZCLEVBQWlELGNBQWpELEVBQWlFLGNBQWpFO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxZQUFULENBQXNCLFVBQXRCLEVBQWtDLENBQUMsQ0FBbkM7TUFFQSxJQUFDLENBQUEsSUFBRCxHQUFRLFFBQVEsQ0FBQyxhQUFULENBQXVCLE1BQXZCO01BQ1IsSUFBQyxDQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBaEIsQ0FBb0IsTUFBcEIsRUFBNEIsWUFBNUI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsQ0FBcUIsSUFBQyxDQUFBLElBQXRCO01BRUEsSUFBQyxDQUFBLGlCQUFELEdBQXFCLFFBQVEsQ0FBQyxhQUFULENBQXVCLE1BQXZCO01BQ3JCLElBQUMsQ0FBQSxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBN0IsQ0FBaUMsb0JBQWpDO01BQ0EsSUFBQyxDQUFBLGlCQUFpQixDQUFDLFdBQW5CLEdBQWlDO01BQ2pDLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixJQUFDLENBQUEsaUJBQXRCO01BRUEsWUFBQSxHQUFlLFNBQUE7QUFDYixZQUFBO1FBQUEsZ0JBQUEsR0FBbUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFYLENBQW1CLElBQUksQ0FBQyxTQUF4QjtlQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQWQsQ0FBdUIsZ0JBQXZCLEVBQXlDLHNCQUF6QztNQUZhO01BR2YsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxZQUFuQztNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLFVBQUosQ0FBZSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQU8sQ0FBQyxtQkFBVCxDQUE2QixPQUE3QixFQUFzQyxZQUF0QztRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFmLENBQW5CO01BRUEsSUFBQyxDQUFBLE1BQUQsQ0FBQTtNQUVBLHNDQUFBLEdBQXlDLENBQUMsQ0FBQyxRQUFGLENBQVcsSUFBQyxDQUFBLE1BQVosRUFBb0IsSUFBcEI7TUFFekMsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxFQUFMLENBQVEsU0FBUixFQUFtQixJQUFDLENBQUEsTUFBcEIsQ0FBbkI7TUFFQSxJQUFHLDJDQUFIO1FBQ0UsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxNQUFNLENBQUMsdUJBQVosQ0FBb0Msc0NBQXBDLENBQW5CLEVBREY7O0lBNUJXOzswQ0ErQmIsT0FBQSxHQUFTLFNBQUE7TUFDUCxJQUFDLENBQUEsYUFBYSxDQUFDLE9BQWYsQ0FBQTthQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsTUFBVCxDQUFBO0lBRk87OzBDQUlULHNCQUFBLEdBQXdCLFNBQUE7YUFDdEIsSUFBSSxDQUFDLGVBQUwsQ0FBQSxDQUFzQixDQUFDLEdBQXZCLENBQTJCLFNBQUMsQ0FBRDtlQUFPLENBQUMsQ0FBQyxhQUFGLENBQUE7TUFBUCxDQUEzQixDQUFvRCxDQUFDLE1BQXJELENBQTRELENBQUMsU0FBQyxDQUFELEVBQUksQ0FBSjtlQUFVLENBQUEsR0FBSTtNQUFkLENBQUQsQ0FBNUQsRUFBK0UsQ0FBL0U7SUFEc0I7OzBDQUd4Qiw2QkFBQSxHQUErQixTQUFBO01BRTdCLElBQUcsbUNBQUg7ZUFDRSxNQUFNLENBQUMsSUFBUCxDQUFZLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBWixDQUFBLENBQVosQ0FBMEMsQ0FBQyxPQUQ3QztPQUFBLE1BQUE7ZUFHRSxFQUhGOztJQUY2Qjs7MENBTy9CLE1BQUEsR0FBUSxTQUFBO0FBQ04sVUFBQTtNQUFBLE1BQUEsR0FBUyxJQUFDLENBQUEsc0JBQUQsQ0FBQSxDQUFBLEdBQTRCLElBQUMsQ0FBQSw2QkFBRCxDQUFBO01BRXJDLElBQVUsSUFBQyxDQUFBLFVBQUQsS0FBZSxNQUF6QjtBQUFBLGVBQUE7O01BRUEsSUFBQyxDQUFBLFVBQUQsR0FBYztNQUNkLElBQUMsQ0FBQSxpQkFBaUIsQ0FBQyxXQUFuQixHQUFpQyxFQUFBLEdBQUUsQ0FBQyxDQUFDLENBQUMsU0FBRixDQUFZLE1BQVosRUFBb0IsYUFBcEIsQ0FBRDs7WUFDakIsQ0FBRSxPQUFwQixDQUFBOztNQUNBLElBQUMsQ0FBQSxpQkFBRCxHQUFxQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsSUFBQyxDQUFBLE9BQW5CLEVBQTRCO1FBQUEsS0FBQSxFQUFTLENBQUMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxNQUFaLEVBQW9CLE1BQXBCLENBQUQsQ0FBQSxHQUE2Qix3QkFBdEM7T0FBNUI7TUFFckIsSUFBRyxNQUFBLEtBQVUsQ0FBYjtlQUNFLElBQUMsQ0FBQSxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQWYsR0FBeUIsT0FEM0I7T0FBQSxNQUFBO2VBR0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBZixHQUF5QixHQUgzQjs7SUFWTTs7Ozs7QUF0RFYiLCJzb3VyY2VzQ29udGVudCI6WyJ7Q29tcG9zaXRlRGlzcG9zYWJsZSwgRGlzcG9zYWJsZX0gPSByZXF1aXJlICdhdG9tJ1xuXyA9IHJlcXVpcmUgJ3VuZGVyc2NvcmUtcGx1cydcbkdyaW0gPSByZXF1aXJlICdncmltJ1xuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBEZXByZWNhdGlvbkNvcFN0YXR1c0JhclZpZXdcbiAgbGFzdExlbmd0aDogbnVsbFxuICB0b29sVGlwRGlzcG9zYWJsZTogbnVsbFxuXG4gIGNvbnN0cnVjdG9yOiAtPlxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcblxuICAgIEBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkZXByZWNhdGlvbi1jb3Atc3RhdHVzJywgJ2lubGluZS1ibG9jaycsICd0ZXh0LXdhcm5pbmcnKVxuICAgIEBlbGVtZW50LnNldEF0dHJpYnV0ZSgndGFiaW5kZXgnLCAtMSlcblxuICAgIEBpY29uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpXG4gICAgQGljb24uY2xhc3NMaXN0LmFkZCgnaWNvbicsICdpY29uLWFsZXJ0JylcbiAgICBAZWxlbWVudC5hcHBlbmRDaGlsZChAaWNvbilcblxuICAgIEBkZXByZWNhdGlvbk51bWJlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKVxuICAgIEBkZXByZWNhdGlvbk51bWJlci5jbGFzc0xpc3QuYWRkKCdkZXByZWNhdGlvbi1udW1iZXInKVxuICAgIEBkZXByZWNhdGlvbk51bWJlci50ZXh0Q29udGVudCA9ICcwJ1xuICAgIEBlbGVtZW50LmFwcGVuZENoaWxkKEBkZXByZWNhdGlvbk51bWJlcilcblxuICAgIGNsaWNrSGFuZGxlciA9IC0+XG4gICAgICB3b3Jrc3BhY2VFbGVtZW50ID0gYXRvbS52aWV3cy5nZXRWaWV3KGF0b20ud29ya3NwYWNlKVxuICAgICAgYXRvbS5jb21tYW5kcy5kaXNwYXRjaCB3b3Jrc3BhY2VFbGVtZW50LCAnZGVwcmVjYXRpb24tY29wOnZpZXcnXG4gICAgQGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBjbGlja0hhbmRsZXIpXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkKG5ldyBEaXNwb3NhYmxlKD0+IEBlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2xpY2tIYW5kbGVyKSkpXG5cbiAgICBAdXBkYXRlKClcblxuICAgIGRlYm91bmNlZFVwZGF0ZURlcHJlY2F0ZWRTZWxlY3RvckNvdW50ID0gXy5kZWJvdW5jZShAdXBkYXRlLCAxMDAwKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIEdyaW0ub24gJ3VwZGF0ZWQnLCBAdXBkYXRlXG4gICAgIyBUT0RPOiBSZW1vdmUgY29uZGl0aW9uYWwgd2hlbiB0aGUgbmV3IFN0eWxlTWFuYWdlciBkZXByZWNhdGlvbiBBUElzIHJlYWNoIHN0YWJsZS5cbiAgICBpZiBhdG9tLnN0eWxlcy5vbkRpZFVwZGF0ZURlcHJlY2F0aW9ucz9cbiAgICAgIEBzdWJzY3JpcHRpb25zLmFkZChhdG9tLnN0eWxlcy5vbkRpZFVwZGF0ZURlcHJlY2F0aW9ucyhkZWJvdW5jZWRVcGRhdGVEZXByZWNhdGVkU2VsZWN0b3JDb3VudCkpXG5cbiAgZGVzdHJveTogLT5cbiAgICBAc3Vic2NyaXB0aW9ucy5kaXNwb3NlKClcbiAgICBAZWxlbWVudC5yZW1vdmUoKVxuXG4gIGdldERlcHJlY2F0ZWRDYWxsQ291bnQ6IC0+XG4gICAgR3JpbS5nZXREZXByZWNhdGlvbnMoKS5tYXAoKGQpIC0+IGQuZ2V0U3RhY2tDb3VudCgpKS5yZWR1Y2UoKChhLCBiKSAtPiBhICsgYiksIDApXG5cbiAgZ2V0RGVwcmVjYXRlZFN0eWxlU2hlZXRzQ291bnQ6IC0+XG4gICAgIyBUT0RPOiBSZW1vdmUgY29uZGl0aW9uYWwgd2hlbiB0aGUgbmV3IFN0eWxlTWFuYWdlciBkZXByZWNhdGlvbiBBUElzIHJlYWNoIHN0YWJsZS5cbiAgICBpZiBhdG9tLnN0eWxlcy5nZXREZXByZWNhdGlvbnM/XG4gICAgICBPYmplY3Qua2V5cyhhdG9tLnN0eWxlcy5nZXREZXByZWNhdGlvbnMoKSkubGVuZ3RoXG4gICAgZWxzZVxuICAgICAgMFxuXG4gIHVwZGF0ZTogPT5cbiAgICBsZW5ndGggPSBAZ2V0RGVwcmVjYXRlZENhbGxDb3VudCgpICsgQGdldERlcHJlY2F0ZWRTdHlsZVNoZWV0c0NvdW50KClcblxuICAgIHJldHVybiBpZiBAbGFzdExlbmd0aCBpcyBsZW5ndGhcblxuICAgIEBsYXN0TGVuZ3RoID0gbGVuZ3RoXG4gICAgQGRlcHJlY2F0aW9uTnVtYmVyLnRleHRDb250ZW50ID0gXCIje18ucGx1cmFsaXplKGxlbmd0aCwgJ2RlcHJlY2F0aW9uJyl9XCJcbiAgICBAdG9vbFRpcERpc3Bvc2FibGU/LmRpc3Bvc2UoKVxuICAgIEB0b29sVGlwRGlzcG9zYWJsZSA9IGF0b20udG9vbHRpcHMuYWRkIEBlbGVtZW50LCB0aXRsZTogXCIje18ucGx1cmFsaXplKGxlbmd0aCwgJ2NhbGwnKX0gdG8gZGVwcmVjYXRlZCBtZXRob2RzXCJcblxuICAgIGlmIGxlbmd0aCBpcyAwXG4gICAgICBAZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgZWxzZVxuICAgICAgQGVsZW1lbnQuc3R5bGUuZGlzcGxheSA9ICcnXG4iXX0=
