(function() {
  var CursorPositionView, Disposable;

  Disposable = require('atom').Disposable;

  module.exports = CursorPositionView = (function() {
    function CursorPositionView() {
      var ref;
      this.viewUpdatePending = false;
      this.element = document.createElement('status-bar-cursor');
      this.element.classList.add('cursor-position', 'inline-block');
      this.goToLineLink = document.createElement('a');
      this.goToLineLink.classList.add('inline-block');
      this.element.appendChild(this.goToLineLink);
      this.formatString = (ref = atom.config.get('status-bar.cursorPositionFormat')) != null ? ref : '%L:%C';
      this.activeItemSubscription = atom.workspace.onDidChangeActiveTextEditor((function(_this) {
        return function(activeEditor) {
          return _this.subscribeToActiveTextEditor();
        };
      })(this));
      this.subscribeToConfig();
      this.subscribeToActiveTextEditor();
      this.tooltip = atom.tooltips.add(this.element, {
        title: (function(_this) {
          return function() {
            return "Line " + _this.row + ", Column " + _this.column;
          };
        })(this)
      });
      this.handleClick();
    }

    CursorPositionView.prototype.destroy = function() {
      var ref, ref1, ref2;
      this.activeItemSubscription.dispose();
      if ((ref = this.cursorSubscription) != null) {
        ref.dispose();
      }
      this.tooltip.dispose();
      if ((ref1 = this.configSubscription) != null) {
        ref1.dispose();
      }
      this.clickSubscription.dispose();
      return (ref2 = this.updateSubscription) != null ? ref2.dispose() : void 0;
    };

    CursorPositionView.prototype.subscribeToActiveTextEditor = function() {
      var ref, ref1, selectionsMarkerLayer;
      if ((ref = this.cursorSubscription) != null) {
        ref.dispose();
      }
      selectionsMarkerLayer = (ref1 = atom.workspace.getActiveTextEditor()) != null ? ref1.selectionsMarkerLayer : void 0;
      this.cursorSubscription = selectionsMarkerLayer != null ? selectionsMarkerLayer.onDidUpdate(this.scheduleUpdate.bind(this)) : void 0;
      return this.scheduleUpdate();
    };

    CursorPositionView.prototype.subscribeToConfig = function() {
      var ref;
      if ((ref = this.configSubscription) != null) {
        ref.dispose();
      }
      return this.configSubscription = atom.config.observe('status-bar.cursorPositionFormat', (function(_this) {
        return function(value) {
          _this.formatString = value != null ? value : '%L:%C';
          return _this.scheduleUpdate();
        };
      })(this));
    };

    CursorPositionView.prototype.handleClick = function() {
      var clickHandler;
      clickHandler = function() {
        return atom.commands.dispatch(atom.views.getView(atom.workspace.getActiveTextEditor()), 'go-to-line:toggle');
      };
      this.element.addEventListener('click', clickHandler);
      return this.clickSubscription = new Disposable((function(_this) {
        return function() {
          return _this.element.removeEventListener('click', clickHandler);
        };
      })(this));
    };

    CursorPositionView.prototype.scheduleUpdate = function() {
      if (this.viewUpdatePending) {
        return;
      }
      this.viewUpdatePending = true;
      return this.updateSubscription = atom.views.updateDocument((function(_this) {
        return function() {
          var position, ref;
          _this.viewUpdatePending = false;
          if (position = (ref = atom.workspace.getActiveTextEditor()) != null ? ref.getCursorBufferPosition() : void 0) {
            _this.row = position.row + 1;
            _this.column = position.column + 1;
            _this.goToLineLink.textContent = _this.formatString.replace('%L', _this.row).replace('%C', _this.column);
            return _this.element.classList.remove('hide');
          } else {
            _this.goToLineLink.textContent = '';
            return _this.element.classList.add('hide');
          }
        };
      })(this));
    };

    return CursorPositionView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zdGF0dXMtYmFyL2xpYi9jdXJzb3ItcG9zaXRpb24tdmlldy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFDLGFBQWMsT0FBQSxDQUFRLE1BQVI7O0VBRWYsTUFBTSxDQUFDLE9BQVAsR0FDTTtJQUNTLDRCQUFBO0FBQ1gsVUFBQTtNQUFBLElBQUMsQ0FBQSxpQkFBRCxHQUFxQjtNQUVyQixJQUFDLENBQUEsT0FBRCxHQUFXLFFBQVEsQ0FBQyxhQUFULENBQXVCLG1CQUF2QjtNQUNYLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLGlCQUF2QixFQUEwQyxjQUExQztNQUNBLElBQUMsQ0FBQSxZQUFELEdBQWdCLFFBQVEsQ0FBQyxhQUFULENBQXVCLEdBQXZCO01BQ2hCLElBQUMsQ0FBQSxZQUFZLENBQUMsU0FBUyxDQUFDLEdBQXhCLENBQTRCLGNBQTVCO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLElBQUMsQ0FBQSxZQUF0QjtNQUVBLElBQUMsQ0FBQSxZQUFELDhFQUFxRTtNQUVyRSxJQUFDLENBQUEsc0JBQUQsR0FBMEIsSUFBSSxDQUFDLFNBQVMsQ0FBQywyQkFBZixDQUEyQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsWUFBRDtpQkFBa0IsS0FBQyxDQUFBLDJCQUFELENBQUE7UUFBbEI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTNDO01BRTFCLElBQUMsQ0FBQSxpQkFBRCxDQUFBO01BQ0EsSUFBQyxDQUFBLDJCQUFELENBQUE7TUFFQSxJQUFDLENBQUEsT0FBRCxHQUFXLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixJQUFDLENBQUEsT0FBbkIsRUFBNEI7UUFBQSxLQUFBLEVBQU8sQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxPQUFBLEdBQVEsS0FBQyxDQUFBLEdBQVQsR0FBYSxXQUFiLEdBQXdCLEtBQUMsQ0FBQTtVQUE1QjtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBUDtPQUE1QjtNQUVYLElBQUMsQ0FBQSxXQUFELENBQUE7SUFsQlc7O2lDQW9CYixPQUFBLEdBQVMsU0FBQTtBQUNQLFVBQUE7TUFBQSxJQUFDLENBQUEsc0JBQXNCLENBQUMsT0FBeEIsQ0FBQTs7V0FDbUIsQ0FBRSxPQUFyQixDQUFBOztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsT0FBVCxDQUFBOztZQUNtQixDQUFFLE9BQXJCLENBQUE7O01BQ0EsSUFBQyxDQUFBLGlCQUFpQixDQUFDLE9BQW5CLENBQUE7NERBQ21CLENBQUUsT0FBckIsQ0FBQTtJQU5POztpQ0FRVCwyQkFBQSxHQUE2QixTQUFBO0FBQzNCLFVBQUE7O1dBQW1CLENBQUUsT0FBckIsQ0FBQTs7TUFDQSxxQkFBQSwrREFBNEQsQ0FBRTtNQUM5RCxJQUFDLENBQUEsa0JBQUQsbUNBQXNCLHFCQUFxQixDQUFFLFdBQXZCLENBQW1DLElBQUMsQ0FBQSxjQUFjLENBQUMsSUFBaEIsQ0FBcUIsSUFBckIsQ0FBbkM7YUFDdEIsSUFBQyxDQUFBLGNBQUQsQ0FBQTtJQUoyQjs7aUNBTTdCLGlCQUFBLEdBQW1CLFNBQUE7QUFDakIsVUFBQTs7V0FBbUIsQ0FBRSxPQUFyQixDQUFBOzthQUNBLElBQUMsQ0FBQSxrQkFBRCxHQUFzQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQVosQ0FBb0IsaUNBQXBCLEVBQXVELENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxLQUFEO1VBQzNFLEtBQUMsQ0FBQSxZQUFELG1CQUFnQixRQUFRO2lCQUN4QixLQUFDLENBQUEsY0FBRCxDQUFBO1FBRjJFO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF2RDtJQUZMOztpQ0FNbkIsV0FBQSxHQUFhLFNBQUE7QUFDWCxVQUFBO01BQUEsWUFBQSxHQUFlLFNBQUE7ZUFBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQWQsQ0FBdUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFYLENBQW1CLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQWYsQ0FBQSxDQUFuQixDQUF2QixFQUFpRixtQkFBakY7TUFBSDtNQUNmLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsWUFBbkM7YUFDQSxJQUFDLENBQUEsaUJBQUQsR0FBcUIsSUFBSSxVQUFKLENBQWUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFPLENBQUMsbUJBQVQsQ0FBNkIsT0FBN0IsRUFBc0MsWUFBdEM7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBZjtJQUhWOztpQ0FLYixjQUFBLEdBQWdCLFNBQUE7TUFDZCxJQUFVLElBQUMsQ0FBQSxpQkFBWDtBQUFBLGVBQUE7O01BRUEsSUFBQyxDQUFBLGlCQUFELEdBQXFCO2FBQ3JCLElBQUMsQ0FBQSxrQkFBRCxHQUFzQixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQVgsQ0FBMEIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO0FBQzlDLGNBQUE7VUFBQSxLQUFDLENBQUEsaUJBQUQsR0FBcUI7VUFDckIsSUFBRyxRQUFBLDZEQUErQyxDQUFFLHVCQUF0QyxDQUFBLFVBQWQ7WUFDRSxLQUFDLENBQUEsR0FBRCxHQUFPLFFBQVEsQ0FBQyxHQUFULEdBQWU7WUFDdEIsS0FBQyxDQUFBLE1BQUQsR0FBVSxRQUFRLENBQUMsTUFBVCxHQUFrQjtZQUM1QixLQUFDLENBQUEsWUFBWSxDQUFDLFdBQWQsR0FBNEIsS0FBQyxDQUFBLFlBQVksQ0FBQyxPQUFkLENBQXNCLElBQXRCLEVBQTRCLEtBQUMsQ0FBQSxHQUE3QixDQUFpQyxDQUFDLE9BQWxDLENBQTBDLElBQTFDLEVBQWdELEtBQUMsQ0FBQSxNQUFqRDttQkFDNUIsS0FBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBbkIsQ0FBMEIsTUFBMUIsRUFKRjtXQUFBLE1BQUE7WUFNRSxLQUFDLENBQUEsWUFBWSxDQUFDLFdBQWQsR0FBNEI7bUJBQzVCLEtBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLE1BQXZCLEVBUEY7O1FBRjhDO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUExQjtJQUpSOzs7OztBQWpEbEIiLCJzb3VyY2VzQ29udGVudCI6WyJ7RGlzcG9zYWJsZX0gPSByZXF1aXJlICdhdG9tJ1xuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBDdXJzb3JQb3NpdGlvblZpZXdcbiAgY29uc3RydWN0b3I6IC0+XG4gICAgQHZpZXdVcGRhdGVQZW5kaW5nID0gZmFsc2VcblxuICAgIEBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3RhdHVzLWJhci1jdXJzb3InKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2N1cnNvci1wb3NpdGlvbicsICdpbmxpbmUtYmxvY2snKVxuICAgIEBnb1RvTGluZUxpbmsgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJylcbiAgICBAZ29Ub0xpbmVMaW5rLmNsYXNzTGlzdC5hZGQoJ2lubGluZS1ibG9jaycpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQGdvVG9MaW5lTGluaylcblxuICAgIEBmb3JtYXRTdHJpbmcgPSBhdG9tLmNvbmZpZy5nZXQoJ3N0YXR1cy1iYXIuY3Vyc29yUG9zaXRpb25Gb3JtYXQnKSA/ICclTDolQydcblxuICAgIEBhY3RpdmVJdGVtU3Vic2NyaXB0aW9uID0gYXRvbS53b3Jrc3BhY2Uub25EaWRDaGFuZ2VBY3RpdmVUZXh0RWRpdG9yIChhY3RpdmVFZGl0b3IpID0+IEBzdWJzY3JpYmVUb0FjdGl2ZVRleHRFZGl0b3IoKVxuXG4gICAgQHN1YnNjcmliZVRvQ29uZmlnKClcbiAgICBAc3Vic2NyaWJlVG9BY3RpdmVUZXh0RWRpdG9yKClcblxuICAgIEB0b29sdGlwID0gYXRvbS50b29sdGlwcy5hZGQoQGVsZW1lbnQsIHRpdGxlOiA9PiBcIkxpbmUgI3tAcm93fSwgQ29sdW1uICN7QGNvbHVtbn1cIilcblxuICAgIEBoYW5kbGVDbGljaygpXG5cbiAgZGVzdHJveTogLT5cbiAgICBAYWN0aXZlSXRlbVN1YnNjcmlwdGlvbi5kaXNwb3NlKClcbiAgICBAY3Vyc29yU3Vic2NyaXB0aW9uPy5kaXNwb3NlKClcbiAgICBAdG9vbHRpcC5kaXNwb3NlKClcbiAgICBAY29uZmlnU3Vic2NyaXB0aW9uPy5kaXNwb3NlKClcbiAgICBAY2xpY2tTdWJzY3JpcHRpb24uZGlzcG9zZSgpXG4gICAgQHVwZGF0ZVN1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG5cbiAgc3Vic2NyaWJlVG9BY3RpdmVUZXh0RWRpdG9yOiAtPlxuICAgIEBjdXJzb3JTdWJzY3JpcHRpb24/LmRpc3Bvc2UoKVxuICAgIHNlbGVjdGlvbnNNYXJrZXJMYXllciA9IGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVRleHRFZGl0b3IoKT8uc2VsZWN0aW9uc01hcmtlckxheWVyXG4gICAgQGN1cnNvclN1YnNjcmlwdGlvbiA9IHNlbGVjdGlvbnNNYXJrZXJMYXllcj8ub25EaWRVcGRhdGUoQHNjaGVkdWxlVXBkYXRlLmJpbmQodGhpcykpXG4gICAgQHNjaGVkdWxlVXBkYXRlKClcblxuICBzdWJzY3JpYmVUb0NvbmZpZzogLT5cbiAgICBAY29uZmlnU3Vic2NyaXB0aW9uPy5kaXNwb3NlKClcbiAgICBAY29uZmlnU3Vic2NyaXB0aW9uID0gYXRvbS5jb25maWcub2JzZXJ2ZSAnc3RhdHVzLWJhci5jdXJzb3JQb3NpdGlvbkZvcm1hdCcsICh2YWx1ZSkgPT5cbiAgICAgIEBmb3JtYXRTdHJpbmcgPSB2YWx1ZSA/ICclTDolQydcbiAgICAgIEBzY2hlZHVsZVVwZGF0ZSgpXG5cbiAgaGFuZGxlQ2xpY2s6IC0+XG4gICAgY2xpY2tIYW5kbGVyID0gLT4gYXRvbS5jb21tYW5kcy5kaXNwYXRjaChhdG9tLnZpZXdzLmdldFZpZXcoYXRvbS53b3Jrc3BhY2UuZ2V0QWN0aXZlVGV4dEVkaXRvcigpKSwgJ2dvLXRvLWxpbmU6dG9nZ2xlJylcbiAgICBAZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGNsaWNrSGFuZGxlcilcbiAgICBAY2xpY2tTdWJzY3JpcHRpb24gPSBuZXcgRGlzcG9zYWJsZSA9PiBAZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIGNsaWNrSGFuZGxlcilcblxuICBzY2hlZHVsZVVwZGF0ZTogLT5cbiAgICByZXR1cm4gaWYgQHZpZXdVcGRhdGVQZW5kaW5nXG5cbiAgICBAdmlld1VwZGF0ZVBlbmRpbmcgPSB0cnVlXG4gICAgQHVwZGF0ZVN1YnNjcmlwdGlvbiA9IGF0b20udmlld3MudXBkYXRlRG9jdW1lbnQgPT5cbiAgICAgIEB2aWV3VXBkYXRlUGVuZGluZyA9IGZhbHNlXG4gICAgICBpZiBwb3NpdGlvbiA9IGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVRleHRFZGl0b3IoKT8uZ2V0Q3Vyc29yQnVmZmVyUG9zaXRpb24oKVxuICAgICAgICBAcm93ID0gcG9zaXRpb24ucm93ICsgMVxuICAgICAgICBAY29sdW1uID0gcG9zaXRpb24uY29sdW1uICsgMVxuICAgICAgICBAZ29Ub0xpbmVMaW5rLnRleHRDb250ZW50ID0gQGZvcm1hdFN0cmluZy5yZXBsYWNlKCclTCcsIEByb3cpLnJlcGxhY2UoJyVDJywgQGNvbHVtbilcbiAgICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZScpXG4gICAgICBlbHNlXG4gICAgICAgIEBnb1RvTGluZUxpbmsudGV4dENvbnRlbnQgPSAnJ1xuICAgICAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdoaWRlJylcbiJdfQ==
