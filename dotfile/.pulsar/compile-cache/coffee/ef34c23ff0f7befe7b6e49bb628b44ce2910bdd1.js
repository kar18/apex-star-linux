(function() {
  var CompositeDisposable, Dialog, Disposable, Emitter, Point, Range, TextEditor, getFullExtension, path, ref;

  ref = require('atom'), TextEditor = ref.TextEditor, CompositeDisposable = ref.CompositeDisposable, Disposable = ref.Disposable, Emitter = ref.Emitter, Range = ref.Range, Point = ref.Point;

  path = require('path');

  getFullExtension = require("./helpers").getFullExtension;

  module.exports = Dialog = (function() {
    function Dialog(arg) {
      var baseName, blurHandler, extension, iconClass, initialPath, prompt, ref1, select, selectionEnd, selectionStart;
      ref1 = arg != null ? arg : {}, initialPath = ref1.initialPath, select = ref1.select, iconClass = ref1.iconClass, prompt = ref1.prompt;
      this.emitter = new Emitter();
      this.disposables = new CompositeDisposable();
      this.element = document.createElement('div');
      this.element.classList.add('tree-view-dialog');
      this.promptText = document.createElement('label');
      this.promptText.classList.add('icon');
      if (iconClass) {
        this.promptText.classList.add(iconClass);
      }
      this.promptText.textContent = prompt;
      this.element.appendChild(this.promptText);
      this.miniEditor = new TextEditor({
        mini: true
      });
      blurHandler = (function(_this) {
        return function() {
          if (document.hasFocus()) {
            return _this.close();
          }
        };
      })(this);
      this.miniEditor.element.addEventListener('blur', blurHandler);
      this.disposables.add(new Disposable((function(_this) {
        return function() {
          return _this.miniEditor.element.removeEventListener('blur', blurHandler);
        };
      })(this)));
      this.disposables.add(this.miniEditor.onDidChange((function(_this) {
        return function() {
          return _this.showError();
        };
      })(this)));
      this.element.appendChild(this.miniEditor.element);
      this.errorMessage = document.createElement('div');
      this.errorMessage.classList.add('error-message');
      this.element.appendChild(this.errorMessage);
      atom.commands.add(this.element, {
        'core:confirm': (function(_this) {
          return function() {
            return _this.onConfirm(_this.miniEditor.getText());
          };
        })(this),
        'core:cancel': (function(_this) {
          return function() {
            return _this.cancel();
          };
        })(this)
      });
      this.miniEditor.setText(initialPath);
      if (select) {
        extension = getFullExtension(initialPath);
        baseName = path.basename(initialPath);
        selectionStart = initialPath.length - baseName.length;
        if (baseName === extension) {
          selectionEnd = initialPath.length;
        } else {
          selectionEnd = initialPath.length - extension.length;
        }
        this.miniEditor.setSelectedBufferRange(Range(Point(0, selectionStart), Point(0, selectionEnd)));
      }
    }

    Dialog.prototype.attach = function() {
      this.panel = atom.workspace.addModalPanel({
        item: this
      });
      this.miniEditor.element.focus();
      return this.miniEditor.scrollToCursorPosition();
    };

    Dialog.prototype.close = function() {
      var activePane, panel;
      panel = this.panel;
      this.panel = null;
      if (panel != null) {
        panel.destroy();
      }
      this.emitter.dispose();
      this.disposables.dispose();
      this.miniEditor.destroy();
      activePane = atom.workspace.getCenter().getActivePane();
      if (!activePane.isDestroyed()) {
        return activePane.activate();
      }
    };

    Dialog.prototype.cancel = function() {
      var ref1;
      this.close();
      return (ref1 = document.querySelector('.tree-view')) != null ? ref1.focus() : void 0;
    };

    Dialog.prototype.showError = function(message) {
      if (message == null) {
        message = '';
      }
      this.errorMessage.textContent = message;
      if (message) {
        this.element.classList.add('error');
        return window.setTimeout(((function(_this) {
          return function() {
            return _this.element.classList.remove('error');
          };
        })(this)), 300);
      }
    };

    return Dialog;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90cmVlLXZpZXcvbGliL2RpYWxvZy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLE1BQXVFLE9BQUEsQ0FBUSxNQUFSLENBQXZFLEVBQUMsMkJBQUQsRUFBYSw2Q0FBYixFQUFrQywyQkFBbEMsRUFBOEMscUJBQTlDLEVBQXVELGlCQUF2RCxFQUE4RDs7RUFDOUQsSUFBQSxHQUFPLE9BQUEsQ0FBUSxNQUFSOztFQUNOLG1CQUFvQixPQUFBLENBQVEsV0FBUjs7RUFFckIsTUFBTSxDQUFDLE9BQVAsR0FDTTtJQUNTLGdCQUFDLEdBQUQ7QUFDWCxVQUFBOzJCQURZLE1BQTJDLElBQTFDLGdDQUFhLHNCQUFRLDRCQUFXO01BQzdDLElBQUMsQ0FBQSxPQUFELEdBQVcsSUFBSSxPQUFKLENBQUE7TUFDWCxJQUFDLENBQUEsV0FBRCxHQUFlLElBQUksbUJBQUosQ0FBQTtNQUVmLElBQUMsQ0FBQSxPQUFELEdBQVcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDWCxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixrQkFBdkI7TUFFQSxJQUFDLENBQUEsVUFBRCxHQUFjLFFBQVEsQ0FBQyxhQUFULENBQXVCLE9BQXZCO01BQ2QsSUFBQyxDQUFBLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBdEIsQ0FBMEIsTUFBMUI7TUFDQSxJQUF3QyxTQUF4QztRQUFBLElBQUMsQ0FBQSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQXRCLENBQTBCLFNBQTFCLEVBQUE7O01BQ0EsSUFBQyxDQUFBLFVBQVUsQ0FBQyxXQUFaLEdBQTBCO01BQzFCLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixJQUFDLENBQUEsVUFBdEI7TUFFQSxJQUFDLENBQUEsVUFBRCxHQUFjLElBQUksVUFBSixDQUFlO1FBQUMsSUFBQSxFQUFNLElBQVA7T0FBZjtNQUNkLFdBQUEsR0FBYyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDWixJQUFZLFFBQVEsQ0FBQyxRQUFULENBQUEsQ0FBWjttQkFBQSxLQUFDLENBQUEsS0FBRCxDQUFBLEVBQUE7O1FBRFk7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBO01BRWQsSUFBQyxDQUFBLFVBQVUsQ0FBQyxPQUFPLENBQUMsZ0JBQXBCLENBQXFDLE1BQXJDLEVBQTZDLFdBQTdDO01BQ0EsSUFBQyxDQUFBLFdBQVcsQ0FBQyxHQUFiLENBQWlCLElBQUksVUFBSixDQUFlLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsVUFBVSxDQUFDLE9BQU8sQ0FBQyxtQkFBcEIsQ0FBd0MsTUFBeEMsRUFBZ0QsV0FBaEQ7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBZixDQUFqQjtNQUNBLElBQUMsQ0FBQSxXQUFXLENBQUMsR0FBYixDQUFpQixJQUFDLENBQUEsVUFBVSxDQUFDLFdBQVosQ0FBd0IsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxTQUFELENBQUE7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBeEIsQ0FBakI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsQ0FBcUIsSUFBQyxDQUFBLFVBQVUsQ0FBQyxPQUFqQztNQUVBLElBQUMsQ0FBQSxZQUFELEdBQWdCLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO01BQ2hCLElBQUMsQ0FBQSxZQUFZLENBQUMsU0FBUyxDQUFDLEdBQXhCLENBQTRCLGVBQTVCO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLElBQUMsQ0FBQSxZQUF0QjtNQUVBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixJQUFDLENBQUEsT0FBbkIsRUFDRTtRQUFBLGNBQUEsRUFBZ0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsU0FBRCxDQUFXLEtBQUMsQ0FBQSxVQUFVLENBQUMsT0FBWixDQUFBLENBQVg7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBaEI7UUFDQSxhQUFBLEVBQWUsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsTUFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBRGY7T0FERjtNQUlBLElBQUMsQ0FBQSxVQUFVLENBQUMsT0FBWixDQUFvQixXQUFwQjtNQUVBLElBQUcsTUFBSDtRQUNFLFNBQUEsR0FBWSxnQkFBQSxDQUFpQixXQUFqQjtRQUNaLFFBQUEsR0FBVyxJQUFJLENBQUMsUUFBTCxDQUFjLFdBQWQ7UUFDWCxjQUFBLEdBQWlCLFdBQVcsQ0FBQyxNQUFaLEdBQXFCLFFBQVEsQ0FBQztRQUMvQyxJQUFHLFFBQUEsS0FBWSxTQUFmO1VBQ0UsWUFBQSxHQUFlLFdBQVcsQ0FBQyxPQUQ3QjtTQUFBLE1BQUE7VUFHRSxZQUFBLEdBQWUsV0FBVyxDQUFDLE1BQVosR0FBcUIsU0FBUyxDQUFDLE9BSGhEOztRQUlBLElBQUMsQ0FBQSxVQUFVLENBQUMsc0JBQVosQ0FBbUMsS0FBQSxDQUFNLEtBQUEsQ0FBTSxDQUFOLEVBQVMsY0FBVCxDQUFOLEVBQWdDLEtBQUEsQ0FBTSxDQUFOLEVBQVMsWUFBVCxDQUFoQyxDQUFuQyxFQVJGOztJQS9CVzs7cUJBeUNiLE1BQUEsR0FBUSxTQUFBO01BQ04sSUFBQyxDQUFBLEtBQUQsR0FBUyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWYsQ0FBNkI7UUFBQSxJQUFBLEVBQU0sSUFBTjtPQUE3QjtNQUNULElBQUMsQ0FBQSxVQUFVLENBQUMsT0FBTyxDQUFDLEtBQXBCLENBQUE7YUFDQSxJQUFDLENBQUEsVUFBVSxDQUFDLHNCQUFaLENBQUE7SUFITTs7cUJBS1IsS0FBQSxHQUFPLFNBQUE7QUFDTCxVQUFBO01BQUEsS0FBQSxHQUFRLElBQUMsQ0FBQTtNQUNULElBQUMsQ0FBQSxLQUFELEdBQVM7O1FBQ1QsS0FBSyxDQUFFLE9BQVAsQ0FBQTs7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLE9BQVQsQ0FBQTtNQUNBLElBQUMsQ0FBQSxXQUFXLENBQUMsT0FBYixDQUFBO01BQ0EsSUFBQyxDQUFBLFVBQVUsQ0FBQyxPQUFaLENBQUE7TUFDQSxVQUFBLEdBQWEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFmLENBQUEsQ0FBMEIsQ0FBQyxhQUEzQixDQUFBO01BQ2IsSUFBQSxDQUE2QixVQUFVLENBQUMsV0FBWCxDQUFBLENBQTdCO2VBQUEsVUFBVSxDQUFDLFFBQVgsQ0FBQSxFQUFBOztJQVJLOztxQkFVUCxNQUFBLEdBQVEsU0FBQTtBQUNOLFVBQUE7TUFBQSxJQUFDLENBQUEsS0FBRCxDQUFBO3lFQUNvQyxDQUFFLEtBQXRDLENBQUE7SUFGTTs7cUJBSVIsU0FBQSxHQUFXLFNBQUMsT0FBRDs7UUFBQyxVQUFROztNQUNsQixJQUFDLENBQUEsWUFBWSxDQUFDLFdBQWQsR0FBNEI7TUFDNUIsSUFBRyxPQUFIO1FBQ0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsT0FBdkI7ZUFDQSxNQUFNLENBQUMsVUFBUCxDQUFrQixDQUFDLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBbkIsQ0FBMEIsT0FBMUI7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBRCxDQUFsQixFQUEyRCxHQUEzRCxFQUZGOztJQUZTOzs7OztBQWxFYiIsInNvdXJjZXNDb250ZW50IjpbIntUZXh0RWRpdG9yLCBDb21wb3NpdGVEaXNwb3NhYmxlLCBEaXNwb3NhYmxlLCBFbWl0dGVyLCBSYW5nZSwgUG9pbnR9ID0gcmVxdWlyZSAnYXRvbSdcbnBhdGggPSByZXF1aXJlICdwYXRoJ1xue2dldEZ1bGxFeHRlbnNpb259ID0gcmVxdWlyZSBcIi4vaGVscGVyc1wiXG5cbm1vZHVsZS5leHBvcnRzID1cbmNsYXNzIERpYWxvZ1xuICBjb25zdHJ1Y3RvcjogKHtpbml0aWFsUGF0aCwgc2VsZWN0LCBpY29uQ2xhc3MsIHByb21wdH0gPSB7fSkgLT5cbiAgICBAZW1pdHRlciA9IG5ldyBFbWl0dGVyKClcbiAgICBAZGlzcG9zYWJsZXMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZSgpXG5cbiAgICBAZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgndHJlZS12aWV3LWRpYWxvZycpXG5cbiAgICBAcHJvbXB0VGV4dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2xhYmVsJylcbiAgICBAcHJvbXB0VGV4dC5jbGFzc0xpc3QuYWRkKCdpY29uJylcbiAgICBAcHJvbXB0VGV4dC5jbGFzc0xpc3QuYWRkKGljb25DbGFzcykgaWYgaWNvbkNsYXNzXG4gICAgQHByb21wdFRleHQudGV4dENvbnRlbnQgPSBwcm9tcHRcbiAgICBAZWxlbWVudC5hcHBlbmRDaGlsZChAcHJvbXB0VGV4dClcblxuICAgIEBtaW5pRWRpdG9yID0gbmV3IFRleHRFZGl0b3Ioe21pbmk6IHRydWV9KVxuICAgIGJsdXJIYW5kbGVyID0gPT5cbiAgICAgIEBjbG9zZSgpIGlmIGRvY3VtZW50Lmhhc0ZvY3VzKClcbiAgICBAbWluaUVkaXRvci5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2JsdXInLCBibHVySGFuZGxlcilcbiAgICBAZGlzcG9zYWJsZXMuYWRkKG5ldyBEaXNwb3NhYmxlKD0+IEBtaW5pRWRpdG9yLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignYmx1cicsIGJsdXJIYW5kbGVyKSkpXG4gICAgQGRpc3Bvc2FibGVzLmFkZChAbWluaUVkaXRvci5vbkRpZENoYW5nZSA9PiBAc2hvd0Vycm9yKCkpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQG1pbmlFZGl0b3IuZWxlbWVudClcblxuICAgIEBlcnJvck1lc3NhZ2UgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIEBlcnJvck1lc3NhZ2UuY2xhc3NMaXN0LmFkZCgnZXJyb3ItbWVzc2FnZScpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQGVycm9yTWVzc2FnZSlcblxuICAgIGF0b20uY29tbWFuZHMuYWRkIEBlbGVtZW50LFxuICAgICAgJ2NvcmU6Y29uZmlybSc6ID0+IEBvbkNvbmZpcm0oQG1pbmlFZGl0b3IuZ2V0VGV4dCgpKVxuICAgICAgJ2NvcmU6Y2FuY2VsJzogPT4gQGNhbmNlbCgpXG5cbiAgICBAbWluaUVkaXRvci5zZXRUZXh0KGluaXRpYWxQYXRoKVxuXG4gICAgaWYgc2VsZWN0XG4gICAgICBleHRlbnNpb24gPSBnZXRGdWxsRXh0ZW5zaW9uKGluaXRpYWxQYXRoKVxuICAgICAgYmFzZU5hbWUgPSBwYXRoLmJhc2VuYW1lKGluaXRpYWxQYXRoKVxuICAgICAgc2VsZWN0aW9uU3RhcnQgPSBpbml0aWFsUGF0aC5sZW5ndGggLSBiYXNlTmFtZS5sZW5ndGhcbiAgICAgIGlmIGJhc2VOYW1lIGlzIGV4dGVuc2lvblxuICAgICAgICBzZWxlY3Rpb25FbmQgPSBpbml0aWFsUGF0aC5sZW5ndGhcbiAgICAgIGVsc2VcbiAgICAgICAgc2VsZWN0aW9uRW5kID0gaW5pdGlhbFBhdGgubGVuZ3RoIC0gZXh0ZW5zaW9uLmxlbmd0aFxuICAgICAgQG1pbmlFZGl0b3Iuc2V0U2VsZWN0ZWRCdWZmZXJSYW5nZShSYW5nZShQb2ludCgwLCBzZWxlY3Rpb25TdGFydCksIFBvaW50KDAsIHNlbGVjdGlvbkVuZCkpKVxuXG4gIGF0dGFjaDogLT5cbiAgICBAcGFuZWwgPSBhdG9tLndvcmtzcGFjZS5hZGRNb2RhbFBhbmVsKGl0ZW06IHRoaXMpXG4gICAgQG1pbmlFZGl0b3IuZWxlbWVudC5mb2N1cygpXG4gICAgQG1pbmlFZGl0b3Iuc2Nyb2xsVG9DdXJzb3JQb3NpdGlvbigpXG5cbiAgY2xvc2U6IC0+XG4gICAgcGFuZWwgPSBAcGFuZWxcbiAgICBAcGFuZWwgPSBudWxsXG4gICAgcGFuZWw/LmRlc3Ryb3koKVxuICAgIEBlbWl0dGVyLmRpc3Bvc2UoKVxuICAgIEBkaXNwb3NhYmxlcy5kaXNwb3NlKClcbiAgICBAbWluaUVkaXRvci5kZXN0cm95KClcbiAgICBhY3RpdmVQYW5lID0gYXRvbS53b3Jrc3BhY2UuZ2V0Q2VudGVyKCkuZ2V0QWN0aXZlUGFuZSgpXG4gICAgYWN0aXZlUGFuZS5hY3RpdmF0ZSgpIHVubGVzcyBhY3RpdmVQYW5lLmlzRGVzdHJveWVkKClcblxuICBjYW5jZWw6IC0+XG4gICAgQGNsb3NlKClcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudHJlZS12aWV3Jyk/LmZvY3VzKClcblxuICBzaG93RXJyb3I6IChtZXNzYWdlPScnKSAtPlxuICAgIEBlcnJvck1lc3NhZ2UudGV4dENvbnRlbnQgPSBtZXNzYWdlXG4gICAgaWYgbWVzc2FnZVxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnZXJyb3InKVxuICAgICAgd2luZG93LnNldFRpbWVvdXQoKD0+IEBlbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2Vycm9yJykpLCAzMDApXG4iXX0=
