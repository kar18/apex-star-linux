(function() {
  var COMPLETIONS, cssDocsURL, firstCharsEqual, firstInlinePropertyNameWithColonPattern, hasScope, importantPrefixPattern, inlinePropertyNameWithColonPattern, lineEndsWithSemicolon, propertyNamePrefixPattern, propertyNameWithColonPattern, pseudoSelectorPrefixPattern, tagSelectorPrefixPattern;

  COMPLETIONS = require('../completions.json');

  firstInlinePropertyNameWithColonPattern = /{\s*(\S+)\s*:/;

  inlinePropertyNameWithColonPattern = /(?:;.+?)*;\s*(\S+)\s*:/;

  propertyNameWithColonPattern = /^\s*(\S+)\s*:/;

  propertyNamePrefixPattern = /[a-zA-Z]+[-a-zA-Z]*$/;

  pseudoSelectorPrefixPattern = /:(:)?([a-z]+[a-z-]*)?$/;

  tagSelectorPrefixPattern = /(^|\s|,)([a-z]+)?$/;

  importantPrefixPattern = /(![a-z]+)$/;

  cssDocsURL = "https://developer.mozilla.org/en-US/docs/Web/CSS";

  module.exports = {
    selector: '.source.css, .source.sass, .source.css.postcss',
    disableForSelector: '.source.css .comment, .source.css .string, .source.sass .comment, .source.sass .string, .source.css.postcss .comment, source.css.postcss .string',
    properties: COMPLETIONS.properties,
    pseudoSelectors: COMPLETIONS.pseudoSelectors,
    tags: COMPLETIONS.tags,
    filterSuggestions: true,
    getSuggestions: function(request) {
      var completions, isSass, scopes, tagCompletions;
      completions = null;
      scopes = request.scopeDescriptor.getScopesArray();
      isSass = hasScope(scopes, 'source.sass', true);
      if (this.isCompletingValue(request)) {
        completions = this.getPropertyValueCompletions(request);
      } else if (this.isCompletingPseudoSelector(request)) {
        completions = this.getPseudoSelectorCompletions(request);
      } else {
        if (isSass && this.isCompletingNameOrTag(request)) {
          completions = this.getPropertyNameCompletions(request).concat(this.getTagCompletions(request));
        } else if (!isSass && this.isCompletingName(request)) {
          completions = this.getPropertyNameCompletions(request);
        }
      }
      if (!isSass && this.isCompletingTagSelector(request)) {
        tagCompletions = this.getTagCompletions(request);
        if (tagCompletions != null ? tagCompletions.length : void 0) {
          if (completions == null) {
            completions = [];
          }
          completions = completions.concat(tagCompletions);
        }
      }
      return completions;
    },
    onDidInsertSuggestion: function(arg) {
      var editor, suggestion;
      editor = arg.editor, suggestion = arg.suggestion;
      if (suggestion.type === 'property') {
        return setTimeout(this.triggerAutocomplete.bind(this, editor), 1);
      }
    },
    triggerAutocomplete: function(editor) {
      return atom.commands.dispatch(atom.views.getView(editor), 'autocomplete-plus:activate', {
        activatedManually: false
      });
    },
    isCompletingValue: function(arg) {
      var beforePrefixBufferPosition, beforePrefixScopes, beforePrefixScopesArray, bufferPosition, editor, prefix, previousBufferPosition, previousScopes, previousScopesArray, scopeDescriptor, scopes;
      scopeDescriptor = arg.scopeDescriptor, bufferPosition = arg.bufferPosition, prefix = arg.prefix, editor = arg.editor;
      scopes = scopeDescriptor.getScopesArray();
      beforePrefixBufferPosition = [bufferPosition.row, Math.max(0, bufferPosition.column - prefix.length - 1)];
      beforePrefixScopes = editor.scopeDescriptorForBufferPosition(beforePrefixBufferPosition);
      beforePrefixScopesArray = beforePrefixScopes.getScopesArray();
      previousBufferPosition = [bufferPosition.row, Math.max(0, bufferPosition.column - 1)];
      previousScopes = editor.scopeDescriptorForBufferPosition(previousBufferPosition);
      previousScopesArray = previousScopes.getScopesArray();
      return (hasScope(scopes, 'meta.property-list.css') && prefix.trim() === ":") || (hasScope(previousScopesArray, 'meta.property-value.css')) || (hasScope(scopes, 'meta.property-list.scss') && prefix.trim() === ":") || (hasScope(previousScopesArray, 'meta.property-value.scss')) || (hasScope(scopes, 'meta.property-list.postcss') && prefix.trim() === ":") || (hasScope(previousScopesArray, 'meta.property-value.postcss')) || (hasScope(scopes, 'source.sass', true) && (hasScope(scopes, 'meta.property-value.sass') || (!hasScope(beforePrefixScopesArray, 'entity.name.tag.css') && prefix.trim() === ":")));
    },
    isCompletingName: function(arg) {
      var bufferPosition, editor, isAtBeginScopePunctuation, isAtEndScopePunctuation, isAtParentSymbol, isAtTerminator, isInPropertyList, isVariable, prefix, previousBufferPosition, previousScopes, previousScopesArray, scopeDescriptor, scopes;
      scopeDescriptor = arg.scopeDescriptor, bufferPosition = arg.bufferPosition, prefix = arg.prefix, editor = arg.editor;
      scopes = scopeDescriptor.getScopesArray();
      isAtTerminator = prefix.endsWith(';');
      isAtParentSymbol = prefix.endsWith('&');
      isVariable = hasScope(scopes, 'variable.css') || hasScope(scopes, 'variable.scss') || hasScope(scopes, 'variable.var.postcss');
      isInPropertyList = !isAtTerminator && (hasScope(scopes, 'meta.property-list.css') || hasScope(scopes, 'meta.property-list.scss') || hasScope(scopes, 'meta.property-list.postcss'));
      if (!isInPropertyList) {
        return false;
      }
      if (isAtParentSymbol || isVariable) {
        return false;
      }
      previousBufferPosition = [bufferPosition.row, Math.max(0, bufferPosition.column - prefix.length - 1)];
      previousScopes = editor.scopeDescriptorForBufferPosition(previousBufferPosition);
      previousScopesArray = previousScopes.getScopesArray();
      if (hasScope(previousScopesArray, 'entity.other.attribute-name.class.css') || hasScope(previousScopesArray, 'entity.other.attribute-name.id.css') || hasScope(previousScopesArray, 'entity.other.attribute-name.id') || hasScope(previousScopesArray, 'entity.other.attribute-name.parent-selector.css') || hasScope(previousScopesArray, 'entity.name.tag.reference.scss') || hasScope(previousScopesArray, 'entity.name.tag.scss') || hasScope(previousScopesArray, 'entity.name.tag.reference.postcss') || hasScope(previousScopesArray, 'entity.name.tag.postcss')) {
        return false;
      }
      isAtBeginScopePunctuation = hasScope(scopes, 'punctuation.section.property-list.begin.bracket.curly.css') || hasScope(scopes, 'punctuation.section.property-list.begin.bracket.curly.scss') || hasScope(scopes, 'punctuation.section.property-list.begin.postcss');
      isAtEndScopePunctuation = hasScope(scopes, 'punctuation.section.property-list.end.bracket.curly.css') || hasScope(scopes, 'punctuation.section.property-list.end.bracket.curly.scss') || hasScope(scopes, 'punctuation.section.property-list.end.postcss');
      if (isAtBeginScopePunctuation) {
        return prefix.endsWith('{');
      } else if (isAtEndScopePunctuation) {
        return !prefix.endsWith('}');
      } else {
        return true;
      }
    },
    isCompletingNameOrTag: function(arg) {
      var bufferPosition, editor, prefix, scopeDescriptor, scopes;
      scopeDescriptor = arg.scopeDescriptor, bufferPosition = arg.bufferPosition, editor = arg.editor;
      scopes = scopeDescriptor.getScopesArray();
      prefix = this.getPropertyNamePrefix(bufferPosition, editor);
      return this.isPropertyNamePrefix(prefix) && hasScope(scopes, 'meta.selector.css') && !hasScope(scopes, 'entity.other.attribute-name.id.css.sass') && !hasScope(scopes, 'entity.other.attribute-name.class.sass');
    },
    isCompletingTagSelector: function(arg) {
      var bufferPosition, editor, previousBufferPosition, previousScopes, previousScopesArray, scopeDescriptor, scopes, tagSelectorPrefix;
      editor = arg.editor, scopeDescriptor = arg.scopeDescriptor, bufferPosition = arg.bufferPosition;
      scopes = scopeDescriptor.getScopesArray();
      tagSelectorPrefix = this.getTagSelectorPrefix(editor, bufferPosition);
      if (!(tagSelectorPrefix != null ? tagSelectorPrefix.length : void 0)) {
        return false;
      }
      previousBufferPosition = [bufferPosition.row, Math.max(0, bufferPosition.column - 1)];
      previousScopes = editor.scopeDescriptorForBufferPosition(previousBufferPosition);
      previousScopesArray = previousScopes.getScopesArray();
      if (hasScope(scopes, 'meta.selector.css') || hasScope(previousScopesArray, 'meta.selector.css')) {
        return true;
      } else if (hasScope(scopes, 'source.css.scss', true) || hasScope(scopes, 'source.css.less', true) || hasScope(scopes, 'source.css.postcss', true)) {
        return !hasScope(previousScopesArray, 'meta.property-value.scss') && !hasScope(previousScopesArray, 'meta.property-value.css') && !hasScope(previousScopesArray, 'meta.property-value.postcss') && !hasScope(previousScopesArray, 'support.type.property-value.css');
      } else {
        return false;
      }
    },
    isCompletingPseudoSelector: function(arg) {
      var bufferPosition, editor, prefix, previousBufferPosition, previousScopes, previousScopesArray, scopeDescriptor, scopes;
      editor = arg.editor, scopeDescriptor = arg.scopeDescriptor, bufferPosition = arg.bufferPosition;
      scopes = scopeDescriptor.getScopesArray();
      previousBufferPosition = [bufferPosition.row, Math.max(0, bufferPosition.column - 1)];
      previousScopes = editor.scopeDescriptorForBufferPosition(previousBufferPosition);
      previousScopesArray = previousScopes.getScopesArray();
      if ((hasScope(scopes, 'meta.selector.css') || hasScope(previousScopesArray, 'meta.selector.css')) && !hasScope(scopes, 'source.sass', true)) {
        return true;
      } else if (hasScope(scopes, 'source.css.scss', true) || hasScope(scopes, 'source.css.less', true) || hasScope(scopes, 'source.sass', true) || hasScope(scopes, 'source.css.postcss', true)) {
        prefix = this.getPseudoSelectorPrefix(editor, bufferPosition);
        if (prefix) {
          previousBufferPosition = [bufferPosition.row, Math.max(0, bufferPosition.column - prefix.length - 1)];
          previousScopes = editor.scopeDescriptorForBufferPosition(previousBufferPosition);
          previousScopesArray = previousScopes.getScopesArray();
          return !hasScope(previousScopesArray, 'meta.property-name.scss') && !hasScope(previousScopesArray, 'meta.property-value.scss') && !hasScope(previousScopesArray, 'meta.property-value.postcss') && !hasScope(previousScopesArray, 'support.type.property-name.css') && !hasScope(previousScopesArray, 'support.type.property-value.css') && !hasScope(previousScopesArray, 'support.type.property-name.postcss');
        } else {
          return false;
        }
      } else {
        return false;
      }
    },
    isPropertyValuePrefix: function(prefix) {
      prefix = prefix.trim();
      return prefix.length > 0 && prefix !== ':';
    },
    isPropertyNamePrefix: function(prefix) {
      if (prefix == null) {
        return false;
      }
      prefix = prefix.trim();
      return prefix.length > 0 && prefix.match(/^[a-zA-Z-]+$/);
    },
    getImportantPrefix: function(editor, bufferPosition) {
      var line, ref;
      line = editor.getTextInRange([[bufferPosition.row, 0], bufferPosition]);
      return (ref = importantPrefixPattern.exec(line)) != null ? ref[1] : void 0;
    },
    getPreviousPropertyName: function(bufferPosition, editor) {
      var column, line, propertyName, ref, ref1, ref2, row;
      row = bufferPosition.row, column = bufferPosition.column;
      while (row >= 0) {
        line = editor.lineTextForBufferRow(row);
        if (row === bufferPosition.row) {
          line = line.substr(0, column);
        }
        propertyName = (ref = inlinePropertyNameWithColonPattern.exec(line)) != null ? ref[1] : void 0;
        if (propertyName == null) {
          propertyName = (ref1 = firstInlinePropertyNameWithColonPattern.exec(line)) != null ? ref1[1] : void 0;
        }
        if (propertyName == null) {
          propertyName = (ref2 = propertyNameWithColonPattern.exec(line)) != null ? ref2[1] : void 0;
        }
        if (propertyName) {
          return propertyName;
        }
        row--;
      }
    },
    getPropertyValueCompletions: function(arg) {
      var addSemicolon, bufferPosition, completions, editor, i, importantPrefix, j, len, len1, prefix, property, ref, scopeDescriptor, scopes, value, values;
      bufferPosition = arg.bufferPosition, editor = arg.editor, prefix = arg.prefix, scopeDescriptor = arg.scopeDescriptor;
      property = this.getPreviousPropertyName(bufferPosition, editor);
      values = (ref = this.properties[property]) != null ? ref.values : void 0;
      if (values == null) {
        return null;
      }
      scopes = scopeDescriptor.getScopesArray();
      addSemicolon = !lineEndsWithSemicolon(bufferPosition, editor) && !hasScope(scopes, 'source.sass', true);
      completions = [];
      if (this.isPropertyValuePrefix(prefix)) {
        for (i = 0, len = values.length; i < len; i++) {
          value = values[i];
          if (firstCharsEqual(value, prefix)) {
            completions.push(this.buildPropertyValueCompletion(value, property, addSemicolon));
          }
        }
      } else if (!hasScope(scopes, 'keyword.other.unit.percentage.css') && !hasScope(scopes, 'keyword.other.unit.scss') && !hasScope(scopes, 'keyword.other.unit.css')) {
        for (j = 0, len1 = values.length; j < len1; j++) {
          value = values[j];
          completions.push(this.buildPropertyValueCompletion(value, property, addSemicolon));
        }
      }
      if (importantPrefix = this.getImportantPrefix(editor, bufferPosition)) {
        completions.push({
          type: 'keyword',
          text: '!important',
          displayText: '!important',
          replacementPrefix: importantPrefix,
          description: "Forces this property to override any other declaration of the same property. Use with caution.",
          descriptionMoreURL: cssDocsURL + "/Specificity#The_!important_exception"
        });
      }
      return completions;
    },
    buildPropertyValueCompletion: function(value, propertyName, addSemicolon) {
      var text;
      text = value;
      if (addSemicolon) {
        text += ';';
      }
      return {
        type: 'value',
        text: text,
        displayText: value,
        description: value + " value for the " + propertyName + " property",
        descriptionMoreURL: cssDocsURL + "/" + propertyName + "#Values"
      };
    },
    getPropertyNamePrefix: function(bufferPosition, editor) {
      var line, ref;
      line = editor.getTextInRange([[bufferPosition.row, 0], bufferPosition]);
      return (ref = propertyNamePrefixPattern.exec(line)) != null ? ref[0] : void 0;
    },
    getPropertyNameCompletions: function(arg) {
      var activatedManually, bufferPosition, completions, editor, line, options, prefix, property, ref, scopeDescriptor, scopes;
      bufferPosition = arg.bufferPosition, editor = arg.editor, scopeDescriptor = arg.scopeDescriptor, activatedManually = arg.activatedManually;
      scopes = scopeDescriptor.getScopesArray();
      line = editor.getTextInRange([[bufferPosition.row, 0], bufferPosition]);
      if (hasScope(scopes, 'source.sass', true) && !line.match(/^(\s|\t)/)) {
        return [];
      }
      prefix = this.getPropertyNamePrefix(bufferPosition, editor);
      if (!(activatedManually || prefix)) {
        return [];
      }
      completions = [];
      ref = this.properties;
      for (property in ref) {
        options = ref[property];
        if (!prefix || firstCharsEqual(property, prefix)) {
          completions.push(this.buildPropertyNameCompletion(property, prefix, options));
        }
      }
      return completions;
    },
    buildPropertyNameCompletion: function(propertyName, prefix, arg) {
      var description;
      description = arg.description;
      return {
        type: 'property',
        text: propertyName + ": ",
        displayText: propertyName,
        replacementPrefix: prefix,
        description: description,
        descriptionMoreURL: cssDocsURL + "/" + propertyName
      };
    },
    getPseudoSelectorPrefix: function(editor, bufferPosition) {
      var line, ref;
      line = editor.getTextInRange([[bufferPosition.row, 0], bufferPosition]);
      return (ref = line.match(pseudoSelectorPrefixPattern)) != null ? ref[0] : void 0;
    },
    getPseudoSelectorCompletions: function(arg) {
      var bufferPosition, completions, editor, options, prefix, pseudoSelector, ref;
      bufferPosition = arg.bufferPosition, editor = arg.editor;
      prefix = this.getPseudoSelectorPrefix(editor, bufferPosition);
      if (!prefix) {
        return null;
      }
      completions = [];
      ref = this.pseudoSelectors;
      for (pseudoSelector in ref) {
        options = ref[pseudoSelector];
        if (firstCharsEqual(pseudoSelector, prefix)) {
          completions.push(this.buildPseudoSelectorCompletion(pseudoSelector, prefix, options));
        }
      }
      return completions;
    },
    buildPseudoSelectorCompletion: function(pseudoSelector, prefix, arg) {
      var argument, completion, description;
      argument = arg.argument, description = arg.description;
      completion = {
        type: 'pseudo-selector',
        replacementPrefix: prefix,
        description: description,
        descriptionMoreURL: cssDocsURL + "/" + pseudoSelector
      };
      if (argument != null) {
        completion.snippet = pseudoSelector + "(${1:" + argument + "})";
      } else {
        completion.text = pseudoSelector;
      }
      return completion;
    },
    getTagSelectorPrefix: function(editor, bufferPosition) {
      var line, ref;
      line = editor.getTextInRange([[bufferPosition.row, 0], bufferPosition]);
      return (ref = tagSelectorPrefixPattern.exec(line)) != null ? ref[2] : void 0;
    },
    getTagCompletions: function(arg) {
      var bufferPosition, completions, editor, i, len, prefix, ref, tag;
      bufferPosition = arg.bufferPosition, editor = arg.editor, prefix = arg.prefix;
      completions = [];
      if (prefix) {
        ref = this.tags;
        for (i = 0, len = ref.length; i < len; i++) {
          tag = ref[i];
          if (firstCharsEqual(tag, prefix)) {
            completions.push(this.buildTagCompletion(tag));
          }
        }
      }
      return completions;
    },
    buildTagCompletion: function(tag) {
      return {
        type: 'tag',
        text: tag,
        description: "Selector for <" + tag + "> elements"
      };
    }
  };

  lineEndsWithSemicolon = function(bufferPosition, editor) {
    var line, row;
    row = bufferPosition.row;
    line = editor.lineTextForBufferRow(row);
    return /;\s*$/.test(line);
  };

  hasScope = function(scopesArray, scope, checkEmbedded) {
    if (checkEmbedded == null) {
      checkEmbedded = false;
    }
    return scopesArray.indexOf(scope) !== -1 || (checkEmbedded && scopesArray.indexOf(scope + ".embedded.html") !== -1);
  };

  firstCharsEqual = function(str1, str2) {
    return str1[0].toLowerCase() === str2[0].toLowerCase();
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9hdXRvY29tcGxldGUtY3NzL2xpYi9wcm92aWRlci5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLFdBQUEsR0FBYyxPQUFBLENBQVEscUJBQVI7O0VBRWQsdUNBQUEsR0FBMEM7O0VBQzFDLGtDQUFBLEdBQXFDOztFQUNyQyw0QkFBQSxHQUErQjs7RUFDL0IseUJBQUEsR0FBNEI7O0VBQzVCLDJCQUFBLEdBQThCOztFQUM5Qix3QkFBQSxHQUEyQjs7RUFDM0Isc0JBQUEsR0FBeUI7O0VBQ3pCLFVBQUEsR0FBYTs7RUFFYixNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsUUFBQSxFQUFVLGdEQUFWO0lBQ0Esa0JBQUEsRUFBb0Isa0pBRHBCO0lBRUEsVUFBQSxFQUFZLFdBQVcsQ0FBQyxVQUZ4QjtJQUdBLGVBQUEsRUFBaUIsV0FBVyxDQUFDLGVBSDdCO0lBSUEsSUFBQSxFQUFNLFdBQVcsQ0FBQyxJQUpsQjtJQVNBLGlCQUFBLEVBQW1CLElBVG5CO0lBV0EsY0FBQSxFQUFnQixTQUFDLE9BQUQ7QUFDZCxVQUFBO01BQUEsV0FBQSxHQUFjO01BQ2QsTUFBQSxHQUFTLE9BQU8sQ0FBQyxlQUFlLENBQUMsY0FBeEIsQ0FBQTtNQUNULE1BQUEsR0FBUyxRQUFBLENBQVMsTUFBVCxFQUFpQixhQUFqQixFQUFnQyxJQUFoQztNQUVULElBQUcsSUFBQyxDQUFBLGlCQUFELENBQW1CLE9BQW5CLENBQUg7UUFDRSxXQUFBLEdBQWMsSUFBQyxDQUFBLDJCQUFELENBQTZCLE9BQTdCLEVBRGhCO09BQUEsTUFFSyxJQUFHLElBQUMsQ0FBQSwwQkFBRCxDQUE0QixPQUE1QixDQUFIO1FBQ0gsV0FBQSxHQUFjLElBQUMsQ0FBQSw0QkFBRCxDQUE4QixPQUE5QixFQURYO09BQUEsTUFBQTtRQUdILElBQUcsTUFBQSxJQUFXLElBQUMsQ0FBQSxxQkFBRCxDQUF1QixPQUF2QixDQUFkO1VBQ0UsV0FBQSxHQUFjLElBQUMsQ0FBQSwwQkFBRCxDQUE0QixPQUE1QixDQUNaLENBQUMsTUFEVyxDQUNKLElBQUMsQ0FBQSxpQkFBRCxDQUFtQixPQUFuQixDQURJLEVBRGhCO1NBQUEsTUFHSyxJQUFHLENBQUksTUFBSixJQUFlLElBQUMsQ0FBQSxnQkFBRCxDQUFrQixPQUFsQixDQUFsQjtVQUNILFdBQUEsR0FBYyxJQUFDLENBQUEsMEJBQUQsQ0FBNEIsT0FBNUIsRUFEWDtTQU5GOztNQVNMLElBQUcsQ0FBSSxNQUFKLElBQWUsSUFBQyxDQUFBLHVCQUFELENBQXlCLE9BQXpCLENBQWxCO1FBQ0UsY0FBQSxHQUFpQixJQUFDLENBQUEsaUJBQUQsQ0FBbUIsT0FBbkI7UUFDakIsNkJBQUcsY0FBYyxDQUFFLGVBQW5COztZQUNFLGNBQWU7O1VBQ2YsV0FBQSxHQUFjLFdBQVcsQ0FBQyxNQUFaLENBQW1CLGNBQW5CLEVBRmhCO1NBRkY7O2FBTUE7SUF0QmMsQ0FYaEI7SUFtQ0EscUJBQUEsRUFBdUIsU0FBQyxHQUFEO0FBQ3JCLFVBQUE7TUFEdUIscUJBQVE7TUFDL0IsSUFBMEQsVUFBVSxDQUFDLElBQVgsS0FBbUIsVUFBN0U7ZUFBQSxVQUFBLENBQVcsSUFBQyxDQUFBLG1CQUFtQixDQUFDLElBQXJCLENBQTBCLElBQTFCLEVBQWdDLE1BQWhDLENBQVgsRUFBb0QsQ0FBcEQsRUFBQTs7SUFEcUIsQ0FuQ3ZCO0lBc0NBLG1CQUFBLEVBQXFCLFNBQUMsTUFBRDthQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQWQsQ0FBdUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFYLENBQW1CLE1BQW5CLENBQXZCLEVBQW1ELDRCQUFuRCxFQUFpRjtRQUFDLGlCQUFBLEVBQW1CLEtBQXBCO09BQWpGO0lBRG1CLENBdENyQjtJQXlDQSxpQkFBQSxFQUFtQixTQUFDLEdBQUQ7QUFDakIsVUFBQTtNQURtQix1Q0FBaUIscUNBQWdCLHFCQUFRO01BQzVELE1BQUEsR0FBUyxlQUFlLENBQUMsY0FBaEIsQ0FBQTtNQUVULDBCQUFBLEdBQTZCLENBQUMsY0FBYyxDQUFDLEdBQWhCLEVBQXFCLElBQUksQ0FBQyxHQUFMLENBQVMsQ0FBVCxFQUFZLGNBQWMsQ0FBQyxNQUFmLEdBQXdCLE1BQU0sQ0FBQyxNQUEvQixHQUF3QyxDQUFwRCxDQUFyQjtNQUM3QixrQkFBQSxHQUFxQixNQUFNLENBQUMsZ0NBQVAsQ0FBd0MsMEJBQXhDO01BQ3JCLHVCQUFBLEdBQTBCLGtCQUFrQixDQUFDLGNBQW5CLENBQUE7TUFFMUIsc0JBQUEsR0FBeUIsQ0FBQyxjQUFjLENBQUMsR0FBaEIsRUFBcUIsSUFBSSxDQUFDLEdBQUwsQ0FBUyxDQUFULEVBQVksY0FBYyxDQUFDLE1BQWYsR0FBd0IsQ0FBcEMsQ0FBckI7TUFDekIsY0FBQSxHQUFpQixNQUFNLENBQUMsZ0NBQVAsQ0FBd0Msc0JBQXhDO01BQ2pCLG1CQUFBLEdBQXNCLGNBQWMsQ0FBQyxjQUFmLENBQUE7YUFFdEIsQ0FBQyxRQUFBLENBQVMsTUFBVCxFQUFpQix3QkFBakIsQ0FBQSxJQUErQyxNQUFNLENBQUMsSUFBUCxDQUFBLENBQUEsS0FBaUIsR0FBakUsQ0FBQSxJQUNBLENBQUMsUUFBQSxDQUFTLG1CQUFULEVBQThCLHlCQUE5QixDQUFELENBREEsSUFFQSxDQUFDLFFBQUEsQ0FBUyxNQUFULEVBQWlCLHlCQUFqQixDQUFBLElBQWdELE1BQU0sQ0FBQyxJQUFQLENBQUEsQ0FBQSxLQUFpQixHQUFsRSxDQUZBLElBR0EsQ0FBQyxRQUFBLENBQVMsbUJBQVQsRUFBOEIsMEJBQTlCLENBQUQsQ0FIQSxJQUlBLENBQUMsUUFBQSxDQUFTLE1BQVQsRUFBaUIsNEJBQWpCLENBQUEsSUFBbUQsTUFBTSxDQUFDLElBQVAsQ0FBQSxDQUFBLEtBQWlCLEdBQXJFLENBSkEsSUFLQSxDQUFDLFFBQUEsQ0FBUyxtQkFBVCxFQUE4Qiw2QkFBOUIsQ0FBRCxDQUxBLElBTUEsQ0FBQyxRQUFBLENBQVMsTUFBVCxFQUFpQixhQUFqQixFQUFnQyxJQUFoQyxDQUFBLElBQTBDLENBQUMsUUFBQSxDQUFTLE1BQVQsRUFBaUIsMEJBQWpCLENBQUEsSUFDMUMsQ0FBQyxDQUFJLFFBQUEsQ0FBUyx1QkFBVCxFQUFrQyxxQkFBbEMsQ0FBSixJQUFpRSxNQUFNLENBQUMsSUFBUCxDQUFBLENBQUEsS0FBaUIsR0FBbkYsQ0FEeUMsQ0FBM0M7SUFqQmlCLENBekNuQjtJQThEQSxnQkFBQSxFQUFrQixTQUFDLEdBQUQ7QUFDaEIsVUFBQTtNQURrQix1Q0FBaUIscUNBQWdCLHFCQUFRO01BQzNELE1BQUEsR0FBUyxlQUFlLENBQUMsY0FBaEIsQ0FBQTtNQUNULGNBQUEsR0FBaUIsTUFBTSxDQUFDLFFBQVAsQ0FBZ0IsR0FBaEI7TUFDakIsZ0JBQUEsR0FBbUIsTUFBTSxDQUFDLFFBQVAsQ0FBZ0IsR0FBaEI7TUFDbkIsVUFBQSxHQUFhLFFBQUEsQ0FBUyxNQUFULEVBQWlCLGNBQWpCLENBQUEsSUFDWCxRQUFBLENBQVMsTUFBVCxFQUFpQixlQUFqQixDQURXLElBRVgsUUFBQSxDQUFTLE1BQVQsRUFBaUIsc0JBQWpCO01BQ0YsZ0JBQUEsR0FBbUIsQ0FBSSxjQUFKLElBQ2pCLENBQUMsUUFBQSxDQUFTLE1BQVQsRUFBaUIsd0JBQWpCLENBQUEsSUFDRCxRQUFBLENBQVMsTUFBVCxFQUFpQix5QkFBakIsQ0FEQyxJQUVELFFBQUEsQ0FBUyxNQUFULEVBQWlCLDRCQUFqQixDQUZBO01BSUYsSUFBQSxDQUFvQixnQkFBcEI7QUFBQSxlQUFPLE1BQVA7O01BQ0EsSUFBZ0IsZ0JBQUEsSUFBb0IsVUFBcEM7QUFBQSxlQUFPLE1BQVA7O01BRUEsc0JBQUEsR0FBeUIsQ0FBQyxjQUFjLENBQUMsR0FBaEIsRUFBcUIsSUFBSSxDQUFDLEdBQUwsQ0FBUyxDQUFULEVBQVksY0FBYyxDQUFDLE1BQWYsR0FBd0IsTUFBTSxDQUFDLE1BQS9CLEdBQXdDLENBQXBELENBQXJCO01BQ3pCLGNBQUEsR0FBaUIsTUFBTSxDQUFDLGdDQUFQLENBQXdDLHNCQUF4QztNQUNqQixtQkFBQSxHQUFzQixjQUFjLENBQUMsY0FBZixDQUFBO01BRXRCLElBQWdCLFFBQUEsQ0FBUyxtQkFBVCxFQUE4Qix1Q0FBOUIsQ0FBQSxJQUNkLFFBQUEsQ0FBUyxtQkFBVCxFQUE4QixvQ0FBOUIsQ0FEYyxJQUVkLFFBQUEsQ0FBUyxtQkFBVCxFQUE4QixnQ0FBOUIsQ0FGYyxJQUdkLFFBQUEsQ0FBUyxtQkFBVCxFQUE4QixpREFBOUIsQ0FIYyxJQUlkLFFBQUEsQ0FBUyxtQkFBVCxFQUE4QixnQ0FBOUIsQ0FKYyxJQUtkLFFBQUEsQ0FBUyxtQkFBVCxFQUE4QixzQkFBOUIsQ0FMYyxJQU1kLFFBQUEsQ0FBUyxtQkFBVCxFQUE4QixtQ0FBOUIsQ0FOYyxJQU9kLFFBQUEsQ0FBUyxtQkFBVCxFQUE4Qix5QkFBOUIsQ0FQRjtBQUFBLGVBQU8sTUFBUDs7TUFTQSx5QkFBQSxHQUE0QixRQUFBLENBQVMsTUFBVCxFQUFpQiwyREFBakIsQ0FBQSxJQUMxQixRQUFBLENBQVMsTUFBVCxFQUFpQiw0REFBakIsQ0FEMEIsSUFFMUIsUUFBQSxDQUFTLE1BQVQsRUFBaUIsaURBQWpCO01BQ0YsdUJBQUEsR0FBMEIsUUFBQSxDQUFTLE1BQVQsRUFBaUIseURBQWpCLENBQUEsSUFDeEIsUUFBQSxDQUFTLE1BQVQsRUFBaUIsMERBQWpCLENBRHdCLElBRXhCLFFBQUEsQ0FBUyxNQUFULEVBQWlCLCtDQUFqQjtNQUVGLElBQUcseUJBQUg7ZUFHRSxNQUFNLENBQUMsUUFBUCxDQUFnQixHQUFoQixFQUhGO09BQUEsTUFJSyxJQUFHLHVCQUFIO2VBR0gsQ0FBSSxNQUFNLENBQUMsUUFBUCxDQUFnQixHQUFoQixFQUhEO09BQUEsTUFBQTtlQUtILEtBTEc7O0lBdkNXLENBOURsQjtJQTRHQSxxQkFBQSxFQUF1QixTQUFDLEdBQUQ7QUFDckIsVUFBQTtNQUR1Qix1Q0FBaUIscUNBQWdCO01BQ3hELE1BQUEsR0FBUyxlQUFlLENBQUMsY0FBaEIsQ0FBQTtNQUNULE1BQUEsR0FBUyxJQUFDLENBQUEscUJBQUQsQ0FBdUIsY0FBdkIsRUFBdUMsTUFBdkM7QUFDVCxhQUFPLElBQUMsQ0FBQSxvQkFBRCxDQUFzQixNQUF0QixDQUFBLElBQ0wsUUFBQSxDQUFTLE1BQVQsRUFBaUIsbUJBQWpCLENBREssSUFFTCxDQUFJLFFBQUEsQ0FBUyxNQUFULEVBQWlCLHlDQUFqQixDQUZDLElBR0wsQ0FBSSxRQUFBLENBQVMsTUFBVCxFQUFpQix3Q0FBakI7SUFOZSxDQTVHdkI7SUFvSEEsdUJBQUEsRUFBeUIsU0FBQyxHQUFEO0FBQ3ZCLFVBQUE7TUFEeUIscUJBQVEsdUNBQWlCO01BQ2xELE1BQUEsR0FBUyxlQUFlLENBQUMsY0FBaEIsQ0FBQTtNQUNULGlCQUFBLEdBQW9CLElBQUMsQ0FBQSxvQkFBRCxDQUFzQixNQUF0QixFQUE4QixjQUE5QjtNQUNwQixJQUFBLDhCQUFvQixpQkFBaUIsQ0FBRSxnQkFBdkM7QUFBQSxlQUFPLE1BQVA7O01BRUEsc0JBQUEsR0FBeUIsQ0FBQyxjQUFjLENBQUMsR0FBaEIsRUFBcUIsSUFBSSxDQUFDLEdBQUwsQ0FBUyxDQUFULEVBQVksY0FBYyxDQUFDLE1BQWYsR0FBd0IsQ0FBcEMsQ0FBckI7TUFDekIsY0FBQSxHQUFpQixNQUFNLENBQUMsZ0NBQVAsQ0FBd0Msc0JBQXhDO01BQ2pCLG1CQUFBLEdBQXNCLGNBQWMsQ0FBQyxjQUFmLENBQUE7TUFFdEIsSUFBRyxRQUFBLENBQVMsTUFBVCxFQUFpQixtQkFBakIsQ0FBQSxJQUF5QyxRQUFBLENBQVMsbUJBQVQsRUFBOEIsbUJBQTlCLENBQTVDO2VBQ0UsS0FERjtPQUFBLE1BRUssSUFBRyxRQUFBLENBQVMsTUFBVCxFQUFpQixpQkFBakIsRUFBb0MsSUFBcEMsQ0FBQSxJQUE2QyxRQUFBLENBQVMsTUFBVCxFQUFpQixpQkFBakIsRUFBb0MsSUFBcEMsQ0FBN0MsSUFBMEYsUUFBQSxDQUFTLE1BQVQsRUFBaUIsb0JBQWpCLEVBQXVDLElBQXZDLENBQTdGO2VBQ0gsQ0FBSSxRQUFBLENBQVMsbUJBQVQsRUFBOEIsMEJBQTlCLENBQUosSUFDRSxDQUFJLFFBQUEsQ0FBUyxtQkFBVCxFQUE4Qix5QkFBOUIsQ0FETixJQUVFLENBQUksUUFBQSxDQUFTLG1CQUFULEVBQThCLDZCQUE5QixDQUZOLElBR0UsQ0FBSSxRQUFBLENBQVMsbUJBQVQsRUFBOEIsaUNBQTlCLEVBSkg7T0FBQSxNQUFBO2VBTUgsTUFORzs7SUFYa0IsQ0FwSHpCO0lBdUlBLDBCQUFBLEVBQTRCLFNBQUMsR0FBRDtBQUMxQixVQUFBO01BRDRCLHFCQUFRLHVDQUFpQjtNQUNyRCxNQUFBLEdBQVMsZUFBZSxDQUFDLGNBQWhCLENBQUE7TUFDVCxzQkFBQSxHQUF5QixDQUFDLGNBQWMsQ0FBQyxHQUFoQixFQUFxQixJQUFJLENBQUMsR0FBTCxDQUFTLENBQVQsRUFBWSxjQUFjLENBQUMsTUFBZixHQUF3QixDQUFwQyxDQUFyQjtNQUN6QixjQUFBLEdBQWlCLE1BQU0sQ0FBQyxnQ0FBUCxDQUF3QyxzQkFBeEM7TUFDakIsbUJBQUEsR0FBc0IsY0FBYyxDQUFDLGNBQWYsQ0FBQTtNQUN0QixJQUFHLENBQUMsUUFBQSxDQUFTLE1BQVQsRUFBaUIsbUJBQWpCLENBQUEsSUFBeUMsUUFBQSxDQUFTLG1CQUFULEVBQThCLG1CQUE5QixDQUExQyxDQUFBLElBQWtHLENBQUksUUFBQSxDQUFTLE1BQVQsRUFBaUIsYUFBakIsRUFBZ0MsSUFBaEMsQ0FBekc7ZUFDRSxLQURGO09BQUEsTUFFSyxJQUFHLFFBQUEsQ0FBUyxNQUFULEVBQWlCLGlCQUFqQixFQUFvQyxJQUFwQyxDQUFBLElBQTZDLFFBQUEsQ0FBUyxNQUFULEVBQWlCLGlCQUFqQixFQUFvQyxJQUFwQyxDQUE3QyxJQUEwRixRQUFBLENBQVMsTUFBVCxFQUFpQixhQUFqQixFQUFnQyxJQUFoQyxDQUExRixJQUFtSSxRQUFBLENBQVMsTUFBVCxFQUFpQixvQkFBakIsRUFBdUMsSUFBdkMsQ0FBdEk7UUFDSCxNQUFBLEdBQVMsSUFBQyxDQUFBLHVCQUFELENBQXlCLE1BQXpCLEVBQWlDLGNBQWpDO1FBQ1QsSUFBRyxNQUFIO1VBQ0Usc0JBQUEsR0FBeUIsQ0FBQyxjQUFjLENBQUMsR0FBaEIsRUFBcUIsSUFBSSxDQUFDLEdBQUwsQ0FBUyxDQUFULEVBQVksY0FBYyxDQUFDLE1BQWYsR0FBd0IsTUFBTSxDQUFDLE1BQS9CLEdBQXdDLENBQXBELENBQXJCO1VBQ3pCLGNBQUEsR0FBaUIsTUFBTSxDQUFDLGdDQUFQLENBQXdDLHNCQUF4QztVQUNqQixtQkFBQSxHQUFzQixjQUFjLENBQUMsY0FBZixDQUFBO2lCQUN0QixDQUFJLFFBQUEsQ0FBUyxtQkFBVCxFQUE4Qix5QkFBOUIsQ0FBSixJQUNFLENBQUksUUFBQSxDQUFTLG1CQUFULEVBQThCLDBCQUE5QixDQUROLElBRUUsQ0FBSSxRQUFBLENBQVMsbUJBQVQsRUFBOEIsNkJBQTlCLENBRk4sSUFHRSxDQUFJLFFBQUEsQ0FBUyxtQkFBVCxFQUE4QixnQ0FBOUIsQ0FITixJQUlFLENBQUksUUFBQSxDQUFTLG1CQUFULEVBQThCLGlDQUE5QixDQUpOLElBS0UsQ0FBSSxRQUFBLENBQVMsbUJBQVQsRUFBOEIsb0NBQTlCLEVBVFI7U0FBQSxNQUFBO2lCQVdFLE1BWEY7U0FGRztPQUFBLE1BQUE7ZUFlSCxNQWZHOztJQVBxQixDQXZJNUI7SUErSkEscUJBQUEsRUFBdUIsU0FBQyxNQUFEO01BQ3JCLE1BQUEsR0FBUyxNQUFNLENBQUMsSUFBUCxDQUFBO2FBQ1QsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsQ0FBaEIsSUFBc0IsTUFBQSxLQUFZO0lBRmIsQ0EvSnZCO0lBbUtBLG9CQUFBLEVBQXNCLFNBQUMsTUFBRDtNQUNwQixJQUFvQixjQUFwQjtBQUFBLGVBQU8sTUFBUDs7TUFDQSxNQUFBLEdBQVMsTUFBTSxDQUFDLElBQVAsQ0FBQTthQUNULE1BQU0sQ0FBQyxNQUFQLEdBQWdCLENBQWhCLElBQXNCLE1BQU0sQ0FBQyxLQUFQLENBQWEsY0FBYjtJQUhGLENBbkt0QjtJQXdLQSxrQkFBQSxFQUFvQixTQUFDLE1BQUQsRUFBUyxjQUFUO0FBQ2xCLFVBQUE7TUFBQSxJQUFBLEdBQU8sTUFBTSxDQUFDLGNBQVAsQ0FBc0IsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFoQixFQUFxQixDQUFyQixDQUFELEVBQTBCLGNBQTFCLENBQXRCO29FQUM0QixDQUFBLENBQUE7SUFGakIsQ0F4S3BCO0lBNEtBLHVCQUFBLEVBQXlCLFNBQUMsY0FBRCxFQUFpQixNQUFqQjtBQUN2QixVQUFBO01BQUMsd0JBQUQsRUFBTTtBQUNOLGFBQU0sR0FBQSxJQUFPLENBQWI7UUFDRSxJQUFBLEdBQU8sTUFBTSxDQUFDLG9CQUFQLENBQTRCLEdBQTVCO1FBQ1AsSUFBaUMsR0FBQSxLQUFPLGNBQWMsQ0FBQyxHQUF2RDtVQUFBLElBQUEsR0FBTyxJQUFJLENBQUMsTUFBTCxDQUFZLENBQVosRUFBZSxNQUFmLEVBQVA7O1FBQ0EsWUFBQSxzRUFBOEQsQ0FBQSxDQUFBOztVQUM5RCx5RkFBb0UsQ0FBQSxDQUFBOzs7VUFDcEUsOEVBQXlELENBQUEsQ0FBQTs7UUFDekQsSUFBdUIsWUFBdkI7QUFBQSxpQkFBTyxhQUFQOztRQUNBLEdBQUE7TUFQRjtJQUZ1QixDQTVLekI7SUF3TEEsMkJBQUEsRUFBNkIsU0FBQyxHQUFEO0FBQzNCLFVBQUE7TUFENkIscUNBQWdCLHFCQUFRLHFCQUFRO01BQzdELFFBQUEsR0FBVyxJQUFDLENBQUEsdUJBQUQsQ0FBeUIsY0FBekIsRUFBeUMsTUFBekM7TUFDWCxNQUFBLGtEQUE4QixDQUFFO01BQ2hDLElBQW1CLGNBQW5CO0FBQUEsZUFBTyxLQUFQOztNQUVBLE1BQUEsR0FBUyxlQUFlLENBQUMsY0FBaEIsQ0FBQTtNQUNULFlBQUEsR0FBZSxDQUFJLHFCQUFBLENBQXNCLGNBQXRCLEVBQXNDLE1BQXRDLENBQUosSUFBc0QsQ0FBSSxRQUFBLENBQVMsTUFBVCxFQUFpQixhQUFqQixFQUFnQyxJQUFoQztNQUV6RSxXQUFBLEdBQWM7TUFDZCxJQUFHLElBQUMsQ0FBQSxxQkFBRCxDQUF1QixNQUF2QixDQUFIO0FBQ0UsYUFBQSx3Q0FBQTs7Y0FBeUIsZUFBQSxDQUFnQixLQUFoQixFQUF1QixNQUF2QjtZQUN2QixXQUFXLENBQUMsSUFBWixDQUFpQixJQUFDLENBQUEsNEJBQUQsQ0FBOEIsS0FBOUIsRUFBcUMsUUFBckMsRUFBK0MsWUFBL0MsQ0FBakI7O0FBREYsU0FERjtPQUFBLE1BR0ssSUFBRyxDQUFJLFFBQUEsQ0FBUyxNQUFULEVBQWlCLG1DQUFqQixDQUFKLElBQ1IsQ0FBSSxRQUFBLENBQVMsTUFBVCxFQUFpQix5QkFBakIsQ0FESSxJQUVSLENBQUksUUFBQSxDQUFTLE1BQVQsRUFBaUIsd0JBQWpCLENBRkM7QUFJSCxhQUFBLDBDQUFBOztVQUNFLFdBQVcsQ0FBQyxJQUFaLENBQWlCLElBQUMsQ0FBQSw0QkFBRCxDQUE4QixLQUE5QixFQUFxQyxRQUFyQyxFQUErQyxZQUEvQyxDQUFqQjtBQURGLFNBSkc7O01BT0wsSUFBRyxlQUFBLEdBQWtCLElBQUMsQ0FBQSxrQkFBRCxDQUFvQixNQUFwQixFQUE0QixjQUE1QixDQUFyQjtRQUVFLFdBQVcsQ0FBQyxJQUFaLENBQ0U7VUFBQSxJQUFBLEVBQU0sU0FBTjtVQUNBLElBQUEsRUFBTSxZQUROO1VBRUEsV0FBQSxFQUFhLFlBRmI7VUFHQSxpQkFBQSxFQUFtQixlQUhuQjtVQUlBLFdBQUEsRUFBYSxnR0FKYjtVQUtBLGtCQUFBLEVBQXVCLFVBQUQsR0FBWSx1Q0FMbEM7U0FERixFQUZGOzthQVVBO0lBN0IyQixDQXhMN0I7SUF1TkEsNEJBQUEsRUFBOEIsU0FBQyxLQUFELEVBQVEsWUFBUixFQUFzQixZQUF0QjtBQUM1QixVQUFBO01BQUEsSUFBQSxHQUFPO01BQ1AsSUFBZSxZQUFmO1FBQUEsSUFBQSxJQUFRLElBQVI7O2FBRUE7UUFDRSxJQUFBLEVBQU0sT0FEUjtRQUVFLElBQUEsRUFBTSxJQUZSO1FBR0UsV0FBQSxFQUFhLEtBSGY7UUFJRSxXQUFBLEVBQWdCLEtBQUQsR0FBTyxpQkFBUCxHQUF3QixZQUF4QixHQUFxQyxXQUp0RDtRQUtFLGtCQUFBLEVBQXVCLFVBQUQsR0FBWSxHQUFaLEdBQWUsWUFBZixHQUE0QixTQUxwRDs7SUFKNEIsQ0F2TjlCO0lBbU9BLHFCQUFBLEVBQXVCLFNBQUMsY0FBRCxFQUFpQixNQUFqQjtBQUNyQixVQUFBO01BQUEsSUFBQSxHQUFPLE1BQU0sQ0FBQyxjQUFQLENBQXNCLENBQUMsQ0FBQyxjQUFjLENBQUMsR0FBaEIsRUFBcUIsQ0FBckIsQ0FBRCxFQUEwQixjQUExQixDQUF0Qjt1RUFDK0IsQ0FBQSxDQUFBO0lBRmpCLENBbk92QjtJQXVPQSwwQkFBQSxFQUE0QixTQUFDLEdBQUQ7QUFFMUIsVUFBQTtNQUY0QixxQ0FBZ0IscUJBQVEsdUNBQWlCO01BRXJFLE1BQUEsR0FBUyxlQUFlLENBQUMsY0FBaEIsQ0FBQTtNQUNULElBQUEsR0FBTyxNQUFNLENBQUMsY0FBUCxDQUFzQixDQUFDLENBQUMsY0FBYyxDQUFDLEdBQWhCLEVBQXFCLENBQXJCLENBQUQsRUFBMEIsY0FBMUIsQ0FBdEI7TUFDUCxJQUFhLFFBQUEsQ0FBUyxNQUFULEVBQWlCLGFBQWpCLEVBQWdDLElBQWhDLENBQUEsSUFBMEMsQ0FBSSxJQUFJLENBQUMsS0FBTCxDQUFXLFVBQVgsQ0FBM0Q7QUFBQSxlQUFPLEdBQVA7O01BRUEsTUFBQSxHQUFTLElBQUMsQ0FBQSxxQkFBRCxDQUF1QixjQUF2QixFQUF1QyxNQUF2QztNQUNULElBQUEsQ0FBQSxDQUFpQixpQkFBQSxJQUFxQixNQUF0QyxDQUFBO0FBQUEsZUFBTyxHQUFQOztNQUVBLFdBQUEsR0FBYztBQUNkO0FBQUEsV0FBQSxlQUFBOztZQUEwQyxDQUFJLE1BQUosSUFBYyxlQUFBLENBQWdCLFFBQWhCLEVBQTBCLE1BQTFCO1VBQ3RELFdBQVcsQ0FBQyxJQUFaLENBQWlCLElBQUMsQ0FBQSwyQkFBRCxDQUE2QixRQUE3QixFQUF1QyxNQUF2QyxFQUErQyxPQUEvQyxDQUFqQjs7QUFERjthQUVBO0lBWjBCLENBdk81QjtJQXFQQSwyQkFBQSxFQUE2QixTQUFDLFlBQUQsRUFBZSxNQUFmLEVBQXVCLEdBQXZCO0FBQzNCLFVBQUE7TUFEbUQsY0FBRDthQUNsRDtRQUFBLElBQUEsRUFBTSxVQUFOO1FBQ0EsSUFBQSxFQUFTLFlBQUQsR0FBYyxJQUR0QjtRQUVBLFdBQUEsRUFBYSxZQUZiO1FBR0EsaUJBQUEsRUFBbUIsTUFIbkI7UUFJQSxXQUFBLEVBQWEsV0FKYjtRQUtBLGtCQUFBLEVBQXVCLFVBQUQsR0FBWSxHQUFaLEdBQWUsWUFMckM7O0lBRDJCLENBclA3QjtJQTZQQSx1QkFBQSxFQUF5QixTQUFDLE1BQUQsRUFBUyxjQUFUO0FBQ3ZCLFVBQUE7TUFBQSxJQUFBLEdBQU8sTUFBTSxDQUFDLGNBQVAsQ0FBc0IsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFoQixFQUFxQixDQUFyQixDQUFELEVBQTBCLGNBQTFCLENBQXRCOzBFQUNrQyxDQUFBLENBQUE7SUFGbEIsQ0E3UHpCO0lBaVFBLDRCQUFBLEVBQThCLFNBQUMsR0FBRDtBQUM1QixVQUFBO01BRDhCLHFDQUFnQjtNQUM5QyxNQUFBLEdBQVMsSUFBQyxDQUFBLHVCQUFELENBQXlCLE1BQXpCLEVBQWlDLGNBQWpDO01BQ1QsSUFBQSxDQUFtQixNQUFuQjtBQUFBLGVBQU8sS0FBUDs7TUFFQSxXQUFBLEdBQWM7QUFDZDtBQUFBLFdBQUEscUJBQUE7O1lBQXFELGVBQUEsQ0FBZ0IsY0FBaEIsRUFBZ0MsTUFBaEM7VUFDbkQsV0FBVyxDQUFDLElBQVosQ0FBaUIsSUFBQyxDQUFBLDZCQUFELENBQStCLGNBQS9CLEVBQStDLE1BQS9DLEVBQXVELE9BQXZELENBQWpCOztBQURGO2FBRUE7SUFQNEIsQ0FqUTlCO0lBMFFBLDZCQUFBLEVBQStCLFNBQUMsY0FBRCxFQUFpQixNQUFqQixFQUF5QixHQUF6QjtBQUM3QixVQUFBO01BRHVELHlCQUFVO01BQ2pFLFVBQUEsR0FDRTtRQUFBLElBQUEsRUFBTSxpQkFBTjtRQUNBLGlCQUFBLEVBQW1CLE1BRG5CO1FBRUEsV0FBQSxFQUFhLFdBRmI7UUFHQSxrQkFBQSxFQUF1QixVQUFELEdBQVksR0FBWixHQUFlLGNBSHJDOztNQUtGLElBQUcsZ0JBQUg7UUFDRSxVQUFVLENBQUMsT0FBWCxHQUF3QixjQUFELEdBQWdCLE9BQWhCLEdBQXVCLFFBQXZCLEdBQWdDLEtBRHpEO09BQUEsTUFBQTtRQUdFLFVBQVUsQ0FBQyxJQUFYLEdBQWtCLGVBSHBCOzthQUlBO0lBWDZCLENBMVEvQjtJQXVSQSxvQkFBQSxFQUFzQixTQUFDLE1BQUQsRUFBUyxjQUFUO0FBQ3BCLFVBQUE7TUFBQSxJQUFBLEdBQU8sTUFBTSxDQUFDLGNBQVAsQ0FBc0IsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFoQixFQUFxQixDQUFyQixDQUFELEVBQTBCLGNBQTFCLENBQXRCO3NFQUM4QixDQUFBLENBQUE7SUFGakIsQ0F2UnRCO0lBMlJBLGlCQUFBLEVBQW1CLFNBQUMsR0FBRDtBQUNqQixVQUFBO01BRG1CLHFDQUFnQixxQkFBUTtNQUMzQyxXQUFBLEdBQWM7TUFDZCxJQUFHLE1BQUg7QUFDRTtBQUFBLGFBQUEscUNBQUE7O2NBQXNCLGVBQUEsQ0FBZ0IsR0FBaEIsRUFBcUIsTUFBckI7WUFDcEIsV0FBVyxDQUFDLElBQVosQ0FBaUIsSUFBQyxDQUFBLGtCQUFELENBQW9CLEdBQXBCLENBQWpCOztBQURGLFNBREY7O2FBR0E7SUFMaUIsQ0EzUm5CO0lBa1NBLGtCQUFBLEVBQW9CLFNBQUMsR0FBRDthQUNsQjtRQUFBLElBQUEsRUFBTSxLQUFOO1FBQ0EsSUFBQSxFQUFNLEdBRE47UUFFQSxXQUFBLEVBQWEsZ0JBQUEsR0FBaUIsR0FBakIsR0FBcUIsWUFGbEM7O0lBRGtCLENBbFNwQjs7O0VBdVNGLHFCQUFBLEdBQXdCLFNBQUMsY0FBRCxFQUFpQixNQUFqQjtBQUN0QixRQUFBO0lBQUMsTUFBTztJQUNSLElBQUEsR0FBTyxNQUFNLENBQUMsb0JBQVAsQ0FBNEIsR0FBNUI7V0FDUCxPQUFPLENBQUMsSUFBUixDQUFhLElBQWI7RUFIc0I7O0VBS3hCLFFBQUEsR0FBVyxTQUFDLFdBQUQsRUFBYyxLQUFkLEVBQXFCLGFBQXJCOztNQUFxQixnQkFBZ0I7O1dBQzlDLFdBQVcsQ0FBQyxPQUFaLENBQW9CLEtBQXBCLENBQUEsS0FBZ0MsQ0FBQyxDQUFqQyxJQUNFLENBQUMsYUFBQSxJQUFrQixXQUFXLENBQUMsT0FBWixDQUF1QixLQUFELEdBQU8sZ0JBQTdCLENBQUEsS0FBbUQsQ0FBQyxDQUF2RTtFQUZPOztFQUlYLGVBQUEsR0FBa0IsU0FBQyxJQUFELEVBQU8sSUFBUDtXQUNoQixJQUFLLENBQUEsQ0FBQSxDQUFFLENBQUMsV0FBUixDQUFBLENBQUEsS0FBeUIsSUFBSyxDQUFBLENBQUEsQ0FBRSxDQUFDLFdBQVIsQ0FBQTtFQURUO0FBNVRsQiIsInNvdXJjZXNDb250ZW50IjpbIkNPTVBMRVRJT05TID0gcmVxdWlyZSgnLi4vY29tcGxldGlvbnMuanNvbicpXG5cbmZpcnN0SW5saW5lUHJvcGVydHlOYW1lV2l0aENvbG9uUGF0dGVybiA9IC97XFxzKihcXFMrKVxccyo6LyAjIC5leGFtcGxlIHsgZGlzcGxheTogfVxuaW5saW5lUHJvcGVydHlOYW1lV2l0aENvbG9uUGF0dGVybiA9IC8oPzo7Lis/KSo7XFxzKihcXFMrKVxccyo6LyAjIC5leGFtcGxlIHsgZGlzcGxheTogYmxvY2s7IGZsb2F0OiBsZWZ0OyBjb2xvcjogfSAobWF0Y2ggdGhlIGxhc3Qgb25lKVxucHJvcGVydHlOYW1lV2l0aENvbG9uUGF0dGVybiA9IC9eXFxzKihcXFMrKVxccyo6LyAjIGRpc3BsYXk6XG5wcm9wZXJ0eU5hbWVQcmVmaXhQYXR0ZXJuID0gL1thLXpBLVpdK1stYS16QS1aXSokL1xucHNldWRvU2VsZWN0b3JQcmVmaXhQYXR0ZXJuID0gLzooOik/KFthLXpdK1thLXotXSopPyQvXG50YWdTZWxlY3RvclByZWZpeFBhdHRlcm4gPSAvKF58XFxzfCwpKFthLXpdKyk/JC9cbmltcG9ydGFudFByZWZpeFBhdHRlcm4gPSAvKCFbYS16XSspJC9cbmNzc0RvY3NVUkwgPSBcImh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0NTU1wiXG5cbm1vZHVsZS5leHBvcnRzID1cbiAgc2VsZWN0b3I6ICcuc291cmNlLmNzcywgLnNvdXJjZS5zYXNzLCAuc291cmNlLmNzcy5wb3N0Y3NzJ1xuICBkaXNhYmxlRm9yU2VsZWN0b3I6ICcuc291cmNlLmNzcyAuY29tbWVudCwgLnNvdXJjZS5jc3MgLnN0cmluZywgLnNvdXJjZS5zYXNzIC5jb21tZW50LCAuc291cmNlLnNhc3MgLnN0cmluZywgLnNvdXJjZS5jc3MucG9zdGNzcyAuY29tbWVudCwgc291cmNlLmNzcy5wb3N0Y3NzIC5zdHJpbmcnXG4gIHByb3BlcnRpZXM6IENPTVBMRVRJT05TLnByb3BlcnRpZXNcbiAgcHNldWRvU2VsZWN0b3JzOiBDT01QTEVUSU9OUy5wc2V1ZG9TZWxlY3RvcnNcbiAgdGFnczogQ09NUExFVElPTlMudGFnc1xuXG4gICMgVGVsbCBhdXRvY29tcGxldGUgdG8gZnV6enkgZmlsdGVyIHRoZSByZXN1bHRzIG9mIGdldFN1Z2dlc3Rpb25zKCkuIFdlIGFyZVxuICAjIHN0aWxsIGZpbHRlcmluZyBieSB0aGUgZmlyc3QgY2hhcmFjdGVyIG9mIHRoZSBwcmVmaXggaW4gdGhpcyBwcm92aWRlciBmb3JcbiAgIyBlZmZpY2llbmN5LlxuICBmaWx0ZXJTdWdnZXN0aW9uczogdHJ1ZVxuXG4gIGdldFN1Z2dlc3Rpb25zOiAocmVxdWVzdCkgLT5cbiAgICBjb21wbGV0aW9ucyA9IG51bGxcbiAgICBzY29wZXMgPSByZXF1ZXN0LnNjb3BlRGVzY3JpcHRvci5nZXRTY29wZXNBcnJheSgpXG4gICAgaXNTYXNzID0gaGFzU2NvcGUoc2NvcGVzLCAnc291cmNlLnNhc3MnLCB0cnVlKVxuXG4gICAgaWYgQGlzQ29tcGxldGluZ1ZhbHVlKHJlcXVlc3QpXG4gICAgICBjb21wbGV0aW9ucyA9IEBnZXRQcm9wZXJ0eVZhbHVlQ29tcGxldGlvbnMocmVxdWVzdClcbiAgICBlbHNlIGlmIEBpc0NvbXBsZXRpbmdQc2V1ZG9TZWxlY3RvcihyZXF1ZXN0KVxuICAgICAgY29tcGxldGlvbnMgPSBAZ2V0UHNldWRvU2VsZWN0b3JDb21wbGV0aW9ucyhyZXF1ZXN0KVxuICAgIGVsc2VcbiAgICAgIGlmIGlzU2FzcyBhbmQgQGlzQ29tcGxldGluZ05hbWVPclRhZyhyZXF1ZXN0KVxuICAgICAgICBjb21wbGV0aW9ucyA9IEBnZXRQcm9wZXJ0eU5hbWVDb21wbGV0aW9ucyhyZXF1ZXN0KVxuICAgICAgICAgIC5jb25jYXQoQGdldFRhZ0NvbXBsZXRpb25zKHJlcXVlc3QpKVxuICAgICAgZWxzZSBpZiBub3QgaXNTYXNzIGFuZCBAaXNDb21wbGV0aW5nTmFtZShyZXF1ZXN0KVxuICAgICAgICBjb21wbGV0aW9ucyA9IEBnZXRQcm9wZXJ0eU5hbWVDb21wbGV0aW9ucyhyZXF1ZXN0KVxuXG4gICAgaWYgbm90IGlzU2FzcyBhbmQgQGlzQ29tcGxldGluZ1RhZ1NlbGVjdG9yKHJlcXVlc3QpXG4gICAgICB0YWdDb21wbGV0aW9ucyA9IEBnZXRUYWdDb21wbGV0aW9ucyhyZXF1ZXN0KVxuICAgICAgaWYgdGFnQ29tcGxldGlvbnM/Lmxlbmd0aFxuICAgICAgICBjb21wbGV0aW9ucyA/PSBbXVxuICAgICAgICBjb21wbGV0aW9ucyA9IGNvbXBsZXRpb25zLmNvbmNhdCh0YWdDb21wbGV0aW9ucylcblxuICAgIGNvbXBsZXRpb25zXG5cbiAgb25EaWRJbnNlcnRTdWdnZXN0aW9uOiAoe2VkaXRvciwgc3VnZ2VzdGlvbn0pIC0+XG4gICAgc2V0VGltZW91dChAdHJpZ2dlckF1dG9jb21wbGV0ZS5iaW5kKHRoaXMsIGVkaXRvciksIDEpIGlmIHN1Z2dlc3Rpb24udHlwZSBpcyAncHJvcGVydHknXG5cbiAgdHJpZ2dlckF1dG9jb21wbGV0ZTogKGVkaXRvcikgLT5cbiAgICBhdG9tLmNvbW1hbmRzLmRpc3BhdGNoKGF0b20udmlld3MuZ2V0VmlldyhlZGl0b3IpLCAnYXV0b2NvbXBsZXRlLXBsdXM6YWN0aXZhdGUnLCB7YWN0aXZhdGVkTWFudWFsbHk6IGZhbHNlfSlcblxuICBpc0NvbXBsZXRpbmdWYWx1ZTogKHtzY29wZURlc2NyaXB0b3IsIGJ1ZmZlclBvc2l0aW9uLCBwcmVmaXgsIGVkaXRvcn0pIC0+XG4gICAgc2NvcGVzID0gc2NvcGVEZXNjcmlwdG9yLmdldFNjb3Blc0FycmF5KClcblxuICAgIGJlZm9yZVByZWZpeEJ1ZmZlclBvc2l0aW9uID0gW2J1ZmZlclBvc2l0aW9uLnJvdywgTWF0aC5tYXgoMCwgYnVmZmVyUG9zaXRpb24uY29sdW1uIC0gcHJlZml4Lmxlbmd0aCAtIDEpXVxuICAgIGJlZm9yZVByZWZpeFNjb3BlcyA9IGVkaXRvci5zY29wZURlc2NyaXB0b3JGb3JCdWZmZXJQb3NpdGlvbihiZWZvcmVQcmVmaXhCdWZmZXJQb3NpdGlvbilcbiAgICBiZWZvcmVQcmVmaXhTY29wZXNBcnJheSA9IGJlZm9yZVByZWZpeFNjb3Blcy5nZXRTY29wZXNBcnJheSgpXG5cbiAgICBwcmV2aW91c0J1ZmZlclBvc2l0aW9uID0gW2J1ZmZlclBvc2l0aW9uLnJvdywgTWF0aC5tYXgoMCwgYnVmZmVyUG9zaXRpb24uY29sdW1uIC0gMSldXG4gICAgcHJldmlvdXNTY29wZXMgPSBlZGl0b3Iuc2NvcGVEZXNjcmlwdG9yRm9yQnVmZmVyUG9zaXRpb24ocHJldmlvdXNCdWZmZXJQb3NpdGlvbilcbiAgICBwcmV2aW91c1Njb3Blc0FycmF5ID0gcHJldmlvdXNTY29wZXMuZ2V0U2NvcGVzQXJyYXkoKVxuXG4gICAgKGhhc1Njb3BlKHNjb3BlcywgJ21ldGEucHJvcGVydHktbGlzdC5jc3MnKSBhbmQgcHJlZml4LnRyaW0oKSBpcyBcIjpcIikgb3JcbiAgICAoaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ21ldGEucHJvcGVydHktdmFsdWUuY3NzJykpIG9yXG4gICAgKGhhc1Njb3BlKHNjb3BlcywgJ21ldGEucHJvcGVydHktbGlzdC5zY3NzJykgYW5kIHByZWZpeC50cmltKCkgaXMgXCI6XCIpIG9yXG4gICAgKGhhc1Njb3BlKHByZXZpb3VzU2NvcGVzQXJyYXksICdtZXRhLnByb3BlcnR5LXZhbHVlLnNjc3MnKSkgb3JcbiAgICAoaGFzU2NvcGUoc2NvcGVzLCAnbWV0YS5wcm9wZXJ0eS1saXN0LnBvc3Rjc3MnKSBhbmQgcHJlZml4LnRyaW0oKSBpcyBcIjpcIikgb3JcbiAgICAoaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ21ldGEucHJvcGVydHktdmFsdWUucG9zdGNzcycpKSBvclxuICAgIChoYXNTY29wZShzY29wZXMsICdzb3VyY2Uuc2FzcycsIHRydWUpIGFuZCAoaGFzU2NvcGUoc2NvcGVzLCAnbWV0YS5wcm9wZXJ0eS12YWx1ZS5zYXNzJykgb3JcbiAgICAgIChub3QgaGFzU2NvcGUoYmVmb3JlUHJlZml4U2NvcGVzQXJyYXksICdlbnRpdHkubmFtZS50YWcuY3NzJykgYW5kIHByZWZpeC50cmltKCkgaXMgXCI6XCIpXG4gICAgKSlcblxuICBpc0NvbXBsZXRpbmdOYW1lOiAoe3Njb3BlRGVzY3JpcHRvciwgYnVmZmVyUG9zaXRpb24sIHByZWZpeCwgZWRpdG9yfSkgLT5cbiAgICBzY29wZXMgPSBzY29wZURlc2NyaXB0b3IuZ2V0U2NvcGVzQXJyYXkoKVxuICAgIGlzQXRUZXJtaW5hdG9yID0gcHJlZml4LmVuZHNXaXRoKCc7JylcbiAgICBpc0F0UGFyZW50U3ltYm9sID0gcHJlZml4LmVuZHNXaXRoKCcmJylcbiAgICBpc1ZhcmlhYmxlID0gaGFzU2NvcGUoc2NvcGVzLCAndmFyaWFibGUuY3NzJykgb3JcbiAgICAgIGhhc1Njb3BlKHNjb3BlcywgJ3ZhcmlhYmxlLnNjc3MnKSBvclxuICAgICAgaGFzU2NvcGUoc2NvcGVzLCAndmFyaWFibGUudmFyLnBvc3Rjc3MnKVxuICAgIGlzSW5Qcm9wZXJ0eUxpc3QgPSBub3QgaXNBdFRlcm1pbmF0b3IgYW5kXG4gICAgICAoaGFzU2NvcGUoc2NvcGVzLCAnbWV0YS5wcm9wZXJ0eS1saXN0LmNzcycpIG9yXG4gICAgICBoYXNTY29wZShzY29wZXMsICdtZXRhLnByb3BlcnR5LWxpc3Quc2NzcycpIG9yXG4gICAgICBoYXNTY29wZShzY29wZXMsICdtZXRhLnByb3BlcnR5LWxpc3QucG9zdGNzcycpKVxuXG4gICAgcmV0dXJuIGZhbHNlIHVubGVzcyBpc0luUHJvcGVydHlMaXN0XG4gICAgcmV0dXJuIGZhbHNlIGlmIGlzQXRQYXJlbnRTeW1ib2wgb3IgaXNWYXJpYWJsZVxuXG4gICAgcHJldmlvdXNCdWZmZXJQb3NpdGlvbiA9IFtidWZmZXJQb3NpdGlvbi5yb3csIE1hdGgubWF4KDAsIGJ1ZmZlclBvc2l0aW9uLmNvbHVtbiAtIHByZWZpeC5sZW5ndGggLSAxKV1cbiAgICBwcmV2aW91c1Njb3BlcyA9IGVkaXRvci5zY29wZURlc2NyaXB0b3JGb3JCdWZmZXJQb3NpdGlvbihwcmV2aW91c0J1ZmZlclBvc2l0aW9uKVxuICAgIHByZXZpb3VzU2NvcGVzQXJyYXkgPSBwcmV2aW91c1Njb3Blcy5nZXRTY29wZXNBcnJheSgpXG5cbiAgICByZXR1cm4gZmFsc2UgaWYgaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ2VudGl0eS5vdGhlci5hdHRyaWJ1dGUtbmFtZS5jbGFzcy5jc3MnKSBvclxuICAgICAgaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ2VudGl0eS5vdGhlci5hdHRyaWJ1dGUtbmFtZS5pZC5jc3MnKSBvclxuICAgICAgaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ2VudGl0eS5vdGhlci5hdHRyaWJ1dGUtbmFtZS5pZCcpIG9yXG4gICAgICBoYXNTY29wZShwcmV2aW91c1Njb3Blc0FycmF5LCAnZW50aXR5Lm90aGVyLmF0dHJpYnV0ZS1uYW1lLnBhcmVudC1zZWxlY3Rvci5jc3MnKSBvclxuICAgICAgaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ2VudGl0eS5uYW1lLnRhZy5yZWZlcmVuY2Uuc2NzcycpIG9yXG4gICAgICBoYXNTY29wZShwcmV2aW91c1Njb3Blc0FycmF5LCAnZW50aXR5Lm5hbWUudGFnLnNjc3MnKSBvclxuICAgICAgaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ2VudGl0eS5uYW1lLnRhZy5yZWZlcmVuY2UucG9zdGNzcycpIG9yXG4gICAgICBoYXNTY29wZShwcmV2aW91c1Njb3Blc0FycmF5LCAnZW50aXR5Lm5hbWUudGFnLnBvc3Rjc3MnKVxuXG4gICAgaXNBdEJlZ2luU2NvcGVQdW5jdHVhdGlvbiA9IGhhc1Njb3BlKHNjb3BlcywgJ3B1bmN0dWF0aW9uLnNlY3Rpb24ucHJvcGVydHktbGlzdC5iZWdpbi5icmFja2V0LmN1cmx5LmNzcycpIG9yXG4gICAgICBoYXNTY29wZShzY29wZXMsICdwdW5jdHVhdGlvbi5zZWN0aW9uLnByb3BlcnR5LWxpc3QuYmVnaW4uYnJhY2tldC5jdXJseS5zY3NzJykgb3JcbiAgICAgIGhhc1Njb3BlKHNjb3BlcywgJ3B1bmN0dWF0aW9uLnNlY3Rpb24ucHJvcGVydHktbGlzdC5iZWdpbi5wb3N0Y3NzJylcbiAgICBpc0F0RW5kU2NvcGVQdW5jdHVhdGlvbiA9IGhhc1Njb3BlKHNjb3BlcywgJ3B1bmN0dWF0aW9uLnNlY3Rpb24ucHJvcGVydHktbGlzdC5lbmQuYnJhY2tldC5jdXJseS5jc3MnKSBvclxuICAgICAgaGFzU2NvcGUoc2NvcGVzLCAncHVuY3R1YXRpb24uc2VjdGlvbi5wcm9wZXJ0eS1saXN0LmVuZC5icmFja2V0LmN1cmx5LnNjc3MnKSBvclxuICAgICAgaGFzU2NvcGUoc2NvcGVzLCAncHVuY3R1YXRpb24uc2VjdGlvbi5wcm9wZXJ0eS1saXN0LmVuZC5wb3N0Y3NzJylcblxuICAgIGlmIGlzQXRCZWdpblNjb3BlUHVuY3R1YXRpb25cbiAgICAgICMgKiBEaXNhbGxvdyBoZXJlOiBgY2FudmFzLHx7fWBcbiAgICAgICMgKiBBbGxvdyBoZXJlOiBgY2FudmFzLHt8IH1gXG4gICAgICBwcmVmaXguZW5kc1dpdGgoJ3snKVxuICAgIGVsc2UgaWYgaXNBdEVuZFNjb3BlUHVuY3R1YXRpb25cbiAgICAgICMgKiBEaXNhbGxvdyBoZXJlOiBgY2FudmFzLHt9fGBcbiAgICAgICMgKiBBbGxvdyBoZXJlOiBgY2FudmFzLHsgfH1gXG4gICAgICBub3QgcHJlZml4LmVuZHNXaXRoKCd9JylcbiAgICBlbHNlXG4gICAgICB0cnVlXG5cbiAgaXNDb21wbGV0aW5nTmFtZU9yVGFnOiAoe3Njb3BlRGVzY3JpcHRvciwgYnVmZmVyUG9zaXRpb24sIGVkaXRvcn0pIC0+XG4gICAgc2NvcGVzID0gc2NvcGVEZXNjcmlwdG9yLmdldFNjb3Blc0FycmF5KClcbiAgICBwcmVmaXggPSBAZ2V0UHJvcGVydHlOYW1lUHJlZml4KGJ1ZmZlclBvc2l0aW9uLCBlZGl0b3IpXG4gICAgcmV0dXJuIEBpc1Byb3BlcnR5TmFtZVByZWZpeChwcmVmaXgpIGFuZFxuICAgICAgaGFzU2NvcGUoc2NvcGVzLCAnbWV0YS5zZWxlY3Rvci5jc3MnKSBhbmRcbiAgICAgIG5vdCBoYXNTY29wZShzY29wZXMsICdlbnRpdHkub3RoZXIuYXR0cmlidXRlLW5hbWUuaWQuY3NzLnNhc3MnKSBhbmRcbiAgICAgIG5vdCBoYXNTY29wZShzY29wZXMsICdlbnRpdHkub3RoZXIuYXR0cmlidXRlLW5hbWUuY2xhc3Muc2FzcycpXG5cbiAgaXNDb21wbGV0aW5nVGFnU2VsZWN0b3I6ICh7ZWRpdG9yLCBzY29wZURlc2NyaXB0b3IsIGJ1ZmZlclBvc2l0aW9ufSkgLT5cbiAgICBzY29wZXMgPSBzY29wZURlc2NyaXB0b3IuZ2V0U2NvcGVzQXJyYXkoKVxuICAgIHRhZ1NlbGVjdG9yUHJlZml4ID0gQGdldFRhZ1NlbGVjdG9yUHJlZml4KGVkaXRvciwgYnVmZmVyUG9zaXRpb24pXG4gICAgcmV0dXJuIGZhbHNlIHVubGVzcyB0YWdTZWxlY3RvclByZWZpeD8ubGVuZ3RoXG5cbiAgICBwcmV2aW91c0J1ZmZlclBvc2l0aW9uID0gW2J1ZmZlclBvc2l0aW9uLnJvdywgTWF0aC5tYXgoMCwgYnVmZmVyUG9zaXRpb24uY29sdW1uIC0gMSldXG4gICAgcHJldmlvdXNTY29wZXMgPSBlZGl0b3Iuc2NvcGVEZXNjcmlwdG9yRm9yQnVmZmVyUG9zaXRpb24ocHJldmlvdXNCdWZmZXJQb3NpdGlvbilcbiAgICBwcmV2aW91c1Njb3Blc0FycmF5ID0gcHJldmlvdXNTY29wZXMuZ2V0U2NvcGVzQXJyYXkoKVxuXG4gICAgaWYgaGFzU2NvcGUoc2NvcGVzLCAnbWV0YS5zZWxlY3Rvci5jc3MnKSBvciBoYXNTY29wZShwcmV2aW91c1Njb3Blc0FycmF5LCAnbWV0YS5zZWxlY3Rvci5jc3MnKVxuICAgICAgdHJ1ZVxuICAgIGVsc2UgaWYgaGFzU2NvcGUoc2NvcGVzLCAnc291cmNlLmNzcy5zY3NzJywgdHJ1ZSkgb3IgaGFzU2NvcGUoc2NvcGVzLCAnc291cmNlLmNzcy5sZXNzJywgdHJ1ZSkgb3IgaGFzU2NvcGUoc2NvcGVzLCAnc291cmNlLmNzcy5wb3N0Y3NzJywgdHJ1ZSlcbiAgICAgIG5vdCBoYXNTY29wZShwcmV2aW91c1Njb3Blc0FycmF5LCAnbWV0YS5wcm9wZXJ0eS12YWx1ZS5zY3NzJykgYW5kXG4gICAgICAgIG5vdCBoYXNTY29wZShwcmV2aW91c1Njb3Blc0FycmF5LCAnbWV0YS5wcm9wZXJ0eS12YWx1ZS5jc3MnKSBhbmRcbiAgICAgICAgbm90IGhhc1Njb3BlKHByZXZpb3VzU2NvcGVzQXJyYXksICdtZXRhLnByb3BlcnR5LXZhbHVlLnBvc3Rjc3MnKSBhbmRcbiAgICAgICAgbm90IGhhc1Njb3BlKHByZXZpb3VzU2NvcGVzQXJyYXksICdzdXBwb3J0LnR5cGUucHJvcGVydHktdmFsdWUuY3NzJylcbiAgICBlbHNlXG4gICAgICBmYWxzZVxuXG4gIGlzQ29tcGxldGluZ1BzZXVkb1NlbGVjdG9yOiAoe2VkaXRvciwgc2NvcGVEZXNjcmlwdG9yLCBidWZmZXJQb3NpdGlvbn0pIC0+XG4gICAgc2NvcGVzID0gc2NvcGVEZXNjcmlwdG9yLmdldFNjb3Blc0FycmF5KClcbiAgICBwcmV2aW91c0J1ZmZlclBvc2l0aW9uID0gW2J1ZmZlclBvc2l0aW9uLnJvdywgTWF0aC5tYXgoMCwgYnVmZmVyUG9zaXRpb24uY29sdW1uIC0gMSldXG4gICAgcHJldmlvdXNTY29wZXMgPSBlZGl0b3Iuc2NvcGVEZXNjcmlwdG9yRm9yQnVmZmVyUG9zaXRpb24ocHJldmlvdXNCdWZmZXJQb3NpdGlvbilcbiAgICBwcmV2aW91c1Njb3Blc0FycmF5ID0gcHJldmlvdXNTY29wZXMuZ2V0U2NvcGVzQXJyYXkoKVxuICAgIGlmIChoYXNTY29wZShzY29wZXMsICdtZXRhLnNlbGVjdG9yLmNzcycpIG9yIGhhc1Njb3BlKHByZXZpb3VzU2NvcGVzQXJyYXksICdtZXRhLnNlbGVjdG9yLmNzcycpKSBhbmQgbm90IGhhc1Njb3BlKHNjb3BlcywgJ3NvdXJjZS5zYXNzJywgdHJ1ZSlcbiAgICAgIHRydWVcbiAgICBlbHNlIGlmIGhhc1Njb3BlKHNjb3BlcywgJ3NvdXJjZS5jc3Muc2NzcycsIHRydWUpIG9yIGhhc1Njb3BlKHNjb3BlcywgJ3NvdXJjZS5jc3MubGVzcycsIHRydWUpIG9yIGhhc1Njb3BlKHNjb3BlcywgJ3NvdXJjZS5zYXNzJywgdHJ1ZSkgb3IgaGFzU2NvcGUoc2NvcGVzLCAnc291cmNlLmNzcy5wb3N0Y3NzJywgdHJ1ZSlcbiAgICAgIHByZWZpeCA9IEBnZXRQc2V1ZG9TZWxlY3RvclByZWZpeChlZGl0b3IsIGJ1ZmZlclBvc2l0aW9uKVxuICAgICAgaWYgcHJlZml4XG4gICAgICAgIHByZXZpb3VzQnVmZmVyUG9zaXRpb24gPSBbYnVmZmVyUG9zaXRpb24ucm93LCBNYXRoLm1heCgwLCBidWZmZXJQb3NpdGlvbi5jb2x1bW4gLSBwcmVmaXgubGVuZ3RoIC0gMSldXG4gICAgICAgIHByZXZpb3VzU2NvcGVzID0gZWRpdG9yLnNjb3BlRGVzY3JpcHRvckZvckJ1ZmZlclBvc2l0aW9uKHByZXZpb3VzQnVmZmVyUG9zaXRpb24pXG4gICAgICAgIHByZXZpb3VzU2NvcGVzQXJyYXkgPSBwcmV2aW91c1Njb3Blcy5nZXRTY29wZXNBcnJheSgpXG4gICAgICAgIG5vdCBoYXNTY29wZShwcmV2aW91c1Njb3Blc0FycmF5LCAnbWV0YS5wcm9wZXJ0eS1uYW1lLnNjc3MnKSBhbmRcbiAgICAgICAgICBub3QgaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ21ldGEucHJvcGVydHktdmFsdWUuc2NzcycpIGFuZFxuICAgICAgICAgIG5vdCBoYXNTY29wZShwcmV2aW91c1Njb3Blc0FycmF5LCAnbWV0YS5wcm9wZXJ0eS12YWx1ZS5wb3N0Y3NzJykgYW5kXG4gICAgICAgICAgbm90IGhhc1Njb3BlKHByZXZpb3VzU2NvcGVzQXJyYXksICdzdXBwb3J0LnR5cGUucHJvcGVydHktbmFtZS5jc3MnKSBhbmRcbiAgICAgICAgICBub3QgaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ3N1cHBvcnQudHlwZS5wcm9wZXJ0eS12YWx1ZS5jc3MnKSBhbmRcbiAgICAgICAgICBub3QgaGFzU2NvcGUocHJldmlvdXNTY29wZXNBcnJheSwgJ3N1cHBvcnQudHlwZS5wcm9wZXJ0eS1uYW1lLnBvc3Rjc3MnKVxuICAgICAgZWxzZVxuICAgICAgICBmYWxzZVxuICAgIGVsc2VcbiAgICAgIGZhbHNlXG5cbiAgaXNQcm9wZXJ0eVZhbHVlUHJlZml4OiAocHJlZml4KSAtPlxuICAgIHByZWZpeCA9IHByZWZpeC50cmltKClcbiAgICBwcmVmaXgubGVuZ3RoID4gMCBhbmQgcHJlZml4IGlzbnQgJzonXG5cbiAgaXNQcm9wZXJ0eU5hbWVQcmVmaXg6IChwcmVmaXgpIC0+XG4gICAgcmV0dXJuIGZhbHNlIHVubGVzcyBwcmVmaXg/XG4gICAgcHJlZml4ID0gcHJlZml4LnRyaW0oKVxuICAgIHByZWZpeC5sZW5ndGggPiAwIGFuZCBwcmVmaXgubWF0Y2goL15bYS16QS1aLV0rJC8pXG5cbiAgZ2V0SW1wb3J0YW50UHJlZml4OiAoZWRpdG9yLCBidWZmZXJQb3NpdGlvbikgLT5cbiAgICBsaW5lID0gZWRpdG9yLmdldFRleHRJblJhbmdlKFtbYnVmZmVyUG9zaXRpb24ucm93LCAwXSwgYnVmZmVyUG9zaXRpb25dKVxuICAgIGltcG9ydGFudFByZWZpeFBhdHRlcm4uZXhlYyhsaW5lKT9bMV1cblxuICBnZXRQcmV2aW91c1Byb3BlcnR5TmFtZTogKGJ1ZmZlclBvc2l0aW9uLCBlZGl0b3IpIC0+XG4gICAge3JvdywgY29sdW1ufSA9IGJ1ZmZlclBvc2l0aW9uXG4gICAgd2hpbGUgcm93ID49IDBcbiAgICAgIGxpbmUgPSBlZGl0b3IubGluZVRleHRGb3JCdWZmZXJSb3cocm93KVxuICAgICAgbGluZSA9IGxpbmUuc3Vic3RyKDAsIGNvbHVtbikgaWYgcm93IGlzIGJ1ZmZlclBvc2l0aW9uLnJvd1xuICAgICAgcHJvcGVydHlOYW1lID0gaW5saW5lUHJvcGVydHlOYW1lV2l0aENvbG9uUGF0dGVybi5leGVjKGxpbmUpP1sxXVxuICAgICAgcHJvcGVydHlOYW1lID89IGZpcnN0SW5saW5lUHJvcGVydHlOYW1lV2l0aENvbG9uUGF0dGVybi5leGVjKGxpbmUpP1sxXVxuICAgICAgcHJvcGVydHlOYW1lID89IHByb3BlcnR5TmFtZVdpdGhDb2xvblBhdHRlcm4uZXhlYyhsaW5lKT9bMV1cbiAgICAgIHJldHVybiBwcm9wZXJ0eU5hbWUgaWYgcHJvcGVydHlOYW1lXG4gICAgICByb3ctLVxuICAgIHJldHVyblxuXG4gIGdldFByb3BlcnR5VmFsdWVDb21wbGV0aW9uczogKHtidWZmZXJQb3NpdGlvbiwgZWRpdG9yLCBwcmVmaXgsIHNjb3BlRGVzY3JpcHRvcn0pIC0+XG4gICAgcHJvcGVydHkgPSBAZ2V0UHJldmlvdXNQcm9wZXJ0eU5hbWUoYnVmZmVyUG9zaXRpb24sIGVkaXRvcilcbiAgICB2YWx1ZXMgPSBAcHJvcGVydGllc1twcm9wZXJ0eV0/LnZhbHVlc1xuICAgIHJldHVybiBudWxsIHVubGVzcyB2YWx1ZXM/XG5cbiAgICBzY29wZXMgPSBzY29wZURlc2NyaXB0b3IuZ2V0U2NvcGVzQXJyYXkoKVxuICAgIGFkZFNlbWljb2xvbiA9IG5vdCBsaW5lRW5kc1dpdGhTZW1pY29sb24oYnVmZmVyUG9zaXRpb24sIGVkaXRvcikgYW5kIG5vdCBoYXNTY29wZShzY29wZXMsICdzb3VyY2Uuc2FzcycsIHRydWUpXG5cbiAgICBjb21wbGV0aW9ucyA9IFtdXG4gICAgaWYgQGlzUHJvcGVydHlWYWx1ZVByZWZpeChwcmVmaXgpXG4gICAgICBmb3IgdmFsdWUgaW4gdmFsdWVzIHdoZW4gZmlyc3RDaGFyc0VxdWFsKHZhbHVlLCBwcmVmaXgpXG4gICAgICAgIGNvbXBsZXRpb25zLnB1c2goQGJ1aWxkUHJvcGVydHlWYWx1ZUNvbXBsZXRpb24odmFsdWUsIHByb3BlcnR5LCBhZGRTZW1pY29sb24pKVxuICAgIGVsc2UgaWYgbm90IGhhc1Njb3BlKHNjb3BlcywgJ2tleXdvcmQub3RoZXIudW5pdC5wZXJjZW50YWdlLmNzcycpIGFuZCAjIENTU1xuICAgIG5vdCBoYXNTY29wZShzY29wZXMsICdrZXl3b3JkLm90aGVyLnVuaXQuc2NzcycpIGFuZCAjIFNDU1MgKFRPRE86IHJlbW92ZSBpbiBBdG9tIDEuMTkuMClcbiAgICBub3QgaGFzU2NvcGUoc2NvcGVzLCAna2V5d29yZC5vdGhlci51bml0LmNzcycpICMgTGVzcywgU2FzcyAoVE9ETzogcmVtb3ZlIGluIEF0b20gMS4xOS4wKVxuICAgICAgIyBEb24ndCBjb21wbGV0ZSBoZXJlOiBgd2lkdGg6IDEwMCV8YFxuICAgICAgZm9yIHZhbHVlIGluIHZhbHVlc1xuICAgICAgICBjb21wbGV0aW9ucy5wdXNoKEBidWlsZFByb3BlcnR5VmFsdWVDb21wbGV0aW9uKHZhbHVlLCBwcm9wZXJ0eSwgYWRkU2VtaWNvbG9uKSlcblxuICAgIGlmIGltcG9ydGFudFByZWZpeCA9IEBnZXRJbXBvcnRhbnRQcmVmaXgoZWRpdG9yLCBidWZmZXJQb3NpdGlvbilcbiAgICAgICMgYXR0ZW50aW9uOiByw6hnbGUgZGFuZ2VyZXV4XG4gICAgICBjb21wbGV0aW9ucy5wdXNoXG4gICAgICAgIHR5cGU6ICdrZXl3b3JkJ1xuICAgICAgICB0ZXh0OiAnIWltcG9ydGFudCdcbiAgICAgICAgZGlzcGxheVRleHQ6ICchaW1wb3J0YW50J1xuICAgICAgICByZXBsYWNlbWVudFByZWZpeDogaW1wb3J0YW50UHJlZml4XG4gICAgICAgIGRlc2NyaXB0aW9uOiBcIkZvcmNlcyB0aGlzIHByb3BlcnR5IHRvIG92ZXJyaWRlIGFueSBvdGhlciBkZWNsYXJhdGlvbiBvZiB0aGUgc2FtZSBwcm9wZXJ0eS4gVXNlIHdpdGggY2F1dGlvbi5cIlxuICAgICAgICBkZXNjcmlwdGlvbk1vcmVVUkw6IFwiI3tjc3NEb2NzVVJMfS9TcGVjaWZpY2l0eSNUaGVfIWltcG9ydGFudF9leGNlcHRpb25cIlxuXG4gICAgY29tcGxldGlvbnNcblxuICBidWlsZFByb3BlcnR5VmFsdWVDb21wbGV0aW9uOiAodmFsdWUsIHByb3BlcnR5TmFtZSwgYWRkU2VtaWNvbG9uKSAtPlxuICAgIHRleHQgPSB2YWx1ZVxuICAgIHRleHQgKz0gJzsnIGlmIGFkZFNlbWljb2xvblxuXG4gICAge1xuICAgICAgdHlwZTogJ3ZhbHVlJ1xuICAgICAgdGV4dDogdGV4dFxuICAgICAgZGlzcGxheVRleHQ6IHZhbHVlXG4gICAgICBkZXNjcmlwdGlvbjogXCIje3ZhbHVlfSB2YWx1ZSBmb3IgdGhlICN7cHJvcGVydHlOYW1lfSBwcm9wZXJ0eVwiXG4gICAgICBkZXNjcmlwdGlvbk1vcmVVUkw6IFwiI3tjc3NEb2NzVVJMfS8je3Byb3BlcnR5TmFtZX0jVmFsdWVzXCJcbiAgICB9XG5cbiAgZ2V0UHJvcGVydHlOYW1lUHJlZml4OiAoYnVmZmVyUG9zaXRpb24sIGVkaXRvcikgLT5cbiAgICBsaW5lID0gZWRpdG9yLmdldFRleHRJblJhbmdlKFtbYnVmZmVyUG9zaXRpb24ucm93LCAwXSwgYnVmZmVyUG9zaXRpb25dKVxuICAgIHByb3BlcnR5TmFtZVByZWZpeFBhdHRlcm4uZXhlYyhsaW5lKT9bMF1cblxuICBnZXRQcm9wZXJ0eU5hbWVDb21wbGV0aW9uczogKHtidWZmZXJQb3NpdGlvbiwgZWRpdG9yLCBzY29wZURlc2NyaXB0b3IsIGFjdGl2YXRlZE1hbnVhbGx5fSkgLT5cbiAgICAjIERvbid0IGF1dG9jb21wbGV0ZSBwcm9wZXJ0eSBuYW1lcyBpbiBTQVNTIG9uIHJvb3QgbGV2ZWxcbiAgICBzY29wZXMgPSBzY29wZURlc2NyaXB0b3IuZ2V0U2NvcGVzQXJyYXkoKVxuICAgIGxpbmUgPSBlZGl0b3IuZ2V0VGV4dEluUmFuZ2UoW1tidWZmZXJQb3NpdGlvbi5yb3csIDBdLCBidWZmZXJQb3NpdGlvbl0pXG4gICAgcmV0dXJuIFtdIGlmIGhhc1Njb3BlKHNjb3BlcywgJ3NvdXJjZS5zYXNzJywgdHJ1ZSkgYW5kIG5vdCBsaW5lLm1hdGNoKC9eKFxcc3xcXHQpLylcblxuICAgIHByZWZpeCA9IEBnZXRQcm9wZXJ0eU5hbWVQcmVmaXgoYnVmZmVyUG9zaXRpb24sIGVkaXRvcilcbiAgICByZXR1cm4gW10gdW5sZXNzIGFjdGl2YXRlZE1hbnVhbGx5IG9yIHByZWZpeFxuXG4gICAgY29tcGxldGlvbnMgPSBbXVxuICAgIGZvciBwcm9wZXJ0eSwgb3B0aW9ucyBvZiBAcHJvcGVydGllcyB3aGVuIG5vdCBwcmVmaXggb3IgZmlyc3RDaGFyc0VxdWFsKHByb3BlcnR5LCBwcmVmaXgpXG4gICAgICBjb21wbGV0aW9ucy5wdXNoKEBidWlsZFByb3BlcnR5TmFtZUNvbXBsZXRpb24ocHJvcGVydHksIHByZWZpeCwgb3B0aW9ucykpXG4gICAgY29tcGxldGlvbnNcblxuICBidWlsZFByb3BlcnR5TmFtZUNvbXBsZXRpb246IChwcm9wZXJ0eU5hbWUsIHByZWZpeCwge2Rlc2NyaXB0aW9ufSkgLT5cbiAgICB0eXBlOiAncHJvcGVydHknXG4gICAgdGV4dDogXCIje3Byb3BlcnR5TmFtZX06IFwiXG4gICAgZGlzcGxheVRleHQ6IHByb3BlcnR5TmFtZVxuICAgIHJlcGxhY2VtZW50UHJlZml4OiBwcmVmaXhcbiAgICBkZXNjcmlwdGlvbjogZGVzY3JpcHRpb25cbiAgICBkZXNjcmlwdGlvbk1vcmVVUkw6IFwiI3tjc3NEb2NzVVJMfS8je3Byb3BlcnR5TmFtZX1cIlxuXG4gIGdldFBzZXVkb1NlbGVjdG9yUHJlZml4OiAoZWRpdG9yLCBidWZmZXJQb3NpdGlvbikgLT5cbiAgICBsaW5lID0gZWRpdG9yLmdldFRleHRJblJhbmdlKFtbYnVmZmVyUG9zaXRpb24ucm93LCAwXSwgYnVmZmVyUG9zaXRpb25dKVxuICAgIGxpbmUubWF0Y2gocHNldWRvU2VsZWN0b3JQcmVmaXhQYXR0ZXJuKT9bMF1cblxuICBnZXRQc2V1ZG9TZWxlY3RvckNvbXBsZXRpb25zOiAoe2J1ZmZlclBvc2l0aW9uLCBlZGl0b3J9KSAtPlxuICAgIHByZWZpeCA9IEBnZXRQc2V1ZG9TZWxlY3RvclByZWZpeChlZGl0b3IsIGJ1ZmZlclBvc2l0aW9uKVxuICAgIHJldHVybiBudWxsIHVubGVzcyBwcmVmaXhcblxuICAgIGNvbXBsZXRpb25zID0gW11cbiAgICBmb3IgcHNldWRvU2VsZWN0b3IsIG9wdGlvbnMgb2YgQHBzZXVkb1NlbGVjdG9ycyB3aGVuIGZpcnN0Q2hhcnNFcXVhbChwc2V1ZG9TZWxlY3RvciwgcHJlZml4KVxuICAgICAgY29tcGxldGlvbnMucHVzaChAYnVpbGRQc2V1ZG9TZWxlY3RvckNvbXBsZXRpb24ocHNldWRvU2VsZWN0b3IsIHByZWZpeCwgb3B0aW9ucykpXG4gICAgY29tcGxldGlvbnNcblxuICBidWlsZFBzZXVkb1NlbGVjdG9yQ29tcGxldGlvbjogKHBzZXVkb1NlbGVjdG9yLCBwcmVmaXgsIHthcmd1bWVudCwgZGVzY3JpcHRpb259KSAtPlxuICAgIGNvbXBsZXRpb24gPVxuICAgICAgdHlwZTogJ3BzZXVkby1zZWxlY3RvcidcbiAgICAgIHJlcGxhY2VtZW50UHJlZml4OiBwcmVmaXhcbiAgICAgIGRlc2NyaXB0aW9uOiBkZXNjcmlwdGlvblxuICAgICAgZGVzY3JpcHRpb25Nb3JlVVJMOiBcIiN7Y3NzRG9jc1VSTH0vI3twc2V1ZG9TZWxlY3Rvcn1cIlxuXG4gICAgaWYgYXJndW1lbnQ/XG4gICAgICBjb21wbGV0aW9uLnNuaXBwZXQgPSBcIiN7cHNldWRvU2VsZWN0b3J9KCR7MToje2FyZ3VtZW50fX0pXCJcbiAgICBlbHNlXG4gICAgICBjb21wbGV0aW9uLnRleHQgPSBwc2V1ZG9TZWxlY3RvclxuICAgIGNvbXBsZXRpb25cblxuICBnZXRUYWdTZWxlY3RvclByZWZpeDogKGVkaXRvciwgYnVmZmVyUG9zaXRpb24pIC0+XG4gICAgbGluZSA9IGVkaXRvci5nZXRUZXh0SW5SYW5nZShbW2J1ZmZlclBvc2l0aW9uLnJvdywgMF0sIGJ1ZmZlclBvc2l0aW9uXSlcbiAgICB0YWdTZWxlY3RvclByZWZpeFBhdHRlcm4uZXhlYyhsaW5lKT9bMl1cblxuICBnZXRUYWdDb21wbGV0aW9uczogKHtidWZmZXJQb3NpdGlvbiwgZWRpdG9yLCBwcmVmaXh9KSAtPlxuICAgIGNvbXBsZXRpb25zID0gW11cbiAgICBpZiBwcmVmaXhcbiAgICAgIGZvciB0YWcgaW4gQHRhZ3Mgd2hlbiBmaXJzdENoYXJzRXF1YWwodGFnLCBwcmVmaXgpXG4gICAgICAgIGNvbXBsZXRpb25zLnB1c2goQGJ1aWxkVGFnQ29tcGxldGlvbih0YWcpKVxuICAgIGNvbXBsZXRpb25zXG5cbiAgYnVpbGRUYWdDb21wbGV0aW9uOiAodGFnKSAtPlxuICAgIHR5cGU6ICd0YWcnXG4gICAgdGV4dDogdGFnXG4gICAgZGVzY3JpcHRpb246IFwiU2VsZWN0b3IgZm9yIDwje3RhZ30+IGVsZW1lbnRzXCJcblxubGluZUVuZHNXaXRoU2VtaWNvbG9uID0gKGJ1ZmZlclBvc2l0aW9uLCBlZGl0b3IpIC0+XG4gIHtyb3d9ID0gYnVmZmVyUG9zaXRpb25cbiAgbGluZSA9IGVkaXRvci5saW5lVGV4dEZvckJ1ZmZlclJvdyhyb3cpXG4gIC87XFxzKiQvLnRlc3QobGluZSlcblxuaGFzU2NvcGUgPSAoc2NvcGVzQXJyYXksIHNjb3BlLCBjaGVja0VtYmVkZGVkID0gZmFsc2UpIC0+XG4gIHNjb3Blc0FycmF5LmluZGV4T2Yoc2NvcGUpIGlzbnQgLTEgb3JcbiAgICAoY2hlY2tFbWJlZGRlZCBhbmQgc2NvcGVzQXJyYXkuaW5kZXhPZihcIiN7c2NvcGV9LmVtYmVkZGVkLmh0bWxcIikgaXNudCAtMSlcblxuZmlyc3RDaGFyc0VxdWFsID0gKHN0cjEsIHN0cjIpIC0+XG4gIHN0cjFbMF0udG9Mb3dlckNhc2UoKSBpcyBzdHIyWzBdLnRvTG93ZXJDYXNlKClcbiJdfQ==
