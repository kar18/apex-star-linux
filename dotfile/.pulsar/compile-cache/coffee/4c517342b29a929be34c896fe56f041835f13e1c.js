(function() {
  var LaunchModeView;

  module.exports = LaunchModeView = (function() {
    function LaunchModeView(arg) {
      var devMode, ref, safeMode;
      ref = arg != null ? arg : {}, safeMode = ref.safeMode, devMode = ref.devMode;
      this.element = document.createElement('status-bar-launch-mode');
      this.element.classList.add('inline-block', 'icon', 'icon-color-mode');
      if (devMode) {
        this.element.classList.add('text-error');
        this.tooltipDisposable = atom.tooltips.add(this.element, {
          title: 'This window is in dev mode'
        });
      } else if (safeMode) {
        this.element.classList.add('text-success');
        this.tooltipDisposable = atom.tooltips.add(this.element, {
          title: 'This window is in safe mode'
        });
      }
    }

    LaunchModeView.prototype.detachedCallback = function() {
      var ref;
      return (ref = this.tooltipDisposable) != null ? ref.dispose() : void 0;
    };

    return LaunchModeView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zdGF0dXMtYmFyL2xpYi9sYXVuY2gtbW9kZS12aWV3LmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsTUFBTSxDQUFDLE9BQVAsR0FDTTtJQUNTLHdCQUFDLEdBQUQ7QUFDWCxVQUFBOzBCQURZLE1BQW9CLElBQW5CLHlCQUFVO01BQ3ZCLElBQUMsQ0FBQSxPQUFELEdBQVcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsd0JBQXZCO01BQ1gsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsY0FBdkIsRUFBdUMsTUFBdkMsRUFBK0MsaUJBQS9DO01BQ0EsSUFBRyxPQUFIO1FBQ0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsWUFBdkI7UUFDQSxJQUFDLENBQUEsaUJBQUQsR0FBcUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLElBQUMsQ0FBQSxPQUFuQixFQUE0QjtVQUFBLEtBQUEsRUFBTyw0QkFBUDtTQUE1QixFQUZ2QjtPQUFBLE1BR0ssSUFBRyxRQUFIO1FBQ0gsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsY0FBdkI7UUFDQSxJQUFDLENBQUEsaUJBQUQsR0FBcUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLElBQUMsQ0FBQSxPQUFuQixFQUE0QjtVQUFBLEtBQUEsRUFBTyw2QkFBUDtTQUE1QixFQUZsQjs7SUFOTTs7NkJBVWIsZ0JBQUEsR0FBa0IsU0FBQTtBQUNoQixVQUFBO3lEQUFrQixDQUFFLE9BQXBCLENBQUE7SUFEZ0I7Ozs7O0FBWnBCIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPVxuY2xhc3MgTGF1bmNoTW9kZVZpZXdcbiAgY29uc3RydWN0b3I6ICh7c2FmZU1vZGUsIGRldk1vZGV9PXt9KSAtPlxuICAgIEBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3RhdHVzLWJhci1sYXVuY2gtbW9kZScpXG4gICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaW5saW5lLWJsb2NrJywgJ2ljb24nLCAnaWNvbi1jb2xvci1tb2RlJylcbiAgICBpZiBkZXZNb2RlXG4gICAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCd0ZXh0LWVycm9yJylcbiAgICAgIEB0b29sdGlwRGlzcG9zYWJsZSA9IGF0b20udG9vbHRpcHMuYWRkKEBlbGVtZW50LCB0aXRsZTogJ1RoaXMgd2luZG93IGlzIGluIGRldiBtb2RlJylcbiAgICBlbHNlIGlmIHNhZmVNb2RlXG4gICAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCd0ZXh0LXN1Y2Nlc3MnKVxuICAgICAgQHRvb2x0aXBEaXNwb3NhYmxlID0gYXRvbS50b29sdGlwcy5hZGQoQGVsZW1lbnQsIHRpdGxlOiAnVGhpcyB3aW5kb3cgaXMgaW4gc2FmZSBtb2RlJylcblxuICBkZXRhY2hlZENhbGxiYWNrOiAtPlxuICAgIEB0b29sdGlwRGlzcG9zYWJsZT8uZGlzcG9zZSgpXG4iXX0=
