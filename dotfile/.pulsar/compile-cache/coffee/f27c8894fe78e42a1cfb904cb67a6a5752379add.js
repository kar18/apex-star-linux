(function() {
  var AtomIoClient, fs, glob, path, remote, request;

  fs = require('fs-plus');

  path = require('path');

  remote = require('electron').remote;

  glob = require('glob');

  request = require('request');

  module.exports = AtomIoClient = (function() {
    function AtomIoClient(packageManager, baseURL) {
      this.packageManager = packageManager;
      this.baseURL = baseURL;
      if (this.baseURL == null) {
        this.baseURL = 'https://api.pulsar-edit.dev/api/';
      }
      this.expiry = 1000 * 60 * 60 * 5;
      this.createAvatarCache();
      this.expireAvatarCache();
    }

    AtomIoClient.prototype.avatar = function(login, callback) {
      return this.cachedAvatar(login, (function(_this) {
        return function(err, cached) {
          var stale;
          if (cached) {
            stale = Date.now() - parseInt(cached.split('-').pop()) > _this.expiry;
          }
          if (cached && (!stale || !_this.online())) {
            return callback(null, cached);
          } else {
            return _this.fetchAndCacheAvatar(login, callback);
          }
        };
      })(this));
    };

    AtomIoClient.prototype["package"] = function(name, callback) {
      var data, packagePath;
      packagePath = "packages/" + name;
      data = this.fetchFromCache(packagePath);
      if (data) {
        return callback(null, data);
      } else {
        return this.request(packagePath, callback);
      }
    };

    AtomIoClient.prototype.featuredPackages = function(callback) {
      var data;
      data = this.fetchFromCache('packages/featured');
      if (data) {
        return callback(null, data);
      } else {
        return this.getFeatured(false, callback);
      }
    };

    AtomIoClient.prototype.featuredThemes = function(callback) {
      var data;
      data = this.fetchFromCache('themes/featured');
      if (data) {
        return callback(null, data);
      } else {
        return this.getFeatured(true, callback);
      }
    };

    AtomIoClient.prototype.getFeatured = function(loadThemes, callback) {
      return this.packageManager.getFeatured(loadThemes).then((function(_this) {
        return function(packages) {
          var cached, key;
          key = loadThemes ? 'themes/featured' : 'packages/featured';
          cached = _this.deepCache(key, packages);
          return callback(null, packages);
        };
      })(this))["catch"](function(error) {
        return callback(error, null);
      });
    };

    AtomIoClient.prototype.request = function(path, callback) {
      var options;
      options = {
        url: "" + this.baseURL + path,
        headers: {
          'User-Agent': navigator.userAgent
        },
        gzip: true
      };
      return request(options, (function(_this) {
        return function(err, res, body) {
          var cached, error;
          if (err) {
            return callback(err);
          }
          try {
            body = _this.parseJSON(body);
            delete body.versions;
            cached = _this.deepCache(path, body);
            return callback(err, cached.data);
          } catch (error1) {
            error = error1;
            return callback(error);
          }
        };
      })(this));
    };

    AtomIoClient.prototype.deepCache = function(path, body) {
      var cached;
      cached = {
        data: body,
        createdOn: Date.now()
      };
      localStorage.setItem(this.cacheKeyForPath(path), JSON.stringify(cached));
      if (body instanceof Array) {
        body.forEach((function(_this) {
          return function(child) {
            return _this.deepCache("packages/" + child.name, child);
          };
        })(this));
      }
      return cached;
    };

    AtomIoClient.prototype.cacheKeyForPath = function(path) {
      return "settings-view:" + path;
    };

    AtomIoClient.prototype.online = function() {
      return navigator.onLine;
    };

    AtomIoClient.prototype.fetchFromCache = function(packagePath) {
      var cached;
      cached = localStorage.getItem(this.cacheKeyForPath(packagePath));
      cached = cached ? this.parseJSON(cached) : void 0;
      if ((cached != null) && (!this.online() || Date.now() - cached.createdOn < this.expiry)) {
        return cached.data;
      } else {
        return null;
      }
    };

    AtomIoClient.prototype.createAvatarCache = function() {
      return fs.makeTree(this.getCachePath());
    };

    AtomIoClient.prototype.avatarPath = function(login) {
      return path.join(this.getCachePath(), login + "-" + (Date.now()));
    };

    AtomIoClient.prototype.cachedAvatar = function(login, callback) {
      return glob(this.avatarGlob(login), (function(_this) {
        return function(err, files) {
          var createdOn, filename, i, imagePath, len, ref;
          if (err) {
            return callback(err);
          }
          files.sort().reverse();
          for (i = 0, len = files.length; i < len; i++) {
            imagePath = files[i];
            filename = path.basename(imagePath);
            ref = filename.split('-'), createdOn = ref[ref.length - 1];
            if (Date.now() - parseInt(createdOn) < _this.expiry) {
              return callback(null, imagePath);
            }
          }
          return callback(null, null);
        };
      })(this));
    };

    AtomIoClient.prototype.avatarGlob = function(login) {
      return path.join(this.getCachePath(), login + "-*([0-9])");
    };

    AtomIoClient.prototype.fetchAndCacheAvatar = function(login, callback) {
      var imagePath, requestObject;
      if (!this.online()) {
        return callback(null, null);
      } else {
        imagePath = this.avatarPath(login);
        requestObject = {
          url: "https://avatars.githubusercontent.com/" + login,
          headers: {
            'User-Agent': navigator.userAgent
          }
        };
        return request.head(requestObject, function(error, response, body) {
          var writeStream;
          if ((error != null) || response.statusCode !== 200 || !response.headers['content-type'].startsWith('image/')) {
            return callback(error);
          } else {
            writeStream = fs.createWriteStream(imagePath);
            writeStream.on('finish', function() {
              return callback(null, imagePath);
            });
            writeStream.on('error', function(error) {
              writeStream.close();
              try {
                if (fs.existsSync(imagePath)) {
                  fs.unlinkSync(imagePath);
                }
              } catch (error1) {}
              return callback(error);
            });
            return request(requestObject).pipe(writeStream);
          }
        });
      }
    };

    AtomIoClient.prototype.expireAvatarCache = function() {
      var deleteAvatar;
      deleteAvatar = (function(_this) {
        return function(child) {
          var avatarPath;
          avatarPath = path.join(_this.getCachePath(), child);
          return fs.unlink(avatarPath, function(error) {
            if (error && error.code !== 'ENOENT') {
              return console.warn("Error deleting avatar (" + error.code + "): " + avatarPath);
            }
          });
        };
      })(this);
      return fs.readdir(this.getCachePath(), function(error, _files) {
        var children, filename, files, i, key, len, parts, results, stamp;
        if (_files == null) {
          _files = [];
        }
        files = {};
        for (i = 0, len = _files.length; i < len; i++) {
          filename = _files[i];
          parts = filename.split('-');
          stamp = parts.pop();
          key = parts.join('-');
          if (files[key] == null) {
            files[key] = [];
          }
          files[key].push(key + "-" + stamp);
        }
        results = [];
        for (key in files) {
          children = files[key];
          children.sort();
          children.pop();
          results.push(children.forEach(deleteAvatar));
        }
        return results;
      });
    };

    AtomIoClient.prototype.getCachePath = function() {
      return this.cachePath != null ? this.cachePath : this.cachePath = path.join(remote.app.getPath('userData'), 'Cache', 'settings-view');
    };

    AtomIoClient.prototype.search = function(query, options) {
      var qs;
      qs = {
        q: query
      };
      if (options.themes) {
        qs.filter = 'theme';
      } else if (options.packages) {
        qs.filter = 'package';
      }
      options = {
        url: this.baseURL + "packages/search",
        headers: {
          'User-Agent': navigator.userAgent
        },
        qs: qs,
        gzip: true
      };
      return new Promise((function(_this) {
        return function(resolve, reject) {
          return request(options, function(err, res, textBody) {
            var body, e, error;
            if (err) {
              error = new Error("Searching for \u201C" + query + "\u201D failed.");
              error.stderr = err.message;
              return reject(error);
            } else {
              try {
                body = _this.parseJSON(textBody);
                if (body.filter) {
                  resolve(body.filter(function(pkg) {
                    var ref;
                    return ((ref = pkg.releases) != null ? ref.latest : void 0) != null;
                  }).map(function(arg) {
                    var downloads, metadata, readme, repository, stargazers_count;
                    readme = arg.readme, metadata = arg.metadata, downloads = arg.downloads, stargazers_count = arg.stargazers_count, repository = arg.repository;
                    return Object.assign(metadata, {
                      readme: readme,
                      downloads: downloads,
                      stargazers_count: stargazers_count,
                      repository: repository.url
                    });
                  }));
                } else {

                }
                error = new Error("Searching for \u201C" + query + "\u201D failed.\n");
                error.stderr = "API returned: " + textBody;
                return reject(error);
              } catch (error1) {
                e = error1;
                error = new Error("Searching for \u201C" + query + "\u201D failed.");
                error.stderr = e.message + '\n' + textBody;
                return reject(error);
              }
            }
          });
        };
      })(this));
    };

    AtomIoClient.prototype.parseJSON = function(s) {
      return JSON.parse(s);
    };

    return AtomIoClient;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zZXR0aW5ncy12aWV3L2xpYi9hdG9tLWlvLWNsaWVudC5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLEVBQUEsR0FBSyxPQUFBLENBQVEsU0FBUjs7RUFDTCxJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBQ04sU0FBVSxPQUFBLENBQVEsVUFBUjs7RUFFWCxJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBQ1AsT0FBQSxHQUFVLE9BQUEsQ0FBUSxTQUFSOztFQUVWLE1BQU0sQ0FBQyxPQUFQLEdBQ007SUFDUyxzQkFBQyxjQUFELEVBQWtCLE9BQWxCO01BQUMsSUFBQyxDQUFBLGlCQUFEO01BQWlCLElBQUMsQ0FBQSxVQUFEOztRQUM3QixJQUFDLENBQUEsVUFBVzs7TUFFWixJQUFDLENBQUEsTUFBRCxHQUFVLElBQUEsR0FBTyxFQUFQLEdBQVksRUFBWixHQUFpQjtNQUMzQixJQUFDLENBQUEsaUJBQUQsQ0FBQTtNQUNBLElBQUMsQ0FBQSxpQkFBRCxDQUFBO0lBTFc7OzJCQVFiLE1BQUEsR0FBUSxTQUFDLEtBQUQsRUFBUSxRQUFSO2FBQ04sSUFBQyxDQUFBLFlBQUQsQ0FBYyxLQUFkLEVBQXFCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxHQUFELEVBQU0sTUFBTjtBQUNuQixjQUFBO1VBQUEsSUFBb0UsTUFBcEU7WUFBQSxLQUFBLEdBQVEsSUFBSSxDQUFDLEdBQUwsQ0FBQSxDQUFBLEdBQWEsUUFBQSxDQUFTLE1BQU0sQ0FBQyxLQUFQLENBQWEsR0FBYixDQUFpQixDQUFDLEdBQWxCLENBQUEsQ0FBVCxDQUFiLEdBQWlELEtBQUMsQ0FBQSxPQUExRDs7VUFDQSxJQUFHLE1BQUEsSUFBVyxDQUFDLENBQUksS0FBSixJQUFhLENBQUksS0FBQyxDQUFBLE1BQUQsQ0FBQSxDQUFsQixDQUFkO21CQUNFLFFBQUEsQ0FBUyxJQUFULEVBQWUsTUFBZixFQURGO1dBQUEsTUFBQTttQkFHRSxLQUFDLENBQUEsbUJBQUQsQ0FBcUIsS0FBckIsRUFBNEIsUUFBNUIsRUFIRjs7UUFGbUI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXJCO0lBRE07OzRCQVVSLFNBQUEsR0FBUyxTQUFDLElBQUQsRUFBTyxRQUFQO0FBQ1AsVUFBQTtNQUFBLFdBQUEsR0FBYyxXQUFBLEdBQVk7TUFDMUIsSUFBQSxHQUFPLElBQUMsQ0FBQSxjQUFELENBQWdCLFdBQWhCO01BQ1AsSUFBRyxJQUFIO2VBQ0UsUUFBQSxDQUFTLElBQVQsRUFBZSxJQUFmLEVBREY7T0FBQSxNQUFBO2VBR0UsSUFBQyxDQUFBLE9BQUQsQ0FBUyxXQUFULEVBQXNCLFFBQXRCLEVBSEY7O0lBSE87OzJCQVFULGdCQUFBLEdBQWtCLFNBQUMsUUFBRDtBQUVoQixVQUFBO01BQUEsSUFBQSxHQUFPLElBQUMsQ0FBQSxjQUFELENBQWdCLG1CQUFoQjtNQUNQLElBQUcsSUFBSDtlQUNFLFFBQUEsQ0FBUyxJQUFULEVBQWUsSUFBZixFQURGO09BQUEsTUFBQTtlQUdFLElBQUMsQ0FBQSxXQUFELENBQWEsS0FBYixFQUFvQixRQUFwQixFQUhGOztJQUhnQjs7MkJBUWxCLGNBQUEsR0FBZ0IsU0FBQyxRQUFEO0FBRWQsVUFBQTtNQUFBLElBQUEsR0FBTyxJQUFDLENBQUEsY0FBRCxDQUFnQixpQkFBaEI7TUFDUCxJQUFHLElBQUg7ZUFDRSxRQUFBLENBQVMsSUFBVCxFQUFlLElBQWYsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsV0FBRCxDQUFhLElBQWIsRUFBbUIsUUFBbkIsRUFIRjs7SUFIYzs7MkJBUWhCLFdBQUEsR0FBYSxTQUFDLFVBQUQsRUFBYSxRQUFiO2FBR1gsSUFBQyxDQUFBLGNBQWMsQ0FBQyxXQUFoQixDQUE0QixVQUE1QixDQUNFLENBQUMsSUFESCxDQUNRLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxRQUFEO0FBRUosY0FBQTtVQUFBLEdBQUEsR0FBUyxVQUFILEdBQW1CLGlCQUFuQixHQUEwQztVQUNoRCxNQUFBLEdBQVMsS0FBQyxDQUFBLFNBQUQsQ0FBVyxHQUFYLEVBQWdCLFFBQWhCO2lCQUtULFFBQUEsQ0FBUyxJQUFULEVBQWUsUUFBZjtRQVJJO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQURSLENBVUUsRUFBQyxLQUFELEVBVkYsQ0FVUyxTQUFDLEtBQUQ7ZUFDTCxRQUFBLENBQVMsS0FBVCxFQUFnQixJQUFoQjtNQURLLENBVlQ7SUFIVzs7MkJBZ0JiLE9BQUEsR0FBUyxTQUFDLElBQUQsRUFBTyxRQUFQO0FBQ1AsVUFBQTtNQUFBLE9BQUEsR0FBVTtRQUNSLEdBQUEsRUFBSyxFQUFBLEdBQUcsSUFBQyxDQUFBLE9BQUosR0FBYyxJQURYO1FBRVIsT0FBQSxFQUFTO1VBQUMsWUFBQSxFQUFjLFNBQVMsQ0FBQyxTQUF6QjtTQUZEO1FBR1IsSUFBQSxFQUFNLElBSEU7O2FBTVYsT0FBQSxDQUFRLE9BQVIsRUFBaUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEdBQUQsRUFBTSxHQUFOLEVBQVcsSUFBWDtBQUNmLGNBQUE7VUFBQSxJQUF3QixHQUF4QjtBQUFBLG1CQUFPLFFBQUEsQ0FBUyxHQUFULEVBQVA7O0FBRUE7WUFHRSxJQUFBLEdBQU8sS0FBQyxDQUFBLFNBQUQsQ0FBVyxJQUFYO1lBQ1AsT0FBTyxJQUFJLENBQUM7WUFFWixNQUFBLEdBQVMsS0FBQyxDQUFBLFNBQUQsQ0FBVyxJQUFYLEVBQWlCLElBQWpCO21CQUtULFFBQUEsQ0FBUyxHQUFULEVBQWMsTUFBTSxDQUFDLElBQXJCLEVBWEY7V0FBQSxjQUFBO1lBWU07bUJBQ0osUUFBQSxDQUFTLEtBQVQsRUFiRjs7UUFIZTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBakI7SUFQTzs7MkJBeUJULFNBQUEsR0FBVyxTQUFDLElBQUQsRUFBTyxJQUFQO0FBQ1QsVUFBQTtNQUFBLE1BQUEsR0FDRTtRQUFBLElBQUEsRUFBTSxJQUFOO1FBQ0EsU0FBQSxFQUFXLElBQUksQ0FBQyxHQUFMLENBQUEsQ0FEWDs7TUFFRixZQUFZLENBQUMsT0FBYixDQUFxQixJQUFDLENBQUEsZUFBRCxDQUFpQixJQUFqQixDQUFyQixFQUE2QyxJQUFJLENBQUMsU0FBTCxDQUFlLE1BQWYsQ0FBN0M7TUFDQSxJQUFHLElBQUEsWUFBZ0IsS0FBbkI7UUFDRSxJQUFJLENBQUMsT0FBTCxDQUFhLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUMsS0FBRDttQkFBVyxLQUFDLENBQUEsU0FBRCxDQUFXLFdBQUEsR0FBWSxLQUFLLENBQUMsSUFBN0IsRUFBcUMsS0FBckM7VUFBWDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBYixFQURGOzthQUVBO0lBUFM7OzJCQVNYLGVBQUEsR0FBaUIsU0FBQyxJQUFEO2FBQ2YsZ0JBQUEsR0FBaUI7SUFERjs7MkJBR2pCLE1BQUEsR0FBUSxTQUFBO2FBQ04sU0FBUyxDQUFDO0lBREo7OzJCQUtSLGNBQUEsR0FBZ0IsU0FBQyxXQUFEO0FBQ2QsVUFBQTtNQUFBLE1BQUEsR0FBUyxZQUFZLENBQUMsT0FBYixDQUFxQixJQUFDLENBQUEsZUFBRCxDQUFpQixXQUFqQixDQUFyQjtNQUNULE1BQUEsR0FBWSxNQUFILEdBQWUsSUFBQyxDQUFBLFNBQUQsQ0FBVyxNQUFYLENBQWYsR0FBQTtNQUNULElBQUcsZ0JBQUEsSUFBWSxDQUFDLENBQUksSUFBQyxDQUFBLE1BQUQsQ0FBQSxDQUFKLElBQWlCLElBQUksQ0FBQyxHQUFMLENBQUEsQ0FBQSxHQUFhLE1BQU0sQ0FBQyxTQUFwQixHQUFnQyxJQUFDLENBQUEsTUFBbkQsQ0FBZjtBQUNFLGVBQU8sTUFBTSxDQUFDLEtBRGhCO09BQUEsTUFBQTtBQUlFLGVBQU8sS0FKVDs7SUFIYzs7MkJBU2hCLGlCQUFBLEdBQW1CLFNBQUE7YUFDakIsRUFBRSxDQUFDLFFBQUgsQ0FBWSxJQUFDLENBQUEsWUFBRCxDQUFBLENBQVo7SUFEaUI7OzJCQUduQixVQUFBLEdBQVksU0FBQyxLQUFEO2FBQ1YsSUFBSSxDQUFDLElBQUwsQ0FBVSxJQUFDLENBQUEsWUFBRCxDQUFBLENBQVYsRUFBOEIsS0FBRCxHQUFPLEdBQVAsR0FBUyxDQUFDLElBQUksQ0FBQyxHQUFMLENBQUEsQ0FBRCxDQUF0QztJQURVOzsyQkFHWixZQUFBLEdBQWMsU0FBQyxLQUFELEVBQVEsUUFBUjthQUNaLElBQUEsQ0FBSyxJQUFDLENBQUEsVUFBRCxDQUFZLEtBQVosQ0FBTCxFQUF5QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsR0FBRCxFQUFNLEtBQU47QUFDdkIsY0FBQTtVQUFBLElBQXdCLEdBQXhCO0FBQUEsbUJBQU8sUUFBQSxDQUFTLEdBQVQsRUFBUDs7VUFDQSxLQUFLLENBQUMsSUFBTixDQUFBLENBQVksQ0FBQyxPQUFiLENBQUE7QUFDQSxlQUFBLHVDQUFBOztZQUNFLFFBQUEsR0FBVyxJQUFJLENBQUMsUUFBTCxDQUFjLFNBQWQ7WUFDWCxNQUFtQixRQUFRLENBQUMsS0FBVCxDQUFlLEdBQWYsQ0FBbkIsRUFBTTtZQUNOLElBQUcsSUFBSSxDQUFDLEdBQUwsQ0FBQSxDQUFBLEdBQWEsUUFBQSxDQUFTLFNBQVQsQ0FBYixHQUFtQyxLQUFDLENBQUEsTUFBdkM7QUFDRSxxQkFBTyxRQUFBLENBQVMsSUFBVCxFQUFlLFNBQWYsRUFEVDs7QUFIRjtpQkFLQSxRQUFBLENBQVMsSUFBVCxFQUFlLElBQWY7UUFSdUI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpCO0lBRFk7OzJCQVdkLFVBQUEsR0FBWSxTQUFDLEtBQUQ7YUFDVixJQUFJLENBQUMsSUFBTCxDQUFVLElBQUMsQ0FBQSxZQUFELENBQUEsQ0FBVixFQUE4QixLQUFELEdBQU8sV0FBcEM7SUFEVTs7MkJBR1osbUJBQUEsR0FBcUIsU0FBQyxLQUFELEVBQVEsUUFBUjtBQUNuQixVQUFBO01BQUEsSUFBRyxDQUFJLElBQUMsQ0FBQSxNQUFELENBQUEsQ0FBUDtlQUNFLFFBQUEsQ0FBUyxJQUFULEVBQWUsSUFBZixFQURGO09BQUEsTUFBQTtRQUdFLFNBQUEsR0FBWSxJQUFDLENBQUEsVUFBRCxDQUFZLEtBQVo7UUFDWixhQUFBLEdBQWdCO1VBQ2QsR0FBQSxFQUFLLHdDQUFBLEdBQXlDLEtBRGhDO1VBRWQsT0FBQSxFQUFTO1lBQUMsWUFBQSxFQUFjLFNBQVMsQ0FBQyxTQUF6QjtXQUZLOztlQUloQixPQUFPLENBQUMsSUFBUixDQUFhLGFBQWIsRUFBNEIsU0FBQyxLQUFELEVBQVEsUUFBUixFQUFrQixJQUFsQjtBQUMxQixjQUFBO1VBQUEsSUFBRyxlQUFBLElBQVUsUUFBUSxDQUFDLFVBQVQsS0FBeUIsR0FBbkMsSUFBMEMsQ0FBSSxRQUFRLENBQUMsT0FBUSxDQUFBLGNBQUEsQ0FBZSxDQUFDLFVBQWpDLENBQTRDLFFBQTVDLENBQWpEO21CQUNFLFFBQUEsQ0FBUyxLQUFULEVBREY7V0FBQSxNQUFBO1lBR0UsV0FBQSxHQUFjLEVBQUUsQ0FBQyxpQkFBSCxDQUFxQixTQUFyQjtZQUNkLFdBQVcsQ0FBQyxFQUFaLENBQWUsUUFBZixFQUF5QixTQUFBO3FCQUFHLFFBQUEsQ0FBUyxJQUFULEVBQWUsU0FBZjtZQUFILENBQXpCO1lBQ0EsV0FBVyxDQUFDLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFNBQUMsS0FBRDtjQUN0QixXQUFXLENBQUMsS0FBWixDQUFBO0FBQ0E7Z0JBQ0UsSUFBMkIsRUFBRSxDQUFDLFVBQUgsQ0FBYyxTQUFkLENBQTNCO2tCQUFBLEVBQUUsQ0FBQyxVQUFILENBQWMsU0FBZCxFQUFBO2lCQURGO2VBQUE7cUJBRUEsUUFBQSxDQUFTLEtBQVQ7WUFKc0IsQ0FBeEI7bUJBS0EsT0FBQSxDQUFRLGFBQVIsQ0FBc0IsQ0FBQyxJQUF2QixDQUE0QixXQUE1QixFQVZGOztRQUQwQixDQUE1QixFQVJGOztJQURtQjs7MkJBMEJyQixpQkFBQSxHQUFtQixTQUFBO0FBQ2pCLFVBQUE7TUFBQSxZQUFBLEdBQWUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEtBQUQ7QUFDYixjQUFBO1VBQUEsVUFBQSxHQUFhLElBQUksQ0FBQyxJQUFMLENBQVUsS0FBQyxDQUFBLFlBQUQsQ0FBQSxDQUFWLEVBQTJCLEtBQTNCO2lCQUNiLEVBQUUsQ0FBQyxNQUFILENBQVUsVUFBVixFQUFzQixTQUFDLEtBQUQ7WUFDcEIsSUFBRyxLQUFBLElBQVUsS0FBSyxDQUFDLElBQU4sS0FBZ0IsUUFBN0I7cUJBQ0UsT0FBTyxDQUFDLElBQVIsQ0FBYSx5QkFBQSxHQUEwQixLQUFLLENBQUMsSUFBaEMsR0FBcUMsS0FBckMsR0FBMEMsVUFBdkQsRUFERjs7VUFEb0IsQ0FBdEI7UUFGYTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7YUFNZixFQUFFLENBQUMsT0FBSCxDQUFXLElBQUMsQ0FBQSxZQUFELENBQUEsQ0FBWCxFQUE0QixTQUFDLEtBQUQsRUFBUSxNQUFSO0FBQzFCLFlBQUE7O1VBQUEsU0FBVTs7UUFDVixLQUFBLEdBQVE7QUFDUixhQUFBLHdDQUFBOztVQUNFLEtBQUEsR0FBUSxRQUFRLENBQUMsS0FBVCxDQUFlLEdBQWY7VUFDUixLQUFBLEdBQVEsS0FBSyxDQUFDLEdBQU4sQ0FBQTtVQUNSLEdBQUEsR0FBTSxLQUFLLENBQUMsSUFBTixDQUFXLEdBQVg7O1lBQ04sS0FBTSxDQUFBLEdBQUEsSUFBUTs7VUFDZCxLQUFNLENBQUEsR0FBQSxDQUFJLENBQUMsSUFBWCxDQUFtQixHQUFELEdBQUssR0FBTCxHQUFRLEtBQTFCO0FBTEY7QUFPQTthQUFBLFlBQUE7O1VBQ0UsUUFBUSxDQUFDLElBQVQsQ0FBQTtVQUNBLFFBQVEsQ0FBQyxHQUFULENBQUE7dUJBSUEsUUFBUSxDQUFDLE9BQVQsQ0FBaUIsWUFBakI7QUFORjs7TUFWMEIsQ0FBNUI7SUFQaUI7OzJCQXlCbkIsWUFBQSxHQUFjLFNBQUE7c0NBQ1osSUFBQyxDQUFBLFlBQUQsSUFBQyxDQUFBLFlBQWEsSUFBSSxDQUFDLElBQUwsQ0FBVSxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQVgsQ0FBbUIsVUFBbkIsQ0FBVixFQUEwQyxPQUExQyxFQUFtRCxlQUFuRDtJQURGOzsyQkFHZCxNQUFBLEdBQVEsU0FBQyxLQUFELEVBQVEsT0FBUjtBQUNOLFVBQUE7TUFBQSxFQUFBLEdBQUs7UUFBQyxDQUFBLEVBQUcsS0FBSjs7TUFFTCxJQUFHLE9BQU8sQ0FBQyxNQUFYO1FBQ0UsRUFBRSxDQUFDLE1BQUgsR0FBWSxRQURkO09BQUEsTUFFSyxJQUFHLE9BQU8sQ0FBQyxRQUFYO1FBQ0gsRUFBRSxDQUFDLE1BQUgsR0FBWSxVQURUOztNQUdMLE9BQUEsR0FBVTtRQUNSLEdBQUEsRUFBUSxJQUFDLENBQUEsT0FBRixHQUFVLGlCQURUO1FBRVIsT0FBQSxFQUFTO1VBQUMsWUFBQSxFQUFjLFNBQVMsQ0FBQyxTQUF6QjtTQUZEO1FBR1IsRUFBQSxFQUFJLEVBSEk7UUFJUixJQUFBLEVBQU0sSUFKRTs7YUFPVixJQUFJLE9BQUosQ0FBWSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsT0FBRCxFQUFVLE1BQVY7aUJBQ1YsT0FBQSxDQUFRLE9BQVIsRUFBaUIsU0FBQyxHQUFELEVBQU0sR0FBTixFQUFXLFFBQVg7QUFDZixnQkFBQTtZQUFBLElBQUcsR0FBSDtjQUNFLEtBQUEsR0FBUSxJQUFJLEtBQUosQ0FBVSxzQkFBQSxHQUF1QixLQUF2QixHQUE2QixnQkFBdkM7Y0FDUixLQUFLLENBQUMsTUFBTixHQUFlLEdBQUcsQ0FBQztxQkFDbkIsTUFBQSxDQUFPLEtBQVAsRUFIRjthQUFBLE1BQUE7QUFLRTtnQkFHRSxJQUFBLEdBQU8sS0FBQyxDQUFBLFNBQUQsQ0FBVyxRQUFYO2dCQUNQLElBQUcsSUFBSSxDQUFDLE1BQVI7a0JBQ0UsT0FBQSxDQUNFLElBQUksQ0FBQyxNQUFMLENBQVksU0FBQyxHQUFEO0FBQVMsd0JBQUE7MkJBQUE7a0JBQVQsQ0FBWixDQUNJLENBQUMsR0FETCxDQUNTLFNBQUMsR0FBRDtBQUNILHdCQUFBO29CQURLLHFCQUFRLHlCQUFVLDJCQUFXLHlDQUFrQjsyQkFDcEQsTUFBTSxDQUFDLE1BQVAsQ0FBYyxRQUFkLEVBQXdCO3NCQUFDLFFBQUEsTUFBRDtzQkFBUyxXQUFBLFNBQVQ7c0JBQW9CLGtCQUFBLGdCQUFwQjtzQkFBc0MsVUFBQSxFQUFZLFVBQVUsQ0FBQyxHQUE3RDtxQkFBeEI7a0JBREcsQ0FEVCxDQURGLEVBREY7aUJBQUEsTUFBQTtBQUFBOztnQkFPQSxLQUFBLEdBQVEsSUFBSSxLQUFKLENBQVUsc0JBQUEsR0FBdUIsS0FBdkIsR0FBNkIsa0JBQXZDO2dCQUNSLEtBQUssQ0FBQyxNQUFOLEdBQWUsZ0JBQUEsR0FBbUI7dUJBQ2xDLE1BQUEsQ0FBTyxLQUFQLEVBYkY7ZUFBQSxjQUFBO2dCQWVNO2dCQUNKLEtBQUEsR0FBUSxJQUFJLEtBQUosQ0FBVSxzQkFBQSxHQUF1QixLQUF2QixHQUE2QixnQkFBdkM7Z0JBQ1IsS0FBSyxDQUFDLE1BQU4sR0FBZSxDQUFDLENBQUMsT0FBRixHQUFZLElBQVosR0FBbUI7dUJBQ2xDLE1BQUEsQ0FBTyxLQUFQLEVBbEJGO2VBTEY7O1VBRGUsQ0FBakI7UUFEVTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWjtJQWZNOzsyQkEwQ1IsU0FBQSxHQUFXLFNBQUMsQ0FBRDthQUNULElBQUksQ0FBQyxLQUFMLENBQVcsQ0FBWDtJQURTOzs7OztBQTFPYiIsInNvdXJjZXNDb250ZW50IjpbImZzID0gcmVxdWlyZSAnZnMtcGx1cydcbnBhdGggPSByZXF1aXJlICdwYXRoJ1xue3JlbW90ZX0gPSByZXF1aXJlICdlbGVjdHJvbidcblxuZ2xvYiA9IHJlcXVpcmUgJ2dsb2InXG5yZXF1ZXN0ID0gcmVxdWlyZSAncmVxdWVzdCdcblxubW9kdWxlLmV4cG9ydHMgPVxuY2xhc3MgQXRvbUlvQ2xpZW50XG4gIGNvbnN0cnVjdG9yOiAoQHBhY2thZ2VNYW5hZ2VyLCBAYmFzZVVSTCkgLT5cbiAgICBAYmFzZVVSTCA/PSAnaHR0cHM6Ly9hcGkucHVsc2FyLWVkaXQuZGV2L2FwaS8nXG4gICAgIyA1IGhvdXIgZXhwaXJ5XG4gICAgQGV4cGlyeSA9IDEwMDAgKiA2MCAqIDYwICogNVxuICAgIEBjcmVhdGVBdmF0YXJDYWNoZSgpXG4gICAgQGV4cGlyZUF2YXRhckNhY2hlKClcblxuICAjIFB1YmxpYzogR2V0IGFuIGF2YXRhciBpbWFnZSBmcm9tIHRoZSBmaWxlc3lzdGVtLCBmZXRjaGluZyBpdCBmaXJzdCBpZiBuZWNlc3NhcnlcbiAgYXZhdGFyOiAobG9naW4sIGNhbGxiYWNrKSAtPlxuICAgIEBjYWNoZWRBdmF0YXIgbG9naW4sIChlcnIsIGNhY2hlZCkgPT5cbiAgICAgIHN0YWxlID0gRGF0ZS5ub3coKSAtIHBhcnNlSW50KGNhY2hlZC5zcGxpdCgnLScpLnBvcCgpKSA+IEBleHBpcnkgaWYgY2FjaGVkXG4gICAgICBpZiBjYWNoZWQgYW5kIChub3Qgc3RhbGUgb3Igbm90IEBvbmxpbmUoKSlcbiAgICAgICAgY2FsbGJhY2sgbnVsbCwgY2FjaGVkXG4gICAgICBlbHNlXG4gICAgICAgIEBmZXRjaEFuZENhY2hlQXZhdGFyKGxvZ2luLCBjYWxsYmFjaylcblxuICAjIFB1YmxpYzogZ2V0IGEgcGFja2FnZSBmcm9tIHRoZSBhdG9tLmlvIEFQSSwgd2l0aCB0aGUgYXBwcm9wcmlhdGUgbGV2ZWwgb2ZcbiAgIyBjYWNoaW5nLlxuICBwYWNrYWdlOiAobmFtZSwgY2FsbGJhY2spIC0+XG4gICAgcGFja2FnZVBhdGggPSBcInBhY2thZ2VzLyN7bmFtZX1cIlxuICAgIGRhdGEgPSBAZmV0Y2hGcm9tQ2FjaGUocGFja2FnZVBhdGgpXG4gICAgaWYgZGF0YVxuICAgICAgY2FsbGJhY2sobnVsbCwgZGF0YSlcbiAgICBlbHNlXG4gICAgICBAcmVxdWVzdChwYWNrYWdlUGF0aCwgY2FsbGJhY2spXG5cbiAgZmVhdHVyZWRQYWNrYWdlczogKGNhbGxiYWNrKSAtPlxuICAgICMgVE9ETyBjbGVhbiB1cCBjYWNoaW5nIGNvcHlwYXN0YVxuICAgIGRhdGEgPSBAZmV0Y2hGcm9tQ2FjaGUgJ3BhY2thZ2VzL2ZlYXR1cmVkJ1xuICAgIGlmIGRhdGFcbiAgICAgIGNhbGxiYWNrKG51bGwsIGRhdGEpXG4gICAgZWxzZVxuICAgICAgQGdldEZlYXR1cmVkKGZhbHNlLCBjYWxsYmFjaylcblxuICBmZWF0dXJlZFRoZW1lczogKGNhbGxiYWNrKSAtPlxuICAgICMgVE9ETyBjbGVhbiB1cCBjYWNoaW5nIGNvcHlwYXN0YVxuICAgIGRhdGEgPSBAZmV0Y2hGcm9tQ2FjaGUgJ3RoZW1lcy9mZWF0dXJlZCdcbiAgICBpZiBkYXRhXG4gICAgICBjYWxsYmFjayhudWxsLCBkYXRhKVxuICAgIGVsc2VcbiAgICAgIEBnZXRGZWF0dXJlZCh0cnVlLCBjYWxsYmFjaylcblxuICBnZXRGZWF0dXJlZDogKGxvYWRUaGVtZXMsIGNhbGxiYWNrKSAtPlxuICAgICMgYXBtIGFscmVhZHkgZG9lcyB0aGlzLCBtaWdodCBhcyB3ZWxsIHVzZSBpdCBpbnN0ZWFkIG9mIHJlcXVlc3QgaSBndWVzcz8gVGhlXG4gICAgIyBkb3duc2lkZSBpcyB0aGF0IEkgbmVlZCB0byByZXBlYXQgY2FjaGluZyBsb2dpYyBoZXJlLlxuICAgIEBwYWNrYWdlTWFuYWdlci5nZXRGZWF0dXJlZChsb2FkVGhlbWVzKVxuICAgICAgLnRoZW4gKHBhY2thZ2VzKSA9PlxuICAgICAgICAjIGNvcHlwYXN0YSBmcm9tIGJlbG93XG4gICAgICAgIGtleSA9IGlmIGxvYWRUaGVtZXMgdGhlbiAndGhlbWVzL2ZlYXR1cmVkJyBlbHNlICdwYWNrYWdlcy9mZWF0dXJlZCdcbiAgICAgICAgY2FjaGVkID0gQGRlZXBDYWNoZShrZXksIHBhY2thZ2VzKVxuICAgICAgICAgICMgZGF0YTogcGFja2FnZXNcbiAgICAgICAgICAjIGNyZWF0ZWRPbjogRGF0ZS5ub3coKVxuICAgICAgICAjIGxvY2FsU3RvcmFnZS5zZXRJdGVtKEBjYWNoZUtleUZvclBhdGgoa2V5KSwgSlNPTi5zdHJpbmdpZnkoY2FjaGVkKSlcbiAgICAgICAgIyBlbmQgY29weXBhc3RhXG4gICAgICAgIGNhbGxiYWNrKG51bGwsIHBhY2thZ2VzKVxuICAgICAgLmNhdGNoIChlcnJvcikgLT5cbiAgICAgICAgY2FsbGJhY2soZXJyb3IsIG51bGwpXG5cbiAgcmVxdWVzdDogKHBhdGgsIGNhbGxiYWNrKSAtPlxuICAgIG9wdGlvbnMgPSB7XG4gICAgICB1cmw6IFwiI3tAYmFzZVVSTH0je3BhdGh9XCJcbiAgICAgIGhlYWRlcnM6IHsnVXNlci1BZ2VudCc6IG5hdmlnYXRvci51c2VyQWdlbnR9XG4gICAgICBnemlwOiB0cnVlXG4gICAgfVxuXG4gICAgcmVxdWVzdCBvcHRpb25zLCAoZXJyLCByZXMsIGJvZHkpID0+XG4gICAgICByZXR1cm4gY2FsbGJhY2soZXJyKSBpZiBlcnJcblxuICAgICAgdHJ5XG4gICAgICAgICMgTk9URTogcmVxdWVzdCdzIGpzb24gb3B0aW9uIGRvZXMgbm90IHBvcHVsYXRlIGVyciBpZiBwYXJzaW5nIGZhaWxzLFxuICAgICAgICAjIHNvIHdlIGRvIGl0IG1hbnVhbGx5XG4gICAgICAgIGJvZHkgPSBAcGFyc2VKU09OKGJvZHkpXG4gICAgICAgIGRlbGV0ZSBib2R5LnZlcnNpb25zXG5cbiAgICAgICAgY2FjaGVkID0gQGRlZXBDYWNoZShwYXRoLCBib2R5KVxuICAgICAgICAjIGNhY2hlZCA9XG4gICAgICAgICMgICBkYXRhOiBib2R5XG4gICAgICAgICMgICBjcmVhdGVkT246IERhdGUubm93KClcbiAgICAgICAgIyBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShAY2FjaGVLZXlGb3JQYXRoKHBhdGgpLCBKU09OLnN0cmluZ2lmeShjYWNoZWQpKVxuICAgICAgICBjYWxsYmFjayhlcnIsIGNhY2hlZC5kYXRhKVxuICAgICAgY2F0Y2ggZXJyb3JcbiAgICAgICAgY2FsbGJhY2soZXJyb3IpXG5cbiAgZGVlcENhY2hlOiAocGF0aCwgYm9keSkgLT5cbiAgICBjYWNoZWQgPVxuICAgICAgZGF0YTogYm9keVxuICAgICAgY3JlYXRlZE9uOiBEYXRlLm5vdygpXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oQGNhY2hlS2V5Rm9yUGF0aChwYXRoKSwgSlNPTi5zdHJpbmdpZnkoY2FjaGVkKSlcbiAgICBpZihib2R5IGluc3RhbmNlb2YgQXJyYXkpXG4gICAgICBib2R5LmZvckVhY2ggKGNoaWxkKSA9PiBAZGVlcENhY2hlKFwicGFja2FnZXMvI3tjaGlsZC5uYW1lfVwiLCBjaGlsZClcbiAgICBjYWNoZWRcblxuICBjYWNoZUtleUZvclBhdGg6IChwYXRoKSAtPlxuICAgIFwic2V0dGluZ3Mtdmlldzoje3BhdGh9XCJcblxuICBvbmxpbmU6IC0+XG4gICAgbmF2aWdhdG9yLm9uTGluZVxuXG4gICMgVGhpcyBjb3VsZCB1c2UgYSBiZXR0ZXIgbmFtZSwgc2luY2UgaXQgY2hlY2tzIHdoZXRoZXIgaXQncyBhcHByb3ByaWF0ZSB0byByZXR1cm5cbiAgIyB0aGUgY2FjaGVkIGRhdGEgYW5kIHByZXRlbmRzIGl0J3MgbnVsbCBpZiBpdCdzIHN0YWxlIGFuZCB3ZSdyZSBvbmxpbmVcbiAgZmV0Y2hGcm9tQ2FjaGU6IChwYWNrYWdlUGF0aCkgLT5cbiAgICBjYWNoZWQgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShAY2FjaGVLZXlGb3JQYXRoKHBhY2thZ2VQYXRoKSlcbiAgICBjYWNoZWQgPSBpZiBjYWNoZWQgdGhlbiBAcGFyc2VKU09OKGNhY2hlZClcbiAgICBpZiBjYWNoZWQ/IGFuZCAobm90IEBvbmxpbmUoKSBvciBEYXRlLm5vdygpIC0gY2FjaGVkLmNyZWF0ZWRPbiA8IEBleHBpcnkpXG4gICAgICByZXR1cm4gY2FjaGVkLmRhdGFcbiAgICBlbHNlXG4gICAgICAjIGZhbHN5IGRhdGEgbWVhbnMgXCJ0cnkgdG8gaGl0IHRoZSBuZXR3b3JrXCJcbiAgICAgIHJldHVybiBudWxsXG5cbiAgY3JlYXRlQXZhdGFyQ2FjaGU6IC0+XG4gICAgZnMubWFrZVRyZWUoQGdldENhY2hlUGF0aCgpKVxuXG4gIGF2YXRhclBhdGg6IChsb2dpbikgLT5cbiAgICBwYXRoLmpvaW4gQGdldENhY2hlUGF0aCgpLCBcIiN7bG9naW59LSN7RGF0ZS5ub3coKX1cIlxuXG4gIGNhY2hlZEF2YXRhcjogKGxvZ2luLCBjYWxsYmFjaykgLT5cbiAgICBnbG9iIEBhdmF0YXJHbG9iKGxvZ2luKSwgKGVyciwgZmlsZXMpID0+XG4gICAgICByZXR1cm4gY2FsbGJhY2soZXJyKSBpZiBlcnJcbiAgICAgIGZpbGVzLnNvcnQoKS5yZXZlcnNlKClcbiAgICAgIGZvciBpbWFnZVBhdGggaW4gZmlsZXNcbiAgICAgICAgZmlsZW5hbWUgPSBwYXRoLmJhc2VuYW1lKGltYWdlUGF0aClcbiAgICAgICAgWy4uLiwgY3JlYXRlZE9uXSA9IGZpbGVuYW1lLnNwbGl0KCctJylcbiAgICAgICAgaWYgRGF0ZS5ub3coKSAtIHBhcnNlSW50KGNyZWF0ZWRPbikgPCBAZXhwaXJ5XG4gICAgICAgICAgcmV0dXJuIGNhbGxiYWNrKG51bGwsIGltYWdlUGF0aClcbiAgICAgIGNhbGxiYWNrKG51bGwsIG51bGwpXG5cbiAgYXZhdGFyR2xvYjogKGxvZ2luKSAtPlxuICAgIHBhdGguam9pbiBAZ2V0Q2FjaGVQYXRoKCksIFwiI3tsb2dpbn0tKihbMC05XSlcIlxuXG4gIGZldGNoQW5kQ2FjaGVBdmF0YXI6IChsb2dpbiwgY2FsbGJhY2spIC0+XG4gICAgaWYgbm90IEBvbmxpbmUoKVxuICAgICAgY2FsbGJhY2sobnVsbCwgbnVsbClcbiAgICBlbHNlXG4gICAgICBpbWFnZVBhdGggPSBAYXZhdGFyUGF0aCBsb2dpblxuICAgICAgcmVxdWVzdE9iamVjdCA9IHtcbiAgICAgICAgdXJsOiBcImh0dHBzOi8vYXZhdGFycy5naXRodWJ1c2VyY29udGVudC5jb20vI3tsb2dpbn1cIlxuICAgICAgICBoZWFkZXJzOiB7J1VzZXItQWdlbnQnOiBuYXZpZ2F0b3IudXNlckFnZW50fVxuICAgICAgfVxuICAgICAgcmVxdWVzdC5oZWFkIHJlcXVlc3RPYmplY3QsIChlcnJvciwgcmVzcG9uc2UsIGJvZHkpIC0+XG4gICAgICAgIGlmIGVycm9yPyBvciByZXNwb25zZS5zdGF0dXNDb2RlIGlzbnQgMjAwIG9yIG5vdCByZXNwb25zZS5oZWFkZXJzWydjb250ZW50LXR5cGUnXS5zdGFydHNXaXRoKCdpbWFnZS8nKVxuICAgICAgICAgIGNhbGxiYWNrKGVycm9yKVxuICAgICAgICBlbHNlXG4gICAgICAgICAgd3JpdGVTdHJlYW0gPSBmcy5jcmVhdGVXcml0ZVN0cmVhbSBpbWFnZVBhdGhcbiAgICAgICAgICB3cml0ZVN0cmVhbS5vbiAnZmluaXNoJywgLT4gY2FsbGJhY2sobnVsbCwgaW1hZ2VQYXRoKVxuICAgICAgICAgIHdyaXRlU3RyZWFtLm9uICdlcnJvcicsIChlcnJvcikgLT5cbiAgICAgICAgICAgIHdyaXRlU3RyZWFtLmNsb3NlKClcbiAgICAgICAgICAgIHRyeVxuICAgICAgICAgICAgICBmcy51bmxpbmtTeW5jIGltYWdlUGF0aCBpZiBmcy5leGlzdHNTeW5jIGltYWdlUGF0aFxuICAgICAgICAgICAgY2FsbGJhY2soZXJyb3IpXG4gICAgICAgICAgcmVxdWVzdChyZXF1ZXN0T2JqZWN0KS5waXBlKHdyaXRlU3RyZWFtKVxuXG4gICMgVGhlIGNhY2hlIGV4cGlyeSBkb2Vzbid0IG5lZWQgdG8gYmUgY2xldmVyLCBvciBldmVuIGNvbXBhcmUgZGF0ZXMsIGl0IGp1c3RcbiAgIyBuZWVkcyB0byBhbHdheXMga2VlcCBhcm91bmQgdGhlIG5ld2VzdCBpdGVtLCBhbmQgdGhhdCBpdGVtIG9ubHkuIFRoZSBsb2NhbFN0b3JhZ2VcbiAgIyBjYWNoZSB1cGRhdGVzIGluIHBsYWNlLCBzbyBpdCBkb2Vzbid0IG5lZWQgdG8gYmUgcHVyZ2VkLlxuXG4gIGV4cGlyZUF2YXRhckNhY2hlOiAtPlxuICAgIGRlbGV0ZUF2YXRhciA9IChjaGlsZCkgPT5cbiAgICAgIGF2YXRhclBhdGggPSBwYXRoLmpvaW4oQGdldENhY2hlUGF0aCgpLCBjaGlsZClcbiAgICAgIGZzLnVubGluayBhdmF0YXJQYXRoLCAoZXJyb3IpIC0+XG4gICAgICAgIGlmIGVycm9yIGFuZCBlcnJvci5jb2RlIGlzbnQgJ0VOT0VOVCcgIyBJZ25vcmUgY2FjaGUgcGF0aHMgdGhhdCBkb24ndCBleGlzdFxuICAgICAgICAgIGNvbnNvbGUud2FybihcIkVycm9yIGRlbGV0aW5nIGF2YXRhciAoI3tlcnJvci5jb2RlfSk6ICN7YXZhdGFyUGF0aH1cIilcblxuICAgIGZzLnJlYWRkaXIgQGdldENhY2hlUGF0aCgpLCAoZXJyb3IsIF9maWxlcykgLT5cbiAgICAgIF9maWxlcyA/PSBbXVxuICAgICAgZmlsZXMgPSB7fVxuICAgICAgZm9yIGZpbGVuYW1lIGluIF9maWxlc1xuICAgICAgICBwYXJ0cyA9IGZpbGVuYW1lLnNwbGl0KCctJylcbiAgICAgICAgc3RhbXAgPSBwYXJ0cy5wb3AoKVxuICAgICAgICBrZXkgPSBwYXJ0cy5qb2luKCctJylcbiAgICAgICAgZmlsZXNba2V5XSA/PSBbXVxuICAgICAgICBmaWxlc1trZXldLnB1c2ggXCIje2tleX0tI3tzdGFtcH1cIlxuXG4gICAgICBmb3Iga2V5LCBjaGlsZHJlbiBvZiBmaWxlc1xuICAgICAgICBjaGlsZHJlbi5zb3J0KClcbiAgICAgICAgY2hpbGRyZW4ucG9wKCkgIyBrZWVwXG4gICAgICAgICMgUmlnaHQgbm93IGEgYnVuY2ggb2YgY2xpZW50cyBtaWdodCBiZSBpbnN0YW50aWF0ZWQgYXQgb25jZSwgc29cbiAgICAgICAgIyB3ZSBjYW4ganVzdCBpZ25vcmUgYXR0ZW1wdHMgdG8gdW5saW5rIGZpbGVzIHRoYXQgaGF2ZSBhbHJlYWR5IGJlZW4gcmVtb3ZlZFxuICAgICAgICAjIC0gdGhpcyBzaG91bGQgYmUgZml4ZWQgd2l0aCBhIHNpbmdsZXRvbiBjbGllbnRcbiAgICAgICAgY2hpbGRyZW4uZm9yRWFjaChkZWxldGVBdmF0YXIpXG5cbiAgZ2V0Q2FjaGVQYXRoOiAtPlxuICAgIEBjYWNoZVBhdGggPz0gcGF0aC5qb2luKHJlbW90ZS5hcHAuZ2V0UGF0aCgndXNlckRhdGEnKSwgJ0NhY2hlJywgJ3NldHRpbmdzLXZpZXcnKVxuXG4gIHNlYXJjaDogKHF1ZXJ5LCBvcHRpb25zKSAtPlxuICAgIHFzID0ge3E6IHF1ZXJ5fVxuXG4gICAgaWYgb3B0aW9ucy50aGVtZXNcbiAgICAgIHFzLmZpbHRlciA9ICd0aGVtZSdcbiAgICBlbHNlIGlmIG9wdGlvbnMucGFja2FnZXNcbiAgICAgIHFzLmZpbHRlciA9ICdwYWNrYWdlJ1xuXG4gICAgb3B0aW9ucyA9IHtcbiAgICAgIHVybDogXCIje0BiYXNlVVJMfXBhY2thZ2VzL3NlYXJjaFwiXG4gICAgICBoZWFkZXJzOiB7J1VzZXItQWdlbnQnOiBuYXZpZ2F0b3IudXNlckFnZW50fVxuICAgICAgcXM6IHFzXG4gICAgICBnemlwOiB0cnVlXG4gICAgfVxuXG4gICAgbmV3IFByb21pc2UgKHJlc29sdmUsIHJlamVjdCkgPT5cbiAgICAgIHJlcXVlc3Qgb3B0aW9ucywgKGVyciwgcmVzLCB0ZXh0Qm9keSkgPT5cbiAgICAgICAgaWYgZXJyXG4gICAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoXCJTZWFyY2hpbmcgZm9yIFxcdTIwMUMje3F1ZXJ5fVxcdTIwMUQgZmFpbGVkLlwiKVxuICAgICAgICAgIGVycm9yLnN0ZGVyciA9IGVyci5tZXNzYWdlXG4gICAgICAgICAgcmVqZWN0KGVycm9yKVxuICAgICAgICBlbHNlXG4gICAgICAgICAgdHJ5XG4gICAgICAgICAgICAjIE5PVEU6IHJlcXVlc3QncyBqc29uIG9wdGlvbiBkb2VzIG5vdCBwb3B1bGF0ZSBlcnIgaWYgcGFyc2luZyBmYWlscyxcbiAgICAgICAgICAgICMgc28gd2UgZG8gaXQgbWFudWFsbHlcbiAgICAgICAgICAgIGJvZHkgPSBAcGFyc2VKU09OKHRleHRCb2R5KVxuICAgICAgICAgICAgaWYgYm9keS5maWx0ZXJcbiAgICAgICAgICAgICAgcmVzb2x2ZShcbiAgICAgICAgICAgICAgICBib2R5LmZpbHRlciAocGtnKSAtPiBwa2cucmVsZWFzZXM/LmxhdGVzdD9cbiAgICAgICAgICAgICAgICAgICAgLm1hcCAoe3JlYWRtZSwgbWV0YWRhdGEsIGRvd25sb2Fkcywgc3RhcmdhemVyc19jb3VudCwgcmVwb3NpdG9yeX0pIC0+XG4gICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmFzc2lnbiBtZXRhZGF0YSwge3JlYWRtZSwgZG93bmxvYWRzLCBzdGFyZ2F6ZXJzX2NvdW50LCByZXBvc2l0b3J5OiByZXBvc2l0b3J5LnVybH1cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoXCJTZWFyY2hpbmcgZm9yIFxcdTIwMUMje3F1ZXJ5fVxcdTIwMUQgZmFpbGVkLlxcblwiKVxuICAgICAgICAgICAgZXJyb3Iuc3RkZXJyID0gXCJBUEkgcmV0dXJuZWQ6IFwiICsgdGV4dEJvZHlcbiAgICAgICAgICAgIHJlamVjdCBlcnJvclxuXG4gICAgICAgICAgY2F0Y2ggZVxuICAgICAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoXCJTZWFyY2hpbmcgZm9yIFxcdTIwMUMje3F1ZXJ5fVxcdTIwMUQgZmFpbGVkLlwiKVxuICAgICAgICAgICAgZXJyb3Iuc3RkZXJyID0gZS5tZXNzYWdlICsgJ1xcbicgKyB0ZXh0Qm9keVxuICAgICAgICAgICAgcmVqZWN0IGVycm9yXG5cbiAgcGFyc2VKU09OOiAocykgLT5cbiAgICBKU09OLnBhcnNlKHMpXG4iXX0=
