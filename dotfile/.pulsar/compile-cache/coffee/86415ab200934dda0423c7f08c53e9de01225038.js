(function() {
  var _, capitalize, escapeHtml, escapeNode, escapeRegex, getReplacementResultsMessage, getSearchResultsMessage, preserveCase, sanitizePattern, showIf, titleize;

  _ = require('underscore-plus');

  escapeNode = null;

  escapeHtml = function(str) {
    if (escapeNode == null) {
      escapeNode = document.createElement('div');
    }
    escapeNode.innerText = str;
    return escapeNode.innerHTML;
  };

  escapeRegex = function(str) {
    return str.replace(/[.?*+^$[\]\\(){}|-]/g, function(match) {
      return "\\" + match;
    });
  };

  sanitizePattern = function(pattern) {
    pattern = escapeHtml(pattern);
    return pattern.replace(/\n/g, '\\n').replace(/\t/g, '\\t');
  };

  getReplacementResultsMessage = function(arg) {
    var findPattern, replacePattern, replacedPathCount, replacementCount;
    findPattern = arg.findPattern, replacePattern = arg.replacePattern, replacedPathCount = arg.replacedPathCount, replacementCount = arg.replacementCount;
    if (replacedPathCount) {
      return "<span class=\"text-highlight\">Replaced <span class=\"highlight-error\">" + (sanitizePattern(findPattern)) + "</span> with <span class=\"highlight-success\">" + (sanitizePattern(replacePattern)) + "</span> " + (_.pluralize(replacementCount, 'time')) + " in " + (_.pluralize(replacedPathCount, 'file')) + "</span>";
    } else {
      return "<span class=\"text-highlight\">Nothing replaced</span>";
    }
  };

  getSearchResultsMessage = function(results) {
    var findPattern, matchCount, pathCount, replacedPathCount;
    if ((results != null ? results.findPattern : void 0) != null) {
      findPattern = results.findPattern, matchCount = results.matchCount, pathCount = results.pathCount, replacedPathCount = results.replacedPathCount;
      if (matchCount) {
        return (_.pluralize(matchCount, 'result')) + " found in " + (_.pluralize(pathCount, 'file')) + " for <span class=\"highlight-info\">" + (sanitizePattern(findPattern)) + "</span>";
      } else {
        return "No " + (replacedPathCount != null ? 'more' : '') + " results found for '" + (sanitizePattern(findPattern)) + "'";
      }
    } else {
      return '';
    }
  };

  showIf = function(condition) {
    if (condition) {
      return null;
    } else {
      return {
        display: 'none'
      };
    }
  };

  capitalize = function(str) {
    return str[0].toUpperCase() + str.toLowerCase().slice(1);
  };

  titleize = function(str) {
    return str.toLowerCase().replace(/(?:^|\s)\S/g, function(capital) {
      return capital.toUpperCase();
    });
  };

  preserveCase = function(text, reference) {
    if (reference === capitalize(reference.toLowerCase())) {
      return capitalize(text);
    } else if (reference === titleize(reference.toLowerCase())) {
      return titleize(text);
    } else if (reference === reference.toUpperCase()) {
      return text.toUpperCase();
    } else if (reference === reference.toLowerCase()) {
      return text.toLowerCase();
    } else {
      return text;
    }
  };

  module.exports = {
    escapeHtml: escapeHtml,
    escapeRegex: escapeRegex,
    sanitizePattern: sanitizePattern,
    getReplacementResultsMessage: getReplacementResultsMessage,
    getSearchResultsMessage: getSearchResultsMessage,
    showIf: showIf,
    preserveCase: preserveCase
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9maW5kLWFuZC1yZXBsYWNlL2xpYi9wcm9qZWN0L3V0aWwuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQSxDQUFBLEdBQUksT0FBQSxDQUFRLGlCQUFSOztFQUVKLFVBQUEsR0FBYTs7RUFFYixVQUFBLEdBQWEsU0FBQyxHQUFEOztNQUNYLGFBQWMsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7O0lBQ2QsVUFBVSxDQUFDLFNBQVgsR0FBdUI7V0FDdkIsVUFBVSxDQUFDO0VBSEE7O0VBS2IsV0FBQSxHQUFjLFNBQUMsR0FBRDtXQUNaLEdBQUcsQ0FBQyxPQUFKLENBQVksc0JBQVosRUFBb0MsU0FBQyxLQUFEO2FBQVcsSUFBQSxHQUFPO0lBQWxCLENBQXBDO0VBRFk7O0VBR2QsZUFBQSxHQUFrQixTQUFDLE9BQUQ7SUFDaEIsT0FBQSxHQUFVLFVBQUEsQ0FBVyxPQUFYO1dBQ1YsT0FBTyxDQUFDLE9BQVIsQ0FBZ0IsS0FBaEIsRUFBdUIsS0FBdkIsQ0FBNkIsQ0FBQyxPQUE5QixDQUFzQyxLQUF0QyxFQUE2QyxLQUE3QztFQUZnQjs7RUFJbEIsNEJBQUEsR0FBK0IsU0FBQyxHQUFEO0FBQzdCLFFBQUE7SUFEK0IsK0JBQWEscUNBQWdCLDJDQUFtQjtJQUMvRSxJQUFHLGlCQUFIO2FBQ0UsMEVBQUEsR0FBMEUsQ0FBQyxlQUFBLENBQWdCLFdBQWhCLENBQUQsQ0FBMUUsR0FBd0csaURBQXhHLEdBQXdKLENBQUMsZUFBQSxDQUFnQixjQUFoQixDQUFELENBQXhKLEdBQXlMLFVBQXpMLEdBQWtNLENBQUMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxnQkFBWixFQUE4QixNQUE5QixDQUFELENBQWxNLEdBQXlPLE1BQXpPLEdBQThPLENBQUMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxpQkFBWixFQUErQixNQUEvQixDQUFELENBQTlPLEdBQXNSLFVBRHhSO0tBQUEsTUFBQTthQUdFLHlEQUhGOztFQUQ2Qjs7RUFNL0IsdUJBQUEsR0FBMEIsU0FBQyxPQUFEO0FBQ3hCLFFBQUE7SUFBQSxJQUFHLHdEQUFIO01BQ0csaUNBQUQsRUFBYywrQkFBZCxFQUEwQiw2QkFBMUIsRUFBcUM7TUFDckMsSUFBRyxVQUFIO2VBQ0ksQ0FBQyxDQUFDLENBQUMsU0FBRixDQUFZLFVBQVosRUFBd0IsUUFBeEIsQ0FBRCxDQUFBLEdBQW1DLFlBQW5DLEdBQThDLENBQUMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxTQUFaLEVBQXVCLE1BQXZCLENBQUQsQ0FBOUMsR0FBOEUsc0NBQTlFLEdBQW1ILENBQUMsZUFBQSxDQUFnQixXQUFoQixDQUFELENBQW5ILEdBQWlKLFVBRHJKO09BQUEsTUFBQTtlQUdFLEtBQUEsR0FBSyxDQUFJLHlCQUFILEdBQTJCLE1BQTNCLEdBQXVDLEVBQXhDLENBQUwsR0FBZ0Qsc0JBQWhELEdBQXFFLENBQUMsZUFBQSxDQUFnQixXQUFoQixDQUFELENBQXJFLEdBQW1HLElBSHJHO09BRkY7S0FBQSxNQUFBO2FBT0UsR0FQRjs7RUFEd0I7O0VBVTFCLE1BQUEsR0FBUyxTQUFDLFNBQUQ7SUFDUCxJQUFHLFNBQUg7YUFDRSxLQURGO0tBQUEsTUFBQTthQUdFO1FBQUMsT0FBQSxFQUFTLE1BQVY7UUFIRjs7RUFETzs7RUFNVCxVQUFBLEdBQWEsU0FBQyxHQUFEO1dBQVMsR0FBSSxDQUFBLENBQUEsQ0FBRSxDQUFDLFdBQVAsQ0FBQSxDQUFBLEdBQXVCLEdBQUcsQ0FBQyxXQUFKLENBQUEsQ0FBaUIsQ0FBQyxLQUFsQixDQUF3QixDQUF4QjtFQUFoQzs7RUFDYixRQUFBLEdBQVcsU0FBQyxHQUFEO1dBQVMsR0FBRyxDQUFDLFdBQUosQ0FBQSxDQUFpQixDQUFDLE9BQWxCLENBQTBCLGFBQTFCLEVBQXlDLFNBQUMsT0FBRDthQUFhLE9BQU8sQ0FBQyxXQUFSLENBQUE7SUFBYixDQUF6QztFQUFUOztFQUVYLFlBQUEsR0FBZSxTQUFDLElBQUQsRUFBTyxTQUFQO0lBRWIsSUFBRyxTQUFBLEtBQWEsVUFBQSxDQUFXLFNBQVMsQ0FBQyxXQUFWLENBQUEsQ0FBWCxDQUFoQjthQUNFLFVBQUEsQ0FBVyxJQUFYLEVBREY7S0FBQSxNQUlLLElBQUcsU0FBQSxLQUFhLFFBQUEsQ0FBUyxTQUFTLENBQUMsV0FBVixDQUFBLENBQVQsQ0FBaEI7YUFDSCxRQUFBLENBQVMsSUFBVCxFQURHO0tBQUEsTUFJQSxJQUFHLFNBQUEsS0FBYSxTQUFTLENBQUMsV0FBVixDQUFBLENBQWhCO2FBQ0gsSUFBSSxDQUFDLFdBQUwsQ0FBQSxFQURHO0tBQUEsTUFJQSxJQUFHLFNBQUEsS0FBYSxTQUFTLENBQUMsV0FBVixDQUFBLENBQWhCO2FBQ0gsSUFBSSxDQUFDLFdBQUwsQ0FBQSxFQURHO0tBQUEsTUFBQTthQUdILEtBSEc7O0VBZFE7O0VBb0JmLE1BQU0sQ0FBQyxPQUFQLEdBQWlCO0lBQ2YsWUFBQSxVQURlO0lBQ0gsYUFBQSxXQURHO0lBQ1UsaUJBQUEsZUFEVjtJQUMyQiw4QkFBQSw0QkFEM0I7SUFFZix5QkFBQSx1QkFGZTtJQUVVLFFBQUEsTUFGVjtJQUVrQixjQUFBLFlBRmxCOztBQTdEakIiLCJzb3VyY2VzQ29udGVudCI6WyJfID0gcmVxdWlyZSAndW5kZXJzY29yZS1wbHVzJ1xuXG5lc2NhcGVOb2RlID0gbnVsbFxuXG5lc2NhcGVIdG1sID0gKHN0cikgLT5cbiAgZXNjYXBlTm9kZSA/PSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICBlc2NhcGVOb2RlLmlubmVyVGV4dCA9IHN0clxuICBlc2NhcGVOb2RlLmlubmVySFRNTFxuXG5lc2NhcGVSZWdleCA9IChzdHIpIC0+XG4gIHN0ci5yZXBsYWNlIC9bLj8qK14kW1xcXVxcXFwoKXt9fC1dL2csIChtYXRjaCkgLT4gXCJcXFxcXCIgKyBtYXRjaFxuXG5zYW5pdGl6ZVBhdHRlcm4gPSAocGF0dGVybikgLT5cbiAgcGF0dGVybiA9IGVzY2FwZUh0bWwocGF0dGVybilcbiAgcGF0dGVybi5yZXBsYWNlKC9cXG4vZywgJ1xcXFxuJykucmVwbGFjZSgvXFx0L2csICdcXFxcdCcpXG5cbmdldFJlcGxhY2VtZW50UmVzdWx0c01lc3NhZ2UgPSAoe2ZpbmRQYXR0ZXJuLCByZXBsYWNlUGF0dGVybiwgcmVwbGFjZWRQYXRoQ291bnQsIHJlcGxhY2VtZW50Q291bnR9KSAtPlxuICBpZiByZXBsYWNlZFBhdGhDb3VudFxuICAgIFwiPHNwYW4gY2xhc3M9XFxcInRleHQtaGlnaGxpZ2h0XFxcIj5SZXBsYWNlZCA8c3BhbiBjbGFzcz1cXFwiaGlnaGxpZ2h0LWVycm9yXFxcIj4je3Nhbml0aXplUGF0dGVybihmaW5kUGF0dGVybil9PC9zcGFuPiB3aXRoIDxzcGFuIGNsYXNzPVxcXCJoaWdobGlnaHQtc3VjY2Vzc1xcXCI+I3tzYW5pdGl6ZVBhdHRlcm4ocmVwbGFjZVBhdHRlcm4pfTwvc3Bhbj4gI3tfLnBsdXJhbGl6ZShyZXBsYWNlbWVudENvdW50LCAndGltZScpfSBpbiAje18ucGx1cmFsaXplKHJlcGxhY2VkUGF0aENvdW50LCAnZmlsZScpfTwvc3Bhbj5cIlxuICBlbHNlXG4gICAgXCI8c3BhbiBjbGFzcz1cXFwidGV4dC1oaWdobGlnaHRcXFwiPk5vdGhpbmcgcmVwbGFjZWQ8L3NwYW4+XCJcblxuZ2V0U2VhcmNoUmVzdWx0c01lc3NhZ2UgPSAocmVzdWx0cykgLT5cbiAgaWYgcmVzdWx0cz8uZmluZFBhdHRlcm4/XG4gICAge2ZpbmRQYXR0ZXJuLCBtYXRjaENvdW50LCBwYXRoQ291bnQsIHJlcGxhY2VkUGF0aENvdW50fSA9IHJlc3VsdHNcbiAgICBpZiBtYXRjaENvdW50XG4gICAgICBcIiN7Xy5wbHVyYWxpemUobWF0Y2hDb3VudCwgJ3Jlc3VsdCcpfSBmb3VuZCBpbiAje18ucGx1cmFsaXplKHBhdGhDb3VudCwgJ2ZpbGUnKX0gZm9yIDxzcGFuIGNsYXNzPVxcXCJoaWdobGlnaHQtaW5mb1xcXCI+I3tzYW5pdGl6ZVBhdHRlcm4oZmluZFBhdHRlcm4pfTwvc3Bhbj5cIlxuICAgIGVsc2VcbiAgICAgIFwiTm8gI3tpZiByZXBsYWNlZFBhdGhDb3VudD8gdGhlbiAnbW9yZScgZWxzZSAnJ30gcmVzdWx0cyBmb3VuZCBmb3IgJyN7c2FuaXRpemVQYXR0ZXJuKGZpbmRQYXR0ZXJuKX0nXCJcbiAgZWxzZVxuICAgICcnXG5cbnNob3dJZiA9IChjb25kaXRpb24pIC0+XG4gIGlmIGNvbmRpdGlvblxuICAgIG51bGxcbiAgZWxzZVxuICAgIHtkaXNwbGF5OiAnbm9uZSd9XG5cbmNhcGl0YWxpemUgPSAoc3RyKSAtPiBzdHJbMF0udG9VcHBlckNhc2UoKSArIHN0ci50b0xvd2VyQ2FzZSgpLnNsaWNlKDEpXG50aXRsZWl6ZSA9IChzdHIpIC0+IHN0ci50b0xvd2VyQ2FzZSgpLnJlcGxhY2UoLyg/Ol58XFxzKVxcUy9nLCAoY2FwaXRhbCkgLT4gY2FwaXRhbC50b1VwcGVyQ2FzZSgpKVxuXG5wcmVzZXJ2ZUNhc2UgPSAodGV4dCwgcmVmZXJlbmNlKSAtPlxuICAjIElmIHJlcGxhY2VkIHRleHQgaXMgY2FwaXRhbGl6ZWQgKHN0cmljdCkgbGlrZSBhIHNlbnRlbmNlLCBjYXBpdGFsaXplIHJlcGxhY2VtZW50XG4gIGlmIHJlZmVyZW5jZSBpcyBjYXBpdGFsaXplKHJlZmVyZW5jZS50b0xvd2VyQ2FzZSgpKVxuICAgIGNhcGl0YWxpemUodGV4dClcblxuICAjIElmIHJlcGxhY2VkIHRleHQgaXMgdGl0bGVpemVkIChpLmUuLCBlYWNoIHdvcmQgc3RhcnQgd2l0aCBhbiB1cHBlcmNhc2UpLCB0aXRsZWl6ZSByZXBsYWNlbWVudFxuICBlbHNlIGlmIHJlZmVyZW5jZSBpcyB0aXRsZWl6ZShyZWZlcmVuY2UudG9Mb3dlckNhc2UoKSlcbiAgICB0aXRsZWl6ZSh0ZXh0KVxuXG4gICMgSWYgcmVwbGFjZWQgdGV4dCBpcyB1cHBlcmNhc2UsIHVwcGVyY2FzZSByZXBsYWNlbWVudFxuICBlbHNlIGlmIHJlZmVyZW5jZSBpcyByZWZlcmVuY2UudG9VcHBlckNhc2UoKVxuICAgIHRleHQudG9VcHBlckNhc2UoKVxuXG4gICMgSWYgcmVwbGFjZWQgdGV4dCBpcyBsb3dlcmNhc2UsIGxvd2VyY2FzZSByZXBsYWNlbWVudFxuICBlbHNlIGlmIHJlZmVyZW5jZSBpcyByZWZlcmVuY2UudG9Mb3dlckNhc2UoKVxuICAgIHRleHQudG9Mb3dlckNhc2UoKVxuICBlbHNlXG4gICAgdGV4dFxuXG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICBlc2NhcGVIdG1sLCBlc2NhcGVSZWdleCwgc2FuaXRpemVQYXR0ZXJuLCBnZXRSZXBsYWNlbWVudFJlc3VsdHNNZXNzYWdlLFxuICBnZXRTZWFyY2hSZXN1bHRzTWVzc2FnZSwgc2hvd0lmLCBwcmVzZXJ2ZUNhc2Vcbn1cbiJdfQ==
