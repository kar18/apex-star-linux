(function() {
  var RootDragAndDropHandler, ipcRenderer, ref, remote, url,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  url = require('url');

  ref = require('electron'), ipcRenderer = ref.ipcRenderer, remote = ref.remote;

  module.exports = RootDragAndDropHandler = (function() {
    function RootDragAndDropHandler(treeView) {
      this.treeView = treeView;
      this.onDrop = bind(this.onDrop, this);
      this.onDropOnOtherWindow = bind(this.onDropOnOtherWindow, this);
      this.onDragOver = bind(this.onDragOver, this);
      this.onDragEnd = bind(this.onDragEnd, this);
      this.onDragLeave = bind(this.onDragLeave, this);
      this.onDragStart = bind(this.onDragStart, this);
      ipcRenderer.on('tree-view:project-folder-dropped', this.onDropOnOtherWindow);
      this.handleEvents();
    }

    RootDragAndDropHandler.prototype.dispose = function() {
      return ipcRenderer.removeListener('tree-view:project-folder-dropped', this.onDropOnOtherWindow);
    };

    RootDragAndDropHandler.prototype.handleEvents = function() {
      this.treeView.element.addEventListener('dragenter', this.onDragEnter.bind(this));
      this.treeView.element.addEventListener('dragend', this.onDragEnd.bind(this));
      this.treeView.element.addEventListener('dragleave', this.onDragLeave.bind(this));
      this.treeView.element.addEventListener('dragover', this.onDragOver.bind(this));
      return this.treeView.element.addEventListener('drop', this.onDrop.bind(this));
    };

    RootDragAndDropHandler.prototype.onDragStart = function(e) {
      var directory, i, index, len, pathUri, projectRoot, ref1, ref2, root, rootIndex;
      if (!this.treeView.list.contains(e.target)) {
        return;
      }
      this.prevDropTargetIndex = null;
      e.dataTransfer.setData('atom-tree-view-root-event', 'true');
      projectRoot = e.target.closest('.project-root');
      directory = projectRoot.directory;
      e.dataTransfer.setData('project-root-index', Array.from(projectRoot.parentElement.children).indexOf(projectRoot));
      rootIndex = -1;
      ref1 = this.treeView.roots;
      for (index = i = 0, len = ref1.length; i < len; index = ++i) {
        root = ref1[index];
        if (root.directory === directory) {
          rootIndex = index;
          break;
        }
      }
      e.dataTransfer.setData('from-root-index', rootIndex);
      e.dataTransfer.setData('from-root-path', directory.path);
      e.dataTransfer.setData('from-window-id', this.getWindowId());
      e.dataTransfer.setData('text/plain', directory.path);
      if ((ref2 = process.platform) === 'darwin' || ref2 === 'linux') {
        if (!this.uriHasProtocol(directory.path)) {
          pathUri = "file://" + directory.path;
        }
        return e.dataTransfer.setData('text/uri-list', pathUri);
      }
    };

    RootDragAndDropHandler.prototype.uriHasProtocol = function(uri) {
      var error;
      try {
        return url.parse(uri).protocol != null;
      } catch (error1) {
        error = error1;
        return false;
      }
    };

    RootDragAndDropHandler.prototype.onDragEnter = function(e) {
      if (!this.treeView.list.contains(e.target)) {
        return;
      }
      if (!this.isAtomTreeViewEvent(e)) {
        return;
      }
      return e.stopPropagation();
    };

    RootDragAndDropHandler.prototype.onDragLeave = function(e) {
      if (!this.treeView.list.contains(e.target)) {
        return;
      }
      if (!this.isAtomTreeViewEvent(e)) {
        return;
      }
      e.stopPropagation();
      if (e.target === e.currentTarget) {
        return this.removePlaceholder();
      }
    };

    RootDragAndDropHandler.prototype.onDragEnd = function(e) {
      if (!e.target.matches('.project-root-header')) {
        return;
      }
      if (!this.isAtomTreeViewEvent(e)) {
        return;
      }
      e.stopPropagation();
      return this.clearDropTarget();
    };

    RootDragAndDropHandler.prototype.onDragOver = function(e) {
      var element, entry, newDropTargetIndex, projectRoots;
      if (!this.treeView.list.contains(e.target)) {
        return;
      }
      if (!this.isAtomTreeViewEvent(e)) {
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      entry = e.currentTarget;
      if (this.treeView.roots.length === 0) {
        this.treeView.list.appendChild(this.getPlaceholder());
        return;
      }
      newDropTargetIndex = this.getDropTargetIndex(e);
      if (newDropTargetIndex == null) {
        return;
      }
      if (this.prevDropTargetIndex === newDropTargetIndex) {
        return;
      }
      this.prevDropTargetIndex = newDropTargetIndex;
      projectRoots = this.treeView.roots;
      if (newDropTargetIndex < projectRoots.length) {
        element = projectRoots[newDropTargetIndex];
        element.classList.add('is-drop-target');
        return element.parentElement.insertBefore(this.getPlaceholder(), element);
      } else {
        element = projectRoots[newDropTargetIndex - 1];
        element.classList.add('drop-target-is-after');
        return element.parentElement.insertBefore(this.getPlaceholder(), element.nextSibling);
      }
    };

    RootDragAndDropHandler.prototype.onDropOnOtherWindow = function(e, fromItemIndex) {
      var paths;
      paths = atom.project.getPaths();
      paths.splice(fromItemIndex, 1);
      atom.project.setPaths(paths);
      return this.clearDropTarget();
    };

    RootDragAndDropHandler.prototype.clearDropTarget = function() {
      var element;
      element = this.treeView.element.querySelector(".is-dragging");
      if (element != null) {
        element.classList.remove('is-dragging');
      }
      if (element != null) {
        element.updateTooltip();
      }
      return this.removePlaceholder();
    };

    RootDragAndDropHandler.prototype.onDrop = function(e) {
      var browserWindow, dataTransfer, fromIndex, fromRootIndex, fromRootPath, fromWindowId, projectPaths, toIndex;
      if (!this.treeView.list.contains(e.target)) {
        return;
      }
      if (!this.isAtomTreeViewEvent(e)) {
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      dataTransfer = e.dataTransfer;
      fromWindowId = parseInt(dataTransfer.getData('from-window-id'));
      fromRootPath = dataTransfer.getData('from-root-path');
      fromIndex = parseInt(dataTransfer.getData('project-root-index'));
      fromRootIndex = parseInt(dataTransfer.getData('from-root-index'));
      toIndex = this.getDropTargetIndex(e);
      this.clearDropTarget();
      if (fromWindowId === this.getWindowId()) {
        if (fromIndex !== toIndex) {
          projectPaths = atom.project.getPaths();
          projectPaths.splice(fromIndex, 1);
          if (toIndex > fromIndex) {
            toIndex -= 1;
          }
          projectPaths.splice(toIndex, 0, fromRootPath);
          return atom.project.setPaths(projectPaths);
        }
      } else {
        projectPaths = atom.project.getPaths();
        projectPaths.splice(toIndex, 0, fromRootPath);
        atom.project.setPaths(projectPaths);
        if (!isNaN(fromWindowId)) {
          browserWindow = remote.BrowserWindow.fromId(fromWindowId);
          return browserWindow != null ? browserWindow.webContents.send('tree-view:project-folder-dropped', fromIndex) : void 0;
        }
      }
    };

    RootDragAndDropHandler.prototype.getDropTargetIndex = function(e) {
      var center, projectRoot, projectRootIndex, projectRoots;
      if (this.isPlaceholder(e.target)) {
        return;
      }
      projectRoots = this.treeView.roots;
      projectRoot = e.target.closest('.project-root');
      if (!projectRoot) {
        projectRoot = projectRoots[projectRoots.length - 1];
      }
      if (!projectRoot) {
        return 0;
      }
      projectRootIndex = this.treeView.roots.indexOf(projectRoot);
      center = projectRoot.getBoundingClientRect().top + projectRoot.offsetHeight / 2;
      if (e.pageY < center) {
        return projectRootIndex;
      } else {
        return projectRootIndex + 1;
      }
    };

    RootDragAndDropHandler.prototype.canDragStart = function(e) {
      return e.target.closest('.project-root-header');
    };

    RootDragAndDropHandler.prototype.isDragging = function(e) {
      var i, item, len, ref1;
      ref1 = e.dataTransfer.items;
      for (i = 0, len = ref1.length; i < len; i++) {
        item = ref1[i];
        if (item.type === 'from-root-path') {
          return true;
        }
      }
      return false;
    };

    RootDragAndDropHandler.prototype.isAtomTreeViewEvent = function(e) {
      var i, item, len, ref1;
      ref1 = e.dataTransfer.items;
      for (i = 0, len = ref1.length; i < len; i++) {
        item = ref1[i];
        if (item.type === 'atom-tree-view-root-event') {
          return true;
        }
      }
      return false;
    };

    RootDragAndDropHandler.prototype.getPlaceholder = function() {
      if (!this.placeholderEl) {
        this.placeholderEl = document.createElement('li');
        this.placeholderEl.classList.add('placeholder');
      }
      return this.placeholderEl;
    };

    RootDragAndDropHandler.prototype.removePlaceholder = function() {
      var ref1;
      if ((ref1 = this.placeholderEl) != null) {
        ref1.remove();
      }
      return this.placeholderEl = null;
    };

    RootDragAndDropHandler.prototype.isPlaceholder = function(element) {
      return element.classList.contains('.placeholder');
    };

    RootDragAndDropHandler.prototype.getWindowId = function() {
      return this.processId != null ? this.processId : this.processId = atom.getCurrentWindow().id;
    };

    return RootDragAndDropHandler;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90cmVlLXZpZXcvbGliL3Jvb3QtZHJhZy1hbmQtZHJvcC5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBLHFEQUFBO0lBQUE7O0VBQUEsR0FBQSxHQUFNLE9BQUEsQ0FBUSxLQUFSOztFQUVOLE1BQXdCLE9BQUEsQ0FBUSxVQUFSLENBQXhCLEVBQUMsNkJBQUQsRUFBYzs7RUFLZCxNQUFNLENBQUMsT0FBUCxHQUNNO0lBQ1MsZ0NBQUMsUUFBRDtNQUFDLElBQUMsQ0FBQSxXQUFEOzs7Ozs7O01BQ1osV0FBVyxDQUFDLEVBQVosQ0FBZSxrQ0FBZixFQUFtRCxJQUFDLENBQUEsbUJBQXBEO01BQ0EsSUFBQyxDQUFBLFlBQUQsQ0FBQTtJQUZXOztxQ0FJYixPQUFBLEdBQVMsU0FBQTthQUNQLFdBQVcsQ0FBQyxjQUFaLENBQTJCLGtDQUEzQixFQUErRCxJQUFDLENBQUEsbUJBQWhFO0lBRE87O3FDQUdULFlBQUEsR0FBYyxTQUFBO01BR1osSUFBQyxDQUFBLFFBQVEsQ0FBQyxPQUFPLENBQUMsZ0JBQWxCLENBQW1DLFdBQW5DLEVBQWdELElBQUMsQ0FBQSxXQUFXLENBQUMsSUFBYixDQUFrQixJQUFsQixDQUFoRDtNQUNBLElBQUMsQ0FBQSxRQUFRLENBQUMsT0FBTyxDQUFDLGdCQUFsQixDQUFtQyxTQUFuQyxFQUE4QyxJQUFDLENBQUEsU0FBUyxDQUFDLElBQVgsQ0FBZ0IsSUFBaEIsQ0FBOUM7TUFDQSxJQUFDLENBQUEsUUFBUSxDQUFDLE9BQU8sQ0FBQyxnQkFBbEIsQ0FBbUMsV0FBbkMsRUFBZ0QsSUFBQyxDQUFBLFdBQVcsQ0FBQyxJQUFiLENBQWtCLElBQWxCLENBQWhEO01BQ0EsSUFBQyxDQUFBLFFBQVEsQ0FBQyxPQUFPLENBQUMsZ0JBQWxCLENBQW1DLFVBQW5DLEVBQStDLElBQUMsQ0FBQSxVQUFVLENBQUMsSUFBWixDQUFpQixJQUFqQixDQUEvQzthQUNBLElBQUMsQ0FBQSxRQUFRLENBQUMsT0FBTyxDQUFDLGdCQUFsQixDQUFtQyxNQUFuQyxFQUEyQyxJQUFDLENBQUEsTUFBTSxDQUFDLElBQVIsQ0FBYSxJQUFiLENBQTNDO0lBUFk7O3FDQVNkLFdBQUEsR0FBYSxTQUFDLENBQUQ7QUFDWCxVQUFBO01BQUEsSUFBQSxDQUFjLElBQUMsQ0FBQSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQWYsQ0FBd0IsQ0FBQyxDQUFDLE1BQTFCLENBQWQ7QUFBQSxlQUFBOztNQUVBLElBQUMsQ0FBQSxtQkFBRCxHQUF1QjtNQUN2QixDQUFDLENBQUMsWUFBWSxDQUFDLE9BQWYsQ0FBdUIsMkJBQXZCLEVBQW9ELE1BQXBEO01BQ0EsV0FBQSxHQUFjLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBVCxDQUFpQixlQUFqQjtNQUNkLFNBQUEsR0FBWSxXQUFXLENBQUM7TUFFeEIsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFmLENBQXVCLG9CQUF2QixFQUE2QyxLQUFLLENBQUMsSUFBTixDQUFXLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBckMsQ0FBOEMsQ0FBQyxPQUEvQyxDQUF1RCxXQUF2RCxDQUE3QztNQUVBLFNBQUEsR0FBWSxDQUFDO0FBQ2I7QUFBQSxXQUFBLHNEQUFBOztZQUFtRSxJQUFJLENBQUMsU0FBTCxLQUFrQjtVQUFwRixTQUFBLEdBQVk7QUFBTzs7QUFBcEI7TUFFQSxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQWYsQ0FBdUIsaUJBQXZCLEVBQTBDLFNBQTFDO01BQ0EsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFmLENBQXVCLGdCQUF2QixFQUF5QyxTQUFTLENBQUMsSUFBbkQ7TUFDQSxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQWYsQ0FBdUIsZ0JBQXZCLEVBQXlDLElBQUMsQ0FBQSxXQUFELENBQUEsQ0FBekM7TUFFQSxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQWYsQ0FBdUIsWUFBdkIsRUFBcUMsU0FBUyxDQUFDLElBQS9DO01BRUEsWUFBRyxPQUFPLENBQUMsU0FBUixLQUFxQixRQUFyQixJQUFBLElBQUEsS0FBK0IsT0FBbEM7UUFDRSxJQUFBLENBQTRDLElBQUMsQ0FBQSxjQUFELENBQWdCLFNBQVMsQ0FBQyxJQUExQixDQUE1QztVQUFBLE9BQUEsR0FBVSxTQUFBLEdBQVUsU0FBUyxDQUFDLEtBQTlCOztlQUNBLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBZixDQUF1QixlQUF2QixFQUF3QyxPQUF4QyxFQUZGOztJQW5CVzs7cUNBdUJiLGNBQUEsR0FBZ0IsU0FBQyxHQUFEO0FBQ2QsVUFBQTtBQUFBO2VBQ0UsZ0NBREY7T0FBQSxjQUFBO1FBRU07ZUFDSixNQUhGOztJQURjOztxQ0FNaEIsV0FBQSxHQUFhLFNBQUMsQ0FBRDtNQUNYLElBQUEsQ0FBYyxJQUFDLENBQUEsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFmLENBQXdCLENBQUMsQ0FBQyxNQUExQixDQUFkO0FBQUEsZUFBQTs7TUFDQSxJQUFBLENBQWMsSUFBQyxDQUFBLG1CQUFELENBQXFCLENBQXJCLENBQWQ7QUFBQSxlQUFBOzthQUVBLENBQUMsQ0FBQyxlQUFGLENBQUE7SUFKVzs7cUNBTWIsV0FBQSxHQUFhLFNBQUMsQ0FBRDtNQUNYLElBQUEsQ0FBYyxJQUFDLENBQUEsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFmLENBQXdCLENBQUMsQ0FBQyxNQUExQixDQUFkO0FBQUEsZUFBQTs7TUFDQSxJQUFBLENBQWMsSUFBQyxDQUFBLG1CQUFELENBQXFCLENBQXJCLENBQWQ7QUFBQSxlQUFBOztNQUVBLENBQUMsQ0FBQyxlQUFGLENBQUE7TUFDQSxJQUF3QixDQUFDLENBQUMsTUFBRixLQUFZLENBQUMsQ0FBQyxhQUF0QztlQUFBLElBQUMsQ0FBQSxpQkFBRCxDQUFBLEVBQUE7O0lBTFc7O3FDQU9iLFNBQUEsR0FBVyxTQUFDLENBQUQ7TUFDVCxJQUFBLENBQWMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFULENBQWlCLHNCQUFqQixDQUFkO0FBQUEsZUFBQTs7TUFDQSxJQUFBLENBQWMsSUFBQyxDQUFBLG1CQUFELENBQXFCLENBQXJCLENBQWQ7QUFBQSxlQUFBOztNQUVBLENBQUMsQ0FBQyxlQUFGLENBQUE7YUFDQSxJQUFDLENBQUEsZUFBRCxDQUFBO0lBTFM7O3FDQU9YLFVBQUEsR0FBWSxTQUFDLENBQUQ7QUFDVixVQUFBO01BQUEsSUFBQSxDQUFjLElBQUMsQ0FBQSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQWYsQ0FBd0IsQ0FBQyxDQUFDLE1BQTFCLENBQWQ7QUFBQSxlQUFBOztNQUNBLElBQUEsQ0FBYyxJQUFDLENBQUEsbUJBQUQsQ0FBcUIsQ0FBckIsQ0FBZDtBQUFBLGVBQUE7O01BRUEsQ0FBQyxDQUFDLGNBQUYsQ0FBQTtNQUNBLENBQUMsQ0FBQyxlQUFGLENBQUE7TUFFQSxLQUFBLEdBQVEsQ0FBQyxDQUFDO01BRVYsSUFBRyxJQUFDLENBQUEsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFoQixLQUEwQixDQUE3QjtRQUNFLElBQUMsQ0FBQSxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQWYsQ0FBMkIsSUFBQyxDQUFBLGNBQUQsQ0FBQSxDQUEzQjtBQUNBLGVBRkY7O01BSUEsa0JBQUEsR0FBcUIsSUFBQyxDQUFBLGtCQUFELENBQW9CLENBQXBCO01BQ3JCLElBQWMsMEJBQWQ7QUFBQSxlQUFBOztNQUNBLElBQVUsSUFBQyxDQUFBLG1CQUFELEtBQXdCLGtCQUFsQztBQUFBLGVBQUE7O01BQ0EsSUFBQyxDQUFBLG1CQUFELEdBQXVCO01BRXZCLFlBQUEsR0FBZSxJQUFDLENBQUEsUUFBUSxDQUFDO01BRXpCLElBQUcsa0JBQUEsR0FBcUIsWUFBWSxDQUFDLE1BQXJDO1FBQ0UsT0FBQSxHQUFVLFlBQWEsQ0FBQSxrQkFBQTtRQUN2QixPQUFPLENBQUMsU0FBUyxDQUFDLEdBQWxCLENBQXNCLGdCQUF0QjtlQUNBLE9BQU8sQ0FBQyxhQUFhLENBQUMsWUFBdEIsQ0FBbUMsSUFBQyxDQUFBLGNBQUQsQ0FBQSxDQUFuQyxFQUFzRCxPQUF0RCxFQUhGO09BQUEsTUFBQTtRQUtFLE9BQUEsR0FBVSxZQUFhLENBQUEsa0JBQUEsR0FBcUIsQ0FBckI7UUFDdkIsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFsQixDQUFzQixzQkFBdEI7ZUFDQSxPQUFPLENBQUMsYUFBYSxDQUFDLFlBQXRCLENBQW1DLElBQUMsQ0FBQSxjQUFELENBQUEsQ0FBbkMsRUFBc0QsT0FBTyxDQUFDLFdBQTlELEVBUEY7O0lBcEJVOztxQ0E2QlosbUJBQUEsR0FBcUIsU0FBQyxDQUFELEVBQUksYUFBSjtBQUNuQixVQUFBO01BQUEsS0FBQSxHQUFRLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBYixDQUFBO01BQ1IsS0FBSyxDQUFDLE1BQU4sQ0FBYSxhQUFiLEVBQTRCLENBQTVCO01BQ0EsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFiLENBQXNCLEtBQXRCO2FBRUEsSUFBQyxDQUFBLGVBQUQsQ0FBQTtJQUxtQjs7cUNBT3JCLGVBQUEsR0FBaUIsU0FBQTtBQUNmLFVBQUE7TUFBQSxPQUFBLEdBQVUsSUFBQyxDQUFBLFFBQVEsQ0FBQyxPQUFPLENBQUMsYUFBbEIsQ0FBZ0MsY0FBaEM7O1FBQ1YsT0FBTyxDQUFFLFNBQVMsQ0FBQyxNQUFuQixDQUEwQixhQUExQjs7O1FBQ0EsT0FBTyxDQUFFLGFBQVQsQ0FBQTs7YUFDQSxJQUFDLENBQUEsaUJBQUQsQ0FBQTtJQUplOztxQ0FNakIsTUFBQSxHQUFRLFNBQUMsQ0FBRDtBQUNOLFVBQUE7TUFBQSxJQUFBLENBQWMsSUFBQyxDQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBZixDQUF3QixDQUFDLENBQUMsTUFBMUIsQ0FBZDtBQUFBLGVBQUE7O01BQ0EsSUFBQSxDQUFjLElBQUMsQ0FBQSxtQkFBRCxDQUFxQixDQUFyQixDQUFkO0FBQUEsZUFBQTs7TUFFQSxDQUFDLENBQUMsY0FBRixDQUFBO01BQ0EsQ0FBQyxDQUFDLGVBQUYsQ0FBQTtNQUVDLGVBQWdCO01BRWpCLFlBQUEsR0FBZSxRQUFBLENBQVMsWUFBWSxDQUFDLE9BQWIsQ0FBcUIsZ0JBQXJCLENBQVQ7TUFDZixZQUFBLEdBQWdCLFlBQVksQ0FBQyxPQUFiLENBQXFCLGdCQUFyQjtNQUNoQixTQUFBLEdBQWdCLFFBQUEsQ0FBUyxZQUFZLENBQUMsT0FBYixDQUFxQixvQkFBckIsQ0FBVDtNQUNoQixhQUFBLEdBQWdCLFFBQUEsQ0FBUyxZQUFZLENBQUMsT0FBYixDQUFxQixpQkFBckIsQ0FBVDtNQUVoQixPQUFBLEdBQVUsSUFBQyxDQUFBLGtCQUFELENBQW9CLENBQXBCO01BRVYsSUFBQyxDQUFBLGVBQUQsQ0FBQTtNQUVBLElBQUcsWUFBQSxLQUFnQixJQUFDLENBQUEsV0FBRCxDQUFBLENBQW5CO1FBQ0UsSUFBTyxTQUFBLEtBQWEsT0FBcEI7VUFDRSxZQUFBLEdBQWUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFiLENBQUE7VUFDZixZQUFZLENBQUMsTUFBYixDQUFvQixTQUFwQixFQUErQixDQUEvQjtVQUNBLElBQUcsT0FBQSxHQUFVLFNBQWI7WUFBNEIsT0FBQSxJQUFXLEVBQXZDOztVQUNBLFlBQVksQ0FBQyxNQUFiLENBQW9CLE9BQXBCLEVBQTZCLENBQTdCLEVBQWdDLFlBQWhDO2lCQUNBLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBYixDQUFzQixZQUF0QixFQUxGO1NBREY7T0FBQSxNQUFBO1FBUUUsWUFBQSxHQUFlLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBYixDQUFBO1FBQ2YsWUFBWSxDQUFDLE1BQWIsQ0FBb0IsT0FBcEIsRUFBNkIsQ0FBN0IsRUFBZ0MsWUFBaEM7UUFDQSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQWIsQ0FBc0IsWUFBdEI7UUFFQSxJQUFHLENBQUksS0FBQSxDQUFNLFlBQU4sQ0FBUDtVQUVFLGFBQUEsR0FBZ0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFyQixDQUE0QixZQUE1Qjt5Q0FDaEIsYUFBYSxDQUFFLFdBQVcsQ0FBQyxJQUEzQixDQUFnQyxrQ0FBaEMsRUFBb0UsU0FBcEUsV0FIRjtTQVpGOztJQWxCTTs7cUNBbUNSLGtCQUFBLEdBQW9CLFNBQUMsQ0FBRDtBQUNsQixVQUFBO01BQUEsSUFBVSxJQUFDLENBQUEsYUFBRCxDQUFlLENBQUMsQ0FBQyxNQUFqQixDQUFWO0FBQUEsZUFBQTs7TUFFQSxZQUFBLEdBQWUsSUFBQyxDQUFBLFFBQVEsQ0FBQztNQUN6QixXQUFBLEdBQWMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFULENBQWlCLGVBQWpCO01BQ2QsSUFBQSxDQUEyRCxXQUEzRDtRQUFBLFdBQUEsR0FBYyxZQUFhLENBQUEsWUFBWSxDQUFDLE1BQWIsR0FBc0IsQ0FBdEIsRUFBM0I7O01BRUEsSUFBQSxDQUFnQixXQUFoQjtBQUFBLGVBQU8sRUFBUDs7TUFFQSxnQkFBQSxHQUFtQixJQUFDLENBQUEsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFoQixDQUF3QixXQUF4QjtNQUVuQixNQUFBLEdBQVMsV0FBVyxDQUFDLHFCQUFaLENBQUEsQ0FBbUMsQ0FBQyxHQUFwQyxHQUEwQyxXQUFXLENBQUMsWUFBWixHQUEyQjtNQUU5RSxJQUFHLENBQUMsQ0FBQyxLQUFGLEdBQVUsTUFBYjtlQUNFLGlCQURGO09BQUEsTUFBQTtlQUdFLGdCQUFBLEdBQW1CLEVBSHJCOztJQWJrQjs7cUNBa0JwQixZQUFBLEdBQWMsU0FBQyxDQUFEO2FBQ1osQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFULENBQWlCLHNCQUFqQjtJQURZOztxQ0FHZCxVQUFBLEdBQVksU0FBQyxDQUFEO0FBQ1YsVUFBQTtBQUFBO0FBQUEsV0FBQSxzQ0FBQTs7UUFDRSxJQUFHLElBQUksQ0FBQyxJQUFMLEtBQWEsZ0JBQWhCO0FBQ0UsaUJBQU8sS0FEVDs7QUFERjtBQUlBLGFBQU87SUFMRzs7cUNBT1osbUJBQUEsR0FBcUIsU0FBQyxDQUFEO0FBQ25CLFVBQUE7QUFBQTtBQUFBLFdBQUEsc0NBQUE7O1FBQ0UsSUFBRyxJQUFJLENBQUMsSUFBTCxLQUFhLDJCQUFoQjtBQUNFLGlCQUFPLEtBRFQ7O0FBREY7QUFJQSxhQUFPO0lBTFk7O3FDQU9yQixjQUFBLEdBQWdCLFNBQUE7TUFDZCxJQUFBLENBQU8sSUFBQyxDQUFBLGFBQVI7UUFDRSxJQUFDLENBQUEsYUFBRCxHQUFpQixRQUFRLENBQUMsYUFBVCxDQUF1QixJQUF2QjtRQUNqQixJQUFDLENBQUEsYUFBYSxDQUFDLFNBQVMsQ0FBQyxHQUF6QixDQUE2QixhQUE3QixFQUZGOzthQUdBLElBQUMsQ0FBQTtJQUphOztxQ0FNaEIsaUJBQUEsR0FBbUIsU0FBQTtBQUNqQixVQUFBOztZQUFjLENBQUUsTUFBaEIsQ0FBQTs7YUFDQSxJQUFDLENBQUEsYUFBRCxHQUFpQjtJQUZBOztxQ0FJbkIsYUFBQSxHQUFlLFNBQUMsT0FBRDthQUNiLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBbEIsQ0FBMkIsY0FBM0I7SUFEYTs7cUNBR2YsV0FBQSxHQUFhLFNBQUE7c0NBQ1gsSUFBQyxDQUFBLFlBQUQsSUFBQyxDQUFBLFlBQWEsSUFBSSxDQUFDLGdCQUFMLENBQUEsQ0FBdUIsQ0FBQztJQUQzQjs7Ozs7QUF2TWYiLCJzb3VyY2VzQ29udGVudCI6WyJ1cmwgPSByZXF1aXJlICd1cmwnXG5cbntpcGNSZW5kZXJlciwgcmVtb3RlfSA9IHJlcXVpcmUgJ2VsZWN0cm9uJ1xuXG4jIFRPRE86IFN1cHBvcnQgZHJhZ2dpbmcgZXh0ZXJuYWwgZm9sZGVycyBhbmQgdXNpbmcgdGhlIGRyYWctYW5kLWRyb3AgaW5kaWNhdG9ycyBmb3IgdGhlbVxuIyBDdXJyZW50bHkgdGhleSdyZSBoYW5kbGVkIGluIFRyZWVWaWV3J3MgZHJhZyBsaXN0ZW5lcnNcblxubW9kdWxlLmV4cG9ydHMgPVxuY2xhc3MgUm9vdERyYWdBbmREcm9wSGFuZGxlclxuICBjb25zdHJ1Y3RvcjogKEB0cmVlVmlldykgLT5cbiAgICBpcGNSZW5kZXJlci5vbigndHJlZS12aWV3OnByb2plY3QtZm9sZGVyLWRyb3BwZWQnLCBAb25Ecm9wT25PdGhlcldpbmRvdylcbiAgICBAaGFuZGxlRXZlbnRzKClcblxuICBkaXNwb3NlOiAtPlxuICAgIGlwY1JlbmRlcmVyLnJlbW92ZUxpc3RlbmVyKCd0cmVlLXZpZXc6cHJvamVjdC1mb2xkZXItZHJvcHBlZCcsIEBvbkRyb3BPbk90aGVyV2luZG93KVxuXG4gIGhhbmRsZUV2ZW50czogLT5cbiAgICAjIG9uRHJhZ1N0YXJ0IGlzIGNhbGxlZCBkaXJlY3RseSBieSBUcmVlVmlldydzIG9uRHJhZ1N0YXJ0XG4gICAgIyB3aWxsIGJlIGNsZWFuZWQgdXAgYnkgdHJlZSB2aWV3LCBzaW5jZSB0aGV5IGFyZSB0cmVlLXZpZXcncyBoYW5kbGVyc1xuICAgIEB0cmVlVmlldy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgJ2RyYWdlbnRlcicsIEBvbkRyYWdFbnRlci5iaW5kKHRoaXMpXG4gICAgQHRyZWVWaWV3LmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lciAnZHJhZ2VuZCcsIEBvbkRyYWdFbmQuYmluZCh0aGlzKVxuICAgIEB0cmVlVmlldy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgJ2RyYWdsZWF2ZScsIEBvbkRyYWdMZWF2ZS5iaW5kKHRoaXMpXG4gICAgQHRyZWVWaWV3LmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lciAnZHJhZ292ZXInLCBAb25EcmFnT3Zlci5iaW5kKHRoaXMpXG4gICAgQHRyZWVWaWV3LmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lciAnZHJvcCcsIEBvbkRyb3AuYmluZCh0aGlzKVxuXG4gIG9uRHJhZ1N0YXJ0OiAoZSkgPT5cbiAgICByZXR1cm4gdW5sZXNzIEB0cmVlVmlldy5saXN0LmNvbnRhaW5zKGUudGFyZ2V0KVxuXG4gICAgQHByZXZEcm9wVGFyZ2V0SW5kZXggPSBudWxsXG4gICAgZS5kYXRhVHJhbnNmZXIuc2V0RGF0YSAnYXRvbS10cmVlLXZpZXctcm9vdC1ldmVudCcsICd0cnVlJ1xuICAgIHByb2plY3RSb290ID0gZS50YXJnZXQuY2xvc2VzdCgnLnByb2plY3Qtcm9vdCcpXG4gICAgZGlyZWN0b3J5ID0gcHJvamVjdFJvb3QuZGlyZWN0b3J5XG5cbiAgICBlLmRhdGFUcmFuc2Zlci5zZXREYXRhICdwcm9qZWN0LXJvb3QtaW5kZXgnLCBBcnJheS5mcm9tKHByb2plY3RSb290LnBhcmVudEVsZW1lbnQuY2hpbGRyZW4pLmluZGV4T2YocHJvamVjdFJvb3QpXG5cbiAgICByb290SW5kZXggPSAtMVxuICAgIChyb290SW5kZXggPSBpbmRleDsgYnJlYWspIGZvciByb290LCBpbmRleCBpbiBAdHJlZVZpZXcucm9vdHMgd2hlbiByb290LmRpcmVjdG9yeSBpcyBkaXJlY3RvcnlcblxuICAgIGUuZGF0YVRyYW5zZmVyLnNldERhdGEgJ2Zyb20tcm9vdC1pbmRleCcsIHJvb3RJbmRleFxuICAgIGUuZGF0YVRyYW5zZmVyLnNldERhdGEgJ2Zyb20tcm9vdC1wYXRoJywgZGlyZWN0b3J5LnBhdGhcbiAgICBlLmRhdGFUcmFuc2Zlci5zZXREYXRhICdmcm9tLXdpbmRvdy1pZCcsIEBnZXRXaW5kb3dJZCgpXG5cbiAgICBlLmRhdGFUcmFuc2Zlci5zZXREYXRhICd0ZXh0L3BsYWluJywgZGlyZWN0b3J5LnBhdGhcblxuICAgIGlmIHByb2Nlc3MucGxhdGZvcm0gaW4gWydkYXJ3aW4nLCAnbGludXgnXVxuICAgICAgcGF0aFVyaSA9IFwiZmlsZTovLyN7ZGlyZWN0b3J5LnBhdGh9XCIgdW5sZXNzIEB1cmlIYXNQcm90b2NvbChkaXJlY3RvcnkucGF0aClcbiAgICAgIGUuZGF0YVRyYW5zZmVyLnNldERhdGEgJ3RleHQvdXJpLWxpc3QnLCBwYXRoVXJpXG5cbiAgdXJpSGFzUHJvdG9jb2w6ICh1cmkpIC0+XG4gICAgdHJ5XG4gICAgICB1cmwucGFyc2UodXJpKS5wcm90b2NvbD9cbiAgICBjYXRjaCBlcnJvclxuICAgICAgZmFsc2VcblxuICBvbkRyYWdFbnRlcjogKGUpIC0+XG4gICAgcmV0dXJuIHVubGVzcyBAdHJlZVZpZXcubGlzdC5jb250YWlucyhlLnRhcmdldClcbiAgICByZXR1cm4gdW5sZXNzIEBpc0F0b21UcmVlVmlld0V2ZW50KGUpXG5cbiAgICBlLnN0b3BQcm9wYWdhdGlvbigpXG5cbiAgb25EcmFnTGVhdmU6IChlKSA9PlxuICAgIHJldHVybiB1bmxlc3MgQHRyZWVWaWV3Lmxpc3QuY29udGFpbnMoZS50YXJnZXQpXG4gICAgcmV0dXJuIHVubGVzcyBAaXNBdG9tVHJlZVZpZXdFdmVudChlKVxuXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuICAgIEByZW1vdmVQbGFjZWhvbGRlcigpIGlmIGUudGFyZ2V0IGlzIGUuY3VycmVudFRhcmdldFxuXG4gIG9uRHJhZ0VuZDogKGUpID0+XG4gICAgcmV0dXJuIHVubGVzcyBlLnRhcmdldC5tYXRjaGVzKCcucHJvamVjdC1yb290LWhlYWRlcicpXG4gICAgcmV0dXJuIHVubGVzcyBAaXNBdG9tVHJlZVZpZXdFdmVudChlKVxuXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuICAgIEBjbGVhckRyb3BUYXJnZXQoKVxuXG4gIG9uRHJhZ092ZXI6IChlKSA9PlxuICAgIHJldHVybiB1bmxlc3MgQHRyZWVWaWV3Lmxpc3QuY29udGFpbnMoZS50YXJnZXQpXG4gICAgcmV0dXJuIHVubGVzcyBAaXNBdG9tVHJlZVZpZXdFdmVudChlKVxuXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuXG4gICAgZW50cnkgPSBlLmN1cnJlbnRUYXJnZXRcblxuICAgIGlmIEB0cmVlVmlldy5yb290cy5sZW5ndGggaXMgMFxuICAgICAgQHRyZWVWaWV3Lmxpc3QuYXBwZW5kQ2hpbGQoQGdldFBsYWNlaG9sZGVyKCkpXG4gICAgICByZXR1cm5cblxuICAgIG5ld0Ryb3BUYXJnZXRJbmRleCA9IEBnZXREcm9wVGFyZ2V0SW5kZXgoZSlcbiAgICByZXR1cm4gdW5sZXNzIG5ld0Ryb3BUYXJnZXRJbmRleD9cbiAgICByZXR1cm4gaWYgQHByZXZEcm9wVGFyZ2V0SW5kZXggaXMgbmV3RHJvcFRhcmdldEluZGV4XG4gICAgQHByZXZEcm9wVGFyZ2V0SW5kZXggPSBuZXdEcm9wVGFyZ2V0SW5kZXhcblxuICAgIHByb2plY3RSb290cyA9IEB0cmVlVmlldy5yb290c1xuXG4gICAgaWYgbmV3RHJvcFRhcmdldEluZGV4IDwgcHJvamVjdFJvb3RzLmxlbmd0aFxuICAgICAgZWxlbWVudCA9IHByb2plY3RSb290c1tuZXdEcm9wVGFyZ2V0SW5kZXhdXG4gICAgICBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2lzLWRyb3AtdGFyZ2V0JylcbiAgICAgIGVsZW1lbnQucGFyZW50RWxlbWVudC5pbnNlcnRCZWZvcmUoQGdldFBsYWNlaG9sZGVyKCksIGVsZW1lbnQpXG4gICAgZWxzZVxuICAgICAgZWxlbWVudCA9IHByb2plY3RSb290c1tuZXdEcm9wVGFyZ2V0SW5kZXggLSAxXVxuICAgICAgZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkcm9wLXRhcmdldC1pcy1hZnRlcicpXG4gICAgICBlbGVtZW50LnBhcmVudEVsZW1lbnQuaW5zZXJ0QmVmb3JlKEBnZXRQbGFjZWhvbGRlcigpLCBlbGVtZW50Lm5leHRTaWJsaW5nKVxuXG4gIG9uRHJvcE9uT3RoZXJXaW5kb3c6IChlLCBmcm9tSXRlbUluZGV4KSA9PlxuICAgIHBhdGhzID0gYXRvbS5wcm9qZWN0LmdldFBhdGhzKClcbiAgICBwYXRocy5zcGxpY2UoZnJvbUl0ZW1JbmRleCwgMSlcbiAgICBhdG9tLnByb2plY3Quc2V0UGF0aHMocGF0aHMpXG5cbiAgICBAY2xlYXJEcm9wVGFyZ2V0KClcblxuICBjbGVhckRyb3BUYXJnZXQ6IC0+XG4gICAgZWxlbWVudCA9IEB0cmVlVmlldy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuaXMtZHJhZ2dpbmdcIilcbiAgICBlbGVtZW50Py5jbGFzc0xpc3QucmVtb3ZlKCdpcy1kcmFnZ2luZycpXG4gICAgZWxlbWVudD8udXBkYXRlVG9vbHRpcCgpXG4gICAgQHJlbW92ZVBsYWNlaG9sZGVyKClcblxuICBvbkRyb3A6IChlKSA9PlxuICAgIHJldHVybiB1bmxlc3MgQHRyZWVWaWV3Lmxpc3QuY29udGFpbnMoZS50YXJnZXQpXG4gICAgcmV0dXJuIHVubGVzcyBAaXNBdG9tVHJlZVZpZXdFdmVudChlKVxuXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuXG4gICAge2RhdGFUcmFuc2Zlcn0gPSBlXG5cbiAgICBmcm9tV2luZG93SWQgPSBwYXJzZUludChkYXRhVHJhbnNmZXIuZ2V0RGF0YSgnZnJvbS13aW5kb3ctaWQnKSlcbiAgICBmcm9tUm9vdFBhdGggID0gZGF0YVRyYW5zZmVyLmdldERhdGEoJ2Zyb20tcm9vdC1wYXRoJylcbiAgICBmcm9tSW5kZXggICAgID0gcGFyc2VJbnQoZGF0YVRyYW5zZmVyLmdldERhdGEoJ3Byb2plY3Qtcm9vdC1pbmRleCcpKVxuICAgIGZyb21Sb290SW5kZXggPSBwYXJzZUludChkYXRhVHJhbnNmZXIuZ2V0RGF0YSgnZnJvbS1yb290LWluZGV4JykpXG5cbiAgICB0b0luZGV4ID0gQGdldERyb3BUYXJnZXRJbmRleChlKVxuXG4gICAgQGNsZWFyRHJvcFRhcmdldCgpXG5cbiAgICBpZiBmcm9tV2luZG93SWQgaXMgQGdldFdpbmRvd0lkKClcbiAgICAgIHVubGVzcyBmcm9tSW5kZXggaXMgdG9JbmRleFxuICAgICAgICBwcm9qZWN0UGF0aHMgPSBhdG9tLnByb2plY3QuZ2V0UGF0aHMoKVxuICAgICAgICBwcm9qZWN0UGF0aHMuc3BsaWNlKGZyb21JbmRleCwgMSlcbiAgICAgICAgaWYgdG9JbmRleCA+IGZyb21JbmRleCB0aGVuIHRvSW5kZXggLT0gMVxuICAgICAgICBwcm9qZWN0UGF0aHMuc3BsaWNlKHRvSW5kZXgsIDAsIGZyb21Sb290UGF0aClcbiAgICAgICAgYXRvbS5wcm9qZWN0LnNldFBhdGhzKHByb2plY3RQYXRocylcbiAgICBlbHNlXG4gICAgICBwcm9qZWN0UGF0aHMgPSBhdG9tLnByb2plY3QuZ2V0UGF0aHMoKVxuICAgICAgcHJvamVjdFBhdGhzLnNwbGljZSh0b0luZGV4LCAwLCBmcm9tUm9vdFBhdGgpXG4gICAgICBhdG9tLnByb2plY3Quc2V0UGF0aHMocHJvamVjdFBhdGhzKVxuXG4gICAgICBpZiBub3QgaXNOYU4oZnJvbVdpbmRvd0lkKVxuICAgICAgICAjIExldCB0aGUgd2luZG93IHdoZXJlIHRoZSBkcmFnIHN0YXJ0ZWQga25vdyB0aGF0IHRoZSB0YWIgd2FzIGRyb3BwZWRcbiAgICAgICAgYnJvd3NlcldpbmRvdyA9IHJlbW90ZS5Ccm93c2VyV2luZG93LmZyb21JZChmcm9tV2luZG93SWQpXG4gICAgICAgIGJyb3dzZXJXaW5kb3c/LndlYkNvbnRlbnRzLnNlbmQoJ3RyZWUtdmlldzpwcm9qZWN0LWZvbGRlci1kcm9wcGVkJywgZnJvbUluZGV4KVxuXG4gIGdldERyb3BUYXJnZXRJbmRleDogKGUpIC0+XG4gICAgcmV0dXJuIGlmIEBpc1BsYWNlaG9sZGVyKGUudGFyZ2V0KVxuXG4gICAgcHJvamVjdFJvb3RzID0gQHRyZWVWaWV3LnJvb3RzXG4gICAgcHJvamVjdFJvb3QgPSBlLnRhcmdldC5jbG9zZXN0KCcucHJvamVjdC1yb290JylcbiAgICBwcm9qZWN0Um9vdCA9IHByb2plY3RSb290c1twcm9qZWN0Um9vdHMubGVuZ3RoIC0gMV0gdW5sZXNzIHByb2plY3RSb290XG5cbiAgICByZXR1cm4gMCB1bmxlc3MgcHJvamVjdFJvb3RcblxuICAgIHByb2plY3RSb290SW5kZXggPSBAdHJlZVZpZXcucm9vdHMuaW5kZXhPZihwcm9qZWN0Um9vdClcblxuICAgIGNlbnRlciA9IHByb2plY3RSb290LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcCArIHByb2plY3RSb290Lm9mZnNldEhlaWdodCAvIDJcblxuICAgIGlmIGUucGFnZVkgPCBjZW50ZXJcbiAgICAgIHByb2plY3RSb290SW5kZXhcbiAgICBlbHNlXG4gICAgICBwcm9qZWN0Um9vdEluZGV4ICsgMVxuXG4gIGNhbkRyYWdTdGFydDogKGUpIC0+XG4gICAgZS50YXJnZXQuY2xvc2VzdCgnLnByb2plY3Qtcm9vdC1oZWFkZXInKVxuXG4gIGlzRHJhZ2dpbmc6IChlKSAtPlxuICAgIGZvciBpdGVtIGluIGUuZGF0YVRyYW5zZmVyLml0ZW1zXG4gICAgICBpZiBpdGVtLnR5cGUgaXMgJ2Zyb20tcm9vdC1wYXRoJ1xuICAgICAgICByZXR1cm4gdHJ1ZVxuXG4gICAgcmV0dXJuIGZhbHNlXG5cbiAgaXNBdG9tVHJlZVZpZXdFdmVudDogKGUpIC0+XG4gICAgZm9yIGl0ZW0gaW4gZS5kYXRhVHJhbnNmZXIuaXRlbXNcbiAgICAgIGlmIGl0ZW0udHlwZSBpcyAnYXRvbS10cmVlLXZpZXctcm9vdC1ldmVudCdcbiAgICAgICAgcmV0dXJuIHRydWVcblxuICAgIHJldHVybiBmYWxzZVxuXG4gIGdldFBsYWNlaG9sZGVyOiAtPlxuICAgIHVubGVzcyBAcGxhY2Vob2xkZXJFbFxuICAgICAgQHBsYWNlaG9sZGVyRWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdsaScpXG4gICAgICBAcGxhY2Vob2xkZXJFbC5jbGFzc0xpc3QuYWRkKCdwbGFjZWhvbGRlcicpXG4gICAgQHBsYWNlaG9sZGVyRWxcblxuICByZW1vdmVQbGFjZWhvbGRlcjogLT5cbiAgICBAcGxhY2Vob2xkZXJFbD8ucmVtb3ZlKClcbiAgICBAcGxhY2Vob2xkZXJFbCA9IG51bGxcblxuICBpc1BsYWNlaG9sZGVyOiAoZWxlbWVudCkgLT5cbiAgICBlbGVtZW50LmNsYXNzTGlzdC5jb250YWlucygnLnBsYWNlaG9sZGVyJylcblxuICBnZXRXaW5kb3dJZDogLT5cbiAgICBAcHJvY2Vzc0lkID89IGF0b20uZ2V0Q3VycmVudFdpbmRvdygpLmlkXG4iXX0=
