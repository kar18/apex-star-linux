(function() {
  var BufferSearch, CompositeDisposable, Disposable, FindOptions, FindView, History, HistoryCycler, ProjectFindView, ReporterProxy, ResultsModel, ResultsPaneView, SelectNext, TextBuffer, getIconServices, metricsReporter, ref, ref1;

  ref = require('atom'), CompositeDisposable = ref.CompositeDisposable, Disposable = ref.Disposable, TextBuffer = ref.TextBuffer;

  SelectNext = require('./select-next');

  ref1 = require('./history'), History = ref1.History, HistoryCycler = ref1.HistoryCycler;

  FindOptions = require('./find-options');

  BufferSearch = require('./buffer-search');

  getIconServices = require('./get-icon-services');

  FindView = require('./find-view');

  ProjectFindView = require('./project-find-view');

  ResultsModel = require('./project/results-model').ResultsModel;

  ResultsPaneView = require('./project/results-pane');

  ReporterProxy = require('./reporter-proxy');

  metricsReporter = new ReporterProxy();

  module.exports = {
    activate: function(arg) {
      var findHistory, findOptions, handleEditorCancel, pathsHistory, ref2, replaceHistory, selectNextObjectForEditorElement, showPanel, togglePanel;
      ref2 = arg != null ? arg : {}, findOptions = ref2.findOptions, findHistory = ref2.findHistory, replaceHistory = ref2.replaceHistory, pathsHistory = ref2.pathsHistory;
      if (atom.config.get('find-and-replace.openProjectFindResultsInRightPane')) {
        atom.config.set('find-and-replace.projectSearchResultsPaneSplitDirection', 'right');
      }
      atom.config.unset('find-and-replace.openProjectFindResultsInRightPane');
      atom.workspace.addOpener(function(filePath) {
        if (filePath.indexOf(ResultsPaneView.URI) !== -1) {
          return new ResultsPaneView();
        }
      });
      this.subscriptions = new CompositeDisposable;
      this.currentItemSub = new Disposable;
      this.findHistory = new History(findHistory);
      this.replaceHistory = new History(replaceHistory);
      this.pathsHistory = new History(pathsHistory);
      this.findOptions = new FindOptions(findOptions);
      this.findModel = new BufferSearch(this.findOptions);
      this.resultsModel = new ResultsModel(this.findOptions, metricsReporter);
      this.subscriptions.add(atom.workspace.getCenter().observeActivePaneItem((function(_this) {
        return function(paneItem) {
          _this.subscriptions["delete"](_this.currentItemSub);
          _this.currentItemSub.dispose();
          if (atom.workspace.isTextEditor(paneItem)) {
            return _this.findModel.setEditor(paneItem);
          } else if ((paneItem != null ? paneItem.observeEmbeddedTextEditor : void 0) != null) {
            _this.currentItemSub = paneItem.observeEmbeddedTextEditor(function(editor) {
              if (atom.workspace.getCenter().getActivePaneItem() === paneItem) {
                return _this.findModel.setEditor(editor);
              }
            });
            return _this.subscriptions.add(_this.currentItemSub);
          } else if ((paneItem != null ? paneItem.getEmbeddedTextEditor : void 0) != null) {
            return _this.findModel.setEditor(paneItem.getEmbeddedTextEditor());
          } else {
            return _this.findModel.setEditor(null);
          }
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('.find-and-replace, .project-find', 'window:focus-next-pane', function() {
        return atom.views.getView(atom.workspace).focus();
      }));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'project-find:show', (function(_this) {
        return function() {
          _this.createViews();
          return showPanel(_this.projectFindPanel, _this.findPanel, function() {
            return _this.projectFindView.focusFindElement();
          });
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'project-find:toggle', (function(_this) {
        return function() {
          _this.createViews();
          return togglePanel(_this.projectFindPanel, _this.findPanel, function() {
            return _this.projectFindView.focusFindElement();
          });
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'project-find:show-in-current-directory', (function(_this) {
        return function(arg1) {
          var target;
          target = arg1.target;
          _this.createViews();
          _this.findPanel.hide();
          _this.projectFindPanel.show();
          _this.projectFindView.focusFindElement();
          return _this.projectFindView.findInCurrentlySelectedDirectory(target);
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'find-and-replace:use-selection-as-find-pattern', (function(_this) {
        return function() {
          var ref3, ref4;
          if (((ref3 = _this.projectFindPanel) != null ? ref3.isVisible() : void 0) || ((ref4 = _this.findPanel) != null ? ref4.isVisible() : void 0)) {
            return;
          }
          return _this.createViews();
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'find-and-replace:use-selection-as-replace-pattern', (function(_this) {
        return function() {
          var ref3, ref4;
          if (((ref3 = _this.projectFindPanel) != null ? ref3.isVisible() : void 0) || ((ref4 = _this.findPanel) != null ? ref4.isVisible() : void 0)) {
            return;
          }
          return _this.createViews();
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'find-and-replace:toggle', (function(_this) {
        return function() {
          _this.createViews();
          return togglePanel(_this.findPanel, _this.projectFindPanel, function() {
            return _this.findView.focusFindEditor();
          });
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'find-and-replace:show', (function(_this) {
        return function() {
          _this.createViews();
          return showPanel(_this.findPanel, _this.projectFindPanel, function() {
            return _this.findView.focusFindEditor();
          });
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'find-and-replace:show-replace', (function(_this) {
        return function() {
          _this.createViews();
          return showPanel(_this.findPanel, _this.projectFindPanel, function() {
            return _this.findView.focusReplaceEditor();
          });
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'find-and-replace:clear-history', (function(_this) {
        return function() {
          _this.findHistory.clear();
          return _this.replaceHistory.clear();
        };
      })(this)));
      handleEditorCancel = (function(_this) {
        return function(arg1) {
          var isMiniEditor, ref3, ref4, target;
          target = arg1.target;
          isMiniEditor = target.tagName === 'ATOM-TEXT-EDITOR' && target.hasAttribute('mini');
          if (!isMiniEditor) {
            if ((ref3 = _this.findPanel) != null) {
              ref3.hide();
            }
            return (ref4 = _this.projectFindPanel) != null ? ref4.hide() : void 0;
          }
        };
      })(this);
      this.subscriptions.add(atom.commands.add('atom-workspace', {
        'core:cancel': handleEditorCancel,
        'core:close': handleEditorCancel
      }));
      selectNextObjectForEditorElement = (function(_this) {
        return function(editorElement) {
          var editor, selectNext;
          if (_this.selectNextObjects == null) {
            _this.selectNextObjects = new WeakMap();
          }
          editor = editorElement.getModel();
          selectNext = _this.selectNextObjects.get(editor);
          if (selectNext == null) {
            selectNext = new SelectNext(editor);
            _this.selectNextObjects.set(editor, selectNext);
          }
          return selectNext;
        };
      })(this);
      showPanel = function(panelToShow, panelToHide, postShowAction) {
        panelToHide.hide();
        panelToShow.show();
        return typeof postShowAction === "function" ? postShowAction() : void 0;
      };
      togglePanel = function(panelToToggle, panelToHide, postToggleAction) {
        panelToHide.hide();
        if (panelToToggle.isVisible()) {
          return panelToToggle.hide();
        } else {
          panelToToggle.show();
          return typeof postToggleAction === "function" ? postToggleAction() : void 0;
        }
      };
      return this.subscriptions.add(atom.commands.add('.editor:not(.mini)', {
        'find-and-replace:select-next': function(event) {
          return selectNextObjectForEditorElement(this).findAndSelectNext();
        },
        'find-and-replace:select-all': function(event) {
          return selectNextObjectForEditorElement(this).findAndSelectAll();
        },
        'find-and-replace:select-undo': function(event) {
          return selectNextObjectForEditorElement(this).undoLastSelection();
        },
        'find-and-replace:select-skip': function(event) {
          return selectNextObjectForEditorElement(this).skipCurrentSelection();
        }
      }));
    },
    consumeMetricsReporter: function(service) {
      metricsReporter.setReporter(service);
      return new Disposable(function() {
        return metricsReporter.unsetReporter();
      });
    },
    consumeElementIcons: function(service) {
      getIconServices().setElementIcons(service);
      return new Disposable(function() {
        return getIconServices().resetElementIcons();
      });
    },
    consumeFileIcons: function(service) {
      getIconServices().setFileIcons(service);
      return new Disposable(function() {
        return getIconServices().resetFileIcons();
      });
    },
    toggleAutocompletions: function(value) {
      var disposable, ref2;
      if (this.findView == null) {
        return;
      }
      if (value) {
        this.autocompleteSubscriptions = new CompositeDisposable;
        disposable = typeof this.autocompleteWatchEditor === "function" ? this.autocompleteWatchEditor(this.findView.findEditor, ['default']) : void 0;
        if (disposable != null) {
          return this.autocompleteSubscriptions.add(disposable);
        }
      } else {
        return (ref2 = this.autocompleteSubscriptions) != null ? ref2.dispose() : void 0;
      }
    },
    consumeAutocompleteWatchEditor: function(watchEditor) {
      this.autocompleteWatchEditor = watchEditor;
      atom.config.observe('find-and-replace.autocompleteSearches', (function(_this) {
        return function(value) {
          return _this.toggleAutocompletions(value);
        };
      })(this));
      return new Disposable((function(_this) {
        return function() {
          var ref2;
          if ((ref2 = _this.autocompleteSubscriptions) != null) {
            ref2.dispose();
          }
          return _this.autocompleteWatchEditor = null;
        };
      })(this));
    },
    provideService: function() {
      return {
        resultsMarkerLayerForTextEditor: this.findModel.resultsMarkerLayerForTextEditor.bind(this.findModel)
      };
    },
    createViews: function() {
      var findBuffer, findHistoryCycler, options, pathsBuffer, pathsHistoryCycler, replaceBuffer, replaceHistoryCycler;
      if (this.findView != null) {
        return;
      }
      findBuffer = new TextBuffer;
      replaceBuffer = new TextBuffer;
      pathsBuffer = new TextBuffer;
      findHistoryCycler = new HistoryCycler(findBuffer, this.findHistory);
      replaceHistoryCycler = new HistoryCycler(replaceBuffer, this.replaceHistory);
      pathsHistoryCycler = new HistoryCycler(pathsBuffer, this.pathsHistory);
      options = {
        findBuffer: findBuffer,
        replaceBuffer: replaceBuffer,
        pathsBuffer: pathsBuffer,
        findHistoryCycler: findHistoryCycler,
        replaceHistoryCycler: replaceHistoryCycler,
        pathsHistoryCycler: pathsHistoryCycler
      };
      this.findView = new FindView(this.findModel, options);
      this.projectFindView = new ProjectFindView(this.resultsModel, options);
      this.findPanel = atom.workspace.addBottomPanel({
        item: this.findView,
        visible: false,
        className: 'tool-panel panel-bottom'
      });
      this.projectFindPanel = atom.workspace.addBottomPanel({
        item: this.projectFindView,
        visible: false,
        className: 'tool-panel panel-bottom'
      });
      this.findView.setPanel(this.findPanel);
      this.projectFindView.setPanel(this.projectFindPanel);
      ResultsPaneView.projectFindView = this.projectFindView;
      return this.toggleAutocompletions(atom.config.get('find-and-replace.autocompleteSearches'));
    },
    deactivate: function() {
      var ref2, ref3, ref4, ref5, ref6, ref7, ref8;
      if ((ref2 = this.findPanel) != null) {
        ref2.destroy();
      }
      this.findPanel = null;
      if ((ref3 = this.findView) != null) {
        ref3.destroy();
      }
      this.findView = null;
      if ((ref4 = this.findModel) != null) {
        ref4.destroy();
      }
      this.findModel = null;
      if ((ref5 = this.projectFindPanel) != null) {
        ref5.destroy();
      }
      this.projectFindPanel = null;
      if ((ref6 = this.projectFindView) != null) {
        ref6.destroy();
      }
      this.projectFindView = null;
      ResultsPaneView.model = null;
      if ((ref7 = this.autocompleteSubscriptions) != null) {
        ref7.dispose();
      }
      this.autocompleteManagerService = null;
      if ((ref8 = this.subscriptions) != null) {
        ref8.dispose();
      }
      return this.subscriptions = null;
    },
    serialize: function() {
      return {
        findOptions: this.findOptions.serialize(),
        findHistory: this.findHistory.serialize(),
        replaceHistory: this.replaceHistory.serialize(),
        pathsHistory: this.pathsHistory.serialize()
      };
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9maW5kLWFuZC1yZXBsYWNlL2xpYi9maW5kLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsTUFBZ0QsT0FBQSxDQUFRLE1BQVIsQ0FBaEQsRUFBQyw2Q0FBRCxFQUFzQiwyQkFBdEIsRUFBa0M7O0VBRWxDLFVBQUEsR0FBYSxPQUFBLENBQVEsZUFBUjs7RUFDYixPQUEyQixPQUFBLENBQVEsV0FBUixDQUEzQixFQUFDLHNCQUFELEVBQVU7O0VBQ1YsV0FBQSxHQUFjLE9BQUEsQ0FBUSxnQkFBUjs7RUFDZCxZQUFBLEdBQWUsT0FBQSxDQUFRLGlCQUFSOztFQUNmLGVBQUEsR0FBa0IsT0FBQSxDQUFRLHFCQUFSOztFQUNsQixRQUFBLEdBQVcsT0FBQSxDQUFRLGFBQVI7O0VBQ1gsZUFBQSxHQUFrQixPQUFBLENBQVEscUJBQVI7O0VBQ2pCLGVBQWdCLE9BQUEsQ0FBUSx5QkFBUjs7RUFDakIsZUFBQSxHQUFrQixPQUFBLENBQVEsd0JBQVI7O0VBQ2xCLGFBQUEsR0FBZ0IsT0FBQSxDQUFRLGtCQUFSOztFQUVoQixlQUFBLEdBQWtCLElBQUksYUFBSixDQUFBOztFQUVsQixNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsUUFBQSxFQUFVLFNBQUMsR0FBRDtBQUVSLFVBQUE7MkJBRlMsTUFBeUQsSUFBeEQsZ0NBQWEsZ0NBQWEsc0NBQWdCO01BRXBELElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLG9EQUFoQixDQUFIO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLHlEQUFoQixFQUEyRSxPQUEzRSxFQURGOztNQUVBLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBWixDQUFrQixvREFBbEI7TUFFQSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQWYsQ0FBeUIsU0FBQyxRQUFEO1FBQ3ZCLElBQXlCLFFBQVEsQ0FBQyxPQUFULENBQWlCLGVBQWUsQ0FBQyxHQUFqQyxDQUFBLEtBQTJDLENBQUMsQ0FBckU7aUJBQUEsSUFBSSxlQUFKLENBQUEsRUFBQTs7TUFEdUIsQ0FBekI7TUFHQSxJQUFDLENBQUEsYUFBRCxHQUFpQixJQUFJO01BQ3JCLElBQUMsQ0FBQSxjQUFELEdBQWtCLElBQUk7TUFDdEIsSUFBQyxDQUFBLFdBQUQsR0FBZSxJQUFJLE9BQUosQ0FBWSxXQUFaO01BQ2YsSUFBQyxDQUFBLGNBQUQsR0FBa0IsSUFBSSxPQUFKLENBQVksY0FBWjtNQUNsQixJQUFDLENBQUEsWUFBRCxHQUFnQixJQUFJLE9BQUosQ0FBWSxZQUFaO01BRWhCLElBQUMsQ0FBQSxXQUFELEdBQWUsSUFBSSxXQUFKLENBQWdCLFdBQWhCO01BQ2YsSUFBQyxDQUFBLFNBQUQsR0FBYSxJQUFJLFlBQUosQ0FBaUIsSUFBQyxDQUFBLFdBQWxCO01BQ2IsSUFBQyxDQUFBLFlBQUQsR0FBZ0IsSUFBSSxZQUFKLENBQWlCLElBQUMsQ0FBQSxXQUFsQixFQUErQixlQUEvQjtNQUVoQixJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFmLENBQUEsQ0FBMEIsQ0FBQyxxQkFBM0IsQ0FBaUQsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLFFBQUQ7VUFDbEUsS0FBQyxDQUFBLGFBQWEsRUFBQyxNQUFELEVBQWQsQ0FBc0IsS0FBQyxDQUFBLGNBQXZCO1VBQ0EsS0FBQyxDQUFBLGNBQWMsQ0FBQyxPQUFoQixDQUFBO1VBRUEsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQWYsQ0FBNEIsUUFBNUIsQ0FBSDttQkFDRSxLQUFDLENBQUEsU0FBUyxDQUFDLFNBQVgsQ0FBcUIsUUFBckIsRUFERjtXQUFBLE1BRUssSUFBRyx3RUFBSDtZQUNILEtBQUMsQ0FBQSxjQUFELEdBQWtCLFFBQVEsQ0FBQyx5QkFBVCxDQUFtQyxTQUFDLE1BQUQ7Y0FDbkQsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQWYsQ0FBQSxDQUEwQixDQUFDLGlCQUEzQixDQUFBLENBQUEsS0FBa0QsUUFBckQ7dUJBQ0UsS0FBQyxDQUFBLFNBQVMsQ0FBQyxTQUFYLENBQXFCLE1BQXJCLEVBREY7O1lBRG1ELENBQW5DO21CQUdsQixLQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsS0FBQyxDQUFBLGNBQXBCLEVBSkc7V0FBQSxNQUtBLElBQUcsb0VBQUg7bUJBQ0gsS0FBQyxDQUFBLFNBQVMsQ0FBQyxTQUFYLENBQXFCLFFBQVEsQ0FBQyxxQkFBVCxDQUFBLENBQXJCLEVBREc7V0FBQSxNQUFBO21CQUdILEtBQUMsQ0FBQSxTQUFTLENBQUMsU0FBWCxDQUFxQixJQUFyQixFQUhHOztRQVg2RDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBakQsQ0FBbkI7TUFnQkEsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixrQ0FBbEIsRUFBc0Qsd0JBQXRELEVBQWdGLFNBQUE7ZUFDakcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFYLENBQW1CLElBQUksQ0FBQyxTQUF4QixDQUFrQyxDQUFDLEtBQW5DLENBQUE7TUFEaUcsQ0FBaEYsQ0FBbkI7TUFHQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyxtQkFBcEMsRUFBeUQsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO1VBQzFFLEtBQUMsQ0FBQSxXQUFELENBQUE7aUJBQ0EsU0FBQSxDQUFVLEtBQUMsQ0FBQSxnQkFBWCxFQUE2QixLQUFDLENBQUEsU0FBOUIsRUFBeUMsU0FBQTttQkFBRyxLQUFDLENBQUEsZUFBZSxDQUFDLGdCQUFqQixDQUFBO1VBQUgsQ0FBekM7UUFGMEU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpELENBQW5CO01BSUEsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MscUJBQXBDLEVBQTJELENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtVQUM1RSxLQUFDLENBQUEsV0FBRCxDQUFBO2lCQUNBLFdBQUEsQ0FBWSxLQUFDLENBQUEsZ0JBQWIsRUFBK0IsS0FBQyxDQUFBLFNBQWhDLEVBQTJDLFNBQUE7bUJBQUcsS0FBQyxDQUFBLGVBQWUsQ0FBQyxnQkFBakIsQ0FBQTtVQUFILENBQTNDO1FBRjRFO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzRCxDQUFuQjtNQUlBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLHdDQUFwQyxFQUE4RSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsSUFBRDtBQUMvRixjQUFBO1VBRGlHLFNBQUQ7VUFDaEcsS0FBQyxDQUFBLFdBQUQsQ0FBQTtVQUNBLEtBQUMsQ0FBQSxTQUFTLENBQUMsSUFBWCxDQUFBO1VBQ0EsS0FBQyxDQUFBLGdCQUFnQixDQUFDLElBQWxCLENBQUE7VUFDQSxLQUFDLENBQUEsZUFBZSxDQUFDLGdCQUFqQixDQUFBO2lCQUNBLEtBQUMsQ0FBQSxlQUFlLENBQUMsZ0NBQWpCLENBQWtELE1BQWxEO1FBTCtGO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE5RSxDQUFuQjtNQU9BLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLGdEQUFwQyxFQUFzRixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7QUFDdkcsY0FBQTtVQUFBLG1EQUEyQixDQUFFLFNBQW5CLENBQUEsV0FBQSw0Q0FBNEMsQ0FBRSxTQUFaLENBQUEsV0FBNUM7QUFBQSxtQkFBQTs7aUJBQ0EsS0FBQyxDQUFBLFdBQUQsQ0FBQTtRQUZ1RztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBdEYsQ0FBbkI7TUFJQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyxtREFBcEMsRUFBeUYsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO0FBQzFHLGNBQUE7VUFBQSxtREFBMkIsQ0FBRSxTQUFuQixDQUFBLFdBQUEsNENBQTRDLENBQUUsU0FBWixDQUFBLFdBQTVDO0FBQUEsbUJBQUE7O2lCQUNBLEtBQUMsQ0FBQSxXQUFELENBQUE7UUFGMEc7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpGLENBQW5CO01BSUEsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MseUJBQXBDLEVBQStELENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtVQUNoRixLQUFDLENBQUEsV0FBRCxDQUFBO2lCQUNBLFdBQUEsQ0FBWSxLQUFDLENBQUEsU0FBYixFQUF3QixLQUFDLENBQUEsZ0JBQXpCLEVBQTJDLFNBQUE7bUJBQUcsS0FBQyxDQUFBLFFBQVEsQ0FBQyxlQUFWLENBQUE7VUFBSCxDQUEzQztRQUZnRjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBL0QsQ0FBbkI7TUFJQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyx1QkFBcEMsRUFBNkQsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO1VBQzlFLEtBQUMsQ0FBQSxXQUFELENBQUE7aUJBQ0EsU0FBQSxDQUFVLEtBQUMsQ0FBQSxTQUFYLEVBQXNCLEtBQUMsQ0FBQSxnQkFBdkIsRUFBeUMsU0FBQTttQkFBRyxLQUFDLENBQUEsUUFBUSxDQUFDLGVBQVYsQ0FBQTtVQUFILENBQXpDO1FBRjhFO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE3RCxDQUFuQjtNQUlBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLCtCQUFwQyxFQUFxRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDdEYsS0FBQyxDQUFBLFdBQUQsQ0FBQTtpQkFDQSxTQUFBLENBQVUsS0FBQyxDQUFBLFNBQVgsRUFBc0IsS0FBQyxDQUFBLGdCQUF2QixFQUF5QyxTQUFBO21CQUFHLEtBQUMsQ0FBQSxRQUFRLENBQUMsa0JBQVYsQ0FBQTtVQUFILENBQXpDO1FBRnNGO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFyRSxDQUFuQjtNQUlBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLGdDQUFwQyxFQUFzRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDdkYsS0FBQyxDQUFBLFdBQVcsQ0FBQyxLQUFiLENBQUE7aUJBQ0EsS0FBQyxDQUFBLGNBQWMsQ0FBQyxLQUFoQixDQUFBO1FBRnVGO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF0RSxDQUFuQjtNQUtBLGtCQUFBLEdBQXFCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxJQUFEO0FBQ25CLGNBQUE7VUFEcUIsU0FBRDtVQUNwQixZQUFBLEdBQWUsTUFBTSxDQUFDLE9BQVAsS0FBa0Isa0JBQWxCLElBQXlDLE1BQU0sQ0FBQyxZQUFQLENBQW9CLE1BQXBCO1VBQ3hELElBQUEsQ0FBTyxZQUFQOztrQkFDWSxDQUFFLElBQVosQ0FBQTs7aUVBQ2lCLENBQUUsSUFBbkIsQ0FBQSxXQUZGOztRQUZtQjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFNckIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFDakI7UUFBQSxhQUFBLEVBQWUsa0JBQWY7UUFDQSxZQUFBLEVBQWMsa0JBRGQ7T0FEaUIsQ0FBbkI7TUFJQSxnQ0FBQSxHQUFtQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsYUFBRDtBQUNqQyxjQUFBOztZQUFBLEtBQUMsQ0FBQSxvQkFBcUIsSUFBSSxPQUFKLENBQUE7O1VBQ3RCLE1BQUEsR0FBUyxhQUFhLENBQUMsUUFBZCxDQUFBO1VBQ1QsVUFBQSxHQUFhLEtBQUMsQ0FBQSxpQkFBaUIsQ0FBQyxHQUFuQixDQUF1QixNQUF2QjtVQUNiLElBQU8sa0JBQVA7WUFDRSxVQUFBLEdBQWEsSUFBSSxVQUFKLENBQWUsTUFBZjtZQUNiLEtBQUMsQ0FBQSxpQkFBaUIsQ0FBQyxHQUFuQixDQUF1QixNQUF2QixFQUErQixVQUEvQixFQUZGOztpQkFHQTtRQVBpQztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFTbkMsU0FBQSxHQUFZLFNBQUMsV0FBRCxFQUFjLFdBQWQsRUFBMkIsY0FBM0I7UUFDVixXQUFXLENBQUMsSUFBWixDQUFBO1FBQ0EsV0FBVyxDQUFDLElBQVosQ0FBQTtzREFDQTtNQUhVO01BS1osV0FBQSxHQUFjLFNBQUMsYUFBRCxFQUFnQixXQUFoQixFQUE2QixnQkFBN0I7UUFDWixXQUFXLENBQUMsSUFBWixDQUFBO1FBRUEsSUFBRyxhQUFhLENBQUMsU0FBZCxDQUFBLENBQUg7aUJBQ0UsYUFBYSxDQUFDLElBQWQsQ0FBQSxFQURGO1NBQUEsTUFBQTtVQUdFLGFBQWEsQ0FBQyxJQUFkLENBQUE7MERBQ0EsNEJBSkY7O01BSFk7YUFTZCxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLG9CQUFsQixFQUNqQjtRQUFBLDhCQUFBLEVBQWdDLFNBQUMsS0FBRDtpQkFDOUIsZ0NBQUEsQ0FBaUMsSUFBakMsQ0FBc0MsQ0FBQyxpQkFBdkMsQ0FBQTtRQUQ4QixDQUFoQztRQUVBLDZCQUFBLEVBQStCLFNBQUMsS0FBRDtpQkFDN0IsZ0NBQUEsQ0FBaUMsSUFBakMsQ0FBc0MsQ0FBQyxnQkFBdkMsQ0FBQTtRQUQ2QixDQUYvQjtRQUlBLDhCQUFBLEVBQWdDLFNBQUMsS0FBRDtpQkFDOUIsZ0NBQUEsQ0FBaUMsSUFBakMsQ0FBc0MsQ0FBQyxpQkFBdkMsQ0FBQTtRQUQ4QixDQUpoQztRQU1BLDhCQUFBLEVBQWdDLFNBQUMsS0FBRDtpQkFDOUIsZ0NBQUEsQ0FBaUMsSUFBakMsQ0FBc0MsQ0FBQyxvQkFBdkMsQ0FBQTtRQUQ4QixDQU5oQztPQURpQixDQUFuQjtJQS9HUSxDQUFWO0lBeUhBLHNCQUFBLEVBQXdCLFNBQUMsT0FBRDtNQUN0QixlQUFlLENBQUMsV0FBaEIsQ0FBNEIsT0FBNUI7YUFDQSxJQUFJLFVBQUosQ0FBZSxTQUFBO2VBQ2IsZUFBZSxDQUFDLGFBQWhCLENBQUE7TUFEYSxDQUFmO0lBRnNCLENBekh4QjtJQThIQSxtQkFBQSxFQUFxQixTQUFDLE9BQUQ7TUFDbkIsZUFBQSxDQUFBLENBQWlCLENBQUMsZUFBbEIsQ0FBa0MsT0FBbEM7YUFDQSxJQUFJLFVBQUosQ0FBZSxTQUFBO2VBQ2IsZUFBQSxDQUFBLENBQWlCLENBQUMsaUJBQWxCLENBQUE7TUFEYSxDQUFmO0lBRm1CLENBOUhyQjtJQW1JQSxnQkFBQSxFQUFrQixTQUFDLE9BQUQ7TUFDaEIsZUFBQSxDQUFBLENBQWlCLENBQUMsWUFBbEIsQ0FBK0IsT0FBL0I7YUFDQSxJQUFJLFVBQUosQ0FBZSxTQUFBO2VBQ2IsZUFBQSxDQUFBLENBQWlCLENBQUMsY0FBbEIsQ0FBQTtNQURhLENBQWY7SUFGZ0IsQ0FuSWxCO0lBd0lBLHFCQUFBLEVBQXVCLFNBQUMsS0FBRDtBQUNyQixVQUFBO01BQUEsSUFBTyxxQkFBUDtBQUNFLGVBREY7O01BRUEsSUFBRyxLQUFIO1FBQ0UsSUFBQyxDQUFBLHlCQUFELEdBQTZCLElBQUk7UUFDakMsVUFBQSx3REFBYSxJQUFDLENBQUEsd0JBQXlCLElBQUMsQ0FBQSxRQUFRLENBQUMsWUFBWSxDQUFDLFNBQUQ7UUFDN0QsSUFBRyxrQkFBSDtpQkFDRSxJQUFDLENBQUEseUJBQXlCLENBQUMsR0FBM0IsQ0FBK0IsVUFBL0IsRUFERjtTQUhGO09BQUEsTUFBQTtxRUFNNEIsQ0FBRSxPQUE1QixDQUFBLFdBTkY7O0lBSHFCLENBeEl2QjtJQW1KQSw4QkFBQSxFQUFnQyxTQUFDLFdBQUQ7TUFDOUIsSUFBQyxDQUFBLHVCQUFELEdBQTJCO01BQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBWixDQUNFLHVDQURGLEVBRUUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEtBQUQ7aUJBQVcsS0FBQyxDQUFBLHFCQUFELENBQXVCLEtBQXZCO1FBQVg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBRkY7YUFHQSxJQUFJLFVBQUosQ0FBZSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7QUFDYixjQUFBOztnQkFBMEIsQ0FBRSxPQUE1QixDQUFBOztpQkFDQSxLQUFDLENBQUEsdUJBQUQsR0FBMkI7UUFGZDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBZjtJQUw4QixDQW5KaEM7SUE0SkEsY0FBQSxFQUFnQixTQUFBO2FBQ2Q7UUFBQSwrQkFBQSxFQUFpQyxJQUFDLENBQUEsU0FBUyxDQUFDLCtCQUErQixDQUFDLElBQTNDLENBQWdELElBQUMsQ0FBQSxTQUFqRCxDQUFqQzs7SUFEYyxDQTVKaEI7SUErSkEsV0FBQSxFQUFhLFNBQUE7QUFDWCxVQUFBO01BQUEsSUFBVSxxQkFBVjtBQUFBLGVBQUE7O01BRUEsVUFBQSxHQUFhLElBQUk7TUFDakIsYUFBQSxHQUFnQixJQUFJO01BQ3BCLFdBQUEsR0FBYyxJQUFJO01BRWxCLGlCQUFBLEdBQW9CLElBQUksYUFBSixDQUFrQixVQUFsQixFQUE4QixJQUFDLENBQUEsV0FBL0I7TUFDcEIsb0JBQUEsR0FBdUIsSUFBSSxhQUFKLENBQWtCLGFBQWxCLEVBQWlDLElBQUMsQ0FBQSxjQUFsQztNQUN2QixrQkFBQSxHQUFxQixJQUFJLGFBQUosQ0FBa0IsV0FBbEIsRUFBK0IsSUFBQyxDQUFBLFlBQWhDO01BRXJCLE9BQUEsR0FBVTtRQUFDLFlBQUEsVUFBRDtRQUFhLGVBQUEsYUFBYjtRQUE0QixhQUFBLFdBQTVCO1FBQXlDLG1CQUFBLGlCQUF6QztRQUE0RCxzQkFBQSxvQkFBNUQ7UUFBa0Ysb0JBQUEsa0JBQWxGOztNQUVWLElBQUMsQ0FBQSxRQUFELEdBQVksSUFBSSxRQUFKLENBQWEsSUFBQyxDQUFBLFNBQWQsRUFBeUIsT0FBekI7TUFFWixJQUFDLENBQUEsZUFBRCxHQUFtQixJQUFJLGVBQUosQ0FBb0IsSUFBQyxDQUFBLFlBQXJCLEVBQW1DLE9BQW5DO01BRW5CLElBQUMsQ0FBQSxTQUFELEdBQWEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFmLENBQThCO1FBQUEsSUFBQSxFQUFNLElBQUMsQ0FBQSxRQUFQO1FBQWlCLE9BQUEsRUFBUyxLQUExQjtRQUFpQyxTQUFBLEVBQVcseUJBQTVDO09BQTlCO01BQ2IsSUFBQyxDQUFBLGdCQUFELEdBQW9CLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBZixDQUE4QjtRQUFBLElBQUEsRUFBTSxJQUFDLENBQUEsZUFBUDtRQUF3QixPQUFBLEVBQVMsS0FBakM7UUFBd0MsU0FBQSxFQUFXLHlCQUFuRDtPQUE5QjtNQUVwQixJQUFDLENBQUEsUUFBUSxDQUFDLFFBQVYsQ0FBbUIsSUFBQyxDQUFBLFNBQXBCO01BQ0EsSUFBQyxDQUFBLGVBQWUsQ0FBQyxRQUFqQixDQUEwQixJQUFDLENBQUEsZ0JBQTNCO01Ba0JBLGVBQWUsQ0FBQyxlQUFoQixHQUFrQyxJQUFDLENBQUE7YUFFbkMsSUFBQyxDQUFBLHFCQUFELENBQXVCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQix1Q0FBaEIsQ0FBdkI7SUF6Q1csQ0EvSmI7SUEwTUEsVUFBQSxFQUFZLFNBQUE7QUFDVixVQUFBOztZQUFVLENBQUUsT0FBWixDQUFBOztNQUNBLElBQUMsQ0FBQSxTQUFELEdBQWE7O1lBQ0osQ0FBRSxPQUFYLENBQUE7O01BQ0EsSUFBQyxDQUFBLFFBQUQsR0FBWTs7WUFDRixDQUFFLE9BQVosQ0FBQTs7TUFDQSxJQUFDLENBQUEsU0FBRCxHQUFhOztZQUVJLENBQUUsT0FBbkIsQ0FBQTs7TUFDQSxJQUFDLENBQUEsZ0JBQUQsR0FBb0I7O1lBQ0osQ0FBRSxPQUFsQixDQUFBOztNQUNBLElBQUMsQ0FBQSxlQUFELEdBQW1CO01BRW5CLGVBQWUsQ0FBQyxLQUFoQixHQUF3Qjs7WUFFRSxDQUFFLE9BQTVCLENBQUE7O01BQ0EsSUFBQyxDQUFBLDBCQUFELEdBQThCOztZQUNoQixDQUFFLE9BQWhCLENBQUE7O2FBQ0EsSUFBQyxDQUFBLGFBQUQsR0FBaUI7SUFsQlAsQ0ExTVo7SUE4TkEsU0FBQSxFQUFXLFNBQUE7YUFDVDtRQUFBLFdBQUEsRUFBYSxJQUFDLENBQUEsV0FBVyxDQUFDLFNBQWIsQ0FBQSxDQUFiO1FBQ0EsV0FBQSxFQUFhLElBQUMsQ0FBQSxXQUFXLENBQUMsU0FBYixDQUFBLENBRGI7UUFFQSxjQUFBLEVBQWdCLElBQUMsQ0FBQSxjQUFjLENBQUMsU0FBaEIsQ0FBQSxDQUZoQjtRQUdBLFlBQUEsRUFBYyxJQUFDLENBQUEsWUFBWSxDQUFDLFNBQWQsQ0FBQSxDQUhkOztJQURTLENBOU5YOztBQWhCRiIsInNvdXJjZXNDb250ZW50IjpbIntDb21wb3NpdGVEaXNwb3NhYmxlLCBEaXNwb3NhYmxlLCBUZXh0QnVmZmVyfSA9IHJlcXVpcmUgJ2F0b20nXG5cblNlbGVjdE5leHQgPSByZXF1aXJlICcuL3NlbGVjdC1uZXh0J1xue0hpc3RvcnksIEhpc3RvcnlDeWNsZXJ9ID0gcmVxdWlyZSAnLi9oaXN0b3J5J1xuRmluZE9wdGlvbnMgPSByZXF1aXJlICcuL2ZpbmQtb3B0aW9ucydcbkJ1ZmZlclNlYXJjaCA9IHJlcXVpcmUgJy4vYnVmZmVyLXNlYXJjaCdcbmdldEljb25TZXJ2aWNlcyA9IHJlcXVpcmUgJy4vZ2V0LWljb24tc2VydmljZXMnXG5GaW5kVmlldyA9IHJlcXVpcmUgJy4vZmluZC12aWV3J1xuUHJvamVjdEZpbmRWaWV3ID0gcmVxdWlyZSAnLi9wcm9qZWN0LWZpbmQtdmlldydcbntSZXN1bHRzTW9kZWx9ID0gcmVxdWlyZSAnLi9wcm9qZWN0L3Jlc3VsdHMtbW9kZWwnXG5SZXN1bHRzUGFuZVZpZXcgPSByZXF1aXJlICcuL3Byb2plY3QvcmVzdWx0cy1wYW5lJ1xuUmVwb3J0ZXJQcm94eSA9IHJlcXVpcmUgJy4vcmVwb3J0ZXItcHJveHknXG5cbm1ldHJpY3NSZXBvcnRlciA9IG5ldyBSZXBvcnRlclByb3h5KClcblxubW9kdWxlLmV4cG9ydHMgPVxuICBhY3RpdmF0ZTogKHtmaW5kT3B0aW9ucywgZmluZEhpc3RvcnksIHJlcGxhY2VIaXN0b3J5LCBwYXRoc0hpc3Rvcnl9PXt9KSAtPlxuICAgICMgQ29udmVydCBvbGQgY29uZmlnIHNldHRpbmcgZm9yIGJhY2t3YXJkIGNvbXBhdGliaWxpdHkuXG4gICAgaWYgYXRvbS5jb25maWcuZ2V0KCdmaW5kLWFuZC1yZXBsYWNlLm9wZW5Qcm9qZWN0RmluZFJlc3VsdHNJblJpZ2h0UGFuZScpXG4gICAgICBhdG9tLmNvbmZpZy5zZXQoJ2ZpbmQtYW5kLXJlcGxhY2UucHJvamVjdFNlYXJjaFJlc3VsdHNQYW5lU3BsaXREaXJlY3Rpb24nLCAncmlnaHQnKVxuICAgIGF0b20uY29uZmlnLnVuc2V0KCdmaW5kLWFuZC1yZXBsYWNlLm9wZW5Qcm9qZWN0RmluZFJlc3VsdHNJblJpZ2h0UGFuZScpXG5cbiAgICBhdG9tLndvcmtzcGFjZS5hZGRPcGVuZXIgKGZpbGVQYXRoKSAtPlxuICAgICAgbmV3IFJlc3VsdHNQYW5lVmlldygpIGlmIGZpbGVQYXRoLmluZGV4T2YoUmVzdWx0c1BhbmVWaWV3LlVSSSkgaXNudCAtMVxuXG4gICAgQHN1YnNjcmlwdGlvbnMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZVxuICAgIEBjdXJyZW50SXRlbVN1YiA9IG5ldyBEaXNwb3NhYmxlXG4gICAgQGZpbmRIaXN0b3J5ID0gbmV3IEhpc3RvcnkoZmluZEhpc3RvcnkpXG4gICAgQHJlcGxhY2VIaXN0b3J5ID0gbmV3IEhpc3RvcnkocmVwbGFjZUhpc3RvcnkpXG4gICAgQHBhdGhzSGlzdG9yeSA9IG5ldyBIaXN0b3J5KHBhdGhzSGlzdG9yeSlcblxuICAgIEBmaW5kT3B0aW9ucyA9IG5ldyBGaW5kT3B0aW9ucyhmaW5kT3B0aW9ucylcbiAgICBAZmluZE1vZGVsID0gbmV3IEJ1ZmZlclNlYXJjaChAZmluZE9wdGlvbnMpXG4gICAgQHJlc3VsdHNNb2RlbCA9IG5ldyBSZXN1bHRzTW9kZWwoQGZpbmRPcHRpb25zLCBtZXRyaWNzUmVwb3J0ZXIpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS53b3Jrc3BhY2UuZ2V0Q2VudGVyKCkub2JzZXJ2ZUFjdGl2ZVBhbmVJdGVtIChwYW5lSXRlbSkgPT5cbiAgICAgIEBzdWJzY3JpcHRpb25zLmRlbGV0ZSBAY3VycmVudEl0ZW1TdWJcbiAgICAgIEBjdXJyZW50SXRlbVN1Yi5kaXNwb3NlKClcblxuICAgICAgaWYgYXRvbS53b3Jrc3BhY2UuaXNUZXh0RWRpdG9yKHBhbmVJdGVtKVxuICAgICAgICBAZmluZE1vZGVsLnNldEVkaXRvcihwYW5lSXRlbSlcbiAgICAgIGVsc2UgaWYgcGFuZUl0ZW0/Lm9ic2VydmVFbWJlZGRlZFRleHRFZGl0b3I/XG4gICAgICAgIEBjdXJyZW50SXRlbVN1YiA9IHBhbmVJdGVtLm9ic2VydmVFbWJlZGRlZFRleHRFZGl0b3IgKGVkaXRvcikgPT5cbiAgICAgICAgICBpZiBhdG9tLndvcmtzcGFjZS5nZXRDZW50ZXIoKS5nZXRBY3RpdmVQYW5lSXRlbSgpIGlzIHBhbmVJdGVtXG4gICAgICAgICAgICBAZmluZE1vZGVsLnNldEVkaXRvcihlZGl0b3IpXG4gICAgICAgIEBzdWJzY3JpcHRpb25zLmFkZCBAY3VycmVudEl0ZW1TdWJcbiAgICAgIGVsc2UgaWYgcGFuZUl0ZW0/LmdldEVtYmVkZGVkVGV4dEVkaXRvcj9cbiAgICAgICAgQGZpbmRNb2RlbC5zZXRFZGl0b3IocGFuZUl0ZW0uZ2V0RW1iZWRkZWRUZXh0RWRpdG9yKCkpXG4gICAgICBlbHNlXG4gICAgICAgIEBmaW5kTW9kZWwuc2V0RWRpdG9yKG51bGwpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb21tYW5kcy5hZGQgJy5maW5kLWFuZC1yZXBsYWNlLCAucHJvamVjdC1maW5kJywgJ3dpbmRvdzpmb2N1cy1uZXh0LXBhbmUnLCAtPlxuICAgICAgYXRvbS52aWV3cy5nZXRWaWV3KGF0b20ud29ya3NwYWNlKS5mb2N1cygpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3Byb2plY3QtZmluZDpzaG93JywgPT5cbiAgICAgIEBjcmVhdGVWaWV3cygpXG4gICAgICBzaG93UGFuZWwgQHByb2plY3RGaW5kUGFuZWwsIEBmaW5kUGFuZWwsID0+IEBwcm9qZWN0RmluZFZpZXcuZm9jdXNGaW5kRWxlbWVudCgpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3Byb2plY3QtZmluZDp0b2dnbGUnLCA9PlxuICAgICAgQGNyZWF0ZVZpZXdzKClcbiAgICAgIHRvZ2dsZVBhbmVsIEBwcm9qZWN0RmluZFBhbmVsLCBAZmluZFBhbmVsLCA9PiBAcHJvamVjdEZpbmRWaWV3LmZvY3VzRmluZEVsZW1lbnQoKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICdwcm9qZWN0LWZpbmQ6c2hvdy1pbi1jdXJyZW50LWRpcmVjdG9yeScsICh7dGFyZ2V0fSkgPT5cbiAgICAgIEBjcmVhdGVWaWV3cygpXG4gICAgICBAZmluZFBhbmVsLmhpZGUoKVxuICAgICAgQHByb2plY3RGaW5kUGFuZWwuc2hvdygpXG4gICAgICBAcHJvamVjdEZpbmRWaWV3LmZvY3VzRmluZEVsZW1lbnQoKVxuICAgICAgQHByb2plY3RGaW5kVmlldy5maW5kSW5DdXJyZW50bHlTZWxlY3RlZERpcmVjdG9yeSh0YXJnZXQpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ2ZpbmQtYW5kLXJlcGxhY2U6dXNlLXNlbGVjdGlvbi1hcy1maW5kLXBhdHRlcm4nLCA9PlxuICAgICAgcmV0dXJuIGlmIEBwcm9qZWN0RmluZFBhbmVsPy5pc1Zpc2libGUoKSBvciBAZmluZFBhbmVsPy5pc1Zpc2libGUoKVxuICAgICAgQGNyZWF0ZVZpZXdzKClcblxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAnZmluZC1hbmQtcmVwbGFjZTp1c2Utc2VsZWN0aW9uLWFzLXJlcGxhY2UtcGF0dGVybicsID0+XG4gICAgICByZXR1cm4gaWYgQHByb2plY3RGaW5kUGFuZWw/LmlzVmlzaWJsZSgpIG9yIEBmaW5kUGFuZWw/LmlzVmlzaWJsZSgpXG4gICAgICBAY3JlYXRlVmlld3MoKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICdmaW5kLWFuZC1yZXBsYWNlOnRvZ2dsZScsID0+XG4gICAgICBAY3JlYXRlVmlld3MoKVxuICAgICAgdG9nZ2xlUGFuZWwgQGZpbmRQYW5lbCwgQHByb2plY3RGaW5kUGFuZWwsID0+IEBmaW5kVmlldy5mb2N1c0ZpbmRFZGl0b3IoKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICdmaW5kLWFuZC1yZXBsYWNlOnNob3cnLCA9PlxuICAgICAgQGNyZWF0ZVZpZXdzKClcbiAgICAgIHNob3dQYW5lbCBAZmluZFBhbmVsLCBAcHJvamVjdEZpbmRQYW5lbCwgPT4gQGZpbmRWaWV3LmZvY3VzRmluZEVkaXRvcigpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ2ZpbmQtYW5kLXJlcGxhY2U6c2hvdy1yZXBsYWNlJywgPT5cbiAgICAgIEBjcmVhdGVWaWV3cygpXG4gICAgICBzaG93UGFuZWwgQGZpbmRQYW5lbCwgQHByb2plY3RGaW5kUGFuZWwsID0+IEBmaW5kVmlldy5mb2N1c1JlcGxhY2VFZGl0b3IoKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICdmaW5kLWFuZC1yZXBsYWNlOmNsZWFyLWhpc3RvcnknLCA9PlxuICAgICAgQGZpbmRIaXN0b3J5LmNsZWFyKClcbiAgICAgIEByZXBsYWNlSGlzdG9yeS5jbGVhcigpXG5cbiAgICAjIEhhbmRsaW5nIGNhbmNlbCBpbiB0aGUgd29ya3NwYWNlICsgY29kZSBlZGl0b3JzXG4gICAgaGFuZGxlRWRpdG9yQ2FuY2VsID0gKHt0YXJnZXR9KSA9PlxuICAgICAgaXNNaW5pRWRpdG9yID0gdGFyZ2V0LnRhZ05hbWUgaXMgJ0FUT00tVEVYVC1FRElUT1InIGFuZCB0YXJnZXQuaGFzQXR0cmlidXRlKCdtaW5pJylcbiAgICAgIHVubGVzcyBpc01pbmlFZGl0b3JcbiAgICAgICAgQGZpbmRQYW5lbD8uaGlkZSgpXG4gICAgICAgIEBwcm9qZWN0RmluZFBhbmVsPy5oaWRlKClcblxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLFxuICAgICAgJ2NvcmU6Y2FuY2VsJzogaGFuZGxlRWRpdG9yQ2FuY2VsXG4gICAgICAnY29yZTpjbG9zZSc6IGhhbmRsZUVkaXRvckNhbmNlbFxuXG4gICAgc2VsZWN0TmV4dE9iamVjdEZvckVkaXRvckVsZW1lbnQgPSAoZWRpdG9yRWxlbWVudCkgPT5cbiAgICAgIEBzZWxlY3ROZXh0T2JqZWN0cyA/PSBuZXcgV2Vha01hcCgpXG4gICAgICBlZGl0b3IgPSBlZGl0b3JFbGVtZW50LmdldE1vZGVsKClcbiAgICAgIHNlbGVjdE5leHQgPSBAc2VsZWN0TmV4dE9iamVjdHMuZ2V0KGVkaXRvcilcbiAgICAgIHVubGVzcyBzZWxlY3ROZXh0P1xuICAgICAgICBzZWxlY3ROZXh0ID0gbmV3IFNlbGVjdE5leHQoZWRpdG9yKVxuICAgICAgICBAc2VsZWN0TmV4dE9iamVjdHMuc2V0KGVkaXRvciwgc2VsZWN0TmV4dClcbiAgICAgIHNlbGVjdE5leHRcblxuICAgIHNob3dQYW5lbCA9IChwYW5lbFRvU2hvdywgcGFuZWxUb0hpZGUsIHBvc3RTaG93QWN0aW9uKSAtPlxuICAgICAgcGFuZWxUb0hpZGUuaGlkZSgpXG4gICAgICBwYW5lbFRvU2hvdy5zaG93KClcbiAgICAgIHBvc3RTaG93QWN0aW9uPygpXG5cbiAgICB0b2dnbGVQYW5lbCA9IChwYW5lbFRvVG9nZ2xlLCBwYW5lbFRvSGlkZSwgcG9zdFRvZ2dsZUFjdGlvbikgLT5cbiAgICAgIHBhbmVsVG9IaWRlLmhpZGUoKVxuXG4gICAgICBpZiBwYW5lbFRvVG9nZ2xlLmlzVmlzaWJsZSgpXG4gICAgICAgIHBhbmVsVG9Ub2dnbGUuaGlkZSgpXG4gICAgICBlbHNlXG4gICAgICAgIHBhbmVsVG9Ub2dnbGUuc2hvdygpXG4gICAgICAgIHBvc3RUb2dnbGVBY3Rpb24/KClcblxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCAnLmVkaXRvcjpub3QoLm1pbmkpJyxcbiAgICAgICdmaW5kLWFuZC1yZXBsYWNlOnNlbGVjdC1uZXh0JzogKGV2ZW50KSAtPlxuICAgICAgICBzZWxlY3ROZXh0T2JqZWN0Rm9yRWRpdG9yRWxlbWVudCh0aGlzKS5maW5kQW5kU2VsZWN0TmV4dCgpXG4gICAgICAnZmluZC1hbmQtcmVwbGFjZTpzZWxlY3QtYWxsJzogKGV2ZW50KSAtPlxuICAgICAgICBzZWxlY3ROZXh0T2JqZWN0Rm9yRWRpdG9yRWxlbWVudCh0aGlzKS5maW5kQW5kU2VsZWN0QWxsKClcbiAgICAgICdmaW5kLWFuZC1yZXBsYWNlOnNlbGVjdC11bmRvJzogKGV2ZW50KSAtPlxuICAgICAgICBzZWxlY3ROZXh0T2JqZWN0Rm9yRWRpdG9yRWxlbWVudCh0aGlzKS51bmRvTGFzdFNlbGVjdGlvbigpXG4gICAgICAnZmluZC1hbmQtcmVwbGFjZTpzZWxlY3Qtc2tpcCc6IChldmVudCkgLT5cbiAgICAgICAgc2VsZWN0TmV4dE9iamVjdEZvckVkaXRvckVsZW1lbnQodGhpcykuc2tpcEN1cnJlbnRTZWxlY3Rpb24oKVxuXG4gIGNvbnN1bWVNZXRyaWNzUmVwb3J0ZXI6IChzZXJ2aWNlKSAtPlxuICAgIG1ldHJpY3NSZXBvcnRlci5zZXRSZXBvcnRlcihzZXJ2aWNlKVxuICAgIG5ldyBEaXNwb3NhYmxlIC0+XG4gICAgICBtZXRyaWNzUmVwb3J0ZXIudW5zZXRSZXBvcnRlcigpXG5cbiAgY29uc3VtZUVsZW1lbnRJY29uczogKHNlcnZpY2UpIC0+XG4gICAgZ2V0SWNvblNlcnZpY2VzKCkuc2V0RWxlbWVudEljb25zIHNlcnZpY2VcbiAgICBuZXcgRGlzcG9zYWJsZSAtPlxuICAgICAgZ2V0SWNvblNlcnZpY2VzKCkucmVzZXRFbGVtZW50SWNvbnMoKVxuXG4gIGNvbnN1bWVGaWxlSWNvbnM6IChzZXJ2aWNlKSAtPlxuICAgIGdldEljb25TZXJ2aWNlcygpLnNldEZpbGVJY29ucyBzZXJ2aWNlXG4gICAgbmV3IERpc3Bvc2FibGUgLT5cbiAgICAgIGdldEljb25TZXJ2aWNlcygpLnJlc2V0RmlsZUljb25zKClcblxuICB0b2dnbGVBdXRvY29tcGxldGlvbnM6ICh2YWx1ZSkgLT5cbiAgICBpZiBub3QgQGZpbmRWaWV3P1xuICAgICAgcmV0dXJuXG4gICAgaWYgdmFsdWVcbiAgICAgIEBhdXRvY29tcGxldGVTdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcbiAgICAgIGRpc3Bvc2FibGUgPSBAYXV0b2NvbXBsZXRlV2F0Y2hFZGl0b3I/KEBmaW5kVmlldy5maW5kRWRpdG9yLCBbJ2RlZmF1bHQnXSlcbiAgICAgIGlmIGRpc3Bvc2FibGU/XG4gICAgICAgIEBhdXRvY29tcGxldGVTdWJzY3JpcHRpb25zLmFkZChkaXNwb3NhYmxlKVxuICAgIGVsc2VcbiAgICAgIEBhdXRvY29tcGxldGVTdWJzY3JpcHRpb25zPy5kaXNwb3NlKClcblxuICBjb25zdW1lQXV0b2NvbXBsZXRlV2F0Y2hFZGl0b3I6ICh3YXRjaEVkaXRvcikgLT5cbiAgICBAYXV0b2NvbXBsZXRlV2F0Y2hFZGl0b3IgPSB3YXRjaEVkaXRvclxuICAgIGF0b20uY29uZmlnLm9ic2VydmUoXG4gICAgICAnZmluZC1hbmQtcmVwbGFjZS5hdXRvY29tcGxldGVTZWFyY2hlcycsXG4gICAgICAodmFsdWUpID0+IEB0b2dnbGVBdXRvY29tcGxldGlvbnModmFsdWUpKVxuICAgIG5ldyBEaXNwb3NhYmxlID0+XG4gICAgICBAYXV0b2NvbXBsZXRlU3Vic2NyaXB0aW9ucz8uZGlzcG9zZSgpXG4gICAgICBAYXV0b2NvbXBsZXRlV2F0Y2hFZGl0b3IgPSBudWxsXG5cbiAgcHJvdmlkZVNlcnZpY2U6IC0+XG4gICAgcmVzdWx0c01hcmtlckxheWVyRm9yVGV4dEVkaXRvcjogQGZpbmRNb2RlbC5yZXN1bHRzTWFya2VyTGF5ZXJGb3JUZXh0RWRpdG9yLmJpbmQoQGZpbmRNb2RlbClcblxuICBjcmVhdGVWaWV3czogLT5cbiAgICByZXR1cm4gaWYgQGZpbmRWaWV3P1xuXG4gICAgZmluZEJ1ZmZlciA9IG5ldyBUZXh0QnVmZmVyXG4gICAgcmVwbGFjZUJ1ZmZlciA9IG5ldyBUZXh0QnVmZmVyXG4gICAgcGF0aHNCdWZmZXIgPSBuZXcgVGV4dEJ1ZmZlclxuXG4gICAgZmluZEhpc3RvcnlDeWNsZXIgPSBuZXcgSGlzdG9yeUN5Y2xlcihmaW5kQnVmZmVyLCBAZmluZEhpc3RvcnkpXG4gICAgcmVwbGFjZUhpc3RvcnlDeWNsZXIgPSBuZXcgSGlzdG9yeUN5Y2xlcihyZXBsYWNlQnVmZmVyLCBAcmVwbGFjZUhpc3RvcnkpXG4gICAgcGF0aHNIaXN0b3J5Q3ljbGVyID0gbmV3IEhpc3RvcnlDeWNsZXIocGF0aHNCdWZmZXIsIEBwYXRoc0hpc3RvcnkpXG5cbiAgICBvcHRpb25zID0ge2ZpbmRCdWZmZXIsIHJlcGxhY2VCdWZmZXIsIHBhdGhzQnVmZmVyLCBmaW5kSGlzdG9yeUN5Y2xlciwgcmVwbGFjZUhpc3RvcnlDeWNsZXIsIHBhdGhzSGlzdG9yeUN5Y2xlcn1cblxuICAgIEBmaW5kVmlldyA9IG5ldyBGaW5kVmlldyhAZmluZE1vZGVsLCBvcHRpb25zKVxuXG4gICAgQHByb2plY3RGaW5kVmlldyA9IG5ldyBQcm9qZWN0RmluZFZpZXcoQHJlc3VsdHNNb2RlbCwgb3B0aW9ucylcblxuICAgIEBmaW5kUGFuZWwgPSBhdG9tLndvcmtzcGFjZS5hZGRCb3R0b21QYW5lbChpdGVtOiBAZmluZFZpZXcsIHZpc2libGU6IGZhbHNlLCBjbGFzc05hbWU6ICd0b29sLXBhbmVsIHBhbmVsLWJvdHRvbScpXG4gICAgQHByb2plY3RGaW5kUGFuZWwgPSBhdG9tLndvcmtzcGFjZS5hZGRCb3R0b21QYW5lbChpdGVtOiBAcHJvamVjdEZpbmRWaWV3LCB2aXNpYmxlOiBmYWxzZSwgY2xhc3NOYW1lOiAndG9vbC1wYW5lbCBwYW5lbC1ib3R0b20nKVxuXG4gICAgQGZpbmRWaWV3LnNldFBhbmVsKEBmaW5kUGFuZWwpXG4gICAgQHByb2plY3RGaW5kVmlldy5zZXRQYW5lbChAcHJvamVjdEZpbmRQYW5lbClcblxuICAgICMgSEFDSzogU29vb28sIHdlIG5lZWQgdG8gZ2V0IHRoZSBtb2RlbCB0byB0aGUgcGFuZSB2aWV3IHdoZW5ldmVyIGl0IGlzXG4gICAgIyBjcmVhdGVkLiBDcmVhdGlvbiBjb3VsZCBjb21lIGZyb20gdGhlIG9wZW5lciBiZWxvdywgb3IsIG1vcmUgcHJvYmxlbWF0aWMsXG4gICAgIyBmcm9tIGEgZGVzZXJpYWxpemUgY2FsbCB3aGVuIHNwbGl0dGluZyBwYW5lcy4gRm9yIG5vdywgYWxsIHBhbmUgdmlld3Mgd2lsbFxuICAgICMgdXNlIHRoaXMgc2FtZSBtb2RlbC4gVGhpcyBuZWVkcyB0byBiZSBpbXByb3ZlZCEgSSBkb250IGtub3cgdGhlIGJlc3Qgd2F5XG4gICAgIyB0byBkZWFsIHdpdGggdGhpczpcbiAgICAjIDEuIEhvdyBzaG91bGQgc2VyaWFsaXphdGlvbiB3b3JrIGluIHRoZSBjYXNlIG9mIGEgc2hhcmVkIG1vZGVsLlxuICAgICMgMi4gT3IgbWF5YmUgd2UgY3JlYXRlIHRoZSBtb2RlbCBlYWNoIHRpbWUgYSBuZXcgcGFuZSBpcyBjcmVhdGVkPyBUaGVuXG4gICAgIyAgICBQcm9qZWN0RmluZFZpZXcgbmVlZHMgdG8ga25vdyBhYm91dCBlYWNoIG1vZGVsIHNvIGl0IGNhbiBpbnZva2UgYSBzZWFyY2guXG4gICAgIyAgICBBbmQgb24gZWFjaCBuZXcgbW9kZWwsIGl0IHdpbGwgcnVuIHRoZSBzZWFyY2ggYWdhaW4uXG4gICAgI1xuICAgICMgU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9hdG9tL2ZpbmQtYW5kLXJlcGxhY2UvaXNzdWVzLzYzXG4gICAgI1Jlc3VsdHNQYW5lVmlldy5tb2RlbCA9IEByZXN1bHRzTW9kZWxcbiAgICAjIFRoaXMgbWFrZXMgcHJvamVjdEZpbmRWaWV3IGFjY2VzaWJsZSBpbiBSZXN1bHRzUGFuZVZpZXcgc28gdGhhdCByZXN1bHRzTW9kZWxcbiAgICAjIGNhbiBiZSBwcm9wZXJseSBzZXQgZm9yIFJlc3VsdHNQYW5lVmlldyBpbnN0YW5jZXMgYW5kIFByb2plY3RGaW5kVmlldyBpbnN0YW5jZVxuICAgICMgYXMgZGlmZmVyZW50IHBhbmUgdmlld3MgZG9uJ3QgbmVjZXNzYXJpbHkgdXNlIHNhbWUgbW9kZWxzIGFueW1vcmVcbiAgICAjIGJ1dCBtb3N0IHJlY2VudCBwYW5lIHZpZXcgYW5kIHByb2plY3RGaW5kVmlldyBkb1xuICAgIFJlc3VsdHNQYW5lVmlldy5wcm9qZWN0RmluZFZpZXcgPSBAcHJvamVjdEZpbmRWaWV3XG5cbiAgICBAdG9nZ2xlQXV0b2NvbXBsZXRpb25zIGF0b20uY29uZmlnLmdldCgnZmluZC1hbmQtcmVwbGFjZS5hdXRvY29tcGxldGVTZWFyY2hlcycpXG5cbiAgZGVhY3RpdmF0ZTogLT5cbiAgICBAZmluZFBhbmVsPy5kZXN0cm95KClcbiAgICBAZmluZFBhbmVsID0gbnVsbFxuICAgIEBmaW5kVmlldz8uZGVzdHJveSgpXG4gICAgQGZpbmRWaWV3ID0gbnVsbFxuICAgIEBmaW5kTW9kZWw/LmRlc3Ryb3koKVxuICAgIEBmaW5kTW9kZWwgPSBudWxsXG5cbiAgICBAcHJvamVjdEZpbmRQYW5lbD8uZGVzdHJveSgpXG4gICAgQHByb2plY3RGaW5kUGFuZWwgPSBudWxsXG4gICAgQHByb2plY3RGaW5kVmlldz8uZGVzdHJveSgpXG4gICAgQHByb2plY3RGaW5kVmlldyA9IG51bGxcblxuICAgIFJlc3VsdHNQYW5lVmlldy5tb2RlbCA9IG51bGxcblxuICAgIEBhdXRvY29tcGxldGVTdWJzY3JpcHRpb25zPy5kaXNwb3NlKClcbiAgICBAYXV0b2NvbXBsZXRlTWFuYWdlclNlcnZpY2UgPSBudWxsXG4gICAgQHN1YnNjcmlwdGlvbnM/LmRpc3Bvc2UoKVxuICAgIEBzdWJzY3JpcHRpb25zID0gbnVsbFxuXG4gIHNlcmlhbGl6ZTogLT5cbiAgICBmaW5kT3B0aW9uczogQGZpbmRPcHRpb25zLnNlcmlhbGl6ZSgpXG4gICAgZmluZEhpc3Rvcnk6IEBmaW5kSGlzdG9yeS5zZXJpYWxpemUoKVxuICAgIHJlcGxhY2VIaXN0b3J5OiBAcmVwbGFjZUhpc3Rvcnkuc2VyaWFsaXplKClcbiAgICBwYXRoc0hpc3Rvcnk6IEBwYXRoc0hpc3Rvcnkuc2VyaWFsaXplKClcbiJdfQ==
