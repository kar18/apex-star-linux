(function() {
  var Dialog, MoveDialog, fs, path, repoForPath,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  path = require('path');

  fs = require('fs-plus');

  Dialog = require('./dialog');

  repoForPath = require("./helpers").repoForPath;

  module.exports = MoveDialog = (function(superClass) {
    extend(MoveDialog, superClass);

    function MoveDialog(initialPath, arg) {
      var prompt;
      this.initialPath = initialPath;
      this.willMove = arg.willMove, this.onMove = arg.onMove, this.onMoveFailed = arg.onMoveFailed;
      if (fs.isDirectorySync(this.initialPath)) {
        prompt = 'Enter the new path for the directory.';
      } else {
        prompt = 'Enter the new path for the file.';
      }
      MoveDialog.__super__.constructor.call(this, {
        prompt: prompt,
        initialPath: atom.project.relativize(this.initialPath),
        select: true,
        iconClass: 'icon-arrow-right'
      });
    }

    MoveDialog.prototype.onConfirm = function(newPath) {
      var directoryPath, error, repo, rootPath;
      newPath = newPath.replace(/\s+$/, '');
      if (!path.isAbsolute(newPath)) {
        rootPath = atom.project.relativizePath(this.initialPath)[0];
        newPath = path.join(rootPath, newPath);
        if (!newPath) {
          return;
        }
      }
      if (this.initialPath === newPath) {
        this.close();
        return;
      }
      if (!this.isNewPathValid(newPath)) {
        this.showError("'" + newPath + "' already exists.");
        return;
      }
      directoryPath = path.dirname(newPath);
      try {
        if (typeof this.willMove === "function") {
          this.willMove({
            initialPath: this.initialPath,
            newPath: newPath
          });
        }
        if (!fs.existsSync(directoryPath)) {
          fs.makeTreeSync(directoryPath);
        }
        fs.moveSync(this.initialPath, newPath);
        if (typeof this.onMove === "function") {
          this.onMove({
            initialPath: this.initialPath,
            newPath: newPath
          });
        }
        if (repo = repoForPath(newPath)) {
          repo.getPathStatus(this.initialPath);
          repo.getPathStatus(newPath);
        }
        return this.close();
      } catch (error1) {
        error = error1;
        this.showError(error.message + ".");
        return typeof this.onMoveFailed === "function" ? this.onMoveFailed({
          initialPath: this.initialPath,
          newPath: newPath
        }) : void 0;
      }
    };

    MoveDialog.prototype.isNewPathValid = function(newPath) {
      var newStat, oldStat;
      try {
        oldStat = fs.statSync(this.initialPath);
        newStat = fs.statSync(newPath);
        return this.initialPath.toLowerCase() === newPath.toLowerCase() && oldStat.dev === newStat.dev && oldStat.ino === newStat.ino;
      } catch (error1) {
        return true;
      }
    };

    return MoveDialog;

  })(Dialog);

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90cmVlLXZpZXcvbGliL21vdmUtZGlhbG9nLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUEseUNBQUE7SUFBQTs7O0VBQUEsSUFBQSxHQUFPLE9BQUEsQ0FBUSxNQUFSOztFQUNQLEVBQUEsR0FBSyxPQUFBLENBQVEsU0FBUjs7RUFDTCxNQUFBLEdBQVMsT0FBQSxDQUFRLFVBQVI7O0VBQ1IsY0FBZSxPQUFBLENBQVEsV0FBUjs7RUFFaEIsTUFBTSxDQUFDLE9BQVAsR0FDTTs7O0lBQ1Msb0JBQUMsV0FBRCxFQUFlLEdBQWY7QUFDWCxVQUFBO01BRFksSUFBQyxDQUFBLGNBQUQ7TUFBZSxJQUFDLENBQUEsZUFBQSxVQUFVLElBQUMsQ0FBQSxhQUFBLFFBQVEsSUFBQyxDQUFBLG1CQUFBO01BQ2hELElBQUcsRUFBRSxDQUFDLGVBQUgsQ0FBbUIsSUFBQyxDQUFBLFdBQXBCLENBQUg7UUFDRSxNQUFBLEdBQVMsd0NBRFg7T0FBQSxNQUFBO1FBR0UsTUFBQSxHQUFTLG1DQUhYOztNQUtBLDRDQUNFO1FBQUEsTUFBQSxFQUFRLE1BQVI7UUFDQSxXQUFBLEVBQWEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFiLENBQXdCLElBQUMsQ0FBQSxXQUF6QixDQURiO1FBRUEsTUFBQSxFQUFRLElBRlI7UUFHQSxTQUFBLEVBQVcsa0JBSFg7T0FERjtJQU5XOzt5QkFZYixTQUFBLEdBQVcsU0FBQyxPQUFEO0FBQ1QsVUFBQTtNQUFBLE9BQUEsR0FBVSxPQUFPLENBQUMsT0FBUixDQUFnQixNQUFoQixFQUF3QixFQUF4QjtNQUNWLElBQUEsQ0FBTyxJQUFJLENBQUMsVUFBTCxDQUFnQixPQUFoQixDQUFQO1FBQ0csV0FBWSxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWIsQ0FBNEIsSUFBQyxDQUFBLFdBQTdCO1FBQ2IsT0FBQSxHQUFVLElBQUksQ0FBQyxJQUFMLENBQVUsUUFBVixFQUFvQixPQUFwQjtRQUNWLElBQUEsQ0FBYyxPQUFkO0FBQUEsaUJBQUE7U0FIRjs7TUFLQSxJQUFHLElBQUMsQ0FBQSxXQUFELEtBQWdCLE9BQW5CO1FBQ0UsSUFBQyxDQUFBLEtBQUQsQ0FBQTtBQUNBLGVBRkY7O01BSUEsSUFBQSxDQUFPLElBQUMsQ0FBQSxjQUFELENBQWdCLE9BQWhCLENBQVA7UUFDRSxJQUFDLENBQUEsU0FBRCxDQUFXLEdBQUEsR0FBSSxPQUFKLEdBQVksbUJBQXZCO0FBQ0EsZUFGRjs7TUFJQSxhQUFBLEdBQWdCLElBQUksQ0FBQyxPQUFMLENBQWEsT0FBYjtBQUNoQjs7VUFDRSxJQUFDLENBQUEsU0FBVTtZQUFBLFdBQUEsRUFBYSxJQUFDLENBQUEsV0FBZDtZQUEyQixPQUFBLEVBQVMsT0FBcEM7OztRQUNYLElBQUEsQ0FBc0MsRUFBRSxDQUFDLFVBQUgsQ0FBYyxhQUFkLENBQXRDO1VBQUEsRUFBRSxDQUFDLFlBQUgsQ0FBZ0IsYUFBaEIsRUFBQTs7UUFDQSxFQUFFLENBQUMsUUFBSCxDQUFZLElBQUMsQ0FBQSxXQUFiLEVBQTBCLE9BQTFCOztVQUNBLElBQUMsQ0FBQSxPQUFRO1lBQUEsV0FBQSxFQUFhLElBQUMsQ0FBQSxXQUFkO1lBQTJCLE9BQUEsRUFBUyxPQUFwQzs7O1FBQ1QsSUFBRyxJQUFBLEdBQU8sV0FBQSxDQUFZLE9BQVosQ0FBVjtVQUNFLElBQUksQ0FBQyxhQUFMLENBQW1CLElBQUMsQ0FBQSxXQUFwQjtVQUNBLElBQUksQ0FBQyxhQUFMLENBQW1CLE9BQW5CLEVBRkY7O2VBR0EsSUFBQyxDQUFBLEtBQUQsQ0FBQSxFQVJGO09BQUEsY0FBQTtRQVNNO1FBQ0osSUFBQyxDQUFBLFNBQUQsQ0FBYyxLQUFLLENBQUMsT0FBUCxHQUFlLEdBQTVCO3lEQUNBLElBQUMsQ0FBQSxhQUFjO1VBQUEsV0FBQSxFQUFhLElBQUMsQ0FBQSxXQUFkO1VBQTJCLE9BQUEsRUFBUyxPQUFwQztvQkFYakI7O0lBaEJTOzt5QkE2QlgsY0FBQSxHQUFnQixTQUFDLE9BQUQ7QUFDZCxVQUFBO0FBQUE7UUFDRSxPQUFBLEdBQVUsRUFBRSxDQUFDLFFBQUgsQ0FBWSxJQUFDLENBQUEsV0FBYjtRQUNWLE9BQUEsR0FBVSxFQUFFLENBQUMsUUFBSCxDQUFZLE9BQVo7ZUFLVixJQUFDLENBQUEsV0FBVyxDQUFDLFdBQWIsQ0FBQSxDQUFBLEtBQThCLE9BQU8sQ0FBQyxXQUFSLENBQUEsQ0FBOUIsSUFDRSxPQUFPLENBQUMsR0FBUixLQUFlLE9BQU8sQ0FBQyxHQUR6QixJQUVFLE9BQU8sQ0FBQyxHQUFSLEtBQWUsT0FBTyxDQUFDLElBVDNCO09BQUEsY0FBQTtlQVdFLEtBWEY7O0lBRGM7Ozs7S0ExQ087QUFOekIiLCJzb3VyY2VzQ29udGVudCI6WyJwYXRoID0gcmVxdWlyZSAncGF0aCdcbmZzID0gcmVxdWlyZSAnZnMtcGx1cydcbkRpYWxvZyA9IHJlcXVpcmUgJy4vZGlhbG9nJ1xue3JlcG9Gb3JQYXRofSA9IHJlcXVpcmUgXCIuL2hlbHBlcnNcIlxuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBNb3ZlRGlhbG9nIGV4dGVuZHMgRGlhbG9nXG4gIGNvbnN0cnVjdG9yOiAoQGluaXRpYWxQYXRoLCB7QHdpbGxNb3ZlLCBAb25Nb3ZlLCBAb25Nb3ZlRmFpbGVkfSkgLT5cbiAgICBpZiBmcy5pc0RpcmVjdG9yeVN5bmMoQGluaXRpYWxQYXRoKVxuICAgICAgcHJvbXB0ID0gJ0VudGVyIHRoZSBuZXcgcGF0aCBmb3IgdGhlIGRpcmVjdG9yeS4nXG4gICAgZWxzZVxuICAgICAgcHJvbXB0ID0gJ0VudGVyIHRoZSBuZXcgcGF0aCBmb3IgdGhlIGZpbGUuJ1xuXG4gICAgc3VwZXJcbiAgICAgIHByb21wdDogcHJvbXB0XG4gICAgICBpbml0aWFsUGF0aDogYXRvbS5wcm9qZWN0LnJlbGF0aXZpemUoQGluaXRpYWxQYXRoKVxuICAgICAgc2VsZWN0OiB0cnVlXG4gICAgICBpY29uQ2xhc3M6ICdpY29uLWFycm93LXJpZ2h0J1xuXG4gIG9uQ29uZmlybTogKG5ld1BhdGgpIC0+XG4gICAgbmV3UGF0aCA9IG5ld1BhdGgucmVwbGFjZSgvXFxzKyQvLCAnJykgIyBSZW1vdmUgdHJhaWxpbmcgd2hpdGVzcGFjZVxuICAgIHVubGVzcyBwYXRoLmlzQWJzb2x1dGUobmV3UGF0aClcbiAgICAgIFtyb290UGF0aF0gPSBhdG9tLnByb2plY3QucmVsYXRpdml6ZVBhdGgoQGluaXRpYWxQYXRoKVxuICAgICAgbmV3UGF0aCA9IHBhdGguam9pbihyb290UGF0aCwgbmV3UGF0aClcbiAgICAgIHJldHVybiB1bmxlc3MgbmV3UGF0aFxuXG4gICAgaWYgQGluaXRpYWxQYXRoIGlzIG5ld1BhdGhcbiAgICAgIEBjbG9zZSgpXG4gICAgICByZXR1cm5cblxuICAgIHVubGVzcyBAaXNOZXdQYXRoVmFsaWQobmV3UGF0aClcbiAgICAgIEBzaG93RXJyb3IoXCInI3tuZXdQYXRofScgYWxyZWFkeSBleGlzdHMuXCIpXG4gICAgICByZXR1cm5cblxuICAgIGRpcmVjdG9yeVBhdGggPSBwYXRoLmRpcm5hbWUobmV3UGF0aClcbiAgICB0cnlcbiAgICAgIEB3aWxsTW92ZT8oaW5pdGlhbFBhdGg6IEBpbml0aWFsUGF0aCwgbmV3UGF0aDogbmV3UGF0aClcbiAgICAgIGZzLm1ha2VUcmVlU3luYyhkaXJlY3RvcnlQYXRoKSB1bmxlc3MgZnMuZXhpc3RzU3luYyhkaXJlY3RvcnlQYXRoKVxuICAgICAgZnMubW92ZVN5bmMoQGluaXRpYWxQYXRoLCBuZXdQYXRoKVxuICAgICAgQG9uTW92ZT8oaW5pdGlhbFBhdGg6IEBpbml0aWFsUGF0aCwgbmV3UGF0aDogbmV3UGF0aClcbiAgICAgIGlmIHJlcG8gPSByZXBvRm9yUGF0aChuZXdQYXRoKVxuICAgICAgICByZXBvLmdldFBhdGhTdGF0dXMoQGluaXRpYWxQYXRoKVxuICAgICAgICByZXBvLmdldFBhdGhTdGF0dXMobmV3UGF0aClcbiAgICAgIEBjbG9zZSgpXG4gICAgY2F0Y2ggZXJyb3JcbiAgICAgIEBzaG93RXJyb3IoXCIje2Vycm9yLm1lc3NhZ2V9LlwiKVxuICAgICAgQG9uTW92ZUZhaWxlZD8oaW5pdGlhbFBhdGg6IEBpbml0aWFsUGF0aCwgbmV3UGF0aDogbmV3UGF0aClcblxuICBpc05ld1BhdGhWYWxpZDogKG5ld1BhdGgpIC0+XG4gICAgdHJ5XG4gICAgICBvbGRTdGF0ID0gZnMuc3RhdFN5bmMoQGluaXRpYWxQYXRoKVxuICAgICAgbmV3U3RhdCA9IGZzLnN0YXRTeW5jKG5ld1BhdGgpXG5cbiAgICAgICMgTmV3IHBhdGggZXhpc3RzIHNvIGNoZWNrIGlmIGl0IHBvaW50cyB0byB0aGUgc2FtZSBmaWxlIGFzIHRoZSBpbml0aWFsXG4gICAgICAjIHBhdGggdG8gc2VlIGlmIHRoZSBjYXNlIG9mIHRoZSBmaWxlIG5hbWUgaXMgYmVpbmcgY2hhbmdlZCBvbiBhIG9uIGFcbiAgICAgICMgY2FzZSBpbnNlbnNpdGl2ZSBmaWxlc3lzdGVtLlxuICAgICAgQGluaXRpYWxQYXRoLnRvTG93ZXJDYXNlKCkgaXMgbmV3UGF0aC50b0xvd2VyQ2FzZSgpIGFuZFxuICAgICAgICBvbGRTdGF0LmRldiBpcyBuZXdTdGF0LmRldiBhbmRcbiAgICAgICAgb2xkU3RhdC5pbm8gaXMgbmV3U3RhdC5pbm9cbiAgICBjYXRjaFxuICAgICAgdHJ1ZSAjIG5ldyBwYXRoIGRvZXMgbm90IGV4aXN0IHNvIGl0IGlzIHZhbGlkXG4iXX0=
