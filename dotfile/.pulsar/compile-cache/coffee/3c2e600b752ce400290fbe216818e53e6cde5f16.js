(function() {
  var BufferedProcess, Client, CompositeDisposable, Emitter, PackageManager, _, createJsonParseError, createProcessError, handleProcessErrors, ref, semver,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  _ = require('underscore-plus');

  ref = require('atom'), BufferedProcess = ref.BufferedProcess, CompositeDisposable = ref.CompositeDisposable, Emitter = ref.Emitter;

  semver = require('semver');

  Client = require('./atom-io-client');

  module.exports = PackageManager = (function() {
    PackageManager.prototype.CACHE_EXPIRY = 1000 * 60 * 10;

    function PackageManager() {
      this.setProxyServersAsync = bind(this.setProxyServersAsync, this);
      this.setProxyServers = bind(this.setProxyServers, this);
      this.packagePromises = [];
      this.apmCache = {
        loadOutdated: {
          value: null,
          expiry: 0
        }
      };
      this.emitter = new Emitter;
    }

    PackageManager.prototype.getClient = function() {
      return this.client != null ? this.client : this.client = new Client(this);
    };

    PackageManager.prototype.isPackageInstalled = function(packageName) {
      if (atom.packages.isPackageLoaded(packageName)) {
        return true;
      } else {
        return atom.packages.getAvailablePackageNames().indexOf(packageName) > -1;
      }
    };

    PackageManager.prototype.packageHasSettings = function(packageName) {
      var grammar, grammars, i, len, pack, ref1, schema;
      grammars = (ref1 = atom.grammars.getGrammars()) != null ? ref1 : [];
      for (i = 0, len = grammars.length; i < len; i++) {
        grammar = grammars[i];
        if (grammar.path) {
          if (grammar.packageName === packageName) {
            return true;
          }
        }
      }
      pack = atom.packages.getLoadedPackage(packageName);
      if ((pack != null) && !atom.packages.isPackageActive(packageName)) {
        pack.activateConfig();
      }
      schema = atom.config.getSchema(packageName);
      return (schema != null) && (schema.type !== 'any');
    };

    PackageManager.prototype.setProxyServers = function(callback) {
      var session;
      session = atom.getCurrentWindow().webContents.session;
      return session.resolveProxy('http://atom.io', (function(_this) {
        return function(httpProxy) {
          _this.applyProxyToEnv('http_proxy', httpProxy);
          return session.resolveProxy('https://pulsar-edit.dev', function(httpsProxy) {
            _this.applyProxyToEnv('https_proxy', httpsProxy);
            return callback();
          });
        };
      })(this));
    };

    PackageManager.prototype.setProxyServersAsync = function(callback) {
      var httpProxyPromise, httpsProxyPromise;
      httpProxyPromise = atom.resolveProxy('http://atom.io').then((function(_this) {
        return function(proxy) {
          return _this.applyProxyToEnv('http_proxy', proxy);
        };
      })(this));
      httpsProxyPromise = atom.resolveProxy('https://pulsar-edit.dev').then((function(_this) {
        return function(proxy) {
          return _this.applyProxyToEnv('https_proxy', proxy);
        };
      })(this));
      return Promise.all([httpProxyPromise, httpsProxyPromise]).then(callback);
    };

    PackageManager.prototype.applyProxyToEnv = function(envName, proxy) {
      if (proxy != null) {
        proxy = proxy.split(' ');
        switch (proxy[0].trim().toUpperCase()) {
          case 'DIRECT':
            delete process.env[envName];
            break;
          case 'PROXY':
            process.env[envName] = 'http://' + proxy[1];
        }
      }
    };

    PackageManager.prototype.runCommand = function(args, callback) {
      var bufferedProcess, command, errorLines, exit, outputLines, stderr, stdout;
      command = atom.packages.getApmPath();
      outputLines = [];
      stdout = function(lines) {
        return outputLines.push(lines);
      };
      errorLines = [];
      stderr = function(lines) {
        return errorLines.push(lines);
      };
      exit = function(code) {
        return callback(code, outputLines.join('\n'), errorLines.join('\n'));
      };
      args.push('--no-color');
      if (atom.config.get('core.useProxySettingsWhenCallingApm')) {
        bufferedProcess = new BufferedProcess({
          command: command,
          args: args,
          stdout: stdout,
          stderr: stderr,
          exit: exit,
          autoStart: false
        });
        if (atom.resolveProxy != null) {
          this.setProxyServersAsync(function() {
            return bufferedProcess.start();
          });
        } else {
          this.setProxyServers(function() {
            return bufferedProcess.start();
          });
        }
        return bufferedProcess;
      } else {
        return new BufferedProcess({
          command: command,
          args: args,
          stdout: stdout,
          stderr: stderr,
          exit: exit
        });
      }
    };

    PackageManager.prototype.loadInstalled = function(callback) {
      var apmProcess, args, errorMessage;
      args = ['ls', '--json'];
      errorMessage = 'Fetching local packages failed.';
      apmProcess = this.runCommand(args, function(code, stdout, stderr) {
        var error, packages, parseError, ref1;
        if (code === 0) {
          try {
            packages = (ref1 = JSON.parse(stdout)) != null ? ref1 : [];
          } catch (error1) {
            parseError = error1;
            error = createJsonParseError(errorMessage, parseError, stdout);
            return callback(error);
          }
          return callback(null, packages);
        } else {
          error = new Error(errorMessage);
          error.stdout = stdout;
          error.stderr = stderr;
          return callback(error);
        }
      });
      return handleProcessErrors(apmProcess, errorMessage, callback);
    };

    PackageManager.prototype.loadFeatured = function(loadThemes, callback) {
      var apmProcess, args, errorMessage, version;
      if (!callback) {
        callback = loadThemes;
        loadThemes = false;
      }
      args = ['featured', '--json'];
      version = atom.getVersion();
      if (loadThemes) {
        args.push('--themes');
      }
      if (semver.valid(version)) {
        args.push('--compatible', version);
      }
      errorMessage = 'Fetching featured packages failed.';
      apmProcess = this.runCommand(args, function(code, stdout, stderr) {
        var error, packages, parseError, ref1;
        if (code === 0) {
          try {
            packages = (ref1 = JSON.parse(stdout)) != null ? ref1 : [];
          } catch (error1) {
            parseError = error1;
            error = createJsonParseError(errorMessage, parseError, stdout);
            return callback(error);
          }
          return callback(null, packages);
        } else {
          error = new Error(errorMessage);
          error.stdout = stdout;
          error.stderr = stderr;
          return callback(error);
        }
      });
      return handleProcessErrors(apmProcess, errorMessage, callback);
    };

    PackageManager.prototype.loadOutdated = function(clearCache, callback) {
      var apmProcess, args, errorMessage, version;
      if (clearCache) {
        this.clearOutdatedCache();
      } else if (this.apmCache.loadOutdated.value && this.apmCache.loadOutdated.expiry > Date.now()) {
        return callback(null, this.apmCache.loadOutdated.value);
      }
      args = ['outdated', '--json'];
      version = atom.getVersion();
      if (semver.valid(version)) {
        args.push('--compatible', version);
      }
      errorMessage = 'Fetching outdated packages and themes failed.';
      apmProcess = this.runCommand(args, (function(_this) {
        return function(code, stdout, stderr) {
          var error, i, len, pack, packages, parseError, ref1, updatablePackages;
          if (code === 0) {
            try {
              packages = (ref1 = JSON.parse(stdout)) != null ? ref1 : [];
            } catch (error1) {
              parseError = error1;
              error = createJsonParseError(errorMessage, parseError, stdout);
              return callback(error);
            }
            updatablePackages = (function() {
              var i, len, results;
              results = [];
              for (i = 0, len = packages.length; i < len; i++) {
                pack = packages[i];
                if (!this.getVersionPinnedPackages().includes(pack != null ? pack.name : void 0)) {
                  results.push(pack);
                }
              }
              return results;
            }).call(_this);
            _this.apmCache.loadOutdated = {
              value: updatablePackages,
              expiry: Date.now() + _this.CACHE_EXPIRY
            };
            for (i = 0, len = updatablePackages.length; i < len; i++) {
              pack = updatablePackages[i];
              _this.emitPackageEvent('update-available', pack);
            }
            return callback(null, updatablePackages);
          } else {
            error = new Error(errorMessage);
            error.stdout = stdout;
            error.stderr = stderr;
            return callback(error);
          }
        };
      })(this));
      return handleProcessErrors(apmProcess, errorMessage, callback);
    };

    PackageManager.prototype.getVersionPinnedPackages = function() {
      var ref1;
      return (ref1 = atom.config.get('core.versionPinnedPackages')) != null ? ref1 : [];
    };

    PackageManager.prototype.clearOutdatedCache = function() {
      return this.apmCache.loadOutdated = {
        value: null,
        expiry: 0
      };
    };

    PackageManager.prototype.loadPackage = function(packageName, callback) {
      var apmProcess, args, errorMessage;
      args = ['view', packageName, '--json'];
      errorMessage = "Fetching package '" + packageName + "' failed.";
      apmProcess = this.runCommand(args, function(code, stdout, stderr) {
        var error, packages, parseError, ref1;
        if (code === 0) {
          try {
            packages = (ref1 = JSON.parse(stdout)) != null ? ref1 : [];
          } catch (error1) {
            parseError = error1;
            error = createJsonParseError(errorMessage, parseError, stdout);
            return callback(error);
          }
          return callback(null, packages);
        } else {
          error = new Error(errorMessage);
          error.stdout = stdout;
          error.stderr = stderr;
          return callback(error);
        }
      });
      return handleProcessErrors(apmProcess, errorMessage, callback);
    };

    PackageManager.prototype.loadCompatiblePackageVersion = function(packageName, callback) {
      var apmProcess, args, errorMessage;
      args = ['view', packageName, '--json', '--compatible', this.normalizeVersion(atom.getVersion())];
      errorMessage = "Fetching package '" + packageName + "' failed.";
      apmProcess = this.runCommand(args, function(code, stdout, stderr) {
        var error, packages, parseError, ref1;
        if (code === 0) {
          try {
            packages = (ref1 = JSON.parse(stdout)) != null ? ref1 : [];
          } catch (error1) {
            parseError = error1;
            error = createJsonParseError(errorMessage, parseError, stdout);
            return callback(error);
          }
          return callback(null, packages);
        } else {
          error = new Error(errorMessage);
          error.stdout = stdout;
          error.stderr = stderr;
          return callback(error);
        }
      });
      return handleProcessErrors(apmProcess, errorMessage, callback);
    };

    PackageManager.prototype.getInstalled = function() {
      return new Promise((function(_this) {
        return function(resolve, reject) {
          return _this.loadInstalled(function(error, result) {
            if (error) {
              return reject(error);
            } else {
              return resolve(result);
            }
          });
        };
      })(this));
    };

    PackageManager.prototype.getFeatured = function(loadThemes) {
      return new Promise((function(_this) {
        return function(resolve, reject) {
          return _this.loadFeatured(!!loadThemes, function(error, result) {
            if (error) {
              return reject(error);
            } else {
              return resolve(result);
            }
          });
        };
      })(this));
    };

    PackageManager.prototype.getOutdated = function(clearCache) {
      if (clearCache == null) {
        clearCache = false;
      }
      return new Promise((function(_this) {
        return function(resolve, reject) {
          return _this.loadOutdated(clearCache, function(error, result) {
            if (error) {
              return reject(error);
            } else {
              return resolve(result);
            }
          });
        };
      })(this));
    };

    PackageManager.prototype.getPackage = function(packageName) {
      var base;
      return (base = this.packagePromises)[packageName] != null ? base[packageName] : base[packageName] = new Promise((function(_this) {
        return function(resolve, reject) {
          return _this.loadPackage(packageName, function(error, result) {
            if (error) {
              return reject(error);
            } else {
              return resolve(result);
            }
          });
        };
      })(this));
    };

    PackageManager.prototype.satisfiesVersion = function(version, metadata) {
      var engine, ref1, ref2;
      engine = (ref1 = (ref2 = metadata.engines) != null ? ref2.atom : void 0) != null ? ref1 : '*';
      if (!semver.validRange(engine)) {
        return false;
      }
      return semver.satisfies(version, engine);
    };

    PackageManager.prototype.normalizeVersion = function(version) {
      if (typeof version === 'string') {
        version = version.split('-')[0];
      }
      return version;
    };

    PackageManager.prototype.update = function(pack, newVersion, callback) {
      var apmInstallSource, apmProcess, args, errorMessage, exit, name, onError, theme;
      name = pack.name, theme = pack.theme, apmInstallSource = pack.apmInstallSource;
      errorMessage = newVersion ? "Updating to \u201C" + name + "@" + newVersion + "\u201D failed." : "Updating to latest sha failed.";
      onError = (function(_this) {
        return function(error) {
          error.packageInstallError = !theme;
          _this.emitPackageEvent('update-failed', pack, error);
          return typeof callback === "function" ? callback(error) : void 0;
        };
      })(this);
      if ((apmInstallSource != null ? apmInstallSource.type : void 0) === 'git') {
        args = ['install', apmInstallSource.source];
      } else {
        args = ['install', name + "@" + newVersion];
      }
      exit = (function(_this) {
        return function(code, stdout, stderr) {
          var error;
          if (code === 0) {
            _this.clearOutdatedCache();
            if (typeof callback === "function") {
              callback();
            }
            return _this.emitPackageEvent('updated', pack);
          } else {
            error = new Error(errorMessage);
            error.stdout = stdout;
            error.stderr = stderr;
            return onError(error);
          }
        };
      })(this);
      this.emitPackageEvent('updating', pack);
      apmProcess = this.runCommand(args, exit);
      return handleProcessErrors(apmProcess, errorMessage, onError);
    };

    PackageManager.prototype.unload = function(name) {
      if (atom.packages.isPackageLoaded(name)) {
        if (atom.packages.isPackageActive(name)) {
          atom.packages.deactivatePackage(name);
        }
        return atom.packages.unloadPackage(name);
      }
    };

    PackageManager.prototype.install = function(pack, callback) {
      var activateOnFailure, activateOnSuccess, apmProcess, args, errorMessage, exit, name, nameWithVersion, onError, theme, version;
      name = pack.name, version = pack.version, theme = pack.theme;
      activateOnSuccess = !theme && !atom.packages.isPackageDisabled(name);
      activateOnFailure = atom.packages.isPackageActive(name);
      nameWithVersion = version != null ? name + "@" + version : name;
      this.unload(name);
      args = ['install', nameWithVersion, '--json'];
      errorMessage = "Installing \u201C" + nameWithVersion + "\u201D failed.";
      onError = (function(_this) {
        return function(error) {
          error.packageInstallError = !theme;
          _this.emitPackageEvent('install-failed', pack, error);
          return typeof callback === "function" ? callback(error) : void 0;
        };
      })(this);
      exit = (function(_this) {
        return function(code, stdout, stderr) {
          var err, error, packageInfo;
          if (code === 0) {
            try {
              packageInfo = JSON.parse(stdout)[0];
              pack = _.extend({}, pack, packageInfo.metadata);
              name = pack.name;
            } catch (error1) {
              err = error1;
            }
            _this.clearOutdatedCache();
            if (activateOnSuccess) {
              atom.packages.activatePackage(name);
            } else {
              atom.packages.loadPackage(name);
            }
            if (typeof callback === "function") {
              callback();
            }
            return _this.emitPackageEvent('installed', pack);
          } else {
            if (activateOnFailure) {
              atom.packages.activatePackage(name);
            }
            error = new Error(errorMessage);
            error.stdout = stdout;
            error.stderr = stderr;
            return onError(error);
          }
        };
      })(this);
      this.emitPackageEvent('installing', pack);
      apmProcess = this.runCommand(args, exit);
      return handleProcessErrors(apmProcess, errorMessage, onError);
    };

    PackageManager.prototype.uninstall = function(pack, callback) {
      var apmProcess, errorMessage, name, onError;
      name = pack.name;
      if (atom.packages.isPackageActive(name)) {
        atom.packages.deactivatePackage(name);
      }
      errorMessage = "Uninstalling \u201C" + name + "\u201D failed.";
      onError = (function(_this) {
        return function(error) {
          _this.emitPackageEvent('uninstall-failed', pack, error);
          return typeof callback === "function" ? callback(error) : void 0;
        };
      })(this);
      this.emitPackageEvent('uninstalling', pack);
      apmProcess = this.runCommand(['uninstall', '--hard', name], (function(_this) {
        return function(code, stdout, stderr) {
          var error;
          if (code === 0) {
            _this.clearOutdatedCache();
            _this.unload(name);
            _this.removePackageNameFromDisabledPackages(name);
            if (typeof callback === "function") {
              callback();
            }
            return _this.emitPackageEvent('uninstalled', pack);
          } else {
            error = new Error(errorMessage);
            error.stdout = stdout;
            error.stderr = stderr;
            return onError(error);
          }
        };
      })(this));
      return handleProcessErrors(apmProcess, errorMessage, onError);
    };

    PackageManager.prototype.canUpgrade = function(installedPackage, availableVersion) {
      var installedVersion;
      if (installedPackage == null) {
        return false;
      }
      installedVersion = installedPackage.metadata.version;
      if (!semver.valid(installedVersion)) {
        return false;
      }
      if (!semver.valid(availableVersion)) {
        return false;
      }
      return semver.gt(availableVersion, installedVersion);
    };

    PackageManager.prototype.getPackageTitle = function(arg) {
      var name;
      name = arg.name;
      return _.undasherize(_.uncamelcase(name));
    };

    PackageManager.prototype.getRepositoryUrl = function(arg) {
      var metadata, ref1, ref2, repoName, repoUrl, repository;
      metadata = arg.metadata;
      repository = metadata.repository;
      repoUrl = (ref1 = (ref2 = repository != null ? repository.url : void 0) != null ? ref2 : repository) != null ? ref1 : '';
      if (repoUrl.match('git@github')) {
        repoName = repoUrl.split(':')[1];
        repoUrl = "https://github.com/" + repoName;
      }
      return repoUrl.replace(/\.git$/, '').replace(/\/+$/, '').replace(/^git\+/, '');
    };

    PackageManager.prototype.getRepositoryBugUri = function(arg) {
      var bugUri, bugs, metadata, ref1, ref2;
      metadata = arg.metadata;
      bugs = metadata.bugs;
      if (typeof bugs === 'string') {
        bugUri = bugs;
      } else {
        bugUri = (ref1 = (ref2 = bugs != null ? bugs.url : void 0) != null ? ref2 : bugs != null ? bugs.email : void 0) != null ? ref1 : this.getRepositoryUrl({
          metadata: metadata
        }) + '/issues/new';
        if (bugUri.includes('@')) {
          bugUri = 'mailto:' + bugUri;
        }
      }
      return bugUri;
    };

    PackageManager.prototype.checkNativeBuildTools = function() {
      return new Promise((function(_this) {
        return function(resolve, reject) {
          var apmProcess;
          apmProcess = _this.runCommand(['install', '--check'], function(code, stdout, stderr) {
            if (code === 0) {
              return resolve();
            } else {
              return reject(new Error());
            }
          });
          return apmProcess.onWillThrowError(function(arg) {
            var error, handle;
            error = arg.error, handle = arg.handle;
            handle();
            return reject(error);
          });
        };
      })(this));
    };

    PackageManager.prototype.removePackageNameFromDisabledPackages = function(packageName) {
      return atom.config.removeAtKeyPath('core.disabledPackages', packageName);
    };

    PackageManager.prototype.emitPackageEvent = function(eventName, pack, error) {
      var ref1, ref2, theme;
      theme = (ref1 = pack.theme) != null ? ref1 : (ref2 = pack.metadata) != null ? ref2.theme : void 0;
      eventName = theme ? "theme-" + eventName : "package-" + eventName;
      return this.emitter.emit(eventName, {
        pack: pack,
        error: error
      });
    };

    PackageManager.prototype.on = function(selectors, callback) {
      var i, len, ref1, selector, subscriptions;
      subscriptions = new CompositeDisposable;
      ref1 = selectors.split(" ");
      for (i = 0, len = ref1.length; i < len; i++) {
        selector = ref1[i];
        subscriptions.add(this.emitter.on(selector, callback));
      }
      return subscriptions;
    };

    return PackageManager;

  })();

  createJsonParseError = function(message, parseError, stdout) {
    var error;
    error = new Error(message);
    error.stdout = '';
    error.stderr = parseError.message + ": " + stdout;
    return error;
  };

  createProcessError = function(message, processError) {
    var error;
    error = new Error(message);
    error.stdout = '';
    error.stderr = processError.message;
    return error;
  };

  handleProcessErrors = function(apmProcess, message, callback) {
    return apmProcess.onWillThrowError(function(arg) {
      var error, handle;
      error = arg.error, handle = arg.handle;
      handle();
      return callback(createProcessError(message, error));
    });
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zZXR0aW5ncy12aWV3L2xpYi9wYWNrYWdlLW1hbmFnZXIuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQSxvSkFBQTtJQUFBOztFQUFBLENBQUEsR0FBSSxPQUFBLENBQVEsaUJBQVI7O0VBQ0osTUFBa0QsT0FBQSxDQUFRLE1BQVIsQ0FBbEQsRUFBQyxxQ0FBRCxFQUFrQiw2Q0FBbEIsRUFBdUM7O0VBQ3ZDLE1BQUEsR0FBUyxPQUFBLENBQVEsUUFBUjs7RUFFVCxNQUFBLEdBQVMsT0FBQSxDQUFRLGtCQUFSOztFQUVULE1BQU0sQ0FBQyxPQUFQLEdBQ007NkJBRUosWUFBQSxHQUFjLElBQUEsR0FBSyxFQUFMLEdBQVE7O0lBRVQsd0JBQUE7OztNQUNYLElBQUMsQ0FBQSxlQUFELEdBQW1CO01BQ25CLElBQUMsQ0FBQSxRQUFELEdBQ0U7UUFBQSxZQUFBLEVBQ0U7VUFBQSxLQUFBLEVBQU8sSUFBUDtVQUNBLE1BQUEsRUFBUSxDQURSO1NBREY7O01BSUYsSUFBQyxDQUFBLE9BQUQsR0FBVyxJQUFJO0lBUEo7OzZCQVNiLFNBQUEsR0FBVyxTQUFBO21DQUNULElBQUMsQ0FBQSxTQUFELElBQUMsQ0FBQSxTQUFVLElBQUksTUFBSixDQUFXLElBQVg7SUFERjs7NkJBR1gsa0JBQUEsR0FBb0IsU0FBQyxXQUFEO01BQ2xCLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFkLENBQThCLFdBQTlCLENBQUg7ZUFDRSxLQURGO09BQUEsTUFBQTtlQUdFLElBQUksQ0FBQyxRQUFRLENBQUMsd0JBQWQsQ0FBQSxDQUF3QyxDQUFDLE9BQXpDLENBQWlELFdBQWpELENBQUEsR0FBZ0UsQ0FBQyxFQUhuRTs7SUFEa0I7OzZCQU1wQixrQkFBQSxHQUFvQixTQUFDLFdBQUQ7QUFDbEIsVUFBQTtNQUFBLFFBQUEseURBQXlDO0FBQ3pDLFdBQUEsMENBQUE7O1lBQTZCLE9BQU8sQ0FBQztVQUNuQyxJQUFlLE9BQU8sQ0FBQyxXQUFSLEtBQXVCLFdBQXRDO0FBQUEsbUJBQU8sS0FBUDs7O0FBREY7TUFHQSxJQUFBLEdBQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZCxDQUErQixXQUEvQjtNQUNQLElBQXlCLGNBQUEsSUFBVSxDQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZCxDQUE4QixXQUE5QixDQUF2QztRQUFBLElBQUksQ0FBQyxjQUFMLENBQUEsRUFBQTs7TUFDQSxNQUFBLEdBQVMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFaLENBQXNCLFdBQXRCO2FBQ1QsZ0JBQUEsSUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFQLEtBQWlCLEtBQWxCO0lBUk07OzZCQVVwQixlQUFBLEdBQWlCLFNBQUMsUUFBRDtBQUNmLFVBQUE7TUFBQSxPQUFBLEdBQVUsSUFBSSxDQUFDLGdCQUFMLENBQUEsQ0FBdUIsQ0FBQyxXQUFXLENBQUM7YUFDOUMsT0FBTyxDQUFDLFlBQVIsQ0FBcUIsZ0JBQXJCLEVBQXVDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxTQUFEO1VBQ3JDLEtBQUMsQ0FBQSxlQUFELENBQWlCLFlBQWpCLEVBQStCLFNBQS9CO2lCQUNBLE9BQU8sQ0FBQyxZQUFSLENBQXFCLHlCQUFyQixFQUFnRCxTQUFDLFVBQUQ7WUFDOUMsS0FBQyxDQUFBLGVBQUQsQ0FBaUIsYUFBakIsRUFBZ0MsVUFBaEM7bUJBQ0EsUUFBQSxDQUFBO1VBRjhDLENBQWhEO1FBRnFDO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF2QztJQUZlOzs2QkFRakIsb0JBQUEsR0FBc0IsU0FBQyxRQUFEO0FBQ3BCLFVBQUE7TUFBQSxnQkFBQSxHQUFtQixJQUFJLENBQUMsWUFBTCxDQUFrQixnQkFBbEIsQ0FBbUMsQ0FBQyxJQUFwQyxDQUF5QyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsS0FBRDtpQkFBVyxLQUFDLENBQUEsZUFBRCxDQUFpQixZQUFqQixFQUErQixLQUEvQjtRQUFYO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6QztNQUNuQixpQkFBQSxHQUFvQixJQUFJLENBQUMsWUFBTCxDQUFrQix5QkFBbEIsQ0FBNEMsQ0FBQyxJQUE3QyxDQUFrRCxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsS0FBRDtpQkFBVyxLQUFDLENBQUEsZUFBRCxDQUFpQixhQUFqQixFQUFnQyxLQUFoQztRQUFYO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFsRDthQUNwQixPQUFPLENBQUMsR0FBUixDQUFZLENBQUMsZ0JBQUQsRUFBbUIsaUJBQW5CLENBQVosQ0FBa0QsQ0FBQyxJQUFuRCxDQUF3RCxRQUF4RDtJQUhvQjs7NkJBS3RCLGVBQUEsR0FBaUIsU0FBQyxPQUFELEVBQVUsS0FBVjtNQUNmLElBQUcsYUFBSDtRQUNFLEtBQUEsR0FBUSxLQUFLLENBQUMsS0FBTixDQUFZLEdBQVo7QUFDUixnQkFBTyxLQUFNLENBQUEsQ0FBQSxDQUFFLENBQUMsSUFBVCxDQUFBLENBQWUsQ0FBQyxXQUFoQixDQUFBLENBQVA7QUFBQSxlQUNPLFFBRFA7WUFDcUIsT0FBTyxPQUFPLENBQUMsR0FBSSxDQUFBLE9BQUE7QUFBakM7QUFEUCxlQUVPLE9BRlA7WUFFcUIsT0FBTyxDQUFDLEdBQUksQ0FBQSxPQUFBLENBQVosR0FBdUIsU0FBQSxHQUFZLEtBQU0sQ0FBQSxDQUFBO0FBRjlELFNBRkY7O0lBRGU7OzZCQVFqQixVQUFBLEdBQVksU0FBQyxJQUFELEVBQU8sUUFBUDtBQUNWLFVBQUE7TUFBQSxPQUFBLEdBQVUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFkLENBQUE7TUFDVixXQUFBLEdBQWM7TUFDZCxNQUFBLEdBQVMsU0FBQyxLQUFEO2VBQVcsV0FBVyxDQUFDLElBQVosQ0FBaUIsS0FBakI7TUFBWDtNQUNULFVBQUEsR0FBYTtNQUNiLE1BQUEsR0FBUyxTQUFDLEtBQUQ7ZUFBVyxVQUFVLENBQUMsSUFBWCxDQUFnQixLQUFoQjtNQUFYO01BQ1QsSUFBQSxHQUFPLFNBQUMsSUFBRDtlQUNMLFFBQUEsQ0FBUyxJQUFULEVBQWUsV0FBVyxDQUFDLElBQVosQ0FBaUIsSUFBakIsQ0FBZixFQUF1QyxVQUFVLENBQUMsSUFBWCxDQUFnQixJQUFoQixDQUF2QztNQURLO01BR1AsSUFBSSxDQUFDLElBQUwsQ0FBVSxZQUFWO01BRUEsSUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IscUNBQWhCLENBQUg7UUFDRSxlQUFBLEdBQWtCLElBQUksZUFBSixDQUFvQjtVQUFDLFNBQUEsT0FBRDtVQUFVLE1BQUEsSUFBVjtVQUFnQixRQUFBLE1BQWhCO1VBQXdCLFFBQUEsTUFBeEI7VUFBZ0MsTUFBQSxJQUFoQztVQUFzQyxTQUFBLEVBQVcsS0FBakQ7U0FBcEI7UUFDbEIsSUFBRyx5QkFBSDtVQUNFLElBQUMsQ0FBQSxvQkFBRCxDQUFzQixTQUFBO21CQUFHLGVBQWUsQ0FBQyxLQUFoQixDQUFBO1VBQUgsQ0FBdEIsRUFERjtTQUFBLE1BQUE7VUFHRSxJQUFDLENBQUEsZUFBRCxDQUFpQixTQUFBO21CQUFHLGVBQWUsQ0FBQyxLQUFoQixDQUFBO1VBQUgsQ0FBakIsRUFIRjs7QUFJQSxlQUFPLGdCQU5UO09BQUEsTUFBQTtBQVFFLGVBQU8sSUFBSSxlQUFKLENBQW9CO1VBQUMsU0FBQSxPQUFEO1VBQVUsTUFBQSxJQUFWO1VBQWdCLFFBQUEsTUFBaEI7VUFBd0IsUUFBQSxNQUF4QjtVQUFnQyxNQUFBLElBQWhDO1NBQXBCLEVBUlQ7O0lBWFU7OzZCQXFCWixhQUFBLEdBQWUsU0FBQyxRQUFEO0FBQ2IsVUFBQTtNQUFBLElBQUEsR0FBTyxDQUFDLElBQUQsRUFBTyxRQUFQO01BQ1AsWUFBQSxHQUFlO01BQ2YsVUFBQSxHQUFhLElBQUMsQ0FBQSxVQUFELENBQVksSUFBWixFQUFrQixTQUFDLElBQUQsRUFBTyxNQUFQLEVBQWUsTUFBZjtBQUM3QixZQUFBO1FBQUEsSUFBRyxJQUFBLEtBQVEsQ0FBWDtBQUNFO1lBQ0UsUUFBQSxnREFBZ0MsR0FEbEM7V0FBQSxjQUFBO1lBRU07WUFDSixLQUFBLEdBQVEsb0JBQUEsQ0FBcUIsWUFBckIsRUFBbUMsVUFBbkMsRUFBK0MsTUFBL0M7QUFDUixtQkFBTyxRQUFBLENBQVMsS0FBVCxFQUpUOztpQkFLQSxRQUFBLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFORjtTQUFBLE1BQUE7VUFRRSxLQUFBLEdBQVEsSUFBSSxLQUFKLENBQVUsWUFBVjtVQUNSLEtBQUssQ0FBQyxNQUFOLEdBQWU7VUFDZixLQUFLLENBQUMsTUFBTixHQUFlO2lCQUNmLFFBQUEsQ0FBUyxLQUFULEVBWEY7O01BRDZCLENBQWxCO2FBY2IsbUJBQUEsQ0FBb0IsVUFBcEIsRUFBZ0MsWUFBaEMsRUFBOEMsUUFBOUM7SUFqQmE7OzZCQW1CZixZQUFBLEdBQWMsU0FBQyxVQUFELEVBQWEsUUFBYjtBQUNaLFVBQUE7TUFBQSxJQUFBLENBQU8sUUFBUDtRQUNFLFFBQUEsR0FBVztRQUNYLFVBQUEsR0FBYSxNQUZmOztNQUlBLElBQUEsR0FBTyxDQUFDLFVBQUQsRUFBYSxRQUFiO01BQ1AsT0FBQSxHQUFVLElBQUksQ0FBQyxVQUFMLENBQUE7TUFDVixJQUF5QixVQUF6QjtRQUFBLElBQUksQ0FBQyxJQUFMLENBQVUsVUFBVixFQUFBOztNQUNBLElBQXNDLE1BQU0sQ0FBQyxLQUFQLENBQWEsT0FBYixDQUF0QztRQUFBLElBQUksQ0FBQyxJQUFMLENBQVUsY0FBVixFQUEwQixPQUExQixFQUFBOztNQUNBLFlBQUEsR0FBZTtNQUVmLFVBQUEsR0FBYSxJQUFDLENBQUEsVUFBRCxDQUFZLElBQVosRUFBa0IsU0FBQyxJQUFELEVBQU8sTUFBUCxFQUFlLE1BQWY7QUFDN0IsWUFBQTtRQUFBLElBQUcsSUFBQSxLQUFRLENBQVg7QUFDRTtZQUNFLFFBQUEsZ0RBQWdDLEdBRGxDO1dBQUEsY0FBQTtZQUVNO1lBQ0osS0FBQSxHQUFRLG9CQUFBLENBQXFCLFlBQXJCLEVBQW1DLFVBQW5DLEVBQStDLE1BQS9DO0FBQ1IsbUJBQU8sUUFBQSxDQUFTLEtBQVQsRUFKVDs7aUJBTUEsUUFBQSxDQUFTLElBQVQsRUFBZSxRQUFmLEVBUEY7U0FBQSxNQUFBO1VBU0UsS0FBQSxHQUFRLElBQUksS0FBSixDQUFVLFlBQVY7VUFDUixLQUFLLENBQUMsTUFBTixHQUFlO1VBQ2YsS0FBSyxDQUFDLE1BQU4sR0FBZTtpQkFDZixRQUFBLENBQVMsS0FBVCxFQVpGOztNQUQ2QixDQUFsQjthQWViLG1CQUFBLENBQW9CLFVBQXBCLEVBQWdDLFlBQWhDLEVBQThDLFFBQTlDO0lBMUJZOzs2QkE0QmQsWUFBQSxHQUFjLFNBQUMsVUFBRCxFQUFhLFFBQWI7QUFDWixVQUFBO01BQUEsSUFBRyxVQUFIO1FBQ0UsSUFBQyxDQUFBLGtCQUFELENBQUEsRUFERjtPQUFBLE1BR0ssSUFBRyxJQUFDLENBQUEsUUFBUSxDQUFDLFlBQVksQ0FBQyxLQUF2QixJQUFpQyxJQUFDLENBQUEsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUF2QixHQUFnQyxJQUFJLENBQUMsR0FBTCxDQUFBLENBQXBFO0FBQ0gsZUFBTyxRQUFBLENBQVMsSUFBVCxFQUFlLElBQUMsQ0FBQSxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQXRDLEVBREo7O01BR0wsSUFBQSxHQUFPLENBQUMsVUFBRCxFQUFhLFFBQWI7TUFDUCxPQUFBLEdBQVUsSUFBSSxDQUFDLFVBQUwsQ0FBQTtNQUNWLElBQXNDLE1BQU0sQ0FBQyxLQUFQLENBQWEsT0FBYixDQUF0QztRQUFBLElBQUksQ0FBQyxJQUFMLENBQVUsY0FBVixFQUEwQixPQUExQixFQUFBOztNQUNBLFlBQUEsR0FBZTtNQUVmLFVBQUEsR0FBYSxJQUFDLENBQUEsVUFBRCxDQUFZLElBQVosRUFBa0IsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLElBQUQsRUFBTyxNQUFQLEVBQWUsTUFBZjtBQUM3QixjQUFBO1VBQUEsSUFBRyxJQUFBLEtBQVEsQ0FBWDtBQUNFO2NBQ0UsUUFBQSxnREFBZ0MsR0FEbEM7YUFBQSxjQUFBO2NBRU07Y0FDSixLQUFBLEdBQVEsb0JBQUEsQ0FBcUIsWUFBckIsRUFBbUMsVUFBbkMsRUFBK0MsTUFBL0M7QUFDUixxQkFBTyxRQUFBLENBQVMsS0FBVCxFQUpUOztZQU1BLGlCQUFBOztBQUFxQjttQkFBQSwwQ0FBQTs7b0JBQStCLENBQUksSUFBQyxDQUFBLHdCQUFELENBQUEsQ0FBMkIsQ0FBQyxRQUE1QixnQkFBcUMsSUFBSSxDQUFFLGFBQTNDOytCQUFuQzs7QUFBQTs7O1lBRXJCLEtBQUMsQ0FBQSxRQUFRLENBQUMsWUFBVixHQUNFO2NBQUEsS0FBQSxFQUFPLGlCQUFQO2NBQ0EsTUFBQSxFQUFRLElBQUksQ0FBQyxHQUFMLENBQUEsQ0FBQSxHQUFhLEtBQUMsQ0FBQSxZQUR0Qjs7QUFHRixpQkFBQSxtREFBQTs7Y0FDRSxLQUFDLENBQUEsZ0JBQUQsQ0FBa0Isa0JBQWxCLEVBQXNDLElBQXRDO0FBREY7bUJBR0EsUUFBQSxDQUFTLElBQVQsRUFBZSxpQkFBZixFQWhCRjtXQUFBLE1BQUE7WUFrQkUsS0FBQSxHQUFRLElBQUksS0FBSixDQUFVLFlBQVY7WUFDUixLQUFLLENBQUMsTUFBTixHQUFlO1lBQ2YsS0FBSyxDQUFDLE1BQU4sR0FBZTttQkFDZixRQUFBLENBQVMsS0FBVCxFQXJCRjs7UUFENkI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWxCO2FBd0JiLG1CQUFBLENBQW9CLFVBQXBCLEVBQWdDLFlBQWhDLEVBQThDLFFBQTlDO0lBcENZOzs2QkFzQ2Qsd0JBQUEsR0FBMEIsU0FBQTtBQUN4QixVQUFBO3FGQUFnRDtJQUR4Qjs7NkJBRzFCLGtCQUFBLEdBQW9CLFNBQUE7YUFDbEIsSUFBQyxDQUFBLFFBQVEsQ0FBQyxZQUFWLEdBQ0U7UUFBQSxLQUFBLEVBQU8sSUFBUDtRQUNBLE1BQUEsRUFBUSxDQURSOztJQUZnQjs7NkJBS3BCLFdBQUEsR0FBYSxTQUFDLFdBQUQsRUFBYyxRQUFkO0FBQ1gsVUFBQTtNQUFBLElBQUEsR0FBTyxDQUFDLE1BQUQsRUFBUyxXQUFULEVBQXNCLFFBQXRCO01BQ1AsWUFBQSxHQUFlLG9CQUFBLEdBQXFCLFdBQXJCLEdBQWlDO01BRWhELFVBQUEsR0FBYSxJQUFDLENBQUEsVUFBRCxDQUFZLElBQVosRUFBa0IsU0FBQyxJQUFELEVBQU8sTUFBUCxFQUFlLE1BQWY7QUFDN0IsWUFBQTtRQUFBLElBQUcsSUFBQSxLQUFRLENBQVg7QUFDRTtZQUNFLFFBQUEsZ0RBQWdDLEdBRGxDO1dBQUEsY0FBQTtZQUVNO1lBQ0osS0FBQSxHQUFRLG9CQUFBLENBQXFCLFlBQXJCLEVBQW1DLFVBQW5DLEVBQStDLE1BQS9DO0FBQ1IsbUJBQU8sUUFBQSxDQUFTLEtBQVQsRUFKVDs7aUJBTUEsUUFBQSxDQUFTLElBQVQsRUFBZSxRQUFmLEVBUEY7U0FBQSxNQUFBO1VBU0UsS0FBQSxHQUFRLElBQUksS0FBSixDQUFVLFlBQVY7VUFDUixLQUFLLENBQUMsTUFBTixHQUFlO1VBQ2YsS0FBSyxDQUFDLE1BQU4sR0FBZTtpQkFDZixRQUFBLENBQVMsS0FBVCxFQVpGOztNQUQ2QixDQUFsQjthQWViLG1CQUFBLENBQW9CLFVBQXBCLEVBQWdDLFlBQWhDLEVBQThDLFFBQTlDO0lBbkJXOzs2QkFxQmIsNEJBQUEsR0FBOEIsU0FBQyxXQUFELEVBQWMsUUFBZDtBQUM1QixVQUFBO01BQUEsSUFBQSxHQUFPLENBQUMsTUFBRCxFQUFTLFdBQVQsRUFBc0IsUUFBdEIsRUFBZ0MsY0FBaEMsRUFBZ0QsSUFBQyxDQUFBLGdCQUFELENBQWtCLElBQUksQ0FBQyxVQUFMLENBQUEsQ0FBbEIsQ0FBaEQ7TUFDUCxZQUFBLEdBQWUsb0JBQUEsR0FBcUIsV0FBckIsR0FBaUM7TUFFaEQsVUFBQSxHQUFhLElBQUMsQ0FBQSxVQUFELENBQVksSUFBWixFQUFrQixTQUFDLElBQUQsRUFBTyxNQUFQLEVBQWUsTUFBZjtBQUM3QixZQUFBO1FBQUEsSUFBRyxJQUFBLEtBQVEsQ0FBWDtBQUNFO1lBQ0UsUUFBQSxnREFBZ0MsR0FEbEM7V0FBQSxjQUFBO1lBRU07WUFDSixLQUFBLEdBQVEsb0JBQUEsQ0FBcUIsWUFBckIsRUFBbUMsVUFBbkMsRUFBK0MsTUFBL0M7QUFDUixtQkFBTyxRQUFBLENBQVMsS0FBVCxFQUpUOztpQkFNQSxRQUFBLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFQRjtTQUFBLE1BQUE7VUFTRSxLQUFBLEdBQVEsSUFBSSxLQUFKLENBQVUsWUFBVjtVQUNSLEtBQUssQ0FBQyxNQUFOLEdBQWU7VUFDZixLQUFLLENBQUMsTUFBTixHQUFlO2lCQUNmLFFBQUEsQ0FBUyxLQUFULEVBWkY7O01BRDZCLENBQWxCO2FBZWIsbUJBQUEsQ0FBb0IsVUFBcEIsRUFBZ0MsWUFBaEMsRUFBOEMsUUFBOUM7SUFuQjRCOzs2QkFxQjlCLFlBQUEsR0FBYyxTQUFBO2FBQ1osSUFBSSxPQUFKLENBQVksQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLE9BQUQsRUFBVSxNQUFWO2lCQUNWLEtBQUMsQ0FBQSxhQUFELENBQWUsU0FBQyxLQUFELEVBQVEsTUFBUjtZQUNiLElBQUcsS0FBSDtxQkFDRSxNQUFBLENBQU8sS0FBUCxFQURGO2FBQUEsTUFBQTtxQkFHRSxPQUFBLENBQVEsTUFBUixFQUhGOztVQURhLENBQWY7UUFEVTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWjtJQURZOzs2QkFRZCxXQUFBLEdBQWEsU0FBQyxVQUFEO2FBQ1gsSUFBSSxPQUFKLENBQVksQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLE9BQUQsRUFBVSxNQUFWO2lCQUNWLEtBQUMsQ0FBQSxZQUFELENBQWMsQ0FBQyxDQUFDLFVBQWhCLEVBQTRCLFNBQUMsS0FBRCxFQUFRLE1BQVI7WUFDMUIsSUFBRyxLQUFIO3FCQUNFLE1BQUEsQ0FBTyxLQUFQLEVBREY7YUFBQSxNQUFBO3FCQUdFLE9BQUEsQ0FBUSxNQUFSLEVBSEY7O1VBRDBCLENBQTVCO1FBRFU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVo7SUFEVzs7NkJBUWIsV0FBQSxHQUFhLFNBQUMsVUFBRDs7UUFBQyxhQUFhOzthQUN6QixJQUFJLE9BQUosQ0FBWSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsT0FBRCxFQUFVLE1BQVY7aUJBQ1YsS0FBQyxDQUFBLFlBQUQsQ0FBYyxVQUFkLEVBQTBCLFNBQUMsS0FBRCxFQUFRLE1BQVI7WUFDeEIsSUFBRyxLQUFIO3FCQUNFLE1BQUEsQ0FBTyxLQUFQLEVBREY7YUFBQSxNQUFBO3FCQUdFLE9BQUEsQ0FBUSxNQUFSLEVBSEY7O1VBRHdCLENBQTFCO1FBRFU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVo7SUFEVzs7NkJBUWIsVUFBQSxHQUFZLFNBQUMsV0FBRDtBQUNWLFVBQUE7c0VBQWlCLENBQUEsV0FBQSxRQUFBLENBQUEsV0FBQSxJQUFnQixJQUFJLE9BQUosQ0FBWSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsT0FBRCxFQUFVLE1BQVY7aUJBQzNDLEtBQUMsQ0FBQSxXQUFELENBQWEsV0FBYixFQUEwQixTQUFDLEtBQUQsRUFBUSxNQUFSO1lBQ3hCLElBQUcsS0FBSDtxQkFDRSxNQUFBLENBQU8sS0FBUCxFQURGO2FBQUEsTUFBQTtxQkFHRSxPQUFBLENBQVEsTUFBUixFQUhGOztVQUR3QixDQUExQjtRQUQyQztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWjtJQUR2Qjs7NkJBUVosZ0JBQUEsR0FBa0IsU0FBQyxPQUFELEVBQVUsUUFBVjtBQUNoQixVQUFBO01BQUEsTUFBQSxvRkFBa0M7TUFDbEMsSUFBQSxDQUFvQixNQUFNLENBQUMsVUFBUCxDQUFrQixNQUFsQixDQUFwQjtBQUFBLGVBQU8sTUFBUDs7QUFDQSxhQUFPLE1BQU0sQ0FBQyxTQUFQLENBQWlCLE9BQWpCLEVBQTBCLE1BQTFCO0lBSFM7OzZCQUtsQixnQkFBQSxHQUFrQixTQUFDLE9BQUQ7TUFDaEIsSUFBa0MsT0FBTyxPQUFQLEtBQWtCLFFBQXBEO1FBQUMsVUFBVyxPQUFPLENBQUMsS0FBUixDQUFjLEdBQWQsS0FBWjs7YUFDQTtJQUZnQjs7NkJBSWxCLE1BQUEsR0FBUSxTQUFDLElBQUQsRUFBTyxVQUFQLEVBQW1CLFFBQW5CO0FBQ04sVUFBQTtNQUFDLGdCQUFELEVBQU8sa0JBQVAsRUFBYztNQUVkLFlBQUEsR0FBa0IsVUFBSCxHQUNiLG9CQUFBLEdBQXFCLElBQXJCLEdBQTBCLEdBQTFCLEdBQTZCLFVBQTdCLEdBQXdDLGdCQUQzQixHQUdiO01BQ0YsT0FBQSxHQUFVLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxLQUFEO1VBQ1IsS0FBSyxDQUFDLG1CQUFOLEdBQTRCLENBQUk7VUFDaEMsS0FBQyxDQUFBLGdCQUFELENBQWtCLGVBQWxCLEVBQW1DLElBQW5DLEVBQXlDLEtBQXpDO2tEQUNBLFNBQVU7UUFIRjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFLVixnQ0FBRyxnQkFBZ0IsQ0FBRSxjQUFsQixLQUEwQixLQUE3QjtRQUNFLElBQUEsR0FBTyxDQUFDLFNBQUQsRUFBWSxnQkFBZ0IsQ0FBQyxNQUE3QixFQURUO09BQUEsTUFBQTtRQUdFLElBQUEsR0FBTyxDQUFDLFNBQUQsRUFBZSxJQUFELEdBQU0sR0FBTixHQUFTLFVBQXZCLEVBSFQ7O01BS0EsSUFBQSxHQUFPLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxJQUFELEVBQU8sTUFBUCxFQUFlLE1BQWY7QUFDTCxjQUFBO1VBQUEsSUFBRyxJQUFBLEtBQVEsQ0FBWDtZQUNFLEtBQUMsQ0FBQSxrQkFBRCxDQUFBOztjQUNBOzttQkFDQSxLQUFDLENBQUEsZ0JBQUQsQ0FBa0IsU0FBbEIsRUFBNkIsSUFBN0IsRUFIRjtXQUFBLE1BQUE7WUFLRSxLQUFBLEdBQVEsSUFBSSxLQUFKLENBQVUsWUFBVjtZQUNSLEtBQUssQ0FBQyxNQUFOLEdBQWU7WUFDZixLQUFLLENBQUMsTUFBTixHQUFlO21CQUNmLE9BQUEsQ0FBUSxLQUFSLEVBUkY7O1FBREs7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBO01BV1AsSUFBQyxDQUFBLGdCQUFELENBQWtCLFVBQWxCLEVBQThCLElBQTlCO01BQ0EsVUFBQSxHQUFhLElBQUMsQ0FBQSxVQUFELENBQVksSUFBWixFQUFrQixJQUFsQjthQUNiLG1CQUFBLENBQW9CLFVBQXBCLEVBQWdDLFlBQWhDLEVBQThDLE9BQTlDO0lBOUJNOzs2QkFnQ1IsTUFBQSxHQUFRLFNBQUMsSUFBRDtNQUNOLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFkLENBQThCLElBQTlCLENBQUg7UUFDRSxJQUF5QyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWQsQ0FBOEIsSUFBOUIsQ0FBekM7VUFBQSxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFkLENBQWdDLElBQWhDLEVBQUE7O2VBQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFkLENBQTRCLElBQTVCLEVBRkY7O0lBRE07OzZCQUtSLE9BQUEsR0FBUyxTQUFDLElBQUQsRUFBTyxRQUFQO0FBQ1AsVUFBQTtNQUFDLGdCQUFELEVBQU8sc0JBQVAsRUFBZ0I7TUFDaEIsaUJBQUEsR0FBb0IsQ0FBSSxLQUFKLElBQWMsQ0FBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFkLENBQWdDLElBQWhDO01BQ3RDLGlCQUFBLEdBQW9CLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZCxDQUE4QixJQUE5QjtNQUNwQixlQUFBLEdBQXFCLGVBQUgsR0FBb0IsSUFBRCxHQUFNLEdBQU4sR0FBUyxPQUE1QixHQUEyQztNQUU3RCxJQUFDLENBQUEsTUFBRCxDQUFRLElBQVI7TUFDQSxJQUFBLEdBQU8sQ0FBQyxTQUFELEVBQVksZUFBWixFQUE2QixRQUE3QjtNQUVQLFlBQUEsR0FBZSxtQkFBQSxHQUFvQixlQUFwQixHQUFvQztNQUNuRCxPQUFBLEdBQVUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEtBQUQ7VUFDUixLQUFLLENBQUMsbUJBQU4sR0FBNEIsQ0FBSTtVQUNoQyxLQUFDLENBQUEsZ0JBQUQsQ0FBa0IsZ0JBQWxCLEVBQW9DLElBQXBDLEVBQTBDLEtBQTFDO2tEQUNBLFNBQVU7UUFIRjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFLVixJQUFBLEdBQU8sQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLElBQUQsRUFBTyxNQUFQLEVBQWUsTUFBZjtBQUNMLGNBQUE7VUFBQSxJQUFHLElBQUEsS0FBUSxDQUFYO0FBRUU7Y0FDRSxXQUFBLEdBQWMsSUFBSSxDQUFDLEtBQUwsQ0FBVyxNQUFYLENBQW1CLENBQUEsQ0FBQTtjQUNqQyxJQUFBLEdBQU8sQ0FBQyxDQUFDLE1BQUYsQ0FBUyxFQUFULEVBQWEsSUFBYixFQUFtQixXQUFXLENBQUMsUUFBL0I7Y0FDUCxJQUFBLEdBQU8sSUFBSSxDQUFDLEtBSGQ7YUFBQSxjQUFBO2NBSU0sYUFKTjs7WUFNQSxLQUFDLENBQUEsa0JBQUQsQ0FBQTtZQUNBLElBQUcsaUJBQUg7Y0FDRSxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWQsQ0FBOEIsSUFBOUIsRUFERjthQUFBLE1BQUE7Y0FHRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQWQsQ0FBMEIsSUFBMUIsRUFIRjs7O2NBS0E7O21CQUNBLEtBQUMsQ0FBQSxnQkFBRCxDQUFrQixXQUFsQixFQUErQixJQUEvQixFQWZGO1dBQUEsTUFBQTtZQWlCRSxJQUF1QyxpQkFBdkM7Y0FBQSxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWQsQ0FBOEIsSUFBOUIsRUFBQTs7WUFDQSxLQUFBLEdBQVEsSUFBSSxLQUFKLENBQVUsWUFBVjtZQUNSLEtBQUssQ0FBQyxNQUFOLEdBQWU7WUFDZixLQUFLLENBQUMsTUFBTixHQUFlO21CQUNmLE9BQUEsQ0FBUSxLQUFSLEVBckJGOztRQURLO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQTtNQXdCUCxJQUFDLENBQUEsZ0JBQUQsQ0FBa0IsWUFBbEIsRUFBZ0MsSUFBaEM7TUFDQSxVQUFBLEdBQWEsSUFBQyxDQUFBLFVBQUQsQ0FBWSxJQUFaLEVBQWtCLElBQWxCO2FBQ2IsbUJBQUEsQ0FBb0IsVUFBcEIsRUFBZ0MsWUFBaEMsRUFBOEMsT0FBOUM7SUF6Q087OzZCQTJDVCxTQUFBLEdBQVcsU0FBQyxJQUFELEVBQU8sUUFBUDtBQUNULFVBQUE7TUFBQyxPQUFRO01BRVQsSUFBeUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFkLENBQThCLElBQTlCLENBQXpDO1FBQUEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBZCxDQUFnQyxJQUFoQyxFQUFBOztNQUVBLFlBQUEsR0FBZSxxQkFBQSxHQUFzQixJQUF0QixHQUEyQjtNQUMxQyxPQUFBLEdBQVUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEtBQUQ7VUFDUixLQUFDLENBQUEsZ0JBQUQsQ0FBa0Isa0JBQWxCLEVBQXNDLElBQXRDLEVBQTRDLEtBQTVDO2tEQUNBLFNBQVU7UUFGRjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFJVixJQUFDLENBQUEsZ0JBQUQsQ0FBa0IsY0FBbEIsRUFBa0MsSUFBbEM7TUFDQSxVQUFBLEdBQWEsSUFBQyxDQUFBLFVBQUQsQ0FBWSxDQUFDLFdBQUQsRUFBYyxRQUFkLEVBQXdCLElBQXhCLENBQVosRUFBMkMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLElBQUQsRUFBTyxNQUFQLEVBQWUsTUFBZjtBQUN0RCxjQUFBO1VBQUEsSUFBRyxJQUFBLEtBQVEsQ0FBWDtZQUNFLEtBQUMsQ0FBQSxrQkFBRCxDQUFBO1lBQ0EsS0FBQyxDQUFBLE1BQUQsQ0FBUSxJQUFSO1lBQ0EsS0FBQyxDQUFBLHFDQUFELENBQXVDLElBQXZDOztjQUNBOzttQkFDQSxLQUFDLENBQUEsZ0JBQUQsQ0FBa0IsYUFBbEIsRUFBaUMsSUFBakMsRUFMRjtXQUFBLE1BQUE7WUFPRSxLQUFBLEdBQVEsSUFBSSxLQUFKLENBQVUsWUFBVjtZQUNSLEtBQUssQ0FBQyxNQUFOLEdBQWU7WUFDZixLQUFLLENBQUMsTUFBTixHQUFlO21CQUNmLE9BQUEsQ0FBUSxLQUFSLEVBVkY7O1FBRHNEO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzQzthQWFiLG1CQUFBLENBQW9CLFVBQXBCLEVBQWdDLFlBQWhDLEVBQThDLE9BQTlDO0lBeEJTOzs2QkEwQlgsVUFBQSxHQUFZLFNBQUMsZ0JBQUQsRUFBbUIsZ0JBQW5CO0FBQ1YsVUFBQTtNQUFBLElBQW9CLHdCQUFwQjtBQUFBLGVBQU8sTUFBUDs7TUFFQSxnQkFBQSxHQUFtQixnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7TUFDN0MsSUFBQSxDQUFvQixNQUFNLENBQUMsS0FBUCxDQUFhLGdCQUFiLENBQXBCO0FBQUEsZUFBTyxNQUFQOztNQUNBLElBQUEsQ0FBb0IsTUFBTSxDQUFDLEtBQVAsQ0FBYSxnQkFBYixDQUFwQjtBQUFBLGVBQU8sTUFBUDs7YUFFQSxNQUFNLENBQUMsRUFBUCxDQUFVLGdCQUFWLEVBQTRCLGdCQUE1QjtJQVBVOzs2QkFTWixlQUFBLEdBQWlCLFNBQUMsR0FBRDtBQUNmLFVBQUE7TUFEaUIsT0FBRDthQUNoQixDQUFDLENBQUMsV0FBRixDQUFjLENBQUMsQ0FBQyxXQUFGLENBQWMsSUFBZCxDQUFkO0lBRGU7OzZCQUdqQixnQkFBQSxHQUFrQixTQUFDLEdBQUQ7QUFDaEIsVUFBQTtNQURrQixXQUFEO01BQ2hCLGFBQWM7TUFDZixPQUFBLCtHQUF5QztNQUN6QyxJQUFHLE9BQU8sQ0FBQyxLQUFSLENBQWMsWUFBZCxDQUFIO1FBQ0UsUUFBQSxHQUFXLE9BQU8sQ0FBQyxLQUFSLENBQWMsR0FBZCxDQUFtQixDQUFBLENBQUE7UUFDOUIsT0FBQSxHQUFVLHFCQUFBLEdBQXNCLFNBRmxDOzthQUdBLE9BQU8sQ0FBQyxPQUFSLENBQWdCLFFBQWhCLEVBQTBCLEVBQTFCLENBQTZCLENBQUMsT0FBOUIsQ0FBc0MsTUFBdEMsRUFBOEMsRUFBOUMsQ0FBaUQsQ0FBQyxPQUFsRCxDQUEwRCxRQUExRCxFQUFvRSxFQUFwRTtJQU5nQjs7NkJBUWxCLG1CQUFBLEdBQXFCLFNBQUMsR0FBRDtBQUNuQixVQUFBO01BRHFCLFdBQUQ7TUFDbkIsT0FBUTtNQUNULElBQUcsT0FBTyxJQUFQLEtBQWUsUUFBbEI7UUFDRSxNQUFBLEdBQVMsS0FEWDtPQUFBLE1BQUE7UUFHRSxNQUFBLDJIQUFtQyxJQUFJLENBQUMsZ0JBQUwsQ0FBc0I7VUFBQyxVQUFBLFFBQUQ7U0FBdEIsQ0FBQSxHQUFvQztRQUN2RSxJQUFHLE1BQU0sQ0FBQyxRQUFQLENBQWdCLEdBQWhCLENBQUg7VUFDRSxNQUFBLEdBQVMsU0FBQSxHQUFZLE9BRHZCO1NBSkY7O2FBTUE7SUFSbUI7OzZCQVVyQixxQkFBQSxHQUF1QixTQUFBO2FBQ3JCLElBQUksT0FBSixDQUFZLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxPQUFELEVBQVUsTUFBVjtBQUNWLGNBQUE7VUFBQSxVQUFBLEdBQWEsS0FBQyxDQUFBLFVBQUQsQ0FBWSxDQUFDLFNBQUQsRUFBWSxTQUFaLENBQVosRUFBb0MsU0FBQyxJQUFELEVBQU8sTUFBUCxFQUFlLE1BQWY7WUFDL0MsSUFBRyxJQUFBLEtBQVEsQ0FBWDtxQkFDRSxPQUFBLENBQUEsRUFERjthQUFBLE1BQUE7cUJBR0UsTUFBQSxDQUFPLElBQUksS0FBSixDQUFBLENBQVAsRUFIRjs7VUFEK0MsQ0FBcEM7aUJBTWIsVUFBVSxDQUFDLGdCQUFYLENBQTRCLFNBQUMsR0FBRDtBQUMxQixnQkFBQTtZQUQ0QixtQkFBTztZQUNuQyxNQUFBLENBQUE7bUJBQ0EsTUFBQSxDQUFPLEtBQVA7VUFGMEIsQ0FBNUI7UUFQVTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWjtJQURxQjs7NkJBWXZCLHFDQUFBLEdBQXVDLFNBQUMsV0FBRDthQUNyQyxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQVosQ0FBNEIsdUJBQTVCLEVBQXFELFdBQXJEO0lBRHFDOzs2QkFhdkMsZ0JBQUEsR0FBa0IsU0FBQyxTQUFELEVBQVksSUFBWixFQUFrQixLQUFsQjtBQUNoQixVQUFBO01BQUEsS0FBQSw2RUFBa0MsQ0FBRTtNQUNwQyxTQUFBLEdBQWUsS0FBSCxHQUFjLFFBQUEsR0FBUyxTQUF2QixHQUF3QyxVQUFBLEdBQVc7YUFDL0QsSUFBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsU0FBZCxFQUF5QjtRQUFDLE1BQUEsSUFBRDtRQUFPLE9BQUEsS0FBUDtPQUF6QjtJQUhnQjs7NkJBS2xCLEVBQUEsR0FBSSxTQUFDLFNBQUQsRUFBWSxRQUFaO0FBQ0YsVUFBQTtNQUFBLGFBQUEsR0FBZ0IsSUFBSTtBQUNwQjtBQUFBLFdBQUEsc0NBQUE7O1FBQ0UsYUFBYSxDQUFDLEdBQWQsQ0FBa0IsSUFBQyxDQUFBLE9BQU8sQ0FBQyxFQUFULENBQVksUUFBWixFQUFzQixRQUF0QixDQUFsQjtBQURGO2FBRUE7SUFKRTs7Ozs7O0VBTU4sb0JBQUEsR0FBdUIsU0FBQyxPQUFELEVBQVUsVUFBVixFQUFzQixNQUF0QjtBQUNyQixRQUFBO0lBQUEsS0FBQSxHQUFRLElBQUksS0FBSixDQUFVLE9BQVY7SUFDUixLQUFLLENBQUMsTUFBTixHQUFlO0lBQ2YsS0FBSyxDQUFDLE1BQU4sR0FBa0IsVUFBVSxDQUFDLE9BQVosR0FBb0IsSUFBcEIsR0FBd0I7V0FDekM7RUFKcUI7O0VBTXZCLGtCQUFBLEdBQXFCLFNBQUMsT0FBRCxFQUFVLFlBQVY7QUFDbkIsUUFBQTtJQUFBLEtBQUEsR0FBUSxJQUFJLEtBQUosQ0FBVSxPQUFWO0lBQ1IsS0FBSyxDQUFDLE1BQU4sR0FBZTtJQUNmLEtBQUssQ0FBQyxNQUFOLEdBQWUsWUFBWSxDQUFDO1dBQzVCO0VBSm1COztFQU1yQixtQkFBQSxHQUFzQixTQUFDLFVBQUQsRUFBYSxPQUFiLEVBQXNCLFFBQXRCO1dBQ3BCLFVBQVUsQ0FBQyxnQkFBWCxDQUE0QixTQUFDLEdBQUQ7QUFDMUIsVUFBQTtNQUQ0QixtQkFBTztNQUNuQyxNQUFBLENBQUE7YUFDQSxRQUFBLENBQVMsa0JBQUEsQ0FBbUIsT0FBbkIsRUFBNEIsS0FBNUIsQ0FBVDtJQUYwQixDQUE1QjtFQURvQjtBQXpidEIiLCJzb3VyY2VzQ29udGVudCI6WyJfID0gcmVxdWlyZSAndW5kZXJzY29yZS1wbHVzJ1xue0J1ZmZlcmVkUHJvY2VzcywgQ29tcG9zaXRlRGlzcG9zYWJsZSwgRW1pdHRlcn0gPSByZXF1aXJlICdhdG9tJ1xuc2VtdmVyID0gcmVxdWlyZSAnc2VtdmVyJ1xuXG5DbGllbnQgPSByZXF1aXJlICcuL2F0b20taW8tY2xpZW50J1xuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBQYWNrYWdlTWFuYWdlclxuICAjIE1pbGxpc2Vjb25kIGV4cGlyeSBmb3IgY2FjaGVkIGxvYWRPdXRkYXRlZCwgZXRjLiB2YWx1ZXNcbiAgQ0FDSEVfRVhQSVJZOiAxMDAwKjYwKjEwXG5cbiAgY29uc3RydWN0b3I6IC0+XG4gICAgQHBhY2thZ2VQcm9taXNlcyA9IFtdXG4gICAgQGFwbUNhY2hlID1cbiAgICAgIGxvYWRPdXRkYXRlZDpcbiAgICAgICAgdmFsdWU6IG51bGxcbiAgICAgICAgZXhwaXJ5OiAwXG5cbiAgICBAZW1pdHRlciA9IG5ldyBFbWl0dGVyXG5cbiAgZ2V0Q2xpZW50OiAtPlxuICAgIEBjbGllbnQgPz0gbmV3IENsaWVudCh0aGlzKVxuXG4gIGlzUGFja2FnZUluc3RhbGxlZDogKHBhY2thZ2VOYW1lKSAtPlxuICAgIGlmIGF0b20ucGFja2FnZXMuaXNQYWNrYWdlTG9hZGVkKHBhY2thZ2VOYW1lKVxuICAgICAgdHJ1ZVxuICAgIGVsc2VcbiAgICAgIGF0b20ucGFja2FnZXMuZ2V0QXZhaWxhYmxlUGFja2FnZU5hbWVzKCkuaW5kZXhPZihwYWNrYWdlTmFtZSkgPiAtMVxuXG4gIHBhY2thZ2VIYXNTZXR0aW5nczogKHBhY2thZ2VOYW1lKSAtPlxuICAgIGdyYW1tYXJzID0gYXRvbS5ncmFtbWFycy5nZXRHcmFtbWFycygpID8gW11cbiAgICBmb3IgZ3JhbW1hciBpbiBncmFtbWFycyB3aGVuIGdyYW1tYXIucGF0aFxuICAgICAgcmV0dXJuIHRydWUgaWYgZ3JhbW1hci5wYWNrYWdlTmFtZSBpcyBwYWNrYWdlTmFtZVxuXG4gICAgcGFjayA9IGF0b20ucGFja2FnZXMuZ2V0TG9hZGVkUGFja2FnZShwYWNrYWdlTmFtZSlcbiAgICBwYWNrLmFjdGl2YXRlQ29uZmlnKCkgaWYgcGFjaz8gYW5kIG5vdCBhdG9tLnBhY2thZ2VzLmlzUGFja2FnZUFjdGl2ZShwYWNrYWdlTmFtZSlcbiAgICBzY2hlbWEgPSBhdG9tLmNvbmZpZy5nZXRTY2hlbWEocGFja2FnZU5hbWUpXG4gICAgc2NoZW1hPyBhbmQgKHNjaGVtYS50eXBlIGlzbnQgJ2FueScpXG5cbiAgc2V0UHJveHlTZXJ2ZXJzOiAoY2FsbGJhY2spID0+XG4gICAgc2Vzc2lvbiA9IGF0b20uZ2V0Q3VycmVudFdpbmRvdygpLndlYkNvbnRlbnRzLnNlc3Npb25cbiAgICBzZXNzaW9uLnJlc29sdmVQcm94eSAnaHR0cDovL2F0b20uaW8nLCAoaHR0cFByb3h5KSA9PlxuICAgICAgQGFwcGx5UHJveHlUb0VudignaHR0cF9wcm94eScsIGh0dHBQcm94eSlcbiAgICAgIHNlc3Npb24ucmVzb2x2ZVByb3h5ICdodHRwczovL3B1bHNhci1lZGl0LmRldicsIChodHRwc1Byb3h5KSA9PlxuICAgICAgICBAYXBwbHlQcm94eVRvRW52KCdodHRwc19wcm94eScsIGh0dHBzUHJveHkpXG4gICAgICAgIGNhbGxiYWNrKClcblxuICBzZXRQcm94eVNlcnZlcnNBc3luYzogKGNhbGxiYWNrKSA9PlxuICAgIGh0dHBQcm94eVByb21pc2UgPSBhdG9tLnJlc29sdmVQcm94eSgnaHR0cDovL2F0b20uaW8nKS50aGVuKChwcm94eSkgPT4gQGFwcGx5UHJveHlUb0VudignaHR0cF9wcm94eScsIHByb3h5KSlcbiAgICBodHRwc1Byb3h5UHJvbWlzZSA9IGF0b20ucmVzb2x2ZVByb3h5KCdodHRwczovL3B1bHNhci1lZGl0LmRldicpLnRoZW4oKHByb3h5KSA9PiBAYXBwbHlQcm94eVRvRW52KCdodHRwc19wcm94eScsIHByb3h5KSlcbiAgICBQcm9taXNlLmFsbChbaHR0cFByb3h5UHJvbWlzZSwgaHR0cHNQcm94eVByb21pc2VdKS50aGVuKGNhbGxiYWNrKVxuXG4gIGFwcGx5UHJveHlUb0VudjogKGVudk5hbWUsIHByb3h5KSAtPlxuICAgIGlmIHByb3h5P1xuICAgICAgcHJveHkgPSBwcm94eS5zcGxpdCgnICcpXG4gICAgICBzd2l0Y2ggcHJveHlbMF0udHJpbSgpLnRvVXBwZXJDYXNlKClcbiAgICAgICAgd2hlbiAnRElSRUNUJyB0aGVuIGRlbGV0ZSBwcm9jZXNzLmVudltlbnZOYW1lXVxuICAgICAgICB3aGVuICdQUk9YWScgIHRoZW4gcHJvY2Vzcy5lbnZbZW52TmFtZV0gPSAnaHR0cDovLycgKyBwcm94eVsxXVxuICAgIHJldHVyblxuXG4gIHJ1bkNvbW1hbmQ6IChhcmdzLCBjYWxsYmFjaykgLT5cbiAgICBjb21tYW5kID0gYXRvbS5wYWNrYWdlcy5nZXRBcG1QYXRoKClcbiAgICBvdXRwdXRMaW5lcyA9IFtdXG4gICAgc3Rkb3V0ID0gKGxpbmVzKSAtPiBvdXRwdXRMaW5lcy5wdXNoKGxpbmVzKVxuICAgIGVycm9yTGluZXMgPSBbXVxuICAgIHN0ZGVyciA9IChsaW5lcykgLT4gZXJyb3JMaW5lcy5wdXNoKGxpbmVzKVxuICAgIGV4aXQgPSAoY29kZSkgLT5cbiAgICAgIGNhbGxiYWNrKGNvZGUsIG91dHB1dExpbmVzLmpvaW4oJ1xcbicpLCBlcnJvckxpbmVzLmpvaW4oJ1xcbicpKVxuXG4gICAgYXJncy5wdXNoKCctLW5vLWNvbG9yJylcblxuICAgIGlmIGF0b20uY29uZmlnLmdldCgnY29yZS51c2VQcm94eVNldHRpbmdzV2hlbkNhbGxpbmdBcG0nKVxuICAgICAgYnVmZmVyZWRQcm9jZXNzID0gbmV3IEJ1ZmZlcmVkUHJvY2Vzcyh7Y29tbWFuZCwgYXJncywgc3Rkb3V0LCBzdGRlcnIsIGV4aXQsIGF1dG9TdGFydDogZmFsc2V9KVxuICAgICAgaWYgYXRvbS5yZXNvbHZlUHJveHk/XG4gICAgICAgIEBzZXRQcm94eVNlcnZlcnNBc3luYyAtPiBidWZmZXJlZFByb2Nlc3Muc3RhcnQoKVxuICAgICAgZWxzZVxuICAgICAgICBAc2V0UHJveHlTZXJ2ZXJzIC0+IGJ1ZmZlcmVkUHJvY2Vzcy5zdGFydCgpXG4gICAgICByZXR1cm4gYnVmZmVyZWRQcm9jZXNzXG4gICAgZWxzZVxuICAgICAgcmV0dXJuIG5ldyBCdWZmZXJlZFByb2Nlc3Moe2NvbW1hbmQsIGFyZ3MsIHN0ZG91dCwgc3RkZXJyLCBleGl0fSlcblxuICBsb2FkSW5zdGFsbGVkOiAoY2FsbGJhY2spIC0+XG4gICAgYXJncyA9IFsnbHMnLCAnLS1qc29uJ11cbiAgICBlcnJvck1lc3NhZ2UgPSAnRmV0Y2hpbmcgbG9jYWwgcGFja2FnZXMgZmFpbGVkLidcbiAgICBhcG1Qcm9jZXNzID0gQHJ1bkNvbW1hbmQgYXJncywgKGNvZGUsIHN0ZG91dCwgc3RkZXJyKSAtPlxuICAgICAgaWYgY29kZSBpcyAwXG4gICAgICAgIHRyeVxuICAgICAgICAgIHBhY2thZ2VzID0gSlNPTi5wYXJzZShzdGRvdXQpID8gW11cbiAgICAgICAgY2F0Y2ggcGFyc2VFcnJvclxuICAgICAgICAgIGVycm9yID0gY3JlYXRlSnNvblBhcnNlRXJyb3IoZXJyb3JNZXNzYWdlLCBwYXJzZUVycm9yLCBzdGRvdXQpXG4gICAgICAgICAgcmV0dXJuIGNhbGxiYWNrKGVycm9yKVxuICAgICAgICBjYWxsYmFjayhudWxsLCBwYWNrYWdlcylcbiAgICAgIGVsc2VcbiAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoZXJyb3JNZXNzYWdlKVxuICAgICAgICBlcnJvci5zdGRvdXQgPSBzdGRvdXRcbiAgICAgICAgZXJyb3Iuc3RkZXJyID0gc3RkZXJyXG4gICAgICAgIGNhbGxiYWNrKGVycm9yKVxuXG4gICAgaGFuZGxlUHJvY2Vzc0Vycm9ycyhhcG1Qcm9jZXNzLCBlcnJvck1lc3NhZ2UsIGNhbGxiYWNrKVxuXG4gIGxvYWRGZWF0dXJlZDogKGxvYWRUaGVtZXMsIGNhbGxiYWNrKSAtPlxuICAgIHVubGVzcyBjYWxsYmFja1xuICAgICAgY2FsbGJhY2sgPSBsb2FkVGhlbWVzXG4gICAgICBsb2FkVGhlbWVzID0gZmFsc2VcblxuICAgIGFyZ3MgPSBbJ2ZlYXR1cmVkJywgJy0tanNvbiddXG4gICAgdmVyc2lvbiA9IGF0b20uZ2V0VmVyc2lvbigpXG4gICAgYXJncy5wdXNoKCctLXRoZW1lcycpIGlmIGxvYWRUaGVtZXNcbiAgICBhcmdzLnB1c2goJy0tY29tcGF0aWJsZScsIHZlcnNpb24pIGlmIHNlbXZlci52YWxpZCh2ZXJzaW9uKVxuICAgIGVycm9yTWVzc2FnZSA9ICdGZXRjaGluZyBmZWF0dXJlZCBwYWNrYWdlcyBmYWlsZWQuJ1xuXG4gICAgYXBtUHJvY2VzcyA9IEBydW5Db21tYW5kIGFyZ3MsIChjb2RlLCBzdGRvdXQsIHN0ZGVycikgLT5cbiAgICAgIGlmIGNvZGUgaXMgMFxuICAgICAgICB0cnlcbiAgICAgICAgICBwYWNrYWdlcyA9IEpTT04ucGFyc2Uoc3Rkb3V0KSA/IFtdXG4gICAgICAgIGNhdGNoIHBhcnNlRXJyb3JcbiAgICAgICAgICBlcnJvciA9IGNyZWF0ZUpzb25QYXJzZUVycm9yKGVycm9yTWVzc2FnZSwgcGFyc2VFcnJvciwgc3Rkb3V0KVxuICAgICAgICAgIHJldHVybiBjYWxsYmFjayhlcnJvcilcblxuICAgICAgICBjYWxsYmFjayhudWxsLCBwYWNrYWdlcylcbiAgICAgIGVsc2VcbiAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoZXJyb3JNZXNzYWdlKVxuICAgICAgICBlcnJvci5zdGRvdXQgPSBzdGRvdXRcbiAgICAgICAgZXJyb3Iuc3RkZXJyID0gc3RkZXJyXG4gICAgICAgIGNhbGxiYWNrKGVycm9yKVxuXG4gICAgaGFuZGxlUHJvY2Vzc0Vycm9ycyhhcG1Qcm9jZXNzLCBlcnJvck1lc3NhZ2UsIGNhbGxiYWNrKVxuXG4gIGxvYWRPdXRkYXRlZDogKGNsZWFyQ2FjaGUsIGNhbGxiYWNrKSAtPlxuICAgIGlmIGNsZWFyQ2FjaGVcbiAgICAgIEBjbGVhck91dGRhdGVkQ2FjaGUoKVxuICAgICMgU2hvcnQgY2lyY3VpdCBpZiB3ZSBoYXZlIGNhY2hlZCBkYXRhLlxuICAgIGVsc2UgaWYgQGFwbUNhY2hlLmxvYWRPdXRkYXRlZC52YWx1ZSBhbmQgQGFwbUNhY2hlLmxvYWRPdXRkYXRlZC5leHBpcnkgPiBEYXRlLm5vdygpXG4gICAgICByZXR1cm4gY2FsbGJhY2sobnVsbCwgQGFwbUNhY2hlLmxvYWRPdXRkYXRlZC52YWx1ZSlcblxuICAgIGFyZ3MgPSBbJ291dGRhdGVkJywgJy0tanNvbiddXG4gICAgdmVyc2lvbiA9IGF0b20uZ2V0VmVyc2lvbigpXG4gICAgYXJncy5wdXNoKCctLWNvbXBhdGlibGUnLCB2ZXJzaW9uKSBpZiBzZW12ZXIudmFsaWQodmVyc2lvbilcbiAgICBlcnJvck1lc3NhZ2UgPSAnRmV0Y2hpbmcgb3V0ZGF0ZWQgcGFja2FnZXMgYW5kIHRoZW1lcyBmYWlsZWQuJ1xuXG4gICAgYXBtUHJvY2VzcyA9IEBydW5Db21tYW5kIGFyZ3MsIChjb2RlLCBzdGRvdXQsIHN0ZGVycikgPT5cbiAgICAgIGlmIGNvZGUgaXMgMFxuICAgICAgICB0cnlcbiAgICAgICAgICBwYWNrYWdlcyA9IEpTT04ucGFyc2Uoc3Rkb3V0KSA/IFtdXG4gICAgICAgIGNhdGNoIHBhcnNlRXJyb3JcbiAgICAgICAgICBlcnJvciA9IGNyZWF0ZUpzb25QYXJzZUVycm9yKGVycm9yTWVzc2FnZSwgcGFyc2VFcnJvciwgc3Rkb3V0KVxuICAgICAgICAgIHJldHVybiBjYWxsYmFjayhlcnJvcilcblxuICAgICAgICB1cGRhdGFibGVQYWNrYWdlcyA9IChwYWNrIGZvciBwYWNrIGluIHBhY2thZ2VzIHdoZW4gbm90IEBnZXRWZXJzaW9uUGlubmVkUGFja2FnZXMoKS5pbmNsdWRlcyhwYWNrPy5uYW1lKSlcblxuICAgICAgICBAYXBtQ2FjaGUubG9hZE91dGRhdGVkID1cbiAgICAgICAgICB2YWx1ZTogdXBkYXRhYmxlUGFja2FnZXNcbiAgICAgICAgICBleHBpcnk6IERhdGUubm93KCkgKyBAQ0FDSEVfRVhQSVJZXG5cbiAgICAgICAgZm9yIHBhY2sgaW4gdXBkYXRhYmxlUGFja2FnZXNcbiAgICAgICAgICBAZW1pdFBhY2thZ2VFdmVudCAndXBkYXRlLWF2YWlsYWJsZScsIHBhY2tcblxuICAgICAgICBjYWxsYmFjayhudWxsLCB1cGRhdGFibGVQYWNrYWdlcylcbiAgICAgIGVsc2VcbiAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoZXJyb3JNZXNzYWdlKVxuICAgICAgICBlcnJvci5zdGRvdXQgPSBzdGRvdXRcbiAgICAgICAgZXJyb3Iuc3RkZXJyID0gc3RkZXJyXG4gICAgICAgIGNhbGxiYWNrKGVycm9yKVxuXG4gICAgaGFuZGxlUHJvY2Vzc0Vycm9ycyhhcG1Qcm9jZXNzLCBlcnJvck1lc3NhZ2UsIGNhbGxiYWNrKVxuXG4gIGdldFZlcnNpb25QaW5uZWRQYWNrYWdlczogLT5cbiAgICBhdG9tLmNvbmZpZy5nZXQoJ2NvcmUudmVyc2lvblBpbm5lZFBhY2thZ2VzJykgPyBbXVxuXG4gIGNsZWFyT3V0ZGF0ZWRDYWNoZTogLT5cbiAgICBAYXBtQ2FjaGUubG9hZE91dGRhdGVkID1cbiAgICAgIHZhbHVlOiBudWxsXG4gICAgICBleHBpcnk6IDBcblxuICBsb2FkUGFja2FnZTogKHBhY2thZ2VOYW1lLCBjYWxsYmFjaykgLT5cbiAgICBhcmdzID0gWyd2aWV3JywgcGFja2FnZU5hbWUsICctLWpzb24nXVxuICAgIGVycm9yTWVzc2FnZSA9IFwiRmV0Y2hpbmcgcGFja2FnZSAnI3twYWNrYWdlTmFtZX0nIGZhaWxlZC5cIlxuXG4gICAgYXBtUHJvY2VzcyA9IEBydW5Db21tYW5kIGFyZ3MsIChjb2RlLCBzdGRvdXQsIHN0ZGVycikgLT5cbiAgICAgIGlmIGNvZGUgaXMgMFxuICAgICAgICB0cnlcbiAgICAgICAgICBwYWNrYWdlcyA9IEpTT04ucGFyc2Uoc3Rkb3V0KSA/IFtdXG4gICAgICAgIGNhdGNoIHBhcnNlRXJyb3JcbiAgICAgICAgICBlcnJvciA9IGNyZWF0ZUpzb25QYXJzZUVycm9yKGVycm9yTWVzc2FnZSwgcGFyc2VFcnJvciwgc3Rkb3V0KVxuICAgICAgICAgIHJldHVybiBjYWxsYmFjayhlcnJvcilcblxuICAgICAgICBjYWxsYmFjayhudWxsLCBwYWNrYWdlcylcbiAgICAgIGVsc2VcbiAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoZXJyb3JNZXNzYWdlKVxuICAgICAgICBlcnJvci5zdGRvdXQgPSBzdGRvdXRcbiAgICAgICAgZXJyb3Iuc3RkZXJyID0gc3RkZXJyXG4gICAgICAgIGNhbGxiYWNrKGVycm9yKVxuXG4gICAgaGFuZGxlUHJvY2Vzc0Vycm9ycyhhcG1Qcm9jZXNzLCBlcnJvck1lc3NhZ2UsIGNhbGxiYWNrKVxuXG4gIGxvYWRDb21wYXRpYmxlUGFja2FnZVZlcnNpb246IChwYWNrYWdlTmFtZSwgY2FsbGJhY2spIC0+XG4gICAgYXJncyA9IFsndmlldycsIHBhY2thZ2VOYW1lLCAnLS1qc29uJywgJy0tY29tcGF0aWJsZScsIEBub3JtYWxpemVWZXJzaW9uKGF0b20uZ2V0VmVyc2lvbigpKV1cbiAgICBlcnJvck1lc3NhZ2UgPSBcIkZldGNoaW5nIHBhY2thZ2UgJyN7cGFja2FnZU5hbWV9JyBmYWlsZWQuXCJcblxuICAgIGFwbVByb2Nlc3MgPSBAcnVuQ29tbWFuZCBhcmdzLCAoY29kZSwgc3Rkb3V0LCBzdGRlcnIpIC0+XG4gICAgICBpZiBjb2RlIGlzIDBcbiAgICAgICAgdHJ5XG4gICAgICAgICAgcGFja2FnZXMgPSBKU09OLnBhcnNlKHN0ZG91dCkgPyBbXVxuICAgICAgICBjYXRjaCBwYXJzZUVycm9yXG4gICAgICAgICAgZXJyb3IgPSBjcmVhdGVKc29uUGFyc2VFcnJvcihlcnJvck1lc3NhZ2UsIHBhcnNlRXJyb3IsIHN0ZG91dClcbiAgICAgICAgICByZXR1cm4gY2FsbGJhY2soZXJyb3IpXG5cbiAgICAgICAgY2FsbGJhY2sobnVsbCwgcGFja2FnZXMpXG4gICAgICBlbHNlXG4gICAgICAgIGVycm9yID0gbmV3IEVycm9yKGVycm9yTWVzc2FnZSlcbiAgICAgICAgZXJyb3Iuc3Rkb3V0ID0gc3Rkb3V0XG4gICAgICAgIGVycm9yLnN0ZGVyciA9IHN0ZGVyclxuICAgICAgICBjYWxsYmFjayhlcnJvcilcblxuICAgIGhhbmRsZVByb2Nlc3NFcnJvcnMoYXBtUHJvY2VzcywgZXJyb3JNZXNzYWdlLCBjYWxsYmFjaylcblxuICBnZXRJbnN0YWxsZWQ6IC0+XG4gICAgbmV3IFByb21pc2UgKHJlc29sdmUsIHJlamVjdCkgPT5cbiAgICAgIEBsb2FkSW5zdGFsbGVkIChlcnJvciwgcmVzdWx0KSAtPlxuICAgICAgICBpZiBlcnJvclxuICAgICAgICAgIHJlamVjdChlcnJvcilcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHJlc29sdmUocmVzdWx0KVxuXG4gIGdldEZlYXR1cmVkOiAobG9hZFRoZW1lcykgLT5cbiAgICBuZXcgUHJvbWlzZSAocmVzb2x2ZSwgcmVqZWN0KSA9PlxuICAgICAgQGxvYWRGZWF0dXJlZCAhIWxvYWRUaGVtZXMsIChlcnJvciwgcmVzdWx0KSAtPlxuICAgICAgICBpZiBlcnJvclxuICAgICAgICAgIHJlamVjdChlcnJvcilcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHJlc29sdmUocmVzdWx0KVxuXG4gIGdldE91dGRhdGVkOiAoY2xlYXJDYWNoZSA9IGZhbHNlKSAtPlxuICAgIG5ldyBQcm9taXNlIChyZXNvbHZlLCByZWplY3QpID0+XG4gICAgICBAbG9hZE91dGRhdGVkIGNsZWFyQ2FjaGUsIChlcnJvciwgcmVzdWx0KSAtPlxuICAgICAgICBpZiBlcnJvclxuICAgICAgICAgIHJlamVjdChlcnJvcilcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHJlc29sdmUocmVzdWx0KVxuXG4gIGdldFBhY2thZ2U6IChwYWNrYWdlTmFtZSkgLT5cbiAgICBAcGFja2FnZVByb21pc2VzW3BhY2thZ2VOYW1lXSA/PSBuZXcgUHJvbWlzZSAocmVzb2x2ZSwgcmVqZWN0KSA9PlxuICAgICAgQGxvYWRQYWNrYWdlIHBhY2thZ2VOYW1lLCAoZXJyb3IsIHJlc3VsdCkgLT5cbiAgICAgICAgaWYgZXJyb3JcbiAgICAgICAgICByZWplY3QoZXJyb3IpXG4gICAgICAgIGVsc2VcbiAgICAgICAgICByZXNvbHZlKHJlc3VsdClcblxuICBzYXRpc2ZpZXNWZXJzaW9uOiAodmVyc2lvbiwgbWV0YWRhdGEpIC0+XG4gICAgZW5naW5lID0gbWV0YWRhdGEuZW5naW5lcz8uYXRvbSA/ICcqJ1xuICAgIHJldHVybiBmYWxzZSB1bmxlc3Mgc2VtdmVyLnZhbGlkUmFuZ2UoZW5naW5lKVxuICAgIHJldHVybiBzZW12ZXIuc2F0aXNmaWVzKHZlcnNpb24sIGVuZ2luZSlcblxuICBub3JtYWxpemVWZXJzaW9uOiAodmVyc2lvbikgLT5cbiAgICBbdmVyc2lvbl0gPSB2ZXJzaW9uLnNwbGl0KCctJykgaWYgdHlwZW9mIHZlcnNpb24gaXMgJ3N0cmluZydcbiAgICB2ZXJzaW9uXG5cbiAgdXBkYXRlOiAocGFjaywgbmV3VmVyc2lvbiwgY2FsbGJhY2spIC0+XG4gICAge25hbWUsIHRoZW1lLCBhcG1JbnN0YWxsU291cmNlfSA9IHBhY2tcblxuICAgIGVycm9yTWVzc2FnZSA9IGlmIG5ld1ZlcnNpb25cbiAgICAgIFwiVXBkYXRpbmcgdG8gXFx1MjAxQyN7bmFtZX1AI3tuZXdWZXJzaW9ufVxcdTIwMUQgZmFpbGVkLlwiXG4gICAgZWxzZVxuICAgICAgXCJVcGRhdGluZyB0byBsYXRlc3Qgc2hhIGZhaWxlZC5cIlxuICAgIG9uRXJyb3IgPSAoZXJyb3IpID0+XG4gICAgICBlcnJvci5wYWNrYWdlSW5zdGFsbEVycm9yID0gbm90IHRoZW1lXG4gICAgICBAZW1pdFBhY2thZ2VFdmVudCAndXBkYXRlLWZhaWxlZCcsIHBhY2ssIGVycm9yXG4gICAgICBjYWxsYmFjaz8oZXJyb3IpXG5cbiAgICBpZiBhcG1JbnN0YWxsU291cmNlPy50eXBlIGlzICdnaXQnXG4gICAgICBhcmdzID0gWydpbnN0YWxsJywgYXBtSW5zdGFsbFNvdXJjZS5zb3VyY2VdXG4gICAgZWxzZVxuICAgICAgYXJncyA9IFsnaW5zdGFsbCcsIFwiI3tuYW1lfUAje25ld1ZlcnNpb259XCJdXG5cbiAgICBleGl0ID0gKGNvZGUsIHN0ZG91dCwgc3RkZXJyKSA9PlxuICAgICAgaWYgY29kZSBpcyAwXG4gICAgICAgIEBjbGVhck91dGRhdGVkQ2FjaGUoKVxuICAgICAgICBjYWxsYmFjaz8oKVxuICAgICAgICBAZW1pdFBhY2thZ2VFdmVudCAndXBkYXRlZCcsIHBhY2tcbiAgICAgIGVsc2VcbiAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoZXJyb3JNZXNzYWdlKVxuICAgICAgICBlcnJvci5zdGRvdXQgPSBzdGRvdXRcbiAgICAgICAgZXJyb3Iuc3RkZXJyID0gc3RkZXJyXG4gICAgICAgIG9uRXJyb3IoZXJyb3IpXG5cbiAgICBAZW1pdFBhY2thZ2VFdmVudCAndXBkYXRpbmcnLCBwYWNrXG4gICAgYXBtUHJvY2VzcyA9IEBydW5Db21tYW5kKGFyZ3MsIGV4aXQpXG4gICAgaGFuZGxlUHJvY2Vzc0Vycm9ycyhhcG1Qcm9jZXNzLCBlcnJvck1lc3NhZ2UsIG9uRXJyb3IpXG5cbiAgdW5sb2FkOiAobmFtZSkgLT5cbiAgICBpZiBhdG9tLnBhY2thZ2VzLmlzUGFja2FnZUxvYWRlZChuYW1lKVxuICAgICAgYXRvbS5wYWNrYWdlcy5kZWFjdGl2YXRlUGFja2FnZShuYW1lKSBpZiBhdG9tLnBhY2thZ2VzLmlzUGFja2FnZUFjdGl2ZShuYW1lKVxuICAgICAgYXRvbS5wYWNrYWdlcy51bmxvYWRQYWNrYWdlKG5hbWUpXG5cbiAgaW5zdGFsbDogKHBhY2ssIGNhbGxiYWNrKSAtPlxuICAgIHtuYW1lLCB2ZXJzaW9uLCB0aGVtZX0gPSBwYWNrXG4gICAgYWN0aXZhdGVPblN1Y2Nlc3MgPSBub3QgdGhlbWUgYW5kIG5vdCBhdG9tLnBhY2thZ2VzLmlzUGFja2FnZURpc2FibGVkKG5hbWUpXG4gICAgYWN0aXZhdGVPbkZhaWx1cmUgPSBhdG9tLnBhY2thZ2VzLmlzUGFja2FnZUFjdGl2ZShuYW1lKVxuICAgIG5hbWVXaXRoVmVyc2lvbiA9IGlmIHZlcnNpb24/IHRoZW4gXCIje25hbWV9QCN7dmVyc2lvbn1cIiBlbHNlIG5hbWVcblxuICAgIEB1bmxvYWQobmFtZSlcbiAgICBhcmdzID0gWydpbnN0YWxsJywgbmFtZVdpdGhWZXJzaW9uLCAnLS1qc29uJ11cblxuICAgIGVycm9yTWVzc2FnZSA9IFwiSW5zdGFsbGluZyBcXHUyMDFDI3tuYW1lV2l0aFZlcnNpb259XFx1MjAxRCBmYWlsZWQuXCJcbiAgICBvbkVycm9yID0gKGVycm9yKSA9PlxuICAgICAgZXJyb3IucGFja2FnZUluc3RhbGxFcnJvciA9IG5vdCB0aGVtZVxuICAgICAgQGVtaXRQYWNrYWdlRXZlbnQgJ2luc3RhbGwtZmFpbGVkJywgcGFjaywgZXJyb3JcbiAgICAgIGNhbGxiYWNrPyhlcnJvcilcblxuICAgIGV4aXQgPSAoY29kZSwgc3Rkb3V0LCBzdGRlcnIpID0+XG4gICAgICBpZiBjb2RlIGlzIDBcbiAgICAgICAgIyBnZXQgcmVhbCBwYWNrYWdlIG5hbWUgZnJvbSBwYWNrYWdlLmpzb25cbiAgICAgICAgdHJ5XG4gICAgICAgICAgcGFja2FnZUluZm8gPSBKU09OLnBhcnNlKHN0ZG91dClbMF1cbiAgICAgICAgICBwYWNrID0gXy5leHRlbmQoe30sIHBhY2ssIHBhY2thZ2VJbmZvLm1ldGFkYXRhKVxuICAgICAgICAgIG5hbWUgPSBwYWNrLm5hbWVcbiAgICAgICAgY2F0Y2ggZXJyXG4gICAgICAgICAgIyB1c2luZyBvbGQgYXBtIHdpdGhvdXQgLS1qc29uIHN1cHBvcnRcbiAgICAgICAgQGNsZWFyT3V0ZGF0ZWRDYWNoZSgpXG4gICAgICAgIGlmIGFjdGl2YXRlT25TdWNjZXNzXG4gICAgICAgICAgYXRvbS5wYWNrYWdlcy5hY3RpdmF0ZVBhY2thZ2UobmFtZSlcbiAgICAgICAgZWxzZVxuICAgICAgICAgIGF0b20ucGFja2FnZXMubG9hZFBhY2thZ2UobmFtZSlcblxuICAgICAgICBjYWxsYmFjaz8oKVxuICAgICAgICBAZW1pdFBhY2thZ2VFdmVudCAnaW5zdGFsbGVkJywgcGFja1xuICAgICAgZWxzZVxuICAgICAgICBhdG9tLnBhY2thZ2VzLmFjdGl2YXRlUGFja2FnZShuYW1lKSBpZiBhY3RpdmF0ZU9uRmFpbHVyZVxuICAgICAgICBlcnJvciA9IG5ldyBFcnJvcihlcnJvck1lc3NhZ2UpXG4gICAgICAgIGVycm9yLnN0ZG91dCA9IHN0ZG91dFxuICAgICAgICBlcnJvci5zdGRlcnIgPSBzdGRlcnJcbiAgICAgICAgb25FcnJvcihlcnJvcilcblxuICAgIEBlbWl0UGFja2FnZUV2ZW50KCdpbnN0YWxsaW5nJywgcGFjaylcbiAgICBhcG1Qcm9jZXNzID0gQHJ1bkNvbW1hbmQoYXJncywgZXhpdClcbiAgICBoYW5kbGVQcm9jZXNzRXJyb3JzKGFwbVByb2Nlc3MsIGVycm9yTWVzc2FnZSwgb25FcnJvcilcblxuICB1bmluc3RhbGw6IChwYWNrLCBjYWxsYmFjaykgLT5cbiAgICB7bmFtZX0gPSBwYWNrXG5cbiAgICBhdG9tLnBhY2thZ2VzLmRlYWN0aXZhdGVQYWNrYWdlKG5hbWUpIGlmIGF0b20ucGFja2FnZXMuaXNQYWNrYWdlQWN0aXZlKG5hbWUpXG5cbiAgICBlcnJvck1lc3NhZ2UgPSBcIlVuaW5zdGFsbGluZyBcXHUyMDFDI3tuYW1lfVxcdTIwMUQgZmFpbGVkLlwiXG4gICAgb25FcnJvciA9IChlcnJvcikgPT5cbiAgICAgIEBlbWl0UGFja2FnZUV2ZW50ICd1bmluc3RhbGwtZmFpbGVkJywgcGFjaywgZXJyb3JcbiAgICAgIGNhbGxiYWNrPyhlcnJvcilcblxuICAgIEBlbWl0UGFja2FnZUV2ZW50KCd1bmluc3RhbGxpbmcnLCBwYWNrKVxuICAgIGFwbVByb2Nlc3MgPSBAcnVuQ29tbWFuZCBbJ3VuaW5zdGFsbCcsICctLWhhcmQnLCBuYW1lXSwgKGNvZGUsIHN0ZG91dCwgc3RkZXJyKSA9PlxuICAgICAgaWYgY29kZSBpcyAwXG4gICAgICAgIEBjbGVhck91dGRhdGVkQ2FjaGUoKVxuICAgICAgICBAdW5sb2FkKG5hbWUpXG4gICAgICAgIEByZW1vdmVQYWNrYWdlTmFtZUZyb21EaXNhYmxlZFBhY2thZ2VzKG5hbWUpXG4gICAgICAgIGNhbGxiYWNrPygpXG4gICAgICAgIEBlbWl0UGFja2FnZUV2ZW50ICd1bmluc3RhbGxlZCcsIHBhY2tcbiAgICAgIGVsc2VcbiAgICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoZXJyb3JNZXNzYWdlKVxuICAgICAgICBlcnJvci5zdGRvdXQgPSBzdGRvdXRcbiAgICAgICAgZXJyb3Iuc3RkZXJyID0gc3RkZXJyXG4gICAgICAgIG9uRXJyb3IoZXJyb3IpXG5cbiAgICBoYW5kbGVQcm9jZXNzRXJyb3JzKGFwbVByb2Nlc3MsIGVycm9yTWVzc2FnZSwgb25FcnJvcilcblxuICBjYW5VcGdyYWRlOiAoaW5zdGFsbGVkUGFja2FnZSwgYXZhaWxhYmxlVmVyc2lvbikgLT5cbiAgICByZXR1cm4gZmFsc2UgdW5sZXNzIGluc3RhbGxlZFBhY2thZ2U/XG5cbiAgICBpbnN0YWxsZWRWZXJzaW9uID0gaW5zdGFsbGVkUGFja2FnZS5tZXRhZGF0YS52ZXJzaW9uXG4gICAgcmV0dXJuIGZhbHNlIHVubGVzcyBzZW12ZXIudmFsaWQoaW5zdGFsbGVkVmVyc2lvbilcbiAgICByZXR1cm4gZmFsc2UgdW5sZXNzIHNlbXZlci52YWxpZChhdmFpbGFibGVWZXJzaW9uKVxuXG4gICAgc2VtdmVyLmd0KGF2YWlsYWJsZVZlcnNpb24sIGluc3RhbGxlZFZlcnNpb24pXG5cbiAgZ2V0UGFja2FnZVRpdGxlOiAoe25hbWV9KSAtPlxuICAgIF8udW5kYXNoZXJpemUoXy51bmNhbWVsY2FzZShuYW1lKSlcblxuICBnZXRSZXBvc2l0b3J5VXJsOiAoe21ldGFkYXRhfSkgLT5cbiAgICB7cmVwb3NpdG9yeX0gPSBtZXRhZGF0YVxuICAgIHJlcG9VcmwgPSByZXBvc2l0b3J5Py51cmwgPyByZXBvc2l0b3J5ID8gJydcbiAgICBpZiByZXBvVXJsLm1hdGNoICdnaXRAZ2l0aHViJ1xuICAgICAgcmVwb05hbWUgPSByZXBvVXJsLnNwbGl0KCc6JylbMV1cbiAgICAgIHJlcG9VcmwgPSBcImh0dHBzOi8vZ2l0aHViLmNvbS8je3JlcG9OYW1lfVwiXG4gICAgcmVwb1VybC5yZXBsYWNlKC9cXC5naXQkLywgJycpLnJlcGxhY2UoL1xcLyskLywgJycpLnJlcGxhY2UoL15naXRcXCsvLCAnJylcblxuICBnZXRSZXBvc2l0b3J5QnVnVXJpOiAoe21ldGFkYXRhfSkgLT5cbiAgICB7YnVnc30gPSBtZXRhZGF0YVxuICAgIGlmIHR5cGVvZiBidWdzIGlzICdzdHJpbmcnXG4gICAgICBidWdVcmkgPSBidWdzXG4gICAgZWxzZVxuICAgICAgYnVnVXJpID0gYnVncz8udXJsID8gYnVncz8uZW1haWwgPyB0aGlzLmdldFJlcG9zaXRvcnlVcmwoe21ldGFkYXRhfSkgKyAnL2lzc3Vlcy9uZXcnXG4gICAgICBpZiBidWdVcmkuaW5jbHVkZXMoJ0AnKVxuICAgICAgICBidWdVcmkgPSAnbWFpbHRvOicgKyBidWdVcmlcbiAgICBidWdVcmlcblxuICBjaGVja05hdGl2ZUJ1aWxkVG9vbHM6IC0+XG4gICAgbmV3IFByb21pc2UgKHJlc29sdmUsIHJlamVjdCkgPT5cbiAgICAgIGFwbVByb2Nlc3MgPSBAcnVuQ29tbWFuZCBbJ2luc3RhbGwnLCAnLS1jaGVjayddLCAoY29kZSwgc3Rkb3V0LCBzdGRlcnIpIC0+XG4gICAgICAgIGlmIGNvZGUgaXMgMFxuICAgICAgICAgIHJlc29sdmUoKVxuICAgICAgICBlbHNlXG4gICAgICAgICAgcmVqZWN0KG5ldyBFcnJvcigpKVxuXG4gICAgICBhcG1Qcm9jZXNzLm9uV2lsbFRocm93RXJyb3IgKHtlcnJvciwgaGFuZGxlfSkgLT5cbiAgICAgICAgaGFuZGxlKClcbiAgICAgICAgcmVqZWN0KGVycm9yKVxuXG4gIHJlbW92ZVBhY2thZ2VOYW1lRnJvbURpc2FibGVkUGFja2FnZXM6IChwYWNrYWdlTmFtZSkgLT5cbiAgICBhdG9tLmNvbmZpZy5yZW1vdmVBdEtleVBhdGgoJ2NvcmUuZGlzYWJsZWRQYWNrYWdlcycsIHBhY2thZ2VOYW1lKVxuXG4gICMgRW1pdHMgdGhlIGFwcHJvcHJpYXRlIGV2ZW50IGZvciB0aGUgZ2l2ZW4gcGFja2FnZS5cbiAgI1xuICAjIEFsbCBldmVudHMgYXJlIGVpdGhlciBvZiB0aGUgZm9ybSBgdGhlbWUtZm9vYCBvciBgcGFja2FnZS1mb29gIGRlcGVuZGluZyBvblxuICAjIHdoZXRoZXIgdGhlIGV2ZW50IGlzIGZvciBhIHRoZW1lIG9yIGEgbm9ybWFsIHBhY2thZ2UuIFRoaXMgbWV0aG9kIHN0YW5kYXJkaXplc1xuICAjIHRoZSBsb2dpYyB0byBkZXRlcm1pbmUgaWYgYSBwYWNrYWdlIGlzIGEgdGhlbWUgb3Igbm90IGFuZCBmb3JtYXRzIHRoZSBldmVudFxuICAjIG5hbWUgYXBwcm9wcmlhdGVseS5cbiAgI1xuICAjIGV2ZW50TmFtZSAtIFRoZSBldmVudCBuYW1lIHN1ZmZpeCB7U3RyaW5nfSBvZiB0aGUgZXZlbnQgdG8gZW1pdC5cbiAgIyBwYWNrIC0gVGhlIHBhY2thZ2UgZm9yIHdoaWNoIHRoZSBldmVudCBpcyBiZWluZyBlbWl0dGVkLlxuICAjIGVycm9yIC0gQW55IGVycm9yIGluZm9ybWF0aW9uIHRvIGJlIGluY2x1ZGVkIGluIHRoZSBjYXNlIG9mIGFuIGVycm9yLlxuICBlbWl0UGFja2FnZUV2ZW50OiAoZXZlbnROYW1lLCBwYWNrLCBlcnJvcikgLT5cbiAgICB0aGVtZSA9IHBhY2sudGhlbWUgPyBwYWNrLm1ldGFkYXRhPy50aGVtZVxuICAgIGV2ZW50TmFtZSA9IGlmIHRoZW1lIHRoZW4gXCJ0aGVtZS0je2V2ZW50TmFtZX1cIiBlbHNlIFwicGFja2FnZS0je2V2ZW50TmFtZX1cIlxuICAgIEBlbWl0dGVyLmVtaXQoZXZlbnROYW1lLCB7cGFjaywgZXJyb3J9KVxuXG4gIG9uOiAoc2VsZWN0b3JzLCBjYWxsYmFjaykgLT5cbiAgICBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcbiAgICBmb3Igc2VsZWN0b3IgaW4gc2VsZWN0b3JzLnNwbGl0KFwiIFwiKVxuICAgICAgc3Vic2NyaXB0aW9ucy5hZGQgQGVtaXR0ZXIub24oc2VsZWN0b3IsIGNhbGxiYWNrKVxuICAgIHN1YnNjcmlwdGlvbnNcblxuY3JlYXRlSnNvblBhcnNlRXJyb3IgPSAobWVzc2FnZSwgcGFyc2VFcnJvciwgc3Rkb3V0KSAtPlxuICBlcnJvciA9IG5ldyBFcnJvcihtZXNzYWdlKVxuICBlcnJvci5zdGRvdXQgPSAnJ1xuICBlcnJvci5zdGRlcnIgPSBcIiN7cGFyc2VFcnJvci5tZXNzYWdlfTogI3tzdGRvdXR9XCJcbiAgZXJyb3JcblxuY3JlYXRlUHJvY2Vzc0Vycm9yID0gKG1lc3NhZ2UsIHByb2Nlc3NFcnJvcikgLT5cbiAgZXJyb3IgPSBuZXcgRXJyb3IobWVzc2FnZSlcbiAgZXJyb3Iuc3Rkb3V0ID0gJydcbiAgZXJyb3Iuc3RkZXJyID0gcHJvY2Vzc0Vycm9yLm1lc3NhZ2VcbiAgZXJyb3JcblxuaGFuZGxlUHJvY2Vzc0Vycm9ycyA9IChhcG1Qcm9jZXNzLCBtZXNzYWdlLCBjYWxsYmFjaykgLT5cbiAgYXBtUHJvY2Vzcy5vbldpbGxUaHJvd0Vycm9yICh7ZXJyb3IsIGhhbmRsZX0pIC0+XG4gICAgaGFuZGxlKClcbiAgICBjYWxsYmFjayhjcmVhdGVQcm9jZXNzRXJyb3IobWVzc2FnZSwgZXJyb3IpKVxuIl19
