(function() {
  var IgnoredNames, Minimatch;

  Minimatch = null;

  module.exports = IgnoredNames = (function() {
    function IgnoredNames() {
      var error, i, ignoredName, ignoredNames, len, ref;
      this.ignoredPatterns = [];
      if (Minimatch == null) {
        Minimatch = require('minimatch').Minimatch;
      }
      ignoredNames = (ref = atom.config.get('core.ignoredNames')) != null ? ref : [];
      if (typeof ignoredNames === 'string') {
        ignoredNames = [ignoredNames];
      }
      for (i = 0, len = ignoredNames.length; i < len; i++) {
        ignoredName = ignoredNames[i];
        if (ignoredName) {
          try {
            this.ignoredPatterns.push(new Minimatch(ignoredName, {
              matchBase: true,
              dot: true
            }));
          } catch (error1) {
            error = error1;
            atom.notifications.addWarning("Error parsing ignore pattern (" + ignoredName + ")", {
              detail: error.message
            });
          }
        }
      }
    }

    IgnoredNames.prototype.matches = function(filePath) {
      var i, ignoredPattern, len, ref;
      ref = this.ignoredPatterns;
      for (i = 0, len = ref.length; i < len; i++) {
        ignoredPattern = ref[i];
        if (ignoredPattern.match(filePath)) {
          return true;
        }
      }
      return false;
    };

    return IgnoredNames;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90cmVlLXZpZXcvbGliL2lnbm9yZWQtbmFtZXMuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQSxTQUFBLEdBQVk7O0VBRVosTUFBTSxDQUFDLE9BQVAsR0FDTTtJQUNTLHNCQUFBO0FBQ1gsVUFBQTtNQUFBLElBQUMsQ0FBQSxlQUFELEdBQW1COztRQUVuQixZQUFhLE9BQUEsQ0FBUSxXQUFSLENBQW9CLENBQUM7O01BRWxDLFlBQUEsZ0VBQXNEO01BQ3RELElBQWlDLE9BQU8sWUFBUCxLQUF1QixRQUF4RDtRQUFBLFlBQUEsR0FBZSxDQUFDLFlBQUQsRUFBZjs7QUFDQSxXQUFBLDhDQUFBOztZQUFxQztBQUNuQztZQUNFLElBQUMsQ0FBQSxlQUFlLENBQUMsSUFBakIsQ0FBc0IsSUFBSSxTQUFKLENBQWMsV0FBZCxFQUEyQjtjQUFBLFNBQUEsRUFBVyxJQUFYO2NBQWlCLEdBQUEsRUFBSyxJQUF0QjthQUEzQixDQUF0QixFQURGO1dBQUEsY0FBQTtZQUVNO1lBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixnQ0FBQSxHQUFpQyxXQUFqQyxHQUE2QyxHQUEzRSxFQUErRTtjQUFBLE1BQUEsRUFBUSxLQUFLLENBQUMsT0FBZDthQUEvRSxFQUhGOzs7QUFERjtJQVBXOzsyQkFhYixPQUFBLEdBQVMsU0FBQyxRQUFEO0FBQ1AsVUFBQTtBQUFBO0FBQUEsV0FBQSxxQ0FBQTs7UUFDRSxJQUFlLGNBQWMsQ0FBQyxLQUFmLENBQXFCLFFBQXJCLENBQWY7QUFBQSxpQkFBTyxLQUFQOztBQURGO0FBR0EsYUFBTztJQUpBOzs7OztBQWpCWCIsInNvdXJjZXNDb250ZW50IjpbIk1pbmltYXRjaCA9IG51bGwgICMgRGVmZXIgcmVxdWlyaW5nIHVudGlsIGFjdHVhbGx5IG5lZWRlZFxuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBJZ25vcmVkTmFtZXNcbiAgY29uc3RydWN0b3I6IC0+XG4gICAgQGlnbm9yZWRQYXR0ZXJucyA9IFtdXG5cbiAgICBNaW5pbWF0Y2ggPz0gcmVxdWlyZSgnbWluaW1hdGNoJykuTWluaW1hdGNoXG5cbiAgICBpZ25vcmVkTmFtZXMgPSBhdG9tLmNvbmZpZy5nZXQoJ2NvcmUuaWdub3JlZE5hbWVzJykgPyBbXVxuICAgIGlnbm9yZWROYW1lcyA9IFtpZ25vcmVkTmFtZXNdIGlmIHR5cGVvZiBpZ25vcmVkTmFtZXMgaXMgJ3N0cmluZydcbiAgICBmb3IgaWdub3JlZE5hbWUgaW4gaWdub3JlZE5hbWVzIHdoZW4gaWdub3JlZE5hbWVcbiAgICAgIHRyeVxuICAgICAgICBAaWdub3JlZFBhdHRlcm5zLnB1c2gobmV3IE1pbmltYXRjaChpZ25vcmVkTmFtZSwgbWF0Y2hCYXNlOiB0cnVlLCBkb3Q6IHRydWUpKVxuICAgICAgY2F0Y2ggZXJyb3JcbiAgICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZFdhcm5pbmcoXCJFcnJvciBwYXJzaW5nIGlnbm9yZSBwYXR0ZXJuICgje2lnbm9yZWROYW1lfSlcIiwgZGV0YWlsOiBlcnJvci5tZXNzYWdlKVxuXG4gIG1hdGNoZXM6IChmaWxlUGF0aCkgLT5cbiAgICBmb3IgaWdub3JlZFBhdHRlcm4gaW4gQGlnbm9yZWRQYXR0ZXJuc1xuICAgICAgcmV0dXJuIHRydWUgaWYgaWdub3JlZFBhdHRlcm4ubWF0Y2goZmlsZVBhdGgpXG5cbiAgICByZXR1cm4gZmFsc2VcbiJdfQ==
