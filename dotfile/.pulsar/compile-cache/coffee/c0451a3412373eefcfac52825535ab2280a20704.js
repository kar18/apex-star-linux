(function() {
  var CompositeDisposable, CursorPositionView, Emitter, FileInfoView, GitView, Grim, LaunchModeView, SelectionCountView, StatusBarView, ref,
    slice = [].slice;

  ref = require('atom'), CompositeDisposable = ref.CompositeDisposable, Emitter = ref.Emitter;

  Grim = require('grim');

  StatusBarView = require('./status-bar-view');

  FileInfoView = require('./file-info-view');

  CursorPositionView = require('./cursor-position-view');

  SelectionCountView = require('./selection-count-view');

  GitView = require('./git-view');

  LaunchModeView = require('./launch-mode-view');

  module.exports = {
    activate: function() {
      var devMode, launchModeView, ref1, safeMode;
      this.emitters = new Emitter();
      this.subscriptions = new CompositeDisposable();
      this.statusBar = new StatusBarView();
      this.attachStatusBar();
      this.subscriptions.add(atom.config.onDidChange('status-bar.fullWidth', (function(_this) {
        return function() {
          return _this.attachStatusBar();
        };
      })(this)));
      this.updateStatusBarVisibility();
      this.statusBarVisibilitySubscription = atom.config.observe('status-bar.isVisible', (function(_this) {
        return function() {
          return _this.updateStatusBarVisibility();
        };
      })(this));
      atom.commands.add('atom-workspace', 'status-bar:toggle', (function(_this) {
        return function() {
          if (_this.statusBarPanel.isVisible()) {
            return atom.config.set('status-bar.isVisible', false);
          } else {
            return atom.config.set('status-bar.isVisible', true);
          }
        };
      })(this));
      ref1 = atom.getLoadSettings(), safeMode = ref1.safeMode, devMode = ref1.devMode;
      if (safeMode || devMode) {
        launchModeView = new LaunchModeView({
          safeMode: safeMode,
          devMode: devMode
        });
        this.statusBar.addLeftTile({
          item: launchModeView.element,
          priority: -1
        });
      }
      this.fileInfo = new FileInfoView();
      this.statusBar.addLeftTile({
        item: this.fileInfo.element,
        priority: 0
      });
      this.cursorPosition = new CursorPositionView();
      this.statusBar.addLeftTile({
        item: this.cursorPosition.element,
        priority: 1
      });
      this.selectionCount = new SelectionCountView();
      this.statusBar.addLeftTile({
        item: this.selectionCount.element,
        priority: 2
      });
      this.gitInfo = new GitView();
      return this.gitInfoTile = this.statusBar.addRightTile({
        item: this.gitInfo.element,
        priority: 0
      });
    },
    deactivate: function() {
      var ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
      if ((ref1 = this.statusBarVisibilitySubscription) != null) {
        ref1.dispose();
      }
      this.statusBarVisibilitySubscription = null;
      if ((ref2 = this.gitInfo) != null) {
        ref2.destroy();
      }
      this.gitInfo = null;
      if ((ref3 = this.fileInfo) != null) {
        ref3.destroy();
      }
      this.fileInfo = null;
      if ((ref4 = this.cursorPosition) != null) {
        ref4.destroy();
      }
      this.cursorPosition = null;
      if ((ref5 = this.selectionCount) != null) {
        ref5.destroy();
      }
      this.selectionCount = null;
      if ((ref6 = this.statusBarPanel) != null) {
        ref6.destroy();
      }
      this.statusBarPanel = null;
      if ((ref7 = this.statusBar) != null) {
        ref7.destroy();
      }
      this.statusBar = null;
      if ((ref8 = this.subscriptions) != null) {
        ref8.dispose();
      }
      this.subscriptions = null;
      if ((ref9 = this.emitters) != null) {
        ref9.dispose();
      }
      this.emitters = null;
      if (atom.__workspaceView != null) {
        return delete atom.__workspaceView.statusBar;
      }
    },
    updateStatusBarVisibility: function() {
      if (atom.config.get('status-bar.isVisible')) {
        return this.statusBarPanel.show();
      } else {
        return this.statusBarPanel.hide();
      }
    },
    provideStatusBar: function() {
      return {
        addLeftTile: this.statusBar.addLeftTile.bind(this.statusBar),
        addRightTile: this.statusBar.addRightTile.bind(this.statusBar),
        getLeftTiles: this.statusBar.getLeftTiles.bind(this.statusBar),
        getRightTiles: this.statusBar.getRightTiles.bind(this.statusBar),
        disableGitInfoTile: this.gitInfoTile.destroy.bind(this.gitInfoTile)
      };
    },
    attachStatusBar: function() {
      var panelArgs;
      if (this.statusBarPanel != null) {
        this.statusBarPanel.destroy();
      }
      panelArgs = {
        item: this.statusBar,
        priority: 0
      };
      if (atom.config.get('status-bar.fullWidth')) {
        return this.statusBarPanel = atom.workspace.addFooterPanel(panelArgs);
      } else {
        return this.statusBarPanel = atom.workspace.addBottomPanel(panelArgs);
      }
    },
    legacyProvideStatusBar: function() {
      var statusbar;
      statusbar = this.provideStatusBar();
      return {
        addLeftTile: function() {
          var args;
          args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
          Grim.deprecate("Use version ^1.0.0 of the status-bar Service API.");
          return statusbar.addLeftTile.apply(statusbar, args);
        },
        addRightTile: function() {
          var args;
          args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
          Grim.deprecate("Use version ^1.0.0 of the status-bar Service API.");
          return statusbar.addRightTile.apply(statusbar, args);
        },
        getLeftTiles: function() {
          Grim.deprecate("Use version ^1.0.0 of the status-bar Service API.");
          return statusbar.getLeftTiles();
        },
        getRightTiles: function() {
          Grim.deprecate("Use version ^1.0.0 of the status-bar Service API.");
          return statusbar.getRightTiles();
        }
      };
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zdGF0dXMtYmFyL2xpYi9tYWluLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUEscUlBQUE7SUFBQTs7RUFBQSxNQUFpQyxPQUFBLENBQVEsTUFBUixDQUFqQyxFQUFDLDZDQUFELEVBQXNCOztFQUN0QixJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBQ1AsYUFBQSxHQUFnQixPQUFBLENBQVEsbUJBQVI7O0VBQ2hCLFlBQUEsR0FBZSxPQUFBLENBQVEsa0JBQVI7O0VBQ2Ysa0JBQUEsR0FBcUIsT0FBQSxDQUFRLHdCQUFSOztFQUNyQixrQkFBQSxHQUFxQixPQUFBLENBQVEsd0JBQVI7O0VBQ3JCLE9BQUEsR0FBVSxPQUFBLENBQVEsWUFBUjs7RUFDVixjQUFBLEdBQWlCLE9BQUEsQ0FBUSxvQkFBUjs7RUFFakIsTUFBTSxDQUFDLE9BQVAsR0FDRTtJQUFBLFFBQUEsRUFBVSxTQUFBO0FBQ1IsVUFBQTtNQUFBLElBQUMsQ0FBQSxRQUFELEdBQVksSUFBSSxPQUFKLENBQUE7TUFDWixJQUFDLENBQUEsYUFBRCxHQUFpQixJQUFJLG1CQUFKLENBQUE7TUFFakIsSUFBQyxDQUFBLFNBQUQsR0FBYSxJQUFJLGFBQUosQ0FBQTtNQUNiLElBQUMsQ0FBQSxlQUFELENBQUE7TUFFQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFaLENBQXdCLHNCQUF4QixFQUFnRCxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQ2pFLEtBQUMsQ0FBQSxlQUFELENBQUE7UUFEaUU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWhELENBQW5CO01BR0EsSUFBQyxDQUFBLHlCQUFELENBQUE7TUFFQSxJQUFDLENBQUEsK0JBQUQsR0FDRSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQVosQ0FBb0Isc0JBQXBCLEVBQTRDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFDMUMsS0FBQyxDQUFBLHlCQUFELENBQUE7UUFEMEM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTVDO01BR0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyxtQkFBcEMsRUFBeUQsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO1VBQ3ZELElBQUcsS0FBQyxDQUFBLGNBQWMsQ0FBQyxTQUFoQixDQUFBLENBQUg7bUJBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLHNCQUFoQixFQUF3QyxLQUF4QyxFQURGO1dBQUEsTUFBQTttQkFHRSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0Isc0JBQWhCLEVBQXdDLElBQXhDLEVBSEY7O1FBRHVEO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6RDtNQU1BLE9BQXNCLElBQUksQ0FBQyxlQUFMLENBQUEsQ0FBdEIsRUFBQyx3QkFBRCxFQUFXO01BQ1gsSUFBRyxRQUFBLElBQVksT0FBZjtRQUNFLGNBQUEsR0FBaUIsSUFBSSxjQUFKLENBQW1CO1VBQUMsVUFBQSxRQUFEO1VBQVcsU0FBQSxPQUFYO1NBQW5CO1FBQ2pCLElBQUMsQ0FBQSxTQUFTLENBQUMsV0FBWCxDQUF1QjtVQUFBLElBQUEsRUFBTSxjQUFjLENBQUMsT0FBckI7VUFBOEIsUUFBQSxFQUFVLENBQUMsQ0FBekM7U0FBdkIsRUFGRjs7TUFJQSxJQUFDLENBQUEsUUFBRCxHQUFZLElBQUksWUFBSixDQUFBO01BQ1osSUFBQyxDQUFBLFNBQVMsQ0FBQyxXQUFYLENBQXVCO1FBQUEsSUFBQSxFQUFNLElBQUMsQ0FBQSxRQUFRLENBQUMsT0FBaEI7UUFBeUIsUUFBQSxFQUFVLENBQW5DO09BQXZCO01BRUEsSUFBQyxDQUFBLGNBQUQsR0FBa0IsSUFBSSxrQkFBSixDQUFBO01BQ2xCLElBQUMsQ0FBQSxTQUFTLENBQUMsV0FBWCxDQUF1QjtRQUFBLElBQUEsRUFBTSxJQUFDLENBQUEsY0FBYyxDQUFDLE9BQXRCO1FBQStCLFFBQUEsRUFBVSxDQUF6QztPQUF2QjtNQUVBLElBQUMsQ0FBQSxjQUFELEdBQWtCLElBQUksa0JBQUosQ0FBQTtNQUNsQixJQUFDLENBQUEsU0FBUyxDQUFDLFdBQVgsQ0FBdUI7UUFBQSxJQUFBLEVBQU0sSUFBQyxDQUFBLGNBQWMsQ0FBQyxPQUF0QjtRQUErQixRQUFBLEVBQVUsQ0FBekM7T0FBdkI7TUFFQSxJQUFDLENBQUEsT0FBRCxHQUFXLElBQUksT0FBSixDQUFBO2FBQ1gsSUFBQyxDQUFBLFdBQUQsR0FBZSxJQUFDLENBQUEsU0FBUyxDQUFDLFlBQVgsQ0FBd0I7UUFBQSxJQUFBLEVBQU0sSUFBQyxDQUFBLE9BQU8sQ0FBQyxPQUFmO1FBQXdCLFFBQUEsRUFBVSxDQUFsQztPQUF4QjtJQXJDUCxDQUFWO0lBdUNBLFVBQUEsRUFBWSxTQUFBO0FBQ1YsVUFBQTs7WUFBZ0MsQ0FBRSxPQUFsQyxDQUFBOztNQUNBLElBQUMsQ0FBQSwrQkFBRCxHQUFtQzs7WUFFM0IsQ0FBRSxPQUFWLENBQUE7O01BQ0EsSUFBQyxDQUFBLE9BQUQsR0FBVzs7WUFFRixDQUFFLE9BQVgsQ0FBQTs7TUFDQSxJQUFDLENBQUEsUUFBRCxHQUFZOztZQUVHLENBQUUsT0FBakIsQ0FBQTs7TUFDQSxJQUFDLENBQUEsY0FBRCxHQUFrQjs7WUFFSCxDQUFFLE9BQWpCLENBQUE7O01BQ0EsSUFBQyxDQUFBLGNBQUQsR0FBa0I7O1lBRUgsQ0FBRSxPQUFqQixDQUFBOztNQUNBLElBQUMsQ0FBQSxjQUFELEdBQWtCOztZQUVSLENBQUUsT0FBWixDQUFBOztNQUNBLElBQUMsQ0FBQSxTQUFELEdBQWE7O1lBRUMsQ0FBRSxPQUFoQixDQUFBOztNQUNBLElBQUMsQ0FBQSxhQUFELEdBQWlCOztZQUVSLENBQUUsT0FBWCxDQUFBOztNQUNBLElBQUMsQ0FBQSxRQUFELEdBQVk7TUFFWixJQUF5Qyw0QkFBekM7ZUFBQSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBNUI7O0lBNUJVLENBdkNaO0lBcUVBLHlCQUFBLEVBQTJCLFNBQUE7TUFDekIsSUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0Isc0JBQWhCLENBQUg7ZUFDRSxJQUFDLENBQUEsY0FBYyxDQUFDLElBQWhCLENBQUEsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsY0FBYyxDQUFDLElBQWhCLENBQUEsRUFIRjs7SUFEeUIsQ0FyRTNCO0lBMkVBLGdCQUFBLEVBQWtCLFNBQUE7YUFDaEI7UUFBQSxXQUFBLEVBQWEsSUFBQyxDQUFBLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBdkIsQ0FBNEIsSUFBQyxDQUFBLFNBQTdCLENBQWI7UUFDQSxZQUFBLEVBQWMsSUFBQyxDQUFBLFNBQVMsQ0FBQyxZQUFZLENBQUMsSUFBeEIsQ0FBNkIsSUFBQyxDQUFBLFNBQTlCLENBRGQ7UUFFQSxZQUFBLEVBQWMsSUFBQyxDQUFBLFNBQVMsQ0FBQyxZQUFZLENBQUMsSUFBeEIsQ0FBNkIsSUFBQyxDQUFBLFNBQTlCLENBRmQ7UUFHQSxhQUFBLEVBQWUsSUFBQyxDQUFBLFNBQVMsQ0FBQyxhQUFhLENBQUMsSUFBekIsQ0FBOEIsSUFBQyxDQUFBLFNBQS9CLENBSGY7UUFJQSxrQkFBQSxFQUFvQixJQUFDLENBQUEsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFyQixDQUEwQixJQUFDLENBQUEsV0FBM0IsQ0FKcEI7O0lBRGdCLENBM0VsQjtJQWtGQSxlQUFBLEVBQWlCLFNBQUE7QUFDZixVQUFBO01BQUEsSUFBNkIsMkJBQTdCO1FBQUEsSUFBQyxDQUFBLGNBQWMsQ0FBQyxPQUFoQixDQUFBLEVBQUE7O01BRUEsU0FBQSxHQUFZO1FBQUMsSUFBQSxFQUFNLElBQUMsQ0FBQSxTQUFSO1FBQW1CLFFBQUEsRUFBVSxDQUE3Qjs7TUFDWixJQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQixzQkFBaEIsQ0FBSDtlQUNFLElBQUMsQ0FBQSxjQUFELEdBQWtCLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBZixDQUE4QixTQUE5QixFQURwQjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsY0FBRCxHQUFrQixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWYsQ0FBOEIsU0FBOUIsRUFIcEI7O0lBSmUsQ0FsRmpCO0lBZ0dBLHNCQUFBLEVBQXdCLFNBQUE7QUFDdEIsVUFBQTtNQUFBLFNBQUEsR0FBWSxJQUFDLENBQUEsZ0JBQUQsQ0FBQTthQUVaO1FBQUEsV0FBQSxFQUFhLFNBQUE7QUFDWCxjQUFBO1VBRFk7VUFDWixJQUFJLENBQUMsU0FBTCxDQUFlLG1EQUFmO2lCQUNBLFNBQVMsQ0FBQyxXQUFWLGtCQUFzQixJQUF0QjtRQUZXLENBQWI7UUFHQSxZQUFBLEVBQWMsU0FBQTtBQUNaLGNBQUE7VUFEYTtVQUNiLElBQUksQ0FBQyxTQUFMLENBQWUsbURBQWY7aUJBQ0EsU0FBUyxDQUFDLFlBQVYsa0JBQXVCLElBQXZCO1FBRlksQ0FIZDtRQU1BLFlBQUEsRUFBYyxTQUFBO1VBQ1osSUFBSSxDQUFDLFNBQUwsQ0FBZSxtREFBZjtpQkFDQSxTQUFTLENBQUMsWUFBVixDQUFBO1FBRlksQ0FOZDtRQVNBLGFBQUEsRUFBZSxTQUFBO1VBQ2IsSUFBSSxDQUFDLFNBQUwsQ0FBZSxtREFBZjtpQkFDQSxTQUFTLENBQUMsYUFBVixDQUFBO1FBRmEsQ0FUZjs7SUFIc0IsQ0FoR3hCOztBQVZGIiwic291cmNlc0NvbnRlbnQiOlsie0NvbXBvc2l0ZURpc3Bvc2FibGUsIEVtaXR0ZXJ9ID0gcmVxdWlyZSAnYXRvbSdcbkdyaW0gPSByZXF1aXJlICdncmltJ1xuU3RhdHVzQmFyVmlldyA9IHJlcXVpcmUgJy4vc3RhdHVzLWJhci12aWV3J1xuRmlsZUluZm9WaWV3ID0gcmVxdWlyZSAnLi9maWxlLWluZm8tdmlldydcbkN1cnNvclBvc2l0aW9uVmlldyA9IHJlcXVpcmUgJy4vY3Vyc29yLXBvc2l0aW9uLXZpZXcnXG5TZWxlY3Rpb25Db3VudFZpZXcgPSByZXF1aXJlICcuL3NlbGVjdGlvbi1jb3VudC12aWV3J1xuR2l0VmlldyA9IHJlcXVpcmUgJy4vZ2l0LXZpZXcnXG5MYXVuY2hNb2RlVmlldyA9IHJlcXVpcmUgJy4vbGF1bmNoLW1vZGUtdmlldydcblxubW9kdWxlLmV4cG9ydHMgPVxuICBhY3RpdmF0ZTogLT5cbiAgICBAZW1pdHRlcnMgPSBuZXcgRW1pdHRlcigpXG4gICAgQHN1YnNjcmlwdGlvbnMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZSgpXG5cbiAgICBAc3RhdHVzQmFyID0gbmV3IFN0YXR1c0JhclZpZXcoKVxuICAgIEBhdHRhY2hTdGF0dXNCYXIoKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29uZmlnLm9uRGlkQ2hhbmdlICdzdGF0dXMtYmFyLmZ1bGxXaWR0aCcsID0+XG4gICAgICBAYXR0YWNoU3RhdHVzQmFyKClcblxuICAgIEB1cGRhdGVTdGF0dXNCYXJWaXNpYmlsaXR5KClcblxuICAgIEBzdGF0dXNCYXJWaXNpYmlsaXR5U3Vic2NyaXB0aW9uID1cbiAgICAgIGF0b20uY29uZmlnLm9ic2VydmUgJ3N0YXR1cy1iYXIuaXNWaXNpYmxlJywgPT5cbiAgICAgICAgQHVwZGF0ZVN0YXR1c0JhclZpc2liaWxpdHkoKVxuXG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3N0YXR1cy1iYXI6dG9nZ2xlJywgPT5cbiAgICAgIGlmIEBzdGF0dXNCYXJQYW5lbC5pc1Zpc2libGUoKVxuICAgICAgICBhdG9tLmNvbmZpZy5zZXQgJ3N0YXR1cy1iYXIuaXNWaXNpYmxlJywgZmFsc2VcbiAgICAgIGVsc2VcbiAgICAgICAgYXRvbS5jb25maWcuc2V0ICdzdGF0dXMtYmFyLmlzVmlzaWJsZScsIHRydWVcblxuICAgIHtzYWZlTW9kZSwgZGV2TW9kZX0gPSBhdG9tLmdldExvYWRTZXR0aW5ncygpXG4gICAgaWYgc2FmZU1vZGUgb3IgZGV2TW9kZVxuICAgICAgbGF1bmNoTW9kZVZpZXcgPSBuZXcgTGF1bmNoTW9kZVZpZXcoe3NhZmVNb2RlLCBkZXZNb2RlfSlcbiAgICAgIEBzdGF0dXNCYXIuYWRkTGVmdFRpbGUoaXRlbTogbGF1bmNoTW9kZVZpZXcuZWxlbWVudCwgcHJpb3JpdHk6IC0xKVxuXG4gICAgQGZpbGVJbmZvID0gbmV3IEZpbGVJbmZvVmlldygpXG4gICAgQHN0YXR1c0Jhci5hZGRMZWZ0VGlsZShpdGVtOiBAZmlsZUluZm8uZWxlbWVudCwgcHJpb3JpdHk6IDApXG5cbiAgICBAY3Vyc29yUG9zaXRpb24gPSBuZXcgQ3Vyc29yUG9zaXRpb25WaWV3KClcbiAgICBAc3RhdHVzQmFyLmFkZExlZnRUaWxlKGl0ZW06IEBjdXJzb3JQb3NpdGlvbi5lbGVtZW50LCBwcmlvcml0eTogMSlcblxuICAgIEBzZWxlY3Rpb25Db3VudCA9IG5ldyBTZWxlY3Rpb25Db3VudFZpZXcoKVxuICAgIEBzdGF0dXNCYXIuYWRkTGVmdFRpbGUoaXRlbTogQHNlbGVjdGlvbkNvdW50LmVsZW1lbnQsIHByaW9yaXR5OiAyKVxuXG4gICAgQGdpdEluZm8gPSBuZXcgR2l0VmlldygpXG4gICAgQGdpdEluZm9UaWxlID0gQHN0YXR1c0Jhci5hZGRSaWdodFRpbGUoaXRlbTogQGdpdEluZm8uZWxlbWVudCwgcHJpb3JpdHk6IDApXG5cbiAgZGVhY3RpdmF0ZTogLT5cbiAgICBAc3RhdHVzQmFyVmlzaWJpbGl0eVN1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG4gICAgQHN0YXR1c0JhclZpc2liaWxpdHlTdWJzY3JpcHRpb24gPSBudWxsXG5cbiAgICBAZ2l0SW5mbz8uZGVzdHJveSgpXG4gICAgQGdpdEluZm8gPSBudWxsXG5cbiAgICBAZmlsZUluZm8/LmRlc3Ryb3koKVxuICAgIEBmaWxlSW5mbyA9IG51bGxcblxuICAgIEBjdXJzb3JQb3NpdGlvbj8uZGVzdHJveSgpXG4gICAgQGN1cnNvclBvc2l0aW9uID0gbnVsbFxuXG4gICAgQHNlbGVjdGlvbkNvdW50Py5kZXN0cm95KClcbiAgICBAc2VsZWN0aW9uQ291bnQgPSBudWxsXG5cbiAgICBAc3RhdHVzQmFyUGFuZWw/LmRlc3Ryb3koKVxuICAgIEBzdGF0dXNCYXJQYW5lbCA9IG51bGxcblxuICAgIEBzdGF0dXNCYXI/LmRlc3Ryb3koKVxuICAgIEBzdGF0dXNCYXIgPSBudWxsXG5cbiAgICBAc3Vic2NyaXB0aW9ucz8uZGlzcG9zZSgpXG4gICAgQHN1YnNjcmlwdGlvbnMgPSBudWxsXG5cbiAgICBAZW1pdHRlcnM/LmRpc3Bvc2UoKVxuICAgIEBlbWl0dGVycyA9IG51bGxcblxuICAgIGRlbGV0ZSBhdG9tLl9fd29ya3NwYWNlVmlldy5zdGF0dXNCYXIgaWYgYXRvbS5fX3dvcmtzcGFjZVZpZXc/XG5cbiAgdXBkYXRlU3RhdHVzQmFyVmlzaWJpbGl0eTogLT5cbiAgICBpZiBhdG9tLmNvbmZpZy5nZXQgJ3N0YXR1cy1iYXIuaXNWaXNpYmxlJ1xuICAgICAgQHN0YXR1c0JhclBhbmVsLnNob3coKVxuICAgIGVsc2VcbiAgICAgIEBzdGF0dXNCYXJQYW5lbC5oaWRlKClcblxuICBwcm92aWRlU3RhdHVzQmFyOiAtPlxuICAgIGFkZExlZnRUaWxlOiBAc3RhdHVzQmFyLmFkZExlZnRUaWxlLmJpbmQoQHN0YXR1c0JhcilcbiAgICBhZGRSaWdodFRpbGU6IEBzdGF0dXNCYXIuYWRkUmlnaHRUaWxlLmJpbmQoQHN0YXR1c0JhcilcbiAgICBnZXRMZWZ0VGlsZXM6IEBzdGF0dXNCYXIuZ2V0TGVmdFRpbGVzLmJpbmQoQHN0YXR1c0JhcilcbiAgICBnZXRSaWdodFRpbGVzOiBAc3RhdHVzQmFyLmdldFJpZ2h0VGlsZXMuYmluZChAc3RhdHVzQmFyKVxuICAgIGRpc2FibGVHaXRJbmZvVGlsZTogQGdpdEluZm9UaWxlLmRlc3Ryb3kuYmluZChAZ2l0SW5mb1RpbGUpXG5cbiAgYXR0YWNoU3RhdHVzQmFyOiAtPlxuICAgIEBzdGF0dXNCYXJQYW5lbC5kZXN0cm95KCkgaWYgQHN0YXR1c0JhclBhbmVsP1xuXG4gICAgcGFuZWxBcmdzID0ge2l0ZW06IEBzdGF0dXNCYXIsIHByaW9yaXR5OiAwfVxuICAgIGlmIGF0b20uY29uZmlnLmdldCgnc3RhdHVzLWJhci5mdWxsV2lkdGgnKVxuICAgICAgQHN0YXR1c0JhclBhbmVsID0gYXRvbS53b3Jrc3BhY2UuYWRkRm9vdGVyUGFuZWwgcGFuZWxBcmdzXG4gICAgZWxzZVxuICAgICAgQHN0YXR1c0JhclBhbmVsID0gYXRvbS53b3Jrc3BhY2UuYWRkQm90dG9tUGFuZWwgcGFuZWxBcmdzXG5cbiAgIyBEZXByZWNhdGVkXG4gICNcbiAgIyBXcmFwIGRlcHJlY2F0aW9uIGNhbGxzIG9uIHRoZSBtZXRob2RzIHJldHVybmVkIHJhdGhlciB0aGFuXG4gICMgU2VydmljZXMgQVBJIG1ldGhvZCB3aGljaCB3b3VsZCBiZSByZWdpc3RlcmVkIGFuZCB0cmlnZ2VyXG4gICMgYSBkZXByZWNhdGlvbiBjYWxsXG4gIGxlZ2FjeVByb3ZpZGVTdGF0dXNCYXI6IC0+XG4gICAgc3RhdHVzYmFyID0gQHByb3ZpZGVTdGF0dXNCYXIoKVxuXG4gICAgYWRkTGVmdFRpbGU6IChhcmdzLi4uKSAtPlxuICAgICAgR3JpbS5kZXByZWNhdGUoXCJVc2UgdmVyc2lvbiBeMS4wLjAgb2YgdGhlIHN0YXR1cy1iYXIgU2VydmljZSBBUEkuXCIpXG4gICAgICBzdGF0dXNiYXIuYWRkTGVmdFRpbGUoYXJncy4uLilcbiAgICBhZGRSaWdodFRpbGU6IChhcmdzLi4uKSAtPlxuICAgICAgR3JpbS5kZXByZWNhdGUoXCJVc2UgdmVyc2lvbiBeMS4wLjAgb2YgdGhlIHN0YXR1cy1iYXIgU2VydmljZSBBUEkuXCIpXG4gICAgICBzdGF0dXNiYXIuYWRkUmlnaHRUaWxlKGFyZ3MuLi4pXG4gICAgZ2V0TGVmdFRpbGVzOiAtPlxuICAgICAgR3JpbS5kZXByZWNhdGUoXCJVc2UgdmVyc2lvbiBeMS4wLjAgb2YgdGhlIHN0YXR1cy1iYXIgU2VydmljZSBBUEkuXCIpXG4gICAgICBzdGF0dXNiYXIuZ2V0TGVmdFRpbGVzKClcbiAgICBnZXRSaWdodFRpbGVzOiAtPlxuICAgICAgR3JpbS5kZXByZWNhdGUoXCJVc2UgdmVyc2lvbiBeMS4wLjAgb2YgdGhlIHN0YXR1cy1iYXIgU2VydmljZSBBUEkuXCIpXG4gICAgICBzdGF0dXNiYXIuZ2V0UmlnaHRUaWxlcygpXG4iXX0=
