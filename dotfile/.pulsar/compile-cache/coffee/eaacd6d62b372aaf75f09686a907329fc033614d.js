(function() {
  var CompositeDisposable, WrapGuideElement;

  CompositeDisposable = require('atom').CompositeDisposable;

  WrapGuideElement = require('./wrap-guide-element');

  module.exports = {
    activate: function() {
      this.subscriptions = new CompositeDisposable();
      this.wrapGuides = new Map();
      return this.subscriptions.add(atom.workspace.observeTextEditors((function(_this) {
        return function(editor) {
          var editorElement, wrapGuideElement;
          if (_this.wrapGuides.has(editor)) {
            return;
          }
          editorElement = atom.views.getView(editor);
          wrapGuideElement = new WrapGuideElement(editor, editorElement);
          _this.wrapGuides.set(editor, wrapGuideElement);
          return _this.subscriptions.add(editor.onDidDestroy(function() {
            _this.wrapGuides.get(editor).destroy();
            return _this.wrapGuides["delete"](editor);
          }));
        };
      })(this)));
    },
    deactivate: function() {
      this.subscriptions.dispose();
      this.wrapGuides.forEach(function(wrapGuide, editor) {
        return wrapGuide.destroy();
      });
      return this.wrapGuides.clear();
    },
    uniqueAscending: function(list) {
      return (list.filter(function(item, index) {
        return list.indexOf(item) === index;
      })).sort(function(a, b) {
        return a - b;
      });
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy93cmFwLWd1aWRlL2xpYi9tYWluLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUMsc0JBQXVCLE9BQUEsQ0FBUSxNQUFSOztFQUN4QixnQkFBQSxHQUFtQixPQUFBLENBQVEsc0JBQVI7O0VBRW5CLE1BQU0sQ0FBQyxPQUFQLEdBQ0U7SUFBQSxRQUFBLEVBQVUsU0FBQTtNQUNSLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUksbUJBQUosQ0FBQTtNQUNqQixJQUFDLENBQUEsVUFBRCxHQUFjLElBQUksR0FBSixDQUFBO2FBRWQsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWYsQ0FBa0MsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLE1BQUQ7QUFDbkQsY0FBQTtVQUFBLElBQVUsS0FBQyxDQUFBLFVBQVUsQ0FBQyxHQUFaLENBQWdCLE1BQWhCLENBQVY7QUFBQSxtQkFBQTs7VUFFQSxhQUFBLEdBQWdCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBWCxDQUFtQixNQUFuQjtVQUNoQixnQkFBQSxHQUFtQixJQUFJLGdCQUFKLENBQXFCLE1BQXJCLEVBQTZCLGFBQTdCO1VBRW5CLEtBQUMsQ0FBQSxVQUFVLENBQUMsR0FBWixDQUFnQixNQUFoQixFQUF3QixnQkFBeEI7aUJBQ0EsS0FBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLE1BQU0sQ0FBQyxZQUFQLENBQW9CLFNBQUE7WUFDckMsS0FBQyxDQUFBLFVBQVUsQ0FBQyxHQUFaLENBQWdCLE1BQWhCLENBQXVCLENBQUMsT0FBeEIsQ0FBQTttQkFDQSxLQUFDLENBQUEsVUFBVSxFQUFDLE1BQUQsRUFBWCxDQUFtQixNQUFuQjtVQUZxQyxDQUFwQixDQUFuQjtRQVBtRDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBbEMsQ0FBbkI7SUFKUSxDQUFWO0lBZUEsVUFBQSxFQUFZLFNBQUE7TUFDVixJQUFDLENBQUEsYUFBYSxDQUFDLE9BQWYsQ0FBQTtNQUNBLElBQUMsQ0FBQSxVQUFVLENBQUMsT0FBWixDQUFvQixTQUFDLFNBQUQsRUFBWSxNQUFaO2VBQXVCLFNBQVMsQ0FBQyxPQUFWLENBQUE7TUFBdkIsQ0FBcEI7YUFDQSxJQUFDLENBQUEsVUFBVSxDQUFDLEtBQVosQ0FBQTtJQUhVLENBZlo7SUFvQkEsZUFBQSxFQUFpQixTQUFDLElBQUQ7YUFDZixDQUFDLElBQUksQ0FBQyxNQUFMLENBQVksU0FBQyxJQUFELEVBQU8sS0FBUDtlQUFpQixJQUFJLENBQUMsT0FBTCxDQUFhLElBQWIsQ0FBQSxLQUFzQjtNQUF2QyxDQUFaLENBQUQsQ0FBMkQsQ0FBQyxJQUE1RCxDQUFpRSxTQUFDLENBQUQsRUFBSSxDQUFKO2VBQVUsQ0FBQSxHQUFJO01BQWQsQ0FBakU7SUFEZSxDQXBCakI7O0FBSkYiLCJzb3VyY2VzQ29udGVudCI6WyJ7Q29tcG9zaXRlRGlzcG9zYWJsZX0gPSByZXF1aXJlICdhdG9tJ1xuV3JhcEd1aWRlRWxlbWVudCA9IHJlcXVpcmUgJy4vd3JhcC1ndWlkZS1lbGVtZW50J1xuXG5tb2R1bGUuZXhwb3J0cyA9XG4gIGFjdGl2YXRlOiAtPlxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGUoKVxuICAgIEB3cmFwR3VpZGVzID0gbmV3IE1hcCgpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS53b3Jrc3BhY2Uub2JzZXJ2ZVRleHRFZGl0b3JzIChlZGl0b3IpID0+XG4gICAgICByZXR1cm4gaWYgQHdyYXBHdWlkZXMuaGFzKGVkaXRvcilcblxuICAgICAgZWRpdG9yRWxlbWVudCA9IGF0b20udmlld3MuZ2V0VmlldyhlZGl0b3IpXG4gICAgICB3cmFwR3VpZGVFbGVtZW50ID0gbmV3IFdyYXBHdWlkZUVsZW1lbnQoZWRpdG9yLCBlZGl0b3JFbGVtZW50KVxuXG4gICAgICBAd3JhcEd1aWRlcy5zZXQoZWRpdG9yLCB3cmFwR3VpZGVFbGVtZW50KVxuICAgICAgQHN1YnNjcmlwdGlvbnMuYWRkIGVkaXRvci5vbkRpZERlc3Ryb3kgPT5cbiAgICAgICAgQHdyYXBHdWlkZXMuZ2V0KGVkaXRvcikuZGVzdHJveSgpXG4gICAgICAgIEB3cmFwR3VpZGVzLmRlbGV0ZShlZGl0b3IpXG5cbiAgZGVhY3RpdmF0ZTogLT5cbiAgICBAc3Vic2NyaXB0aW9ucy5kaXNwb3NlKClcbiAgICBAd3JhcEd1aWRlcy5mb3JFYWNoICh3cmFwR3VpZGUsIGVkaXRvcikgLT4gd3JhcEd1aWRlLmRlc3Ryb3koKVxuICAgIEB3cmFwR3VpZGVzLmNsZWFyKClcblxuICB1bmlxdWVBc2NlbmRpbmc6IChsaXN0KSAtPlxuICAgIChsaXN0LmZpbHRlcigoaXRlbSwgaW5kZXgpIC0+IGxpc3QuaW5kZXhPZihpdGVtKSBpcyBpbmRleCkpLnNvcnQoKGEsIGIpIC0+IGEgLSBiKVxuIl19
