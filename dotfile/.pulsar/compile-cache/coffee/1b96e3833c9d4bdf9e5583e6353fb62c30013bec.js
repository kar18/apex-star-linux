(function() {
  var CompositeDisposable, Notification, NotificationElement, Notifications, NotificationsLog, StackTraceParser, fs, isCoreOrPackageStackTrace, ref;

  ref = require('atom'), Notification = ref.Notification, CompositeDisposable = ref.CompositeDisposable;

  fs = require('fs-plus');

  StackTraceParser = null;

  NotificationElement = require('./notification-element');

  NotificationsLog = require('./notifications-log');

  Notifications = {
    isInitialized: false,
    subscriptions: null,
    duplicateTimeDelay: 500,
    lastNotification: null,
    activate: function(state) {
      var CommandLogger, i, len, notification, ref1;
      CommandLogger = require('./command-logger');
      CommandLogger.start();
      this.subscriptions = new CompositeDisposable;
      ref1 = atom.notifications.getNotifications();
      for (i = 0, len = ref1.length; i < len; i++) {
        notification = ref1[i];
        this.addNotificationView(notification);
      }
      this.subscriptions.add(atom.notifications.onDidAddNotification((function(_this) {
        return function(notification) {
          return _this.addNotificationView(notification);
        };
      })(this)));
      this.subscriptions.add(atom.onWillThrowError(function(arg) {
        var line, match, message, options, originalError, preventDefault, url;
        message = arg.message, url = arg.url, line = arg.line, originalError = arg.originalError, preventDefault = arg.preventDefault;
        if (originalError.name === 'BufferedProcessError') {
          message = message.replace('Uncaught BufferedProcessError: ', '');
          return atom.notifications.addError(message, {
            dismissable: true
          });
        } else if (originalError.code === 'ENOENT' && !/\/atom/i.test(message) && (match = /spawn (.+) ENOENT/.exec(message))) {
          message = "'" + match[1] + "' could not be spawned.\nIs it installed and on your path?\nIf so please open an issue on the package spawning the process.";
          return atom.notifications.addError(message, {
            dismissable: true
          });
        } else if (!atom.inDevMode() || atom.config.get('notifications.showErrorsInDevMode')) {
          preventDefault();
          if (originalError.stack && !isCoreOrPackageStackTrace(originalError.stack)) {
            return;
          }
          options = {
            detail: url + ":" + line,
            stack: originalError.stack,
            dismissable: true
          };
          return atom.notifications.addFatalError(message, options);
        }
      }));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'core:cancel', function() {
        var j, len1, ref2, results;
        ref2 = atom.notifications.getNotifications();
        results = [];
        for (j = 0, len1 = ref2.length; j < len1; j++) {
          notification = ref2[j];
          results.push(notification.dismiss());
        }
        return results;
      }));
      this.subscriptions.add(atom.config.observe('notifications.defaultTimeout', (function(_this) {
        return function(value) {
          return _this.visibilityDuration = value;
        };
      })(this)));
      if (atom.inDevMode()) {
        this.subscriptions.add(atom.commands.add('atom-workspace', 'notifications:trigger-error', function() {
          var error, options;
          try {
            return abc + 2;
          } catch (error1) {
            error = error1;
            options = {
              detail: error.stack.split('\n')[1],
              stack: error.stack,
              dismissable: true
            };
            return atom.notifications.addFatalError("Uncaught " + (error.stack.split('\n')[0]), options);
          }
        }));
      }
      if (this.notificationsLog != null) {
        this.addNotificationsLogSubscriptions();
      }
      this.subscriptions.add(atom.workspace.addOpener((function(_this) {
        return function(uri) {
          if (uri === NotificationsLog.prototype.getURI()) {
            return _this.createLog();
          }
        };
      })(this)));
      this.subscriptions.add(atom.commands.add('atom-workspace', 'notifications:toggle-log', function() {
        return atom.workspace.toggle(NotificationsLog.prototype.getURI());
      }));
      return this.subscriptions.add(atom.commands.add('atom-workspace', 'notifications:clear-log', function() {
        var j, len1, ref2;
        ref2 = atom.notifications.getNotifications();
        for (j = 0, len1 = ref2.length; j < len1; j++) {
          notification = ref2[j];
          notification.options.dismissable = true;
          notification.dismissed = false;
          notification.dismiss();
        }
        return atom.notifications.clear();
      }));
    },
    deactivate: function() {
      var ref1, ref2, ref3;
      this.subscriptions.dispose();
      if ((ref1 = this.notificationsElement) != null) {
        ref1.remove();
      }
      if ((ref2 = this.notificationsPanel) != null) {
        ref2.destroy();
      }
      if ((ref3 = this.notificationsLog) != null) {
        ref3.destroy();
      }
      this.subscriptions = null;
      this.notificationsElement = null;
      this.notificationsPanel = null;
      return this.isInitialized = false;
    },
    initializeIfNotInitialized: function() {
      if (this.isInitialized) {
        return;
      }
      this.subscriptions.add(atom.views.addViewProvider(Notification, (function(_this) {
        return function(model) {
          return new NotificationElement(model, _this.visibilityDuration);
        };
      })(this)));
      this.notificationsElement = document.createElement('atom-notifications');
      atom.views.getView(atom.workspace).appendChild(this.notificationsElement);
      return this.isInitialized = true;
    },
    createLog: function(state) {
      this.notificationsLog = new NotificationsLog(this.duplicateTimeDelay, state != null ? state.typesHidden : void 0);
      if (this.subscriptions != null) {
        this.addNotificationsLogSubscriptions();
      }
      return this.notificationsLog;
    },
    addNotificationsLogSubscriptions: function() {
      this.subscriptions.add(this.notificationsLog.onDidDestroy((function(_this) {
        return function() {
          return _this.notificationsLog = null;
        };
      })(this)));
      return this.subscriptions.add(this.notificationsLog.onItemClick((function(_this) {
        return function(notification) {
          var view;
          view = atom.views.getView(notification);
          view.makeDismissable();
          if (!view.element.classList.contains('remove')) {
            return;
          }
          view.element.classList.remove('remove');
          _this.notificationsElement.appendChild(view.element);
          notification.dismissed = false;
          return notification.setDisplayed(true);
        };
      })(this)));
    },
    addNotificationView: function(notification) {
      var ref1, ref2, timeSpan;
      if (notification == null) {
        return;
      }
      this.initializeIfNotInitialized();
      if (notification.wasDisplayed()) {
        return;
      }
      if (this.lastNotification != null) {
        timeSpan = notification.getTimestamp() - this.lastNotification.getTimestamp();
        if (!(timeSpan < this.duplicateTimeDelay && notification.isEqual(this.lastNotification))) {
          this.notificationsElement.appendChild(atom.views.getView(notification).element);
          if ((ref1 = this.notificationsLog) != null) {
            ref1.addNotification(notification);
          }
        }
      } else {
        this.notificationsElement.appendChild(atom.views.getView(notification).element);
        if ((ref2 = this.notificationsLog) != null) {
          ref2.addNotification(notification);
        }
      }
      notification.setDisplayed(true);
      return this.lastNotification = notification;
    }
  };

  isCoreOrPackageStackTrace = function(stack) {
    var file, i, len, ref1;
    if (StackTraceParser == null) {
      StackTraceParser = require('stacktrace-parser');
    }
    ref1 = StackTraceParser.parse(stack);
    for (i = 0, len = ref1.length; i < len; i++) {
      file = ref1[i].file;
      if (file === '<embedded>' || fs.isAbsolute(file)) {
        return true;
      }
    }
    return false;
  };

  module.exports = Notifications;

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9ub3RpZmljYXRpb25zL2xpYi9tYWluLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsTUFBc0MsT0FBQSxDQUFRLE1BQVIsQ0FBdEMsRUFBQywrQkFBRCxFQUFlOztFQUNmLEVBQUEsR0FBSyxPQUFBLENBQVEsU0FBUjs7RUFDTCxnQkFBQSxHQUFtQjs7RUFDbkIsbUJBQUEsR0FBc0IsT0FBQSxDQUFRLHdCQUFSOztFQUN0QixnQkFBQSxHQUFtQixPQUFBLENBQVEscUJBQVI7O0VBRW5CLGFBQUEsR0FDRTtJQUFBLGFBQUEsRUFBZSxLQUFmO0lBQ0EsYUFBQSxFQUFlLElBRGY7SUFFQSxrQkFBQSxFQUFvQixHQUZwQjtJQUdBLGdCQUFBLEVBQWtCLElBSGxCO0lBS0EsUUFBQSxFQUFVLFNBQUMsS0FBRDtBQUNSLFVBQUE7TUFBQSxhQUFBLEdBQWdCLE9BQUEsQ0FBUSxrQkFBUjtNQUNoQixhQUFhLENBQUMsS0FBZCxDQUFBO01BQ0EsSUFBQyxDQUFBLGFBQUQsR0FBaUIsSUFBSTtBQUVyQjtBQUFBLFdBQUEsc0NBQUE7O1FBQUEsSUFBQyxDQUFBLG1CQUFELENBQXFCLFlBQXJCO0FBQUE7TUFDQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxvQkFBbkIsQ0FBd0MsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLFlBQUQ7aUJBQWtCLEtBQUMsQ0FBQSxtQkFBRCxDQUFxQixZQUFyQjtRQUFsQjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBeEMsQ0FBbkI7TUFFQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLGdCQUFMLENBQXNCLFNBQUMsR0FBRDtBQUN2QyxZQUFBO1FBRHlDLHVCQUFTLGVBQUssaUJBQU0sbUNBQWU7UUFDNUUsSUFBRyxhQUFhLENBQUMsSUFBZCxLQUFzQixzQkFBekI7VUFDRSxPQUFBLEdBQVUsT0FBTyxDQUFDLE9BQVIsQ0FBZ0IsaUNBQWhCLEVBQW1ELEVBQW5EO2lCQUNWLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBbkIsQ0FBNEIsT0FBNUIsRUFBcUM7WUFBQSxXQUFBLEVBQWEsSUFBYjtXQUFyQyxFQUZGO1NBQUEsTUFJSyxJQUFHLGFBQWEsQ0FBQyxJQUFkLEtBQXNCLFFBQXRCLElBQW1DLENBQUksU0FBUyxDQUFDLElBQVYsQ0FBZSxPQUFmLENBQXZDLElBQW1FLENBQUEsS0FBQSxHQUFRLG1CQUFtQixDQUFDLElBQXBCLENBQXlCLE9BQXpCLENBQVIsQ0FBdEU7VUFDSCxPQUFBLEdBQVUsR0FBQSxHQUNMLEtBQU0sQ0FBQSxDQUFBLENBREQsR0FDSTtpQkFJZCxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQW5CLENBQTRCLE9BQTVCLEVBQXFDO1lBQUEsV0FBQSxFQUFhLElBQWI7V0FBckMsRUFORztTQUFBLE1BUUEsSUFBRyxDQUFJLElBQUksQ0FBQyxTQUFMLENBQUEsQ0FBSixJQUF3QixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsbUNBQWhCLENBQTNCO1VBQ0gsY0FBQSxDQUFBO1VBR0EsSUFBRyxhQUFhLENBQUMsS0FBZCxJQUF3QixDQUFJLHlCQUFBLENBQTBCLGFBQWEsQ0FBQyxLQUF4QyxDQUEvQjtBQUNFLG1CQURGOztVQUdBLE9BQUEsR0FDRTtZQUFBLE1BQUEsRUFBVyxHQUFELEdBQUssR0FBTCxHQUFRLElBQWxCO1lBQ0EsS0FBQSxFQUFPLGFBQWEsQ0FBQyxLQURyQjtZQUVBLFdBQUEsRUFBYSxJQUZiOztpQkFHRixJQUFJLENBQUMsYUFBYSxDQUFDLGFBQW5CLENBQWlDLE9BQWpDLEVBQTBDLE9BQTFDLEVBWEc7O01BYmtDLENBQXRCLENBQW5CO01BMEJBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLGFBQXBDLEVBQW1ELFNBQUE7QUFDcEUsWUFBQTtBQUFBO0FBQUE7YUFBQSx3Q0FBQTs7dUJBQUEsWUFBWSxDQUFDLE9BQWIsQ0FBQTtBQUFBOztNQURvRSxDQUFuRCxDQUFuQjtNQUdBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQVosQ0FBb0IsOEJBQXBCLEVBQW9ELENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxLQUFEO2lCQUFXLEtBQUMsQ0FBQSxrQkFBRCxHQUFzQjtRQUFqQztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBcEQsQ0FBbkI7TUFFQSxJQUFHLElBQUksQ0FBQyxTQUFMLENBQUEsQ0FBSDtRQUNFLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLDZCQUFwQyxFQUFtRSxTQUFBO0FBQ3BGLGNBQUE7QUFBQTttQkFDRSxHQUFBLEdBQU0sRUFEUjtXQUFBLGNBQUE7WUFFTTtZQUNKLE9BQUEsR0FDRTtjQUFBLE1BQUEsRUFBUSxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQVosQ0FBa0IsSUFBbEIsQ0FBd0IsQ0FBQSxDQUFBLENBQWhDO2NBQ0EsS0FBQSxFQUFPLEtBQUssQ0FBQyxLQURiO2NBRUEsV0FBQSxFQUFhLElBRmI7O21CQUdGLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBbkIsQ0FBaUMsV0FBQSxHQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFaLENBQWtCLElBQWxCLENBQXdCLENBQUEsQ0FBQSxDQUF6QixDQUE1QyxFQUEyRSxPQUEzRSxFQVBGOztRQURvRixDQUFuRSxDQUFuQixFQURGOztNQVdBLElBQXVDLDZCQUF2QztRQUFBLElBQUMsQ0FBQSxnQ0FBRCxDQUFBLEVBQUE7O01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBZixDQUF5QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsR0FBRDtVQUFTLElBQWdCLEdBQUEsS0FBTyxnQkFBZ0IsQ0FBQSxTQUFFLENBQUEsTUFBbEIsQ0FBQSxDQUF2QjttQkFBQSxLQUFDLENBQUEsU0FBRCxDQUFBLEVBQUE7O1FBQVQ7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpCLENBQW5CO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MsMEJBQXBDLEVBQWdFLFNBQUE7ZUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQWYsQ0FBc0IsZ0JBQWdCLENBQUEsU0FBRSxDQUFBLE1BQWxCLENBQUEsQ0FBdEI7TUFBSCxDQUFoRSxDQUFuQjthQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLHlCQUFwQyxFQUErRCxTQUFBO0FBQ2hGLFlBQUE7QUFBQTtBQUFBLGFBQUEsd0NBQUE7O1VBQ0UsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFyQixHQUFtQztVQUNuQyxZQUFZLENBQUMsU0FBYixHQUF5QjtVQUN6QixZQUFZLENBQUMsT0FBYixDQUFBO0FBSEY7ZUFJQSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQW5CLENBQUE7TUFMZ0YsQ0FBL0QsQ0FBbkI7SUFyRFEsQ0FMVjtJQWlFQSxVQUFBLEVBQVksU0FBQTtBQUNWLFVBQUE7TUFBQSxJQUFDLENBQUEsYUFBYSxDQUFDLE9BQWYsQ0FBQTs7WUFDcUIsQ0FBRSxNQUF2QixDQUFBOzs7WUFDbUIsQ0FBRSxPQUFyQixDQUFBOzs7WUFDaUIsQ0FBRSxPQUFuQixDQUFBOztNQUVBLElBQUMsQ0FBQSxhQUFELEdBQWlCO01BQ2pCLElBQUMsQ0FBQSxvQkFBRCxHQUF3QjtNQUN4QixJQUFDLENBQUEsa0JBQUQsR0FBc0I7YUFFdEIsSUFBQyxDQUFBLGFBQUQsR0FBaUI7SUFWUCxDQWpFWjtJQTZFQSwwQkFBQSxFQUE0QixTQUFBO01BQzFCLElBQVUsSUFBQyxDQUFBLGFBQVg7QUFBQSxlQUFBOztNQUVBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsS0FBSyxDQUFDLGVBQVgsQ0FBMkIsWUFBM0IsRUFBeUMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEtBQUQ7aUJBQzFELElBQUksbUJBQUosQ0FBd0IsS0FBeEIsRUFBK0IsS0FBQyxDQUFBLGtCQUFoQztRQUQwRDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBekMsQ0FBbkI7TUFHQSxJQUFDLENBQUEsb0JBQUQsR0FBd0IsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsb0JBQXZCO01BQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBWCxDQUFtQixJQUFJLENBQUMsU0FBeEIsQ0FBa0MsQ0FBQyxXQUFuQyxDQUErQyxJQUFDLENBQUEsb0JBQWhEO2FBRUEsSUFBQyxDQUFBLGFBQUQsR0FBaUI7SUFUUyxDQTdFNUI7SUF3RkEsU0FBQSxFQUFXLFNBQUMsS0FBRDtNQUNULElBQUMsQ0FBQSxnQkFBRCxHQUFvQixJQUFJLGdCQUFKLENBQXFCLElBQUMsQ0FBQSxrQkFBdEIsa0JBQTBDLEtBQUssQ0FBRSxvQkFBakQ7TUFDcEIsSUFBdUMsMEJBQXZDO1FBQUEsSUFBQyxDQUFBLGdDQUFELENBQUEsRUFBQTs7YUFDQSxJQUFDLENBQUE7SUFIUSxDQXhGWDtJQTZGQSxnQ0FBQSxFQUFrQyxTQUFBO01BQ2hDLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsZ0JBQWdCLENBQUMsWUFBbEIsQ0FBK0IsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxnQkFBRCxHQUFvQjtRQUF2QjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBL0IsQ0FBbkI7YUFDQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBQyxDQUFBLGdCQUFnQixDQUFDLFdBQWxCLENBQThCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxZQUFEO0FBQy9DLGNBQUE7VUFBQSxJQUFBLEdBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFYLENBQW1CLFlBQW5CO1VBQ1AsSUFBSSxDQUFDLGVBQUwsQ0FBQTtVQUVBLElBQUEsQ0FBYyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUF2QixDQUFnQyxRQUFoQyxDQUFkO0FBQUEsbUJBQUE7O1VBQ0EsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBdkIsQ0FBOEIsUUFBOUI7VUFDQSxLQUFDLENBQUEsb0JBQW9CLENBQUMsV0FBdEIsQ0FBa0MsSUFBSSxDQUFDLE9BQXZDO1VBQ0EsWUFBWSxDQUFDLFNBQWIsR0FBeUI7aUJBQ3pCLFlBQVksQ0FBQyxZQUFiLENBQTBCLElBQTFCO1FBUitDO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE5QixDQUFuQjtJQUZnQyxDQTdGbEM7SUF5R0EsbUJBQUEsRUFBcUIsU0FBQyxZQUFEO0FBQ25CLFVBQUE7TUFBQSxJQUFjLG9CQUFkO0FBQUEsZUFBQTs7TUFDQSxJQUFDLENBQUEsMEJBQUQsQ0FBQTtNQUNBLElBQVUsWUFBWSxDQUFDLFlBQWIsQ0FBQSxDQUFWO0FBQUEsZUFBQTs7TUFFQSxJQUFHLDZCQUFIO1FBRUUsUUFBQSxHQUFXLFlBQVksQ0FBQyxZQUFiLENBQUEsQ0FBQSxHQUE4QixJQUFDLENBQUEsZ0JBQWdCLENBQUMsWUFBbEIsQ0FBQTtRQUN6QyxJQUFBLENBQUEsQ0FBTyxRQUFBLEdBQVcsSUFBQyxDQUFBLGtCQUFaLElBQW1DLFlBQVksQ0FBQyxPQUFiLENBQXFCLElBQUMsQ0FBQSxnQkFBdEIsQ0FBMUMsQ0FBQTtVQUNFLElBQUMsQ0FBQSxvQkFBb0IsQ0FBQyxXQUF0QixDQUFrQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVgsQ0FBbUIsWUFBbkIsQ0FBZ0MsQ0FBQyxPQUFuRTs7Z0JBQ2lCLENBQUUsZUFBbkIsQ0FBbUMsWUFBbkM7V0FGRjtTQUhGO09BQUEsTUFBQTtRQU9FLElBQUMsQ0FBQSxvQkFBb0IsQ0FBQyxXQUF0QixDQUFrQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVgsQ0FBbUIsWUFBbkIsQ0FBZ0MsQ0FBQyxPQUFuRTs7Y0FDaUIsQ0FBRSxlQUFuQixDQUFtQyxZQUFuQztTQVJGOztNQVVBLFlBQVksQ0FBQyxZQUFiLENBQTBCLElBQTFCO2FBQ0EsSUFBQyxDQUFBLGdCQUFELEdBQW9CO0lBaEJELENBekdyQjs7O0VBMkhGLHlCQUFBLEdBQTRCLFNBQUMsS0FBRDtBQUMxQixRQUFBOztNQUFBLG1CQUFvQixPQUFBLENBQVEsbUJBQVI7O0FBQ3BCO0FBQUEsU0FBQSxzQ0FBQTtNQUFLO01BQ0gsSUFBRyxJQUFBLEtBQVEsWUFBUixJQUF3QixFQUFFLENBQUMsVUFBSCxDQUFjLElBQWQsQ0FBM0I7QUFDRSxlQUFPLEtBRFQ7O0FBREY7V0FHQTtFQUwwQjs7RUFPNUIsTUFBTSxDQUFDLE9BQVAsR0FBaUI7QUF6SWpCIiwic291cmNlc0NvbnRlbnQiOlsie05vdGlmaWNhdGlvbiwgQ29tcG9zaXRlRGlzcG9zYWJsZX0gPSByZXF1aXJlICdhdG9tJ1xuZnMgPSByZXF1aXJlICdmcy1wbHVzJ1xuU3RhY2tUcmFjZVBhcnNlciA9IG51bGxcbk5vdGlmaWNhdGlvbkVsZW1lbnQgPSByZXF1aXJlICcuL25vdGlmaWNhdGlvbi1lbGVtZW50J1xuTm90aWZpY2F0aW9uc0xvZyA9IHJlcXVpcmUgJy4vbm90aWZpY2F0aW9ucy1sb2cnXG5cbk5vdGlmaWNhdGlvbnMgPVxuICBpc0luaXRpYWxpemVkOiBmYWxzZVxuICBzdWJzY3JpcHRpb25zOiBudWxsXG4gIGR1cGxpY2F0ZVRpbWVEZWxheTogNTAwXG4gIGxhc3ROb3RpZmljYXRpb246IG51bGxcblxuICBhY3RpdmF0ZTogKHN0YXRlKSAtPlxuICAgIENvbW1hbmRMb2dnZXIgPSByZXF1aXJlICcuL2NvbW1hbmQtbG9nZ2VyJ1xuICAgIENvbW1hbmRMb2dnZXIuc3RhcnQoKVxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcblxuICAgIEBhZGROb3RpZmljYXRpb25WaWV3KG5vdGlmaWNhdGlvbikgZm9yIG5vdGlmaWNhdGlvbiBpbiBhdG9tLm5vdGlmaWNhdGlvbnMuZ2V0Tm90aWZpY2F0aW9ucygpXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20ubm90aWZpY2F0aW9ucy5vbkRpZEFkZE5vdGlmaWNhdGlvbiAobm90aWZpY2F0aW9uKSA9PiBAYWRkTm90aWZpY2F0aW9uVmlldyhub3RpZmljYXRpb24pXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5vbldpbGxUaHJvd0Vycm9yICh7bWVzc2FnZSwgdXJsLCBsaW5lLCBvcmlnaW5hbEVycm9yLCBwcmV2ZW50RGVmYXVsdH0pIC0+XG4gICAgICBpZiBvcmlnaW5hbEVycm9yLm5hbWUgaXMgJ0J1ZmZlcmVkUHJvY2Vzc0Vycm9yJ1xuICAgICAgICBtZXNzYWdlID0gbWVzc2FnZS5yZXBsYWNlKCdVbmNhdWdodCBCdWZmZXJlZFByb2Nlc3NFcnJvcjogJywgJycpXG4gICAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRFcnJvcihtZXNzYWdlLCBkaXNtaXNzYWJsZTogdHJ1ZSlcblxuICAgICAgZWxzZSBpZiBvcmlnaW5hbEVycm9yLmNvZGUgaXMgJ0VOT0VOVCcgYW5kIG5vdCAvXFwvYXRvbS9pLnRlc3QobWVzc2FnZSkgYW5kIG1hdGNoID0gL3NwYXduICguKykgRU5PRU5ULy5leGVjKG1lc3NhZ2UpXG4gICAgICAgIG1lc3NhZ2UgPSBcIlwiXCJcbiAgICAgICAgICAnI3ttYXRjaFsxXX0nIGNvdWxkIG5vdCBiZSBzcGF3bmVkLlxuICAgICAgICAgIElzIGl0IGluc3RhbGxlZCBhbmQgb24geW91ciBwYXRoP1xuICAgICAgICAgIElmIHNvIHBsZWFzZSBvcGVuIGFuIGlzc3VlIG9uIHRoZSBwYWNrYWdlIHNwYXduaW5nIHRoZSBwcm9jZXNzLlxuICAgICAgICBcIlwiXCJcbiAgICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZEVycm9yKG1lc3NhZ2UsIGRpc21pc3NhYmxlOiB0cnVlKVxuXG4gICAgICBlbHNlIGlmIG5vdCBhdG9tLmluRGV2TW9kZSgpIG9yIGF0b20uY29uZmlnLmdldCgnbm90aWZpY2F0aW9ucy5zaG93RXJyb3JzSW5EZXZNb2RlJylcbiAgICAgICAgcHJldmVudERlZmF1bHQoKVxuXG4gICAgICAgICMgSWdub3JlIGVycm9ycyB3aXRoIG5vIHBhdGhzIGluIHRoZW0gc2luY2UgdGhleSBhcmUgaW1wb3NzaWJsZSB0byB0cmFjZVxuICAgICAgICBpZiBvcmlnaW5hbEVycm9yLnN0YWNrIGFuZCBub3QgaXNDb3JlT3JQYWNrYWdlU3RhY2tUcmFjZShvcmlnaW5hbEVycm9yLnN0YWNrKVxuICAgICAgICAgIHJldHVyblxuXG4gICAgICAgIG9wdGlvbnMgPVxuICAgICAgICAgIGRldGFpbDogXCIje3VybH06I3tsaW5lfVwiXG4gICAgICAgICAgc3RhY2s6IG9yaWdpbmFsRXJyb3Iuc3RhY2tcbiAgICAgICAgICBkaXNtaXNzYWJsZTogdHJ1ZVxuICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRmF0YWxFcnJvcihtZXNzYWdlLCBvcHRpb25zKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICdjb3JlOmNhbmNlbCcsIC0+XG4gICAgICBub3RpZmljYXRpb24uZGlzbWlzcygpIGZvciBub3RpZmljYXRpb24gaW4gYXRvbS5ub3RpZmljYXRpb25zLmdldE5vdGlmaWNhdGlvbnMoKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29uZmlnLm9ic2VydmUgJ25vdGlmaWNhdGlvbnMuZGVmYXVsdFRpbWVvdXQnLCAodmFsdWUpID0+IEB2aXNpYmlsaXR5RHVyYXRpb24gPSB2YWx1ZVxuXG4gICAgaWYgYXRvbS5pbkRldk1vZGUoKVxuICAgICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICdub3RpZmljYXRpb25zOnRyaWdnZXItZXJyb3InLCAtPlxuICAgICAgICB0cnlcbiAgICAgICAgICBhYmMgKyAyICMgbm9wZVxuICAgICAgICBjYXRjaCBlcnJvclxuICAgICAgICAgIG9wdGlvbnMgPVxuICAgICAgICAgICAgZGV0YWlsOiBlcnJvci5zdGFjay5zcGxpdCgnXFxuJylbMV1cbiAgICAgICAgICAgIHN0YWNrOiBlcnJvci5zdGFja1xuICAgICAgICAgICAgZGlzbWlzc2FibGU6IHRydWVcbiAgICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRmF0YWxFcnJvcihcIlVuY2F1Z2h0ICN7ZXJyb3Iuc3RhY2suc3BsaXQoJ1xcbicpWzBdfVwiLCBvcHRpb25zKVxuXG4gICAgQGFkZE5vdGlmaWNhdGlvbnNMb2dTdWJzY3JpcHRpb25zKCkgaWYgQG5vdGlmaWNhdGlvbnNMb2c/XG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20ud29ya3NwYWNlLmFkZE9wZW5lciAodXJpKSA9PiBAY3JlYXRlTG9nKCkgaWYgdXJpIGlzIE5vdGlmaWNhdGlvbnNMb2c6OmdldFVSSSgpXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICdub3RpZmljYXRpb25zOnRvZ2dsZS1sb2cnLCAtPiBhdG9tLndvcmtzcGFjZS50b2dnbGUoTm90aWZpY2F0aW9uc0xvZzo6Z2V0VVJJKCkpXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICdub3RpZmljYXRpb25zOmNsZWFyLWxvZycsIC0+XG4gICAgICBmb3Igbm90aWZpY2F0aW9uIGluIGF0b20ubm90aWZpY2F0aW9ucy5nZXROb3RpZmljYXRpb25zKClcbiAgICAgICAgbm90aWZpY2F0aW9uLm9wdGlvbnMuZGlzbWlzc2FibGUgPSB0cnVlXG4gICAgICAgIG5vdGlmaWNhdGlvbi5kaXNtaXNzZWQgPSBmYWxzZVxuICAgICAgICBub3RpZmljYXRpb24uZGlzbWlzcygpXG4gICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuY2xlYXIoKVxuXG4gIGRlYWN0aXZhdGU6IC0+XG4gICAgQHN1YnNjcmlwdGlvbnMuZGlzcG9zZSgpXG4gICAgQG5vdGlmaWNhdGlvbnNFbGVtZW50Py5yZW1vdmUoKVxuICAgIEBub3RpZmljYXRpb25zUGFuZWw/LmRlc3Ryb3koKVxuICAgIEBub3RpZmljYXRpb25zTG9nPy5kZXN0cm95KClcblxuICAgIEBzdWJzY3JpcHRpb25zID0gbnVsbFxuICAgIEBub3RpZmljYXRpb25zRWxlbWVudCA9IG51bGxcbiAgICBAbm90aWZpY2F0aW9uc1BhbmVsID0gbnVsbFxuXG4gICAgQGlzSW5pdGlhbGl6ZWQgPSBmYWxzZVxuXG4gIGluaXRpYWxpemVJZk5vdEluaXRpYWxpemVkOiAtPlxuICAgIHJldHVybiBpZiBAaXNJbml0aWFsaXplZFxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20udmlld3MuYWRkVmlld1Byb3ZpZGVyIE5vdGlmaWNhdGlvbiwgKG1vZGVsKSA9PlxuICAgICAgbmV3IE5vdGlmaWNhdGlvbkVsZW1lbnQobW9kZWwsIEB2aXNpYmlsaXR5RHVyYXRpb24pXG5cbiAgICBAbm90aWZpY2F0aW9uc0VsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhdG9tLW5vdGlmaWNhdGlvbnMnKVxuICAgIGF0b20udmlld3MuZ2V0VmlldyhhdG9tLndvcmtzcGFjZSkuYXBwZW5kQ2hpbGQoQG5vdGlmaWNhdGlvbnNFbGVtZW50KVxuXG4gICAgQGlzSW5pdGlhbGl6ZWQgPSB0cnVlXG5cbiAgY3JlYXRlTG9nOiAoc3RhdGUpIC0+XG4gICAgQG5vdGlmaWNhdGlvbnNMb2cgPSBuZXcgTm90aWZpY2F0aW9uc0xvZyBAZHVwbGljYXRlVGltZURlbGF5LCBzdGF0ZT8udHlwZXNIaWRkZW5cbiAgICBAYWRkTm90aWZpY2F0aW9uc0xvZ1N1YnNjcmlwdGlvbnMoKSBpZiBAc3Vic2NyaXB0aW9ucz9cbiAgICBAbm90aWZpY2F0aW9uc0xvZ1xuXG4gIGFkZE5vdGlmaWNhdGlvbnNMb2dTdWJzY3JpcHRpb25zOiAtPlxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBAbm90aWZpY2F0aW9uc0xvZy5vbkRpZERlc3Ryb3kgPT4gQG5vdGlmaWNhdGlvbnNMb2cgPSBudWxsXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIEBub3RpZmljYXRpb25zTG9nLm9uSXRlbUNsaWNrIChub3RpZmljYXRpb24pID0+XG4gICAgICB2aWV3ID0gYXRvbS52aWV3cy5nZXRWaWV3KG5vdGlmaWNhdGlvbilcbiAgICAgIHZpZXcubWFrZURpc21pc3NhYmxlKClcblxuICAgICAgcmV0dXJuIHVubGVzcyB2aWV3LmVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdyZW1vdmUnKVxuICAgICAgdmlldy5lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ3JlbW92ZScpXG4gICAgICBAbm90aWZpY2F0aW9uc0VsZW1lbnQuYXBwZW5kQ2hpbGQodmlldy5lbGVtZW50KVxuICAgICAgbm90aWZpY2F0aW9uLmRpc21pc3NlZCA9IGZhbHNlXG4gICAgICBub3RpZmljYXRpb24uc2V0RGlzcGxheWVkKHRydWUpXG5cbiAgYWRkTm90aWZpY2F0aW9uVmlldzogKG5vdGlmaWNhdGlvbikgLT5cbiAgICByZXR1cm4gdW5sZXNzIG5vdGlmaWNhdGlvbj9cbiAgICBAaW5pdGlhbGl6ZUlmTm90SW5pdGlhbGl6ZWQoKVxuICAgIHJldHVybiBpZiBub3RpZmljYXRpb24ud2FzRGlzcGxheWVkKClcblxuICAgIGlmIEBsYXN0Tm90aWZpY2F0aW9uP1xuICAgICAgIyBkbyBub3Qgc2hvdyBkdXBsaWNhdGVzIHVubGVzcyBzb21lIGFtb3VudCBvZiB0aW1lIGhhcyBwYXNzZWRcbiAgICAgIHRpbWVTcGFuID0gbm90aWZpY2F0aW9uLmdldFRpbWVzdGFtcCgpIC0gQGxhc3ROb3RpZmljYXRpb24uZ2V0VGltZXN0YW1wKClcbiAgICAgIHVubGVzcyB0aW1lU3BhbiA8IEBkdXBsaWNhdGVUaW1lRGVsYXkgYW5kIG5vdGlmaWNhdGlvbi5pc0VxdWFsKEBsYXN0Tm90aWZpY2F0aW9uKVxuICAgICAgICBAbm90aWZpY2F0aW9uc0VsZW1lbnQuYXBwZW5kQ2hpbGQoYXRvbS52aWV3cy5nZXRWaWV3KG5vdGlmaWNhdGlvbikuZWxlbWVudClcbiAgICAgICAgQG5vdGlmaWNhdGlvbnNMb2c/LmFkZE5vdGlmaWNhdGlvbihub3RpZmljYXRpb24pXG4gICAgZWxzZVxuICAgICAgQG5vdGlmaWNhdGlvbnNFbGVtZW50LmFwcGVuZENoaWxkKGF0b20udmlld3MuZ2V0Vmlldyhub3RpZmljYXRpb24pLmVsZW1lbnQpXG4gICAgICBAbm90aWZpY2F0aW9uc0xvZz8uYWRkTm90aWZpY2F0aW9uKG5vdGlmaWNhdGlvbilcblxuICAgIG5vdGlmaWNhdGlvbi5zZXREaXNwbGF5ZWQodHJ1ZSlcbiAgICBAbGFzdE5vdGlmaWNhdGlvbiA9IG5vdGlmaWNhdGlvblxuXG5pc0NvcmVPclBhY2thZ2VTdGFja1RyYWNlID0gKHN0YWNrKSAtPlxuICBTdGFja1RyYWNlUGFyc2VyID89IHJlcXVpcmUgJ3N0YWNrdHJhY2UtcGFyc2VyJ1xuICBmb3Ige2ZpbGV9IGluIFN0YWNrVHJhY2VQYXJzZXIucGFyc2Uoc3RhY2spXG4gICAgaWYgZmlsZSBpcyAnPGVtYmVkZGVkPicgb3IgZnMuaXNBYnNvbHV0ZShmaWxlKVxuICAgICAgcmV0dXJuIHRydWVcbiAgZmFsc2VcblxubW9kdWxlLmV4cG9ydHMgPSBOb3RpZmljYXRpb25zXG4iXX0=
