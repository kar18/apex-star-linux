(function() {
  var CompositeDisposable, GitRepositoryAsync, GitView, _, ref;

  _ = require("underscore-plus");

  ref = require("atom"), CompositeDisposable = ref.CompositeDisposable, GitRepositoryAsync = ref.GitRepositoryAsync;

  module.exports = GitView = (function() {
    function GitView() {
      this.element = document.createElement('status-bar-git');
      this.element.classList.add('git-view');
      this.createBranchArea();
      this.createCommitsArea();
      this.createStatusArea();
      this.activeItemSubscription = atom.workspace.getCenter().onDidChangeActivePaneItem((function(_this) {
        return function() {
          return _this.subscribeToActiveItem();
        };
      })(this));
      this.projectPathSubscription = atom.project.onDidChangePaths((function(_this) {
        return function() {
          return _this.subscribeToRepositories();
        };
      })(this));
      this.subscribeToRepositories();
      this.subscribeToActiveItem();
    }

    GitView.prototype.createBranchArea = function() {
      var branchIcon;
      this.branchArea = document.createElement('div');
      this.branchArea.classList.add('git-branch', 'inline-block');
      this.element.appendChild(this.branchArea);
      this.element.branchArea = this.branchArea;
      branchIcon = document.createElement('span');
      branchIcon.classList.add('icon', 'icon-git-branch');
      this.branchArea.appendChild(branchIcon);
      this.branchLabel = document.createElement('span');
      this.branchLabel.classList.add('branch-label');
      this.branchArea.appendChild(this.branchLabel);
      return this.element.branchLabel = this.branchLabel;
    };

    GitView.prototype.createCommitsArea = function() {
      this.commitsArea = document.createElement('div');
      this.commitsArea.classList.add('git-commits', 'inline-block');
      this.element.appendChild(this.commitsArea);
      this.commitsAhead = document.createElement('span');
      this.commitsAhead.classList.add('icon', 'icon-arrow-up', 'commits-ahead-label');
      this.commitsArea.appendChild(this.commitsAhead);
      this.commitsBehind = document.createElement('span');
      this.commitsBehind.classList.add('icon', 'icon-arrow-down', 'commits-behind-label');
      return this.commitsArea.appendChild(this.commitsBehind);
    };

    GitView.prototype.createStatusArea = function() {
      this.gitStatus = document.createElement('div');
      this.gitStatus.classList.add('git-status', 'inline-block');
      this.element.appendChild(this.gitStatus);
      this.gitStatusIcon = document.createElement('span');
      this.gitStatusIcon.classList.add('icon');
      this.gitStatus.appendChild(this.gitStatusIcon);
      return this.element.gitStatusIcon = this.gitStatusIcon;
    };

    GitView.prototype.subscribeToActiveItem = function() {
      var activeItem, ref1;
      activeItem = this.getActiveItem();
      if ((ref1 = this.savedSubscription) != null) {
        ref1.dispose();
      }
      this.savedSubscription = activeItem != null ? typeof activeItem.onDidSave === "function" ? activeItem.onDidSave((function(_this) {
        return function() {
          return _this.update();
        };
      })(this)) : void 0 : void 0;
      return this.update();
    };

    GitView.prototype.subscribeToRepositories = function() {
      var i, len, ref1, ref2, repo, results;
      if ((ref1 = this.repositorySubscriptions) != null) {
        ref1.dispose();
      }
      this.repositorySubscriptions = new CompositeDisposable;
      ref2 = atom.project.getRepositories();
      results = [];
      for (i = 0, len = ref2.length; i < len; i++) {
        repo = ref2[i];
        if (!(repo != null)) {
          continue;
        }
        this.repositorySubscriptions.add(repo.onDidChangeStatus((function(_this) {
          return function(arg) {
            var path, status;
            path = arg.path, status = arg.status;
            if (path === _this.getActiveItemPath()) {
              return _this.update();
            }
          };
        })(this)));
        results.push(this.repositorySubscriptions.add(repo.onDidChangeStatuses((function(_this) {
          return function() {
            return _this.update();
          };
        })(this))));
      }
      return results;
    };

    GitView.prototype.destroy = function() {
      var ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8;
      if ((ref1 = this.activeItemSubscription) != null) {
        ref1.dispose();
      }
      if ((ref2 = this.projectPathSubscription) != null) {
        ref2.dispose();
      }
      if ((ref3 = this.savedSubscription) != null) {
        ref3.dispose();
      }
      if ((ref4 = this.repositorySubscriptions) != null) {
        ref4.dispose();
      }
      if ((ref5 = this.branchTooltipDisposable) != null) {
        ref5.dispose();
      }
      if ((ref6 = this.commitsAheadTooltipDisposable) != null) {
        ref6.dispose();
      }
      if ((ref7 = this.commitsBehindTooltipDisposable) != null) {
        ref7.dispose();
      }
      return (ref8 = this.statusTooltipDisposable) != null ? ref8.dispose() : void 0;
    };

    GitView.prototype.getActiveItemPath = function() {
      var ref1;
      return (ref1 = this.getActiveItem()) != null ? typeof ref1.getPath === "function" ? ref1.getPath() : void 0 : void 0;
    };

    GitView.prototype.getRepositoryForActiveItem = function() {
      var i, len, ref1, repo, rootDir, rootDirIndex;
      rootDir = atom.project.relativizePath(this.getActiveItemPath())[0];
      rootDirIndex = atom.project.getPaths().indexOf(rootDir);
      if (rootDirIndex >= 0) {
        return atom.project.getRepositories()[rootDirIndex];
      } else {
        ref1 = atom.project.getRepositories();
        for (i = 0, len = ref1.length; i < len; i++) {
          repo = ref1[i];
          if (repo) {
            return repo;
          }
        }
      }
    };

    GitView.prototype.getActiveItem = function() {
      return atom.workspace.getCenter().getActivePaneItem();
    };

    GitView.prototype.update = function() {
      var repo;
      repo = this.getRepositoryForActiveItem();
      this.updateBranchText(repo);
      this.updateAheadBehindCount(repo);
      return this.updateStatusText(repo);
    };

    GitView.prototype.updateBranchText = function(repo) {
      var head, ref1;
      if (this.showGitInformation(repo)) {
        head = repo.getShortHead(this.getActiveItemPath());
        this.branchLabel.textContent = head;
        if (head) {
          this.branchArea.style.display = '';
        }
        if ((ref1 = this.branchTooltipDisposable) != null) {
          ref1.dispose();
        }
        return this.branchTooltipDisposable = atom.tooltips.add(this.branchArea, {
          title: "On branch " + head
        });
      } else {
        return this.branchArea.style.display = 'none';
      }
    };

    GitView.prototype.showGitInformation = function(repo) {
      var itemPath;
      if (repo == null) {
        return false;
      }
      if (itemPath = this.getActiveItemPath()) {
        return atom.project.contains(itemPath);
      } else {
        return this.getActiveItem() == null;
      }
    };

    GitView.prototype.updateAheadBehindCount = function(repo) {
      var ahead, behind, itemPath, ref1, ref2, ref3;
      if (!this.showGitInformation(repo)) {
        this.commitsArea.style.display = 'none';
        return;
      }
      itemPath = this.getActiveItemPath();
      ref1 = repo.getCachedUpstreamAheadBehindCount(itemPath), ahead = ref1.ahead, behind = ref1.behind;
      if (ahead > 0) {
        this.commitsAhead.textContent = ahead;
        this.commitsAhead.style.display = '';
        if ((ref2 = this.commitsAheadTooltipDisposable) != null) {
          ref2.dispose();
        }
        this.commitsAheadTooltipDisposable = atom.tooltips.add(this.commitsAhead, {
          title: (_.pluralize(ahead, 'commit')) + " ahead of upstream"
        });
      } else {
        this.commitsAhead.style.display = 'none';
      }
      if (behind > 0) {
        this.commitsBehind.textContent = behind;
        this.commitsBehind.style.display = '';
        if ((ref3 = this.commitsBehindTooltipDisposable) != null) {
          ref3.dispose();
        }
        this.commitsBehindTooltipDisposable = atom.tooltips.add(this.commitsBehind, {
          title: (_.pluralize(behind, 'commit')) + " behind upstream"
        });
      } else {
        this.commitsBehind.style.display = 'none';
      }
      if (ahead > 0 || behind > 0) {
        return this.commitsArea.style.display = '';
      } else {
        return this.commitsArea.style.display = 'none';
      }
    };

    GitView.prototype.clearStatus = function() {
      return this.gitStatusIcon.classList.remove('icon-diff-modified', 'status-modified', 'icon-diff-added', 'status-added', 'icon-diff-ignored', 'status-ignored');
    };

    GitView.prototype.updateAsNewFile = function() {
      var textEditor;
      this.clearStatus();
      this.gitStatusIcon.classList.add('icon-diff-added', 'status-added');
      if (textEditor = atom.workspace.getActiveTextEditor()) {
        this.gitStatusIcon.textContent = "+" + (textEditor.getLineCount());
        this.updateTooltipText((_.pluralize(textEditor.getLineCount(), 'line')) + " in this new file not yet committed");
      } else {
        this.gitStatusIcon.textContent = '';
        this.updateTooltipText();
      }
      return this.gitStatus.style.display = '';
    };

    GitView.prototype.updateAsModifiedFile = function(repo, path) {
      var stats;
      stats = repo.getDiffStats(path);
      this.clearStatus();
      this.gitStatusIcon.classList.add('icon-diff-modified', 'status-modified');
      if (stats.added && stats.deleted) {
        this.gitStatusIcon.textContent = "+" + stats.added + ", -" + stats.deleted;
        this.updateTooltipText((_.pluralize(stats.added, 'line')) + " added and " + (_.pluralize(stats.deleted, 'line')) + " deleted in this file not yet committed");
      } else if (stats.added) {
        this.gitStatusIcon.textContent = "+" + stats.added;
        this.updateTooltipText((_.pluralize(stats.added, 'line')) + " added to this file not yet committed");
      } else if (stats.deleted) {
        this.gitStatusIcon.textContent = "-" + stats.deleted;
        this.updateTooltipText((_.pluralize(stats.deleted, 'line')) + " deleted from this file not yet committed");
      } else {
        this.gitStatusIcon.textContent = '';
        this.updateTooltipText();
      }
      return this.gitStatus.style.display = '';
    };

    GitView.prototype.updateAsIgnoredFile = function() {
      this.clearStatus();
      this.gitStatusIcon.classList.add('icon-diff-ignored', 'status-ignored');
      this.gitStatusIcon.textContent = '';
      this.gitStatus.style.display = '';
      return this.updateTooltipText("File is ignored by git");
    };

    GitView.prototype.updateTooltipText = function(text) {
      var ref1;
      if ((ref1 = this.statusTooltipDisposable) != null) {
        ref1.dispose();
      }
      if (text) {
        return this.statusTooltipDisposable = atom.tooltips.add(this.gitStatusIcon, {
          title: text
        });
      }
    };

    GitView.prototype.updateStatusText = function(repo) {
      var hideStatus, itemPath, ref1, status;
      hideStatus = (function(_this) {
        return function() {
          _this.clearStatus();
          return _this.gitStatus.style.display = 'none';
        };
      })(this);
      itemPath = this.getActiveItemPath();
      if (this.showGitInformation(repo) && (itemPath != null)) {
        status = (ref1 = repo.getCachedPathStatus(itemPath)) != null ? ref1 : 0;
        if (repo.isStatusNew(status)) {
          return this.updateAsNewFile();
        }
        if (repo.isStatusModified(status)) {
          return this.updateAsModifiedFile(repo, itemPath);
        }
        if (repo.isPathIgnored(itemPath)) {
          return this.updateAsIgnoredFile();
        } else {
          return hideStatus();
        }
      } else {
        return hideStatus();
      }
    };

    return GitView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zdGF0dXMtYmFyL2xpYi9naXQtdmlldy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLENBQUEsR0FBSSxPQUFBLENBQVEsaUJBQVI7O0VBQ0osTUFBNEMsT0FBQSxDQUFRLE1BQVIsQ0FBNUMsRUFBQyw2Q0FBRCxFQUFzQjs7RUFFdEIsTUFBTSxDQUFDLE9BQVAsR0FDTTtJQUNTLGlCQUFBO01BQ1gsSUFBQyxDQUFBLE9BQUQsR0FBVyxRQUFRLENBQUMsYUFBVCxDQUF1QixnQkFBdkI7TUFDWCxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixVQUF2QjtNQUVBLElBQUMsQ0FBQSxnQkFBRCxDQUFBO01BQ0EsSUFBQyxDQUFBLGlCQUFELENBQUE7TUFDQSxJQUFDLENBQUEsZ0JBQUQsQ0FBQTtNQUVBLElBQUMsQ0FBQSxzQkFBRCxHQUEwQixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQWYsQ0FBQSxDQUEwQixDQUFDLHlCQUEzQixDQUFxRCxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQzdFLEtBQUMsQ0FBQSxxQkFBRCxDQUFBO1FBRDZFO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFyRDtNQUUxQixJQUFDLENBQUEsdUJBQUQsR0FBMkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBYixDQUE4QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQ3ZELEtBQUMsQ0FBQSx1QkFBRCxDQUFBO1FBRHVEO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE5QjtNQUUzQixJQUFDLENBQUEsdUJBQUQsQ0FBQTtNQUNBLElBQUMsQ0FBQSxxQkFBRCxDQUFBO0lBYlc7O3NCQWViLGdCQUFBLEdBQWtCLFNBQUE7QUFDaEIsVUFBQTtNQUFBLElBQUMsQ0FBQSxVQUFELEdBQWMsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDZCxJQUFDLENBQUEsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUF0QixDQUEwQixZQUExQixFQUF3QyxjQUF4QztNQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixJQUFDLENBQUEsVUFBdEI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFVBQVQsR0FBc0IsSUFBQyxDQUFBO01BRXZCLFVBQUEsR0FBYSxRQUFRLENBQUMsYUFBVCxDQUF1QixNQUF2QjtNQUNiLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBckIsQ0FBeUIsTUFBekIsRUFBaUMsaUJBQWpDO01BQ0EsSUFBQyxDQUFBLFVBQVUsQ0FBQyxXQUFaLENBQXdCLFVBQXhCO01BRUEsSUFBQyxDQUFBLFdBQUQsR0FBZSxRQUFRLENBQUMsYUFBVCxDQUF1QixNQUF2QjtNQUNmLElBQUMsQ0FBQSxXQUFXLENBQUMsU0FBUyxDQUFDLEdBQXZCLENBQTJCLGNBQTNCO01BQ0EsSUFBQyxDQUFBLFVBQVUsQ0FBQyxXQUFaLENBQXdCLElBQUMsQ0FBQSxXQUF6QjthQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxHQUF1QixJQUFDLENBQUE7SUFiUjs7c0JBZWxCLGlCQUFBLEdBQW1CLFNBQUE7TUFDakIsSUFBQyxDQUFBLFdBQUQsR0FBZSxRQUFRLENBQUMsYUFBVCxDQUF1QixLQUF2QjtNQUNmLElBQUMsQ0FBQSxXQUFXLENBQUMsU0FBUyxDQUFDLEdBQXZCLENBQTJCLGFBQTNCLEVBQTBDLGNBQTFDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLElBQUMsQ0FBQSxXQUF0QjtNQUVBLElBQUMsQ0FBQSxZQUFELEdBQWdCLFFBQVEsQ0FBQyxhQUFULENBQXVCLE1BQXZCO01BQ2hCLElBQUMsQ0FBQSxZQUFZLENBQUMsU0FBUyxDQUFDLEdBQXhCLENBQTRCLE1BQTVCLEVBQW9DLGVBQXBDLEVBQXFELHFCQUFyRDtNQUNBLElBQUMsQ0FBQSxXQUFXLENBQUMsV0FBYixDQUF5QixJQUFDLENBQUEsWUFBMUI7TUFFQSxJQUFDLENBQUEsYUFBRCxHQUFpQixRQUFRLENBQUMsYUFBVCxDQUF1QixNQUF2QjtNQUNqQixJQUFDLENBQUEsYUFBYSxDQUFDLFNBQVMsQ0FBQyxHQUF6QixDQUE2QixNQUE3QixFQUFxQyxpQkFBckMsRUFBd0Qsc0JBQXhEO2FBQ0EsSUFBQyxDQUFBLFdBQVcsQ0FBQyxXQUFiLENBQXlCLElBQUMsQ0FBQSxhQUExQjtJQVhpQjs7c0JBYW5CLGdCQUFBLEdBQWtCLFNBQUE7TUFDaEIsSUFBQyxDQUFBLFNBQUQsR0FBYSxRQUFRLENBQUMsYUFBVCxDQUF1QixLQUF2QjtNQUNiLElBQUMsQ0FBQSxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQXJCLENBQXlCLFlBQXpCLEVBQXVDLGNBQXZDO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLElBQUMsQ0FBQSxTQUF0QjtNQUVBLElBQUMsQ0FBQSxhQUFELEdBQWlCLFFBQVEsQ0FBQyxhQUFULENBQXVCLE1BQXZCO01BQ2pCLElBQUMsQ0FBQSxhQUFhLENBQUMsU0FBUyxDQUFDLEdBQXpCLENBQTZCLE1BQTdCO01BQ0EsSUFBQyxDQUFBLFNBQVMsQ0FBQyxXQUFYLENBQXVCLElBQUMsQ0FBQSxhQUF4QjthQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxHQUF5QixJQUFDLENBQUE7SUFSVjs7c0JBVWxCLHFCQUFBLEdBQXVCLFNBQUE7QUFDckIsVUFBQTtNQUFBLFVBQUEsR0FBYSxJQUFDLENBQUEsYUFBRCxDQUFBOztZQUVLLENBQUUsT0FBcEIsQ0FBQTs7TUFDQSxJQUFDLENBQUEsaUJBQUQscUVBQXFCLFVBQVUsQ0FBRSxVQUFXLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsTUFBRCxDQUFBO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBO2FBRTVDLElBQUMsQ0FBQSxNQUFELENBQUE7SUFOcUI7O3NCQVF2Qix1QkFBQSxHQUF5QixTQUFBO0FBQ3ZCLFVBQUE7O1lBQXdCLENBQUUsT0FBMUIsQ0FBQTs7TUFDQSxJQUFDLENBQUEsdUJBQUQsR0FBMkIsSUFBSTtBQUUvQjtBQUFBO1dBQUEsc0NBQUE7O2NBQWdEOzs7UUFDOUMsSUFBQyxDQUFBLHVCQUF1QixDQUFDLEdBQXpCLENBQTZCLElBQUksQ0FBQyxpQkFBTCxDQUF1QixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFDLEdBQUQ7QUFDbEQsZ0JBQUE7WUFEb0QsaUJBQU07WUFDMUQsSUFBYSxJQUFBLEtBQVEsS0FBQyxDQUFBLGlCQUFELENBQUEsQ0FBckI7cUJBQUEsS0FBQyxDQUFBLE1BQUQsQ0FBQSxFQUFBOztVQURrRDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBdkIsQ0FBN0I7cUJBRUEsSUFBQyxDQUFBLHVCQUF1QixDQUFDLEdBQXpCLENBQTZCLElBQUksQ0FBQyxtQkFBTCxDQUF5QixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUNwRCxLQUFDLENBQUEsTUFBRCxDQUFBO1VBRG9EO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6QixDQUE3QjtBQUhGOztJQUp1Qjs7c0JBVXpCLE9BQUEsR0FBUyxTQUFBO0FBQ1AsVUFBQTs7WUFBdUIsQ0FBRSxPQUF6QixDQUFBOzs7WUFDd0IsQ0FBRSxPQUExQixDQUFBOzs7WUFDa0IsQ0FBRSxPQUFwQixDQUFBOzs7WUFDd0IsQ0FBRSxPQUExQixDQUFBOzs7WUFDd0IsQ0FBRSxPQUExQixDQUFBOzs7WUFDOEIsQ0FBRSxPQUFoQyxDQUFBOzs7WUFDK0IsQ0FBRSxPQUFqQyxDQUFBOztpRUFDd0IsQ0FBRSxPQUExQixDQUFBO0lBUk87O3NCQVVULGlCQUFBLEdBQW1CLFNBQUE7QUFDakIsVUFBQTs4RkFBZ0IsQ0FBRTtJQUREOztzQkFHbkIsMEJBQUEsR0FBNEIsU0FBQTtBQUMxQixVQUFBO01BQUMsVUFBVyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWIsQ0FBNEIsSUFBQyxDQUFBLGlCQUFELENBQUEsQ0FBNUI7TUFDWixZQUFBLEdBQWUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFiLENBQUEsQ0FBdUIsQ0FBQyxPQUF4QixDQUFnQyxPQUFoQztNQUNmLElBQUcsWUFBQSxJQUFnQixDQUFuQjtlQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBYixDQUFBLENBQStCLENBQUEsWUFBQSxFQURqQztPQUFBLE1BQUE7QUFHRTtBQUFBLGFBQUEsc0NBQUE7O2NBQWdEO0FBQzlDLG1CQUFPOztBQURULFNBSEY7O0lBSDBCOztzQkFTNUIsYUFBQSxHQUFlLFNBQUE7YUFDYixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQWYsQ0FBQSxDQUEwQixDQUFDLGlCQUEzQixDQUFBO0lBRGE7O3NCQUdmLE1BQUEsR0FBUSxTQUFBO0FBQ04sVUFBQTtNQUFBLElBQUEsR0FBTyxJQUFDLENBQUEsMEJBQUQsQ0FBQTtNQUNQLElBQUMsQ0FBQSxnQkFBRCxDQUFrQixJQUFsQjtNQUNBLElBQUMsQ0FBQSxzQkFBRCxDQUF3QixJQUF4QjthQUNBLElBQUMsQ0FBQSxnQkFBRCxDQUFrQixJQUFsQjtJQUpNOztzQkFNUixnQkFBQSxHQUFrQixTQUFDLElBQUQ7QUFDaEIsVUFBQTtNQUFBLElBQUcsSUFBQyxDQUFBLGtCQUFELENBQW9CLElBQXBCLENBQUg7UUFDRSxJQUFBLEdBQU8sSUFBSSxDQUFDLFlBQUwsQ0FBa0IsSUFBQyxDQUFBLGlCQUFELENBQUEsQ0FBbEI7UUFDUCxJQUFDLENBQUEsV0FBVyxDQUFDLFdBQWIsR0FBMkI7UUFDM0IsSUFBa0MsSUFBbEM7VUFBQSxJQUFDLENBQUEsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFsQixHQUE0QixHQUE1Qjs7O2NBQ3dCLENBQUUsT0FBMUIsQ0FBQTs7ZUFDQSxJQUFDLENBQUEsdUJBQUQsR0FBMkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLElBQUMsQ0FBQSxVQUFuQixFQUErQjtVQUFBLEtBQUEsRUFBTyxZQUFBLEdBQWEsSUFBcEI7U0FBL0IsRUFMN0I7T0FBQSxNQUFBO2VBT0UsSUFBQyxDQUFBLFVBQVUsQ0FBQyxLQUFLLENBQUMsT0FBbEIsR0FBNEIsT0FQOUI7O0lBRGdCOztzQkFVbEIsa0JBQUEsR0FBb0IsU0FBQyxJQUFEO0FBQ2xCLFVBQUE7TUFBQSxJQUFvQixZQUFwQjtBQUFBLGVBQU8sTUFBUDs7TUFFQSxJQUFHLFFBQUEsR0FBVyxJQUFDLENBQUEsaUJBQUQsQ0FBQSxDQUFkO2VBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFiLENBQXNCLFFBQXRCLEVBREY7T0FBQSxNQUFBO2VBR00sNkJBSE47O0lBSGtCOztzQkFRcEIsc0JBQUEsR0FBd0IsU0FBQyxJQUFEO0FBQ3RCLFVBQUE7TUFBQSxJQUFBLENBQU8sSUFBQyxDQUFBLGtCQUFELENBQW9CLElBQXBCLENBQVA7UUFDRSxJQUFDLENBQUEsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFuQixHQUE2QjtBQUM3QixlQUZGOztNQUlBLFFBQUEsR0FBVyxJQUFDLENBQUEsaUJBQUQsQ0FBQTtNQUNYLE9BQWtCLElBQUksQ0FBQyxpQ0FBTCxDQUF1QyxRQUF2QyxDQUFsQixFQUFDLGtCQUFELEVBQVE7TUFDUixJQUFHLEtBQUEsR0FBUSxDQUFYO1FBQ0UsSUFBQyxDQUFBLFlBQVksQ0FBQyxXQUFkLEdBQTRCO1FBQzVCLElBQUMsQ0FBQSxZQUFZLENBQUMsS0FBSyxDQUFDLE9BQXBCLEdBQThCOztjQUNBLENBQUUsT0FBaEMsQ0FBQTs7UUFDQSxJQUFDLENBQUEsNkJBQUQsR0FBaUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLElBQUMsQ0FBQSxZQUFuQixFQUFpQztVQUFBLEtBQUEsRUFBUyxDQUFDLENBQUMsQ0FBQyxTQUFGLENBQVksS0FBWixFQUFtQixRQUFuQixDQUFELENBQUEsR0FBOEIsb0JBQXZDO1NBQWpDLEVBSm5DO09BQUEsTUFBQTtRQU1FLElBQUMsQ0FBQSxZQUFZLENBQUMsS0FBSyxDQUFDLE9BQXBCLEdBQThCLE9BTmhDOztNQVFBLElBQUcsTUFBQSxHQUFTLENBQVo7UUFDRSxJQUFDLENBQUEsYUFBYSxDQUFDLFdBQWYsR0FBNkI7UUFDN0IsSUFBQyxDQUFBLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBckIsR0FBK0I7O2NBQ0EsQ0FBRSxPQUFqQyxDQUFBOztRQUNBLElBQUMsQ0FBQSw4QkFBRCxHQUFrQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsSUFBQyxDQUFBLGFBQW5CLEVBQWtDO1VBQUEsS0FBQSxFQUFTLENBQUMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxNQUFaLEVBQW9CLFFBQXBCLENBQUQsQ0FBQSxHQUErQixrQkFBeEM7U0FBbEMsRUFKcEM7T0FBQSxNQUFBO1FBTUUsSUFBQyxDQUFBLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBckIsR0FBK0IsT0FOakM7O01BUUEsSUFBRyxLQUFBLEdBQVEsQ0FBUixJQUFhLE1BQUEsR0FBUyxDQUF6QjtlQUNFLElBQUMsQ0FBQSxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQW5CLEdBQTZCLEdBRC9CO09BQUEsTUFBQTtlQUdFLElBQUMsQ0FBQSxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQW5CLEdBQTZCLE9BSC9COztJQXZCc0I7O3NCQTRCeEIsV0FBQSxHQUFhLFNBQUE7YUFDWCxJQUFDLENBQUEsYUFBYSxDQUFDLFNBQVMsQ0FBQyxNQUF6QixDQUFnQyxvQkFBaEMsRUFBc0QsaUJBQXRELEVBQXlFLGlCQUF6RSxFQUE0RixjQUE1RixFQUE0RyxtQkFBNUcsRUFBaUksZ0JBQWpJO0lBRFc7O3NCQUdiLGVBQUEsR0FBaUIsU0FBQTtBQUNmLFVBQUE7TUFBQSxJQUFDLENBQUEsV0FBRCxDQUFBO01BRUEsSUFBQyxDQUFBLGFBQWEsQ0FBQyxTQUFTLENBQUMsR0FBekIsQ0FBNkIsaUJBQTdCLEVBQWdELGNBQWhEO01BQ0EsSUFBRyxVQUFBLEdBQWEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBZixDQUFBLENBQWhCO1FBQ0UsSUFBQyxDQUFBLGFBQWEsQ0FBQyxXQUFmLEdBQTZCLEdBQUEsR0FBRyxDQUFDLFVBQVUsQ0FBQyxZQUFYLENBQUEsQ0FBRDtRQUNoQyxJQUFDLENBQUEsaUJBQUQsQ0FBcUIsQ0FBQyxDQUFDLENBQUMsU0FBRixDQUFZLFVBQVUsQ0FBQyxZQUFYLENBQUEsQ0FBWixFQUF1QyxNQUF2QyxDQUFELENBQUEsR0FBZ0QscUNBQXJFLEVBRkY7T0FBQSxNQUFBO1FBSUUsSUFBQyxDQUFBLGFBQWEsQ0FBQyxXQUFmLEdBQTZCO1FBQzdCLElBQUMsQ0FBQSxpQkFBRCxDQUFBLEVBTEY7O2FBT0EsSUFBQyxDQUFBLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBakIsR0FBMkI7SUFYWjs7c0JBYWpCLG9CQUFBLEdBQXNCLFNBQUMsSUFBRCxFQUFPLElBQVA7QUFDcEIsVUFBQTtNQUFBLEtBQUEsR0FBUSxJQUFJLENBQUMsWUFBTCxDQUFrQixJQUFsQjtNQUNSLElBQUMsQ0FBQSxXQUFELENBQUE7TUFFQSxJQUFDLENBQUEsYUFBYSxDQUFDLFNBQVMsQ0FBQyxHQUF6QixDQUE2QixvQkFBN0IsRUFBbUQsaUJBQW5EO01BQ0EsSUFBRyxLQUFLLENBQUMsS0FBTixJQUFnQixLQUFLLENBQUMsT0FBekI7UUFDRSxJQUFDLENBQUEsYUFBYSxDQUFDLFdBQWYsR0FBNkIsR0FBQSxHQUFJLEtBQUssQ0FBQyxLQUFWLEdBQWdCLEtBQWhCLEdBQXFCLEtBQUssQ0FBQztRQUN4RCxJQUFDLENBQUEsaUJBQUQsQ0FBcUIsQ0FBQyxDQUFDLENBQUMsU0FBRixDQUFZLEtBQUssQ0FBQyxLQUFsQixFQUF5QixNQUF6QixDQUFELENBQUEsR0FBa0MsYUFBbEMsR0FBOEMsQ0FBQyxDQUFDLENBQUMsU0FBRixDQUFZLEtBQUssQ0FBQyxPQUFsQixFQUEyQixNQUEzQixDQUFELENBQTlDLEdBQWtGLHlDQUF2RyxFQUZGO09BQUEsTUFHSyxJQUFHLEtBQUssQ0FBQyxLQUFUO1FBQ0gsSUFBQyxDQUFBLGFBQWEsQ0FBQyxXQUFmLEdBQTZCLEdBQUEsR0FBSSxLQUFLLENBQUM7UUFDdkMsSUFBQyxDQUFBLGlCQUFELENBQXFCLENBQUMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxLQUFLLENBQUMsS0FBbEIsRUFBeUIsTUFBekIsQ0FBRCxDQUFBLEdBQWtDLHVDQUF2RCxFQUZHO09BQUEsTUFHQSxJQUFHLEtBQUssQ0FBQyxPQUFUO1FBQ0gsSUFBQyxDQUFBLGFBQWEsQ0FBQyxXQUFmLEdBQTZCLEdBQUEsR0FBSSxLQUFLLENBQUM7UUFDdkMsSUFBQyxDQUFBLGlCQUFELENBQXFCLENBQUMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxLQUFLLENBQUMsT0FBbEIsRUFBMkIsTUFBM0IsQ0FBRCxDQUFBLEdBQW9DLDJDQUF6RCxFQUZHO09BQUEsTUFBQTtRQUlILElBQUMsQ0FBQSxhQUFhLENBQUMsV0FBZixHQUE2QjtRQUM3QixJQUFDLENBQUEsaUJBQUQsQ0FBQSxFQUxHOzthQU9MLElBQUMsQ0FBQSxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQWpCLEdBQTJCO0lBbEJQOztzQkFvQnRCLG1CQUFBLEdBQXFCLFNBQUE7TUFDbkIsSUFBQyxDQUFBLFdBQUQsQ0FBQTtNQUVBLElBQUMsQ0FBQSxhQUFhLENBQUMsU0FBUyxDQUFDLEdBQXpCLENBQTZCLG1CQUE3QixFQUFtRCxnQkFBbkQ7TUFDQSxJQUFDLENBQUEsYUFBYSxDQUFDLFdBQWYsR0FBNkI7TUFDN0IsSUFBQyxDQUFBLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBakIsR0FBMkI7YUFDM0IsSUFBQyxDQUFBLGlCQUFELENBQW1CLHdCQUFuQjtJQU5tQjs7c0JBUXJCLGlCQUFBLEdBQW1CLFNBQUMsSUFBRDtBQUNqQixVQUFBOztZQUF3QixDQUFFLE9BQTFCLENBQUE7O01BQ0EsSUFBRyxJQUFIO2VBQ0UsSUFBQyxDQUFBLHVCQUFELEdBQTJCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixJQUFDLENBQUEsYUFBbkIsRUFBa0M7VUFBQSxLQUFBLEVBQU8sSUFBUDtTQUFsQyxFQUQ3Qjs7SUFGaUI7O3NCQUtuQixnQkFBQSxHQUFrQixTQUFDLElBQUQ7QUFDaEIsVUFBQTtNQUFBLFVBQUEsR0FBYSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDWCxLQUFDLENBQUEsV0FBRCxDQUFBO2lCQUNBLEtBQUMsQ0FBQSxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQWpCLEdBQTJCO1FBRmhCO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQTtNQUliLFFBQUEsR0FBVyxJQUFDLENBQUEsaUJBQUQsQ0FBQTtNQUNYLElBQUcsSUFBQyxDQUFBLGtCQUFELENBQW9CLElBQXBCLENBQUEsSUFBOEIsa0JBQWpDO1FBQ0UsTUFBQSxnRUFBOEM7UUFDOUMsSUFBRyxJQUFJLENBQUMsV0FBTCxDQUFpQixNQUFqQixDQUFIO0FBQ0UsaUJBQU8sSUFBQyxDQUFBLGVBQUQsQ0FBQSxFQURUOztRQUdBLElBQUcsSUFBSSxDQUFDLGdCQUFMLENBQXNCLE1BQXRCLENBQUg7QUFDRSxpQkFBTyxJQUFDLENBQUEsb0JBQUQsQ0FBc0IsSUFBdEIsRUFBNEIsUUFBNUIsRUFEVDs7UUFHQSxJQUFHLElBQUksQ0FBQyxhQUFMLENBQW1CLFFBQW5CLENBQUg7aUJBQ0UsSUFBQyxDQUFBLG1CQUFELENBQUEsRUFERjtTQUFBLE1BQUE7aUJBR0UsVUFBQSxDQUFBLEVBSEY7U0FSRjtPQUFBLE1BQUE7ZUFhRSxVQUFBLENBQUEsRUFiRjs7SUFOZ0I7Ozs7O0FBMU1wQiIsInNvdXJjZXNDb250ZW50IjpbIl8gPSByZXF1aXJlIFwidW5kZXJzY29yZS1wbHVzXCJcbntDb21wb3NpdGVEaXNwb3NhYmxlLCBHaXRSZXBvc2l0b3J5QXN5bmN9ID0gcmVxdWlyZSBcImF0b21cIlxuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBHaXRWaWV3XG4gIGNvbnN0cnVjdG9yOiAtPlxuICAgIEBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3RhdHVzLWJhci1naXQnKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2dpdC12aWV3JylcblxuICAgIEBjcmVhdGVCcmFuY2hBcmVhKClcbiAgICBAY3JlYXRlQ29tbWl0c0FyZWEoKVxuICAgIEBjcmVhdGVTdGF0dXNBcmVhKClcblxuICAgIEBhY3RpdmVJdGVtU3Vic2NyaXB0aW9uID0gYXRvbS53b3Jrc3BhY2UuZ2V0Q2VudGVyKCkub25EaWRDaGFuZ2VBY3RpdmVQYW5lSXRlbSA9PlxuICAgICAgQHN1YnNjcmliZVRvQWN0aXZlSXRlbSgpXG4gICAgQHByb2plY3RQYXRoU3Vic2NyaXB0aW9uID0gYXRvbS5wcm9qZWN0Lm9uRGlkQ2hhbmdlUGF0aHMgPT5cbiAgICAgIEBzdWJzY3JpYmVUb1JlcG9zaXRvcmllcygpXG4gICAgQHN1YnNjcmliZVRvUmVwb3NpdG9yaWVzKClcbiAgICBAc3Vic2NyaWJlVG9BY3RpdmVJdGVtKClcblxuICBjcmVhdGVCcmFuY2hBcmVhOiAtPlxuICAgIEBicmFuY2hBcmVhID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICBAYnJhbmNoQXJlYS5jbGFzc0xpc3QuYWRkKCdnaXQtYnJhbmNoJywgJ2lubGluZS1ibG9jaycpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQGJyYW5jaEFyZWEpXG4gICAgQGVsZW1lbnQuYnJhbmNoQXJlYSA9IEBicmFuY2hBcmVhXG5cbiAgICBicmFuY2hJY29uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpXG4gICAgYnJhbmNoSWNvbi5jbGFzc0xpc3QuYWRkKCdpY29uJywgJ2ljb24tZ2l0LWJyYW5jaCcpXG4gICAgQGJyYW5jaEFyZWEuYXBwZW5kQ2hpbGQoYnJhbmNoSWNvbilcblxuICAgIEBicmFuY2hMYWJlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKVxuICAgIEBicmFuY2hMYWJlbC5jbGFzc0xpc3QuYWRkKCdicmFuY2gtbGFiZWwnKVxuICAgIEBicmFuY2hBcmVhLmFwcGVuZENoaWxkKEBicmFuY2hMYWJlbClcbiAgICBAZWxlbWVudC5icmFuY2hMYWJlbCA9IEBicmFuY2hMYWJlbFxuXG4gIGNyZWF0ZUNvbW1pdHNBcmVhOiAtPlxuICAgIEBjb21taXRzQXJlYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgQGNvbW1pdHNBcmVhLmNsYXNzTGlzdC5hZGQoJ2dpdC1jb21taXRzJywgJ2lubGluZS1ibG9jaycpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQGNvbW1pdHNBcmVhKVxuXG4gICAgQGNvbW1pdHNBaGVhZCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKVxuICAgIEBjb21taXRzQWhlYWQuY2xhc3NMaXN0LmFkZCgnaWNvbicsICdpY29uLWFycm93LXVwJywgJ2NvbW1pdHMtYWhlYWQtbGFiZWwnKVxuICAgIEBjb21taXRzQXJlYS5hcHBlbmRDaGlsZChAY29tbWl0c0FoZWFkKVxuXG4gICAgQGNvbW1pdHNCZWhpbmQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzcGFuJylcbiAgICBAY29tbWl0c0JlaGluZC5jbGFzc0xpc3QuYWRkKCdpY29uJywgJ2ljb24tYXJyb3ctZG93bicsICdjb21taXRzLWJlaGluZC1sYWJlbCcpXG4gICAgQGNvbW1pdHNBcmVhLmFwcGVuZENoaWxkKEBjb21taXRzQmVoaW5kKVxuXG4gIGNyZWF0ZVN0YXR1c0FyZWE6IC0+XG4gICAgQGdpdFN0YXR1cyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgQGdpdFN0YXR1cy5jbGFzc0xpc3QuYWRkKCdnaXQtc3RhdHVzJywgJ2lubGluZS1ibG9jaycpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQGdpdFN0YXR1cylcblxuICAgIEBnaXRTdGF0dXNJY29uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpXG4gICAgQGdpdFN0YXR1c0ljb24uY2xhc3NMaXN0LmFkZCgnaWNvbicpXG4gICAgQGdpdFN0YXR1cy5hcHBlbmRDaGlsZChAZ2l0U3RhdHVzSWNvbilcbiAgICBAZWxlbWVudC5naXRTdGF0dXNJY29uID0gQGdpdFN0YXR1c0ljb25cblxuICBzdWJzY3JpYmVUb0FjdGl2ZUl0ZW06IC0+XG4gICAgYWN0aXZlSXRlbSA9IEBnZXRBY3RpdmVJdGVtKClcblxuICAgIEBzYXZlZFN1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG4gICAgQHNhdmVkU3Vic2NyaXB0aW9uID0gYWN0aXZlSXRlbT8ub25EaWRTYXZlPyA9PiBAdXBkYXRlKClcblxuICAgIEB1cGRhdGUoKVxuXG4gIHN1YnNjcmliZVRvUmVwb3NpdG9yaWVzOiAtPlxuICAgIEByZXBvc2l0b3J5U3Vic2NyaXB0aW9ucz8uZGlzcG9zZSgpXG4gICAgQHJlcG9zaXRvcnlTdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcblxuICAgIGZvciByZXBvIGluIGF0b20ucHJvamVjdC5nZXRSZXBvc2l0b3JpZXMoKSB3aGVuIHJlcG8/XG4gICAgICBAcmVwb3NpdG9yeVN1YnNjcmlwdGlvbnMuYWRkIHJlcG8ub25EaWRDaGFuZ2VTdGF0dXMgKHtwYXRoLCBzdGF0dXN9KSA9PlxuICAgICAgICBAdXBkYXRlKCkgaWYgcGF0aCBpcyBAZ2V0QWN0aXZlSXRlbVBhdGgoKVxuICAgICAgQHJlcG9zaXRvcnlTdWJzY3JpcHRpb25zLmFkZCByZXBvLm9uRGlkQ2hhbmdlU3RhdHVzZXMgPT5cbiAgICAgICAgQHVwZGF0ZSgpXG5cbiAgZGVzdHJveTogLT5cbiAgICBAYWN0aXZlSXRlbVN1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG4gICAgQHByb2plY3RQYXRoU3Vic2NyaXB0aW9uPy5kaXNwb3NlKClcbiAgICBAc2F2ZWRTdWJzY3JpcHRpb24/LmRpc3Bvc2UoKVxuICAgIEByZXBvc2l0b3J5U3Vic2NyaXB0aW9ucz8uZGlzcG9zZSgpXG4gICAgQGJyYW5jaFRvb2x0aXBEaXNwb3NhYmxlPy5kaXNwb3NlKClcbiAgICBAY29tbWl0c0FoZWFkVG9vbHRpcERpc3Bvc2FibGU/LmRpc3Bvc2UoKVxuICAgIEBjb21taXRzQmVoaW5kVG9vbHRpcERpc3Bvc2FibGU/LmRpc3Bvc2UoKVxuICAgIEBzdGF0dXNUb29sdGlwRGlzcG9zYWJsZT8uZGlzcG9zZSgpXG5cbiAgZ2V0QWN0aXZlSXRlbVBhdGg6IC0+XG4gICAgQGdldEFjdGl2ZUl0ZW0oKT8uZ2V0UGF0aD8oKVxuXG4gIGdldFJlcG9zaXRvcnlGb3JBY3RpdmVJdGVtOiAtPlxuICAgIFtyb290RGlyXSA9IGF0b20ucHJvamVjdC5yZWxhdGl2aXplUGF0aChAZ2V0QWN0aXZlSXRlbVBhdGgoKSlcbiAgICByb290RGlySW5kZXggPSBhdG9tLnByb2plY3QuZ2V0UGF0aHMoKS5pbmRleE9mKHJvb3REaXIpXG4gICAgaWYgcm9vdERpckluZGV4ID49IDBcbiAgICAgIGF0b20ucHJvamVjdC5nZXRSZXBvc2l0b3JpZXMoKVtyb290RGlySW5kZXhdXG4gICAgZWxzZVxuICAgICAgZm9yIHJlcG8gaW4gYXRvbS5wcm9qZWN0LmdldFJlcG9zaXRvcmllcygpIHdoZW4gcmVwb1xuICAgICAgICByZXR1cm4gcmVwb1xuXG4gIGdldEFjdGl2ZUl0ZW06IC0+XG4gICAgYXRvbS53b3Jrc3BhY2UuZ2V0Q2VudGVyKCkuZ2V0QWN0aXZlUGFuZUl0ZW0oKVxuXG4gIHVwZGF0ZTogLT5cbiAgICByZXBvID0gQGdldFJlcG9zaXRvcnlGb3JBY3RpdmVJdGVtKClcbiAgICBAdXBkYXRlQnJhbmNoVGV4dChyZXBvKVxuICAgIEB1cGRhdGVBaGVhZEJlaGluZENvdW50KHJlcG8pXG4gICAgQHVwZGF0ZVN0YXR1c1RleHQocmVwbylcblxuICB1cGRhdGVCcmFuY2hUZXh0OiAocmVwbykgLT5cbiAgICBpZiBAc2hvd0dpdEluZm9ybWF0aW9uKHJlcG8pXG4gICAgICBoZWFkID0gcmVwby5nZXRTaG9ydEhlYWQoQGdldEFjdGl2ZUl0ZW1QYXRoKCkpXG4gICAgICBAYnJhbmNoTGFiZWwudGV4dENvbnRlbnQgPSBoZWFkXG4gICAgICBAYnJhbmNoQXJlYS5zdHlsZS5kaXNwbGF5ID0gJycgaWYgaGVhZFxuICAgICAgQGJyYW5jaFRvb2x0aXBEaXNwb3NhYmxlPy5kaXNwb3NlKClcbiAgICAgIEBicmFuY2hUb29sdGlwRGlzcG9zYWJsZSA9IGF0b20udG9vbHRpcHMuYWRkIEBicmFuY2hBcmVhLCB0aXRsZTogXCJPbiBicmFuY2ggI3toZWFkfVwiXG4gICAgZWxzZVxuICAgICAgQGJyYW5jaEFyZWEuc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuXG4gIHNob3dHaXRJbmZvcm1hdGlvbjogKHJlcG8pIC0+XG4gICAgcmV0dXJuIGZhbHNlIHVubGVzcyByZXBvP1xuXG4gICAgaWYgaXRlbVBhdGggPSBAZ2V0QWN0aXZlSXRlbVBhdGgoKVxuICAgICAgYXRvbS5wcm9qZWN0LmNvbnRhaW5zKGl0ZW1QYXRoKVxuICAgIGVsc2VcbiAgICAgIG5vdCBAZ2V0QWN0aXZlSXRlbSgpP1xuXG4gIHVwZGF0ZUFoZWFkQmVoaW5kQ291bnQ6IChyZXBvKSAtPlxuICAgIHVubGVzcyBAc2hvd0dpdEluZm9ybWF0aW9uKHJlcG8pXG4gICAgICBAY29tbWl0c0FyZWEuc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgICAgcmV0dXJuXG5cbiAgICBpdGVtUGF0aCA9IEBnZXRBY3RpdmVJdGVtUGF0aCgpXG4gICAge2FoZWFkLCBiZWhpbmR9ID0gcmVwby5nZXRDYWNoZWRVcHN0cmVhbUFoZWFkQmVoaW5kQ291bnQoaXRlbVBhdGgpXG4gICAgaWYgYWhlYWQgPiAwXG4gICAgICBAY29tbWl0c0FoZWFkLnRleHRDb250ZW50ID0gYWhlYWRcbiAgICAgIEBjb21taXRzQWhlYWQuc3R5bGUuZGlzcGxheSA9ICcnXG4gICAgICBAY29tbWl0c0FoZWFkVG9vbHRpcERpc3Bvc2FibGU/LmRpc3Bvc2UoKVxuICAgICAgQGNvbW1pdHNBaGVhZFRvb2x0aXBEaXNwb3NhYmxlID0gYXRvbS50b29sdGlwcy5hZGQgQGNvbW1pdHNBaGVhZCwgdGl0bGU6IFwiI3tfLnBsdXJhbGl6ZShhaGVhZCwgJ2NvbW1pdCcpfSBhaGVhZCBvZiB1cHN0cmVhbVwiXG4gICAgZWxzZVxuICAgICAgQGNvbW1pdHNBaGVhZC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG5cbiAgICBpZiBiZWhpbmQgPiAwXG4gICAgICBAY29tbWl0c0JlaGluZC50ZXh0Q29udGVudCA9IGJlaGluZFxuICAgICAgQGNvbW1pdHNCZWhpbmQuc3R5bGUuZGlzcGxheSA9ICcnXG4gICAgICBAY29tbWl0c0JlaGluZFRvb2x0aXBEaXNwb3NhYmxlPy5kaXNwb3NlKClcbiAgICAgIEBjb21taXRzQmVoaW5kVG9vbHRpcERpc3Bvc2FibGUgPSBhdG9tLnRvb2x0aXBzLmFkZCBAY29tbWl0c0JlaGluZCwgdGl0bGU6IFwiI3tfLnBsdXJhbGl6ZShiZWhpbmQsICdjb21taXQnKX0gYmVoaW5kIHVwc3RyZWFtXCJcbiAgICBlbHNlXG4gICAgICBAY29tbWl0c0JlaGluZC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG5cbiAgICBpZiBhaGVhZCA+IDAgb3IgYmVoaW5kID4gMFxuICAgICAgQGNvbW1pdHNBcmVhLnN0eWxlLmRpc3BsYXkgPSAnJ1xuICAgIGVsc2VcbiAgICAgIEBjb21taXRzQXJlYS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG5cbiAgY2xlYXJTdGF0dXM6IC0+XG4gICAgQGdpdFN0YXR1c0ljb24uY2xhc3NMaXN0LnJlbW92ZSgnaWNvbi1kaWZmLW1vZGlmaWVkJywgJ3N0YXR1cy1tb2RpZmllZCcsICdpY29uLWRpZmYtYWRkZWQnLCAnc3RhdHVzLWFkZGVkJywgJ2ljb24tZGlmZi1pZ25vcmVkJywgJ3N0YXR1cy1pZ25vcmVkJylcblxuICB1cGRhdGVBc05ld0ZpbGU6IC0+XG4gICAgQGNsZWFyU3RhdHVzKClcblxuICAgIEBnaXRTdGF0dXNJY29uLmNsYXNzTGlzdC5hZGQoJ2ljb24tZGlmZi1hZGRlZCcsICdzdGF0dXMtYWRkZWQnKVxuICAgIGlmIHRleHRFZGl0b3IgPSBhdG9tLndvcmtzcGFjZS5nZXRBY3RpdmVUZXh0RWRpdG9yKClcbiAgICAgIEBnaXRTdGF0dXNJY29uLnRleHRDb250ZW50ID0gXCIrI3t0ZXh0RWRpdG9yLmdldExpbmVDb3VudCgpfVwiXG4gICAgICBAdXBkYXRlVG9vbHRpcFRleHQoXCIje18ucGx1cmFsaXplKHRleHRFZGl0b3IuZ2V0TGluZUNvdW50KCksICdsaW5lJyl9IGluIHRoaXMgbmV3IGZpbGUgbm90IHlldCBjb21taXR0ZWRcIilcbiAgICBlbHNlXG4gICAgICBAZ2l0U3RhdHVzSWNvbi50ZXh0Q29udGVudCA9ICcnXG4gICAgICBAdXBkYXRlVG9vbHRpcFRleHQoKVxuXG4gICAgQGdpdFN0YXR1cy5zdHlsZS5kaXNwbGF5ID0gJydcblxuICB1cGRhdGVBc01vZGlmaWVkRmlsZTogKHJlcG8sIHBhdGgpIC0+XG4gICAgc3RhdHMgPSByZXBvLmdldERpZmZTdGF0cyhwYXRoKVxuICAgIEBjbGVhclN0YXR1cygpXG5cbiAgICBAZ2l0U3RhdHVzSWNvbi5jbGFzc0xpc3QuYWRkKCdpY29uLWRpZmYtbW9kaWZpZWQnLCAnc3RhdHVzLW1vZGlmaWVkJylcbiAgICBpZiBzdGF0cy5hZGRlZCBhbmQgc3RhdHMuZGVsZXRlZFxuICAgICAgQGdpdFN0YXR1c0ljb24udGV4dENvbnRlbnQgPSBcIisje3N0YXRzLmFkZGVkfSwgLSN7c3RhdHMuZGVsZXRlZH1cIlxuICAgICAgQHVwZGF0ZVRvb2x0aXBUZXh0KFwiI3tfLnBsdXJhbGl6ZShzdGF0cy5hZGRlZCwgJ2xpbmUnKX0gYWRkZWQgYW5kICN7Xy5wbHVyYWxpemUoc3RhdHMuZGVsZXRlZCwgJ2xpbmUnKX0gZGVsZXRlZCBpbiB0aGlzIGZpbGUgbm90IHlldCBjb21taXR0ZWRcIilcbiAgICBlbHNlIGlmIHN0YXRzLmFkZGVkXG4gICAgICBAZ2l0U3RhdHVzSWNvbi50ZXh0Q29udGVudCA9IFwiKyN7c3RhdHMuYWRkZWR9XCJcbiAgICAgIEB1cGRhdGVUb29sdGlwVGV4dChcIiN7Xy5wbHVyYWxpemUoc3RhdHMuYWRkZWQsICdsaW5lJyl9IGFkZGVkIHRvIHRoaXMgZmlsZSBub3QgeWV0IGNvbW1pdHRlZFwiKVxuICAgIGVsc2UgaWYgc3RhdHMuZGVsZXRlZFxuICAgICAgQGdpdFN0YXR1c0ljb24udGV4dENvbnRlbnQgPSBcIi0je3N0YXRzLmRlbGV0ZWR9XCJcbiAgICAgIEB1cGRhdGVUb29sdGlwVGV4dChcIiN7Xy5wbHVyYWxpemUoc3RhdHMuZGVsZXRlZCwgJ2xpbmUnKX0gZGVsZXRlZCBmcm9tIHRoaXMgZmlsZSBub3QgeWV0IGNvbW1pdHRlZFwiKVxuICAgIGVsc2VcbiAgICAgIEBnaXRTdGF0dXNJY29uLnRleHRDb250ZW50ID0gJydcbiAgICAgIEB1cGRhdGVUb29sdGlwVGV4dCgpXG5cbiAgICBAZ2l0U3RhdHVzLnN0eWxlLmRpc3BsYXkgPSAnJ1xuXG4gIHVwZGF0ZUFzSWdub3JlZEZpbGU6IC0+XG4gICAgQGNsZWFyU3RhdHVzKClcblxuICAgIEBnaXRTdGF0dXNJY29uLmNsYXNzTGlzdC5hZGQoJ2ljb24tZGlmZi1pZ25vcmVkJywgICdzdGF0dXMtaWdub3JlZCcpXG4gICAgQGdpdFN0YXR1c0ljb24udGV4dENvbnRlbnQgPSAnJ1xuICAgIEBnaXRTdGF0dXMuc3R5bGUuZGlzcGxheSA9ICcnXG4gICAgQHVwZGF0ZVRvb2x0aXBUZXh0KFwiRmlsZSBpcyBpZ25vcmVkIGJ5IGdpdFwiKVxuXG4gIHVwZGF0ZVRvb2x0aXBUZXh0OiAodGV4dCkgLT5cbiAgICBAc3RhdHVzVG9vbHRpcERpc3Bvc2FibGU/LmRpc3Bvc2UoKVxuICAgIGlmIHRleHRcbiAgICAgIEBzdGF0dXNUb29sdGlwRGlzcG9zYWJsZSA9IGF0b20udG9vbHRpcHMuYWRkIEBnaXRTdGF0dXNJY29uLCB0aXRsZTogdGV4dFxuXG4gIHVwZGF0ZVN0YXR1c1RleHQ6IChyZXBvKSAtPlxuICAgIGhpZGVTdGF0dXMgPSA9PlxuICAgICAgQGNsZWFyU3RhdHVzKClcbiAgICAgIEBnaXRTdGF0dXMuc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuXG4gICAgaXRlbVBhdGggPSBAZ2V0QWN0aXZlSXRlbVBhdGgoKVxuICAgIGlmIEBzaG93R2l0SW5mb3JtYXRpb24ocmVwbykgYW5kIGl0ZW1QYXRoP1xuICAgICAgc3RhdHVzID0gcmVwby5nZXRDYWNoZWRQYXRoU3RhdHVzKGl0ZW1QYXRoKSA/IDBcbiAgICAgIGlmIHJlcG8uaXNTdGF0dXNOZXcoc3RhdHVzKVxuICAgICAgICByZXR1cm4gQHVwZGF0ZUFzTmV3RmlsZSgpXG5cbiAgICAgIGlmIHJlcG8uaXNTdGF0dXNNb2RpZmllZChzdGF0dXMpXG4gICAgICAgIHJldHVybiBAdXBkYXRlQXNNb2RpZmllZEZpbGUocmVwbywgaXRlbVBhdGgpXG5cbiAgICAgIGlmIHJlcG8uaXNQYXRoSWdub3JlZChpdGVtUGF0aClcbiAgICAgICAgQHVwZGF0ZUFzSWdub3JlZEZpbGUoKVxuICAgICAgZWxzZVxuICAgICAgICBoaWRlU3RhdHVzKClcbiAgICBlbHNlXG4gICAgICBoaWRlU3RhdHVzKClcbiJdfQ==
