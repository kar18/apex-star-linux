(function() {
  var SelectionCountView, _;

  _ = require('underscore-plus');

  module.exports = SelectionCountView = (function() {
    function SelectionCountView() {
      var ref;
      this.element = document.createElement('status-bar-selection');
      this.element.classList.add('selection-count', 'inline-block');
      this.tooltipElement = document.createElement('div');
      this.tooltipDisposable = atom.tooltips.add(this.element, {
        item: this.tooltipElement
      });
      this.formatString = (ref = atom.config.get('status-bar.selectionCountFormat')) != null ? ref : '(%L, %C)';
      this.activeItemSubscription = atom.workspace.onDidChangeActiveTextEditor((function(_this) {
        return function() {
          return _this.subscribeToActiveTextEditor();
        };
      })(this));
      this.subscribeToConfig();
      this.subscribeToActiveTextEditor();
    }

    SelectionCountView.prototype.destroy = function() {
      var ref, ref1;
      this.activeItemSubscription.dispose();
      if ((ref = this.selectionSubscription) != null) {
        ref.dispose();
      }
      if ((ref1 = this.configSubscription) != null) {
        ref1.dispose();
      }
      return this.tooltipDisposable.dispose();
    };

    SelectionCountView.prototype.subscribeToConfig = function() {
      var ref;
      if ((ref = this.configSubscription) != null) {
        ref.dispose();
      }
      return this.configSubscription = atom.config.observe('status-bar.selectionCountFormat', (function(_this) {
        return function(value) {
          _this.formatString = value != null ? value : '(%L, %C)';
          return _this.scheduleUpdateCount();
        };
      })(this));
    };

    SelectionCountView.prototype.subscribeToActiveTextEditor = function() {
      var activeEditor, ref, selectionsMarkerLayer;
      if ((ref = this.selectionSubscription) != null) {
        ref.dispose();
      }
      activeEditor = this.getActiveTextEditor();
      selectionsMarkerLayer = activeEditor != null ? activeEditor.selectionsMarkerLayer : void 0;
      this.selectionSubscription = selectionsMarkerLayer != null ? selectionsMarkerLayer.onDidUpdate(this.scheduleUpdateCount.bind(this)) : void 0;
      return this.scheduleUpdateCount();
    };

    SelectionCountView.prototype.getActiveTextEditor = function() {
      return atom.workspace.getActiveTextEditor();
    };

    SelectionCountView.prototype.scheduleUpdateCount = function() {
      if (!this.scheduledUpdate) {
        this.scheduledUpdate = true;
        return atom.views.updateDocument((function(_this) {
          return function() {
            _this.updateCount();
            return _this.scheduledUpdate = false;
          };
        })(this));
      }
    };

    SelectionCountView.prototype.updateCount = function() {
      var count, lineCount, range, ref, ref1;
      count = (ref = this.getActiveTextEditor()) != null ? ref.getSelectedText().length : void 0;
      range = (ref1 = this.getActiveTextEditor()) != null ? ref1.getSelectedBufferRange() : void 0;
      lineCount = range != null ? range.getRowCount() : void 0;
      if ((range != null ? range.end.column : void 0) === 0) {
        lineCount -= 1;
      }
      if (count > 0) {
        this.element.textContent = this.formatString.replace('%L', lineCount).replace('%C', count);
        return this.tooltipElement.textContent = (_.pluralize(lineCount, 'line')) + ", " + (_.pluralize(count, 'character')) + " selected";
      } else {
        this.element.textContent = '';
        return this.tooltipElement.textContent = '';
      }
    };

    return SelectionCountView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9zdGF0dXMtYmFyL2xpYi9zZWxlY3Rpb24tY291bnQtdmlldy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLENBQUEsR0FBSSxPQUFBLENBQVEsaUJBQVI7O0VBRUosTUFBTSxDQUFDLE9BQVAsR0FDTTtJQUNTLDRCQUFBO0FBQ1gsVUFBQTtNQUFBLElBQUMsQ0FBQSxPQUFELEdBQVcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsc0JBQXZCO01BQ1gsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsaUJBQXZCLEVBQTBDLGNBQTFDO01BRUEsSUFBQyxDQUFBLGNBQUQsR0FBa0IsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDbEIsSUFBQyxDQUFBLGlCQUFELEdBQXFCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixJQUFDLENBQUEsT0FBbkIsRUFBNEI7UUFBQSxJQUFBLEVBQU0sSUFBQyxDQUFBLGNBQVA7T0FBNUI7TUFFckIsSUFBQyxDQUFBLFlBQUQsOEVBQXFFO01BRXJFLElBQUMsQ0FBQSxzQkFBRCxHQUEwQixJQUFJLENBQUMsU0FBUyxDQUFDLDJCQUFmLENBQTJDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsMkJBQUQsQ0FBQTtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzQztNQUUxQixJQUFDLENBQUEsaUJBQUQsQ0FBQTtNQUNBLElBQUMsQ0FBQSwyQkFBRCxDQUFBO0lBWlc7O2lDQWNiLE9BQUEsR0FBUyxTQUFBO0FBQ1AsVUFBQTtNQUFBLElBQUMsQ0FBQSxzQkFBc0IsQ0FBQyxPQUF4QixDQUFBOztXQUNzQixDQUFFLE9BQXhCLENBQUE7OztZQUNtQixDQUFFLE9BQXJCLENBQUE7O2FBQ0EsSUFBQyxDQUFBLGlCQUFpQixDQUFDLE9BQW5CLENBQUE7SUFKTzs7aUNBTVQsaUJBQUEsR0FBbUIsU0FBQTtBQUNqQixVQUFBOztXQUFtQixDQUFFLE9BQXJCLENBQUE7O2FBQ0EsSUFBQyxDQUFBLGtCQUFELEdBQXNCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBWixDQUFvQixpQ0FBcEIsRUFBdUQsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLEtBQUQ7VUFDM0UsS0FBQyxDQUFBLFlBQUQsbUJBQWdCLFFBQVE7aUJBQ3hCLEtBQUMsQ0FBQSxtQkFBRCxDQUFBO1FBRjJFO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF2RDtJQUZMOztpQ0FNbkIsMkJBQUEsR0FBNkIsU0FBQTtBQUMzQixVQUFBOztXQUFzQixDQUFFLE9BQXhCLENBQUE7O01BQ0EsWUFBQSxHQUFlLElBQUMsQ0FBQSxtQkFBRCxDQUFBO01BQ2YscUJBQUEsMEJBQXdCLFlBQVksQ0FBRTtNQUN0QyxJQUFDLENBQUEscUJBQUQsbUNBQXlCLHFCQUFxQixDQUFFLFdBQXZCLENBQW1DLElBQUMsQ0FBQSxtQkFBbUIsQ0FBQyxJQUFyQixDQUEwQixJQUExQixDQUFuQzthQUN6QixJQUFDLENBQUEsbUJBQUQsQ0FBQTtJQUwyQjs7aUNBTzdCLG1CQUFBLEdBQXFCLFNBQUE7YUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBZixDQUFBO0lBRG1COztpQ0FHckIsbUJBQUEsR0FBcUIsU0FBQTtNQUNuQixJQUFBLENBQU8sSUFBQyxDQUFBLGVBQVI7UUFDRSxJQUFDLENBQUEsZUFBRCxHQUFtQjtlQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQVgsQ0FBMEIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTtZQUN4QixLQUFDLENBQUEsV0FBRCxDQUFBO21CQUNBLEtBQUMsQ0FBQSxlQUFELEdBQW1CO1VBRks7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTFCLEVBRkY7O0lBRG1COztpQ0FPckIsV0FBQSxHQUFhLFNBQUE7QUFDWCxVQUFBO01BQUEsS0FBQSxtREFBOEIsQ0FBRSxlQUF4QixDQUFBLENBQXlDLENBQUM7TUFDbEQsS0FBQSxxREFBOEIsQ0FBRSxzQkFBeEIsQ0FBQTtNQUNSLFNBQUEsbUJBQVksS0FBSyxDQUFFLFdBQVAsQ0FBQTtNQUNaLHFCQUFrQixLQUFLLENBQUUsR0FBRyxDQUFDLGdCQUFYLEtBQXFCLENBQXZDO1FBQUEsU0FBQSxJQUFhLEVBQWI7O01BQ0EsSUFBRyxLQUFBLEdBQVEsQ0FBWDtRQUNFLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxHQUF1QixJQUFDLENBQUEsWUFBWSxDQUFDLE9BQWQsQ0FBc0IsSUFBdEIsRUFBNEIsU0FBNUIsQ0FBc0MsQ0FBQyxPQUF2QyxDQUErQyxJQUEvQyxFQUFxRCxLQUFyRDtlQUN2QixJQUFDLENBQUEsY0FBYyxDQUFDLFdBQWhCLEdBQWdDLENBQUMsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxTQUFaLEVBQXVCLE1BQXZCLENBQUQsQ0FBQSxHQUFnQyxJQUFoQyxHQUFtQyxDQUFDLENBQUMsQ0FBQyxTQUFGLENBQVksS0FBWixFQUFtQixXQUFuQixDQUFELENBQW5DLEdBQW9FLFlBRnRHO09BQUEsTUFBQTtRQUlFLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxHQUF1QjtlQUN2QixJQUFDLENBQUEsY0FBYyxDQUFDLFdBQWhCLEdBQThCLEdBTGhDOztJQUxXOzs7OztBQS9DZiIsInNvdXJjZXNDb250ZW50IjpbIl8gPSByZXF1aXJlICd1bmRlcnNjb3JlLXBsdXMnXG5cbm1vZHVsZS5leHBvcnRzID1cbmNsYXNzIFNlbGVjdGlvbkNvdW50Vmlld1xuICBjb25zdHJ1Y3RvcjogLT5cbiAgICBAZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0YXR1cy1iYXItc2VsZWN0aW9uJylcbiAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdzZWxlY3Rpb24tY291bnQnLCAnaW5saW5lLWJsb2NrJylcblxuICAgIEB0b29sdGlwRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgQHRvb2x0aXBEaXNwb3NhYmxlID0gYXRvbS50b29sdGlwcy5hZGQgQGVsZW1lbnQsIGl0ZW06IEB0b29sdGlwRWxlbWVudFxuXG4gICAgQGZvcm1hdFN0cmluZyA9IGF0b20uY29uZmlnLmdldCgnc3RhdHVzLWJhci5zZWxlY3Rpb25Db3VudEZvcm1hdCcpID8gJyglTCwgJUMpJ1xuXG4gICAgQGFjdGl2ZUl0ZW1TdWJzY3JpcHRpb24gPSBhdG9tLndvcmtzcGFjZS5vbkRpZENoYW5nZUFjdGl2ZVRleHRFZGl0b3IgPT4gQHN1YnNjcmliZVRvQWN0aXZlVGV4dEVkaXRvcigpXG5cbiAgICBAc3Vic2NyaWJlVG9Db25maWcoKVxuICAgIEBzdWJzY3JpYmVUb0FjdGl2ZVRleHRFZGl0b3IoKVxuXG4gIGRlc3Ryb3k6IC0+XG4gICAgQGFjdGl2ZUl0ZW1TdWJzY3JpcHRpb24uZGlzcG9zZSgpXG4gICAgQHNlbGVjdGlvblN1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG4gICAgQGNvbmZpZ1N1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG4gICAgQHRvb2x0aXBEaXNwb3NhYmxlLmRpc3Bvc2UoKVxuXG4gIHN1YnNjcmliZVRvQ29uZmlnOiAtPlxuICAgIEBjb25maWdTdWJzY3JpcHRpb24/LmRpc3Bvc2UoKVxuICAgIEBjb25maWdTdWJzY3JpcHRpb24gPSBhdG9tLmNvbmZpZy5vYnNlcnZlICdzdGF0dXMtYmFyLnNlbGVjdGlvbkNvdW50Rm9ybWF0JywgKHZhbHVlKSA9PlxuICAgICAgQGZvcm1hdFN0cmluZyA9IHZhbHVlID8gJyglTCwgJUMpJ1xuICAgICAgQHNjaGVkdWxlVXBkYXRlQ291bnQoKVxuXG4gIHN1YnNjcmliZVRvQWN0aXZlVGV4dEVkaXRvcjogLT5cbiAgICBAc2VsZWN0aW9uU3Vic2NyaXB0aW9uPy5kaXNwb3NlKClcbiAgICBhY3RpdmVFZGl0b3IgPSBAZ2V0QWN0aXZlVGV4dEVkaXRvcigpXG4gICAgc2VsZWN0aW9uc01hcmtlckxheWVyID0gYWN0aXZlRWRpdG9yPy5zZWxlY3Rpb25zTWFya2VyTGF5ZXJcbiAgICBAc2VsZWN0aW9uU3Vic2NyaXB0aW9uID0gc2VsZWN0aW9uc01hcmtlckxheWVyPy5vbkRpZFVwZGF0ZShAc2NoZWR1bGVVcGRhdGVDb3VudC5iaW5kKHRoaXMpKVxuICAgIEBzY2hlZHVsZVVwZGF0ZUNvdW50KClcblxuICBnZXRBY3RpdmVUZXh0RWRpdG9yOiAtPlxuICAgIGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVRleHRFZGl0b3IoKVxuXG4gIHNjaGVkdWxlVXBkYXRlQ291bnQ6IC0+XG4gICAgdW5sZXNzIEBzY2hlZHVsZWRVcGRhdGVcbiAgICAgIEBzY2hlZHVsZWRVcGRhdGUgPSB0cnVlXG4gICAgICBhdG9tLnZpZXdzLnVwZGF0ZURvY3VtZW50ID0+XG4gICAgICAgIEB1cGRhdGVDb3VudCgpXG4gICAgICAgIEBzY2hlZHVsZWRVcGRhdGUgPSBmYWxzZVxuXG4gIHVwZGF0ZUNvdW50OiAtPlxuICAgIGNvdW50ID0gQGdldEFjdGl2ZVRleHRFZGl0b3IoKT8uZ2V0U2VsZWN0ZWRUZXh0KCkubGVuZ3RoXG4gICAgcmFuZ2UgPSBAZ2V0QWN0aXZlVGV4dEVkaXRvcigpPy5nZXRTZWxlY3RlZEJ1ZmZlclJhbmdlKClcbiAgICBsaW5lQ291bnQgPSByYW5nZT8uZ2V0Um93Q291bnQoKVxuICAgIGxpbmVDb3VudCAtPSAxIGlmIHJhbmdlPy5lbmQuY29sdW1uIGlzIDBcbiAgICBpZiBjb3VudCA+IDBcbiAgICAgIEBlbGVtZW50LnRleHRDb250ZW50ID0gQGZvcm1hdFN0cmluZy5yZXBsYWNlKCclTCcsIGxpbmVDb3VudCkucmVwbGFjZSgnJUMnLCBjb3VudClcbiAgICAgIEB0b29sdGlwRWxlbWVudC50ZXh0Q29udGVudCA9IFwiI3tfLnBsdXJhbGl6ZShsaW5lQ291bnQsICdsaW5lJyl9LCAje18ucGx1cmFsaXplKGNvdW50LCAnY2hhcmFjdGVyJyl9IHNlbGVjdGVkXCJcbiAgICBlbHNlXG4gICAgICBAZWxlbWVudC50ZXh0Q29udGVudCA9ICcnXG4gICAgICBAdG9vbHRpcEVsZW1lbnQudGV4dENvbnRlbnQgPSAnJ1xuIl19
