(function() {
  var Emitter, FindOptions, Params, _, escapeRegExp;

  _ = require('underscore-plus');

  Emitter = require('atom').Emitter;

  Params = ['findPattern', 'replacePattern', 'pathsPattern', 'useRegex', 'wholeWord', 'caseSensitive', 'inCurrentSelection', 'leadingContextLineCount', 'trailingContextLineCount'];

  module.exports = FindOptions = (function() {
    function FindOptions(state) {
      var ref, ref1, ref10, ref11, ref12, ref13, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
      if (state == null) {
        state = {};
      }
      this.emitter = new Emitter;
      this.findPattern = '';
      this.replacePattern = (ref = state.replacePattern) != null ? ref : '';
      this.pathsPattern = (ref1 = state.pathsPattern) != null ? ref1 : '';
      this.useRegex = (ref2 = (ref3 = state.useRegex) != null ? ref3 : atom.config.get('find-and-replace.useRegex')) != null ? ref2 : false;
      this.caseSensitive = (ref4 = (ref5 = state.caseSensitive) != null ? ref5 : atom.config.get('find-and-replace.caseSensitive')) != null ? ref4 : false;
      this.wholeWord = (ref6 = (ref7 = state.wholeWord) != null ? ref7 : atom.config.get('find-and-replace.wholeWord')) != null ? ref6 : false;
      this.inCurrentSelection = (ref8 = (ref9 = state.inCurrentSelection) != null ? ref9 : atom.config.get('find-and-replace.inCurrentSelection')) != null ? ref8 : false;
      this.leadingContextLineCount = (ref10 = (ref11 = state.leadingContextLineCount) != null ? ref11 : atom.config.get('find-and-replace.leadingContextLineCount')) != null ? ref10 : 0;
      this.trailingContextLineCount = (ref12 = (ref13 = state.trailingContextLineCount) != null ? ref13 : atom.config.get('find-and-replace.trailingContextLineCount')) != null ? ref12 : 0;
    }

    FindOptions.prototype.onDidChange = function(callback) {
      return this.emitter.on('did-change', callback);
    };

    FindOptions.prototype.onDidChangeUseRegex = function(callback) {
      return this.emitter.on('did-change-useRegex', callback);
    };

    FindOptions.prototype.onDidChangeReplacePattern = function(callback) {
      return this.emitter.on('did-change-replacePattern', callback);
    };

    FindOptions.prototype.serialize = function() {
      var j, len, param, result;
      result = {};
      for (j = 0, len = Params.length; j < len; j++) {
        param = Params[j];
        result[param] = this[param];
      }
      return result;
    };

    FindOptions.prototype.set = function(newParams) {
      var changedParams, j, key, len, param, val;
      if (newParams == null) {
        newParams = {};
      }
      changedParams = {};
      for (j = 0, len = Params.length; j < len; j++) {
        key = Params[j];
        if ((newParams[key] != null) && newParams[key] !== this[key]) {
          if (changedParams == null) {
            changedParams = {};
          }
          this[key] = changedParams[key] = newParams[key];
        }
      }
      if (Object.keys(changedParams).length) {
        for (param in changedParams) {
          val = changedParams[param];
          this.emitter.emit("did-change-" + param);
        }
        this.emitter.emit('did-change', changedParams);
      }
      return changedParams;
    };

    FindOptions.prototype.getFindPatternRegex = function(forceUnicode) {
      var expression, flags, i, j, ref;
      if (forceUnicode == null) {
        forceUnicode = false;
      }
      for (i = j = 0, ref = this.findPattern.length; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
        if (this.findPattern.charCodeAt(i) > 128) {
          forceUnicode = true;
          break;
        }
      }
      flags = 'gm';
      if (!this.caseSensitive) {
        flags += 'i';
      }
      if (forceUnicode) {
        flags += 'u';
      }
      if (this.useRegex) {
        expression = this.findPattern;
      } else {
        expression = escapeRegExp(this.findPattern);
      }
      if (this.wholeWord) {
        expression = "\\b" + expression + "\\b";
      }
      return new RegExp(expression, flags);
    };

    return FindOptions;

  })();

  escapeRegExp = function(string) {
    return string.replace(/[\/\\^$*+?.()|[\]{}]/g, '\\$&');
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9maW5kLWFuZC1yZXBsYWNlL2xpYi9maW5kLW9wdGlvbnMuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQSxDQUFBLEdBQUksT0FBQSxDQUFRLGlCQUFSOztFQUNILFVBQVcsT0FBQSxDQUFRLE1BQVI7O0VBRVosTUFBQSxHQUFTLENBQ1AsYUFETyxFQUVQLGdCQUZPLEVBR1AsY0FITyxFQUlQLFVBSk8sRUFLUCxXQUxPLEVBTVAsZUFOTyxFQU9QLG9CQVBPLEVBUVAseUJBUk8sRUFTUCwwQkFUTzs7RUFZVCxNQUFNLENBQUMsT0FBUCxHQUNNO0lBQ1MscUJBQUMsS0FBRDtBQUNYLFVBQUE7O1FBRFksUUFBTTs7TUFDbEIsSUFBQyxDQUFBLE9BQUQsR0FBVyxJQUFJO01BRWYsSUFBQyxDQUFBLFdBQUQsR0FBZTtNQUNmLElBQUMsQ0FBQSxjQUFELGdEQUF5QztNQUN6QyxJQUFDLENBQUEsWUFBRCxnREFBcUM7TUFDckMsSUFBQyxDQUFBLFFBQUQsbUhBQTRFO01BQzVFLElBQUMsQ0FBQSxhQUFELDZIQUEyRjtNQUMzRixJQUFDLENBQUEsU0FBRCxxSEFBK0U7TUFDL0UsSUFBQyxDQUFBLGtCQUFELHVJQUEwRztNQUMxRyxJQUFDLENBQUEsdUJBQUQscUpBQXlIO01BQ3pILElBQUMsQ0FBQSx3QkFBRCx1SkFBNEg7SUFYakg7OzBCQWFiLFdBQUEsR0FBYSxTQUFDLFFBQUQ7YUFDWCxJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxZQUFaLEVBQTBCLFFBQTFCO0lBRFc7OzBCQUdiLG1CQUFBLEdBQXFCLFNBQUMsUUFBRDthQUNuQixJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSxxQkFBWixFQUFtQyxRQUFuQztJQURtQjs7MEJBR3JCLHlCQUFBLEdBQTJCLFNBQUMsUUFBRDthQUN6QixJQUFDLENBQUEsT0FBTyxDQUFDLEVBQVQsQ0FBWSwyQkFBWixFQUF5QyxRQUF6QztJQUR5Qjs7MEJBRzNCLFNBQUEsR0FBVyxTQUFBO0FBQ1QsVUFBQTtNQUFBLE1BQUEsR0FBUztBQUNULFdBQUEsd0NBQUE7O1FBQ0UsTUFBTyxDQUFBLEtBQUEsQ0FBUCxHQUFnQixJQUFLLENBQUEsS0FBQTtBQUR2QjthQUVBO0lBSlM7OzBCQU1YLEdBQUEsR0FBSyxTQUFDLFNBQUQ7QUFDSCxVQUFBOztRQURJLFlBQVU7O01BQ2QsYUFBQSxHQUFnQjtBQUNoQixXQUFBLHdDQUFBOztRQUNFLElBQUcsd0JBQUEsSUFBb0IsU0FBVSxDQUFBLEdBQUEsQ0FBVixLQUFvQixJQUFLLENBQUEsR0FBQSxDQUFoRDs7WUFDRSxnQkFBaUI7O1VBQ2pCLElBQUssQ0FBQSxHQUFBLENBQUwsR0FBWSxhQUFjLENBQUEsR0FBQSxDQUFkLEdBQXFCLFNBQVUsQ0FBQSxHQUFBLEVBRjdDOztBQURGO01BS0EsSUFBRyxNQUFNLENBQUMsSUFBUCxDQUFZLGFBQVosQ0FBMEIsQ0FBQyxNQUE5QjtBQUNFLGFBQUEsc0JBQUE7O1VBQ0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxJQUFULENBQWMsYUFBQSxHQUFjLEtBQTVCO0FBREY7UUFFQSxJQUFDLENBQUEsT0FBTyxDQUFDLElBQVQsQ0FBYyxZQUFkLEVBQTRCLGFBQTVCLEVBSEY7O0FBSUEsYUFBTztJQVhKOzswQkFhTCxtQkFBQSxHQUFxQixTQUFDLFlBQUQ7QUFDbkIsVUFBQTs7UUFEb0IsZUFBZTs7QUFDbkMsV0FBUyxrR0FBVDtRQUNFLElBQUcsSUFBQyxDQUFBLFdBQVcsQ0FBQyxVQUFiLENBQXdCLENBQXhCLENBQUEsR0FBNkIsR0FBaEM7VUFDRSxZQUFBLEdBQWU7QUFDZixnQkFGRjs7QUFERjtNQUtBLEtBQUEsR0FBUTtNQUNSLElBQUEsQ0FBb0IsSUFBQyxDQUFBLGFBQXJCO1FBQUEsS0FBQSxJQUFTLElBQVQ7O01BQ0EsSUFBZ0IsWUFBaEI7UUFBQSxLQUFBLElBQVMsSUFBVDs7TUFFQSxJQUFHLElBQUMsQ0FBQSxRQUFKO1FBQ0UsVUFBQSxHQUFhLElBQUMsQ0FBQSxZQURoQjtPQUFBLE1BQUE7UUFHRSxVQUFBLEdBQWEsWUFBQSxDQUFhLElBQUMsQ0FBQSxXQUFkLEVBSGY7O01BS0EsSUFBc0MsSUFBQyxDQUFBLFNBQXZDO1FBQUEsVUFBQSxHQUFhLEtBQUEsR0FBTSxVQUFOLEdBQWlCLE1BQTlCOzthQUVBLElBQUksTUFBSixDQUFXLFVBQVgsRUFBdUIsS0FBdkI7SUFqQm1COzs7Ozs7RUF1QnZCLFlBQUEsR0FBZSxTQUFDLE1BQUQ7V0FDYixNQUFNLENBQUMsT0FBUCxDQUFlLHVCQUFmLEVBQXdDLE1BQXhDO0VBRGE7QUFqRmYiLCJzb3VyY2VzQ29udGVudCI6WyJfID0gcmVxdWlyZSAndW5kZXJzY29yZS1wbHVzJ1xue0VtaXR0ZXJ9ID0gcmVxdWlyZSAnYXRvbSdcblxuUGFyYW1zID0gW1xuICAnZmluZFBhdHRlcm4nXG4gICdyZXBsYWNlUGF0dGVybidcbiAgJ3BhdGhzUGF0dGVybidcbiAgJ3VzZVJlZ2V4J1xuICAnd2hvbGVXb3JkJ1xuICAnY2FzZVNlbnNpdGl2ZSdcbiAgJ2luQ3VycmVudFNlbGVjdGlvbidcbiAgJ2xlYWRpbmdDb250ZXh0TGluZUNvdW50J1xuICAndHJhaWxpbmdDb250ZXh0TGluZUNvdW50J1xuXVxuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBGaW5kT3B0aW9uc1xuICBjb25zdHJ1Y3RvcjogKHN0YXRlPXt9KSAtPlxuICAgIEBlbWl0dGVyID0gbmV3IEVtaXR0ZXJcblxuICAgIEBmaW5kUGF0dGVybiA9ICcnXG4gICAgQHJlcGxhY2VQYXR0ZXJuID0gc3RhdGUucmVwbGFjZVBhdHRlcm4gPyAnJ1xuICAgIEBwYXRoc1BhdHRlcm4gPSBzdGF0ZS5wYXRoc1BhdHRlcm4gPyAnJ1xuICAgIEB1c2VSZWdleCA9IHN0YXRlLnVzZVJlZ2V4ID8gYXRvbS5jb25maWcuZ2V0KCdmaW5kLWFuZC1yZXBsYWNlLnVzZVJlZ2V4JykgPyBmYWxzZVxuICAgIEBjYXNlU2Vuc2l0aXZlID0gc3RhdGUuY2FzZVNlbnNpdGl2ZSA/IGF0b20uY29uZmlnLmdldCgnZmluZC1hbmQtcmVwbGFjZS5jYXNlU2Vuc2l0aXZlJykgPyBmYWxzZVxuICAgIEB3aG9sZVdvcmQgPSBzdGF0ZS53aG9sZVdvcmQgPyBhdG9tLmNvbmZpZy5nZXQoJ2ZpbmQtYW5kLXJlcGxhY2Uud2hvbGVXb3JkJykgPyBmYWxzZVxuICAgIEBpbkN1cnJlbnRTZWxlY3Rpb24gPSBzdGF0ZS5pbkN1cnJlbnRTZWxlY3Rpb24gPyBhdG9tLmNvbmZpZy5nZXQoJ2ZpbmQtYW5kLXJlcGxhY2UuaW5DdXJyZW50U2VsZWN0aW9uJykgPyBmYWxzZVxuICAgIEBsZWFkaW5nQ29udGV4dExpbmVDb3VudCA9IHN0YXRlLmxlYWRpbmdDb250ZXh0TGluZUNvdW50ID8gYXRvbS5jb25maWcuZ2V0KCdmaW5kLWFuZC1yZXBsYWNlLmxlYWRpbmdDb250ZXh0TGluZUNvdW50JykgPyAwXG4gICAgQHRyYWlsaW5nQ29udGV4dExpbmVDb3VudCA9IHN0YXRlLnRyYWlsaW5nQ29udGV4dExpbmVDb3VudCA/IGF0b20uY29uZmlnLmdldCgnZmluZC1hbmQtcmVwbGFjZS50cmFpbGluZ0NvbnRleHRMaW5lQ291bnQnKSA/IDBcblxuICBvbkRpZENoYW5nZTogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdkaWQtY2hhbmdlJywgY2FsbGJhY2spXG5cbiAgb25EaWRDaGFuZ2VVc2VSZWdleDogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdkaWQtY2hhbmdlLXVzZVJlZ2V4JywgY2FsbGJhY2spXG5cbiAgb25EaWRDaGFuZ2VSZXBsYWNlUGF0dGVybjogKGNhbGxiYWNrKSAtPlxuICAgIEBlbWl0dGVyLm9uKCdkaWQtY2hhbmdlLXJlcGxhY2VQYXR0ZXJuJywgY2FsbGJhY2spXG5cbiAgc2VyaWFsaXplOiAtPlxuICAgIHJlc3VsdCA9IHt9XG4gICAgZm9yIHBhcmFtIGluIFBhcmFtc1xuICAgICAgcmVzdWx0W3BhcmFtXSA9IHRoaXNbcGFyYW1dXG4gICAgcmVzdWx0XG5cbiAgc2V0OiAobmV3UGFyYW1zPXt9KSAtPlxuICAgIGNoYW5nZWRQYXJhbXMgPSB7fVxuICAgIGZvciBrZXkgaW4gUGFyYW1zXG4gICAgICBpZiBuZXdQYXJhbXNba2V5XT8gYW5kIG5ld1BhcmFtc1trZXldIGlzbnQgdGhpc1trZXldXG4gICAgICAgIGNoYW5nZWRQYXJhbXMgPz0ge31cbiAgICAgICAgdGhpc1trZXldID0gY2hhbmdlZFBhcmFtc1trZXldID0gbmV3UGFyYW1zW2tleV1cblxuICAgIGlmIE9iamVjdC5rZXlzKGNoYW5nZWRQYXJhbXMpLmxlbmd0aFxuICAgICAgZm9yIHBhcmFtLCB2YWwgb2YgY2hhbmdlZFBhcmFtc1xuICAgICAgICBAZW1pdHRlci5lbWl0KFwiZGlkLWNoYW5nZS0je3BhcmFtfVwiKVxuICAgICAgQGVtaXR0ZXIuZW1pdCgnZGlkLWNoYW5nZScsIGNoYW5nZWRQYXJhbXMpXG4gICAgcmV0dXJuIGNoYW5nZWRQYXJhbXNcblxuICBnZXRGaW5kUGF0dGVyblJlZ2V4OiAoZm9yY2VVbmljb2RlID0gZmFsc2UpIC0+XG4gICAgZm9yIGkgaW4gWzAuLkBmaW5kUGF0dGVybi5sZW5ndGhdXG4gICAgICBpZiBAZmluZFBhdHRlcm4uY2hhckNvZGVBdChpKSA+IDEyOFxuICAgICAgICBmb3JjZVVuaWNvZGUgPSB0cnVlXG4gICAgICAgIGJyZWFrXG5cbiAgICBmbGFncyA9ICdnbSdcbiAgICBmbGFncyArPSAnaScgdW5sZXNzIEBjYXNlU2Vuc2l0aXZlXG4gICAgZmxhZ3MgKz0gJ3UnIGlmIGZvcmNlVW5pY29kZVxuXG4gICAgaWYgQHVzZVJlZ2V4XG4gICAgICBleHByZXNzaW9uID0gQGZpbmRQYXR0ZXJuXG4gICAgZWxzZVxuICAgICAgZXhwcmVzc2lvbiA9IGVzY2FwZVJlZ0V4cChAZmluZFBhdHRlcm4pXG5cbiAgICBleHByZXNzaW9uID0gXCJcXFxcYiN7ZXhwcmVzc2lvbn1cXFxcYlwiIGlmIEB3aG9sZVdvcmRcblxuICAgIG5ldyBSZWdFeHAoZXhwcmVzc2lvbiwgZmxhZ3MpXG5cbiMgVGhpcyBpcyBkaWZmZXJlbnQgZnJvbSBfLmVzY2FwZVJlZ0V4cCwgd2hpY2ggZXNjYXBlcyBkYXNoZXMuIEVzY2FwZWQgZGFzaGVzXG4jIGFyZSBub3QgYWxsb3dlZCBvdXRzaWRlIG9mIGNoYXJhY3RlciBjbGFzc2VzIGluIFJlZ0V4cHMgd2l0aCB0aGUgYHVgIGZsYWcuXG4jXG4jIFNlZSBhdG9tL2ZpbmQtYW5kLXJlcGxhY2UjMTAyMlxuZXNjYXBlUmVnRXhwID0gKHN0cmluZykgLT5cbiAgc3RyaW5nLnJlcGxhY2UoL1tcXC9cXFxcXiQqKz8uKCl8W1xcXXt9XS9nLCAnXFxcXCQmJylcbiJdfQ==
