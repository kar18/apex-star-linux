(function() {
  var CopyDialog, Dialog, fs, path, repoForPath,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  path = require('path');

  fs = require('fs-plus');

  Dialog = require('./dialog');

  repoForPath = require("./helpers").repoForPath;

  module.exports = CopyDialog = (function(superClass) {
    extend(CopyDialog, superClass);

    function CopyDialog(initialPath, arg) {
      this.initialPath = initialPath;
      this.onCopy = arg.onCopy;
      CopyDialog.__super__.constructor.call(this, {
        prompt: 'Enter the new path for the duplicate.',
        initialPath: atom.project.relativize(this.initialPath),
        select: true,
        iconClass: 'icon-arrow-right'
      });
    }

    CopyDialog.prototype.onConfirm = function(newPath) {
      var activeEditor, error, repo, rootPath;
      newPath = newPath.replace(/\s+$/, '');
      if (!path.isAbsolute(newPath)) {
        rootPath = atom.project.relativizePath(this.initialPath)[0];
        newPath = path.join(rootPath, newPath);
        if (!newPath) {
          return;
        }
      }
      if (this.initialPath === newPath) {
        this.close();
        return;
      }
      if (fs.existsSync(newPath)) {
        this.showError("'" + newPath + "' already exists.");
        return;
      }
      activeEditor = atom.workspace.getActiveTextEditor();
      if ((activeEditor != null ? activeEditor.getPath() : void 0) !== this.initialPath) {
        activeEditor = null;
      }
      try {
        if (fs.isDirectorySync(this.initialPath)) {
          fs.copySync(this.initialPath, newPath);
          if (typeof this.onCopy === "function") {
            this.onCopy({
              initialPath: this.initialPath,
              newPath: newPath
            });
          }
        } else {
          fs.copy(this.initialPath, newPath, (function(_this) {
            return function() {
              if (typeof _this.onCopy === "function") {
                _this.onCopy({
                  initialPath: _this.initialPath,
                  newPath: newPath
                });
              }
              return atom.workspace.open(newPath, {
                activatePane: true,
                initialLine: activeEditor != null ? activeEditor.getLastCursor().getBufferRow() : void 0,
                initialColumn: activeEditor != null ? activeEditor.getLastCursor().getBufferColumn() : void 0
              });
            };
          })(this));
        }
        if (repo = repoForPath(newPath)) {
          repo.getPathStatus(this.initialPath);
          repo.getPathStatus(newPath);
        }
        return this.close();
      } catch (error1) {
        error = error1;
        return this.showError(error.message + ".");
      }
    };

    return CopyDialog;

  })(Dialog);

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy90cmVlLXZpZXcvbGliL2NvcHktZGlhbG9nLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUEseUNBQUE7SUFBQTs7O0VBQUEsSUFBQSxHQUFPLE9BQUEsQ0FBUSxNQUFSOztFQUNQLEVBQUEsR0FBSyxPQUFBLENBQVEsU0FBUjs7RUFDTCxNQUFBLEdBQVMsT0FBQSxDQUFRLFVBQVI7O0VBQ1IsY0FBZSxPQUFBLENBQVEsV0FBUjs7RUFFaEIsTUFBTSxDQUFDLE9BQVAsR0FDTTs7O0lBQ1Msb0JBQUMsV0FBRCxFQUFlLEdBQWY7TUFBQyxJQUFDLENBQUEsY0FBRDtNQUFlLElBQUMsQ0FBQSxTQUFGLElBQUU7TUFDNUIsNENBQ0U7UUFBQSxNQUFBLEVBQVEsdUNBQVI7UUFDQSxXQUFBLEVBQWEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFiLENBQXdCLElBQUMsQ0FBQSxXQUF6QixDQURiO1FBRUEsTUFBQSxFQUFRLElBRlI7UUFHQSxTQUFBLEVBQVcsa0JBSFg7T0FERjtJQURXOzt5QkFPYixTQUFBLEdBQVcsU0FBQyxPQUFEO0FBQ1QsVUFBQTtNQUFBLE9BQUEsR0FBVSxPQUFPLENBQUMsT0FBUixDQUFnQixNQUFoQixFQUF3QixFQUF4QjtNQUNWLElBQUEsQ0FBTyxJQUFJLENBQUMsVUFBTCxDQUFnQixPQUFoQixDQUFQO1FBQ0csV0FBWSxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWIsQ0FBNEIsSUFBQyxDQUFBLFdBQTdCO1FBQ2IsT0FBQSxHQUFVLElBQUksQ0FBQyxJQUFMLENBQVUsUUFBVixFQUFvQixPQUFwQjtRQUNWLElBQUEsQ0FBYyxPQUFkO0FBQUEsaUJBQUE7U0FIRjs7TUFLQSxJQUFHLElBQUMsQ0FBQSxXQUFELEtBQWdCLE9BQW5CO1FBQ0UsSUFBQyxDQUFBLEtBQUQsQ0FBQTtBQUNBLGVBRkY7O01BSUEsSUFBRyxFQUFFLENBQUMsVUFBSCxDQUFjLE9BQWQsQ0FBSDtRQUNFLElBQUMsQ0FBQSxTQUFELENBQVcsR0FBQSxHQUFJLE9BQUosR0FBWSxtQkFBdkI7QUFDQSxlQUZGOztNQUlBLFlBQUEsR0FBZSxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFmLENBQUE7TUFDZiw0QkFBMkIsWUFBWSxDQUFFLE9BQWQsQ0FBQSxXQUFBLEtBQTJCLElBQUMsQ0FBQSxXQUF2RDtRQUFBLFlBQUEsR0FBZSxLQUFmOztBQUNBO1FBQ0UsSUFBRyxFQUFFLENBQUMsZUFBSCxDQUFtQixJQUFDLENBQUEsV0FBcEIsQ0FBSDtVQUNFLEVBQUUsQ0FBQyxRQUFILENBQVksSUFBQyxDQUFBLFdBQWIsRUFBMEIsT0FBMUI7O1lBQ0EsSUFBQyxDQUFBLE9BQVE7Y0FBQyxXQUFBLEVBQWEsSUFBQyxDQUFBLFdBQWY7Y0FBNEIsT0FBQSxFQUFTLE9BQXJDOztXQUZYO1NBQUEsTUFBQTtVQUlFLEVBQUUsQ0FBQyxJQUFILENBQVEsSUFBQyxDQUFBLFdBQVQsRUFBc0IsT0FBdEIsRUFBK0IsQ0FBQSxTQUFBLEtBQUE7bUJBQUEsU0FBQTs7Z0JBQzdCLEtBQUMsQ0FBQSxPQUFRO2tCQUFDLFdBQUEsRUFBYSxLQUFDLENBQUEsV0FBZjtrQkFBNEIsT0FBQSxFQUFTLE9BQXJDOzs7cUJBQ1QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFmLENBQW9CLE9BQXBCLEVBQ0U7Z0JBQUEsWUFBQSxFQUFjLElBQWQ7Z0JBQ0EsV0FBQSx5QkFBYSxZQUFZLENBQUUsYUFBZCxDQUFBLENBQTZCLENBQUMsWUFBOUIsQ0FBQSxVQURiO2dCQUVBLGFBQUEseUJBQWUsWUFBWSxDQUFFLGFBQWQsQ0FBQSxDQUE2QixDQUFDLGVBQTlCLENBQUEsVUFGZjtlQURGO1lBRjZCO1VBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEvQixFQUpGOztRQVVBLElBQUcsSUFBQSxHQUFPLFdBQUEsQ0FBWSxPQUFaLENBQVY7VUFDRSxJQUFJLENBQUMsYUFBTCxDQUFtQixJQUFDLENBQUEsV0FBcEI7VUFDQSxJQUFJLENBQUMsYUFBTCxDQUFtQixPQUFuQixFQUZGOztlQUdBLElBQUMsQ0FBQSxLQUFELENBQUEsRUFkRjtPQUFBLGNBQUE7UUFlTTtlQUNKLElBQUMsQ0FBQSxTQUFELENBQWMsS0FBSyxDQUFDLE9BQVAsR0FBZSxHQUE1QixFQWhCRjs7SUFqQlM7Ozs7S0FSWTtBQU56QiIsInNvdXJjZXNDb250ZW50IjpbInBhdGggPSByZXF1aXJlICdwYXRoJ1xuZnMgPSByZXF1aXJlICdmcy1wbHVzJ1xuRGlhbG9nID0gcmVxdWlyZSAnLi9kaWFsb2cnXG57cmVwb0ZvclBhdGh9ID0gcmVxdWlyZSBcIi4vaGVscGVyc1wiXG5cbm1vZHVsZS5leHBvcnRzID1cbmNsYXNzIENvcHlEaWFsb2cgZXh0ZW5kcyBEaWFsb2dcbiAgY29uc3RydWN0b3I6IChAaW5pdGlhbFBhdGgsIHtAb25Db3B5fSkgLT5cbiAgICBzdXBlclxuICAgICAgcHJvbXB0OiAnRW50ZXIgdGhlIG5ldyBwYXRoIGZvciB0aGUgZHVwbGljYXRlLidcbiAgICAgIGluaXRpYWxQYXRoOiBhdG9tLnByb2plY3QucmVsYXRpdml6ZShAaW5pdGlhbFBhdGgpXG4gICAgICBzZWxlY3Q6IHRydWVcbiAgICAgIGljb25DbGFzczogJ2ljb24tYXJyb3ctcmlnaHQnXG5cbiAgb25Db25maXJtOiAobmV3UGF0aCkgLT5cbiAgICBuZXdQYXRoID0gbmV3UGF0aC5yZXBsYWNlKC9cXHMrJC8sICcnKSAjIFJlbW92ZSB0cmFpbGluZyB3aGl0ZXNwYWNlXG4gICAgdW5sZXNzIHBhdGguaXNBYnNvbHV0ZShuZXdQYXRoKVxuICAgICAgW3Jvb3RQYXRoXSA9IGF0b20ucHJvamVjdC5yZWxhdGl2aXplUGF0aChAaW5pdGlhbFBhdGgpXG4gICAgICBuZXdQYXRoID0gcGF0aC5qb2luKHJvb3RQYXRoLCBuZXdQYXRoKVxuICAgICAgcmV0dXJuIHVubGVzcyBuZXdQYXRoXG5cbiAgICBpZiBAaW5pdGlhbFBhdGggaXMgbmV3UGF0aFxuICAgICAgQGNsb3NlKClcbiAgICAgIHJldHVyblxuXG4gICAgaWYgZnMuZXhpc3RzU3luYyhuZXdQYXRoKVxuICAgICAgQHNob3dFcnJvcihcIicje25ld1BhdGh9JyBhbHJlYWR5IGV4aXN0cy5cIilcbiAgICAgIHJldHVyblxuXG4gICAgYWN0aXZlRWRpdG9yID0gYXRvbS53b3Jrc3BhY2UuZ2V0QWN0aXZlVGV4dEVkaXRvcigpXG4gICAgYWN0aXZlRWRpdG9yID0gbnVsbCB1bmxlc3MgYWN0aXZlRWRpdG9yPy5nZXRQYXRoKCkgaXMgQGluaXRpYWxQYXRoXG4gICAgdHJ5XG4gICAgICBpZiBmcy5pc0RpcmVjdG9yeVN5bmMoQGluaXRpYWxQYXRoKVxuICAgICAgICBmcy5jb3B5U3luYyhAaW5pdGlhbFBhdGgsIG5ld1BhdGgpXG4gICAgICAgIEBvbkNvcHk/KHtpbml0aWFsUGF0aDogQGluaXRpYWxQYXRoLCBuZXdQYXRoOiBuZXdQYXRofSlcbiAgICAgIGVsc2VcbiAgICAgICAgZnMuY29weSBAaW5pdGlhbFBhdGgsIG5ld1BhdGgsID0+XG4gICAgICAgICAgQG9uQ29weT8oe2luaXRpYWxQYXRoOiBAaW5pdGlhbFBhdGgsIG5ld1BhdGg6IG5ld1BhdGh9KVxuICAgICAgICAgIGF0b20ud29ya3NwYWNlLm9wZW4gbmV3UGF0aCxcbiAgICAgICAgICAgIGFjdGl2YXRlUGFuZTogdHJ1ZVxuICAgICAgICAgICAgaW5pdGlhbExpbmU6IGFjdGl2ZUVkaXRvcj8uZ2V0TGFzdEN1cnNvcigpLmdldEJ1ZmZlclJvdygpXG4gICAgICAgICAgICBpbml0aWFsQ29sdW1uOiBhY3RpdmVFZGl0b3I/LmdldExhc3RDdXJzb3IoKS5nZXRCdWZmZXJDb2x1bW4oKVxuICAgICAgaWYgcmVwbyA9IHJlcG9Gb3JQYXRoKG5ld1BhdGgpXG4gICAgICAgIHJlcG8uZ2V0UGF0aFN0YXR1cyhAaW5pdGlhbFBhdGgpXG4gICAgICAgIHJlcG8uZ2V0UGF0aFN0YXR1cyhuZXdQYXRoKVxuICAgICAgQGNsb3NlKClcbiAgICBjYXRjaCBlcnJvclxuICAgICAgQHNob3dFcnJvcihcIiN7ZXJyb3IubWVzc2FnZX0uXCIpXG4iXX0=
