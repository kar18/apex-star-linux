(function() {
  var CompositeDisposable, Disposable, Emitter, NotificationsLog, NotificationsLogItem, ref, typeIcons;

  ref = require('atom'), Emitter = ref.Emitter, CompositeDisposable = ref.CompositeDisposable, Disposable = ref.Disposable;

  NotificationsLogItem = require('./notifications-log-item');

  typeIcons = {
    fatal: 'bug',
    error: 'flame',
    warning: 'alert',
    info: 'info',
    success: 'check'
  };

  module.exports = NotificationsLog = (function() {
    NotificationsLog.prototype.subscriptions = null;

    NotificationsLog.prototype.logItems = [];

    NotificationsLog.prototype.typesHidden = {
      fatal: false,
      error: false,
      warning: false,
      info: false,
      success: false
    };

    function NotificationsLog(duplicateTimeDelay, typesHidden) {
      this.duplicateTimeDelay = duplicateTimeDelay;
      if (typesHidden == null) {
        typesHidden = null;
      }
      if (typesHidden != null) {
        this.typesHidden = typesHidden;
      }
      this.emitter = new Emitter;
      this.subscriptions = new CompositeDisposable;
      this.subscriptions.add(atom.notifications.onDidClearNotifications((function(_this) {
        return function() {
          return _this.clearLogItems();
        };
      })(this)));
      this.render();
      this.subscriptions.add(new Disposable((function(_this) {
        return function() {
          return _this.clearLogItems();
        };
      })(this)));
    }

    NotificationsLog.prototype.render = function() {
      var button, header, i, icon, lastNotification, len, notification, ref1, timeSpan, type;
      this.element = document.createElement('div');
      this.element.classList.add('notifications-log');
      header = document.createElement('header');
      this.element.appendChild(header);
      this.list = document.createElement('ul');
      this.list.classList.add('notifications-log-items');
      this.element.appendChild(this.list);
      for (type in typeIcons) {
        icon = typeIcons[type];
        button = document.createElement('button');
        button.classList.add('notification-type', 'btn', 'icon', "icon-" + icon, type);
        button.classList.toggle('show-type', !this.typesHidden[type]);
        this.list.classList.toggle("hide-" + type, this.typesHidden[type]);
        button.dataset.type = type;
        button.addEventListener('click', (function(_this) {
          return function(e) {
            return _this.toggleType(e.target.dataset.type);
          };
        })(this));
        this.subscriptions.add(atom.tooltips.add(button, {
          title: "Toggle " + type + " notifications"
        }));
        header.appendChild(button);
      }
      button = document.createElement('button');
      button.classList.add('notifications-clear-log', 'btn', 'icon', 'icon-trashcan');
      button.addEventListener('click', function(e) {
        return atom.commands.dispatch(atom.views.getView(atom.workspace), "notifications:clear-log");
      });
      this.subscriptions.add(atom.tooltips.add(button, {
        title: "Clear notifications"
      }));
      header.appendChild(button);
      lastNotification = null;
      ref1 = atom.notifications.getNotifications();
      for (i = 0, len = ref1.length; i < len; i++) {
        notification = ref1[i];
        if (lastNotification != null) {
          timeSpan = notification.getTimestamp() - lastNotification.getTimestamp();
          if (!(timeSpan < this.duplicateTimeDelay && notification.isEqual(lastNotification))) {
            this.addNotification(notification);
          }
        } else {
          this.addNotification(notification);
        }
        lastNotification = notification;
      }
      return this.subscriptions.add(new Disposable((function(_this) {
        return function() {
          return _this.element.remove();
        };
      })(this)));
    };

    NotificationsLog.prototype.destroy = function() {
      this.subscriptions.dispose();
      return this.emitter.emit('did-destroy');
    };

    NotificationsLog.prototype.getElement = function() {
      return this.element;
    };

    NotificationsLog.prototype.getURI = function() {
      return 'atom://notifications/log';
    };

    NotificationsLog.prototype.getTitle = function() {
      return 'Log';
    };

    NotificationsLog.prototype.getLongTitle = function() {
      return 'Notifications Log';
    };

    NotificationsLog.prototype.getIconName = function() {
      return 'alert';
    };

    NotificationsLog.prototype.getDefaultLocation = function() {
      return 'bottom';
    };

    NotificationsLog.prototype.getAllowedLocations = function() {
      return ['left', 'right', 'bottom'];
    };

    NotificationsLog.prototype.serialize = function() {
      return {
        typesHidden: this.typesHidden,
        deserializer: 'notifications/NotificationsLog'
      };
    };

    NotificationsLog.prototype.toggleType = function(type, force) {
      var button, hide;
      button = this.element.querySelector(".notification-type." + type);
      hide = !button.classList.toggle('show-type', force);
      this.list.classList.toggle("hide-" + type, hide);
      return this.typesHidden[type] = hide;
    };

    NotificationsLog.prototype.addNotification = function(notification) {
      var logItem;
      logItem = new NotificationsLogItem(notification);
      logItem.onClick((function(_this) {
        return function() {
          return _this.emitter.emit('item-clicked', notification);
        };
      })(this));
      this.logItems.push(logItem);
      return this.list.insertBefore(logItem.getElement(), this.list.firstChild);
    };

    NotificationsLog.prototype.onItemClick = function(callback) {
      return this.emitter.on('item-clicked', callback);
    };

    NotificationsLog.prototype.onDidDestroy = function(callback) {
      return this.emitter.on('did-destroy', callback);
    };

    NotificationsLog.prototype.clearLogItems = function() {
      var i, len, logItem, ref1;
      ref1 = this.logItems;
      for (i = 0, len = ref1.length; i < len; i++) {
        logItem = ref1[i];
        logItem.destroy();
      }
      return this.logItems = [];
    };

    return NotificationsLog;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9ub3RpZmljYXRpb25zL2xpYi9ub3RpZmljYXRpb25zLWxvZy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLE1BQTZDLE9BQUEsQ0FBUSxNQUFSLENBQTdDLEVBQUMscUJBQUQsRUFBVSw2Q0FBVixFQUErQjs7RUFDL0Isb0JBQUEsR0FBdUIsT0FBQSxDQUFRLDBCQUFSOztFQUV2QixTQUFBLEdBQ0U7SUFBQSxLQUFBLEVBQU8sS0FBUDtJQUNBLEtBQUEsRUFBTyxPQURQO0lBRUEsT0FBQSxFQUFTLE9BRlQ7SUFHQSxJQUFBLEVBQU0sTUFITjtJQUlBLE9BQUEsRUFBUyxPQUpUOzs7RUFNRixNQUFNLENBQUMsT0FBUCxHQUF1QjsrQkFDckIsYUFBQSxHQUFlOzsrQkFDZixRQUFBLEdBQVU7OytCQUNWLFdBQUEsR0FDRTtNQUFBLEtBQUEsRUFBTyxLQUFQO01BQ0EsS0FBQSxFQUFPLEtBRFA7TUFFQSxPQUFBLEVBQVMsS0FGVDtNQUdBLElBQUEsRUFBTSxLQUhOO01BSUEsT0FBQSxFQUFTLEtBSlQ7OztJQU1XLDBCQUFDLGtCQUFELEVBQXNCLFdBQXRCO01BQUMsSUFBQyxDQUFBLHFCQUFEOztRQUFxQixjQUFjOztNQUMvQyxJQUE4QixtQkFBOUI7UUFBQSxJQUFDLENBQUEsV0FBRCxHQUFlLFlBQWY7O01BQ0EsSUFBQyxDQUFBLE9BQUQsR0FBVyxJQUFJO01BQ2YsSUFBQyxDQUFBLGFBQUQsR0FBaUIsSUFBSTtNQUNyQixJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyx1QkFBbkIsQ0FBMkMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxhQUFELENBQUE7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBM0MsQ0FBbkI7TUFDQSxJQUFDLENBQUEsTUFBRCxDQUFBO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksVUFBSixDQUFlLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsYUFBRCxDQUFBO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWYsQ0FBbkI7SUFOVzs7K0JBUWIsTUFBQSxHQUFRLFNBQUE7QUFDTixVQUFBO01BQUEsSUFBQyxDQUFBLE9BQUQsR0FBVyxRQUFRLENBQUMsYUFBVCxDQUF1QixLQUF2QjtNQUNYLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLG1CQUF2QjtNQUVBLE1BQUEsR0FBUyxRQUFRLENBQUMsYUFBVCxDQUF1QixRQUF2QjtNQUNULElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxDQUFxQixNQUFyQjtNQUVBLElBQUMsQ0FBQSxJQUFELEdBQVEsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsSUFBdkI7TUFDUixJQUFDLENBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFoQixDQUFvQix5QkFBcEI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsQ0FBcUIsSUFBQyxDQUFBLElBQXRCO0FBRUEsV0FBQSxpQkFBQTs7UUFDRSxNQUFBLEdBQVMsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsUUFBdkI7UUFDVCxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQWpCLENBQXFCLG1CQUFyQixFQUEwQyxLQUExQyxFQUFpRCxNQUFqRCxFQUF5RCxPQUFBLEdBQVEsSUFBakUsRUFBeUUsSUFBekU7UUFDQSxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQWpCLENBQXdCLFdBQXhCLEVBQXFDLENBQUksSUFBQyxDQUFBLFdBQVksQ0FBQSxJQUFBLENBQXREO1FBQ0EsSUFBQyxDQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBaEIsQ0FBdUIsT0FBQSxHQUFRLElBQS9CLEVBQXVDLElBQUMsQ0FBQSxXQUFZLENBQUEsSUFBQSxDQUFwRDtRQUNBLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBZixHQUFzQjtRQUN0QixNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUMsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQyxDQUFEO21CQUFPLEtBQUMsQ0FBQSxVQUFELENBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBN0I7VUFBUDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBakM7UUFDQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLE1BQWxCLEVBQTBCO1VBQUMsS0FBQSxFQUFPLFNBQUEsR0FBVSxJQUFWLEdBQWUsZ0JBQXZCO1NBQTFCLENBQW5CO1FBQ0EsTUFBTSxDQUFDLFdBQVAsQ0FBbUIsTUFBbkI7QUFSRjtNQVVBLE1BQUEsR0FBUyxRQUFRLENBQUMsYUFBVCxDQUF1QixRQUF2QjtNQUNULE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBakIsQ0FBcUIseUJBQXJCLEVBQWdELEtBQWhELEVBQXVELE1BQXZELEVBQStELGVBQS9EO01BQ0EsTUFBTSxDQUFDLGdCQUFQLENBQXdCLE9BQXhCLEVBQWlDLFNBQUMsQ0FBRDtlQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBZCxDQUF1QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVgsQ0FBbUIsSUFBSSxDQUFDLFNBQXhCLENBQXZCLEVBQTJELHlCQUEzRDtNQUFQLENBQWpDO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixNQUFsQixFQUEwQjtRQUFDLEtBQUEsRUFBTyxxQkFBUjtPQUExQixDQUFuQjtNQUNBLE1BQU0sQ0FBQyxXQUFQLENBQW1CLE1BQW5CO01BRUEsZ0JBQUEsR0FBbUI7QUFDbkI7QUFBQSxXQUFBLHNDQUFBOztRQUNFLElBQUcsd0JBQUg7VUFFRSxRQUFBLEdBQVcsWUFBWSxDQUFDLFlBQWIsQ0FBQSxDQUFBLEdBQThCLGdCQUFnQixDQUFDLFlBQWpCLENBQUE7VUFDekMsSUFBQSxDQUFBLENBQU8sUUFBQSxHQUFXLElBQUMsQ0FBQSxrQkFBWixJQUFtQyxZQUFZLENBQUMsT0FBYixDQUFxQixnQkFBckIsQ0FBMUMsQ0FBQTtZQUNFLElBQUMsQ0FBQSxlQUFELENBQWlCLFlBQWpCLEVBREY7V0FIRjtTQUFBLE1BQUE7VUFNRSxJQUFDLENBQUEsZUFBRCxDQUFpQixZQUFqQixFQU5GOztRQVFBLGdCQUFBLEdBQW1CO0FBVHJCO2FBV0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksVUFBSixDQUFlLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBTyxDQUFDLE1BQVQsQ0FBQTtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFmLENBQW5CO0lBdkNNOzsrQkF5Q1IsT0FBQSxHQUFTLFNBQUE7TUFDUCxJQUFDLENBQUEsYUFBYSxDQUFDLE9BQWYsQ0FBQTthQUNBLElBQUMsQ0FBQSxPQUFPLENBQUMsSUFBVCxDQUFjLGFBQWQ7SUFGTzs7K0JBSVQsVUFBQSxHQUFZLFNBQUE7YUFBRyxJQUFDLENBQUE7SUFBSjs7K0JBRVosTUFBQSxHQUFRLFNBQUE7YUFBRztJQUFIOzsrQkFFUixRQUFBLEdBQVUsU0FBQTthQUFHO0lBQUg7OytCQUVWLFlBQUEsR0FBYyxTQUFBO2FBQUc7SUFBSDs7K0JBRWQsV0FBQSxHQUFhLFNBQUE7YUFBRztJQUFIOzsrQkFFYixrQkFBQSxHQUFvQixTQUFBO2FBQUc7SUFBSDs7K0JBRXBCLG1CQUFBLEdBQXFCLFNBQUE7YUFBRyxDQUFDLE1BQUQsRUFBUyxPQUFULEVBQWtCLFFBQWxCO0lBQUg7OytCQUVyQixTQUFBLEdBQVcsU0FBQTtBQUNULGFBQU87UUFDSixhQUFELElBQUMsQ0FBQSxXQURJO1FBRUwsWUFBQSxFQUFjLGdDQUZUOztJQURFOzsrQkFNWCxVQUFBLEdBQVksU0FBQyxJQUFELEVBQU8sS0FBUDtBQUNWLFVBQUE7TUFBQSxNQUFBLEdBQVMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxhQUFULENBQXVCLHFCQUFBLEdBQXNCLElBQTdDO01BQ1QsSUFBQSxHQUFPLENBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFqQixDQUF3QixXQUF4QixFQUFxQyxLQUFyQztNQUNYLElBQUMsQ0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQWhCLENBQXVCLE9BQUEsR0FBUSxJQUEvQixFQUF1QyxJQUF2QzthQUNBLElBQUMsQ0FBQSxXQUFZLENBQUEsSUFBQSxDQUFiLEdBQXFCO0lBSlg7OytCQU1aLGVBQUEsR0FBaUIsU0FBQyxZQUFEO0FBQ2YsVUFBQTtNQUFBLE9BQUEsR0FBVSxJQUFJLG9CQUFKLENBQXlCLFlBQXpCO01BQ1YsT0FBTyxDQUFDLE9BQVIsQ0FBZ0IsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFPLENBQUMsSUFBVCxDQUFjLGNBQWQsRUFBOEIsWUFBOUI7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBaEI7TUFDQSxJQUFDLENBQUEsUUFBUSxDQUFDLElBQVYsQ0FBZSxPQUFmO2FBQ0EsSUFBQyxDQUFBLElBQUksQ0FBQyxZQUFOLENBQW1CLE9BQU8sQ0FBQyxVQUFSLENBQUEsQ0FBbkIsRUFBeUMsSUFBQyxDQUFBLElBQUksQ0FBQyxVQUEvQztJQUplOzsrQkFNakIsV0FBQSxHQUFhLFNBQUMsUUFBRDthQUNYLElBQUMsQ0FBQSxPQUFPLENBQUMsRUFBVCxDQUFZLGNBQVosRUFBNEIsUUFBNUI7SUFEVzs7K0JBR2IsWUFBQSxHQUFjLFNBQUMsUUFBRDthQUNaLElBQUMsQ0FBQSxPQUFPLENBQUMsRUFBVCxDQUFZLGFBQVosRUFBMkIsUUFBM0I7SUFEWTs7K0JBR2QsYUFBQSxHQUFlLFNBQUE7QUFDYixVQUFBO0FBQUE7QUFBQSxXQUFBLHNDQUFBOztRQUFBLE9BQU8sQ0FBQyxPQUFSLENBQUE7QUFBQTthQUNBLElBQUMsQ0FBQSxRQUFELEdBQVk7SUFGQzs7Ozs7QUEvR2pCIiwic291cmNlc0NvbnRlbnQiOlsie0VtaXR0ZXIsIENvbXBvc2l0ZURpc3Bvc2FibGUsIERpc3Bvc2FibGV9ID0gcmVxdWlyZSAnYXRvbSdcbk5vdGlmaWNhdGlvbnNMb2dJdGVtID0gcmVxdWlyZSAnLi9ub3RpZmljYXRpb25zLWxvZy1pdGVtJ1xuXG50eXBlSWNvbnMgPVxuICBmYXRhbDogJ2J1ZydcbiAgZXJyb3I6ICdmbGFtZSdcbiAgd2FybmluZzogJ2FsZXJ0J1xuICBpbmZvOiAnaW5mbydcbiAgc3VjY2VzczogJ2NoZWNrJ1xuXG5tb2R1bGUuZXhwb3J0cyA9IGNsYXNzIE5vdGlmaWNhdGlvbnNMb2dcbiAgc3Vic2NyaXB0aW9uczogbnVsbFxuICBsb2dJdGVtczogW11cbiAgdHlwZXNIaWRkZW46XG4gICAgZmF0YWw6IGZhbHNlXG4gICAgZXJyb3I6IGZhbHNlXG4gICAgd2FybmluZzogZmFsc2VcbiAgICBpbmZvOiBmYWxzZVxuICAgIHN1Y2Nlc3M6IGZhbHNlXG5cbiAgY29uc3RydWN0b3I6IChAZHVwbGljYXRlVGltZURlbGF5LCB0eXBlc0hpZGRlbiA9IG51bGwpIC0+XG4gICAgQHR5cGVzSGlkZGVuID0gdHlwZXNIaWRkZW4gaWYgdHlwZXNIaWRkZW4/XG4gICAgQGVtaXR0ZXIgPSBuZXcgRW1pdHRlclxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5ub3RpZmljYXRpb25zLm9uRGlkQ2xlYXJOb3RpZmljYXRpb25zID0+IEBjbGVhckxvZ0l0ZW1zKClcbiAgICBAcmVuZGVyKClcbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgbmV3IERpc3Bvc2FibGUgPT4gQGNsZWFyTG9nSXRlbXMoKVxuXG4gIHJlbmRlcjogLT5cbiAgICBAZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnbm90aWZpY2F0aW9ucy1sb2cnKVxuXG4gICAgaGVhZGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaGVhZGVyJylcbiAgICBAZWxlbWVudC5hcHBlbmRDaGlsZChoZWFkZXIpXG5cbiAgICBAbGlzdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3VsJylcbiAgICBAbGlzdC5jbGFzc0xpc3QuYWRkKCdub3RpZmljYXRpb25zLWxvZy1pdGVtcycpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQoQGxpc3QpXG5cbiAgICBmb3IgdHlwZSwgaWNvbiBvZiB0eXBlSWNvbnNcbiAgICAgIGJ1dHRvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2J1dHRvbicpXG4gICAgICBidXR0b24uY2xhc3NMaXN0LmFkZCgnbm90aWZpY2F0aW9uLXR5cGUnLCAnYnRuJywgJ2ljb24nLCBcImljb24tI3tpY29ufVwiLCB0eXBlKVxuICAgICAgYnV0dG9uLmNsYXNzTGlzdC50b2dnbGUoJ3Nob3ctdHlwZScsIG5vdCBAdHlwZXNIaWRkZW5bdHlwZV0pXG4gICAgICBAbGlzdC5jbGFzc0xpc3QudG9nZ2xlKFwiaGlkZS0je3R5cGV9XCIsIEB0eXBlc0hpZGRlblt0eXBlXSlcbiAgICAgIGJ1dHRvbi5kYXRhc2V0LnR5cGUgPSB0eXBlXG4gICAgICBidXR0b24uYWRkRXZlbnRMaXN0ZW5lciAnY2xpY2snLCAoZSkgPT4gQHRvZ2dsZVR5cGUoZS50YXJnZXQuZGF0YXNldC50eXBlKVxuICAgICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20udG9vbHRpcHMuYWRkKGJ1dHRvbiwge3RpdGxlOiBcIlRvZ2dsZSAje3R5cGV9IG5vdGlmaWNhdGlvbnNcIn0pXG4gICAgICBoZWFkZXIuYXBwZW5kQ2hpbGQoYnV0dG9uKVxuXG4gICAgYnV0dG9uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYnV0dG9uJylcbiAgICBidXR0b24uY2xhc3NMaXN0LmFkZCgnbm90aWZpY2F0aW9ucy1jbGVhci1sb2cnLCAnYnRuJywgJ2ljb24nLCAnaWNvbi10cmFzaGNhbicpXG4gICAgYnV0dG9uLmFkZEV2ZW50TGlzdGVuZXIgJ2NsaWNrJywgKGUpIC0+IGF0b20uY29tbWFuZHMuZGlzcGF0Y2goYXRvbS52aWV3cy5nZXRWaWV3KGF0b20ud29ya3NwYWNlKSwgXCJub3RpZmljYXRpb25zOmNsZWFyLWxvZ1wiKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLnRvb2x0aXBzLmFkZChidXR0b24sIHt0aXRsZTogXCJDbGVhciBub3RpZmljYXRpb25zXCJ9KVxuICAgIGhlYWRlci5hcHBlbmRDaGlsZChidXR0b24pXG5cbiAgICBsYXN0Tm90aWZpY2F0aW9uID0gbnVsbFxuICAgIGZvciBub3RpZmljYXRpb24gaW4gYXRvbS5ub3RpZmljYXRpb25zLmdldE5vdGlmaWNhdGlvbnMoKVxuICAgICAgaWYgbGFzdE5vdGlmaWNhdGlvbj9cbiAgICAgICAgIyBkbyBub3Qgc2hvdyBkdXBsaWNhdGVzIHVubGVzcyBzb21lIGFtb3VudCBvZiB0aW1lIGhhcyBwYXNzZWRcbiAgICAgICAgdGltZVNwYW4gPSBub3RpZmljYXRpb24uZ2V0VGltZXN0YW1wKCkgLSBsYXN0Tm90aWZpY2F0aW9uLmdldFRpbWVzdGFtcCgpXG4gICAgICAgIHVubGVzcyB0aW1lU3BhbiA8IEBkdXBsaWNhdGVUaW1lRGVsYXkgYW5kIG5vdGlmaWNhdGlvbi5pc0VxdWFsKGxhc3ROb3RpZmljYXRpb24pXG4gICAgICAgICAgQGFkZE5vdGlmaWNhdGlvbihub3RpZmljYXRpb24pXG4gICAgICBlbHNlXG4gICAgICAgIEBhZGROb3RpZmljYXRpb24obm90aWZpY2F0aW9uKVxuXG4gICAgICBsYXN0Tm90aWZpY2F0aW9uID0gbm90aWZpY2F0aW9uXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgbmV3IERpc3Bvc2FibGUgPT4gQGVsZW1lbnQucmVtb3ZlKClcblxuICBkZXN0cm95OiAtPlxuICAgIEBzdWJzY3JpcHRpb25zLmRpc3Bvc2UoKVxuICAgIEBlbWl0dGVyLmVtaXQgJ2RpZC1kZXN0cm95J1xuXG4gIGdldEVsZW1lbnQ6IC0+IEBlbGVtZW50XG5cbiAgZ2V0VVJJOiAtPiAnYXRvbTovL25vdGlmaWNhdGlvbnMvbG9nJ1xuXG4gIGdldFRpdGxlOiAtPiAnTG9nJ1xuXG4gIGdldExvbmdUaXRsZTogLT4gJ05vdGlmaWNhdGlvbnMgTG9nJ1xuXG4gIGdldEljb25OYW1lOiAtPiAnYWxlcnQnXG5cbiAgZ2V0RGVmYXVsdExvY2F0aW9uOiAtPiAnYm90dG9tJ1xuXG4gIGdldEFsbG93ZWRMb2NhdGlvbnM6IC0+IFsnbGVmdCcsICdyaWdodCcsICdib3R0b20nXVxuXG4gIHNlcmlhbGl6ZTogLT5cbiAgICByZXR1cm4ge1xuICAgICAgQHR5cGVzSGlkZGVuXG4gICAgICBkZXNlcmlhbGl6ZXI6ICdub3RpZmljYXRpb25zL05vdGlmaWNhdGlvbnNMb2cnXG4gICAgfVxuXG4gIHRvZ2dsZVR5cGU6ICh0eXBlLCBmb3JjZSkgLT5cbiAgICBidXR0b24gPSBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKFwiLm5vdGlmaWNhdGlvbi10eXBlLiN7dHlwZX1cIilcbiAgICBoaWRlID0gbm90IGJ1dHRvbi5jbGFzc0xpc3QudG9nZ2xlKCdzaG93LXR5cGUnLCBmb3JjZSlcbiAgICBAbGlzdC5jbGFzc0xpc3QudG9nZ2xlKFwiaGlkZS0je3R5cGV9XCIsIGhpZGUpXG4gICAgQHR5cGVzSGlkZGVuW3R5cGVdID0gaGlkZVxuXG4gIGFkZE5vdGlmaWNhdGlvbjogKG5vdGlmaWNhdGlvbikgLT5cbiAgICBsb2dJdGVtID0gbmV3IE5vdGlmaWNhdGlvbnNMb2dJdGVtKG5vdGlmaWNhdGlvbilcbiAgICBsb2dJdGVtLm9uQ2xpY2sgPT4gQGVtaXR0ZXIuZW1pdCgnaXRlbS1jbGlja2VkJywgbm90aWZpY2F0aW9uKVxuICAgIEBsb2dJdGVtcy5wdXNoIGxvZ0l0ZW1cbiAgICBAbGlzdC5pbnNlcnRCZWZvcmUobG9nSXRlbS5nZXRFbGVtZW50KCksIEBsaXN0LmZpcnN0Q2hpbGQpXG5cbiAgb25JdGVtQ2xpY2s6IChjYWxsYmFjaykgLT5cbiAgICBAZW1pdHRlci5vbiAnaXRlbS1jbGlja2VkJywgY2FsbGJhY2tcblxuICBvbkRpZERlc3Ryb3k6IChjYWxsYmFjaykgLT5cbiAgICBAZW1pdHRlci5vbiAnZGlkLWRlc3Ryb3knLCBjYWxsYmFja1xuXG4gIGNsZWFyTG9nSXRlbXM6IC0+XG4gICAgbG9nSXRlbS5kZXN0cm95KCkgZm9yIGxvZ0l0ZW0gaW4gQGxvZ0l0ZW1zXG4gICAgQGxvZ0l0ZW1zID0gW11cbiJdfQ==
