(function() {
  var ButtonListTemplate, ButtonTemplate, DOMPurify, FatalMetaNotificationTemplate, MetaNotificationTemplate, NotificationElement, NotificationIssue, NotificationTemplate, TemplateHelper, UserUtilities, addSplitLinesToContainer, createDOMPurify, fs, marked, path, shell;

  createDOMPurify = require('dompurify');

  fs = require('fs-plus');

  path = require('path');

  marked = require('marked');

  shell = require('electron').shell;

  NotificationIssue = require('./notification-issue');

  TemplateHelper = require('./template-helper');

  UserUtilities = require('./user-utilities');

  DOMPurify = null;

  NotificationTemplate = "<div class=\"content\">\n  <div class=\"message item\"></div>\n  <div class=\"detail item\">\n    <div class=\"detail-content\"></div>\n    <a href=\"#\" class=\"stack-toggle\"></a>\n    <div class=\"stack-container\"></div>\n  </div>\n  <div class=\"meta item\"></div>\n</div>\n<div class=\"close icon icon-x\"></div>\n<div class=\"close-all btn btn-error\">Close All</div>";

  FatalMetaNotificationTemplate = "<div class=\"description fatal-notification\"></div>\n<div class=\"btn-toolbar\">\n  <a href=\"#\" class=\"btn-issue btn btn-error\"></a>\n  <a href=\"#\" class=\"btn-copy-report icon icon-clippy\" title=\"Copy error report to clipboard\"></a>\n</div>";

  MetaNotificationTemplate = "<div class=\"description\"></div>";

  ButtonListTemplate = "<div class=\"btn-toolbar\"></div>";

  ButtonTemplate = "<a href=\"#\" class=\"btn\"></a>";

  module.exports = NotificationElement = (function() {
    NotificationElement.prototype.animationDuration = 360;

    NotificationElement.prototype.visibilityDuration = 5000;

    NotificationElement.prototype.autohideTimeout = null;

    function NotificationElement(model, visibilityDuration) {
      this.model = model;
      this.visibilityDuration = visibilityDuration;
      this.fatalTemplate = TemplateHelper.create(FatalMetaNotificationTemplate);
      this.metaTemplate = TemplateHelper.create(MetaNotificationTemplate);
      this.buttonListTemplate = TemplateHelper.create(ButtonListTemplate);
      this.buttonTemplate = TemplateHelper.create(ButtonTemplate);
      this.element = document.createElement('atom-notification');
      if (this.model.getType() === 'fatal') {
        this.issue = new NotificationIssue(this.model);
      }
      this.renderPromise = this.render()["catch"](function(e) {
        console.error(e.message);
        return console.error(e.stack);
      });
      this.model.onDidDismiss((function(_this) {
        return function() {
          return _this.removeNotification();
        };
      })(this));
      if (!this.model.isDismissable()) {
        this.autohide();
        this.element.addEventListener('click', this.makeDismissable.bind(this), {
          once: true
        });
      }
      this.element.issue = this.issue;
      this.element.getRenderPromise = this.getRenderPromise.bind(this);
    }

    NotificationElement.prototype.getModel = function() {
      return this.model;
    };

    NotificationElement.prototype.getRenderPromise = function() {
      return this.renderPromise;
    };

    NotificationElement.prototype.render = function() {
      var buttonClass, closeAllButton, closeButton, description, detail, metaContainer, metaContent, notificationContainer, options, stack, stackContainer, stackToggle, toolbar;
      this.element.classList.add("" + (this.model.getType()));
      this.element.classList.add("icon", "icon-" + (this.model.getIcon()), "native-key-bindings");
      if (detail = this.model.getDetail()) {
        this.element.classList.add('has-detail');
      }
      if (this.model.isDismissable()) {
        this.element.classList.add('has-close');
      }
      if (detail && (this.model.getOptions().stack != null)) {
        this.element.classList.add('has-stack');
      }
      this.element.setAttribute('tabindex', '-1');
      this.element.innerHTML = NotificationTemplate;
      options = this.model.getOptions();
      notificationContainer = this.element.querySelector('.message');
      if (DOMPurify === null) {
        DOMPurify = createDOMPurify();
      }
      notificationContainer.innerHTML = DOMPurify.sanitize(marked(this.model.getMessage()));
      if (detail = this.model.getDetail()) {
        addSplitLinesToContainer(this.element.querySelector('.detail-content'), detail);
        if (stack = options.stack) {
          stackToggle = this.element.querySelector('.stack-toggle');
          stackContainer = this.element.querySelector('.stack-container');
          addSplitLinesToContainer(stackContainer, stack);
          stackToggle.addEventListener('click', (function(_this) {
            return function(e) {
              return _this.handleStackTraceToggleClick(e, stackContainer);
            };
          })(this));
          this.handleStackTraceToggleClick({
            currentTarget: stackToggle
          }, stackContainer);
        }
      }
      if (metaContent = options.description) {
        this.element.classList.add('has-description');
        metaContainer = this.element.querySelector('.meta');
        metaContainer.appendChild(TemplateHelper.render(this.metaTemplate));
        description = this.element.querySelector('.description');
        description.innerHTML = marked(metaContent);
      }
      if (options.buttons && options.buttons.length > 0) {
        this.element.classList.add('has-buttons');
        metaContainer = this.element.querySelector('.meta');
        metaContainer.appendChild(TemplateHelper.render(this.buttonListTemplate));
        toolbar = this.element.querySelector('.btn-toolbar');
        buttonClass = this.model.getType();
        if (buttonClass === 'fatal') {
          buttonClass = 'error';
        }
        buttonClass = "btn-" + buttonClass;
        options.buttons.forEach((function(_this) {
          return function(button) {
            var buttonEl;
            toolbar.appendChild(TemplateHelper.render(_this.buttonTemplate));
            buttonEl = toolbar.childNodes[toolbar.childNodes.length - 1];
            buttonEl.textContent = button.text;
            buttonEl.classList.add(buttonClass);
            if (button.className != null) {
              buttonEl.classList.add.apply(buttonEl.classList, button.className.split(' '));
            }
            if (button.onDidClick != null) {
              return buttonEl.addEventListener('click', function(e) {
                return button.onDidClick.call(_this, e);
              });
            }
          };
        })(this));
      }
      closeButton = this.element.querySelector('.close');
      closeButton.addEventListener('click', (function(_this) {
        return function() {
          return _this.handleRemoveNotificationClick();
        };
      })(this));
      closeAllButton = this.element.querySelector('.close-all');
      closeAllButton.classList.add(this.getButtonClass());
      closeAllButton.addEventListener('click', (function(_this) {
        return function() {
          return _this.handleRemoveAllNotificationsClick();
        };
      })(this));
      if (this.model.getType() === 'fatal') {
        return this.renderFatalError();
      } else {
        return Promise.resolve();
      }
    };

    NotificationElement.prototype.renderFatalError = function() {
      var copyReportButton, fatalContainer, fatalNotification, issueButton, packageName, promises, repoUrl;
      repoUrl = this.issue.getRepoUrl();
      packageName = this.issue.getPackageName();
      fatalContainer = this.element.querySelector('.meta');
      fatalContainer.appendChild(TemplateHelper.render(this.fatalTemplate));
      fatalNotification = this.element.querySelector('.fatal-notification');
      issueButton = fatalContainer.querySelector('.btn-issue');
      copyReportButton = fatalContainer.querySelector('.btn-copy-report');
      atom.tooltips.add(copyReportButton, {
        title: copyReportButton.getAttribute('title')
      });
      copyReportButton.addEventListener('click', (function(_this) {
        return function(e) {
          e.preventDefault();
          return _this.issue.getIssueBody().then(function(issueBody) {
            return atom.clipboard.write(issueBody);
          });
        };
      })(this));
      if ((packageName != null) && (repoUrl != null)) {
        fatalNotification.innerHTML = "The error was thrown from the <a href=\"" + repoUrl + "\">" + packageName + " package</a>. ";
      } else if (packageName != null) {
        issueButton.remove();
        fatalNotification.textContent = "The error was thrown from the " + packageName + " package. ";
      } else {
        fatalNotification.textContent = "This is likely a bug in Atom. ";
      }
      if (issueButton.parentNode != null) {
        if ((packageName != null) && (repoUrl != null)) {
          issueButton.textContent = "Create issue on the " + packageName + " package";
        } else {
          issueButton.textContent = "Create issue on atom/atom";
        }
        promises = [];
        promises.push(this.issue.findSimilarIssues());
        promises.push(UserUtilities.checkAtomUpToDate());
        if (packageName != null) {
          promises.push(UserUtilities.checkPackageUpToDate(packageName));
        }
        return Promise.all(promises).then((function(_this) {
          return function(allData) {
            var atomCheck, issue, issues, packageCheck, packagePath, ref;
            issues = allData[0], atomCheck = allData[1], packageCheck = allData[2];
            if ((issues != null ? issues.open : void 0) || (issues != null ? issues.closed : void 0)) {
              issue = issues.open || issues.closed;
              issueButton.setAttribute('href', issue.html_url);
              issueButton.textContent = "View Issue";
              fatalNotification.innerHTML += " This issue has already been reported.";
            } else if ((packageCheck != null) && !packageCheck.upToDate && !packageCheck.isCore) {
              issueButton.setAttribute('href', '#');
              issueButton.textContent = "Check for package updates";
              issueButton.addEventListener('click', function(e) {
                var command;
                e.preventDefault();
                command = 'settings-view:check-for-package-updates';
                return atom.commands.dispatch(atom.views.getView(atom.workspace), command);
              });
              fatalNotification.innerHTML += "<code>" + packageName + "</code> is out of date: " + packageCheck.installedVersion + " installed;\n" + packageCheck.latestVersion + " latest.\nUpgrading to the latest version may fix this issue.";
            } else if ((packageCheck != null) && !packageCheck.upToDate && packageCheck.isCore) {
              issueButton.remove();
              fatalNotification.innerHTML += "<br><br>\nLocally installed core Atom package <code>" + packageName + "</code> is out of date: " + packageCheck.installedVersion + " installed locally;\n" + packageCheck.versionShippedWithAtom + " included with the version of Atom you're running.\nRemoving the locally installed version may fix this issue.";
              packagePath = (ref = atom.packages.getLoadedPackage(packageName)) != null ? ref.path : void 0;
              if (fs.isSymbolicLinkSync(packagePath)) {
                fatalNotification.innerHTML += "<br><br>\nUse: <code>apm unlink " + packagePath + "</code>";
              }
            } else if ((atomCheck != null) && !atomCheck.upToDate) {
              issueButton.remove();
              fatalNotification.innerHTML += "Atom is out of date: " + atomCheck.installedVersion + " installed;\n" + atomCheck.latestVersion + " latest.\nUpgrading to the <a href='https://github.com/atom/atom/releases/tag/v" + atomCheck.latestVersion + "'>latest version</a> may fix this issue.";
            } else {
              fatalNotification.innerHTML += " You can help by creating an issue. Please explain what actions triggered this error.";
              issueButton.addEventListener('click', function(e) {
                e.preventDefault();
                issueButton.classList.add('opening');
                return _this.issue.getIssueUrlForSystem().then(function(issueUrl) {
                  shell.openExternal(issueUrl);
                  return issueButton.classList.remove('opening');
                });
              });
            }
          };
        })(this));
      } else {
        return Promise.resolve();
      }
    };

    NotificationElement.prototype.makeDismissable = function() {
      if (!this.model.isDismissable()) {
        clearTimeout(this.autohideTimeout);
        this.model.options.dismissable = true;
        this.model.dismissed = false;
        return this.element.classList.add('has-close');
      }
    };

    NotificationElement.prototype.removeNotification = function() {
      if (!this.element.classList.contains('remove')) {
        this.element.classList.add('remove');
        return this.removeNotificationAfterTimeout();
      }
    };

    NotificationElement.prototype.handleRemoveNotificationClick = function() {
      this.removeNotification();
      return this.model.dismiss();
    };

    NotificationElement.prototype.handleRemoveAllNotificationsClick = function() {
      var i, len, notification, notifications;
      notifications = atom.notifications.getNotifications();
      for (i = 0, len = notifications.length; i < len; i++) {
        notification = notifications[i];
        atom.views.getView(notification).removeNotification();
        if (notification.isDismissable() && !notification.isDismissed()) {
          notification.dismiss();
        }
      }
    };

    NotificationElement.prototype.handleStackTraceToggleClick = function(e, container) {
      if (typeof e.preventDefault === "function") {
        e.preventDefault();
      }
      if (container.style.display === 'none') {
        e.currentTarget.innerHTML = '<span class="icon icon-dash"></span>Hide Stack Trace';
        return container.style.display = 'block';
      } else {
        e.currentTarget.innerHTML = '<span class="icon icon-plus"></span>Show Stack Trace';
        return container.style.display = 'none';
      }
    };

    NotificationElement.prototype.autohide = function() {
      return this.autohideTimeout = setTimeout((function(_this) {
        return function() {
          return _this.removeNotification();
        };
      })(this), this.visibilityDuration);
    };

    NotificationElement.prototype.removeNotificationAfterTimeout = function() {
      if (this.element === document.activeElement) {
        atom.workspace.getActivePane().activate();
      }
      return setTimeout((function(_this) {
        return function() {
          return _this.element.remove();
        };
      })(this), this.animationDuration);
    };

    NotificationElement.prototype.getButtonClass = function() {
      var type;
      type = "btn-" + (this.model.getType());
      if (type === 'btn-fatal') {
        return 'btn-error';
      } else {
        return type;
      }
    };

    return NotificationElement;

  })();

  addSplitLinesToContainer = function(container, content) {
    var div, i, len, line, ref;
    if (typeof content !== 'string') {
      content = content.toString();
    }
    ref = content.split('\n');
    for (i = 0, len = ref.length; i < len; i++) {
      line = ref[i];
      div = document.createElement('div');
      div.classList.add('line');
      div.textContent = line;
      container.appendChild(div);
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9ub3RpZmljYXRpb25zL2xpYi9ub3RpZmljYXRpb24tZWxlbWVudC5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLGVBQUEsR0FBa0IsT0FBQSxDQUFRLFdBQVI7O0VBQ2xCLEVBQUEsR0FBSyxPQUFBLENBQVEsU0FBUjs7RUFDTCxJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBQ1AsTUFBQSxHQUFTLE9BQUEsQ0FBUSxRQUFSOztFQUNSLFFBQVMsT0FBQSxDQUFRLFVBQVI7O0VBRVYsaUJBQUEsR0FBb0IsT0FBQSxDQUFRLHNCQUFSOztFQUNwQixjQUFBLEdBQWlCLE9BQUEsQ0FBUSxtQkFBUjs7RUFDakIsYUFBQSxHQUFnQixPQUFBLENBQVEsa0JBQVI7O0VBRWhCLFNBQUEsR0FBWTs7RUFFWixvQkFBQSxHQUF1Qjs7RUFjdkIsNkJBQUEsR0FBZ0M7O0VBUWhDLHdCQUFBLEdBQTJCOztFQUkzQixrQkFBQSxHQUFxQjs7RUFJckIsY0FBQSxHQUFpQjs7RUFJakIsTUFBTSxDQUFDLE9BQVAsR0FDTTtrQ0FDSixpQkFBQSxHQUFtQjs7a0NBQ25CLGtCQUFBLEdBQW9COztrQ0FDcEIsZUFBQSxHQUFpQjs7SUFFSiw2QkFBQyxLQUFELEVBQVMsa0JBQVQ7TUFBQyxJQUFDLENBQUEsUUFBRDtNQUFRLElBQUMsQ0FBQSxxQkFBRDtNQUNwQixJQUFDLENBQUEsYUFBRCxHQUFpQixjQUFjLENBQUMsTUFBZixDQUFzQiw2QkFBdEI7TUFDakIsSUFBQyxDQUFBLFlBQUQsR0FBZ0IsY0FBYyxDQUFDLE1BQWYsQ0FBc0Isd0JBQXRCO01BQ2hCLElBQUMsQ0FBQSxrQkFBRCxHQUFzQixjQUFjLENBQUMsTUFBZixDQUFzQixrQkFBdEI7TUFDdEIsSUFBQyxDQUFBLGNBQUQsR0FBa0IsY0FBYyxDQUFDLE1BQWYsQ0FBc0IsY0FBdEI7TUFFbEIsSUFBQyxDQUFBLE9BQUQsR0FBVyxRQUFRLENBQUMsYUFBVCxDQUF1QixtQkFBdkI7TUFDWCxJQUEwQyxJQUFDLENBQUEsS0FBSyxDQUFDLE9BQVAsQ0FBQSxDQUFBLEtBQW9CLE9BQTlEO1FBQUEsSUFBQyxDQUFBLEtBQUQsR0FBUyxJQUFJLGlCQUFKLENBQXNCLElBQUMsQ0FBQSxLQUF2QixFQUFUOztNQUNBLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUMsQ0FBQSxNQUFELENBQUEsQ0FBUyxFQUFDLEtBQUQsRUFBVCxDQUFnQixTQUFDLENBQUQ7UUFDL0IsT0FBTyxDQUFDLEtBQVIsQ0FBYyxDQUFDLENBQUMsT0FBaEI7ZUFDQSxPQUFPLENBQUMsS0FBUixDQUFjLENBQUMsQ0FBQyxLQUFoQjtNQUYrQixDQUFoQjtNQUlqQixJQUFDLENBQUEsS0FBSyxDQUFDLFlBQVAsQ0FBb0IsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxrQkFBRCxDQUFBO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXBCO01BRUEsSUFBQSxDQUFPLElBQUMsQ0FBQSxLQUFLLENBQUMsYUFBUCxDQUFBLENBQVA7UUFDRSxJQUFDLENBQUEsUUFBRCxDQUFBO1FBQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxJQUFDLENBQUEsZUFBZSxDQUFDLElBQWpCLENBQXNCLElBQXRCLENBQW5DLEVBQWdFO1VBQUMsSUFBQSxFQUFNLElBQVA7U0FBaEUsRUFGRjs7TUFJQSxJQUFDLENBQUEsT0FBTyxDQUFDLEtBQVQsR0FBaUIsSUFBQyxDQUFBO01BQ2xCLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsR0FBNEIsSUFBQyxDQUFBLGdCQUFnQixDQUFDLElBQWxCLENBQXVCLElBQXZCO0lBbkJqQjs7a0NBcUJiLFFBQUEsR0FBVSxTQUFBO2FBQUcsSUFBQyxDQUFBO0lBQUo7O2tDQUVWLGdCQUFBLEdBQWtCLFNBQUE7YUFBRyxJQUFDLENBQUE7SUFBSjs7a0NBRWxCLE1BQUEsR0FBUSxTQUFBO0FBQ04sVUFBQTtNQUFBLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLEVBQUEsR0FBRSxDQUFDLElBQUMsQ0FBQSxLQUFLLENBQUMsT0FBUCxDQUFBLENBQUQsQ0FBekI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixNQUF2QixFQUErQixPQUFBLEdBQU8sQ0FBQyxJQUFDLENBQUEsS0FBSyxDQUFDLE9BQVAsQ0FBQSxDQUFELENBQXRDLEVBQTJELHFCQUEzRDtNQUVBLElBQXdDLE1BQUEsR0FBUyxJQUFDLENBQUEsS0FBSyxDQUFDLFNBQVAsQ0FBQSxDQUFqRDtRQUFBLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLFlBQXZCLEVBQUE7O01BQ0EsSUFBdUMsSUFBQyxDQUFBLEtBQUssQ0FBQyxhQUFQLENBQUEsQ0FBdkM7UUFBQSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixXQUF2QixFQUFBOztNQUNBLElBQXVDLE1BQUEsSUFBVyx1Q0FBbEQ7UUFBQSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixXQUF2QixFQUFBOztNQUVBLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFBVCxDQUFzQixVQUF0QixFQUFrQyxJQUFsQztNQUVBLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBVCxHQUFxQjtNQUVyQixPQUFBLEdBQVUsSUFBQyxDQUFBLEtBQUssQ0FBQyxVQUFQLENBQUE7TUFFVixxQkFBQSxHQUF3QixJQUFDLENBQUEsT0FBTyxDQUFDLGFBQVQsQ0FBdUIsVUFBdkI7TUFFeEIsSUFBRyxTQUFBLEtBQWEsSUFBaEI7UUFDRSxTQUFBLEdBQVksZUFBQSxDQUFBLEVBRGQ7O01BRUEscUJBQXFCLENBQUMsU0FBdEIsR0FBa0MsU0FBUyxDQUFDLFFBQVYsQ0FBbUIsTUFBQSxDQUFPLElBQUMsQ0FBQSxLQUFLLENBQUMsVUFBUCxDQUFBLENBQVAsQ0FBbkI7TUFFbEMsSUFBRyxNQUFBLEdBQVMsSUFBQyxDQUFBLEtBQUssQ0FBQyxTQUFQLENBQUEsQ0FBWjtRQUNFLHdCQUFBLENBQXlCLElBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxDQUF1QixpQkFBdkIsQ0FBekIsRUFBb0UsTUFBcEU7UUFFQSxJQUFHLEtBQUEsR0FBUSxPQUFPLENBQUMsS0FBbkI7VUFDRSxXQUFBLEdBQWMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxhQUFULENBQXVCLGVBQXZCO1VBQ2QsY0FBQSxHQUFpQixJQUFDLENBQUEsT0FBTyxDQUFDLGFBQVQsQ0FBdUIsa0JBQXZCO1VBRWpCLHdCQUFBLENBQXlCLGNBQXpCLEVBQXlDLEtBQXpDO1VBRUEsV0FBVyxDQUFDLGdCQUFaLENBQTZCLE9BQTdCLEVBQXNDLENBQUEsU0FBQSxLQUFBO21CQUFBLFNBQUMsQ0FBRDtxQkFBTyxLQUFDLENBQUEsMkJBQUQsQ0FBNkIsQ0FBN0IsRUFBZ0MsY0FBaEM7WUFBUDtVQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBdEM7VUFDQSxJQUFDLENBQUEsMkJBQUQsQ0FBNkI7WUFBQyxhQUFBLEVBQWUsV0FBaEI7V0FBN0IsRUFBMkQsY0FBM0QsRUFQRjtTQUhGOztNQVlBLElBQUcsV0FBQSxHQUFjLE9BQU8sQ0FBQyxXQUF6QjtRQUNFLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLGlCQUF2QjtRQUNBLGFBQUEsR0FBZ0IsSUFBQyxDQUFBLE9BQU8sQ0FBQyxhQUFULENBQXVCLE9BQXZCO1FBQ2hCLGFBQWEsQ0FBQyxXQUFkLENBQTBCLGNBQWMsQ0FBQyxNQUFmLENBQXNCLElBQUMsQ0FBQSxZQUF2QixDQUExQjtRQUNBLFdBQUEsR0FBYyxJQUFDLENBQUEsT0FBTyxDQUFDLGFBQVQsQ0FBdUIsY0FBdkI7UUFDZCxXQUFXLENBQUMsU0FBWixHQUF3QixNQUFBLENBQU8sV0FBUCxFQUwxQjs7TUFPQSxJQUFHLE9BQU8sQ0FBQyxPQUFSLElBQW9CLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBaEIsR0FBeUIsQ0FBaEQ7UUFDRSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixhQUF2QjtRQUNBLGFBQUEsR0FBZ0IsSUFBQyxDQUFBLE9BQU8sQ0FBQyxhQUFULENBQXVCLE9BQXZCO1FBQ2hCLGFBQWEsQ0FBQyxXQUFkLENBQTBCLGNBQWMsQ0FBQyxNQUFmLENBQXNCLElBQUMsQ0FBQSxrQkFBdkIsQ0FBMUI7UUFDQSxPQUFBLEdBQVUsSUFBQyxDQUFBLE9BQU8sQ0FBQyxhQUFULENBQXVCLGNBQXZCO1FBQ1YsV0FBQSxHQUFjLElBQUMsQ0FBQSxLQUFLLENBQUMsT0FBUCxDQUFBO1FBQ2QsSUFBeUIsV0FBQSxLQUFlLE9BQXhDO1VBQUEsV0FBQSxHQUFjLFFBQWQ7O1FBQ0EsV0FBQSxHQUFjLE1BQUEsR0FBTztRQUNyQixPQUFPLENBQUMsT0FBTyxDQUFDLE9BQWhCLENBQXdCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUMsTUFBRDtBQUN0QixnQkFBQTtZQUFBLE9BQU8sQ0FBQyxXQUFSLENBQW9CLGNBQWMsQ0FBQyxNQUFmLENBQXNCLEtBQUMsQ0FBQSxjQUF2QixDQUFwQjtZQUNBLFFBQUEsR0FBVyxPQUFPLENBQUMsVUFBVyxDQUFBLE9BQU8sQ0FBQyxVQUFVLENBQUMsTUFBbkIsR0FBNEIsQ0FBNUI7WUFDOUIsUUFBUSxDQUFDLFdBQVQsR0FBdUIsTUFBTSxDQUFDO1lBQzlCLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsV0FBdkI7WUFDQSxJQUFHLHdCQUFIO2NBQ0UsUUFBUSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsS0FBdkIsQ0FBNkIsUUFBUSxDQUFDLFNBQXRDLEVBQWlELE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBakIsQ0FBdUIsR0FBdkIsQ0FBakQsRUFERjs7WUFFQSxJQUFHLHlCQUFIO3FCQUNFLFFBQVEsQ0FBQyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxTQUFDLENBQUQ7dUJBQ2pDLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBbEIsQ0FBdUIsS0FBdkIsRUFBNkIsQ0FBN0I7Y0FEaUMsQ0FBbkMsRUFERjs7VUFQc0I7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXhCLEVBUkY7O01BbUJBLFdBQUEsR0FBYyxJQUFDLENBQUEsT0FBTyxDQUFDLGFBQVQsQ0FBdUIsUUFBdkI7TUFDZCxXQUFXLENBQUMsZ0JBQVosQ0FBNkIsT0FBN0IsRUFBc0MsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSw2QkFBRCxDQUFBO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXRDO01BRUEsY0FBQSxHQUFpQixJQUFDLENBQUEsT0FBTyxDQUFDLGFBQVQsQ0FBdUIsWUFBdkI7TUFDakIsY0FBYyxDQUFDLFNBQVMsQ0FBQyxHQUF6QixDQUE2QixJQUFDLENBQUEsY0FBRCxDQUFBLENBQTdCO01BQ0EsY0FBYyxDQUFDLGdCQUFmLENBQWdDLE9BQWhDLEVBQXlDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsaUNBQUQsQ0FBQTtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6QztNQUVBLElBQUcsSUFBQyxDQUFBLEtBQUssQ0FBQyxPQUFQLENBQUEsQ0FBQSxLQUFvQixPQUF2QjtlQUNFLElBQUMsQ0FBQSxnQkFBRCxDQUFBLEVBREY7T0FBQSxNQUFBO2VBR0UsT0FBTyxDQUFDLE9BQVIsQ0FBQSxFQUhGOztJQWpFTTs7a0NBc0VSLGdCQUFBLEdBQWtCLFNBQUE7QUFDaEIsVUFBQTtNQUFBLE9BQUEsR0FBVSxJQUFDLENBQUEsS0FBSyxDQUFDLFVBQVAsQ0FBQTtNQUNWLFdBQUEsR0FBYyxJQUFDLENBQUEsS0FBSyxDQUFDLGNBQVAsQ0FBQTtNQUVkLGNBQUEsR0FBaUIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxhQUFULENBQXVCLE9BQXZCO01BQ2pCLGNBQWMsQ0FBQyxXQUFmLENBQTJCLGNBQWMsQ0FBQyxNQUFmLENBQXNCLElBQUMsQ0FBQSxhQUF2QixDQUEzQjtNQUNBLGlCQUFBLEdBQW9CLElBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxDQUF1QixxQkFBdkI7TUFFcEIsV0FBQSxHQUFjLGNBQWMsQ0FBQyxhQUFmLENBQTZCLFlBQTdCO01BRWQsZ0JBQUEsR0FBbUIsY0FBYyxDQUFDLGFBQWYsQ0FBNkIsa0JBQTdCO01BQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0M7UUFBQSxLQUFBLEVBQU8sZ0JBQWdCLENBQUMsWUFBakIsQ0FBOEIsT0FBOUIsQ0FBUDtPQUFwQztNQUNBLGdCQUFnQixDQUFDLGdCQUFqQixDQUFrQyxPQUFsQyxFQUEyQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsQ0FBRDtVQUN6QyxDQUFDLENBQUMsY0FBRixDQUFBO2lCQUNBLEtBQUMsQ0FBQSxLQUFLLENBQUMsWUFBUCxDQUFBLENBQXFCLENBQUMsSUFBdEIsQ0FBMkIsU0FBQyxTQUFEO21CQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQWYsQ0FBcUIsU0FBckI7VUFEeUIsQ0FBM0I7UUFGeUM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTNDO01BS0EsSUFBRyxxQkFBQSxJQUFpQixpQkFBcEI7UUFDRSxpQkFBaUIsQ0FBQyxTQUFsQixHQUE4QiwwQ0FBQSxHQUEyQyxPQUEzQyxHQUFtRCxLQUFuRCxHQUF3RCxXQUF4RCxHQUFvRSxpQkFEcEc7T0FBQSxNQUVLLElBQUcsbUJBQUg7UUFDSCxXQUFXLENBQUMsTUFBWixDQUFBO1FBQ0EsaUJBQWlCLENBQUMsV0FBbEIsR0FBZ0MsZ0NBQUEsR0FBaUMsV0FBakMsR0FBNkMsYUFGMUU7T0FBQSxNQUFBO1FBSUgsaUJBQWlCLENBQUMsV0FBbEIsR0FBZ0MsaUNBSjdCOztNQU9MLElBQUcsOEJBQUg7UUFDRSxJQUFHLHFCQUFBLElBQWlCLGlCQUFwQjtVQUNFLFdBQVcsQ0FBQyxXQUFaLEdBQTBCLHNCQUFBLEdBQXVCLFdBQXZCLEdBQW1DLFdBRC9EO1NBQUEsTUFBQTtVQUdFLFdBQVcsQ0FBQyxXQUFaLEdBQTBCLDRCQUg1Qjs7UUFLQSxRQUFBLEdBQVc7UUFDWCxRQUFRLENBQUMsSUFBVCxDQUFjLElBQUMsQ0FBQSxLQUFLLENBQUMsaUJBQVAsQ0FBQSxDQUFkO1FBQ0EsUUFBUSxDQUFDLElBQVQsQ0FBYyxhQUFhLENBQUMsaUJBQWQsQ0FBQSxDQUFkO1FBQ0EsSUFBaUUsbUJBQWpFO1VBQUEsUUFBUSxDQUFDLElBQVQsQ0FBYyxhQUFhLENBQUMsb0JBQWQsQ0FBbUMsV0FBbkMsQ0FBZCxFQUFBOztlQUVBLE9BQU8sQ0FBQyxHQUFSLENBQVksUUFBWixDQUFxQixDQUFDLElBQXRCLENBQTJCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUMsT0FBRDtBQUN6QixnQkFBQTtZQUFDLG1CQUFELEVBQVMsc0JBQVQsRUFBb0I7WUFFcEIsc0JBQUcsTUFBTSxDQUFFLGNBQVIsc0JBQWdCLE1BQU0sQ0FBRSxnQkFBM0I7Y0FDRSxLQUFBLEdBQVEsTUFBTSxDQUFDLElBQVAsSUFBZSxNQUFNLENBQUM7Y0FDOUIsV0FBVyxDQUFDLFlBQVosQ0FBeUIsTUFBekIsRUFBaUMsS0FBSyxDQUFDLFFBQXZDO2NBQ0EsV0FBVyxDQUFDLFdBQVosR0FBMEI7Y0FDMUIsaUJBQWlCLENBQUMsU0FBbEIsSUFBK0IseUNBSmpDO2FBQUEsTUFLSyxJQUFHLHNCQUFBLElBQWtCLENBQUksWUFBWSxDQUFDLFFBQW5DLElBQWdELENBQUksWUFBWSxDQUFDLE1BQXBFO2NBQ0gsV0FBVyxDQUFDLFlBQVosQ0FBeUIsTUFBekIsRUFBaUMsR0FBakM7Y0FDQSxXQUFXLENBQUMsV0FBWixHQUEwQjtjQUMxQixXQUFXLENBQUMsZ0JBQVosQ0FBNkIsT0FBN0IsRUFBc0MsU0FBQyxDQUFEO0FBQ3BDLG9CQUFBO2dCQUFBLENBQUMsQ0FBQyxjQUFGLENBQUE7Z0JBQ0EsT0FBQSxHQUFVO3VCQUNWLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBZCxDQUF1QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVgsQ0FBbUIsSUFBSSxDQUFDLFNBQXhCLENBQXZCLEVBQTJELE9BQTNEO2NBSG9DLENBQXRDO2NBS0EsaUJBQWlCLENBQUMsU0FBbEIsSUFBK0IsUUFBQSxHQUNyQixXQURxQixHQUNULDBCQURTLEdBQ2lCLFlBQVksQ0FBQyxnQkFEOUIsR0FDK0MsZUFEL0MsR0FFM0IsWUFBWSxDQUFDLGFBRmMsR0FFQSxnRUFWNUI7YUFBQSxNQWFBLElBQUcsc0JBQUEsSUFBa0IsQ0FBSSxZQUFZLENBQUMsUUFBbkMsSUFBZ0QsWUFBWSxDQUFDLE1BQWhFO2NBQ0gsV0FBVyxDQUFDLE1BQVosQ0FBQTtjQUVBLGlCQUFpQixDQUFDLFNBQWxCLElBQStCLHNEQUFBLEdBRWUsV0FGZixHQUUyQiwwQkFGM0IsR0FFcUQsWUFBWSxDQUFDLGdCQUZsRSxHQUVtRix1QkFGbkYsR0FHM0IsWUFBWSxDQUFDLHNCQUhjLEdBR1M7Y0FJeEMsV0FBQSxvRUFBeUQsQ0FBRTtjQUMzRCxJQUFHLEVBQUUsQ0FBQyxrQkFBSCxDQUFzQixXQUF0QixDQUFIO2dCQUNFLGlCQUFpQixDQUFDLFNBQWxCLElBQStCLGtDQUFBLEdBRVAsV0FGTyxHQUVLLFVBSHRDO2VBWEc7YUFBQSxNQWdCQSxJQUFHLG1CQUFBLElBQWUsQ0FBSSxTQUFTLENBQUMsUUFBaEM7Y0FDSCxXQUFXLENBQUMsTUFBWixDQUFBO2NBRUEsaUJBQWlCLENBQUMsU0FBbEIsSUFBK0IsdUJBQUEsR0FDTixTQUFTLENBQUMsZ0JBREosR0FDcUIsZUFEckIsR0FFM0IsU0FBUyxDQUFDLGFBRmlCLEdBRUgsaUZBRkcsR0FHMEMsU0FBUyxDQUFDLGFBSHBELEdBR2tFLDJDQU45RjthQUFBLE1BQUE7Y0FTSCxpQkFBaUIsQ0FBQyxTQUFsQixJQUErQjtjQUMvQixXQUFXLENBQUMsZ0JBQVosQ0FBNkIsT0FBN0IsRUFBc0MsU0FBQyxDQUFEO2dCQUNwQyxDQUFDLENBQUMsY0FBRixDQUFBO2dCQUNBLFdBQVcsQ0FBQyxTQUFTLENBQUMsR0FBdEIsQ0FBMEIsU0FBMUI7dUJBQ0EsS0FBQyxDQUFBLEtBQUssQ0FBQyxvQkFBUCxDQUFBLENBQTZCLENBQUMsSUFBOUIsQ0FBbUMsU0FBQyxRQUFEO2tCQUNqQyxLQUFLLENBQUMsWUFBTixDQUFtQixRQUFuQjt5QkFDQSxXQUFXLENBQUMsU0FBUyxDQUFDLE1BQXRCLENBQTZCLFNBQTdCO2dCQUZpQyxDQUFuQztjQUhvQyxDQUF0QyxFQVZHOztVQXJDb0I7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTNCLEVBWEY7T0FBQSxNQUFBO2VBbUVFLE9BQU8sQ0FBQyxPQUFSLENBQUEsRUFuRUY7O0lBMUJnQjs7a0NBK0ZsQixlQUFBLEdBQWlCLFNBQUE7TUFDZixJQUFBLENBQU8sSUFBQyxDQUFBLEtBQUssQ0FBQyxhQUFQLENBQUEsQ0FBUDtRQUNFLFlBQUEsQ0FBYSxJQUFDLENBQUEsZUFBZDtRQUNBLElBQUMsQ0FBQSxLQUFLLENBQUMsT0FBTyxDQUFDLFdBQWYsR0FBNkI7UUFDN0IsSUFBQyxDQUFBLEtBQUssQ0FBQyxTQUFQLEdBQW1CO2VBQ25CLElBQUMsQ0FBQSxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQW5CLENBQXVCLFdBQXZCLEVBSkY7O0lBRGU7O2tDQU9qQixrQkFBQSxHQUFvQixTQUFBO01BQ2xCLElBQUEsQ0FBTyxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFuQixDQUE0QixRQUE1QixDQUFQO1FBQ0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsUUFBdkI7ZUFDQSxJQUFDLENBQUEsOEJBQUQsQ0FBQSxFQUZGOztJQURrQjs7a0NBS3BCLDZCQUFBLEdBQStCLFNBQUE7TUFDN0IsSUFBQyxDQUFBLGtCQUFELENBQUE7YUFDQSxJQUFDLENBQUEsS0FBSyxDQUFDLE9BQVAsQ0FBQTtJQUY2Qjs7a0NBSS9CLGlDQUFBLEdBQW1DLFNBQUE7QUFDakMsVUFBQTtNQUFBLGFBQUEsR0FBZ0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBbkIsQ0FBQTtBQUNoQixXQUFBLCtDQUFBOztRQUNFLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBWCxDQUFtQixZQUFuQixDQUFnQyxDQUFDLGtCQUFqQyxDQUFBO1FBQ0EsSUFBRyxZQUFZLENBQUMsYUFBYixDQUFBLENBQUEsSUFBaUMsQ0FBSSxZQUFZLENBQUMsV0FBYixDQUFBLENBQXhDO1VBQ0UsWUFBWSxDQUFDLE9BQWIsQ0FBQSxFQURGOztBQUZGO0lBRmlDOztrQ0FRbkMsMkJBQUEsR0FBNkIsU0FBQyxDQUFELEVBQUksU0FBSjs7UUFDM0IsQ0FBQyxDQUFDOztNQUNGLElBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFoQixLQUEyQixNQUE5QjtRQUNFLENBQUMsQ0FBQyxhQUFhLENBQUMsU0FBaEIsR0FBNEI7ZUFDNUIsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFoQixHQUEwQixRQUY1QjtPQUFBLE1BQUE7UUFJRSxDQUFDLENBQUMsYUFBYSxDQUFDLFNBQWhCLEdBQTRCO2VBQzVCLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBaEIsR0FBMEIsT0FMNUI7O0lBRjJCOztrQ0FTN0IsUUFBQSxHQUFVLFNBQUE7YUFDUixJQUFDLENBQUEsZUFBRCxHQUFtQixVQUFBLENBQVcsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUM1QixLQUFDLENBQUEsa0JBQUQsQ0FBQTtRQUQ0QjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWCxFQUVqQixJQUFDLENBQUEsa0JBRmdCO0lBRFg7O2tDQUtWLDhCQUFBLEdBQWdDLFNBQUE7TUFDOUIsSUFBNkMsSUFBQyxDQUFBLE9BQUQsS0FBWSxRQUFRLENBQUMsYUFBbEU7UUFBQSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWYsQ0FBQSxDQUE4QixDQUFDLFFBQS9CLENBQUEsRUFBQTs7YUFFQSxVQUFBLENBQVcsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUNULEtBQUMsQ0FBQSxPQUFPLENBQUMsTUFBVCxDQUFBO1FBRFM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVgsRUFFRSxJQUFDLENBQUEsaUJBRkg7SUFIOEI7O2tDQU9oQyxjQUFBLEdBQWdCLFNBQUE7QUFDZCxVQUFBO01BQUEsSUFBQSxHQUFPLE1BQUEsR0FBTSxDQUFDLElBQUMsQ0FBQSxLQUFLLENBQUMsT0FBUCxDQUFBLENBQUQ7TUFDYixJQUFHLElBQUEsS0FBUSxXQUFYO2VBQTRCLFlBQTVCO09BQUEsTUFBQTtlQUE2QyxLQUE3Qzs7SUFGYzs7Ozs7O0VBSWxCLHdCQUFBLEdBQTJCLFNBQUMsU0FBRCxFQUFZLE9BQVo7QUFDekIsUUFBQTtJQUFBLElBQWdDLE9BQU8sT0FBUCxLQUFvQixRQUFwRDtNQUFBLE9BQUEsR0FBVSxPQUFPLENBQUMsUUFBUixDQUFBLEVBQVY7O0FBQ0E7QUFBQSxTQUFBLHFDQUFBOztNQUNFLEdBQUEsR0FBTSxRQUFRLENBQUMsYUFBVCxDQUF1QixLQUF2QjtNQUNOLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBZCxDQUFrQixNQUFsQjtNQUNBLEdBQUcsQ0FBQyxXQUFKLEdBQWtCO01BQ2xCLFNBQVMsQ0FBQyxXQUFWLENBQXNCLEdBQXRCO0FBSkY7RUFGeUI7QUFuUzNCIiwic291cmNlc0NvbnRlbnQiOlsiY3JlYXRlRE9NUHVyaWZ5ID0gcmVxdWlyZSAnZG9tcHVyaWZ5J1xuZnMgPSByZXF1aXJlICdmcy1wbHVzJ1xucGF0aCA9IHJlcXVpcmUgJ3BhdGgnXG5tYXJrZWQgPSByZXF1aXJlICdtYXJrZWQnXG57c2hlbGx9ID0gcmVxdWlyZSAnZWxlY3Ryb24nXG5cbk5vdGlmaWNhdGlvbklzc3VlID0gcmVxdWlyZSAnLi9ub3RpZmljYXRpb24taXNzdWUnXG5UZW1wbGF0ZUhlbHBlciA9IHJlcXVpcmUgJy4vdGVtcGxhdGUtaGVscGVyJ1xuVXNlclV0aWxpdGllcyA9IHJlcXVpcmUgJy4vdXNlci11dGlsaXRpZXMnXG5cbkRPTVB1cmlmeSA9IG51bGxcblxuTm90aWZpY2F0aW9uVGVtcGxhdGUgPSBcIlwiXCJcbiAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cbiAgICA8ZGl2IGNsYXNzPVwibWVzc2FnZSBpdGVtXCI+PC9kaXY+XG4gICAgPGRpdiBjbGFzcz1cImRldGFpbCBpdGVtXCI+XG4gICAgICA8ZGl2IGNsYXNzPVwiZGV0YWlsLWNvbnRlbnRcIj48L2Rpdj5cbiAgICAgIDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJzdGFjay10b2dnbGVcIj48L2E+XG4gICAgICA8ZGl2IGNsYXNzPVwic3RhY2stY29udGFpbmVyXCI+PC9kaXY+XG4gICAgPC9kaXY+XG4gICAgPGRpdiBjbGFzcz1cIm1ldGEgaXRlbVwiPjwvZGl2PlxuICA8L2Rpdj5cbiAgPGRpdiBjbGFzcz1cImNsb3NlIGljb24gaWNvbi14XCI+PC9kaXY+XG4gIDxkaXYgY2xhc3M9XCJjbG9zZS1hbGwgYnRuIGJ0bi1lcnJvclwiPkNsb3NlIEFsbDwvZGl2PlxuXCJcIlwiXG5cbkZhdGFsTWV0YU5vdGlmaWNhdGlvblRlbXBsYXRlID0gXCJcIlwiXG4gIDxkaXYgY2xhc3M9XCJkZXNjcmlwdGlvbiBmYXRhbC1ub3RpZmljYXRpb25cIj48L2Rpdj5cbiAgPGRpdiBjbGFzcz1cImJ0bi10b29sYmFyXCI+XG4gICAgPGEgaHJlZj1cIiNcIiBjbGFzcz1cImJ0bi1pc3N1ZSBidG4gYnRuLWVycm9yXCI+PC9hPlxuICAgIDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJidG4tY29weS1yZXBvcnQgaWNvbiBpY29uLWNsaXBweVwiIHRpdGxlPVwiQ29weSBlcnJvciByZXBvcnQgdG8gY2xpcGJvYXJkXCI+PC9hPlxuICA8L2Rpdj5cblwiXCJcIlxuXG5NZXRhTm90aWZpY2F0aW9uVGVtcGxhdGUgPSBcIlwiXCJcbiAgPGRpdiBjbGFzcz1cImRlc2NyaXB0aW9uXCI+PC9kaXY+XG5cIlwiXCJcblxuQnV0dG9uTGlzdFRlbXBsYXRlID0gXCJcIlwiXG4gIDxkaXYgY2xhc3M9XCJidG4tdG9vbGJhclwiPjwvZGl2PlxuXCJcIlwiXG5cbkJ1dHRvblRlbXBsYXRlID0gXCJcIlwiXG4gIDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJidG5cIj48L2E+XG5cIlwiXCJcblxubW9kdWxlLmV4cG9ydHMgPVxuY2xhc3MgTm90aWZpY2F0aW9uRWxlbWVudFxuICBhbmltYXRpb25EdXJhdGlvbjogMzYwXG4gIHZpc2liaWxpdHlEdXJhdGlvbjogNTAwMFxuICBhdXRvaGlkZVRpbWVvdXQ6IG51bGxcblxuICBjb25zdHJ1Y3RvcjogKEBtb2RlbCwgQHZpc2liaWxpdHlEdXJhdGlvbikgLT5cbiAgICBAZmF0YWxUZW1wbGF0ZSA9IFRlbXBsYXRlSGVscGVyLmNyZWF0ZShGYXRhbE1ldGFOb3RpZmljYXRpb25UZW1wbGF0ZSlcbiAgICBAbWV0YVRlbXBsYXRlID0gVGVtcGxhdGVIZWxwZXIuY3JlYXRlKE1ldGFOb3RpZmljYXRpb25UZW1wbGF0ZSlcbiAgICBAYnV0dG9uTGlzdFRlbXBsYXRlID0gVGVtcGxhdGVIZWxwZXIuY3JlYXRlKEJ1dHRvbkxpc3RUZW1wbGF0ZSlcbiAgICBAYnV0dG9uVGVtcGxhdGUgPSBUZW1wbGF0ZUhlbHBlci5jcmVhdGUoQnV0dG9uVGVtcGxhdGUpXG5cbiAgICBAZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2F0b20tbm90aWZpY2F0aW9uJylcbiAgICBAaXNzdWUgPSBuZXcgTm90aWZpY2F0aW9uSXNzdWUoQG1vZGVsKSBpZiBAbW9kZWwuZ2V0VHlwZSgpIGlzICdmYXRhbCdcbiAgICBAcmVuZGVyUHJvbWlzZSA9IEByZW5kZXIoKS5jYXRjaCAoZSkgLT5cbiAgICAgIGNvbnNvbGUuZXJyb3IgZS5tZXNzYWdlXG4gICAgICBjb25zb2xlLmVycm9yIGUuc3RhY2tcblxuICAgIEBtb2RlbC5vbkRpZERpc21pc3MgPT4gQHJlbW92ZU5vdGlmaWNhdGlvbigpXG5cbiAgICB1bmxlc3MgQG1vZGVsLmlzRGlzbWlzc2FibGUoKVxuICAgICAgQGF1dG9oaWRlKClcbiAgICAgIEBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIgJ2NsaWNrJywgQG1ha2VEaXNtaXNzYWJsZS5iaW5kKHRoaXMpLCB7b25jZTogdHJ1ZX1cblxuICAgIEBlbGVtZW50Lmlzc3VlID0gQGlzc3VlXG4gICAgQGVsZW1lbnQuZ2V0UmVuZGVyUHJvbWlzZSA9IEBnZXRSZW5kZXJQcm9taXNlLmJpbmQodGhpcylcblxuICBnZXRNb2RlbDogLT4gQG1vZGVsXG5cbiAgZ2V0UmVuZGVyUHJvbWlzZTogLT4gQHJlbmRlclByb21pc2VcblxuICByZW5kZXI6IC0+XG4gICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCBcIiN7QG1vZGVsLmdldFR5cGUoKX1cIlxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQgXCJpY29uXCIsIFwiaWNvbi0je0Btb2RlbC5nZXRJY29uKCl9XCIsIFwibmF0aXZlLWtleS1iaW5kaW5nc1wiXG5cbiAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdoYXMtZGV0YWlsJykgaWYgZGV0YWlsID0gQG1vZGVsLmdldERldGFpbCgpXG4gICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaGFzLWNsb3NlJykgaWYgQG1vZGVsLmlzRGlzbWlzc2FibGUoKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2hhcy1zdGFjaycpIGlmIGRldGFpbCBhbmQgQG1vZGVsLmdldE9wdGlvbnMoKS5zdGFjaz9cblxuICAgIEBlbGVtZW50LnNldEF0dHJpYnV0ZSgndGFiaW5kZXgnLCAnLTEnKVxuXG4gICAgQGVsZW1lbnQuaW5uZXJIVE1MID0gTm90aWZpY2F0aW9uVGVtcGxhdGVcblxuICAgIG9wdGlvbnMgPSBAbW9kZWwuZ2V0T3B0aW9ucygpXG5cbiAgICBub3RpZmljYXRpb25Db250YWluZXIgPSBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcubWVzc2FnZScpXG5cbiAgICBpZiBET01QdXJpZnkgaXMgbnVsbFxuICAgICAgRE9NUHVyaWZ5ID0gY3JlYXRlRE9NUHVyaWZ5KClcbiAgICBub3RpZmljYXRpb25Db250YWluZXIuaW5uZXJIVE1MID0gRE9NUHVyaWZ5LnNhbml0aXplKG1hcmtlZChAbW9kZWwuZ2V0TWVzc2FnZSgpKSlcblxuICAgIGlmIGRldGFpbCA9IEBtb2RlbC5nZXREZXRhaWwoKVxuICAgICAgYWRkU3BsaXRMaW5lc1RvQ29udGFpbmVyKEBlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5kZXRhaWwtY29udGVudCcpLCBkZXRhaWwpXG5cbiAgICAgIGlmIHN0YWNrID0gb3B0aW9ucy5zdGFja1xuICAgICAgICBzdGFja1RvZ2dsZSA9IEBlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zdGFjay10b2dnbGUnKVxuICAgICAgICBzdGFja0NvbnRhaW5lciA9IEBlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zdGFjay1jb250YWluZXInKVxuXG4gICAgICAgIGFkZFNwbGl0TGluZXNUb0NvbnRhaW5lcihzdGFja0NvbnRhaW5lciwgc3RhY2spXG5cbiAgICAgICAgc3RhY2tUb2dnbGUuYWRkRXZlbnRMaXN0ZW5lciAnY2xpY2snLCAoZSkgPT4gQGhhbmRsZVN0YWNrVHJhY2VUb2dnbGVDbGljayhlLCBzdGFja0NvbnRhaW5lcilcbiAgICAgICAgQGhhbmRsZVN0YWNrVHJhY2VUb2dnbGVDbGljayh7Y3VycmVudFRhcmdldDogc3RhY2tUb2dnbGV9LCBzdGFja0NvbnRhaW5lcilcblxuICAgIGlmIG1ldGFDb250ZW50ID0gb3B0aW9ucy5kZXNjcmlwdGlvblxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaGFzLWRlc2NyaXB0aW9uJylcbiAgICAgIG1ldGFDb250YWluZXIgPSBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcubWV0YScpXG4gICAgICBtZXRhQ29udGFpbmVyLmFwcGVuZENoaWxkKFRlbXBsYXRlSGVscGVyLnJlbmRlcihAbWV0YVRlbXBsYXRlKSlcbiAgICAgIGRlc2NyaXB0aW9uID0gQGVsZW1lbnQucXVlcnlTZWxlY3RvcignLmRlc2NyaXB0aW9uJylcbiAgICAgIGRlc2NyaXB0aW9uLmlubmVySFRNTCA9IG1hcmtlZChtZXRhQ29udGVudClcblxuICAgIGlmIG9wdGlvbnMuYnV0dG9ucyBhbmQgb3B0aW9ucy5idXR0b25zLmxlbmd0aCA+IDBcbiAgICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2hhcy1idXR0b25zJylcbiAgICAgIG1ldGFDb250YWluZXIgPSBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcubWV0YScpXG4gICAgICBtZXRhQ29udGFpbmVyLmFwcGVuZENoaWxkKFRlbXBsYXRlSGVscGVyLnJlbmRlcihAYnV0dG9uTGlzdFRlbXBsYXRlKSlcbiAgICAgIHRvb2xiYXIgPSBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuYnRuLXRvb2xiYXInKVxuICAgICAgYnV0dG9uQ2xhc3MgPSBAbW9kZWwuZ2V0VHlwZSgpXG4gICAgICBidXR0b25DbGFzcyA9ICdlcnJvcicgaWYgYnV0dG9uQ2xhc3MgaXMgJ2ZhdGFsJ1xuICAgICAgYnV0dG9uQ2xhc3MgPSBcImJ0bi0je2J1dHRvbkNsYXNzfVwiXG4gICAgICBvcHRpb25zLmJ1dHRvbnMuZm9yRWFjaCAoYnV0dG9uKSA9PlxuICAgICAgICB0b29sYmFyLmFwcGVuZENoaWxkKFRlbXBsYXRlSGVscGVyLnJlbmRlcihAYnV0dG9uVGVtcGxhdGUpKVxuICAgICAgICBidXR0b25FbCA9IHRvb2xiYXIuY2hpbGROb2Rlc1t0b29sYmFyLmNoaWxkTm9kZXMubGVuZ3RoIC0gMV1cbiAgICAgICAgYnV0dG9uRWwudGV4dENvbnRlbnQgPSBidXR0b24udGV4dFxuICAgICAgICBidXR0b25FbC5jbGFzc0xpc3QuYWRkKGJ1dHRvbkNsYXNzKVxuICAgICAgICBpZiBidXR0b24uY2xhc3NOYW1lP1xuICAgICAgICAgIGJ1dHRvbkVsLmNsYXNzTGlzdC5hZGQuYXBwbHkoYnV0dG9uRWwuY2xhc3NMaXN0LCBidXR0b24uY2xhc3NOYW1lLnNwbGl0KCcgJykpXG4gICAgICAgIGlmIGJ1dHRvbi5vbkRpZENsaWNrP1xuICAgICAgICAgIGJ1dHRvbkVsLmFkZEV2ZW50TGlzdGVuZXIgJ2NsaWNrJywgKGUpID0+XG4gICAgICAgICAgICBidXR0b24ub25EaWRDbGljay5jYWxsKHRoaXMsIGUpXG5cbiAgICBjbG9zZUJ1dHRvbiA9IEBlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jbG9zZScpXG4gICAgY2xvc2VCdXR0b24uYWRkRXZlbnRMaXN0ZW5lciAnY2xpY2snLCA9PiBAaGFuZGxlUmVtb3ZlTm90aWZpY2F0aW9uQ2xpY2soKVxuXG4gICAgY2xvc2VBbGxCdXR0b24gPSBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuY2xvc2UtYWxsJylcbiAgICBjbG9zZUFsbEJ1dHRvbi5jbGFzc0xpc3QuYWRkIEBnZXRCdXR0b25DbGFzcygpXG4gICAgY2xvc2VBbGxCdXR0b24uYWRkRXZlbnRMaXN0ZW5lciAnY2xpY2snLCA9PiBAaGFuZGxlUmVtb3ZlQWxsTm90aWZpY2F0aW9uc0NsaWNrKClcblxuICAgIGlmIEBtb2RlbC5nZXRUeXBlKCkgaXMgJ2ZhdGFsJ1xuICAgICAgQHJlbmRlckZhdGFsRXJyb3IoKVxuICAgIGVsc2VcbiAgICAgIFByb21pc2UucmVzb2x2ZSgpXG5cbiAgcmVuZGVyRmF0YWxFcnJvcjogLT5cbiAgICByZXBvVXJsID0gQGlzc3VlLmdldFJlcG9VcmwoKVxuICAgIHBhY2thZ2VOYW1lID0gQGlzc3VlLmdldFBhY2thZ2VOYW1lKClcblxuICAgIGZhdGFsQ29udGFpbmVyID0gQGVsZW1lbnQucXVlcnlTZWxlY3RvcignLm1ldGEnKVxuICAgIGZhdGFsQ29udGFpbmVyLmFwcGVuZENoaWxkKFRlbXBsYXRlSGVscGVyLnJlbmRlcihAZmF0YWxUZW1wbGF0ZSkpXG4gICAgZmF0YWxOb3RpZmljYXRpb24gPSBAZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuZmF0YWwtbm90aWZpY2F0aW9uJylcblxuICAgIGlzc3VlQnV0dG9uID0gZmF0YWxDb250YWluZXIucXVlcnlTZWxlY3RvcignLmJ0bi1pc3N1ZScpXG5cbiAgICBjb3B5UmVwb3J0QnV0dG9uID0gZmF0YWxDb250YWluZXIucXVlcnlTZWxlY3RvcignLmJ0bi1jb3B5LXJlcG9ydCcpXG4gICAgYXRvbS50b29sdGlwcy5hZGQoY29weVJlcG9ydEJ1dHRvbiwgdGl0bGU6IGNvcHlSZXBvcnRCdXR0b24uZ2V0QXR0cmlidXRlKCd0aXRsZScpKVxuICAgIGNvcHlSZXBvcnRCdXR0b24uYWRkRXZlbnRMaXN0ZW5lciAnY2xpY2snLCAoZSkgPT5cbiAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgQGlzc3VlLmdldElzc3VlQm9keSgpLnRoZW4gKGlzc3VlQm9keSkgLT5cbiAgICAgICAgYXRvbS5jbGlwYm9hcmQud3JpdGUoaXNzdWVCb2R5KVxuXG4gICAgaWYgcGFja2FnZU5hbWU/IGFuZCByZXBvVXJsP1xuICAgICAgZmF0YWxOb3RpZmljYXRpb24uaW5uZXJIVE1MID0gXCJUaGUgZXJyb3Igd2FzIHRocm93biBmcm9tIHRoZSA8YSBocmVmPVxcXCIje3JlcG9Vcmx9XFxcIj4je3BhY2thZ2VOYW1lfSBwYWNrYWdlPC9hPi4gXCJcbiAgICBlbHNlIGlmIHBhY2thZ2VOYW1lP1xuICAgICAgaXNzdWVCdXR0b24ucmVtb3ZlKClcbiAgICAgIGZhdGFsTm90aWZpY2F0aW9uLnRleHRDb250ZW50ID0gXCJUaGUgZXJyb3Igd2FzIHRocm93biBmcm9tIHRoZSAje3BhY2thZ2VOYW1lfSBwYWNrYWdlLiBcIlxuICAgIGVsc2VcbiAgICAgIGZhdGFsTm90aWZpY2F0aW9uLnRleHRDb250ZW50ID0gXCJUaGlzIGlzIGxpa2VseSBhIGJ1ZyBpbiBBdG9tLiBcIlxuXG4gICAgIyBXZSBvbmx5IHNob3cgdGhlIGNyZWF0ZSBpc3N1ZSBidXR0b24gaWYgaXQncyBjbGVhcmx5IGluIGF0b20gY29yZSBvciBpbiBhIHBhY2thZ2Ugd2l0aCBhIHJlcG8gdXJsXG4gICAgaWYgaXNzdWVCdXR0b24ucGFyZW50Tm9kZT9cbiAgICAgIGlmIHBhY2thZ2VOYW1lPyBhbmQgcmVwb1VybD9cbiAgICAgICAgaXNzdWVCdXR0b24udGV4dENvbnRlbnQgPSBcIkNyZWF0ZSBpc3N1ZSBvbiB0aGUgI3twYWNrYWdlTmFtZX0gcGFja2FnZVwiXG4gICAgICBlbHNlXG4gICAgICAgIGlzc3VlQnV0dG9uLnRleHRDb250ZW50ID0gXCJDcmVhdGUgaXNzdWUgb24gYXRvbS9hdG9tXCJcblxuICAgICAgcHJvbWlzZXMgPSBbXVxuICAgICAgcHJvbWlzZXMucHVzaCBAaXNzdWUuZmluZFNpbWlsYXJJc3N1ZXMoKVxuICAgICAgcHJvbWlzZXMucHVzaCBVc2VyVXRpbGl0aWVzLmNoZWNrQXRvbVVwVG9EYXRlKClcbiAgICAgIHByb21pc2VzLnB1c2ggVXNlclV0aWxpdGllcy5jaGVja1BhY2thZ2VVcFRvRGF0ZShwYWNrYWdlTmFtZSkgaWYgcGFja2FnZU5hbWU/XG5cbiAgICAgIFByb21pc2UuYWxsKHByb21pc2VzKS50aGVuIChhbGxEYXRhKSA9PlxuICAgICAgICBbaXNzdWVzLCBhdG9tQ2hlY2ssIHBhY2thZ2VDaGVja10gPSBhbGxEYXRhXG5cbiAgICAgICAgaWYgaXNzdWVzPy5vcGVuIG9yIGlzc3Vlcz8uY2xvc2VkXG4gICAgICAgICAgaXNzdWUgPSBpc3N1ZXMub3BlbiBvciBpc3N1ZXMuY2xvc2VkXG4gICAgICAgICAgaXNzdWVCdXR0b24uc2V0QXR0cmlidXRlKCdocmVmJywgaXNzdWUuaHRtbF91cmwpXG4gICAgICAgICAgaXNzdWVCdXR0b24udGV4dENvbnRlbnQgPSBcIlZpZXcgSXNzdWVcIlxuICAgICAgICAgIGZhdGFsTm90aWZpY2F0aW9uLmlubmVySFRNTCArPSBcIiBUaGlzIGlzc3VlIGhhcyBhbHJlYWR5IGJlZW4gcmVwb3J0ZWQuXCJcbiAgICAgICAgZWxzZSBpZiBwYWNrYWdlQ2hlY2s/IGFuZCBub3QgcGFja2FnZUNoZWNrLnVwVG9EYXRlIGFuZCBub3QgcGFja2FnZUNoZWNrLmlzQ29yZVxuICAgICAgICAgIGlzc3VlQnV0dG9uLnNldEF0dHJpYnV0ZSgnaHJlZicsICcjJylcbiAgICAgICAgICBpc3N1ZUJ1dHRvbi50ZXh0Q29udGVudCA9IFwiQ2hlY2sgZm9yIHBhY2thZ2UgdXBkYXRlc1wiXG4gICAgICAgICAgaXNzdWVCdXR0b24uYWRkRXZlbnRMaXN0ZW5lciAnY2xpY2snLCAoZSkgLT5cbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgY29tbWFuZCA9ICdzZXR0aW5ncy12aWV3OmNoZWNrLWZvci1wYWNrYWdlLXVwZGF0ZXMnXG4gICAgICAgICAgICBhdG9tLmNvbW1hbmRzLmRpc3BhdGNoKGF0b20udmlld3MuZ2V0VmlldyhhdG9tLndvcmtzcGFjZSksIGNvbW1hbmQpXG5cbiAgICAgICAgICBmYXRhbE5vdGlmaWNhdGlvbi5pbm5lckhUTUwgKz0gXCJcIlwiXG4gICAgICAgICAgICA8Y29kZT4je3BhY2thZ2VOYW1lfTwvY29kZT4gaXMgb3V0IG9mIGRhdGU6ICN7cGFja2FnZUNoZWNrLmluc3RhbGxlZFZlcnNpb259IGluc3RhbGxlZDtcbiAgICAgICAgICAgICN7cGFja2FnZUNoZWNrLmxhdGVzdFZlcnNpb259IGxhdGVzdC5cbiAgICAgICAgICAgIFVwZ3JhZGluZyB0byB0aGUgbGF0ZXN0IHZlcnNpb24gbWF5IGZpeCB0aGlzIGlzc3VlLlxuICAgICAgICAgIFwiXCJcIlxuICAgICAgICBlbHNlIGlmIHBhY2thZ2VDaGVjaz8gYW5kIG5vdCBwYWNrYWdlQ2hlY2sudXBUb0RhdGUgYW5kIHBhY2thZ2VDaGVjay5pc0NvcmVcbiAgICAgICAgICBpc3N1ZUJ1dHRvbi5yZW1vdmUoKVxuXG4gICAgICAgICAgZmF0YWxOb3RpZmljYXRpb24uaW5uZXJIVE1MICs9IFwiXCJcIlxuICAgICAgICAgICAgPGJyPjxicj5cbiAgICAgICAgICAgIExvY2FsbHkgaW5zdGFsbGVkIGNvcmUgQXRvbSBwYWNrYWdlIDxjb2RlPiN7cGFja2FnZU5hbWV9PC9jb2RlPiBpcyBvdXQgb2YgZGF0ZTogI3twYWNrYWdlQ2hlY2suaW5zdGFsbGVkVmVyc2lvbn0gaW5zdGFsbGVkIGxvY2FsbHk7XG4gICAgICAgICAgICAje3BhY2thZ2VDaGVjay52ZXJzaW9uU2hpcHBlZFdpdGhBdG9tfSBpbmNsdWRlZCB3aXRoIHRoZSB2ZXJzaW9uIG9mIEF0b20geW91J3JlIHJ1bm5pbmcuXG4gICAgICAgICAgICBSZW1vdmluZyB0aGUgbG9jYWxseSBpbnN0YWxsZWQgdmVyc2lvbiBtYXkgZml4IHRoaXMgaXNzdWUuXG4gICAgICAgICAgXCJcIlwiXG5cbiAgICAgICAgICBwYWNrYWdlUGF0aCA9IGF0b20ucGFja2FnZXMuZ2V0TG9hZGVkUGFja2FnZShwYWNrYWdlTmFtZSk/LnBhdGhcbiAgICAgICAgICBpZiBmcy5pc1N5bWJvbGljTGlua1N5bmMocGFja2FnZVBhdGgpXG4gICAgICAgICAgICBmYXRhbE5vdGlmaWNhdGlvbi5pbm5lckhUTUwgKz0gXCJcIlwiXG4gICAgICAgICAgICA8YnI+PGJyPlxuICAgICAgICAgICAgVXNlOiA8Y29kZT5hcG0gdW5saW5rICN7cGFja2FnZVBhdGh9PC9jb2RlPlxuICAgICAgICAgIFwiXCJcIlxuICAgICAgICBlbHNlIGlmIGF0b21DaGVjaz8gYW5kIG5vdCBhdG9tQ2hlY2sudXBUb0RhdGVcbiAgICAgICAgICBpc3N1ZUJ1dHRvbi5yZW1vdmUoKVxuXG4gICAgICAgICAgZmF0YWxOb3RpZmljYXRpb24uaW5uZXJIVE1MICs9IFwiXCJcIlxuICAgICAgICAgICAgQXRvbSBpcyBvdXQgb2YgZGF0ZTogI3thdG9tQ2hlY2suaW5zdGFsbGVkVmVyc2lvbn0gaW5zdGFsbGVkO1xuICAgICAgICAgICAgI3thdG9tQ2hlY2subGF0ZXN0VmVyc2lvbn0gbGF0ZXN0LlxuICAgICAgICAgICAgVXBncmFkaW5nIHRvIHRoZSA8YSBocmVmPSdodHRwczovL2dpdGh1Yi5jb20vYXRvbS9hdG9tL3JlbGVhc2VzL3RhZy92I3thdG9tQ2hlY2subGF0ZXN0VmVyc2lvbn0nPmxhdGVzdCB2ZXJzaW9uPC9hPiBtYXkgZml4IHRoaXMgaXNzdWUuXG4gICAgICAgICAgXCJcIlwiXG4gICAgICAgIGVsc2VcbiAgICAgICAgICBmYXRhbE5vdGlmaWNhdGlvbi5pbm5lckhUTUwgKz0gXCIgWW91IGNhbiBoZWxwIGJ5IGNyZWF0aW5nIGFuIGlzc3VlLiBQbGVhc2UgZXhwbGFpbiB3aGF0IGFjdGlvbnMgdHJpZ2dlcmVkIHRoaXMgZXJyb3IuXCJcbiAgICAgICAgICBpc3N1ZUJ1dHRvbi5hZGRFdmVudExpc3RlbmVyICdjbGljaycsIChlKSA9PlxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICBpc3N1ZUJ1dHRvbi5jbGFzc0xpc3QuYWRkKCdvcGVuaW5nJylcbiAgICAgICAgICAgIEBpc3N1ZS5nZXRJc3N1ZVVybEZvclN5c3RlbSgpLnRoZW4gKGlzc3VlVXJsKSAtPlxuICAgICAgICAgICAgICBzaGVsbC5vcGVuRXh0ZXJuYWwoaXNzdWVVcmwpXG4gICAgICAgICAgICAgIGlzc3VlQnV0dG9uLmNsYXNzTGlzdC5yZW1vdmUoJ29wZW5pbmcnKVxuXG4gICAgICAgIHJldHVyblxuICAgIGVsc2VcbiAgICAgIFByb21pc2UucmVzb2x2ZSgpXG5cbiAgbWFrZURpc21pc3NhYmxlOiAtPlxuICAgIHVubGVzcyBAbW9kZWwuaXNEaXNtaXNzYWJsZSgpXG4gICAgICBjbGVhclRpbWVvdXQoQGF1dG9oaWRlVGltZW91dClcbiAgICAgIEBtb2RlbC5vcHRpb25zLmRpc21pc3NhYmxlID0gdHJ1ZVxuICAgICAgQG1vZGVsLmRpc21pc3NlZCA9IGZhbHNlXG4gICAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdoYXMtY2xvc2UnKVxuXG4gIHJlbW92ZU5vdGlmaWNhdGlvbjogLT5cbiAgICB1bmxlc3MgQGVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdyZW1vdmUnKVxuICAgICAgQGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgncmVtb3ZlJylcbiAgICAgIEByZW1vdmVOb3RpZmljYXRpb25BZnRlclRpbWVvdXQoKVxuXG4gIGhhbmRsZVJlbW92ZU5vdGlmaWNhdGlvbkNsaWNrOiAtPlxuICAgIEByZW1vdmVOb3RpZmljYXRpb24oKVxuICAgIEBtb2RlbC5kaXNtaXNzKClcblxuICBoYW5kbGVSZW1vdmVBbGxOb3RpZmljYXRpb25zQ2xpY2s6IC0+XG4gICAgbm90aWZpY2F0aW9ucyA9IGF0b20ubm90aWZpY2F0aW9ucy5nZXROb3RpZmljYXRpb25zKClcbiAgICBmb3Igbm90aWZpY2F0aW9uIGluIG5vdGlmaWNhdGlvbnNcbiAgICAgIGF0b20udmlld3MuZ2V0Vmlldyhub3RpZmljYXRpb24pLnJlbW92ZU5vdGlmaWNhdGlvbigpXG4gICAgICBpZiBub3RpZmljYXRpb24uaXNEaXNtaXNzYWJsZSgpIGFuZCBub3Qgbm90aWZpY2F0aW9uLmlzRGlzbWlzc2VkKClcbiAgICAgICAgbm90aWZpY2F0aW9uLmRpc21pc3MoKVxuICAgIHJldHVyblxuXG4gIGhhbmRsZVN0YWNrVHJhY2VUb2dnbGVDbGljazogKGUsIGNvbnRhaW5lcikgLT5cbiAgICBlLnByZXZlbnREZWZhdWx0PygpXG4gICAgaWYgY29udGFpbmVyLnN0eWxlLmRpc3BsYXkgaXMgJ25vbmUnXG4gICAgICBlLmN1cnJlbnRUYXJnZXQuaW5uZXJIVE1MID0gJzxzcGFuIGNsYXNzPVwiaWNvbiBpY29uLWRhc2hcIj48L3NwYW4+SGlkZSBTdGFjayBUcmFjZSdcbiAgICAgIGNvbnRhaW5lci5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJ1xuICAgIGVsc2VcbiAgICAgIGUuY3VycmVudFRhcmdldC5pbm5lckhUTUwgPSAnPHNwYW4gY2xhc3M9XCJpY29uIGljb24tcGx1c1wiPjwvc3Bhbj5TaG93IFN0YWNrIFRyYWNlJ1xuICAgICAgY29udGFpbmVyLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcblxuICBhdXRvaGlkZTogLT5cbiAgICBAYXV0b2hpZGVUaW1lb3V0ID0gc2V0VGltZW91dCA9PlxuICAgICAgQHJlbW92ZU5vdGlmaWNhdGlvbigpXG4gICAgLCBAdmlzaWJpbGl0eUR1cmF0aW9uXG5cbiAgcmVtb3ZlTm90aWZpY2F0aW9uQWZ0ZXJUaW1lb3V0OiAtPlxuICAgIGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVBhbmUoKS5hY3RpdmF0ZSgpIGlmIEBlbGVtZW50IGlzIGRvY3VtZW50LmFjdGl2ZUVsZW1lbnRcblxuICAgIHNldFRpbWVvdXQgPT5cbiAgICAgIEBlbGVtZW50LnJlbW92ZSgpXG4gICAgLCBAYW5pbWF0aW9uRHVyYXRpb24gIyBrZWVwIGluIHN5bmMgd2l0aCBDU1MgYW5pbWF0aW9uXG5cbiAgZ2V0QnV0dG9uQ2xhc3M6IC0+XG4gICAgdHlwZSA9IFwiYnRuLSN7QG1vZGVsLmdldFR5cGUoKX1cIlxuICAgIGlmIHR5cGUgaXMgJ2J0bi1mYXRhbCcgdGhlbiAnYnRuLWVycm9yJyBlbHNlIHR5cGVcblxuYWRkU3BsaXRMaW5lc1RvQ29udGFpbmVyID0gKGNvbnRhaW5lciwgY29udGVudCkgLT5cbiAgY29udGVudCA9IGNvbnRlbnQudG9TdHJpbmcoKSBpZiB0eXBlb2YgY29udGVudCBpc250ICdzdHJpbmcnXG4gIGZvciBsaW5lIGluIGNvbnRlbnQuc3BsaXQoJ1xcbicpXG4gICAgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICBkaXYuY2xhc3NMaXN0LmFkZCAnbGluZSdcbiAgICBkaXYudGV4dENvbnRlbnQgPSBsaW5lXG4gICAgY29udGFpbmVyLmFwcGVuZENoaWxkKGRpdilcbiAgcmV0dXJuXG4iXX0=
