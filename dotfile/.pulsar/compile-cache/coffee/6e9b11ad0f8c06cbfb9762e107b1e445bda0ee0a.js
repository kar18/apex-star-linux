(function() {
  var CompositeDisposable, WrapGuideElement;

  CompositeDisposable = require('atom').CompositeDisposable;

  module.exports = WrapGuideElement = (function() {
    function WrapGuideElement(editor, editorElement) {
      this.editor = editor;
      this.editorElement = editorElement;
      this.subscriptions = new CompositeDisposable();
      this.configSubscriptions = new CompositeDisposable();
      this.element = document.createElement('div');
      this.element.setAttribute('is', 'wrap-guide');
      this.element.classList.add('wrap-guide-container');
      this.attachToLines();
      this.handleEvents();
      this.updateGuide();
      this.element.updateGuide = this.updateGuide.bind(this);
      this.element.getDefaultColumn = this.getDefaultColumn.bind(this);
    }

    WrapGuideElement.prototype.attachToLines = function() {
      var scrollView;
      scrollView = this.editorElement.querySelector('.scroll-view');
      return scrollView != null ? scrollView.appendChild(this.element) : void 0;
    };

    WrapGuideElement.prototype.handleEvents = function() {
      var updateGuideCallback;
      updateGuideCallback = (function(_this) {
        return function() {
          return _this.updateGuide();
        };
      })(this);
      this.handleConfigEvents();
      this.subscriptions.add(atom.config.onDidChange('editor.fontSize', (function(_this) {
        return function() {
          return _this.editorElement.getComponent().getNextUpdatePromise().then(function() {
            return updateGuideCallback();
          });
        };
      })(this)));
      this.subscriptions.add(this.editorElement.onDidChangeScrollLeft(updateGuideCallback));
      this.subscriptions.add(this.editor.onDidChangePath(updateGuideCallback));
      this.subscriptions.add(this.editor.onDidChangeGrammar((function(_this) {
        return function() {
          _this.configSubscriptions.dispose();
          _this.handleConfigEvents();
          return updateGuideCallback();
        };
      })(this)));
      this.subscriptions.add(this.editor.onDidDestroy((function(_this) {
        return function() {
          _this.subscriptions.dispose();
          return _this.configSubscriptions.dispose();
        };
      })(this)));
      return this.subscriptions.add(this.editorElement.onDidAttach((function(_this) {
        return function() {
          _this.attachToLines();
          return updateGuideCallback();
        };
      })(this)));
    };

    WrapGuideElement.prototype.handleConfigEvents = function() {
      var uniqueAscending, updateGuideCallback, updateGuidesCallback, updatePreferredLineLengthCallback;
      uniqueAscending = require('./main').uniqueAscending;
      updatePreferredLineLengthCallback = (function(_this) {
        return function(args) {
          var columns, i;
          columns = atom.config.get('wrap-guide.columns', {
            scope: _this.editor.getRootScopeDescriptor()
          });
          if (columns.length > 0) {
            columns[columns.length - 1] = args.newValue;
            columns = uniqueAscending((function() {
              var j, len, results;
              results = [];
              for (j = 0, len = columns.length; j < len; j++) {
                i = columns[j];
                if (i <= args.newValue) {
                  results.push(i);
                }
              }
              return results;
            })());
            atom.config.set('wrap-guide.columns', columns, {
              scopeSelector: "." + (_this.editor.getGrammar().scopeName)
            });
          }
          return _this.updateGuide();
        };
      })(this);
      this.configSubscriptions.add(atom.config.onDidChange('editor.preferredLineLength', {
        scope: this.editor.getRootScopeDescriptor()
      }, updatePreferredLineLengthCallback));
      updateGuideCallback = (function(_this) {
        return function() {
          return _this.updateGuide();
        };
      })(this);
      this.configSubscriptions.add(atom.config.onDidChange('wrap-guide.enabled', {
        scope: this.editor.getRootScopeDescriptor()
      }, updateGuideCallback));
      updateGuidesCallback = (function(_this) {
        return function(args) {
          var columns;
          columns = uniqueAscending(args.newValue);
          if (columns != null ? columns.length : void 0) {
            atom.config.set('wrap-guide.columns', columns);
            atom.config.set('editor.preferredLineLength', columns[columns.length - 1], {
              scopeSelector: "." + (_this.editor.getGrammar().scopeName)
            });
            return _this.updateGuide();
          }
        };
      })(this);
      return this.configSubscriptions.add(atom.config.onDidChange('wrap-guide.columns', {
        scope: this.editor.getRootScopeDescriptor()
      }, updateGuidesCallback));
    };

    WrapGuideElement.prototype.getDefaultColumn = function() {
      return atom.config.get('editor.preferredLineLength', {
        scope: this.editor.getRootScopeDescriptor()
      });
    };

    WrapGuideElement.prototype.getGuidesColumns = function(path, scopeName) {
      var columns, ref;
      columns = (ref = atom.config.get('wrap-guide.columns', {
        scope: this.editor.getRootScopeDescriptor()
      })) != null ? ref : [];
      if (columns.length > 0) {
        return columns;
      }
      return [this.getDefaultColumn()];
    };

    WrapGuideElement.prototype.isEnabled = function() {
      var ref;
      return (ref = atom.config.get('wrap-guide.enabled', {
        scope: this.editor.getRootScopeDescriptor()
      })) != null ? ref : true;
    };

    WrapGuideElement.prototype.hide = function() {
      return this.element.style.display = 'none';
    };

    WrapGuideElement.prototype.show = function() {
      return this.element.style.display = 'block';
    };

    WrapGuideElement.prototype.updateGuide = function() {
      if (this.isEnabled()) {
        return this.updateGuides();
      } else {
        return this.hide();
      }
    };

    WrapGuideElement.prototype.updateGuides = function() {
      this.removeGuides();
      this.appendGuides();
      if (this.element.children.length) {
        return this.show();
      } else {
        return this.hide();
      }
    };

    WrapGuideElement.prototype.destroy = function() {
      this.element.remove();
      this.subscriptions.dispose();
      return this.configSubscriptions.dispose();
    };

    WrapGuideElement.prototype.removeGuides = function() {
      var results;
      results = [];
      while (this.element.firstChild) {
        results.push(this.element.removeChild(this.element.firstChild));
      }
      return results;
    };

    WrapGuideElement.prototype.appendGuides = function() {
      var column, columns, j, len, results;
      columns = this.getGuidesColumns(this.editor.getPath(), this.editor.getGrammar().scopeName);
      results = [];
      for (j = 0, len = columns.length; j < len; j++) {
        column = columns[j];
        if (!(column < 0)) {
          results.push(this.appendGuide(column));
        } else {
          results.push(void 0);
        }
      }
      return results;
    };

    WrapGuideElement.prototype.appendGuide = function(column) {
      var columnWidth, guide;
      columnWidth = this.editorElement.getDefaultCharacterWidth() * column;
      columnWidth -= this.editorElement.getScrollLeft();
      guide = document.createElement('div');
      guide.classList.add('wrap-guide');
      guide.style.left = (Math.round(columnWidth)) + "px";
      return this.element.appendChild(guide);
    };

    return WrapGuideElement;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy93cmFwLWd1aWRlL2xpYi93cmFwLWd1aWRlLWVsZW1lbnQuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQyxzQkFBdUIsT0FBQSxDQUFRLE1BQVI7O0VBRXhCLE1BQU0sQ0FBQyxPQUFQLEdBQ007SUFDUywwQkFBQyxNQUFELEVBQVUsYUFBVjtNQUFDLElBQUMsQ0FBQSxTQUFEO01BQVMsSUFBQyxDQUFBLGdCQUFEO01BQ3JCLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUksbUJBQUosQ0FBQTtNQUNqQixJQUFDLENBQUEsbUJBQUQsR0FBdUIsSUFBSSxtQkFBSixDQUFBO01BQ3ZCLElBQUMsQ0FBQSxPQUFELEdBQVcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDWCxJQUFDLENBQUEsT0FBTyxDQUFDLFlBQVQsQ0FBc0IsSUFBdEIsRUFBNEIsWUFBNUI7TUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixzQkFBdkI7TUFDQSxJQUFDLENBQUEsYUFBRCxDQUFBO01BQ0EsSUFBQyxDQUFBLFlBQUQsQ0FBQTtNQUNBLElBQUMsQ0FBQSxXQUFELENBQUE7TUFFQSxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsR0FBdUIsSUFBQyxDQUFBLFdBQVcsQ0FBQyxJQUFiLENBQWtCLElBQWxCO01BQ3ZCLElBQUMsQ0FBQSxPQUFPLENBQUMsZ0JBQVQsR0FBNEIsSUFBQyxDQUFBLGdCQUFnQixDQUFDLElBQWxCLENBQXVCLElBQXZCO0lBWGpCOzsrQkFhYixhQUFBLEdBQWUsU0FBQTtBQUNiLFVBQUE7TUFBQSxVQUFBLEdBQWEsSUFBQyxDQUFBLGFBQWEsQ0FBQyxhQUFmLENBQTZCLGNBQTdCO2tDQUNiLFVBQVUsQ0FBRSxXQUFaLENBQXdCLElBQUMsQ0FBQSxPQUF6QjtJQUZhOzsrQkFJZixZQUFBLEdBQWMsU0FBQTtBQUNaLFVBQUE7TUFBQSxtQkFBQSxHQUFzQixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLFdBQUQsQ0FBQTtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQTtNQUV0QixJQUFDLENBQUEsa0JBQUQsQ0FBQTtNQUVBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVosQ0FBd0IsaUJBQXhCLEVBQTJDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFHNUQsS0FBQyxDQUFBLGFBQWEsQ0FBQyxZQUFmLENBQUEsQ0FBNkIsQ0FBQyxvQkFBOUIsQ0FBQSxDQUFvRCxDQUFDLElBQXJELENBQTBELFNBQUE7bUJBQUcsbUJBQUEsQ0FBQTtVQUFILENBQTFEO1FBSDREO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzQyxDQUFuQjtNQUtBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsYUFBYSxDQUFDLHFCQUFmLENBQXFDLG1CQUFyQyxDQUFuQjtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsTUFBTSxDQUFDLGVBQVIsQ0FBd0IsbUJBQXhCLENBQW5CO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUMsQ0FBQSxNQUFNLENBQUMsa0JBQVIsQ0FBMkIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO1VBQzVDLEtBQUMsQ0FBQSxtQkFBbUIsQ0FBQyxPQUFyQixDQUFBO1VBQ0EsS0FBQyxDQUFBLGtCQUFELENBQUE7aUJBQ0EsbUJBQUEsQ0FBQTtRQUg0QztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBM0IsQ0FBbkI7TUFLQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBQyxDQUFBLE1BQU0sQ0FBQyxZQUFSLENBQXFCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtVQUN0QyxLQUFDLENBQUEsYUFBYSxDQUFDLE9BQWYsQ0FBQTtpQkFDQSxLQUFDLENBQUEsbUJBQW1CLENBQUMsT0FBckIsQ0FBQTtRQUZzQztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBckIsQ0FBbkI7YUFJQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxXQUFmLENBQTJCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtVQUM1QyxLQUFDLENBQUEsYUFBRCxDQUFBO2lCQUNBLG1CQUFBLENBQUE7UUFGNEM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTNCLENBQW5CO0lBckJZOzsrQkF5QmQsa0JBQUEsR0FBb0IsU0FBQTtBQUNsQixVQUFBO01BQUMsa0JBQW1CLE9BQUEsQ0FBUSxRQUFSO01BRXBCLGlDQUFBLEdBQW9DLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxJQUFEO0FBRWxDLGNBQUE7VUFBQSxPQUFBLEdBQVUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLG9CQUFoQixFQUFzQztZQUFBLEtBQUEsRUFBTyxLQUFDLENBQUEsTUFBTSxDQUFDLHNCQUFSLENBQUEsQ0FBUDtXQUF0QztVQUNWLElBQUcsT0FBTyxDQUFDLE1BQVIsR0FBaUIsQ0FBcEI7WUFDRSxPQUFRLENBQUEsT0FBTyxDQUFDLE1BQVIsR0FBaUIsQ0FBakIsQ0FBUixHQUE4QixJQUFJLENBQUM7WUFDbkMsT0FBQSxHQUFVLGVBQUE7O0FBQWdCO21CQUFBLHlDQUFBOztvQkFBd0IsQ0FBQSxJQUFLLElBQUksQ0FBQzsrQkFBbEM7O0FBQUE7O2dCQUFoQjtZQUNWLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQixvQkFBaEIsRUFBc0MsT0FBdEMsRUFDRTtjQUFBLGFBQUEsRUFBZSxHQUFBLEdBQUcsQ0FBQyxLQUFDLENBQUEsTUFBTSxDQUFDLFVBQVIsQ0FBQSxDQUFvQixDQUFDLFNBQXRCLENBQWxCO2FBREYsRUFIRjs7aUJBS0EsS0FBQyxDQUFBLFdBQUQsQ0FBQTtRQVJrQztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFTcEMsSUFBQyxDQUFBLG1CQUFtQixDQUFDLEdBQXJCLENBQXlCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBWixDQUN2Qiw0QkFEdUIsRUFFdkI7UUFBQSxLQUFBLEVBQU8sSUFBQyxDQUFBLE1BQU0sQ0FBQyxzQkFBUixDQUFBLENBQVA7T0FGdUIsRUFHdkIsaUNBSHVCLENBQXpCO01BTUEsbUJBQUEsR0FBc0IsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxXQUFELENBQUE7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUE7TUFDdEIsSUFBQyxDQUFBLG1CQUFtQixDQUFDLEdBQXJCLENBQXlCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBWixDQUN2QixvQkFEdUIsRUFFdkI7UUFBQSxLQUFBLEVBQU8sSUFBQyxDQUFBLE1BQU0sQ0FBQyxzQkFBUixDQUFBLENBQVA7T0FGdUIsRUFHdkIsbUJBSHVCLENBQXpCO01BTUEsb0JBQUEsR0FBdUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLElBQUQ7QUFFckIsY0FBQTtVQUFBLE9BQUEsR0FBVSxlQUFBLENBQWdCLElBQUksQ0FBQyxRQUFyQjtVQUNWLHNCQUFHLE9BQU8sQ0FBRSxlQUFaO1lBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLG9CQUFoQixFQUFzQyxPQUF0QztZQUNBLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQiw0QkFBaEIsRUFBOEMsT0FBUSxDQUFBLE9BQU8sQ0FBQyxNQUFSLEdBQWlCLENBQWpCLENBQXRELEVBQ0U7Y0FBQSxhQUFBLEVBQWUsR0FBQSxHQUFHLENBQUMsS0FBQyxDQUFBLE1BQU0sQ0FBQyxVQUFSLENBQUEsQ0FBb0IsQ0FBQyxTQUF0QixDQUFsQjthQURGO21CQUVBLEtBQUMsQ0FBQSxXQUFELENBQUEsRUFKRjs7UUFIcUI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBO2FBUXZCLElBQUMsQ0FBQSxtQkFBbUIsQ0FBQyxHQUFyQixDQUF5QixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVosQ0FDdkIsb0JBRHVCLEVBRXZCO1FBQUEsS0FBQSxFQUFPLElBQUMsQ0FBQSxNQUFNLENBQUMsc0JBQVIsQ0FBQSxDQUFQO09BRnVCLEVBR3ZCLG9CQUh1QixDQUF6QjtJQWpDa0I7OytCQXVDcEIsZ0JBQUEsR0FBa0IsU0FBQTthQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsNEJBQWhCLEVBQThDO1FBQUEsS0FBQSxFQUFPLElBQUMsQ0FBQSxNQUFNLENBQUMsc0JBQVIsQ0FBQSxDQUFQO09BQTlDO0lBRGdCOzsrQkFHbEIsZ0JBQUEsR0FBa0IsU0FBQyxJQUFELEVBQU8sU0FBUDtBQUNoQixVQUFBO01BQUEsT0FBQTs7MEJBQTJGO01BQzNGLElBQWtCLE9BQU8sQ0FBQyxNQUFSLEdBQWlCLENBQW5DO0FBQUEsZUFBTyxRQUFQOztBQUNBLGFBQU8sQ0FBQyxJQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFEO0lBSFM7OytCQUtsQixTQUFBLEdBQVcsU0FBQTtBQUNULFVBQUE7OzswQkFBaUY7SUFEeEU7OytCQUdYLElBQUEsR0FBTSxTQUFBO2FBQ0osSUFBQyxDQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBZixHQUF5QjtJQURyQjs7K0JBR04sSUFBQSxHQUFNLFNBQUE7YUFDSixJQUFDLENBQUEsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFmLEdBQXlCO0lBRHJCOzsrQkFHTixXQUFBLEdBQWEsU0FBQTtNQUNYLElBQUcsSUFBQyxDQUFBLFNBQUQsQ0FBQSxDQUFIO2VBQ0UsSUFBQyxDQUFBLFlBQUQsQ0FBQSxFQURGO09BQUEsTUFBQTtlQUdFLElBQUMsQ0FBQSxJQUFELENBQUEsRUFIRjs7SUFEVzs7K0JBTWIsWUFBQSxHQUFjLFNBQUE7TUFDWixJQUFDLENBQUEsWUFBRCxDQUFBO01BQ0EsSUFBQyxDQUFBLFlBQUQsQ0FBQTtNQUNBLElBQUcsSUFBQyxDQUFBLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBckI7ZUFDRSxJQUFDLENBQUEsSUFBRCxDQUFBLEVBREY7T0FBQSxNQUFBO2VBR0UsSUFBQyxDQUFBLElBQUQsQ0FBQSxFQUhGOztJQUhZOzsrQkFRZCxPQUFBLEdBQVMsU0FBQTtNQUNQLElBQUMsQ0FBQSxPQUFPLENBQUMsTUFBVCxDQUFBO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxPQUFmLENBQUE7YUFDQSxJQUFDLENBQUEsbUJBQW1CLENBQUMsT0FBckIsQ0FBQTtJQUhPOzsrQkFLVCxZQUFBLEdBQWMsU0FBQTtBQUNaLFVBQUE7QUFBQTthQUFNLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBZjtxQkFDRSxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsQ0FBcUIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxVQUE5QjtNQURGLENBQUE7O0lBRFk7OytCQUlkLFlBQUEsR0FBYyxTQUFBO0FBQ1osVUFBQTtNQUFBLE9BQUEsR0FBVSxJQUFDLENBQUEsZ0JBQUQsQ0FBa0IsSUFBQyxDQUFBLE1BQU0sQ0FBQyxPQUFSLENBQUEsQ0FBbEIsRUFBcUMsSUFBQyxDQUFBLE1BQU0sQ0FBQyxVQUFSLENBQUEsQ0FBb0IsQ0FBQyxTQUExRDtBQUNWO1dBQUEseUNBQUE7O1FBQ0UsSUFBQSxDQUFBLENBQTRCLE1BQUEsR0FBUyxDQUFyQyxDQUFBO3VCQUFBLElBQUMsQ0FBQSxXQUFELENBQWEsTUFBYixHQUFBO1NBQUEsTUFBQTsrQkFBQTs7QUFERjs7SUFGWTs7K0JBS2QsV0FBQSxHQUFhLFNBQUMsTUFBRDtBQUNYLFVBQUE7TUFBQSxXQUFBLEdBQWMsSUFBQyxDQUFBLGFBQWEsQ0FBQyx3QkFBZixDQUFBLENBQUEsR0FBNEM7TUFDMUQsV0FBQSxJQUFlLElBQUMsQ0FBQSxhQUFhLENBQUMsYUFBZixDQUFBO01BQ2YsS0FBQSxHQUFRLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO01BQ1IsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFoQixDQUFvQixZQUFwQjtNQUNBLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBWixHQUFxQixDQUFDLElBQUksQ0FBQyxLQUFMLENBQVcsV0FBWCxDQUFELENBQUEsR0FBeUI7YUFDOUMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLEtBQXJCO0lBTlc7Ozs7O0FBbElmIiwic291cmNlc0NvbnRlbnQiOlsie0NvbXBvc2l0ZURpc3Bvc2FibGV9ID0gcmVxdWlyZSAnYXRvbSdcblxubW9kdWxlLmV4cG9ydHMgPVxuY2xhc3MgV3JhcEd1aWRlRWxlbWVudFxuICBjb25zdHJ1Y3RvcjogKEBlZGl0b3IsIEBlZGl0b3JFbGVtZW50KSAtPlxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGUoKVxuICAgIEBjb25maWdTdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGUoKVxuICAgIEBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICBAZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2lzJywgJ3dyYXAtZ3VpZGUnKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3dyYXAtZ3VpZGUtY29udGFpbmVyJylcbiAgICBAYXR0YWNoVG9MaW5lcygpXG4gICAgQGhhbmRsZUV2ZW50cygpXG4gICAgQHVwZGF0ZUd1aWRlKClcblxuICAgIEBlbGVtZW50LnVwZGF0ZUd1aWRlID0gQHVwZGF0ZUd1aWRlLmJpbmQodGhpcylcbiAgICBAZWxlbWVudC5nZXREZWZhdWx0Q29sdW1uID0gQGdldERlZmF1bHRDb2x1bW4uYmluZCh0aGlzKVxuXG4gIGF0dGFjaFRvTGluZXM6IC0+XG4gICAgc2Nyb2xsVmlldyA9IEBlZGl0b3JFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zY3JvbGwtdmlldycpXG4gICAgc2Nyb2xsVmlldz8uYXBwZW5kQ2hpbGQoQGVsZW1lbnQpXG5cbiAgaGFuZGxlRXZlbnRzOiAtPlxuICAgIHVwZGF0ZUd1aWRlQ2FsbGJhY2sgPSA9PiBAdXBkYXRlR3VpZGUoKVxuXG4gICAgQGhhbmRsZUNvbmZpZ0V2ZW50cygpXG5cbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb25maWcub25EaWRDaGFuZ2UgJ2VkaXRvci5mb250U2l6ZScsID0+XG4gICAgICAjIFdhaXQgZm9yIGVkaXRvciB0byBmaW5pc2ggdXBkYXRpbmcgYmVmb3JlIHVwZGF0aW5nIHdyYXAgZ3VpZGVcbiAgICAgICMgVE9ETzogVXNlIGFzeW5jL2F3YWl0IG9uY2UgdGhpcyBmaWxlIGlzIGNvbnZlcnRlZCB0byBKU1xuICAgICAgQGVkaXRvckVsZW1lbnQuZ2V0Q29tcG9uZW50KCkuZ2V0TmV4dFVwZGF0ZVByb21pc2UoKS50aGVuIC0+IHVwZGF0ZUd1aWRlQ2FsbGJhY2soKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIEBlZGl0b3JFbGVtZW50Lm9uRGlkQ2hhbmdlU2Nyb2xsTGVmdCh1cGRhdGVHdWlkZUNhbGxiYWNrKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBAZWRpdG9yLm9uRGlkQ2hhbmdlUGF0aCh1cGRhdGVHdWlkZUNhbGxiYWNrKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBAZWRpdG9yLm9uRGlkQ2hhbmdlR3JhbW1hciA9PlxuICAgICAgQGNvbmZpZ1N1YnNjcmlwdGlvbnMuZGlzcG9zZSgpXG4gICAgICBAaGFuZGxlQ29uZmlnRXZlbnRzKClcbiAgICAgIHVwZGF0ZUd1aWRlQ2FsbGJhY2soKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIEBlZGl0b3Iub25EaWREZXN0cm95ID0+XG4gICAgICBAc3Vic2NyaXB0aW9ucy5kaXNwb3NlKClcbiAgICAgIEBjb25maWdTdWJzY3JpcHRpb25zLmRpc3Bvc2UoKVxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIEBlZGl0b3JFbGVtZW50Lm9uRGlkQXR0YWNoID0+XG4gICAgICBAYXR0YWNoVG9MaW5lcygpXG4gICAgICB1cGRhdGVHdWlkZUNhbGxiYWNrKClcblxuICBoYW5kbGVDb25maWdFdmVudHM6IC0+XG4gICAge3VuaXF1ZUFzY2VuZGluZ30gPSByZXF1aXJlICcuL21haW4nXG5cbiAgICB1cGRhdGVQcmVmZXJyZWRMaW5lTGVuZ3RoQ2FsbGJhY2sgPSAoYXJncykgPT5cbiAgICAgICMgZW5zdXJlIHRoYXQgdGhlIHJpZ2h0LW1vc3Qgd3JhcCBndWlkZSBpcyB0aGUgcHJlZmVycmVkTGluZUxlbmd0aFxuICAgICAgY29sdW1ucyA9IGF0b20uY29uZmlnLmdldCgnd3JhcC1ndWlkZS5jb2x1bW5zJywgc2NvcGU6IEBlZGl0b3IuZ2V0Um9vdFNjb3BlRGVzY3JpcHRvcigpKVxuICAgICAgaWYgY29sdW1ucy5sZW5ndGggPiAwXG4gICAgICAgIGNvbHVtbnNbY29sdW1ucy5sZW5ndGggLSAxXSA9IGFyZ3MubmV3VmFsdWVcbiAgICAgICAgY29sdW1ucyA9IHVuaXF1ZUFzY2VuZGluZyhpIGZvciBpIGluIGNvbHVtbnMgd2hlbiBpIDw9IGFyZ3MubmV3VmFsdWUpXG4gICAgICAgIGF0b20uY29uZmlnLnNldCAnd3JhcC1ndWlkZS5jb2x1bW5zJywgY29sdW1ucyxcbiAgICAgICAgICBzY29wZVNlbGVjdG9yOiBcIi4je0BlZGl0b3IuZ2V0R3JhbW1hcigpLnNjb3BlTmFtZX1cIlxuICAgICAgQHVwZGF0ZUd1aWRlKClcbiAgICBAY29uZmlnU3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb25maWcub25EaWRDaGFuZ2UoXG4gICAgICAnZWRpdG9yLnByZWZlcnJlZExpbmVMZW5ndGgnLFxuICAgICAgc2NvcGU6IEBlZGl0b3IuZ2V0Um9vdFNjb3BlRGVzY3JpcHRvcigpLFxuICAgICAgdXBkYXRlUHJlZmVycmVkTGluZUxlbmd0aENhbGxiYWNrXG4gICAgKVxuXG4gICAgdXBkYXRlR3VpZGVDYWxsYmFjayA9ID0+IEB1cGRhdGVHdWlkZSgpXG4gICAgQGNvbmZpZ1N1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29uZmlnLm9uRGlkQ2hhbmdlKFxuICAgICAgJ3dyYXAtZ3VpZGUuZW5hYmxlZCcsXG4gICAgICBzY29wZTogQGVkaXRvci5nZXRSb290U2NvcGVEZXNjcmlwdG9yKCksXG4gICAgICB1cGRhdGVHdWlkZUNhbGxiYWNrXG4gICAgKVxuXG4gICAgdXBkYXRlR3VpZGVzQ2FsbGJhY2sgPSAoYXJncykgPT5cbiAgICAgICMgZW5zdXJlIHRoYXQgbXVsdGlwbGUgZ3VpZGVzIHN0YXkgc29ydGVkIGluIGFzY2VuZGluZyBvcmRlclxuICAgICAgY29sdW1ucyA9IHVuaXF1ZUFzY2VuZGluZyhhcmdzLm5ld1ZhbHVlKVxuICAgICAgaWYgY29sdW1ucz8ubGVuZ3RoXG4gICAgICAgIGF0b20uY29uZmlnLnNldCgnd3JhcC1ndWlkZS5jb2x1bW5zJywgY29sdW1ucylcbiAgICAgICAgYXRvbS5jb25maWcuc2V0ICdlZGl0b3IucHJlZmVycmVkTGluZUxlbmd0aCcsIGNvbHVtbnNbY29sdW1ucy5sZW5ndGggLSAxXSxcbiAgICAgICAgICBzY29wZVNlbGVjdG9yOiBcIi4je0BlZGl0b3IuZ2V0R3JhbW1hcigpLnNjb3BlTmFtZX1cIlxuICAgICAgICBAdXBkYXRlR3VpZGUoKVxuICAgIEBjb25maWdTdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbmZpZy5vbkRpZENoYW5nZShcbiAgICAgICd3cmFwLWd1aWRlLmNvbHVtbnMnLFxuICAgICAgc2NvcGU6IEBlZGl0b3IuZ2V0Um9vdFNjb3BlRGVzY3JpcHRvcigpLFxuICAgICAgdXBkYXRlR3VpZGVzQ2FsbGJhY2tcbiAgICApXG5cbiAgZ2V0RGVmYXVsdENvbHVtbjogLT5cbiAgICBhdG9tLmNvbmZpZy5nZXQoJ2VkaXRvci5wcmVmZXJyZWRMaW5lTGVuZ3RoJywgc2NvcGU6IEBlZGl0b3IuZ2V0Um9vdFNjb3BlRGVzY3JpcHRvcigpKVxuXG4gIGdldEd1aWRlc0NvbHVtbnM6IChwYXRoLCBzY29wZU5hbWUpIC0+XG4gICAgY29sdW1ucyA9IGF0b20uY29uZmlnLmdldCgnd3JhcC1ndWlkZS5jb2x1bW5zJywgc2NvcGU6IEBlZGl0b3IuZ2V0Um9vdFNjb3BlRGVzY3JpcHRvcigpKSA/IFtdXG4gICAgcmV0dXJuIGNvbHVtbnMgaWYgY29sdW1ucy5sZW5ndGggPiAwXG4gICAgcmV0dXJuIFtAZ2V0RGVmYXVsdENvbHVtbigpXVxuXG4gIGlzRW5hYmxlZDogLT5cbiAgICBhdG9tLmNvbmZpZy5nZXQoJ3dyYXAtZ3VpZGUuZW5hYmxlZCcsIHNjb3BlOiBAZWRpdG9yLmdldFJvb3RTY29wZURlc2NyaXB0b3IoKSkgPyB0cnVlXG5cbiAgaGlkZTogLT5cbiAgICBAZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG5cbiAgc2hvdzogLT5cbiAgICBAZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJ1xuXG4gIHVwZGF0ZUd1aWRlOiAtPlxuICAgIGlmIEBpc0VuYWJsZWQoKVxuICAgICAgQHVwZGF0ZUd1aWRlcygpXG4gICAgZWxzZVxuICAgICAgQGhpZGUoKVxuXG4gIHVwZGF0ZUd1aWRlczogLT5cbiAgICBAcmVtb3ZlR3VpZGVzKClcbiAgICBAYXBwZW5kR3VpZGVzKClcbiAgICBpZiBAZWxlbWVudC5jaGlsZHJlbi5sZW5ndGhcbiAgICAgIEBzaG93KClcbiAgICBlbHNlXG4gICAgICBAaGlkZSgpXG5cbiAgZGVzdHJveTogLT5cbiAgICBAZWxlbWVudC5yZW1vdmUoKVxuICAgIEBzdWJzY3JpcHRpb25zLmRpc3Bvc2UoKVxuICAgIEBjb25maWdTdWJzY3JpcHRpb25zLmRpc3Bvc2UoKVxuXG4gIHJlbW92ZUd1aWRlczogLT5cbiAgICB3aGlsZSBAZWxlbWVudC5maXJzdENoaWxkXG4gICAgICBAZWxlbWVudC5yZW1vdmVDaGlsZChAZWxlbWVudC5maXJzdENoaWxkKVxuXG4gIGFwcGVuZEd1aWRlczogLT5cbiAgICBjb2x1bW5zID0gQGdldEd1aWRlc0NvbHVtbnMoQGVkaXRvci5nZXRQYXRoKCksIEBlZGl0b3IuZ2V0R3JhbW1hcigpLnNjb3BlTmFtZSlcbiAgICBmb3IgY29sdW1uIGluIGNvbHVtbnNcbiAgICAgIEBhcHBlbmRHdWlkZShjb2x1bW4pIHVubGVzcyBjb2x1bW4gPCAwXG5cbiAgYXBwZW5kR3VpZGU6IChjb2x1bW4pIC0+XG4gICAgY29sdW1uV2lkdGggPSBAZWRpdG9yRWxlbWVudC5nZXREZWZhdWx0Q2hhcmFjdGVyV2lkdGgoKSAqIGNvbHVtblxuICAgIGNvbHVtbldpZHRoIC09IEBlZGl0b3JFbGVtZW50LmdldFNjcm9sbExlZnQoKVxuICAgIGd1aWRlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICBndWlkZS5jbGFzc0xpc3QuYWRkKCd3cmFwLWd1aWRlJylcbiAgICBndWlkZS5zdHlsZS5sZWZ0ID0gXCIje01hdGgucm91bmQoY29sdW1uV2lkdGgpfXB4XCJcbiAgICBAZWxlbWVudC5hcHBlbmRDaGlsZChndWlkZSlcbiJdfQ==
