(function() {
  var CharacterPattern, _;

  _ = require('underscore-plus');

  CharacterPattern = /[^\s]/;

  module.exports = {
    activate: function() {
      return this.commandDisposable = atom.commands.add('atom-text-editor', {
        'autoflow:reflow-selection': (function(_this) {
          return function(event) {
            return _this.reflowSelection(event.currentTarget.getModel());
          };
        })(this)
      });
    },
    deactivate: function() {
      var ref;
      if ((ref = this.commandDisposable) != null) {
        ref.dispose();
      }
      return this.commandDisposable = null;
    },
    reflowSelection: function(editor) {
      var range, reflowOptions, reflowedText;
      range = editor.getSelectedBufferRange();
      if (range.isEmpty()) {
        range = editor.getCurrentParagraphBufferRange();
      }
      if (range == null) {
        return;
      }
      reflowOptions = {
        wrapColumn: this.getPreferredLineLength(editor),
        tabLength: this.getTabLength(editor)
      };
      reflowedText = this.reflow(editor.getTextInRange(range), reflowOptions);
      return editor.getBuffer().setTextInRange(range, reflowedText);
    },
    reflow: function(text, arg) {
      var beginningLinesToIgnore, block, blockLines, currentLine, currentLineLength, endingLinesToIgnore, escapedLinePrefix, firstLine, i, j, latexTagEndRegex, latexTagRegex, latexTagStartRegex, leadingVerticalSpace, len, len1, linePrefix, linePrefixTabExpanded, lines, paragraphBlocks, paragraphs, ref, segment, tabLength, tabLengthInSpaces, trailingVerticalSpace, wrapColumn, wrappedLinePrefix, wrappedLines;
      wrapColumn = arg.wrapColumn, tabLength = arg.tabLength;
      paragraphs = [];
      text = text.replace(/\r\n?/g, '\n');
      leadingVerticalSpace = text.match(/^\s*\n/);
      if (leadingVerticalSpace) {
        text = text.substr(leadingVerticalSpace.length);
      } else {
        leadingVerticalSpace = '';
      }
      trailingVerticalSpace = text.match(/\n\s*$/);
      if (trailingVerticalSpace) {
        text = text.substr(0, text.length - trailingVerticalSpace.length);
      } else {
        trailingVerticalSpace = '';
      }
      paragraphBlocks = text.split(/\n\s*\n/g);
      if (tabLength) {
        tabLengthInSpaces = Array(tabLength + 1).join(' ');
      } else {
        tabLengthInSpaces = '';
      }
      for (i = 0, len = paragraphBlocks.length; i < len; i++) {
        block = paragraphBlocks[i];
        blockLines = block.split('\n');
        beginningLinesToIgnore = [];
        endingLinesToIgnore = [];
        latexTagRegex = /^\s*\\\w+(\[.*\])?\{\w+\}(\[.*\])?\s*$/g;
        latexTagStartRegex = /^\s*\\\w+\s*\{\s*$/g;
        latexTagEndRegex = /^\s*\}\s*$/g;
        while (blockLines.length > 0 && (blockLines[0].match(latexTagRegex) || blockLines[0].match(latexTagStartRegex))) {
          beginningLinesToIgnore.push(blockLines[0]);
          blockLines.shift();
        }
        while (blockLines.length > 0 && (blockLines[blockLines.length - 1].match(latexTagRegex) || blockLines[blockLines.length - 1].match(latexTagEndRegex))) {
          endingLinesToIgnore.unshift(blockLines[blockLines.length - 1]);
          blockLines.pop();
        }
        if (!(blockLines.length > 0)) {
          paragraphs.push(block);
          continue;
        }
        linePrefix = blockLines[0].match(/^\s*(\/\/|\/\*|;;|#'|\|\|\||--|[#%*>-])?\s*/g)[0];
        linePrefixTabExpanded = linePrefix;
        if (tabLengthInSpaces) {
          linePrefixTabExpanded = linePrefix.replace(/\t/g, tabLengthInSpaces);
        }
        if (linePrefix) {
          escapedLinePrefix = _.escapeRegExp(linePrefix);
          blockLines = blockLines.map(function(blockLine) {
            return blockLine.replace(RegExp("^" + escapedLinePrefix), '');
          });
        }
        blockLines = blockLines.map(function(blockLine) {
          return blockLine.replace(/^\s+/, '');
        });
        lines = [];
        currentLine = [];
        currentLineLength = linePrefixTabExpanded.length;
        wrappedLinePrefix = linePrefix.replace(/^(\s*)\/\*/, '$1  ').replace(/^(\s*)-(?!-)/, '$1 ');
        firstLine = true;
        ref = this.segmentText(blockLines.join(' '));
        for (j = 0, len1 = ref.length; j < len1; j++) {
          segment = ref[j];
          if (this.wrapSegment(segment, currentLineLength, wrapColumn)) {
            if (firstLine !== true) {
              if (linePrefix.search(/^\s*\/\*/) !== -1 || linePrefix.search(/^\s*-(?!-)/) !== -1) {
                linePrefix = wrappedLinePrefix;
              }
            }
            lines.push(linePrefix + currentLine.join(''));
            currentLine = [];
            currentLineLength = linePrefixTabExpanded.length;
            firstLine = false;
          }
          currentLine.push(segment);
          currentLineLength += segment.length;
        }
        lines.push(linePrefix + currentLine.join(''));
        wrappedLines = beginningLinesToIgnore.concat(lines.concat(endingLinesToIgnore));
        paragraphs.push(wrappedLines.join('\n').replace(/\s+\n/g, '\n'));
      }
      return leadingVerticalSpace + paragraphs.join('\n\n') + trailingVerticalSpace;
    },
    getTabLength: function(editor) {
      var ref;
      return (ref = atom.config.get('editor.tabLength', {
        scope: editor.getRootScopeDescriptor()
      })) != null ? ref : 2;
    },
    getPreferredLineLength: function(editor) {
      return atom.config.get('editor.preferredLineLength', {
        scope: editor.getRootScopeDescriptor()
      });
    },
    wrapSegment: function(segment, currentLineLength, wrapColumn) {
      return CharacterPattern.test(segment) && (currentLineLength + segment.length > wrapColumn) && (currentLineLength > 0 || segment.length < wrapColumn);
    },
    segmentText: function(text) {
      var match, re, segments;
      segments = [];
      re = /[\s]+|[^\s]+/g;
      while (match = re.exec(text)) {
        segments.push(match[0]);
      }
      return segments;
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9hdXRvZmxvdy9saWIvYXV0b2Zsb3cuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQSxDQUFBLEdBQUksT0FBQSxDQUFRLGlCQUFSOztFQUVKLGdCQUFBLEdBQW1COztFQU1uQixNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsUUFBQSxFQUFVLFNBQUE7YUFDUixJQUFDLENBQUEsaUJBQUQsR0FBcUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGtCQUFsQixFQUNuQjtRQUFBLDJCQUFBLEVBQTZCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUMsS0FBRDttQkFDM0IsS0FBQyxDQUFBLGVBQUQsQ0FBaUIsS0FBSyxDQUFDLGFBQWEsQ0FBQyxRQUFwQixDQUFBLENBQWpCO1VBRDJCO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE3QjtPQURtQjtJQURiLENBQVY7SUFLQSxVQUFBLEVBQVksU0FBQTtBQUNWLFVBQUE7O1dBQWtCLENBQUUsT0FBcEIsQ0FBQTs7YUFDQSxJQUFDLENBQUEsaUJBQUQsR0FBcUI7SUFGWCxDQUxaO0lBU0EsZUFBQSxFQUFpQixTQUFDLE1BQUQ7QUFDZixVQUFBO01BQUEsS0FBQSxHQUFRLE1BQU0sQ0FBQyxzQkFBUCxDQUFBO01BQ1IsSUFBbUQsS0FBSyxDQUFDLE9BQU4sQ0FBQSxDQUFuRDtRQUFBLEtBQUEsR0FBUSxNQUFNLENBQUMsOEJBQVAsQ0FBQSxFQUFSOztNQUNBLElBQWMsYUFBZDtBQUFBLGVBQUE7O01BRUEsYUFBQSxHQUNJO1FBQUEsVUFBQSxFQUFZLElBQUMsQ0FBQSxzQkFBRCxDQUF3QixNQUF4QixDQUFaO1FBQ0EsU0FBQSxFQUFXLElBQUMsQ0FBQSxZQUFELENBQWMsTUFBZCxDQURYOztNQUVKLFlBQUEsR0FBZSxJQUFDLENBQUEsTUFBRCxDQUFRLE1BQU0sQ0FBQyxjQUFQLENBQXNCLEtBQXRCLENBQVIsRUFBc0MsYUFBdEM7YUFDZixNQUFNLENBQUMsU0FBUCxDQUFBLENBQWtCLENBQUMsY0FBbkIsQ0FBa0MsS0FBbEMsRUFBeUMsWUFBekM7SUFUZSxDQVRqQjtJQW9CQSxNQUFBLEVBQVEsU0FBQyxJQUFELEVBQU8sR0FBUDtBQUNOLFVBQUE7TUFEYyw2QkFBWTtNQUMxQixVQUFBLEdBQWE7TUFFYixJQUFBLEdBQU8sSUFBSSxDQUFDLE9BQUwsQ0FBYSxRQUFiLEVBQXVCLElBQXZCO01BRVAsb0JBQUEsR0FBdUIsSUFBSSxDQUFDLEtBQUwsQ0FBVyxRQUFYO01BQ3ZCLElBQUcsb0JBQUg7UUFDRSxJQUFBLEdBQU8sSUFBSSxDQUFDLE1BQUwsQ0FBWSxvQkFBb0IsQ0FBQyxNQUFqQyxFQURUO09BQUEsTUFBQTtRQUdFLG9CQUFBLEdBQXVCLEdBSHpCOztNQUtBLHFCQUFBLEdBQXdCLElBQUksQ0FBQyxLQUFMLENBQVcsUUFBWDtNQUN4QixJQUFHLHFCQUFIO1FBQ0UsSUFBQSxHQUFPLElBQUksQ0FBQyxNQUFMLENBQVksQ0FBWixFQUFlLElBQUksQ0FBQyxNQUFMLEdBQWMscUJBQXFCLENBQUMsTUFBbkQsRUFEVDtPQUFBLE1BQUE7UUFHRSxxQkFBQSxHQUF3QixHQUgxQjs7TUFLQSxlQUFBLEdBQWtCLElBQUksQ0FBQyxLQUFMLENBQVcsVUFBWDtNQUNsQixJQUFHLFNBQUg7UUFDRSxpQkFBQSxHQUFvQixLQUFBLENBQU0sU0FBQSxHQUFZLENBQWxCLENBQW9CLENBQUMsSUFBckIsQ0FBMEIsR0FBMUIsRUFEdEI7T0FBQSxNQUFBO1FBR0UsaUJBQUEsR0FBb0IsR0FIdEI7O0FBS0EsV0FBQSxpREFBQTs7UUFDRSxVQUFBLEdBQWEsS0FBSyxDQUFDLEtBQU4sQ0FBWSxJQUFaO1FBSWIsc0JBQUEsR0FBeUI7UUFDekIsbUJBQUEsR0FBc0I7UUFDdEIsYUFBQSxHQUFnQjtRQUNoQixrQkFBQSxHQUFxQjtRQUNyQixnQkFBQSxHQUFtQjtBQUNuQixlQUFNLFVBQVUsQ0FBQyxNQUFYLEdBQW9CLENBQXBCLElBQTBCLENBQzFCLFVBQVcsQ0FBQSxDQUFBLENBQUUsQ0FBQyxLQUFkLENBQW9CLGFBQXBCLENBQUEsSUFDQSxVQUFXLENBQUEsQ0FBQSxDQUFFLENBQUMsS0FBZCxDQUFvQixrQkFBcEIsQ0FGMEIsQ0FBaEM7VUFHRSxzQkFBc0IsQ0FBQyxJQUF2QixDQUE0QixVQUFXLENBQUEsQ0FBQSxDQUF2QztVQUNBLFVBQVUsQ0FBQyxLQUFYLENBQUE7UUFKRjtBQUtBLGVBQU0sVUFBVSxDQUFDLE1BQVgsR0FBb0IsQ0FBcEIsSUFBMEIsQ0FDMUIsVUFBVyxDQUFBLFVBQVUsQ0FBQyxNQUFYLEdBQW9CLENBQXBCLENBQXNCLENBQUMsS0FBbEMsQ0FBd0MsYUFBeEMsQ0FBQSxJQUNBLFVBQVcsQ0FBQSxVQUFVLENBQUMsTUFBWCxHQUFvQixDQUFwQixDQUFzQixDQUFDLEtBQWxDLENBQXdDLGdCQUF4QyxDQUYwQixDQUFoQztVQUdFLG1CQUFtQixDQUFDLE9BQXBCLENBQTRCLFVBQVcsQ0FBQSxVQUFVLENBQUMsTUFBWCxHQUFvQixDQUFwQixDQUF2QztVQUNBLFVBQVUsQ0FBQyxHQUFYLENBQUE7UUFKRjtRQVVBLElBQUEsQ0FBQSxDQUFPLFVBQVUsQ0FBQyxNQUFYLEdBQW9CLENBQTNCLENBQUE7VUFDRSxVQUFVLENBQUMsSUFBWCxDQUFnQixLQUFoQjtBQUNBLG1CQUZGOztRQU1BLFVBQUEsR0FBYSxVQUFXLENBQUEsQ0FBQSxDQUFFLENBQUMsS0FBZCxDQUFvQiw4Q0FBcEIsQ0FBb0UsQ0FBQSxDQUFBO1FBQ2pGLHFCQUFBLEdBQXdCO1FBQ3hCLElBQUcsaUJBQUg7VUFDRSxxQkFBQSxHQUF3QixVQUFVLENBQUMsT0FBWCxDQUFtQixLQUFuQixFQUEwQixpQkFBMUIsRUFEMUI7O1FBR0EsSUFBRyxVQUFIO1VBQ0UsaUJBQUEsR0FBb0IsQ0FBQyxDQUFDLFlBQUYsQ0FBZSxVQUFmO1VBQ3BCLFVBQUEsR0FBYSxVQUFVLENBQUMsR0FBWCxDQUFlLFNBQUMsU0FBRDttQkFDMUIsU0FBUyxDQUFDLE9BQVYsQ0FBa0IsTUFBQSxDQUFBLEdBQUEsR0FBTSxpQkFBTixDQUFsQixFQUErQyxFQUEvQztVQUQwQixDQUFmLEVBRmY7O1FBS0EsVUFBQSxHQUFhLFVBQVUsQ0FBQyxHQUFYLENBQWUsU0FBQyxTQUFEO2lCQUMxQixTQUFTLENBQUMsT0FBVixDQUFrQixNQUFsQixFQUEwQixFQUExQjtRQUQwQixDQUFmO1FBR2IsS0FBQSxHQUFRO1FBQ1IsV0FBQSxHQUFjO1FBQ2QsaUJBQUEsR0FBb0IscUJBQXFCLENBQUM7UUFFMUMsaUJBQUEsR0FBb0IsVUFDbEIsQ0FBQyxPQURpQixDQUNULFlBRFMsRUFDSyxNQURMLENBRWxCLENBQUMsT0FGaUIsQ0FFVCxjQUZTLEVBRU8sS0FGUDtRQUlwQixTQUFBLEdBQVk7QUFDWjtBQUFBLGFBQUEsdUNBQUE7O1VBQ0UsSUFBRyxJQUFDLENBQUEsV0FBRCxDQUFhLE9BQWIsRUFBc0IsaUJBQXRCLEVBQXlDLFVBQXpDLENBQUg7WUFHRSxJQUFHLFNBQUEsS0FBZSxJQUFsQjtjQUVFLElBQUcsVUFBVSxDQUFDLE1BQVgsQ0FBa0IsVUFBbEIsQ0FBQSxLQUFtQyxDQUFDLENBQXBDLElBQXlDLFVBQVUsQ0FBQyxNQUFYLENBQWtCLFlBQWxCLENBQUEsS0FBcUMsQ0FBQyxDQUFsRjtnQkFDRSxVQUFBLEdBQWEsa0JBRGY7ZUFGRjs7WUFJQSxLQUFLLENBQUMsSUFBTixDQUFXLFVBQUEsR0FBYSxXQUFXLENBQUMsSUFBWixDQUFpQixFQUFqQixDQUF4QjtZQUNBLFdBQUEsR0FBYztZQUNkLGlCQUFBLEdBQW9CLHFCQUFxQixDQUFDO1lBQzFDLFNBQUEsR0FBWSxNQVZkOztVQVdBLFdBQVcsQ0FBQyxJQUFaLENBQWlCLE9BQWpCO1VBQ0EsaUJBQUEsSUFBcUIsT0FBTyxDQUFDO0FBYi9CO1FBY0EsS0FBSyxDQUFDLElBQU4sQ0FBVyxVQUFBLEdBQWEsV0FBVyxDQUFDLElBQVosQ0FBaUIsRUFBakIsQ0FBeEI7UUFFQSxZQUFBLEdBQWUsc0JBQXNCLENBQUMsTUFBdkIsQ0FBOEIsS0FBSyxDQUFDLE1BQU4sQ0FBYSxtQkFBYixDQUE5QjtRQUNmLFVBQVUsQ0FBQyxJQUFYLENBQWdCLFlBQVksQ0FBQyxJQUFiLENBQWtCLElBQWxCLENBQXVCLENBQUMsT0FBeEIsQ0FBZ0MsUUFBaEMsRUFBMEMsSUFBMUMsQ0FBaEI7QUF0RUY7QUF3RUEsYUFBTyxvQkFBQSxHQUF1QixVQUFVLENBQUMsSUFBWCxDQUFnQixNQUFoQixDQUF2QixHQUFpRDtJQS9GbEQsQ0FwQlI7SUFxSEEsWUFBQSxFQUFjLFNBQUMsTUFBRDtBQUNaLFVBQUE7OzswQkFBOEU7SUFEbEUsQ0FySGQ7SUF3SEEsc0JBQUEsRUFBd0IsU0FBQyxNQUFEO2FBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQiw0QkFBaEIsRUFBOEM7UUFBQSxLQUFBLEVBQU8sTUFBTSxDQUFDLHNCQUFQLENBQUEsQ0FBUDtPQUE5QztJQURzQixDQXhIeEI7SUEySEEsV0FBQSxFQUFhLFNBQUMsT0FBRCxFQUFVLGlCQUFWLEVBQTZCLFVBQTdCO2FBQ1gsZ0JBQWdCLENBQUMsSUFBakIsQ0FBc0IsT0FBdEIsQ0FBQSxJQUNFLENBQUMsaUJBQUEsR0FBb0IsT0FBTyxDQUFDLE1BQTVCLEdBQXFDLFVBQXRDLENBREYsSUFFRSxDQUFDLGlCQUFBLEdBQW9CLENBQXBCLElBQXlCLE9BQU8sQ0FBQyxNQUFSLEdBQWlCLFVBQTNDO0lBSFMsQ0EzSGI7SUFnSUEsV0FBQSxFQUFhLFNBQUMsSUFBRDtBQUNYLFVBQUE7TUFBQSxRQUFBLEdBQVc7TUFDWCxFQUFBLEdBQUs7QUFDbUIsYUFBTSxLQUFBLEdBQVEsRUFBRSxDQUFDLElBQUgsQ0FBUSxJQUFSLENBQWQ7UUFBeEIsUUFBUSxDQUFDLElBQVQsQ0FBYyxLQUFNLENBQUEsQ0FBQSxDQUFwQjtNQUF3QjthQUN4QjtJQUpXLENBaEliOztBQVRGIiwic291cmNlc0NvbnRlbnQiOlsiXyA9IHJlcXVpcmUgJ3VuZGVyc2NvcmUtcGx1cydcblxuQ2hhcmFjdGVyUGF0dGVybiA9IC8vL1xuICBbXG4gICAgXlxcc1xuICBdXG4vLy9cblxubW9kdWxlLmV4cG9ydHMgPVxuICBhY3RpdmF0ZTogLT5cbiAgICBAY29tbWFuZERpc3Bvc2FibGUgPSBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS10ZXh0LWVkaXRvcicsXG4gICAgICAnYXV0b2Zsb3c6cmVmbG93LXNlbGVjdGlvbic6IChldmVudCkgPT5cbiAgICAgICAgQHJlZmxvd1NlbGVjdGlvbihldmVudC5jdXJyZW50VGFyZ2V0LmdldE1vZGVsKCkpXG5cbiAgZGVhY3RpdmF0ZTogLT5cbiAgICBAY29tbWFuZERpc3Bvc2FibGU/LmRpc3Bvc2UoKVxuICAgIEBjb21tYW5kRGlzcG9zYWJsZSA9IG51bGxcblxuICByZWZsb3dTZWxlY3Rpb246IChlZGl0b3IpIC0+XG4gICAgcmFuZ2UgPSBlZGl0b3IuZ2V0U2VsZWN0ZWRCdWZmZXJSYW5nZSgpXG4gICAgcmFuZ2UgPSBlZGl0b3IuZ2V0Q3VycmVudFBhcmFncmFwaEJ1ZmZlclJhbmdlKCkgaWYgcmFuZ2UuaXNFbXB0eSgpXG4gICAgcmV0dXJuIHVubGVzcyByYW5nZT9cblxuICAgIHJlZmxvd09wdGlvbnMgPVxuICAgICAgICB3cmFwQ29sdW1uOiBAZ2V0UHJlZmVycmVkTGluZUxlbmd0aChlZGl0b3IpXG4gICAgICAgIHRhYkxlbmd0aDogQGdldFRhYkxlbmd0aChlZGl0b3IpXG4gICAgcmVmbG93ZWRUZXh0ID0gQHJlZmxvdyhlZGl0b3IuZ2V0VGV4dEluUmFuZ2UocmFuZ2UpLCByZWZsb3dPcHRpb25zKVxuICAgIGVkaXRvci5nZXRCdWZmZXIoKS5zZXRUZXh0SW5SYW5nZShyYW5nZSwgcmVmbG93ZWRUZXh0KVxuXG4gIHJlZmxvdzogKHRleHQsIHt3cmFwQ29sdW1uLCB0YWJMZW5ndGh9KSAtPlxuICAgIHBhcmFncmFwaHMgPSBbXVxuICAgICMgQ29udmVydCBhbGwgXFxyXFxuIGFuZCBcXHIgdG8gXFxuLiBUaGUgdGV4dCBidWZmZXIgd2lsbCBub3JtYWxpemUgdGhlbSBsYXRlclxuICAgIHRleHQgPSB0ZXh0LnJlcGxhY2UoL1xcclxcbj8vZywgJ1xcbicpXG5cbiAgICBsZWFkaW5nVmVydGljYWxTcGFjZSA9IHRleHQubWF0Y2goL15cXHMqXFxuLylcbiAgICBpZiBsZWFkaW5nVmVydGljYWxTcGFjZVxuICAgICAgdGV4dCA9IHRleHQuc3Vic3RyKGxlYWRpbmdWZXJ0aWNhbFNwYWNlLmxlbmd0aClcbiAgICBlbHNlXG4gICAgICBsZWFkaW5nVmVydGljYWxTcGFjZSA9ICcnXG5cbiAgICB0cmFpbGluZ1ZlcnRpY2FsU3BhY2UgPSB0ZXh0Lm1hdGNoKC9cXG5cXHMqJC8pXG4gICAgaWYgdHJhaWxpbmdWZXJ0aWNhbFNwYWNlXG4gICAgICB0ZXh0ID0gdGV4dC5zdWJzdHIoMCwgdGV4dC5sZW5ndGggLSB0cmFpbGluZ1ZlcnRpY2FsU3BhY2UubGVuZ3RoKVxuICAgIGVsc2VcbiAgICAgIHRyYWlsaW5nVmVydGljYWxTcGFjZSA9ICcnXG5cbiAgICBwYXJhZ3JhcGhCbG9ja3MgPSB0ZXh0LnNwbGl0KC9cXG5cXHMqXFxuL2cpXG4gICAgaWYgdGFiTGVuZ3RoXG4gICAgICB0YWJMZW5ndGhJblNwYWNlcyA9IEFycmF5KHRhYkxlbmd0aCArIDEpLmpvaW4oJyAnKVxuICAgIGVsc2VcbiAgICAgIHRhYkxlbmd0aEluU3BhY2VzID0gJydcblxuICAgIGZvciBibG9jayBpbiBwYXJhZ3JhcGhCbG9ja3NcbiAgICAgIGJsb2NrTGluZXMgPSBibG9jay5zcGxpdCgnXFxuJylcblxuICAgICAgIyBGb3IgTGFUZVggdGFncyBzdXJyb3VuZGluZyB0aGUgdGV4dCwgd2Ugc2ltcGx5IGlnbm9yZSB0aGVtLCBhbmRcbiAgICAgICMgcmVwcm9kdWNlIHRoZW0gdmVyYmF0aW0gaW4gdGhlIHdyYXBwZWQgdGV4dC5cbiAgICAgIGJlZ2lubmluZ0xpbmVzVG9JZ25vcmUgPSBbXVxuICAgICAgZW5kaW5nTGluZXNUb0lnbm9yZSA9IFtdXG4gICAgICBsYXRleFRhZ1JlZ2V4ID0gL15cXHMqXFxcXFxcdysoXFxbLipcXF0pP1xce1xcdytcXH0oXFxbLipcXF0pP1xccyokL2cgICAgIyBlLmcuIFxcYmVnaW57dmVyYmF0aW19XG4gICAgICBsYXRleFRhZ1N0YXJ0UmVnZXggPSAvXlxccypcXFxcXFx3K1xccypcXHtcXHMqJC9nICAgICAgICAgICAgICAgICAgICMgZS5nLiBcXGl0ZW17XG4gICAgICBsYXRleFRhZ0VuZFJlZ2V4ID0gL15cXHMqXFx9XFxzKiQvZyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIyBlLmcuIH1cbiAgICAgIHdoaWxlIGJsb2NrTGluZXMubGVuZ3RoID4gMCBhbmQgKFxuICAgICAgICAgICAgYmxvY2tMaW5lc1swXS5tYXRjaChsYXRleFRhZ1JlZ2V4KSBvclxuICAgICAgICAgICAgYmxvY2tMaW5lc1swXS5tYXRjaChsYXRleFRhZ1N0YXJ0UmVnZXgpKVxuICAgICAgICBiZWdpbm5pbmdMaW5lc1RvSWdub3JlLnB1c2goYmxvY2tMaW5lc1swXSlcbiAgICAgICAgYmxvY2tMaW5lcy5zaGlmdCgpXG4gICAgICB3aGlsZSBibG9ja0xpbmVzLmxlbmd0aCA+IDAgYW5kIChcbiAgICAgICAgICAgIGJsb2NrTGluZXNbYmxvY2tMaW5lcy5sZW5ndGggLSAxXS5tYXRjaChsYXRleFRhZ1JlZ2V4KSBvclxuICAgICAgICAgICAgYmxvY2tMaW5lc1tibG9ja0xpbmVzLmxlbmd0aCAtIDFdLm1hdGNoKGxhdGV4VGFnRW5kUmVnZXgpKVxuICAgICAgICBlbmRpbmdMaW5lc1RvSWdub3JlLnVuc2hpZnQoYmxvY2tMaW5lc1tibG9ja0xpbmVzLmxlbmd0aCAtIDFdKVxuICAgICAgICBibG9ja0xpbmVzLnBvcCgpXG5cbiAgICAgICMgVGhlIHBhcmFncmFwaCBtaWdodCBiZSBhIExhVGVYIHNlY3Rpb24gd2l0aCBubyB0ZXh0LCBvbmx5IHRhZ3M6XG4gICAgICAjIFxcZG9jdW1lbnRjbGFzc3thcnRpY2xlfVxuICAgICAgIyBJbiB0aGF0IGNhc2UsIHdlIGhhdmUgbm90aGluZyB0byByZWZsb3cuXG4gICAgICAjIFB1c2ggdGhlIHRhZ3MgdmVyYmF0aW0gYW5kIGNvbnRpbnVlIHRvIHRoZSBuZXh0IHBhcmFncmFwaC5cbiAgICAgIHVubGVzcyBibG9ja0xpbmVzLmxlbmd0aCA+IDBcbiAgICAgICAgcGFyYWdyYXBocy5wdXNoKGJsb2NrKVxuICAgICAgICBjb250aW51ZVxuXG4gICAgICAjIFRPRE86IHRoaXMgY291bGQgYmUgbW9yZSBsYW5ndWFnZSBzcGVjaWZpYy4gVXNlIHRoZSBhY3R1YWwgY29tbWVudCBjaGFyLlxuICAgICAgIyBSZW1lbWJlciB0aGF0IGAtYCBoYXMgdG8gYmUgdGhlIGxhc3QgY2hhcmFjdGVyIGluIHRoZSBjaGFyYWN0ZXIgY2xhc3MuXG4gICAgICBsaW5lUHJlZml4ID0gYmxvY2tMaW5lc1swXS5tYXRjaCgvXlxccyooXFwvXFwvfFxcL1xcKnw7O3wjJ3xcXHxcXHxcXHx8LS18WyMlKj4tXSk/XFxzKi9nKVswXVxuICAgICAgbGluZVByZWZpeFRhYkV4cGFuZGVkID0gbGluZVByZWZpeFxuICAgICAgaWYgdGFiTGVuZ3RoSW5TcGFjZXNcbiAgICAgICAgbGluZVByZWZpeFRhYkV4cGFuZGVkID0gbGluZVByZWZpeC5yZXBsYWNlKC9cXHQvZywgdGFiTGVuZ3RoSW5TcGFjZXMpXG5cbiAgICAgIGlmIGxpbmVQcmVmaXhcbiAgICAgICAgZXNjYXBlZExpbmVQcmVmaXggPSBfLmVzY2FwZVJlZ0V4cChsaW5lUHJlZml4KVxuICAgICAgICBibG9ja0xpbmVzID0gYmxvY2tMaW5lcy5tYXAgKGJsb2NrTGluZSkgLT5cbiAgICAgICAgICBibG9ja0xpbmUucmVwbGFjZSgvLy9eI3tlc2NhcGVkTGluZVByZWZpeH0vLy8sICcnKVxuXG4gICAgICBibG9ja0xpbmVzID0gYmxvY2tMaW5lcy5tYXAgKGJsb2NrTGluZSkgLT5cbiAgICAgICAgYmxvY2tMaW5lLnJlcGxhY2UoL15cXHMrLywgJycpXG5cbiAgICAgIGxpbmVzID0gW11cbiAgICAgIGN1cnJlbnRMaW5lID0gW11cbiAgICAgIGN1cnJlbnRMaW5lTGVuZ3RoID0gbGluZVByZWZpeFRhYkV4cGFuZGVkLmxlbmd0aFxuXG4gICAgICB3cmFwcGVkTGluZVByZWZpeCA9IGxpbmVQcmVmaXhcbiAgICAgICAgLnJlcGxhY2UoL14oXFxzKilcXC9cXCovLCAnJDEgICcpXG4gICAgICAgIC5yZXBsYWNlKC9eKFxccyopLSg/IS0pLywgJyQxICcpXG5cbiAgICAgIGZpcnN0TGluZSA9IHRydWVcbiAgICAgIGZvciBzZWdtZW50IGluIEBzZWdtZW50VGV4dChibG9ja0xpbmVzLmpvaW4oJyAnKSlcbiAgICAgICAgaWYgQHdyYXBTZWdtZW50KHNlZ21lbnQsIGN1cnJlbnRMaW5lTGVuZ3RoLCB3cmFwQ29sdW1uKVxuXG4gICAgICAgICAgIyBJbmRlcGVuZGVudCBvZiBsaW5lIHByZWZpeCBkb24ndCBtZXNzIHdpdGggaXQgb24gdGhlIGZpcnN0IGxpbmVcbiAgICAgICAgICBpZiBmaXJzdExpbmUgaXNudCB0cnVlXG4gICAgICAgICAgICAjIEhhbmRsZSBDIGNvbW1lbnRzXG4gICAgICAgICAgICBpZiBsaW5lUHJlZml4LnNlYXJjaCgvXlxccypcXC9cXCovKSBpc250IC0xIG9yIGxpbmVQcmVmaXguc2VhcmNoKC9eXFxzKi0oPyEtKS8pIGlzbnQgLTFcbiAgICAgICAgICAgICAgbGluZVByZWZpeCA9IHdyYXBwZWRMaW5lUHJlZml4XG4gICAgICAgICAgbGluZXMucHVzaChsaW5lUHJlZml4ICsgY3VycmVudExpbmUuam9pbignJykpXG4gICAgICAgICAgY3VycmVudExpbmUgPSBbXVxuICAgICAgICAgIGN1cnJlbnRMaW5lTGVuZ3RoID0gbGluZVByZWZpeFRhYkV4cGFuZGVkLmxlbmd0aFxuICAgICAgICAgIGZpcnN0TGluZSA9IGZhbHNlXG4gICAgICAgIGN1cnJlbnRMaW5lLnB1c2goc2VnbWVudClcbiAgICAgICAgY3VycmVudExpbmVMZW5ndGggKz0gc2VnbWVudC5sZW5ndGhcbiAgICAgIGxpbmVzLnB1c2gobGluZVByZWZpeCArIGN1cnJlbnRMaW5lLmpvaW4oJycpKVxuXG4gICAgICB3cmFwcGVkTGluZXMgPSBiZWdpbm5pbmdMaW5lc1RvSWdub3JlLmNvbmNhdChsaW5lcy5jb25jYXQoZW5kaW5nTGluZXNUb0lnbm9yZSkpXG4gICAgICBwYXJhZ3JhcGhzLnB1c2god3JhcHBlZExpbmVzLmpvaW4oJ1xcbicpLnJlcGxhY2UoL1xccytcXG4vZywgJ1xcbicpKVxuXG4gICAgcmV0dXJuIGxlYWRpbmdWZXJ0aWNhbFNwYWNlICsgcGFyYWdyYXBocy5qb2luKCdcXG5cXG4nKSArIHRyYWlsaW5nVmVydGljYWxTcGFjZVxuXG4gIGdldFRhYkxlbmd0aDogKGVkaXRvcikgLT5cbiAgICBhdG9tLmNvbmZpZy5nZXQoJ2VkaXRvci50YWJMZW5ndGgnLCBzY29wZTogZWRpdG9yLmdldFJvb3RTY29wZURlc2NyaXB0b3IoKSkgPyAyXG5cbiAgZ2V0UHJlZmVycmVkTGluZUxlbmd0aDogKGVkaXRvcikgLT5cbiAgICBhdG9tLmNvbmZpZy5nZXQoJ2VkaXRvci5wcmVmZXJyZWRMaW5lTGVuZ3RoJywgc2NvcGU6IGVkaXRvci5nZXRSb290U2NvcGVEZXNjcmlwdG9yKCkpXG5cbiAgd3JhcFNlZ21lbnQ6IChzZWdtZW50LCBjdXJyZW50TGluZUxlbmd0aCwgd3JhcENvbHVtbikgLT5cbiAgICBDaGFyYWN0ZXJQYXR0ZXJuLnRlc3Qoc2VnbWVudCkgYW5kXG4gICAgICAoY3VycmVudExpbmVMZW5ndGggKyBzZWdtZW50Lmxlbmd0aCA+IHdyYXBDb2x1bW4pIGFuZFxuICAgICAgKGN1cnJlbnRMaW5lTGVuZ3RoID4gMCBvciBzZWdtZW50Lmxlbmd0aCA8IHdyYXBDb2x1bW4pXG5cbiAgc2VnbWVudFRleHQ6ICh0ZXh0KSAtPlxuICAgIHNlZ21lbnRzID0gW11cbiAgICByZSA9IC9bXFxzXSt8W15cXHNdKy9nXG4gICAgc2VnbWVudHMucHVzaChtYXRjaFswXSkgd2hpbGUgbWF0Y2ggPSByZS5leGVjKHRleHQpXG4gICAgc2VnbWVudHNcbiJdfQ==
