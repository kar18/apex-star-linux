(function() {
  var Bookmarks, BookmarksView, CompositeDisposable, ReactBookmarks, disposables, editorsBookmarks;

  CompositeDisposable = require('atom').CompositeDisposable;

  Bookmarks = null;

  ReactBookmarks = null;

  BookmarksView = require('./bookmarks-view');

  editorsBookmarks = null;

  disposables = null;

  module.exports = {
    activate: function(bookmarksByEditorId) {
      var bookmarksView, watchedEditors;
      editorsBookmarks = [];
      watchedEditors = new WeakSet();
      bookmarksView = null;
      disposables = new CompositeDisposable;
      atom.commands.add('atom-workspace', 'bookmarks:view-all', function() {
        if (bookmarksView == null) {
          bookmarksView = new BookmarksView(editorsBookmarks);
        }
        return bookmarksView.show();
      });
      return atom.workspace.observeTextEditors(function(textEditor) {
        var bookmarks, state;
        if (watchedEditors.has(textEditor)) {
          return;
        }
        if (Bookmarks == null) {
          Bookmarks = require('./bookmarks');
        }
        if (state = bookmarksByEditorId[textEditor.id]) {
          bookmarks = Bookmarks.deserialize(textEditor, state);
        } else {
          bookmarks = new Bookmarks(textEditor);
        }
        editorsBookmarks.push(bookmarks);
        watchedEditors.add(textEditor);
        return disposables.add(textEditor.onDidDestroy(function() {
          var index;
          index = editorsBookmarks.indexOf(bookmarks);
          if (index !== -1) {
            editorsBookmarks.splice(index, 1);
          }
          bookmarks.destroy();
          return watchedEditors["delete"](textEditor);
        }));
      });
    },
    deactivate: function() {
      var bookmarks, i, len;
      if (typeof bookmarksView !== "undefined" && bookmarksView !== null) {
        bookmarksView.destroy();
      }
      for (i = 0, len = editorsBookmarks.length; i < len; i++) {
        bookmarks = editorsBookmarks[i];
        bookmarks.deactivate();
      }
      return disposables.dispose();
    },
    serialize: function() {
      var bookmarks, bookmarksByEditorId, i, len;
      bookmarksByEditorId = {};
      for (i = 0, len = editorsBookmarks.length; i < len; i++) {
        bookmarks = editorsBookmarks[i];
        bookmarksByEditorId[bookmarks.editor.id] = bookmarks.serialize();
      }
      return bookmarksByEditorId;
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9ib29rbWFya3MvbGliL21haW4uY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQyxzQkFBdUIsT0FBQSxDQUFRLE1BQVI7O0VBRXhCLFNBQUEsR0FBWTs7RUFDWixjQUFBLEdBQWlCOztFQUNqQixhQUFBLEdBQWdCLE9BQUEsQ0FBUSxrQkFBUjs7RUFDaEIsZ0JBQUEsR0FBbUI7O0VBQ25CLFdBQUEsR0FBYzs7RUFFZCxNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsUUFBQSxFQUFVLFNBQUMsbUJBQUQ7QUFDUixVQUFBO01BQUEsZ0JBQUEsR0FBbUI7TUFDbkIsY0FBQSxHQUFpQixJQUFJLE9BQUosQ0FBQTtNQUNqQixhQUFBLEdBQWdCO01BQ2hCLFdBQUEsR0FBYyxJQUFJO01BRWxCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFDRSxvQkFERixFQUN3QixTQUFBOztVQUNwQixnQkFBaUIsSUFBSSxhQUFKLENBQWtCLGdCQUFsQjs7ZUFDakIsYUFBYSxDQUFDLElBQWQsQ0FBQTtNQUZvQixDQUR4QjthQUtBLElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWYsQ0FBa0MsU0FBQyxVQUFEO0FBQ2hDLFlBQUE7UUFBQSxJQUFVLGNBQWMsQ0FBQyxHQUFmLENBQW1CLFVBQW5CLENBQVY7QUFBQSxpQkFBQTs7O1VBRUEsWUFBYSxPQUFBLENBQVEsYUFBUjs7UUFDYixJQUFHLEtBQUEsR0FBUSxtQkFBb0IsQ0FBQSxVQUFVLENBQUMsRUFBWCxDQUEvQjtVQUNFLFNBQUEsR0FBWSxTQUFTLENBQUMsV0FBVixDQUFzQixVQUF0QixFQUFrQyxLQUFsQyxFQURkO1NBQUEsTUFBQTtVQUdFLFNBQUEsR0FBWSxJQUFJLFNBQUosQ0FBYyxVQUFkLEVBSGQ7O1FBSUEsZ0JBQWdCLENBQUMsSUFBakIsQ0FBc0IsU0FBdEI7UUFDQSxjQUFjLENBQUMsR0FBZixDQUFtQixVQUFuQjtlQUNBLFdBQVcsQ0FBQyxHQUFaLENBQWdCLFVBQVUsQ0FBQyxZQUFYLENBQXdCLFNBQUE7QUFDdEMsY0FBQTtVQUFBLEtBQUEsR0FBUSxnQkFBZ0IsQ0FBQyxPQUFqQixDQUF5QixTQUF6QjtVQUNSLElBQXFDLEtBQUEsS0FBVyxDQUFDLENBQWpEO1lBQUEsZ0JBQWdCLENBQUMsTUFBakIsQ0FBd0IsS0FBeEIsRUFBK0IsQ0FBL0IsRUFBQTs7VUFDQSxTQUFTLENBQUMsT0FBVixDQUFBO2lCQUNBLGNBQWMsRUFBQyxNQUFELEVBQWQsQ0FBc0IsVUFBdEI7UUFKc0MsQ0FBeEIsQ0FBaEI7TUFWZ0MsQ0FBbEM7SUFYUSxDQUFWO0lBMkJBLFVBQUEsRUFBWSxTQUFBO0FBQ1YsVUFBQTs7UUFBQSxhQUFhLENBQUUsT0FBZixDQUFBOztBQUNBLFdBQUEsa0RBQUE7O1FBQUEsU0FBUyxDQUFDLFVBQVYsQ0FBQTtBQUFBO2FBQ0EsV0FBVyxDQUFDLE9BQVosQ0FBQTtJQUhVLENBM0JaO0lBZ0NBLFNBQUEsRUFBVyxTQUFBO0FBQ1QsVUFBQTtNQUFBLG1CQUFBLEdBQXNCO0FBQ3RCLFdBQUEsa0RBQUE7O1FBQ0UsbUJBQW9CLENBQUEsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFqQixDQUFwQixHQUEyQyxTQUFTLENBQUMsU0FBVixDQUFBO0FBRDdDO2FBRUE7SUFKUyxDQWhDWDs7QUFURiIsInNvdXJjZXNDb250ZW50IjpbIntDb21wb3NpdGVEaXNwb3NhYmxlfSA9IHJlcXVpcmUgJ2F0b20nXG5cbkJvb2ttYXJrcyA9IG51bGxcblJlYWN0Qm9va21hcmtzID0gbnVsbFxuQm9va21hcmtzVmlldyA9IHJlcXVpcmUgJy4vYm9va21hcmtzLXZpZXcnXG5lZGl0b3JzQm9va21hcmtzID0gbnVsbFxuZGlzcG9zYWJsZXMgPSBudWxsXG5cbm1vZHVsZS5leHBvcnRzID1cbiAgYWN0aXZhdGU6IChib29rbWFya3NCeUVkaXRvcklkKSAtPlxuICAgIGVkaXRvcnNCb29rbWFya3MgPSBbXVxuICAgIHdhdGNoZWRFZGl0b3JzID0gbmV3IFdlYWtTZXQoKVxuICAgIGJvb2ttYXJrc1ZpZXcgPSBudWxsXG4gICAgZGlzcG9zYWJsZXMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZVxuXG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJyxcbiAgICAgICdib29rbWFya3M6dmlldy1hbGwnLCAtPlxuICAgICAgICBib29rbWFya3NWaWV3ID89IG5ldyBCb29rbWFya3NWaWV3KGVkaXRvcnNCb29rbWFya3MpXG4gICAgICAgIGJvb2ttYXJrc1ZpZXcuc2hvdygpXG5cbiAgICBhdG9tLndvcmtzcGFjZS5vYnNlcnZlVGV4dEVkaXRvcnMgKHRleHRFZGl0b3IpIC0+XG4gICAgICByZXR1cm4gaWYgd2F0Y2hlZEVkaXRvcnMuaGFzKHRleHRFZGl0b3IpXG5cbiAgICAgIEJvb2ttYXJrcyA/PSByZXF1aXJlICcuL2Jvb2ttYXJrcydcbiAgICAgIGlmIHN0YXRlID0gYm9va21hcmtzQnlFZGl0b3JJZFt0ZXh0RWRpdG9yLmlkXVxuICAgICAgICBib29rbWFya3MgPSBCb29rbWFya3MuZGVzZXJpYWxpemUodGV4dEVkaXRvciwgc3RhdGUpXG4gICAgICBlbHNlXG4gICAgICAgIGJvb2ttYXJrcyA9IG5ldyBCb29rbWFya3ModGV4dEVkaXRvcilcbiAgICAgIGVkaXRvcnNCb29rbWFya3MucHVzaChib29rbWFya3MpXG4gICAgICB3YXRjaGVkRWRpdG9ycy5hZGQodGV4dEVkaXRvcilcbiAgICAgIGRpc3Bvc2FibGVzLmFkZCB0ZXh0RWRpdG9yLm9uRGlkRGVzdHJveSAtPlxuICAgICAgICBpbmRleCA9IGVkaXRvcnNCb29rbWFya3MuaW5kZXhPZihib29rbWFya3MpXG4gICAgICAgIGVkaXRvcnNCb29rbWFya3Muc3BsaWNlKGluZGV4LCAxKSBpZiBpbmRleCBpc250IC0xXG4gICAgICAgIGJvb2ttYXJrcy5kZXN0cm95KClcbiAgICAgICAgd2F0Y2hlZEVkaXRvcnMuZGVsZXRlKHRleHRFZGl0b3IpXG5cbiAgZGVhY3RpdmF0ZTogLT5cbiAgICBib29rbWFya3NWaWV3Py5kZXN0cm95KClcbiAgICBib29rbWFya3MuZGVhY3RpdmF0ZSgpIGZvciBib29rbWFya3MgaW4gZWRpdG9yc0Jvb2ttYXJrc1xuICAgIGRpc3Bvc2FibGVzLmRpc3Bvc2UoKVxuXG4gIHNlcmlhbGl6ZTogLT5cbiAgICBib29rbWFya3NCeUVkaXRvcklkID0ge31cbiAgICBmb3IgYm9va21hcmtzIGluIGVkaXRvcnNCb29rbWFya3NcbiAgICAgIGJvb2ttYXJrc0J5RWRpdG9ySWRbYm9va21hcmtzLmVkaXRvci5pZF0gPSBib29rbWFya3Muc2VyaWFsaXplKClcbiAgICBib29rbWFya3NCeUVkaXRvcklkXG4iXX0=
