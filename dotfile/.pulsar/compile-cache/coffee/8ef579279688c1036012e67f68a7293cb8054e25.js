(function() {
  var CommandLogger, FileURLRegExp, NotificationIssue, StackTraceParser, TITLE_CHAR_LIMIT, UserUtilities, fs, path,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  fs = require('fs-plus');

  path = require('path');

  StackTraceParser = require('stacktrace-parser');

  CommandLogger = require('./command-logger');

  UserUtilities = require('./user-utilities');

  TITLE_CHAR_LIMIT = 100;

  FileURLRegExp = new RegExp('file://\w*/(.*)');

  module.exports = NotificationIssue = (function() {
    function NotificationIssue(notification) {
      this.notification = notification;
      this.normalizedStackPaths = bind(this.normalizedStackPaths, this);
    }

    NotificationIssue.prototype.findSimilarIssues = function() {
      var githubHeaders, issueTitle, query, repo, repoUrl;
      repoUrl = this.getRepoUrl();
      if (repoUrl == null) {
        repoUrl = 'atom/atom';
      }
      repo = repoUrl.replace(/http(s)?:\/\/(\d+\.)?github.com\//gi, '');
      issueTitle = this.getIssueTitle();
      query = issueTitle + " repo:" + repo;
      githubHeaders = new Headers({
        accept: 'application/vnd.github.v3+json',
        contentType: "application/json"
      });
      return fetch("https://api.github.com/search/issues?q=" + (encodeURIComponent(query)) + "&sort=created", {
        headers: githubHeaders
      }).then(function(r) {
        return r != null ? r.json() : void 0;
      }).then(function(data) {
        var issue, issues, j, len, ref;
        if ((data != null ? data.items : void 0) != null) {
          issues = {};
          ref = data.items;
          for (j = 0, len = ref.length; j < len; j++) {
            issue = ref[j];
            if (issue.title.indexOf(issueTitle) > -1 && (issues[issue.state] == null)) {
              issues[issue.state] = issue;
              if ((issues.open != null) && (issues.closed != null)) {
                return issues;
              }
            }
          }
          if ((issues.open != null) || (issues.closed != null)) {
            return issues;
          }
        }
        return null;
      })["catch"](function(e) {
        return null;
      });
    };

    NotificationIssue.prototype.getIssueUrlForSystem = function() {
      return this.getIssueUrl().then(function(issueUrl) {
        return fetch("https://is.gd/create.php?format=simple", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: "url=" + (encodeURIComponent(issueUrl))
        }).then(function(r) {
          return r.text();
        })["catch"](function(e) {
          return null;
        });
      });
    };

    NotificationIssue.prototype.getIssueUrl = function() {
      return this.getIssueBody().then((function(_this) {
        return function(issueBody) {
          var repoUrl;
          repoUrl = _this.getRepoUrl();
          if (repoUrl == null) {
            repoUrl = 'https://github.com/atom/atom';
          }
          return repoUrl + "/issues/new?title=" + (_this.encodeURI(_this.getIssueTitle())) + "&body=" + (_this.encodeURI(issueBody));
        };
      })(this));
    };

    NotificationIssue.prototype.encodeURI = function(str) {
      return encodeURI(str).replace(/#/g, '%23').replace(/;/g, '%3B').replace(/%20/g, '+');
    };

    NotificationIssue.prototype.getIssueTitle = function() {
      var title;
      title = this.notification.getMessage();
      title = title.replace(process.env.ATOM_HOME, '$ATOM_HOME');
      if (process.platform === 'win32') {
        title = title.replace(process.env.USERPROFILE, '~');
        title = title.replace(path.sep, path.posix.sep);
      } else {
        title = title.replace(process.env.HOME, '~');
      }
      if (title.length > TITLE_CHAR_LIMIT) {
        title = title.substring(0, TITLE_CHAR_LIMIT - 3) + '...';
      }
      return title.replace(/\r?\n|\r/g, "");
    };

    NotificationIssue.prototype.getIssueBody = function() {
      return new Promise((function(_this) {
        return function(resolve, reject) {
          var nonCorePackagesPromise, systemPromise;
          if (_this.issueBody) {
            return resolve(_this.issueBody);
          }
          systemPromise = UserUtilities.getOSVersion();
          nonCorePackagesPromise = UserUtilities.getNonCorePackages();
          return Promise.all([systemPromise, nonCorePackagesPromise]).then(function(all) {
            var copyText, message, nonCorePackages, options, packageMessage, packageName, packageVersion, ref, ref1, repoUrl, rootUserStatus, systemName, systemUser;
            systemName = all[0], nonCorePackages = all[1];
            message = _this.notification.getMessage();
            options = _this.notification.getOptions();
            repoUrl = _this.getRepoUrl();
            packageName = _this.getPackageName();
            if (packageName != null) {
              packageVersion = (ref = atom.packages.getLoadedPackage(packageName)) != null ? (ref1 = ref.metadata) != null ? ref1.version : void 0 : void 0;
            }
            copyText = '';
            systemUser = process.env.USER;
            rootUserStatus = '';
            if (systemUser === 'root') {
              rootUserStatus = '**User**: root';
            }
            if ((packageName != null) && (repoUrl != null)) {
              packageMessage = "[" + packageName + "](" + repoUrl + ") package " + packageVersion;
            } else if (packageName != null) {
              packageMessage = "'" + packageName + "' package v" + packageVersion;
            } else {
              packageMessage = 'Atom Core';
            }
            _this.issueBody = "<!--\nHave you read Atom's Code of Conduct? By filing an Issue, you are expected to comply with it, including treating everyone with respect: https://github.com/atom/.github/blob/master/CODE_OF_CONDUCT.md\n\nDo you want to ask a question? Are you looking for support? The Atom message board is the best place for getting support: https://discuss.atom.io\n-->\n\n### Prerequisites\n\n* [ ] Put an X between the brackets on this line if you have done all of the following:\n    * Reproduced the problem in Safe Mode: <https://flight-manual.atom.io/hacking-atom/sections/debugging/#using-safe-mode>\n    * Followed all applicable steps in the debugging guide: <https://flight-manual.atom.io/hacking-atom/sections/debugging/>\n    * Checked the FAQs on the message board for common solutions: <https://discuss.atom.io/c/faq>\n    * Checked that your issue isn't already filed: <https://github.com/issues?q=is%3Aissue+user%3Aatom>\n    * Checked that there is not already an Atom package that provides the described functionality: <https://atom.io/packages>\n\n### Description\n\n<!-- Description of the issue -->\n\n### Steps to Reproduce\n\n1. <!-- First Step -->\n2. <!-- Second Step -->\n3. <!-- and so on… -->\n\n**Expected behavior:**\n\n<!-- What you expect to happen -->\n\n**Actual behavior:**\n\n<!-- What actually happens -->\n\n### Versions\n\n**Atom**: " + (atom.getVersion()) + " " + process.arch + "\n**Electron**: " + process.versions.electron + "\n**OS**: " + systemName + "\n**Thrown From**: " + packageMessage + "\n" + rootUserStatus + "\n\n### Stack Trace\n\n" + message + "\n\n```\nAt " + options.detail + "\n\n" + (_this.normalizedStackPaths(options.stack)) + "\n```\n\n### Commands\n\n" + (CommandLogger.instance().getText()) + "\n\n### Non-Core Packages\n\n```\n" + (nonCorePackages.join('\n')) + "\n```\n\n### Additional Information\n\n<!-- Any additional information, configuration or data that might be necessary to reproduce the issue. -->\n" + copyText;
            return resolve(_this.issueBody);
          });
        };
      })(this));
    };

    NotificationIssue.prototype.normalizedStackPaths = function(stack) {
      return stack != null ? stack.replace(/(^\W+at )([\w.]{2,} [(])?(.*)(:\d+:\d+[)]?)/gm, (function(_this) {
        return function(m, p1, p2, p3, p4) {
          return p1 + (p2 || '') + _this.normalizePath(p3) + p4;
        };
      })(this)) : void 0;
    };

    NotificationIssue.prototype.normalizePath = function(path) {
      return path.replace('file:///', '').replace(/[\/]/g, '\\').replace(fs.getHomeDirectory(), '~').replace(/\\/g, '/').replace(/.*(\/(app\.asar|packages\/).*)/, '$1');
    };

    NotificationIssue.prototype.getRepoUrl = function() {
      var packageName, packagePath, ref, ref1, ref2, ref3, ref4, repo, repoUrl;
      packageName = this.getPackageName();
      if (packageName == null) {
        return;
      }
      repo = (ref = atom.packages.getLoadedPackage(packageName)) != null ? (ref1 = ref.metadata) != null ? ref1.repository : void 0 : void 0;
      repoUrl = (ref2 = repo != null ? repo.url : void 0) != null ? ref2 : repo;
      if (!repoUrl) {
        if (packagePath = atom.packages.resolvePackagePath(packageName)) {
          try {
            repo = (ref3 = JSON.parse(fs.readFileSync(path.join(packagePath, 'package.json')))) != null ? ref3.repository : void 0;
            repoUrl = (ref4 = repo != null ? repo.url : void 0) != null ? ref4 : repo;
          } catch (error) {}
        }
      }
      return repoUrl != null ? repoUrl.replace(/\.git$/, '').replace(/^git\+/, '') : void 0;
    };

    NotificationIssue.prototype.getPackageNameFromFilePath = function(filePath) {
      var packageName, ref, ref1, ref2, ref3;
      if (!filePath) {
        return;
      }
      packageName = (ref = /\/\.atom\/dev\/packages\/([^\/]+)\//.exec(filePath)) != null ? ref[1] : void 0;
      if (packageName) {
        return packageName;
      }
      packageName = (ref1 = /\\\.atom\\dev\\packages\\([^\\]+)\\/.exec(filePath)) != null ? ref1[1] : void 0;
      if (packageName) {
        return packageName;
      }
      packageName = (ref2 = /\/\.atom\/packages\/([^\/]+)\//.exec(filePath)) != null ? ref2[1] : void 0;
      if (packageName) {
        return packageName;
      }
      packageName = (ref3 = /\\\.atom\\packages\\([^\\]+)\\/.exec(filePath)) != null ? ref3[1] : void 0;
      if (packageName) {
        return packageName;
      }
    };

    NotificationIssue.prototype.getPackageName = function() {
      var file, getPackageName, i, j, options, packageName, packagePath, packagePaths, ref, stack;
      options = this.notification.getOptions();
      if (options.packageName != null) {
        return options.packageName;
      }
      if (!((options.stack != null) || (options.detail != null))) {
        return;
      }
      packagePaths = this.getPackagePathsByPackageName();
      for (packageName in packagePaths) {
        packagePath = packagePaths[packageName];
        if (packagePath.indexOf(path.join('.atom', 'dev', 'packages')) > -1 || packagePath.indexOf(path.join('.atom', 'packages')) > -1) {
          packagePaths[packageName] = fs.realpathSync(packagePath);
        }
      }
      getPackageName = (function(_this) {
        return function(filePath) {
          var isSubfolder, match, packName;
          filePath = /\((.+?):\d+|\((.+)\)|(.+)/.exec(filePath)[0];
          if (match = FileURLRegExp.exec(filePath)) {
            filePath = match[1];
          }
          filePath = path.normalize(filePath);
          if (path.isAbsolute(filePath)) {
            for (packName in packagePaths) {
              packagePath = packagePaths[packName];
              if (filePath === 'node.js') {
                continue;
              }
              isSubfolder = filePath.indexOf(path.normalize(packagePath + path.sep)) === 0;
              if (isSubfolder) {
                return packName;
              }
            }
          }
          return _this.getPackageNameFromFilePath(filePath);
        };
      })(this);
      if ((options.detail != null) && (packageName = getPackageName(options.detail))) {
        return packageName;
      }
      if (options.stack != null) {
        stack = StackTraceParser.parse(options.stack);
        for (i = j = 0, ref = stack.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
          file = stack[i].file;
          if (!file) {
            return;
          }
          packageName = getPackageName(file);
          if (packageName != null) {
            return packageName;
          }
        }
      }
    };

    NotificationIssue.prototype.getPackagePathsByPackageName = function() {
      var j, len, pack, packagePathsByPackageName, ref;
      packagePathsByPackageName = {};
      ref = atom.packages.getLoadedPackages();
      for (j = 0, len = ref.length; j < len; j++) {
        pack = ref[j];
        packagePathsByPackageName[pack.name] = pack.path;
      }
      return packagePathsByPackageName;
    };

    return NotificationIssue;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL29wdC9QdWxzYXIvcmVzb3VyY2VzL2FwcC5hc2FyL25vZGVfbW9kdWxlcy9ub3RpZmljYXRpb25zL2xpYi9ub3RpZmljYXRpb24taXNzdWUuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQSw0R0FBQTtJQUFBOztFQUFBLEVBQUEsR0FBSyxPQUFBLENBQVEsU0FBUjs7RUFDTCxJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBQ1AsZ0JBQUEsR0FBbUIsT0FBQSxDQUFRLG1CQUFSOztFQUVuQixhQUFBLEdBQWdCLE9BQUEsQ0FBUSxrQkFBUjs7RUFDaEIsYUFBQSxHQUFnQixPQUFBLENBQVEsa0JBQVI7O0VBRWhCLGdCQUFBLEdBQW1COztFQUVuQixhQUFBLEdBQWdCLElBQUksTUFBSixDQUFXLGlCQUFYOztFQUVoQixNQUFNLENBQUMsT0FBUCxHQUNNO0lBQ1MsMkJBQUMsWUFBRDtNQUFDLElBQUMsQ0FBQSxlQUFEOztJQUFEOztnQ0FFYixpQkFBQSxHQUFtQixTQUFBO0FBQ2pCLFVBQUE7TUFBQSxPQUFBLEdBQVUsSUFBQyxDQUFBLFVBQUQsQ0FBQTtNQUNWLElBQTZCLGVBQTdCO1FBQUEsT0FBQSxHQUFVLFlBQVY7O01BQ0EsSUFBQSxHQUFPLE9BQU8sQ0FBQyxPQUFSLENBQWdCLHFDQUFoQixFQUF1RCxFQUF2RDtNQUNQLFVBQUEsR0FBYSxJQUFDLENBQUEsYUFBRCxDQUFBO01BQ2IsS0FBQSxHQUFXLFVBQUQsR0FBWSxRQUFaLEdBQW9CO01BQzlCLGFBQUEsR0FBZ0IsSUFBSSxPQUFKLENBQVk7UUFDMUIsTUFBQSxFQUFRLGdDQURrQjtRQUUxQixXQUFBLEVBQWEsa0JBRmE7T0FBWjthQUtoQixLQUFBLENBQU0seUNBQUEsR0FBeUMsQ0FBQyxrQkFBQSxDQUFtQixLQUFuQixDQUFELENBQXpDLEdBQW9FLGVBQTFFLEVBQTBGO1FBQUMsT0FBQSxFQUFTLGFBQVY7T0FBMUYsQ0FDRSxDQUFDLElBREgsQ0FDUSxTQUFDLENBQUQ7MkJBQU8sQ0FBQyxDQUFFLElBQUgsQ0FBQTtNQUFQLENBRFIsQ0FFRSxDQUFDLElBRkgsQ0FFUSxTQUFDLElBQUQ7QUFDSixZQUFBO1FBQUEsSUFBRyw0Q0FBSDtVQUNFLE1BQUEsR0FBUztBQUNUO0FBQUEsZUFBQSxxQ0FBQTs7WUFDRSxJQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBWixDQUFvQixVQUFwQixDQUFBLEdBQWtDLENBQUMsQ0FBbkMsSUFBNkMsNkJBQWhEO2NBQ0UsTUFBTyxDQUFBLEtBQUssQ0FBQyxLQUFOLENBQVAsR0FBc0I7Y0FDdEIsSUFBaUIscUJBQUEsSUFBaUIsdUJBQWxDO0FBQUEsdUJBQU8sT0FBUDtlQUZGOztBQURGO1VBS0EsSUFBaUIscUJBQUEsSUFBZ0IsdUJBQWpDO0FBQUEsbUJBQU8sT0FBUDtXQVBGOztlQVFBO01BVEksQ0FGUixDQVlFLEVBQUMsS0FBRCxFQVpGLENBWVMsU0FBQyxDQUFEO2VBQU87TUFBUCxDQVpUO0lBWGlCOztnQ0F5Qm5CLG9CQUFBLEdBQXNCLFNBQUE7YUFHcEIsSUFBQyxDQUFBLFdBQUQsQ0FBQSxDQUFjLENBQUMsSUFBZixDQUFvQixTQUFDLFFBQUQ7ZUFDbEIsS0FBQSxDQUFNLHdDQUFOLEVBQWdEO1VBQzlDLE1BQUEsRUFBUSxNQURzQztVQUU5QyxPQUFBLEVBQVM7WUFBQyxjQUFBLEVBQWdCLG1DQUFqQjtXQUZxQztVQUc5QyxJQUFBLEVBQU0sTUFBQSxHQUFNLENBQUMsa0JBQUEsQ0FBbUIsUUFBbkIsQ0FBRCxDQUhrQztTQUFoRCxDQUtBLENBQUMsSUFMRCxDQUtNLFNBQUMsQ0FBRDtpQkFBTyxDQUFDLENBQUMsSUFBRixDQUFBO1FBQVAsQ0FMTixDQU1BLEVBQUMsS0FBRCxFQU5BLENBTU8sU0FBQyxDQUFEO2lCQUFPO1FBQVAsQ0FOUDtNQURrQixDQUFwQjtJQUhvQjs7Z0NBWXRCLFdBQUEsR0FBYSxTQUFBO2FBQ1gsSUFBQyxDQUFBLFlBQUQsQ0FBQSxDQUFlLENBQUMsSUFBaEIsQ0FBcUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLFNBQUQ7QUFDbkIsY0FBQTtVQUFBLE9BQUEsR0FBVSxLQUFDLENBQUEsVUFBRCxDQUFBO1VBQ1YsSUFBZ0QsZUFBaEQ7WUFBQSxPQUFBLEdBQVUsK0JBQVY7O2lCQUNHLE9BQUQsR0FBUyxvQkFBVCxHQUE0QixDQUFDLEtBQUMsQ0FBQSxTQUFELENBQVcsS0FBQyxDQUFBLGFBQUQsQ0FBQSxDQUFYLENBQUQsQ0FBNUIsR0FBMEQsUUFBMUQsR0FBaUUsQ0FBQyxLQUFDLENBQUEsU0FBRCxDQUFXLFNBQVgsQ0FBRDtRQUhoRDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBckI7SUFEVzs7Z0NBTWIsU0FBQSxHQUFXLFNBQUMsR0FBRDthQUNULFNBQUEsQ0FBVSxHQUFWLENBQWMsQ0FBQyxPQUFmLENBQXVCLElBQXZCLEVBQTZCLEtBQTdCLENBQW1DLENBQUMsT0FBcEMsQ0FBNEMsSUFBNUMsRUFBa0QsS0FBbEQsQ0FBd0QsQ0FBQyxPQUF6RCxDQUFpRSxNQUFqRSxFQUF5RSxHQUF6RTtJQURTOztnQ0FHWCxhQUFBLEdBQWUsU0FBQTtBQUNiLFVBQUE7TUFBQSxLQUFBLEdBQVEsSUFBQyxDQUFBLFlBQVksQ0FBQyxVQUFkLENBQUE7TUFDUixLQUFBLEdBQVEsS0FBSyxDQUFDLE9BQU4sQ0FBYyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQTFCLEVBQXFDLFlBQXJDO01BQ1IsSUFBRyxPQUFPLENBQUMsUUFBUixLQUFvQixPQUF2QjtRQUNFLEtBQUEsR0FBUSxLQUFLLENBQUMsT0FBTixDQUFjLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBMUIsRUFBdUMsR0FBdkM7UUFDUixLQUFBLEdBQVEsS0FBSyxDQUFDLE9BQU4sQ0FBYyxJQUFJLENBQUMsR0FBbkIsRUFBd0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFuQyxFQUZWO09BQUEsTUFBQTtRQUlFLEtBQUEsR0FBUSxLQUFLLENBQUMsT0FBTixDQUFjLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBMUIsRUFBZ0MsR0FBaEMsRUFKVjs7TUFNQSxJQUFHLEtBQUssQ0FBQyxNQUFOLEdBQWUsZ0JBQWxCO1FBQ0UsS0FBQSxHQUFRLEtBQUssQ0FBQyxTQUFOLENBQWdCLENBQWhCLEVBQW1CLGdCQUFBLEdBQW1CLENBQXRDLENBQUEsR0FBMkMsTUFEckQ7O2FBRUEsS0FBSyxDQUFDLE9BQU4sQ0FBYyxXQUFkLEVBQTJCLEVBQTNCO0lBWGE7O2dDQWFmLFlBQUEsR0FBYyxTQUFBO2FBQ1osSUFBSSxPQUFKLENBQVksQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLE9BQUQsRUFBVSxNQUFWO0FBQ1YsY0FBQTtVQUFBLElBQThCLEtBQUMsQ0FBQSxTQUEvQjtBQUFBLG1CQUFPLE9BQUEsQ0FBUSxLQUFDLENBQUEsU0FBVCxFQUFQOztVQUNBLGFBQUEsR0FBZ0IsYUFBYSxDQUFDLFlBQWQsQ0FBQTtVQUNoQixzQkFBQSxHQUF5QixhQUFhLENBQUMsa0JBQWQsQ0FBQTtpQkFFekIsT0FBTyxDQUFDLEdBQVIsQ0FBWSxDQUFDLGFBQUQsRUFBZ0Isc0JBQWhCLENBQVosQ0FBb0QsQ0FBQyxJQUFyRCxDQUEwRCxTQUFDLEdBQUQ7QUFDeEQsZ0JBQUE7WUFBQyxtQkFBRCxFQUFhO1lBRWIsT0FBQSxHQUFVLEtBQUMsQ0FBQSxZQUFZLENBQUMsVUFBZCxDQUFBO1lBQ1YsT0FBQSxHQUFVLEtBQUMsQ0FBQSxZQUFZLENBQUMsVUFBZCxDQUFBO1lBQ1YsT0FBQSxHQUFVLEtBQUMsQ0FBQSxVQUFELENBQUE7WUFDVixXQUFBLEdBQWMsS0FBQyxDQUFBLGNBQUQsQ0FBQTtZQUNkLElBQW1GLG1CQUFuRjtjQUFBLGNBQUEscUdBQXNFLENBQUUsMEJBQXhFOztZQUNBLFFBQUEsR0FBVztZQUNYLFVBQUEsR0FBYSxPQUFPLENBQUMsR0FBRyxDQUFDO1lBQ3pCLGNBQUEsR0FBaUI7WUFFakIsSUFBRyxVQUFBLEtBQWMsTUFBakI7Y0FDRSxjQUFBLEdBQWlCLGlCQURuQjs7WUFHQSxJQUFHLHFCQUFBLElBQWlCLGlCQUFwQjtjQUNFLGNBQUEsR0FBaUIsR0FBQSxHQUFJLFdBQUosR0FBZ0IsSUFBaEIsR0FBb0IsT0FBcEIsR0FBNEIsWUFBNUIsR0FBd0MsZUFEM0Q7YUFBQSxNQUVLLElBQUcsbUJBQUg7Y0FDSCxjQUFBLEdBQWlCLEdBQUEsR0FBSSxXQUFKLEdBQWdCLGFBQWhCLEdBQTZCLGVBRDNDO2FBQUEsTUFBQTtjQUdILGNBQUEsR0FBaUIsWUFIZDs7WUFLTCxLQUFDLENBQUEsU0FBRCxHQUFhLG0xQ0FBQSxHQW9DQSxDQUFDLElBQUksQ0FBQyxVQUFMLENBQUEsQ0FBRCxDQXBDQSxHQW9DbUIsR0FwQ25CLEdBb0NzQixPQUFPLENBQUMsSUFwQzlCLEdBb0NtQyxrQkFwQ25DLEdBcUNLLE9BQU8sQ0FBQyxRQUFRLENBQUMsUUFyQ3RCLEdBcUMrQixZQXJDL0IsR0FzQ0QsVUF0Q0MsR0FzQ1UscUJBdENWLEdBdUNRLGNBdkNSLEdBdUN1QixJQXZDdkIsR0F3Q1QsY0F4Q1MsR0F3Q00seUJBeENOLEdBNENULE9BNUNTLEdBNENELGNBNUNDLEdBK0NOLE9BQU8sQ0FBQyxNQS9DRixHQStDUyxNQS9DVCxHQWlEVixDQUFDLEtBQUMsQ0FBQSxvQkFBRCxDQUFzQixPQUFPLENBQUMsS0FBOUIsQ0FBRCxDQWpEVSxHQWlENEIsMkJBakQ1QixHQXNEVixDQUFDLGFBQWEsQ0FBQyxRQUFkLENBQUEsQ0FBd0IsQ0FBQyxPQUF6QixDQUFBLENBQUQsQ0F0RFUsR0FzRDBCLG9DQXREMUIsR0EyRFYsQ0FBQyxlQUFlLENBQUMsSUFBaEIsQ0FBcUIsSUFBckIsQ0FBRCxDQTNEVSxHQTJEa0IscUpBM0RsQixHQWlFVDttQkFFSixPQUFBLENBQVEsS0FBQyxDQUFBLFNBQVQ7VUF6RndELENBQTFEO1FBTFU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVo7SUFEWTs7Z0NBaUdkLG9CQUFBLEdBQXNCLFNBQUMsS0FBRDs2QkFDcEIsS0FBSyxDQUFFLE9BQVAsQ0FBZSwrQ0FBZixFQUFnRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsQ0FBRCxFQUFJLEVBQUosRUFBUSxFQUFSLEVBQVksRUFBWixFQUFnQixFQUFoQjtpQkFBdUIsRUFBQSxHQUFLLENBQUMsRUFBQSxJQUFNLEVBQVAsQ0FBTCxHQUNyRixLQUFDLENBQUEsYUFBRCxDQUFlLEVBQWYsQ0FEcUYsR0FDaEU7UUFEeUM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWhFO0lBRG9COztnQ0FJdEIsYUFBQSxHQUFlLFNBQUMsSUFBRDthQUNiLElBQUksQ0FBQyxPQUFMLENBQWEsVUFBYixFQUF5QixFQUF6QixDQUNJLENBQUMsT0FETCxDQUNhLE9BRGIsRUFDcUIsSUFEckIsQ0FFSSxDQUFDLE9BRkwsQ0FFYSxFQUFFLENBQUMsZ0JBQUgsQ0FBQSxDQUZiLEVBRW9DLEdBRnBDLENBR0ksQ0FBQyxPQUhMLENBR2EsS0FIYixFQUdvQixHQUhwQixDQUlJLENBQUMsT0FKTCxDQUlhLGdDQUpiLEVBSStDLElBSi9DO0lBRGE7O2dDQU9mLFVBQUEsR0FBWSxTQUFBO0FBQ1YsVUFBQTtNQUFBLFdBQUEsR0FBYyxJQUFDLENBQUEsY0FBRCxDQUFBO01BQ2QsSUFBYyxtQkFBZDtBQUFBLGVBQUE7O01BQ0EsSUFBQSxxR0FBNEQsQ0FBRTtNQUM5RCxPQUFBLDhEQUFzQjtNQUN0QixJQUFBLENBQU8sT0FBUDtRQUNFLElBQUcsV0FBQSxHQUFjLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWQsQ0FBaUMsV0FBakMsQ0FBakI7QUFDRTtZQUNFLElBQUEsOEZBQTBFLENBQUU7WUFDNUUsT0FBQSw4REFBc0IsS0FGeEI7V0FBQSxpQkFERjtTQURGOzsrQkFNQSxPQUFPLENBQUUsT0FBVCxDQUFpQixRQUFqQixFQUEyQixFQUEzQixDQUE4QixDQUFDLE9BQS9CLENBQXVDLFFBQXZDLEVBQWlELEVBQWpEO0lBWFU7O2dDQWFaLDBCQUFBLEdBQTRCLFNBQUMsUUFBRDtBQUMxQixVQUFBO01BQUEsSUFBQSxDQUFjLFFBQWQ7QUFBQSxlQUFBOztNQUVBLFdBQUEsNkVBQW9FLENBQUEsQ0FBQTtNQUNwRSxJQUFzQixXQUF0QjtBQUFBLGVBQU8sWUFBUDs7TUFFQSxXQUFBLCtFQUFvRSxDQUFBLENBQUE7TUFDcEUsSUFBc0IsV0FBdEI7QUFBQSxlQUFPLFlBQVA7O01BRUEsV0FBQSwwRUFBK0QsQ0FBQSxDQUFBO01BQy9ELElBQXNCLFdBQXRCO0FBQUEsZUFBTyxZQUFQOztNQUVBLFdBQUEsMEVBQStELENBQUEsQ0FBQTtNQUMvRCxJQUFzQixXQUF0QjtBQUFBLGVBQU8sWUFBUDs7SUFiMEI7O2dDQWU1QixjQUFBLEdBQWdCLFNBQUE7QUFDZCxVQUFBO01BQUEsT0FBQSxHQUFVLElBQUMsQ0FBQSxZQUFZLENBQUMsVUFBZCxDQUFBO01BRVYsSUFBOEIsMkJBQTlCO0FBQUEsZUFBTyxPQUFPLENBQUMsWUFBZjs7TUFDQSxJQUFBLENBQUEsQ0FBYyx1QkFBQSxJQUFrQix3QkFBaEMsQ0FBQTtBQUFBLGVBQUE7O01BRUEsWUFBQSxHQUFlLElBQUMsQ0FBQSw0QkFBRCxDQUFBO0FBQ2YsV0FBQSwyQkFBQTs7UUFDRSxJQUFHLFdBQVcsQ0FBQyxPQUFaLENBQW9CLElBQUksQ0FBQyxJQUFMLENBQVUsT0FBVixFQUFtQixLQUFuQixFQUEwQixVQUExQixDQUFwQixDQUFBLEdBQTZELENBQUMsQ0FBOUQsSUFBbUUsV0FBVyxDQUFDLE9BQVosQ0FBb0IsSUFBSSxDQUFDLElBQUwsQ0FBVSxPQUFWLEVBQW1CLFVBQW5CLENBQXBCLENBQUEsR0FBc0QsQ0FBQyxDQUE3SDtVQUNFLFlBQWEsQ0FBQSxXQUFBLENBQWIsR0FBNEIsRUFBRSxDQUFDLFlBQUgsQ0FBZ0IsV0FBaEIsRUFEOUI7O0FBREY7TUFJQSxjQUFBLEdBQWlCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxRQUFEO0FBQ2YsY0FBQTtVQUFBLFFBQUEsR0FBVywyQkFBMkIsQ0FBQyxJQUE1QixDQUFpQyxRQUFqQyxDQUEyQyxDQUFBLENBQUE7VUFHdEQsSUFBRyxLQUFBLEdBQVEsYUFBYSxDQUFDLElBQWQsQ0FBbUIsUUFBbkIsQ0FBWDtZQUNFLFFBQUEsR0FBVyxLQUFNLENBQUEsQ0FBQSxFQURuQjs7VUFHQSxRQUFBLEdBQVcsSUFBSSxDQUFDLFNBQUwsQ0FBZSxRQUFmO1VBRVgsSUFBRyxJQUFJLENBQUMsVUFBTCxDQUFnQixRQUFoQixDQUFIO0FBQ0UsaUJBQUEsd0JBQUE7O2NBQ0UsSUFBWSxRQUFBLEtBQVksU0FBeEI7QUFBQSx5QkFBQTs7Y0FDQSxXQUFBLEdBQWMsUUFBUSxDQUFDLE9BQVQsQ0FBaUIsSUFBSSxDQUFDLFNBQUwsQ0FBZSxXQUFBLEdBQWMsSUFBSSxDQUFDLEdBQWxDLENBQWpCLENBQUEsS0FBNEQ7Y0FDMUUsSUFBbUIsV0FBbkI7QUFBQSx1QkFBTyxTQUFQOztBQUhGLGFBREY7O2lCQUtBLEtBQUMsQ0FBQSwwQkFBRCxDQUE0QixRQUE1QjtRQWRlO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQTtNQWdCakIsSUFBRyx3QkFBQSxJQUFvQixDQUFBLFdBQUEsR0FBYyxjQUFBLENBQWUsT0FBTyxDQUFDLE1BQXZCLENBQWQsQ0FBdkI7QUFDRSxlQUFPLFlBRFQ7O01BR0EsSUFBRyxxQkFBSDtRQUNFLEtBQUEsR0FBUSxnQkFBZ0IsQ0FBQyxLQUFqQixDQUF1QixPQUFPLENBQUMsS0FBL0I7QUFDUixhQUFTLHFGQUFUO1VBQ0csT0FBUSxLQUFNLENBQUEsQ0FBQTtVQUdmLElBQUEsQ0FBYyxJQUFkO0FBQUEsbUJBQUE7O1VBQ0EsV0FBQSxHQUFjLGNBQUEsQ0FBZSxJQUFmO1VBQ2QsSUFBc0IsbUJBQXRCO0FBQUEsbUJBQU8sWUFBUDs7QUFORixTQUZGOztJQTlCYzs7Z0NBMENoQiw0QkFBQSxHQUE4QixTQUFBO0FBQzVCLFVBQUE7TUFBQSx5QkFBQSxHQUE0QjtBQUM1QjtBQUFBLFdBQUEscUNBQUE7O1FBQ0UseUJBQTBCLENBQUEsSUFBSSxDQUFDLElBQUwsQ0FBMUIsR0FBdUMsSUFBSSxDQUFDO0FBRDlDO2FBRUE7SUFKNEI7Ozs7O0FBNVBoQyIsInNvdXJjZXNDb250ZW50IjpbImZzID0gcmVxdWlyZSAnZnMtcGx1cydcbnBhdGggPSByZXF1aXJlICdwYXRoJ1xuU3RhY2tUcmFjZVBhcnNlciA9IHJlcXVpcmUgJ3N0YWNrdHJhY2UtcGFyc2VyJ1xuXG5Db21tYW5kTG9nZ2VyID0gcmVxdWlyZSAnLi9jb21tYW5kLWxvZ2dlcidcblVzZXJVdGlsaXRpZXMgPSByZXF1aXJlICcuL3VzZXItdXRpbGl0aWVzJ1xuXG5USVRMRV9DSEFSX0xJTUlUID0gMTAwICMgVHJ1bmNhdGUgaXNzdWUgdGl0bGUgdG8gMTAwIGNoYXJhY3RlcnMgKGluY2x1ZGluZyBlbGxpcHNpcylcblxuRmlsZVVSTFJlZ0V4cCA9IG5ldyBSZWdFeHAoJ2ZpbGU6Ly9cXHcqLyguKiknKVxuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBOb3RpZmljYXRpb25Jc3N1ZVxuICBjb25zdHJ1Y3RvcjogKEBub3RpZmljYXRpb24pIC0+XG5cbiAgZmluZFNpbWlsYXJJc3N1ZXM6IC0+XG4gICAgcmVwb1VybCA9IEBnZXRSZXBvVXJsKClcbiAgICByZXBvVXJsID0gJ2F0b20vYXRvbScgdW5sZXNzIHJlcG9Vcmw/XG4gICAgcmVwbyA9IHJlcG9VcmwucmVwbGFjZSAvaHR0cChzKT86XFwvXFwvKFxcZCtcXC4pP2dpdGh1Yi5jb21cXC8vZ2ksICcnXG4gICAgaXNzdWVUaXRsZSA9IEBnZXRJc3N1ZVRpdGxlKClcbiAgICBxdWVyeSA9IFwiI3tpc3N1ZVRpdGxlfSByZXBvOiN7cmVwb31cIlxuICAgIGdpdGh1YkhlYWRlcnMgPSBuZXcgSGVhZGVycyh7XG4gICAgICBhY2NlcHQ6ICdhcHBsaWNhdGlvbi92bmQuZ2l0aHViLnYzK2pzb24nXG4gICAgICBjb250ZW50VHlwZTogXCJhcHBsaWNhdGlvbi9qc29uXCJcbiAgICB9KVxuXG4gICAgZmV0Y2ggXCJodHRwczovL2FwaS5naXRodWIuY29tL3NlYXJjaC9pc3N1ZXM/cT0je2VuY29kZVVSSUNvbXBvbmVudChxdWVyeSl9JnNvcnQ9Y3JlYXRlZFwiLCB7aGVhZGVyczogZ2l0aHViSGVhZGVyc31cbiAgICAgIC50aGVuIChyKSAtPiByPy5qc29uKClcbiAgICAgIC50aGVuIChkYXRhKSAtPlxuICAgICAgICBpZiBkYXRhPy5pdGVtcz9cbiAgICAgICAgICBpc3N1ZXMgPSB7fVxuICAgICAgICAgIGZvciBpc3N1ZSBpbiBkYXRhLml0ZW1zXG4gICAgICAgICAgICBpZiBpc3N1ZS50aXRsZS5pbmRleE9mKGlzc3VlVGl0bGUpID4gLTEgYW5kIG5vdCBpc3N1ZXNbaXNzdWUuc3RhdGVdP1xuICAgICAgICAgICAgICBpc3N1ZXNbaXNzdWUuc3RhdGVdID0gaXNzdWVcbiAgICAgICAgICAgICAgcmV0dXJuIGlzc3VlcyBpZiBpc3N1ZXMub3Blbj8gYW5kIGlzc3Vlcy5jbG9zZWQ/XG5cbiAgICAgICAgICByZXR1cm4gaXNzdWVzIGlmIGlzc3Vlcy5vcGVuPyBvciBpc3N1ZXMuY2xvc2VkP1xuICAgICAgICBudWxsXG4gICAgICAuY2F0Y2ggKGUpIC0+IG51bGxcblxuICBnZXRJc3N1ZVVybEZvclN5c3RlbTogLT5cbiAgICAjIFdpbmRvd3Mgd2lsbCBub3QgbGF1bmNoIFVSTHMgZ3JlYXRlciB0aGFuIH4yMDAwIGJ5dGVzIHNvIHdlIG5lZWQgdG8gc2hyaW5rIGl0XG4gICAgIyBBbHNvIGlzLmdkIGhhcyBhIGxpbWl0IG9mIDUwMDAgYnl0ZXMuLi5cbiAgICBAZ2V0SXNzdWVVcmwoKS50aGVuIChpc3N1ZVVybCkgLT5cbiAgICAgIGZldGNoIFwiaHR0cHM6Ly9pcy5nZC9jcmVhdGUucGhwP2Zvcm1hdD1zaW1wbGVcIiwge1xuICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgaGVhZGVyczogeydDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJ30sXG4gICAgICAgIGJvZHk6IFwidXJsPSN7ZW5jb2RlVVJJQ29tcG9uZW50KGlzc3VlVXJsKX1cIlxuICAgICAgfVxuICAgICAgLnRoZW4gKHIpIC0+IHIudGV4dCgpXG4gICAgICAuY2F0Y2ggKGUpIC0+IG51bGxcblxuICBnZXRJc3N1ZVVybDogLT5cbiAgICBAZ2V0SXNzdWVCb2R5KCkudGhlbiAoaXNzdWVCb2R5KSA9PlxuICAgICAgcmVwb1VybCA9IEBnZXRSZXBvVXJsKClcbiAgICAgIHJlcG9VcmwgPSAnaHR0cHM6Ly9naXRodWIuY29tL2F0b20vYXRvbScgdW5sZXNzIHJlcG9Vcmw/XG4gICAgICBcIiN7cmVwb1VybH0vaXNzdWVzL25ldz90aXRsZT0je0BlbmNvZGVVUkkoQGdldElzc3VlVGl0bGUoKSl9JmJvZHk9I3tAZW5jb2RlVVJJKGlzc3VlQm9keSl9XCJcblxuICBlbmNvZGVVUkk6IChzdHIpIC0+XG4gICAgZW5jb2RlVVJJKHN0cikucmVwbGFjZSgvIy9nLCAnJTIzJykucmVwbGFjZSgvOy9nLCAnJTNCJykucmVwbGFjZSgvJTIwL2csICcrJylcblxuICBnZXRJc3N1ZVRpdGxlOiAtPlxuICAgIHRpdGxlID0gQG5vdGlmaWNhdGlvbi5nZXRNZXNzYWdlKClcbiAgICB0aXRsZSA9IHRpdGxlLnJlcGxhY2UocHJvY2Vzcy5lbnYuQVRPTV9IT01FLCAnJEFUT01fSE9NRScpXG4gICAgaWYgcHJvY2Vzcy5wbGF0Zm9ybSBpcyAnd2luMzInXG4gICAgICB0aXRsZSA9IHRpdGxlLnJlcGxhY2UocHJvY2Vzcy5lbnYuVVNFUlBST0ZJTEUsICd+JylcbiAgICAgIHRpdGxlID0gdGl0bGUucmVwbGFjZShwYXRoLnNlcCwgcGF0aC5wb3NpeC5zZXApICMgU3RhbmRhcmRpemUgaXNzdWUgdGl0bGVzXG4gICAgZWxzZVxuICAgICAgdGl0bGUgPSB0aXRsZS5yZXBsYWNlKHByb2Nlc3MuZW52LkhPTUUsICd+JylcblxuICAgIGlmIHRpdGxlLmxlbmd0aCA+IFRJVExFX0NIQVJfTElNSVRcbiAgICAgIHRpdGxlID0gdGl0bGUuc3Vic3RyaW5nKDAsIFRJVExFX0NIQVJfTElNSVQgLSAzKSArICcuLi4nXG4gICAgdGl0bGUucmVwbGFjZSgvXFxyP1xcbnxcXHIvZywgXCJcIilcblxuICBnZXRJc3N1ZUJvZHk6IC0+XG4gICAgbmV3IFByb21pc2UgKHJlc29sdmUsIHJlamVjdCkgPT5cbiAgICAgIHJldHVybiByZXNvbHZlKEBpc3N1ZUJvZHkpIGlmIEBpc3N1ZUJvZHlcbiAgICAgIHN5c3RlbVByb21pc2UgPSBVc2VyVXRpbGl0aWVzLmdldE9TVmVyc2lvbigpXG4gICAgICBub25Db3JlUGFja2FnZXNQcm9taXNlID0gVXNlclV0aWxpdGllcy5nZXROb25Db3JlUGFja2FnZXMoKVxuXG4gICAgICBQcm9taXNlLmFsbChbc3lzdGVtUHJvbWlzZSwgbm9uQ29yZVBhY2thZ2VzUHJvbWlzZV0pLnRoZW4gKGFsbCkgPT5cbiAgICAgICAgW3N5c3RlbU5hbWUsIG5vbkNvcmVQYWNrYWdlc10gPSBhbGxcblxuICAgICAgICBtZXNzYWdlID0gQG5vdGlmaWNhdGlvbi5nZXRNZXNzYWdlKClcbiAgICAgICAgb3B0aW9ucyA9IEBub3RpZmljYXRpb24uZ2V0T3B0aW9ucygpXG4gICAgICAgIHJlcG9VcmwgPSBAZ2V0UmVwb1VybCgpXG4gICAgICAgIHBhY2thZ2VOYW1lID0gQGdldFBhY2thZ2VOYW1lKClcbiAgICAgICAgcGFja2FnZVZlcnNpb24gPSBhdG9tLnBhY2thZ2VzLmdldExvYWRlZFBhY2thZ2UocGFja2FnZU5hbWUpPy5tZXRhZGF0YT8udmVyc2lvbiBpZiBwYWNrYWdlTmFtZT9cbiAgICAgICAgY29weVRleHQgPSAnJ1xuICAgICAgICBzeXN0ZW1Vc2VyID0gcHJvY2Vzcy5lbnYuVVNFUlxuICAgICAgICByb290VXNlclN0YXR1cyA9ICcnXG5cbiAgICAgICAgaWYgc3lzdGVtVXNlciBpcyAncm9vdCdcbiAgICAgICAgICByb290VXNlclN0YXR1cyA9ICcqKlVzZXIqKjogcm9vdCdcblxuICAgICAgICBpZiBwYWNrYWdlTmFtZT8gYW5kIHJlcG9Vcmw/XG4gICAgICAgICAgcGFja2FnZU1lc3NhZ2UgPSBcIlsje3BhY2thZ2VOYW1lfV0oI3tyZXBvVXJsfSkgcGFja2FnZSAje3BhY2thZ2VWZXJzaW9ufVwiXG4gICAgICAgIGVsc2UgaWYgcGFja2FnZU5hbWU/XG4gICAgICAgICAgcGFja2FnZU1lc3NhZ2UgPSBcIicje3BhY2thZ2VOYW1lfScgcGFja2FnZSB2I3twYWNrYWdlVmVyc2lvbn1cIlxuICAgICAgICBlbHNlXG4gICAgICAgICAgcGFja2FnZU1lc3NhZ2UgPSAnQXRvbSBDb3JlJ1xuXG4gICAgICAgIEBpc3N1ZUJvZHkgPSBcIlwiXCJcbiAgICAgICAgICA8IS0tXG4gICAgICAgICAgSGF2ZSB5b3UgcmVhZCBBdG9tJ3MgQ29kZSBvZiBDb25kdWN0PyBCeSBmaWxpbmcgYW4gSXNzdWUsIHlvdSBhcmUgZXhwZWN0ZWQgdG8gY29tcGx5IHdpdGggaXQsIGluY2x1ZGluZyB0cmVhdGluZyBldmVyeW9uZSB3aXRoIHJlc3BlY3Q6IGh0dHBzOi8vZ2l0aHViLmNvbS9hdG9tLy5naXRodWIvYmxvYi9tYXN0ZXIvQ09ERV9PRl9DT05EVUNULm1kXG5cbiAgICAgICAgICBEbyB5b3Ugd2FudCB0byBhc2sgYSBxdWVzdGlvbj8gQXJlIHlvdSBsb29raW5nIGZvciBzdXBwb3J0PyBUaGUgQXRvbSBtZXNzYWdlIGJvYXJkIGlzIHRoZSBiZXN0IHBsYWNlIGZvciBnZXR0aW5nIHN1cHBvcnQ6IGh0dHBzOi8vZGlzY3Vzcy5hdG9tLmlvXG4gICAgICAgICAgLS0+XG5cbiAgICAgICAgICAjIyMgUHJlcmVxdWlzaXRlc1xuXG4gICAgICAgICAgKiBbIF0gUHV0IGFuIFggYmV0d2VlbiB0aGUgYnJhY2tldHMgb24gdGhpcyBsaW5lIGlmIHlvdSBoYXZlIGRvbmUgYWxsIG9mIHRoZSBmb2xsb3dpbmc6XG4gICAgICAgICAgICAgICogUmVwcm9kdWNlZCB0aGUgcHJvYmxlbSBpbiBTYWZlIE1vZGU6IDxodHRwczovL2ZsaWdodC1tYW51YWwuYXRvbS5pby9oYWNraW5nLWF0b20vc2VjdGlvbnMvZGVidWdnaW5nLyN1c2luZy1zYWZlLW1vZGU+XG4gICAgICAgICAgICAgICogRm9sbG93ZWQgYWxsIGFwcGxpY2FibGUgc3RlcHMgaW4gdGhlIGRlYnVnZ2luZyBndWlkZTogPGh0dHBzOi8vZmxpZ2h0LW1hbnVhbC5hdG9tLmlvL2hhY2tpbmctYXRvbS9zZWN0aW9ucy9kZWJ1Z2dpbmcvPlxuICAgICAgICAgICAgICAqIENoZWNrZWQgdGhlIEZBUXMgb24gdGhlIG1lc3NhZ2UgYm9hcmQgZm9yIGNvbW1vbiBzb2x1dGlvbnM6IDxodHRwczovL2Rpc2N1c3MuYXRvbS5pby9jL2ZhcT5cbiAgICAgICAgICAgICAgKiBDaGVja2VkIHRoYXQgeW91ciBpc3N1ZSBpc24ndCBhbHJlYWR5IGZpbGVkOiA8aHR0cHM6Ly9naXRodWIuY29tL2lzc3Vlcz9xPWlzJTNBaXNzdWUrdXNlciUzQWF0b20+XG4gICAgICAgICAgICAgICogQ2hlY2tlZCB0aGF0IHRoZXJlIGlzIG5vdCBhbHJlYWR5IGFuIEF0b20gcGFja2FnZSB0aGF0IHByb3ZpZGVzIHRoZSBkZXNjcmliZWQgZnVuY3Rpb25hbGl0eTogPGh0dHBzOi8vYXRvbS5pby9wYWNrYWdlcz5cblxuICAgICAgICAgICMjIyBEZXNjcmlwdGlvblxuXG4gICAgICAgICAgPCEtLSBEZXNjcmlwdGlvbiBvZiB0aGUgaXNzdWUgLS0+XG5cbiAgICAgICAgICAjIyMgU3RlcHMgdG8gUmVwcm9kdWNlXG5cbiAgICAgICAgICAxLiA8IS0tIEZpcnN0IFN0ZXAgLS0+XG4gICAgICAgICAgMi4gPCEtLSBTZWNvbmQgU3RlcCAtLT5cbiAgICAgICAgICAzLiA8IS0tIGFuZCBzbyBvbuKApiAtLT5cblxuICAgICAgICAgICoqRXhwZWN0ZWQgYmVoYXZpb3I6KipcblxuICAgICAgICAgIDwhLS0gV2hhdCB5b3UgZXhwZWN0IHRvIGhhcHBlbiAtLT5cblxuICAgICAgICAgICoqQWN0dWFsIGJlaGF2aW9yOioqXG5cbiAgICAgICAgICA8IS0tIFdoYXQgYWN0dWFsbHkgaGFwcGVucyAtLT5cblxuICAgICAgICAgICMjIyBWZXJzaW9uc1xuXG4gICAgICAgICAgKipBdG9tKio6ICN7YXRvbS5nZXRWZXJzaW9uKCl9ICN7cHJvY2Vzcy5hcmNofVxuICAgICAgICAgICoqRWxlY3Ryb24qKjogI3twcm9jZXNzLnZlcnNpb25zLmVsZWN0cm9ufVxuICAgICAgICAgICoqT1MqKjogI3tzeXN0ZW1OYW1lfVxuICAgICAgICAgICoqVGhyb3duIEZyb20qKjogI3twYWNrYWdlTWVzc2FnZX1cbiAgICAgICAgICAje3Jvb3RVc2VyU3RhdHVzfVxuXG4gICAgICAgICAgIyMjIFN0YWNrIFRyYWNlXG5cbiAgICAgICAgICAje21lc3NhZ2V9XG5cbiAgICAgICAgICBgYGBcbiAgICAgICAgICBBdCAje29wdGlvbnMuZGV0YWlsfVxuXG4gICAgICAgICAgI3tAbm9ybWFsaXplZFN0YWNrUGF0aHMob3B0aW9ucy5zdGFjayl9XG4gICAgICAgICAgYGBgXG5cbiAgICAgICAgICAjIyMgQ29tbWFuZHNcblxuICAgICAgICAgICN7Q29tbWFuZExvZ2dlci5pbnN0YW5jZSgpLmdldFRleHQoKX1cblxuICAgICAgICAgICMjIyBOb24tQ29yZSBQYWNrYWdlc1xuXG4gICAgICAgICAgYGBgXG4gICAgICAgICAgI3tub25Db3JlUGFja2FnZXMuam9pbignXFxuJyl9XG4gICAgICAgICAgYGBgXG5cbiAgICAgICAgICAjIyMgQWRkaXRpb25hbCBJbmZvcm1hdGlvblxuXG4gICAgICAgICAgPCEtLSBBbnkgYWRkaXRpb25hbCBpbmZvcm1hdGlvbiwgY29uZmlndXJhdGlvbiBvciBkYXRhIHRoYXQgbWlnaHQgYmUgbmVjZXNzYXJ5IHRvIHJlcHJvZHVjZSB0aGUgaXNzdWUuIC0tPlxuICAgICAgICAgICN7Y29weVRleHR9XG4gICAgICAgIFwiXCJcIlxuICAgICAgICByZXNvbHZlKEBpc3N1ZUJvZHkpXG5cbiAgbm9ybWFsaXplZFN0YWNrUGF0aHM6IChzdGFjaykgPT5cbiAgICBzdGFjaz8ucmVwbGFjZSAvKF5cXFcrYXQgKShbXFx3Ll17Mix9IFsoXSk/KC4qKSg6XFxkKzpcXGQrWyldPykvZ20sIChtLCBwMSwgcDIsIHAzLCBwNCkgPT4gcDEgKyAocDIgb3IgJycpICtcbiAgICAgIEBub3JtYWxpemVQYXRoKHAzKSArIHA0XG5cbiAgbm9ybWFsaXplUGF0aDogKHBhdGgpIC0+XG4gICAgcGF0aC5yZXBsYWNlKCdmaWxlOi8vLycsICcnKSAgICAgICAgICAgICAgICAgICAgICAgICAjIFJhbmRvbWx5IGluc2VydGVkIGZpbGUgdXJsIHByb3RvY29sc1xuICAgICAgICAucmVwbGFjZSgvWy9dL2csICdcXFxcJykgICAgICAgICAgICAgICAgICAgICAgICAgICAjIFRlbXAgc3dpdGNoIGZvciBXaW5kb3dzIGhvbWUgbWF0Y2hpbmdcbiAgICAgICAgLnJlcGxhY2UoZnMuZ2V0SG9tZURpcmVjdG9yeSgpLCAnficpICAgICAgICAgICAgICMgUmVtb3ZlIHVzZXJzIGhvbWUgZGlyIGZvciBhcG0tZGV2J2VkIHBhY2thZ2VzXG4gICAgICAgIC5yZXBsYWNlKC9cXFxcL2csICcvJykgICAgICAgICAgICAgICAgICAgICAgICAgICAgICMgU3dpdGNoIFxcIGJhY2sgdG8gLyBmb3IgZXZlcnlvbmVcbiAgICAgICAgLnJlcGxhY2UoLy4qKFxcLyhhcHBcXC5hc2FyfHBhY2thZ2VzXFwvKS4qKS8sICckMScpICMgUmVtb3ZlIGV2ZXJ5dGhpbmcgYmVmb3JlIGFwcC5hc2FyIG9yIHBhY2FrZ2VzXG5cbiAgZ2V0UmVwb1VybDogLT5cbiAgICBwYWNrYWdlTmFtZSA9IEBnZXRQYWNrYWdlTmFtZSgpXG4gICAgcmV0dXJuIHVubGVzcyBwYWNrYWdlTmFtZT9cbiAgICByZXBvID0gYXRvbS5wYWNrYWdlcy5nZXRMb2FkZWRQYWNrYWdlKHBhY2thZ2VOYW1lKT8ubWV0YWRhdGE/LnJlcG9zaXRvcnlcbiAgICByZXBvVXJsID0gcmVwbz8udXJsID8gcmVwb1xuICAgIHVubGVzcyByZXBvVXJsXG4gICAgICBpZiBwYWNrYWdlUGF0aCA9IGF0b20ucGFja2FnZXMucmVzb2x2ZVBhY2thZ2VQYXRoKHBhY2thZ2VOYW1lKVxuICAgICAgICB0cnlcbiAgICAgICAgICByZXBvID0gSlNPTi5wYXJzZShmcy5yZWFkRmlsZVN5bmMocGF0aC5qb2luKHBhY2thZ2VQYXRoLCAncGFja2FnZS5qc29uJykpKT8ucmVwb3NpdG9yeVxuICAgICAgICAgIHJlcG9VcmwgPSByZXBvPy51cmwgPyByZXBvXG5cbiAgICByZXBvVXJsPy5yZXBsYWNlKC9cXC5naXQkLywgJycpLnJlcGxhY2UoL15naXRcXCsvLCAnJylcblxuICBnZXRQYWNrYWdlTmFtZUZyb21GaWxlUGF0aDogKGZpbGVQYXRoKSAtPlxuICAgIHJldHVybiB1bmxlc3MgZmlsZVBhdGhcblxuICAgIHBhY2thZ2VOYW1lID0gL1xcL1xcLmF0b21cXC9kZXZcXC9wYWNrYWdlc1xcLyhbXlxcL10rKVxcLy8uZXhlYyhmaWxlUGF0aCk/WzFdXG4gICAgcmV0dXJuIHBhY2thZ2VOYW1lIGlmIHBhY2thZ2VOYW1lXG5cbiAgICBwYWNrYWdlTmFtZSA9IC9cXFxcXFwuYXRvbVxcXFxkZXZcXFxccGFja2FnZXNcXFxcKFteXFxcXF0rKVxcXFwvLmV4ZWMoZmlsZVBhdGgpP1sxXVxuICAgIHJldHVybiBwYWNrYWdlTmFtZSBpZiBwYWNrYWdlTmFtZVxuXG4gICAgcGFja2FnZU5hbWUgPSAvXFwvXFwuYXRvbVxcL3BhY2thZ2VzXFwvKFteXFwvXSspXFwvLy5leGVjKGZpbGVQYXRoKT9bMV1cbiAgICByZXR1cm4gcGFja2FnZU5hbWUgaWYgcGFja2FnZU5hbWVcblxuICAgIHBhY2thZ2VOYW1lID0gL1xcXFxcXC5hdG9tXFxcXHBhY2thZ2VzXFxcXChbXlxcXFxdKylcXFxcLy5leGVjKGZpbGVQYXRoKT9bMV1cbiAgICByZXR1cm4gcGFja2FnZU5hbWUgaWYgcGFja2FnZU5hbWVcblxuICBnZXRQYWNrYWdlTmFtZTogLT5cbiAgICBvcHRpb25zID0gQG5vdGlmaWNhdGlvbi5nZXRPcHRpb25zKClcblxuICAgIHJldHVybiBvcHRpb25zLnBhY2thZ2VOYW1lIGlmIG9wdGlvbnMucGFja2FnZU5hbWU/XG4gICAgcmV0dXJuIHVubGVzcyBvcHRpb25zLnN0YWNrPyBvciBvcHRpb25zLmRldGFpbD9cblxuICAgIHBhY2thZ2VQYXRocyA9IEBnZXRQYWNrYWdlUGF0aHNCeVBhY2thZ2VOYW1lKClcbiAgICBmb3IgcGFja2FnZU5hbWUsIHBhY2thZ2VQYXRoIG9mIHBhY2thZ2VQYXRoc1xuICAgICAgaWYgcGFja2FnZVBhdGguaW5kZXhPZihwYXRoLmpvaW4oJy5hdG9tJywgJ2RldicsICdwYWNrYWdlcycpKSA+IC0xIG9yIHBhY2thZ2VQYXRoLmluZGV4T2YocGF0aC5qb2luKCcuYXRvbScsICdwYWNrYWdlcycpKSA+IC0xXG4gICAgICAgIHBhY2thZ2VQYXRoc1twYWNrYWdlTmFtZV0gPSBmcy5yZWFscGF0aFN5bmMocGFja2FnZVBhdGgpXG5cbiAgICBnZXRQYWNrYWdlTmFtZSA9IChmaWxlUGF0aCkgPT5cbiAgICAgIGZpbGVQYXRoID0gL1xcKCguKz8pOlxcZCt8XFwoKC4rKVxcKXwoLispLy5leGVjKGZpbGVQYXRoKVswXVxuXG4gICAgICAjIFN0YWNrIHRyYWNlcyBtYXkgYmUgYSBmaWxlIFVSSVxuICAgICAgaWYgbWF0Y2ggPSBGaWxlVVJMUmVnRXhwLmV4ZWMoZmlsZVBhdGgpXG4gICAgICAgIGZpbGVQYXRoID0gbWF0Y2hbMV1cblxuICAgICAgZmlsZVBhdGggPSBwYXRoLm5vcm1hbGl6ZShmaWxlUGF0aClcblxuICAgICAgaWYgcGF0aC5pc0Fic29sdXRlKGZpbGVQYXRoKVxuICAgICAgICBmb3IgcGFja05hbWUsIHBhY2thZ2VQYXRoIG9mIHBhY2thZ2VQYXRoc1xuICAgICAgICAgIGNvbnRpbnVlIGlmIGZpbGVQYXRoIGlzICdub2RlLmpzJ1xuICAgICAgICAgIGlzU3ViZm9sZGVyID0gZmlsZVBhdGguaW5kZXhPZihwYXRoLm5vcm1hbGl6ZShwYWNrYWdlUGF0aCArIHBhdGguc2VwKSkgaXMgMFxuICAgICAgICAgIHJldHVybiBwYWNrTmFtZSBpZiBpc1N1YmZvbGRlclxuICAgICAgQGdldFBhY2thZ2VOYW1lRnJvbUZpbGVQYXRoKGZpbGVQYXRoKVxuXG4gICAgaWYgb3B0aW9ucy5kZXRhaWw/IGFuZCBwYWNrYWdlTmFtZSA9IGdldFBhY2thZ2VOYW1lKG9wdGlvbnMuZGV0YWlsKVxuICAgICAgcmV0dXJuIHBhY2thZ2VOYW1lXG5cbiAgICBpZiBvcHRpb25zLnN0YWNrP1xuICAgICAgc3RhY2sgPSBTdGFja1RyYWNlUGFyc2VyLnBhcnNlKG9wdGlvbnMuc3RhY2spXG4gICAgICBmb3IgaSBpbiBbMC4uLnN0YWNrLmxlbmd0aF1cbiAgICAgICAge2ZpbGV9ID0gc3RhY2tbaV1cblxuICAgICAgICAjIEVtcHR5IHdoZW4gaXQgd2FzIHJ1biBmcm9tIHRoZSBkZXYgY29uc29sZVxuICAgICAgICByZXR1cm4gdW5sZXNzIGZpbGVcbiAgICAgICAgcGFja2FnZU5hbWUgPSBnZXRQYWNrYWdlTmFtZShmaWxlKVxuICAgICAgICByZXR1cm4gcGFja2FnZU5hbWUgaWYgcGFja2FnZU5hbWU/XG5cbiAgICByZXR1cm5cblxuICBnZXRQYWNrYWdlUGF0aHNCeVBhY2thZ2VOYW1lOiAtPlxuICAgIHBhY2thZ2VQYXRoc0J5UGFja2FnZU5hbWUgPSB7fVxuICAgIGZvciBwYWNrIGluIGF0b20ucGFja2FnZXMuZ2V0TG9hZGVkUGFja2FnZXMoKVxuICAgICAgcGFja2FnZVBhdGhzQnlQYWNrYWdlTmFtZVtwYWNrLm5hbWVdID0gcGFjay5wYXRoXG4gICAgcGFja2FnZVBhdGhzQnlQYWNrYWdlTmFtZVxuIl19
