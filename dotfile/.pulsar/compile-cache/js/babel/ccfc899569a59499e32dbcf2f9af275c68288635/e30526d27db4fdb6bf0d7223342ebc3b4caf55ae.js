"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _etch = _interopRequireDefault(require("etch"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
/** @babel */
/** @jsx etch.dom */

class ChangeLogView {
  constructor(props) {
    this.props = props;
    _etch.default.initialize(this);
  }
  didChangeShowChangeLog() {
    atom.config.set('welcome.showChangeLog', this.checked);
  }
  dismissVersion() {
    atom.config.set('welcome.lastViewedChangeLog', atom.getVersion().split(" ")[0]);
  }
  wasVersionDismissed() {
    const lastVersion = atom.config.get('welcome.lastViewedChangeLog');
    const curVersion = atom.getVersion().split(".");
    if (lastVersion[0] < curVersion[0] && lastVersion[1] < curVersion[1] && lastVersion[2].split(" ")[0] < curVersion[2].split(" ")[0]) {
      return false;
    } else {
      return true;
    }
  }
  update() {}
  serialize() {
    return {
      deserializer: 'ChangeLogView',
      uri: this.props.uri
    };
  }
  render() {
    return _etch.default.dom("div", {
      className: "welcome"
    }, _etch.default.dom("div", {
      className: "welcome-container"
    }, _etch.default.dom("div", {
      className: "header"
    }, _etch.default.dom("a", {
      title: "Full Change Log",
      href: "https://github.com/pulsar-edit/pulsar/blob/master/CHANGELOG.md"
    }, _etch.default.dom("h1", {
      className: "welcome-title"
    }, "Change Log"))), _etch.default.dom("div", {
      className: "welcome-panel"
    }, _etch.default.dom("p", null, "Take a look at some of the awesome things ", atom.branding.name, " has changed:"), _etch.default.dom("p", null, "Feel free to read our ", _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/blob/master/CHANGELOG.md"
    }, "Full Change Log"), "."), _etch.default.dom("ul", null, _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/28"
    }, "Bump to Electron 12 and Node 14")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/7"
    }, "Added a rebranding API")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/67"
    }, "Removed experimental file watchers on the editor and fixes for how the Config File is watched.")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/131"
    }, "Ability to install packages from Git Repositories")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/68"
    }, "Migrated to a new Pulsar Package Repository Backend")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/settings-view/pull/2"
    }, "Better error messages when a package fails to install.")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/14"
    }, "Bumped Tree-Sitter to 0.20.1 and all grammars to their recent versions")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/101"
    }, "Native support for ARM Linux")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/54"
    }, "Native Support for Apple Silicon")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/105"
    }, "Removed Benchmark Mode")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/40"
    }, "Removed all telemetry from the editor.")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://pulsar-edit.dev"
    }, "New Pulsar Website")), _etch.default.dom("li", null, _etch.default.dom("a", {
      href: "https://github.com/pulsar-edit/pulsar/pull/186"
    }, "Apple Silicon support for `github` Package v0.36.13"))), _etch.default.dom("section", {
      className: "welcome-panel"
    }, _etch.default.dom("label", null, _etch.default.dom("input", {
      className: "input-checkbox",
      type: "checkbox",
      checked: atom.config.get('welcome.showChangeLog'),
      onchange: this.didChangeShowChangeLog
    }), "Show the Change Log after an update.")), _etch.default.dom("section", {
      className: "welcome-panel"
    }, _etch.default.dom("label", null, _etch.default.dom("input", {
      className: "input-checkbox",
      type: "checkbox",
      checked: this.wasVersionDismissed(),
      onchange: this.dismissVersion
    }), "Dismiss this Change Log")))));
  }
  getURI() {
    return this.props.uri;
  }
  getTitle() {
    return 'Change Log';
  }
  isEqual(other) {
    return other instanceof ChangeLogView;
  }
}
exports.default = ChangeLogView;
module.exports = exports.default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJDaGFuZ2VMb2dWaWV3IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsImV0Y2giLCJpbml0aWFsaXplIiwiZGlkQ2hhbmdlU2hvd0NoYW5nZUxvZyIsImF0b20iLCJjb25maWciLCJzZXQiLCJjaGVja2VkIiwiZGlzbWlzc1ZlcnNpb24iLCJnZXRWZXJzaW9uIiwic3BsaXQiLCJ3YXNWZXJzaW9uRGlzbWlzc2VkIiwibGFzdFZlcnNpb24iLCJnZXQiLCJjdXJWZXJzaW9uIiwidXBkYXRlIiwic2VyaWFsaXplIiwiZGVzZXJpYWxpemVyIiwidXJpIiwicmVuZGVyIiwiYnJhbmRpbmciLCJuYW1lIiwiZ2V0VVJJIiwiZ2V0VGl0bGUiLCJpc0VxdWFsIiwib3RoZXIiXSwic291cmNlcyI6WyJjaGFuZ2Vsb2ctdmlldy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKiogQGJhYmVsICovXG4vKiogQGpzeCBldGNoLmRvbSAqL1xuXG5pbXBvcnQgZXRjaCBmcm9tICdldGNoJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2hhbmdlTG9nVmlldyB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgdGhpcy5wcm9wcyA9IHByb3BzO1xuICAgIGV0Y2guaW5pdGlhbGl6ZSh0aGlzKTtcbiAgfVxuXG4gIGRpZENoYW5nZVNob3dDaGFuZ2VMb2coKSB7XG4gICAgYXRvbS5jb25maWcuc2V0KCd3ZWxjb21lLnNob3dDaGFuZ2VMb2cnLCB0aGlzLmNoZWNrZWQpO1xuICB9XG5cbiAgZGlzbWlzc1ZlcnNpb24oKSB7XG4gICAgYXRvbS5jb25maWcuc2V0KCd3ZWxjb21lLmxhc3RWaWV3ZWRDaGFuZ2VMb2cnLCBhdG9tLmdldFZlcnNpb24oKS5zcGxpdChcIiBcIilbMF0pO1xuICB9XG5cbiAgd2FzVmVyc2lvbkRpc21pc3NlZCgpIHtcbiAgICBjb25zdCBsYXN0VmVyc2lvbiA9IGF0b20uY29uZmlnLmdldCgnd2VsY29tZS5sYXN0Vmlld2VkQ2hhbmdlTG9nJyk7XG4gICAgY29uc3QgY3VyVmVyc2lvbiA9IGF0b20uZ2V0VmVyc2lvbigpLnNwbGl0KFwiLlwiKTtcbiAgICBpZiAobGFzdFZlcnNpb25bMF0gPCBjdXJWZXJzaW9uWzBdICYmIGxhc3RWZXJzaW9uWzFdIDwgY3VyVmVyc2lvblsxXSAmJiBsYXN0VmVyc2lvblsyXS5zcGxpdChcIiBcIilbMF0gPCBjdXJWZXJzaW9uWzJdLnNwbGl0KFwiIFwiKVswXSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gIH1cblxuICB1cGRhdGUoKSB7fVxuXG4gIHNlcmlhbGl6ZSgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgZGVzZXJpYWxpemVyOiAnQ2hhbmdlTG9nVmlldycsXG4gICAgICB1cmk6IHRoaXMucHJvcHMudXJpXG4gICAgfTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3ZWxjb21lXCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwid2VsY29tZS1jb250YWluZXJcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWRlclwiPlxuICAgICAgICAgICAgPGEgdGl0bGU9XCJGdWxsIENoYW5nZSBMb2dcIiBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3B1bHNhci1lZGl0L3B1bHNhci9ibG9iL21hc3Rlci9DSEFOR0VMT0cubWRcIj5cbiAgICAgICAgICAgICAgey8qIExPR08gR09FUyBIRVJFICovfVxuICAgICAgICAgICAgICA8aDEgY2xhc3NOYW1lPVwid2VsY29tZS10aXRsZVwiPlxuICAgICAgICAgICAgICAgIENoYW5nZSBMb2dcbiAgICAgICAgICAgICAgPC9oMT5cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIndlbGNvbWUtcGFuZWxcIj5cbiAgICAgICAgICAgIDxwPlRha2UgYSBsb29rIGF0IHNvbWUgb2YgdGhlIGF3ZXNvbWUgdGhpbmdzIHthdG9tLmJyYW5kaW5nLm5hbWV9IGhhcyBjaGFuZ2VkOjwvcD5cbiAgICAgICAgICAgIDxwPkZlZWwgZnJlZSB0byByZWFkIG91ciA8YSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3B1bHNhci1lZGl0L3B1bHNhci9ibG9iL21hc3Rlci9DSEFOR0VMT0cubWRcIj5GdWxsIENoYW5nZSBMb2c8L2E+LjwvcD5cbiAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJodHRwczovL2dpdGh1Yi5jb20vcHVsc2FyLWVkaXQvcHVsc2FyL3B1bGwvMjhcIj5cbiAgICAgICAgICAgICAgICAgIEJ1bXAgdG8gRWxlY3Ryb24gMTIgYW5kIE5vZGUgMTRcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3B1bHNhci1lZGl0L3B1bHNhci9wdWxsLzdcIj5cbiAgICAgICAgICAgICAgICAgIEFkZGVkIGEgcmVicmFuZGluZyBBUElcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3B1bHNhci1lZGl0L3B1bHNhci9wdWxsLzY3XCI+XG4gICAgICAgICAgICAgICAgICBSZW1vdmVkIGV4cGVyaW1lbnRhbCBmaWxlIHdhdGNoZXJzIG9uIHRoZSBlZGl0b3IgYW5kIGZpeGVzIGZvciBob3cgdGhlIENvbmZpZyBGaWxlIGlzIHdhdGNoZWQuXG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgPGEgaHJlZj1cImh0dHBzOi8vZ2l0aHViLmNvbS9wdWxzYXItZWRpdC9wdWxzYXIvcHVsbC8xMzFcIj5cbiAgICAgICAgICAgICAgICAgIEFiaWxpdHkgdG8gaW5zdGFsbCBwYWNrYWdlcyBmcm9tIEdpdCBSZXBvc2l0b3JpZXNcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3B1bHNhci1lZGl0L3B1bHNhci9wdWxsLzY4XCI+XG4gICAgICAgICAgICAgICAgICBNaWdyYXRlZCB0byBhIG5ldyBQdWxzYXIgUGFja2FnZSBSZXBvc2l0b3J5IEJhY2tlbmRcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3B1bHNhci1lZGl0L3NldHRpbmdzLXZpZXcvcHVsbC8yXCI+XG4gICAgICAgICAgICAgICAgICBCZXR0ZXIgZXJyb3IgbWVzc2FnZXMgd2hlbiBhIHBhY2thZ2UgZmFpbHMgdG8gaW5zdGFsbC5cbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3B1bHNhci1lZGl0L3B1bHNhci9wdWxsLzE0XCI+XG4gICAgICAgICAgICAgICAgICBCdW1wZWQgVHJlZS1TaXR0ZXIgdG8gMC4yMC4xIGFuZCBhbGwgZ3JhbW1hcnMgdG8gdGhlaXIgcmVjZW50IHZlcnNpb25zXG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgPGEgaHJlZj1cImh0dHBzOi8vZ2l0aHViLmNvbS9wdWxzYXItZWRpdC9wdWxzYXIvcHVsbC8xMDFcIj5cbiAgICAgICAgICAgICAgICAgIE5hdGl2ZSBzdXBwb3J0IGZvciBBUk0gTGludXhcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3B1bHNhci1lZGl0L3B1bHNhci9wdWxsLzU0XCI+XG4gICAgICAgICAgICAgICAgICBOYXRpdmUgU3VwcG9ydCBmb3IgQXBwbGUgU2lsaWNvblxuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJodHRwczovL2dpdGh1Yi5jb20vcHVsc2FyLWVkaXQvcHVsc2FyL3B1bGwvMTA1XCI+XG4gICAgICAgICAgICAgICAgICBSZW1vdmVkIEJlbmNobWFyayBNb2RlXG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgPGEgaHJlZj1cImh0dHBzOi8vZ2l0aHViLmNvbS9wdWxzYXItZWRpdC9wdWxzYXIvcHVsbC80MFwiPlxuICAgICAgICAgICAgICAgICAgUmVtb3ZlZCBhbGwgdGVsZW1ldHJ5IGZyb20gdGhlIGVkaXRvci5cbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly9wdWxzYXItZWRpdC5kZXZcIj5cbiAgICAgICAgICAgICAgICAgIE5ldyBQdWxzYXIgV2Vic2l0ZVxuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJodHRwczovL2dpdGh1Yi5jb20vcHVsc2FyLWVkaXQvcHVsc2FyL3B1bGwvMTg2XCI+XG4gICAgICAgICAgICAgICAgICBBcHBsZSBTaWxpY29uIHN1cHBvcnQgZm9yIGBnaXRodWJgIFBhY2thZ2UgdjAuMzYuMTNcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvbGk+XG5cbiAgICAgICAgICAgIDwvdWw+XG5cbiAgICAgICAgICAgIDxzZWN0aW9uIGNsYXNzTmFtZT1cIndlbGNvbWUtcGFuZWxcIj5cbiAgICAgICAgICAgICAgPGxhYmVsPlxuICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzc05hbWU9XCJpbnB1dC1jaGVja2JveFwiXG4gICAgICAgICAgICAgICAgICB0eXBlPVwiY2hlY2tib3hcIlxuICAgICAgICAgICAgICAgICAgY2hlY2tlZD17YXRvbS5jb25maWcuZ2V0KCd3ZWxjb21lLnNob3dDaGFuZ2VMb2cnKX1cbiAgICAgICAgICAgICAgICAgIG9uY2hhbmdlPXt0aGlzLmRpZENoYW5nZVNob3dDaGFuZ2VMb2d9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICBTaG93IHRoZSBDaGFuZ2UgTG9nIGFmdGVyIGFuIHVwZGF0ZS5cbiAgICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgICAgIDxzZWN0aW9uIGNsYXNzTmFtZT1cIndlbGNvbWUtcGFuZWxcIj5cbiAgICAgICAgICAgICAgPGxhYmVsPlxuICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzc05hbWU9XCJpbnB1dC1jaGVja2JveFwiXG4gICAgICAgICAgICAgICAgICB0eXBlPVwiY2hlY2tib3hcIlxuICAgICAgICAgICAgICAgICAgY2hlY2tlZD17dGhpcy53YXNWZXJzaW9uRGlzbWlzc2VkKCl9XG4gICAgICAgICAgICAgICAgICBvbmNoYW5nZT17dGhpcy5kaXNtaXNzVmVyc2lvbn1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIERpc21pc3MgdGhpcyBDaGFuZ2UgTG9nXG4gICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICA8L3NlY3Rpb24+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIGdldFVSSSgpIHtcbiAgICByZXR1cm4gdGhpcy5wcm9wcy51cmk7XG4gIH1cblxuICBnZXRUaXRsZSgpIHtcbiAgICByZXR1cm4gJ0NoYW5nZSBMb2cnO1xuICB9XG5cbiAgaXNFcXVhbChvdGhlcikge1xuICAgIHJldHVybiBvdGhlciBpbnN0YW5jZW9mIENoYW5nZUxvZ1ZpZXc7XG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBR0E7QUFBd0I7QUFIeEI7QUFDQTs7QUFJZSxNQUFNQSxhQUFhLENBQUM7RUFDakNDLFdBQVcsQ0FBQ0MsS0FBSyxFQUFFO0lBQ2pCLElBQUksQ0FBQ0EsS0FBSyxHQUFHQSxLQUFLO0lBQ2xCQyxhQUFJLENBQUNDLFVBQVUsQ0FBQyxJQUFJLENBQUM7RUFDdkI7RUFFQUMsc0JBQXNCLEdBQUc7SUFDdkJDLElBQUksQ0FBQ0MsTUFBTSxDQUFDQyxHQUFHLENBQUMsdUJBQXVCLEVBQUUsSUFBSSxDQUFDQyxPQUFPLENBQUM7RUFDeEQ7RUFFQUMsY0FBYyxHQUFHO0lBQ2ZKLElBQUksQ0FBQ0MsTUFBTSxDQUFDQyxHQUFHLENBQUMsNkJBQTZCLEVBQUVGLElBQUksQ0FBQ0ssVUFBVSxFQUFFLENBQUNDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztFQUNqRjtFQUVBQyxtQkFBbUIsR0FBRztJQUNwQixNQUFNQyxXQUFXLEdBQUdSLElBQUksQ0FBQ0MsTUFBTSxDQUFDUSxHQUFHLENBQUMsNkJBQTZCLENBQUM7SUFDbEUsTUFBTUMsVUFBVSxHQUFHVixJQUFJLENBQUNLLFVBQVUsRUFBRSxDQUFDQyxLQUFLLENBQUMsR0FBRyxDQUFDO0lBQy9DLElBQUlFLFdBQVcsQ0FBQyxDQUFDLENBQUMsR0FBR0UsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJRixXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUdFLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSUYsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDRixLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUdJLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQ0osS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO01BQ2xJLE9BQU8sS0FBSztJQUNkLENBQUMsTUFBTTtNQUNMLE9BQU8sSUFBSTtJQUNiO0VBQ0Y7RUFFQUssTUFBTSxHQUFHLENBQUM7RUFFVkMsU0FBUyxHQUFHO0lBQ1YsT0FBTztNQUNMQyxZQUFZLEVBQUUsZUFBZTtNQUM3QkMsR0FBRyxFQUFFLElBQUksQ0FBQ2xCLEtBQUssQ0FBQ2tCO0lBQ2xCLENBQUM7RUFDSDtFQUVBQyxNQUFNLEdBQUc7SUFDUCxPQUNFO01BQUssU0FBUyxFQUFDO0lBQVMsR0FDdEI7TUFBSyxTQUFTLEVBQUM7SUFBbUIsR0FDaEM7TUFBSyxTQUFTLEVBQUM7SUFBUSxHQUNyQjtNQUFHLEtBQUssRUFBQyxpQkFBaUI7TUFBQyxJQUFJLEVBQUM7SUFBZ0UsR0FFOUY7TUFBSSxTQUFTLEVBQUM7SUFBZSxnQkFFeEIsQ0FDSCxDQUNBLEVBQ047TUFBSyxTQUFTLEVBQUM7SUFBZSxHQUM1QiwyRUFBOENmLElBQUksQ0FBQ2dCLFFBQVEsQ0FBQ0MsSUFBSSxrQkFBa0IsRUFDbEYsdURBQXlCO01BQUcsSUFBSSxFQUFDO0lBQWdFLHFCQUFvQixNQUFLLEVBQzFILDhCQUNFLDhCQUNFO01BQUcsSUFBSSxFQUFDO0lBQStDLHFDQUVuRCxDQUNELEVBQ0wsOEJBQ0U7TUFBRyxJQUFJLEVBQUM7SUFBOEMsNEJBRWxELENBQ0QsRUFDTCw4QkFDRTtNQUFHLElBQUksRUFBQztJQUErQyxvR0FFbkQsQ0FDRCxFQUNMLDhCQUNFO01BQUcsSUFBSSxFQUFDO0lBQWdELHVEQUVwRCxDQUNELEVBQ0wsOEJBQ0U7TUFBRyxJQUFJLEVBQUM7SUFBK0MseURBRW5ELENBQ0QsRUFDTCw4QkFDRTtNQUFHLElBQUksRUFBQztJQUFxRCw0REFFekQsQ0FDRCxFQUNMLDhCQUNFO01BQUcsSUFBSSxFQUFDO0lBQStDLDRFQUVuRCxDQUNELEVBQ0wsOEJBQ0U7TUFBRyxJQUFJLEVBQUM7SUFBZ0Qsa0NBRXBELENBQ0QsRUFDTCw4QkFDRTtNQUFHLElBQUksRUFBQztJQUErQyxzQ0FFbkQsQ0FDRCxFQUNMLDhCQUNFO01BQUcsSUFBSSxFQUFDO0lBQWdELDRCQUVwRCxDQUNELEVBQ0wsOEJBQ0U7TUFBRyxJQUFJLEVBQUM7SUFBK0MsNENBRW5ELENBQ0QsRUFDTCw4QkFDRTtNQUFHLElBQUksRUFBQztJQUF5Qix3QkFFN0IsQ0FDRCxFQUNMLDhCQUNFO01BQUcsSUFBSSxFQUFDO0lBQWdELHlEQUVwRCxDQUNELENBRUYsRUFFTDtNQUFTLFNBQVMsRUFBQztJQUFlLEdBQ2hDLGlDQUNFO01BQU8sU0FBUyxFQUFDLGdCQUFnQjtNQUMvQixJQUFJLEVBQUMsVUFBVTtNQUNmLE9BQU8sRUFBRWpCLElBQUksQ0FBQ0MsTUFBTSxDQUFDUSxHQUFHLENBQUMsdUJBQXVCLENBQUU7TUFDbEQsUUFBUSxFQUFFLElBQUksQ0FBQ1Y7SUFBdUIsRUFDdEMseUNBRUksQ0FDQSxFQUNWO01BQVMsU0FBUyxFQUFDO0lBQWUsR0FDaEMsaUNBQ0U7TUFBTyxTQUFTLEVBQUMsZ0JBQWdCO01BQy9CLElBQUksRUFBQyxVQUFVO01BQ2YsT0FBTyxFQUFFLElBQUksQ0FBQ1EsbUJBQW1CLEVBQUc7TUFDcEMsUUFBUSxFQUFFLElBQUksQ0FBQ0g7SUFBZSxFQUM5Qiw0QkFFSSxDQUNBLENBQ04sQ0FDRixDQUNGO0VBRVY7RUFFQWMsTUFBTSxHQUFHO0lBQ1AsT0FBTyxJQUFJLENBQUN0QixLQUFLLENBQUNrQixHQUFHO0VBQ3ZCO0VBRUFLLFFBQVEsR0FBRztJQUNULE9BQU8sWUFBWTtFQUNyQjtFQUVBQyxPQUFPLENBQUNDLEtBQUssRUFBRTtJQUNiLE9BQU9BLEtBQUssWUFBWTNCLGFBQWE7RUFDdkM7QUFDRjtBQUFDO0FBQUEifQ==