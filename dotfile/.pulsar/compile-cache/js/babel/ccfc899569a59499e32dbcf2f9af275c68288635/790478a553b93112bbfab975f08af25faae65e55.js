"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _path = _interopRequireDefault(require("path"));
var _etch = _interopRequireDefault(require("etch"));
var _underscorePlus = _interopRequireDefault(require("underscore-plus"));
var _atom = require("atom");
var _generalPanel = _interopRequireDefault(require("./general-panel"));
var _editorPanel = _interopRequireDefault(require("./editor-panel"));
var _packageDetailView = _interopRequireDefault(require("./package-detail-view"));
var _keybindingsPanel = _interopRequireDefault(require("./keybindings-panel"));
var _installPanel = _interopRequireDefault(require("./install-panel"));
var _themesPanel = _interopRequireDefault(require("./themes-panel"));
var _installedPackagesPanel = _interopRequireDefault(require("./installed-packages-panel"));
var _updatesPanel = _interopRequireDefault(require("./updates-panel"));
var _uriHandlerPanel = _interopRequireDefault(require("./uri-handler-panel"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
/** @babel */
/** @jsx etch.dom */

class SettingsView {
  constructor({
    uri,
    packageManager,
    snippetsProvider,
    activePanel
  } = {}) {
    this.uri = uri;
    this.packageManager = packageManager;
    this.snippetsProvider = snippetsProvider;
    this.deferredPanel = activePanel;
    this.destroyed = false;
    this.panelsByName = {};
    this.panelCreateCallbacks = {};
    _etch.default.initialize(this);
    this.disposables = new _atom.CompositeDisposable();
    this.disposables.add(atom.commands.add(this.element, {
      'core:move-up': () => {
        this.scrollUp();
      },
      'core:move-down': () => {
        this.scrollDown();
      },
      'core:page-up': () => {
        this.pageUp();
      },
      'core:page-down': () => {
        this.pageDown();
      },
      'core:move-to-top': () => {
        this.scrollToTop();
      },
      'core:move-to-bottom': () => {
        this.scrollToBottom();
      }
    }));
    this.disposables.add(atom.packages.onDidActivateInitialPackages(() => {
      this.disposables.add(atom.packages.onDidActivatePackage(pack => this.removePanelCache(pack.name)), atom.packages.onDidDeactivatePackage(pack => this.removePanelCache(pack.name)));
    }));
    process.nextTick(() => this.initializePanels());
  }
  removePanelCache(name) {
    delete this.panelsByName[name];
  }
  update() {}
  destroy() {
    this.destroyed = true;
    this.disposables.dispose();
    for (let name in this.panelsByName) {
      const panel = this.panelsByName[name];
      panel.destroy();
    }
    return _etch.default.destroy(this);
  }
  render() {
    return _etch.default.dom("div", {
      className: "settings-view pane-item",
      tabIndex: "-1"
    }, _etch.default.dom("div", {
      className: "config-menu",
      ref: "sidebar"
    }, _etch.default.dom("ul", {
      className: "panels-menu nav nav-pills nav-stacked",
      ref: "panelMenu"
    }, _etch.default.dom("div", {
      className: "panel-menu-separator",
      ref: "menuSeparator"
    })), _etch.default.dom("div", {
      className: "button-area"
    }, _etch.default.dom("button", {
      className: "btn btn-default icon icon-link-external",
      ref: "openDotAtom"
    }, "Open Config Folder"))), _etch.default.dom("div", {
      className: "panels",
      tabIndex: "-1",
      ref: "panels"
    }));
  }

  // This prevents the view being actually disposed when closed
  // If you remove it you will need to ensure the cached settingsView
  // in main.coffee is correctly released on close as well...
  onDidChangeTitle() {
    return new _atom.Disposable();
  }
  initializePanels() {
    if (this.refs.panels.children.length > 1) {
      return;
    }
    const clickHandler = event => {
      const target = event.target.closest('.panels-menu li a, .panels-packages li a');
      if (target) {
        this.showPanel(target.closest('li').name);
      }
    };
    this.element.addEventListener('click', clickHandler);
    this.disposables.add(new _atom.Disposable(() => this.element.removeEventListener('click', clickHandler)));
    const focusHandler = () => {
      this.focusActivePanel();
    };
    this.element.addEventListener('focus', focusHandler);
    this.disposables.add(new _atom.Disposable(() => this.element.removeEventListener('focus', focusHandler)));
    const openDotAtomClickHandler = () => {
      atom.open({
        pathsToOpen: [atom.getConfigDirPath()]
      });
    };
    this.refs.openDotAtom.addEventListener('click', openDotAtomClickHandler);
    this.disposables.add(new _atom.Disposable(() => this.refs.openDotAtom.removeEventListener('click', openDotAtomClickHandler)));
    this.addCorePanel('Core', 'settings', () => new _generalPanel.default());
    this.addCorePanel('Editor', 'code', () => new _editorPanel.default());
    if (atom.config.getSchema('core.uriHandlerRegistration').type !== 'any') {
      // "feature flag" based on core support for URI handling
      this.addCorePanel('URI Handling', 'link', () => new _uriHandlerPanel.default());
    }
    if (process.platform === 'win32' && require('atom').WinShell != null) {
      const SystemPanel = require('./system-windows-panel');
      this.addCorePanel('System', 'device-desktop', () => new SystemPanel());
    }
    this.addCorePanel('Keybindings', 'keyboard', () => new _keybindingsPanel.default());
    this.addCorePanel('Packages', 'package', () => new _installedPackagesPanel.default(this, this.packageManager));
    this.addCorePanel('Themes', 'paintcan', () => new _themesPanel.default(this, this.packageManager));
    this.addCorePanel('Updates', 'cloud-download', () => new _updatesPanel.default(this, this.packageManager));
    this.addCorePanel('Install', 'plus', () => new _installPanel.default(this, this.packageManager));
    this.showDeferredPanel();
    if (!this.activePanel) {
      this.showPanel('Core');
    }
    if (document.body.contains(this.element)) {
      this.refs.sidebar.style.width = this.refs.sidebar.offsetWidth;
    }
  }
  serialize() {
    return {
      deserializer: 'SettingsView',
      version: 2,
      activePanel: this.activePanel != null ? this.activePanel : this.deferredPanel,
      uri: this.uri
    };
  }
  getPackages() {
    let bundledPackageMetadataCache;
    if (this.packages != null) {
      return this.packages;
    }
    this.packages = atom.packages.getLoadedPackages();
    try {
      const packageMetadata = require(_path.default.join(atom.getLoadSettings().resourcePath, 'package.json'));
      bundledPackageMetadataCache = packageMetadata ? packageMetadata._atomPackages : null;
    } catch (error) {}

    // Include disabled packages so they can be re-enabled from the UI
    const disabledPackages = atom.config.get('core.disabledPackages') || [];
    for (const packageName of disabledPackages) {
      var metadata;
      const packagePath = atom.packages.resolvePackagePath(packageName);
      if (!packagePath) {
        continue;
      }
      try {
        metadata = require(_path.default.join(packagePath, 'package.json'));
      } catch (error) {
        if (bundledPackageMetadataCache && bundledPackageMetadataCache[packageName]) {
          metadata = bundledPackageMetadataCache[packageName].metadata;
        }
      }
      if (metadata == null) {
        continue;
      }
      const name = metadata.name != null ? metadata.name : packageName;
      if (!_underscorePlus.default.findWhere(this.packages, {
        name
      })) {
        this.packages.push({
          name,
          metadata,
          path: packagePath
        });
      }
    }
    this.packages.sort((pack1, pack2) => {
      const title1 = this.packageManager.getPackageTitle(pack1);
      const title2 = this.packageManager.getPackageTitle(pack2);
      return title1.localeCompare(title2);
    });
    return this.packages;
  }
  addCorePanel(name, iconName, panelCreateCallback) {
    const panelMenuItem = document.createElement('li');
    panelMenuItem.name = name;
    panelMenuItem.setAttribute('name', name);
    const a = document.createElement('a');
    a.classList.add('icon', `icon-${iconName}`);
    a.textContent = name;
    panelMenuItem.appendChild(a);
    this.refs.menuSeparator.parentElement.insertBefore(panelMenuItem, this.refs.menuSeparator);
    this.addPanel(name, panelCreateCallback);
  }
  addPanel(name, panelCreateCallback) {
    this.panelCreateCallbacks[name] = panelCreateCallback;
    if (this.deferredPanel && this.deferredPanel.name === name) {
      this.showDeferredPanel();
    }
  }
  getOrCreatePanel(name, options) {
    let panel = this.panelsByName[name];
    if (panel) return panel;
    if (name in this.panelCreateCallbacks) {
      panel = this.panelCreateCallbacks[name]();
      delete this.panelCreateCallbacks[name];
    } else if (options && options.pack) {
      if (!options.pack.metadata) {
        options.pack.metadata = _underscorePlus.default.clone(options.pack);
      }
      panel = new _packageDetailView.default(options.pack, this, this.packageManager, this.snippetsProvider);
    }
    if (panel) {
      this.panelsByName[name] = panel;
    }
    return panel;
  }
  makePanelMenuActive(name) {
    const previouslyActivePanel = this.refs.sidebar.querySelector('.active');
    if (previouslyActivePanel) {
      previouslyActivePanel.classList.remove('active');
    }
    const newActivePanel = this.refs.sidebar.querySelector(`[name='${name}']`);
    if (newActivePanel) {
      newActivePanel.classList.add('active');
    }
  }
  focusActivePanel() {
    // Pass focus to panel that is currently visible
    for (let i = 0; i < this.refs.panels.children.length; i++) {
      const child = this.refs.panels.children[i];
      if (child.offsetWidth > 0) {
        child.focus();
      }
    }
  }
  showDeferredPanel() {
    if (this.deferredPanel) {
      const {
        name,
        options
      } = this.deferredPanel;
      this.showPanel(name, options);
    }
  }

  // Public: show a panel.
  //
  // * `name` {String} the name of the panel to show
  // * `options` {Object} an options hash. Will be passed to `beforeShow()` on
  //   the panel. Options may include (but are not limited to):
  //   * `uri` the URI the panel was launched from
  showPanel(name, options) {
    if (this.activePanel) {
      const prev = this.panelsByName[this.activePanel.name];
      if (prev) {
        prev.scrollPosition = prev.element.scrollTop;
      }
    }
    const panel = this.getOrCreatePanel(name, options);
    if (panel) {
      this.appendPanel(panel, options);
      this.makePanelMenuActive(name);
      this.setActivePanel(name, options);
      this.deferredPanel = null;
    } else {
      this.deferredPanel = {
        name,
        options
      };
    }
  }
  showPanelForURI(uri) {
    const regex = /config\/([a-z]+)\/?([a-zA-Z0-9_-]+)?/i;
    const match = regex.exec(uri);
    if (match) {
      const path1 = match[1];
      const path2 = match[2];
      if (path1 === 'packages' && path2 != null) {
        this.showPanel(path2, {
          uri: uri,
          pack: {
            name: path2
          },
          back: atom.packages.getLoadedPackage(path2) ? 'Packages' : null
        });
      } else {
        const panelName = path1[0].toUpperCase() + path1.slice(1);
        this.showPanel(panelName, {
          uri
        });
      }
    }
  }
  appendPanel(panel, options) {
    for (let i = 0; i < this.refs.panels.children.length; i++) {
      this.refs.panels.children[i].style.display = 'none';
    }
    if (!this.refs.panels.contains(panel.element)) {
      this.refs.panels.appendChild(panel.element);
    }
    if (panel.beforeShow) {
      panel.beforeShow(options);
    }
    panel.show();
    panel.focus();
  }
  setActivePanel(name, options = {}) {
    this.activePanel = {
      name,
      options
    };
    const panel = this.panelsByName[name];
    if (panel && panel.scrollPosition) {
      panel.element.scrollTop = panel.scrollPosition;
      delete panel.scrollPosition;
    }
  }
  removePanel(name) {
    const panel = this.panelsByName[name];
    if (panel) {
      panel.destroy();
      delete this.panelsByName[name];
    }
  }
  getTitle() {
    return 'Settings';
  }
  getIconName() {
    return 'tools';
  }
  getURI() {
    return this.uri;
  }
  isEqual(other) {
    return other instanceof SettingsView;
  }
  scrollUp() {
    this.element.scrollTop -= document.body.offsetHeight / 20;
  }
  scrollDown() {
    this.element.scrollTop += document.body.offsetHeight / 20;
  }
  pageUp() {
    this.element.scrollTop -= this.element.offsetHeight;
  }
  pageDown() {
    this.element.scrollTop += this.element.offsetHeight;
  }
  scrollToTop() {
    this.element.scrollTop = 0;
  }
  scrollToBottom() {
    this.element.scrollTop = this.element.scrollHeight;
  }
}
exports.default = SettingsView;
module.exports = exports.default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJTZXR0aW5nc1ZpZXciLCJjb25zdHJ1Y3RvciIsInVyaSIsInBhY2thZ2VNYW5hZ2VyIiwic25pcHBldHNQcm92aWRlciIsImFjdGl2ZVBhbmVsIiwiZGVmZXJyZWRQYW5lbCIsImRlc3Ryb3llZCIsInBhbmVsc0J5TmFtZSIsInBhbmVsQ3JlYXRlQ2FsbGJhY2tzIiwiZXRjaCIsImluaXRpYWxpemUiLCJkaXNwb3NhYmxlcyIsIkNvbXBvc2l0ZURpc3Bvc2FibGUiLCJhZGQiLCJhdG9tIiwiY29tbWFuZHMiLCJlbGVtZW50Iiwic2Nyb2xsVXAiLCJzY3JvbGxEb3duIiwicGFnZVVwIiwicGFnZURvd24iLCJzY3JvbGxUb1RvcCIsInNjcm9sbFRvQm90dG9tIiwicGFja2FnZXMiLCJvbkRpZEFjdGl2YXRlSW5pdGlhbFBhY2thZ2VzIiwib25EaWRBY3RpdmF0ZVBhY2thZ2UiLCJwYWNrIiwicmVtb3ZlUGFuZWxDYWNoZSIsIm5hbWUiLCJvbkRpZERlYWN0aXZhdGVQYWNrYWdlIiwicHJvY2VzcyIsIm5leHRUaWNrIiwiaW5pdGlhbGl6ZVBhbmVscyIsInVwZGF0ZSIsImRlc3Ryb3kiLCJkaXNwb3NlIiwicGFuZWwiLCJyZW5kZXIiLCJvbkRpZENoYW5nZVRpdGxlIiwiRGlzcG9zYWJsZSIsInJlZnMiLCJwYW5lbHMiLCJjaGlsZHJlbiIsImxlbmd0aCIsImNsaWNrSGFuZGxlciIsImV2ZW50IiwidGFyZ2V0IiwiY2xvc2VzdCIsInNob3dQYW5lbCIsImFkZEV2ZW50TGlzdGVuZXIiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwiZm9jdXNIYW5kbGVyIiwiZm9jdXNBY3RpdmVQYW5lbCIsIm9wZW5Eb3RBdG9tQ2xpY2tIYW5kbGVyIiwib3BlbiIsInBhdGhzVG9PcGVuIiwiZ2V0Q29uZmlnRGlyUGF0aCIsIm9wZW5Eb3RBdG9tIiwiYWRkQ29yZVBhbmVsIiwiR2VuZXJhbFBhbmVsIiwiRWRpdG9yUGFuZWwiLCJjb25maWciLCJnZXRTY2hlbWEiLCJ0eXBlIiwiVXJpSGFuZGxlclBhbmVsIiwicGxhdGZvcm0iLCJyZXF1aXJlIiwiV2luU2hlbGwiLCJTeXN0ZW1QYW5lbCIsIktleWJpbmRpbmdzUGFuZWwiLCJJbnN0YWxsZWRQYWNrYWdlc1BhbmVsIiwiVGhlbWVzUGFuZWwiLCJVcGRhdGVzUGFuZWwiLCJJbnN0YWxsUGFuZWwiLCJzaG93RGVmZXJyZWRQYW5lbCIsImRvY3VtZW50IiwiYm9keSIsImNvbnRhaW5zIiwic2lkZWJhciIsInN0eWxlIiwid2lkdGgiLCJvZmZzZXRXaWR0aCIsInNlcmlhbGl6ZSIsImRlc2VyaWFsaXplciIsInZlcnNpb24iLCJnZXRQYWNrYWdlcyIsImJ1bmRsZWRQYWNrYWdlTWV0YWRhdGFDYWNoZSIsImdldExvYWRlZFBhY2thZ2VzIiwicGFja2FnZU1ldGFkYXRhIiwicGF0aCIsImpvaW4iLCJnZXRMb2FkU2V0dGluZ3MiLCJyZXNvdXJjZVBhdGgiLCJfYXRvbVBhY2thZ2VzIiwiZXJyb3IiLCJkaXNhYmxlZFBhY2thZ2VzIiwiZ2V0IiwicGFja2FnZU5hbWUiLCJtZXRhZGF0YSIsInBhY2thZ2VQYXRoIiwicmVzb2x2ZVBhY2thZ2VQYXRoIiwiXyIsImZpbmRXaGVyZSIsInB1c2giLCJzb3J0IiwicGFjazEiLCJwYWNrMiIsInRpdGxlMSIsImdldFBhY2thZ2VUaXRsZSIsInRpdGxlMiIsImxvY2FsZUNvbXBhcmUiLCJpY29uTmFtZSIsInBhbmVsQ3JlYXRlQ2FsbGJhY2siLCJwYW5lbE1lbnVJdGVtIiwiY3JlYXRlRWxlbWVudCIsInNldEF0dHJpYnV0ZSIsImEiLCJjbGFzc0xpc3QiLCJ0ZXh0Q29udGVudCIsImFwcGVuZENoaWxkIiwibWVudVNlcGFyYXRvciIsInBhcmVudEVsZW1lbnQiLCJpbnNlcnRCZWZvcmUiLCJhZGRQYW5lbCIsImdldE9yQ3JlYXRlUGFuZWwiLCJvcHRpb25zIiwiY2xvbmUiLCJQYWNrYWdlRGV0YWlsVmlldyIsIm1ha2VQYW5lbE1lbnVBY3RpdmUiLCJwcmV2aW91c2x5QWN0aXZlUGFuZWwiLCJxdWVyeVNlbGVjdG9yIiwicmVtb3ZlIiwibmV3QWN0aXZlUGFuZWwiLCJpIiwiY2hpbGQiLCJmb2N1cyIsInByZXYiLCJzY3JvbGxQb3NpdGlvbiIsInNjcm9sbFRvcCIsImFwcGVuZFBhbmVsIiwic2V0QWN0aXZlUGFuZWwiLCJzaG93UGFuZWxGb3JVUkkiLCJyZWdleCIsIm1hdGNoIiwiZXhlYyIsInBhdGgxIiwicGF0aDIiLCJiYWNrIiwiZ2V0TG9hZGVkUGFja2FnZSIsInBhbmVsTmFtZSIsInRvVXBwZXJDYXNlIiwic2xpY2UiLCJkaXNwbGF5IiwiYmVmb3JlU2hvdyIsInNob3ciLCJyZW1vdmVQYW5lbCIsImdldFRpdGxlIiwiZ2V0SWNvbk5hbWUiLCJnZXRVUkkiLCJpc0VxdWFsIiwib3RoZXIiLCJvZmZzZXRIZWlnaHQiLCJzY3JvbGxIZWlnaHQiXSwic291cmNlcyI6WyJzZXR0aW5ncy12aWV3LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKiBAYmFiZWwgKi9cbi8qKiBAanN4IGV0Y2guZG9tICovXG5cbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnXG5pbXBvcnQgZXRjaCBmcm9tICdldGNoJ1xuaW1wb3J0IF8gZnJvbSAndW5kZXJzY29yZS1wbHVzJ1xuaW1wb3J0IHtDb21wb3NpdGVEaXNwb3NhYmxlLCBEaXNwb3NhYmxlfSBmcm9tICdhdG9tJ1xuXG5pbXBvcnQgR2VuZXJhbFBhbmVsIGZyb20gJy4vZ2VuZXJhbC1wYW5lbCdcbmltcG9ydCBFZGl0b3JQYW5lbCBmcm9tICcuL2VkaXRvci1wYW5lbCdcbmltcG9ydCBQYWNrYWdlRGV0YWlsVmlldyBmcm9tICcuL3BhY2thZ2UtZGV0YWlsLXZpZXcnXG5pbXBvcnQgS2V5YmluZGluZ3NQYW5lbCBmcm9tICcuL2tleWJpbmRpbmdzLXBhbmVsJ1xuaW1wb3J0IEluc3RhbGxQYW5lbCBmcm9tICcuL2luc3RhbGwtcGFuZWwnXG5pbXBvcnQgVGhlbWVzUGFuZWwgZnJvbSAnLi90aGVtZXMtcGFuZWwnXG5pbXBvcnQgSW5zdGFsbGVkUGFja2FnZXNQYW5lbCBmcm9tICcuL2luc3RhbGxlZC1wYWNrYWdlcy1wYW5lbCdcbmltcG9ydCBVcGRhdGVzUGFuZWwgZnJvbSAnLi91cGRhdGVzLXBhbmVsJ1xuaW1wb3J0IFVyaUhhbmRsZXJQYW5lbCBmcm9tICcuL3VyaS1oYW5kbGVyLXBhbmVsJ1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZXR0aW5nc1ZpZXcge1xuICBjb25zdHJ1Y3RvciAoe3VyaSwgcGFja2FnZU1hbmFnZXIsIHNuaXBwZXRzUHJvdmlkZXIsIGFjdGl2ZVBhbmVsfSA9IHt9KSB7XG4gICAgdGhpcy51cmkgPSB1cmlcbiAgICB0aGlzLnBhY2thZ2VNYW5hZ2VyID0gcGFja2FnZU1hbmFnZXJcbiAgICB0aGlzLnNuaXBwZXRzUHJvdmlkZXIgPSBzbmlwcGV0c1Byb3ZpZGVyXG4gICAgdGhpcy5kZWZlcnJlZFBhbmVsID0gYWN0aXZlUGFuZWxcbiAgICB0aGlzLmRlc3Ryb3llZCA9IGZhbHNlXG4gICAgdGhpcy5wYW5lbHNCeU5hbWUgPSB7fVxuICAgIHRoaXMucGFuZWxDcmVhdGVDYWxsYmFja3MgPSB7fVxuXG4gICAgZXRjaC5pbml0aWFsaXplKHRoaXMpXG4gICAgdGhpcy5kaXNwb3NhYmxlcyA9IG5ldyBDb21wb3NpdGVEaXNwb3NhYmxlKClcbiAgICB0aGlzLmRpc3Bvc2FibGVzLmFkZChhdG9tLmNvbW1hbmRzLmFkZCh0aGlzLmVsZW1lbnQsIHtcbiAgICAgICdjb3JlOm1vdmUtdXAnOiAoKSA9PiB7IHRoaXMuc2Nyb2xsVXAoKSB9LFxuICAgICAgJ2NvcmU6bW92ZS1kb3duJzogKCkgPT4geyB0aGlzLnNjcm9sbERvd24oKSB9LFxuICAgICAgJ2NvcmU6cGFnZS11cCc6ICgpID0+IHsgdGhpcy5wYWdlVXAoKSB9LFxuICAgICAgJ2NvcmU6cGFnZS1kb3duJzogKCkgPT4geyB0aGlzLnBhZ2VEb3duKCkgfSxcbiAgICAgICdjb3JlOm1vdmUtdG8tdG9wJzogKCkgPT4geyB0aGlzLnNjcm9sbFRvVG9wKCkgfSxcbiAgICAgICdjb3JlOm1vdmUtdG8tYm90dG9tJzogKCkgPT4geyB0aGlzLnNjcm9sbFRvQm90dG9tKCkgfVxuICAgIH0pKVxuXG4gICAgdGhpcy5kaXNwb3NhYmxlcy5hZGQoYXRvbS5wYWNrYWdlcy5vbkRpZEFjdGl2YXRlSW5pdGlhbFBhY2thZ2VzKCgpID0+IHtcbiAgICAgIHRoaXMuZGlzcG9zYWJsZXMuYWRkKFxuICAgICAgICBhdG9tLnBhY2thZ2VzLm9uRGlkQWN0aXZhdGVQYWNrYWdlKHBhY2sgPT4gdGhpcy5yZW1vdmVQYW5lbENhY2hlKHBhY2submFtZSkpLFxuICAgICAgICBhdG9tLnBhY2thZ2VzLm9uRGlkRGVhY3RpdmF0ZVBhY2thZ2UocGFjayA9PiB0aGlzLnJlbW92ZVBhbmVsQ2FjaGUocGFjay5uYW1lKSlcbiAgICAgIClcbiAgICB9KSlcblxuICAgIHByb2Nlc3MubmV4dFRpY2soKCkgPT4gdGhpcy5pbml0aWFsaXplUGFuZWxzKCkpXG4gIH1cblxuICByZW1vdmVQYW5lbENhY2hlIChuYW1lKSB7XG4gICAgZGVsZXRlIHRoaXMucGFuZWxzQnlOYW1lW25hbWVdXG4gIH1cblxuICB1cGRhdGUgKCkge31cblxuICBkZXN0cm95ICgpIHtcbiAgICB0aGlzLmRlc3Ryb3llZCA9IHRydWVcbiAgICB0aGlzLmRpc3Bvc2FibGVzLmRpc3Bvc2UoKVxuICAgIGZvciAobGV0IG5hbWUgaW4gdGhpcy5wYW5lbHNCeU5hbWUpIHtcbiAgICAgIGNvbnN0IHBhbmVsID0gdGhpcy5wYW5lbHNCeU5hbWVbbmFtZV1cbiAgICAgIHBhbmVsLmRlc3Ryb3koKVxuICAgIH1cblxuICAgIHJldHVybiBldGNoLmRlc3Ryb3kodGhpcylcbiAgfVxuXG4gIHJlbmRlciAoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPSdzZXR0aW5ncy12aWV3IHBhbmUtaXRlbScgdGFiSW5kZXg9Jy0xJz5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9J2NvbmZpZy1tZW51JyByZWY9J3NpZGViYXInPlxuICAgICAgICAgIDx1bCBjbGFzc05hbWU9J3BhbmVscy1tZW51IG5hdiBuYXYtcGlsbHMgbmF2LXN0YWNrZWQnIHJlZj0ncGFuZWxNZW51Jz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdwYW5lbC1tZW51LXNlcGFyYXRvcicgcmVmPSdtZW51U2VwYXJhdG9yJyAvPlxuICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9J2J1dHRvbi1hcmVhJz5cbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPSdidG4gYnRuLWRlZmF1bHQgaWNvbiBpY29uLWxpbmstZXh0ZXJuYWwnIHJlZj0nb3BlbkRvdEF0b20nPk9wZW4gQ29uZmlnIEZvbGRlcjwvYnV0dG9uPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgey8qIFRoZSB0YWJpbmRleCBhdHRyIGJlbG93IGVuc3VyZXMgdGhhdCBjbGlja3MgaW4gYSBwYW5lbCBpdGVtIHdvbid0XG4gICAgICAgIGNhdXNlIHRoaXMgdmlldyB0byBnYWluIGZvY3VzLiBUaGlzIGlzIGltcG9ydGFudCBiZWNhdXNlIHdoZW4gdGhpcyB2aWV3XG4gICAgICAgIGdhaW5zIGZvY3VzIChlLmcuIGltbWVkaWF0ZWx5IGFmdGVyIGF0b20gZGlzcGxheXMgaXQpLCBpdCBmb2N1c2VzIHRoZVxuICAgICAgICBjdXJyZW50bHkgYWN0aXZlIHBhbmVsIGl0ZW0uIElmIHRoYXQgZm9jdXNpbmcgY2F1c2VzIHRoZSBhY3RpdmUgcGFuZWwgdG9cbiAgICAgICAgc2Nyb2xsIChlLmcuIGJlY2F1c2UgdGhlIGFjdGl2ZSBwYW5lbCBpdHNlbGYgcGFzc2VzIGZvY3VzIG9uIHRvIGEgc2VhcmNoXG4gICAgICAgIGJveCBhdCB0aGUgdG9wIG9mIGEgc2Nyb2xsZWQgcGFuZWwpLCB0aGVuIHRoZSBicm93c2VyIHdpbGwgbm90IGZpcmUgdGhlXG4gICAgICAgIGNsaWNrIGV2ZW50IG9uIHRoZSBlbGVtZW50IHdpdGhpbiB0aGUgcGFuZWwgb24gd2hpY2ggdGhlIHVzZXIgb3JpZ2luYWxseVxuICAgICAgICBjbGlja2VkIChlLmcuIGEgcGFja2FnZSBjYXJkKS4gVGhpcyB3b3VsZCBwcmV2ZW50IHVzIGZyb20gc2hvd2luZyBhXG4gICAgICAgIHBhY2thZ2UgZGV0YWlsIHZpZXcgd2hlbiBjbGlja2luZyBvbiBhIHBhY2thZ2UgY2FyZC4gUGhldyEgKi99XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdwYW5lbHMnIHRhYkluZGV4PSctMScgcmVmPSdwYW5lbHMnIC8+XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cblxuICAvLyBUaGlzIHByZXZlbnRzIHRoZSB2aWV3IGJlaW5nIGFjdHVhbGx5IGRpc3Bvc2VkIHdoZW4gY2xvc2VkXG4gIC8vIElmIHlvdSByZW1vdmUgaXQgeW91IHdpbGwgbmVlZCB0byBlbnN1cmUgdGhlIGNhY2hlZCBzZXR0aW5nc1ZpZXdcbiAgLy8gaW4gbWFpbi5jb2ZmZWUgaXMgY29ycmVjdGx5IHJlbGVhc2VkIG9uIGNsb3NlIGFzIHdlbGwuLi5cbiAgb25EaWRDaGFuZ2VUaXRsZSAoKSB7IHJldHVybiBuZXcgRGlzcG9zYWJsZSgpIH1cblxuICBpbml0aWFsaXplUGFuZWxzICgpIHtcbiAgICBpZiAodGhpcy5yZWZzLnBhbmVscy5jaGlsZHJlbi5sZW5ndGggPiAxKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBjb25zdCBjbGlja0hhbmRsZXIgPSAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IHRhcmdldCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcucGFuZWxzLW1lbnUgbGkgYSwgLnBhbmVscy1wYWNrYWdlcyBsaSBhJylcbiAgICAgIGlmICh0YXJnZXQpIHtcbiAgICAgICAgdGhpcy5zaG93UGFuZWwodGFyZ2V0LmNsb3Nlc3QoJ2xpJykubmFtZSlcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2xpY2tIYW5kbGVyKVxuICAgIHRoaXMuZGlzcG9zYWJsZXMuYWRkKG5ldyBEaXNwb3NhYmxlKCgpID0+IHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIGNsaWNrSGFuZGxlcikpKVxuXG4gICAgY29uc3QgZm9jdXNIYW5kbGVyID0gKCkgPT4ge1xuICAgICAgdGhpcy5mb2N1c0FjdGl2ZVBhbmVsKClcbiAgICB9XG4gICAgdGhpcy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2ZvY3VzJywgZm9jdXNIYW5kbGVyKVxuICAgIHRoaXMuZGlzcG9zYWJsZXMuYWRkKG5ldyBEaXNwb3NhYmxlKCgpID0+IHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdmb2N1cycsIGZvY3VzSGFuZGxlcikpKVxuXG4gICAgY29uc3Qgb3BlbkRvdEF0b21DbGlja0hhbmRsZXIgPSAoKSA9PiB7XG4gICAgICBhdG9tLm9wZW4oe3BhdGhzVG9PcGVuOiBbYXRvbS5nZXRDb25maWdEaXJQYXRoKCldfSlcbiAgICB9XG4gICAgdGhpcy5yZWZzLm9wZW5Eb3RBdG9tLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgb3BlbkRvdEF0b21DbGlja0hhbmRsZXIpXG4gICAgdGhpcy5kaXNwb3NhYmxlcy5hZGQobmV3IERpc3Bvc2FibGUoKCkgPT4gdGhpcy5yZWZzLm9wZW5Eb3RBdG9tLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgb3BlbkRvdEF0b21DbGlja0hhbmRsZXIpKSlcblxuICAgIHRoaXMuYWRkQ29yZVBhbmVsKCdDb3JlJywgJ3NldHRpbmdzJywgKCkgPT4gbmV3IEdlbmVyYWxQYW5lbCgpKVxuICAgIHRoaXMuYWRkQ29yZVBhbmVsKCdFZGl0b3InLCAnY29kZScsICgpID0+IG5ldyBFZGl0b3JQYW5lbCgpKVxuICAgIGlmIChhdG9tLmNvbmZpZy5nZXRTY2hlbWEoJ2NvcmUudXJpSGFuZGxlclJlZ2lzdHJhdGlvbicpLnR5cGUgIT09ICdhbnknKSB7XG4gICAgICAvLyBcImZlYXR1cmUgZmxhZ1wiIGJhc2VkIG9uIGNvcmUgc3VwcG9ydCBmb3IgVVJJIGhhbmRsaW5nXG4gICAgICB0aGlzLmFkZENvcmVQYW5lbCgnVVJJIEhhbmRsaW5nJywgJ2xpbmsnLCAoKSA9PiBuZXcgVXJpSGFuZGxlclBhbmVsKCkpXG4gICAgfVxuICAgIGlmICgocHJvY2Vzcy5wbGF0Zm9ybSA9PT0gJ3dpbjMyJykgJiYgKHJlcXVpcmUoJ2F0b20nKS5XaW5TaGVsbCAhPSBudWxsKSkge1xuICAgICAgY29uc3QgU3lzdGVtUGFuZWwgPSByZXF1aXJlKCcuL3N5c3RlbS13aW5kb3dzLXBhbmVsJylcbiAgICAgIHRoaXMuYWRkQ29yZVBhbmVsKCdTeXN0ZW0nLCAnZGV2aWNlLWRlc2t0b3AnLCAoKSA9PiBuZXcgU3lzdGVtUGFuZWwoKSlcbiAgICB9XG4gICAgdGhpcy5hZGRDb3JlUGFuZWwoJ0tleWJpbmRpbmdzJywgJ2tleWJvYXJkJywgKCkgPT4gbmV3IEtleWJpbmRpbmdzUGFuZWwoKSlcbiAgICB0aGlzLmFkZENvcmVQYW5lbCgnUGFja2FnZXMnLCAncGFja2FnZScsICgpID0+IG5ldyBJbnN0YWxsZWRQYWNrYWdlc1BhbmVsKHRoaXMsIHRoaXMucGFja2FnZU1hbmFnZXIpKVxuICAgIHRoaXMuYWRkQ29yZVBhbmVsKCdUaGVtZXMnLCAncGFpbnRjYW4nLCAoKSA9PiBuZXcgVGhlbWVzUGFuZWwodGhpcywgdGhpcy5wYWNrYWdlTWFuYWdlcikpXG4gICAgdGhpcy5hZGRDb3JlUGFuZWwoJ1VwZGF0ZXMnLCAnY2xvdWQtZG93bmxvYWQnLCAoKSA9PiBuZXcgVXBkYXRlc1BhbmVsKHRoaXMsIHRoaXMucGFja2FnZU1hbmFnZXIpKVxuICAgIHRoaXMuYWRkQ29yZVBhbmVsKCdJbnN0YWxsJywgJ3BsdXMnLCAoKSA9PiBuZXcgSW5zdGFsbFBhbmVsKHRoaXMsIHRoaXMucGFja2FnZU1hbmFnZXIpKVxuXG4gICAgdGhpcy5zaG93RGVmZXJyZWRQYW5lbCgpXG5cbiAgICBpZiAoIXRoaXMuYWN0aXZlUGFuZWwpIHtcbiAgICAgIHRoaXMuc2hvd1BhbmVsKCdDb3JlJylcbiAgICB9XG5cbiAgICBpZiAoZG9jdW1lbnQuYm9keS5jb250YWlucyh0aGlzLmVsZW1lbnQpKSB7XG4gICAgICB0aGlzLnJlZnMuc2lkZWJhci5zdHlsZS53aWR0aCA9IHRoaXMucmVmcy5zaWRlYmFyLm9mZnNldFdpZHRoXG4gICAgfVxuICB9XG5cbiAgc2VyaWFsaXplICgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgZGVzZXJpYWxpemVyOiAnU2V0dGluZ3NWaWV3JyxcbiAgICAgIHZlcnNpb246IDIsXG4gICAgICBhY3RpdmVQYW5lbDogdGhpcy5hY3RpdmVQYW5lbCAhPSBudWxsID8gdGhpcy5hY3RpdmVQYW5lbCA6IHRoaXMuZGVmZXJyZWRQYW5lbCxcbiAgICAgIHVyaTogdGhpcy51cmlcbiAgICB9XG4gIH1cblxuICBnZXRQYWNrYWdlcyAoKSB7XG4gICAgbGV0IGJ1bmRsZWRQYWNrYWdlTWV0YWRhdGFDYWNoZVxuICAgIGlmICh0aGlzLnBhY2thZ2VzICE9IG51bGwpIHsgcmV0dXJuIHRoaXMucGFja2FnZXMgfVxuXG4gICAgdGhpcy5wYWNrYWdlcyA9IGF0b20ucGFja2FnZXMuZ2V0TG9hZGVkUGFja2FnZXMoKVxuXG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHBhY2thZ2VNZXRhZGF0YSA9IHJlcXVpcmUocGF0aC5qb2luKGF0b20uZ2V0TG9hZFNldHRpbmdzKCkucmVzb3VyY2VQYXRoLCAncGFja2FnZS5qc29uJykpXG4gICAgICBidW5kbGVkUGFja2FnZU1ldGFkYXRhQ2FjaGUgPSBwYWNrYWdlTWV0YWRhdGEgPyBwYWNrYWdlTWV0YWRhdGEuX2F0b21QYWNrYWdlcyA6IG51bGxcbiAgICB9IGNhdGNoIChlcnJvcikge31cblxuICAgIC8vIEluY2x1ZGUgZGlzYWJsZWQgcGFja2FnZXMgc28gdGhleSBjYW4gYmUgcmUtZW5hYmxlZCBmcm9tIHRoZSBVSVxuICAgIGNvbnN0IGRpc2FibGVkUGFja2FnZXMgPSBhdG9tLmNvbmZpZy5nZXQoJ2NvcmUuZGlzYWJsZWRQYWNrYWdlcycpIHx8IFtdXG4gICAgZm9yIChjb25zdCBwYWNrYWdlTmFtZSBvZiBkaXNhYmxlZFBhY2thZ2VzKSB7XG4gICAgICB2YXIgbWV0YWRhdGFcbiAgICAgIGNvbnN0IHBhY2thZ2VQYXRoID0gYXRvbS5wYWNrYWdlcy5yZXNvbHZlUGFja2FnZVBhdGgocGFja2FnZU5hbWUpXG4gICAgICBpZiAoIXBhY2thZ2VQYXRoKSB7XG4gICAgICAgIGNvbnRpbnVlXG4gICAgICB9XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIG1ldGFkYXRhID0gcmVxdWlyZShwYXRoLmpvaW4ocGFja2FnZVBhdGgsICdwYWNrYWdlLmpzb24nKSlcbiAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIGlmIChidW5kbGVkUGFja2FnZU1ldGFkYXRhQ2FjaGUgJiYgYnVuZGxlZFBhY2thZ2VNZXRhZGF0YUNhY2hlW3BhY2thZ2VOYW1lXSkge1xuICAgICAgICAgIG1ldGFkYXRhID0gYnVuZGxlZFBhY2thZ2VNZXRhZGF0YUNhY2hlW3BhY2thZ2VOYW1lXS5tZXRhZGF0YVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAobWV0YWRhdGEgPT0gbnVsbCkge1xuICAgICAgICBjb250aW51ZVxuICAgICAgfVxuXG4gICAgICBjb25zdCBuYW1lID0gbWV0YWRhdGEubmFtZSAhPSBudWxsID8gbWV0YWRhdGEubmFtZSA6IHBhY2thZ2VOYW1lXG4gICAgICBpZiAoIV8uZmluZFdoZXJlKHRoaXMucGFja2FnZXMsIHtuYW1lfSkpIHtcbiAgICAgICAgdGhpcy5wYWNrYWdlcy5wdXNoKHtuYW1lLCBtZXRhZGF0YSwgcGF0aDogcGFja2FnZVBhdGh9KVxuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMucGFja2FnZXMuc29ydCgocGFjazEsIHBhY2syKSA9PiB7XG4gICAgICBjb25zdCB0aXRsZTEgPSB0aGlzLnBhY2thZ2VNYW5hZ2VyLmdldFBhY2thZ2VUaXRsZShwYWNrMSlcbiAgICAgIGNvbnN0IHRpdGxlMiA9IHRoaXMucGFja2FnZU1hbmFnZXIuZ2V0UGFja2FnZVRpdGxlKHBhY2syKVxuICAgICAgcmV0dXJuIHRpdGxlMS5sb2NhbGVDb21wYXJlKHRpdGxlMilcbiAgICB9KVxuXG4gICAgcmV0dXJuIHRoaXMucGFja2FnZXNcbiAgfVxuXG4gIGFkZENvcmVQYW5lbCAobmFtZSwgaWNvbk5hbWUsIHBhbmVsQ3JlYXRlQ2FsbGJhY2spIHtcbiAgICBjb25zdCBwYW5lbE1lbnVJdGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGknKVxuICAgIHBhbmVsTWVudUl0ZW0ubmFtZSA9IG5hbWVcbiAgICBwYW5lbE1lbnVJdGVtLnNldEF0dHJpYnV0ZSgnbmFtZScsIG5hbWUpXG5cbiAgICBjb25zdCBhID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpXG4gICAgYS5jbGFzc0xpc3QuYWRkKCdpY29uJywgYGljb24tJHtpY29uTmFtZX1gKVxuICAgIGEudGV4dENvbnRlbnQgPSBuYW1lXG4gICAgcGFuZWxNZW51SXRlbS5hcHBlbmRDaGlsZChhKVxuXG4gICAgdGhpcy5yZWZzLm1lbnVTZXBhcmF0b3IucGFyZW50RWxlbWVudC5pbnNlcnRCZWZvcmUocGFuZWxNZW51SXRlbSwgdGhpcy5yZWZzLm1lbnVTZXBhcmF0b3IpXG4gICAgdGhpcy5hZGRQYW5lbChuYW1lLCBwYW5lbENyZWF0ZUNhbGxiYWNrKVxuICB9XG5cbiAgYWRkUGFuZWwgKG5hbWUsIHBhbmVsQ3JlYXRlQ2FsbGJhY2spIHtcbiAgICB0aGlzLnBhbmVsQ3JlYXRlQ2FsbGJhY2tzW25hbWVdID0gcGFuZWxDcmVhdGVDYWxsYmFja1xuICAgIGlmICh0aGlzLmRlZmVycmVkUGFuZWwgJiYgdGhpcy5kZWZlcnJlZFBhbmVsLm5hbWUgPT09IG5hbWUpIHtcbiAgICAgIHRoaXMuc2hvd0RlZmVycmVkUGFuZWwoKVxuICAgIH1cbiAgfVxuXG4gIGdldE9yQ3JlYXRlUGFuZWwgKG5hbWUsIG9wdGlvbnMpIHtcbiAgICBsZXQgcGFuZWwgPSB0aGlzLnBhbmVsc0J5TmFtZVtuYW1lXVxuICAgIGlmIChwYW5lbCkgcmV0dXJuIHBhbmVsXG5cbiAgICBpZiAobmFtZSBpbiB0aGlzLnBhbmVsQ3JlYXRlQ2FsbGJhY2tzKSB7XG4gICAgICBwYW5lbCA9IHRoaXMucGFuZWxDcmVhdGVDYWxsYmFja3NbbmFtZV0oKVxuICAgICAgZGVsZXRlIHRoaXMucGFuZWxDcmVhdGVDYWxsYmFja3NbbmFtZV1cbiAgICB9IGVsc2UgaWYgKG9wdGlvbnMgJiYgb3B0aW9ucy5wYWNrKSB7XG4gICAgICBpZiAoIW9wdGlvbnMucGFjay5tZXRhZGF0YSkge1xuICAgICAgICBvcHRpb25zLnBhY2subWV0YWRhdGEgPSBfLmNsb25lKG9wdGlvbnMucGFjaylcbiAgICAgIH1cbiAgICAgIHBhbmVsID0gbmV3IFBhY2thZ2VEZXRhaWxWaWV3KG9wdGlvbnMucGFjaywgdGhpcywgdGhpcy5wYWNrYWdlTWFuYWdlciwgdGhpcy5zbmlwcGV0c1Byb3ZpZGVyKVxuICAgIH1cbiAgICBpZiAocGFuZWwpIHtcbiAgICAgIHRoaXMucGFuZWxzQnlOYW1lW25hbWVdID0gcGFuZWxcbiAgICB9XG5cbiAgICByZXR1cm4gcGFuZWxcbiAgfVxuXG4gIG1ha2VQYW5lbE1lbnVBY3RpdmUgKG5hbWUpIHtcbiAgICBjb25zdCBwcmV2aW91c2x5QWN0aXZlUGFuZWwgPSB0aGlzLnJlZnMuc2lkZWJhci5xdWVyeVNlbGVjdG9yKCcuYWN0aXZlJylcbiAgICBpZiAocHJldmlvdXNseUFjdGl2ZVBhbmVsKSB7XG4gICAgICBwcmV2aW91c2x5QWN0aXZlUGFuZWwuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJylcbiAgICB9XG5cbiAgICBjb25zdCBuZXdBY3RpdmVQYW5lbCA9IHRoaXMucmVmcy5zaWRlYmFyLnF1ZXJ5U2VsZWN0b3IoYFtuYW1lPScke25hbWV9J11gKVxuICAgIGlmIChuZXdBY3RpdmVQYW5lbCkge1xuICAgICAgbmV3QWN0aXZlUGFuZWwuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcbiAgICB9XG4gIH1cblxuICBmb2N1c0FjdGl2ZVBhbmVsICgpIHtcbiAgICAvLyBQYXNzIGZvY3VzIHRvIHBhbmVsIHRoYXQgaXMgY3VycmVudGx5IHZpc2libGVcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucmVmcy5wYW5lbHMuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgIGNvbnN0IGNoaWxkID0gdGhpcy5yZWZzLnBhbmVscy5jaGlsZHJlbltpXVxuICAgICAgaWYgKGNoaWxkLm9mZnNldFdpZHRoID4gMCkge1xuICAgICAgICBjaGlsZC5mb2N1cygpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgc2hvd0RlZmVycmVkUGFuZWwgKCkge1xuICAgIGlmICh0aGlzLmRlZmVycmVkUGFuZWwpIHtcbiAgICAgIGNvbnN0IHtuYW1lLCBvcHRpb25zfSA9IHRoaXMuZGVmZXJyZWRQYW5lbFxuICAgICAgdGhpcy5zaG93UGFuZWwobmFtZSwgb3B0aW9ucylcbiAgICB9XG4gIH1cblxuICAvLyBQdWJsaWM6IHNob3cgYSBwYW5lbC5cbiAgLy9cbiAgLy8gKiBgbmFtZWAge1N0cmluZ30gdGhlIG5hbWUgb2YgdGhlIHBhbmVsIHRvIHNob3dcbiAgLy8gKiBgb3B0aW9uc2Age09iamVjdH0gYW4gb3B0aW9ucyBoYXNoLiBXaWxsIGJlIHBhc3NlZCB0byBgYmVmb3JlU2hvdygpYCBvblxuICAvLyAgIHRoZSBwYW5lbC4gT3B0aW9ucyBtYXkgaW5jbHVkZSAoYnV0IGFyZSBub3QgbGltaXRlZCB0byk6XG4gIC8vICAgKiBgdXJpYCB0aGUgVVJJIHRoZSBwYW5lbCB3YXMgbGF1bmNoZWQgZnJvbVxuICBzaG93UGFuZWwgKG5hbWUsIG9wdGlvbnMpIHtcbiAgICBpZiAodGhpcy5hY3RpdmVQYW5lbCkge1xuICAgICAgY29uc3QgcHJldiA9IHRoaXMucGFuZWxzQnlOYW1lW3RoaXMuYWN0aXZlUGFuZWwubmFtZV1cbiAgICAgIGlmIChwcmV2KSB7XG4gICAgICAgIHByZXYuc2Nyb2xsUG9zaXRpb24gPSBwcmV2LmVsZW1lbnQuc2Nyb2xsVG9wXG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgcGFuZWwgPSB0aGlzLmdldE9yQ3JlYXRlUGFuZWwobmFtZSwgb3B0aW9ucylcbiAgICBpZiAocGFuZWwpIHtcbiAgICAgIHRoaXMuYXBwZW5kUGFuZWwocGFuZWwsIG9wdGlvbnMpXG4gICAgICB0aGlzLm1ha2VQYW5lbE1lbnVBY3RpdmUobmFtZSlcbiAgICAgIHRoaXMuc2V0QWN0aXZlUGFuZWwobmFtZSwgb3B0aW9ucylcbiAgICAgIHRoaXMuZGVmZXJyZWRQYW5lbCA9IG51bGxcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5kZWZlcnJlZFBhbmVsID0ge25hbWUsIG9wdGlvbnN9XG4gICAgfVxuICB9XG5cbiAgc2hvd1BhbmVsRm9yVVJJICh1cmkpIHtcbiAgICBjb25zdCByZWdleCA9IC9jb25maWdcXC8oW2Etel0rKVxcLz8oW2EtekEtWjAtOV8tXSspPy9pXG4gICAgY29uc3QgbWF0Y2ggPSByZWdleC5leGVjKHVyaSlcblxuICAgIGlmIChtYXRjaCkge1xuICAgICAgY29uc3QgcGF0aDEgPSBtYXRjaFsxXVxuICAgICAgY29uc3QgcGF0aDIgPSBtYXRjaFsyXVxuXG4gICAgICBpZiAocGF0aDEgPT09ICdwYWNrYWdlcycgJiYgcGF0aDIgIT0gbnVsbCkge1xuICAgICAgICB0aGlzLnNob3dQYW5lbChwYXRoMiwge1xuICAgICAgICAgIHVyaTogdXJpLFxuICAgICAgICAgIHBhY2s6IHtuYW1lOiBwYXRoMn0sXG4gICAgICAgICAgYmFjazogYXRvbS5wYWNrYWdlcy5nZXRMb2FkZWRQYWNrYWdlKHBhdGgyKSA/ICdQYWNrYWdlcycgOiBudWxsXG4gICAgICAgIH0pXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCBwYW5lbE5hbWUgPSBwYXRoMVswXS50b1VwcGVyQ2FzZSgpICsgcGF0aDEuc2xpY2UoMSlcbiAgICAgICAgdGhpcy5zaG93UGFuZWwocGFuZWxOYW1lLCB7dXJpfSlcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBhcHBlbmRQYW5lbCAocGFuZWwsIG9wdGlvbnMpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucmVmcy5wYW5lbHMuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMucmVmcy5wYW5lbHMuY2hpbGRyZW5baV0uc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgIH1cblxuICAgIGlmICghdGhpcy5yZWZzLnBhbmVscy5jb250YWlucyhwYW5lbC5lbGVtZW50KSkge1xuICAgICAgdGhpcy5yZWZzLnBhbmVscy5hcHBlbmRDaGlsZChwYW5lbC5lbGVtZW50KVxuICAgIH1cblxuICAgIGlmIChwYW5lbC5iZWZvcmVTaG93KSB7XG4gICAgICBwYW5lbC5iZWZvcmVTaG93KG9wdGlvbnMpXG4gICAgfVxuICAgIHBhbmVsLnNob3coKVxuICAgIHBhbmVsLmZvY3VzKClcbiAgfVxuXG4gIHNldEFjdGl2ZVBhbmVsIChuYW1lLCBvcHRpb25zID0ge30pIHtcbiAgICB0aGlzLmFjdGl2ZVBhbmVsID0ge25hbWUsIG9wdGlvbnN9XG5cbiAgICBjb25zdCBwYW5lbCA9IHRoaXMucGFuZWxzQnlOYW1lW25hbWVdXG4gICAgaWYgKHBhbmVsICYmIHBhbmVsLnNjcm9sbFBvc2l0aW9uKSB7XG4gICAgICBwYW5lbC5lbGVtZW50LnNjcm9sbFRvcCA9IHBhbmVsLnNjcm9sbFBvc2l0aW9uXG4gICAgICBkZWxldGUgcGFuZWwuc2Nyb2xsUG9zaXRpb25cbiAgICB9XG4gIH1cblxuICByZW1vdmVQYW5lbCAobmFtZSkge1xuICAgIGNvbnN0IHBhbmVsID0gdGhpcy5wYW5lbHNCeU5hbWVbbmFtZV1cbiAgICBpZiAocGFuZWwpIHtcbiAgICAgIHBhbmVsLmRlc3Ryb3koKVxuICAgICAgZGVsZXRlIHRoaXMucGFuZWxzQnlOYW1lW25hbWVdXG4gICAgfVxuICB9XG5cbiAgZ2V0VGl0bGUgKCkge1xuICAgIHJldHVybiAnU2V0dGluZ3MnXG4gIH1cblxuICBnZXRJY29uTmFtZSAoKSB7XG4gICAgcmV0dXJuICd0b29scydcbiAgfVxuXG4gIGdldFVSSSAoKSB7XG4gICAgcmV0dXJuIHRoaXMudXJpXG4gIH1cblxuICBpc0VxdWFsIChvdGhlcikge1xuICAgIHJldHVybiBvdGhlciBpbnN0YW5jZW9mIFNldHRpbmdzVmlld1xuICB9XG5cbiAgc2Nyb2xsVXAgKCkge1xuICAgIHRoaXMuZWxlbWVudC5zY3JvbGxUb3AgLT0gZG9jdW1lbnQuYm9keS5vZmZzZXRIZWlnaHQgLyAyMFxuICB9XG5cbiAgc2Nyb2xsRG93biAoKSB7XG4gICAgdGhpcy5lbGVtZW50LnNjcm9sbFRvcCArPSBkb2N1bWVudC5ib2R5Lm9mZnNldEhlaWdodCAvIDIwXG4gIH1cblxuICBwYWdlVXAgKCkge1xuICAgIHRoaXMuZWxlbWVudC5zY3JvbGxUb3AgLT0gdGhpcy5lbGVtZW50Lm9mZnNldEhlaWdodFxuICB9XG5cbiAgcGFnZURvd24gKCkge1xuICAgIHRoaXMuZWxlbWVudC5zY3JvbGxUb3AgKz0gdGhpcy5lbGVtZW50Lm9mZnNldEhlaWdodFxuICB9XG5cbiAgc2Nyb2xsVG9Ub3AgKCkge1xuICAgIHRoaXMuZWxlbWVudC5zY3JvbGxUb3AgPSAwXG4gIH1cblxuICBzY3JvbGxUb0JvdHRvbSAoKSB7XG4gICAgdGhpcy5lbGVtZW50LnNjcm9sbFRvcCA9IHRoaXMuZWxlbWVudC5zY3JvbGxIZWlnaHRcbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFpRDtBQWhCakQ7QUFDQTs7QUFpQmUsTUFBTUEsWUFBWSxDQUFDO0VBQ2hDQyxXQUFXLENBQUU7SUFBQ0MsR0FBRztJQUFFQyxjQUFjO0lBQUVDLGdCQUFnQjtJQUFFQztFQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtJQUN0RSxJQUFJLENBQUNILEdBQUcsR0FBR0EsR0FBRztJQUNkLElBQUksQ0FBQ0MsY0FBYyxHQUFHQSxjQUFjO0lBQ3BDLElBQUksQ0FBQ0MsZ0JBQWdCLEdBQUdBLGdCQUFnQjtJQUN4QyxJQUFJLENBQUNFLGFBQWEsR0FBR0QsV0FBVztJQUNoQyxJQUFJLENBQUNFLFNBQVMsR0FBRyxLQUFLO0lBQ3RCLElBQUksQ0FBQ0MsWUFBWSxHQUFHLENBQUMsQ0FBQztJQUN0QixJQUFJLENBQUNDLG9CQUFvQixHQUFHLENBQUMsQ0FBQztJQUU5QkMsYUFBSSxDQUFDQyxVQUFVLENBQUMsSUFBSSxDQUFDO0lBQ3JCLElBQUksQ0FBQ0MsV0FBVyxHQUFHLElBQUlDLHlCQUFtQixFQUFFO0lBQzVDLElBQUksQ0FBQ0QsV0FBVyxDQUFDRSxHQUFHLENBQUNDLElBQUksQ0FBQ0MsUUFBUSxDQUFDRixHQUFHLENBQUMsSUFBSSxDQUFDRyxPQUFPLEVBQUU7TUFDbkQsY0FBYyxFQUFFLE1BQU07UUFBRSxJQUFJLENBQUNDLFFBQVEsRUFBRTtNQUFDLENBQUM7TUFDekMsZ0JBQWdCLEVBQUUsTUFBTTtRQUFFLElBQUksQ0FBQ0MsVUFBVSxFQUFFO01BQUMsQ0FBQztNQUM3QyxjQUFjLEVBQUUsTUFBTTtRQUFFLElBQUksQ0FBQ0MsTUFBTSxFQUFFO01BQUMsQ0FBQztNQUN2QyxnQkFBZ0IsRUFBRSxNQUFNO1FBQUUsSUFBSSxDQUFDQyxRQUFRLEVBQUU7TUFBQyxDQUFDO01BQzNDLGtCQUFrQixFQUFFLE1BQU07UUFBRSxJQUFJLENBQUNDLFdBQVcsRUFBRTtNQUFDLENBQUM7TUFDaEQscUJBQXFCLEVBQUUsTUFBTTtRQUFFLElBQUksQ0FBQ0MsY0FBYyxFQUFFO01BQUM7SUFDdkQsQ0FBQyxDQUFDLENBQUM7SUFFSCxJQUFJLENBQUNYLFdBQVcsQ0FBQ0UsR0FBRyxDQUFDQyxJQUFJLENBQUNTLFFBQVEsQ0FBQ0MsNEJBQTRCLENBQUMsTUFBTTtNQUNwRSxJQUFJLENBQUNiLFdBQVcsQ0FBQ0UsR0FBRyxDQUNsQkMsSUFBSSxDQUFDUyxRQUFRLENBQUNFLG9CQUFvQixDQUFDQyxJQUFJLElBQUksSUFBSSxDQUFDQyxnQkFBZ0IsQ0FBQ0QsSUFBSSxDQUFDRSxJQUFJLENBQUMsQ0FBQyxFQUM1RWQsSUFBSSxDQUFDUyxRQUFRLENBQUNNLHNCQUFzQixDQUFDSCxJQUFJLElBQUksSUFBSSxDQUFDQyxnQkFBZ0IsQ0FBQ0QsSUFBSSxDQUFDRSxJQUFJLENBQUMsQ0FBQyxDQUMvRTtJQUNILENBQUMsQ0FBQyxDQUFDO0lBRUhFLE9BQU8sQ0FBQ0MsUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDQyxnQkFBZ0IsRUFBRSxDQUFDO0VBQ2pEO0VBRUFMLGdCQUFnQixDQUFFQyxJQUFJLEVBQUU7SUFDdEIsT0FBTyxJQUFJLENBQUNyQixZQUFZLENBQUNxQixJQUFJLENBQUM7RUFDaEM7RUFFQUssTUFBTSxHQUFJLENBQUM7RUFFWEMsT0FBTyxHQUFJO0lBQ1QsSUFBSSxDQUFDNUIsU0FBUyxHQUFHLElBQUk7SUFDckIsSUFBSSxDQUFDSyxXQUFXLENBQUN3QixPQUFPLEVBQUU7SUFDMUIsS0FBSyxJQUFJUCxJQUFJLElBQUksSUFBSSxDQUFDckIsWUFBWSxFQUFFO01BQ2xDLE1BQU02QixLQUFLLEdBQUcsSUFBSSxDQUFDN0IsWUFBWSxDQUFDcUIsSUFBSSxDQUFDO01BQ3JDUSxLQUFLLENBQUNGLE9BQU8sRUFBRTtJQUNqQjtJQUVBLE9BQU96QixhQUFJLENBQUN5QixPQUFPLENBQUMsSUFBSSxDQUFDO0VBQzNCO0VBRUFHLE1BQU0sR0FBSTtJQUNSLE9BQ0U7TUFBSyxTQUFTLEVBQUMseUJBQXlCO01BQUMsUUFBUSxFQUFDO0lBQUksR0FDcEQ7TUFBSyxTQUFTLEVBQUMsYUFBYTtNQUFDLEdBQUcsRUFBQztJQUFTLEdBQ3hDO01BQUksU0FBUyxFQUFDLHVDQUF1QztNQUFDLEdBQUcsRUFBQztJQUFXLEdBQ25FO01BQUssU0FBUyxFQUFDLHNCQUFzQjtNQUFDLEdBQUcsRUFBQztJQUFlLEVBQUcsQ0FDekQsRUFDTDtNQUFLLFNBQVMsRUFBQztJQUFhLEdBQzFCO01BQVEsU0FBUyxFQUFDLHlDQUF5QztNQUFDLEdBQUcsRUFBQztJQUFhLHdCQUE0QixDQUNyRyxDQUNGLEVBVU47TUFBSyxTQUFTLEVBQUMsUUFBUTtNQUFDLFFBQVEsRUFBQyxJQUFJO01BQUMsR0FBRyxFQUFDO0lBQVEsRUFBRyxDQUNqRDtFQUVWOztFQUVBO0VBQ0E7RUFDQTtFQUNBQyxnQkFBZ0IsR0FBSTtJQUFFLE9BQU8sSUFBSUMsZ0JBQVUsRUFBRTtFQUFDO0VBRTlDUCxnQkFBZ0IsR0FBSTtJQUNsQixJQUFJLElBQUksQ0FBQ1EsSUFBSSxDQUFDQyxNQUFNLENBQUNDLFFBQVEsQ0FBQ0MsTUFBTSxHQUFHLENBQUMsRUFBRTtNQUN4QztJQUNGO0lBRUEsTUFBTUMsWUFBWSxHQUFJQyxLQUFLLElBQUs7TUFDOUIsTUFBTUMsTUFBTSxHQUFHRCxLQUFLLENBQUNDLE1BQU0sQ0FBQ0MsT0FBTyxDQUFDLDBDQUEwQyxDQUFDO01BQy9FLElBQUlELE1BQU0sRUFBRTtRQUNWLElBQUksQ0FBQ0UsU0FBUyxDQUFDRixNQUFNLENBQUNDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQ25CLElBQUksQ0FBQztNQUMzQztJQUNGLENBQUM7SUFDRCxJQUFJLENBQUNaLE9BQU8sQ0FBQ2lDLGdCQUFnQixDQUFDLE9BQU8sRUFBRUwsWUFBWSxDQUFDO0lBQ3BELElBQUksQ0FBQ2pDLFdBQVcsQ0FBQ0UsR0FBRyxDQUFDLElBQUkwQixnQkFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDdkIsT0FBTyxDQUFDa0MsbUJBQW1CLENBQUMsT0FBTyxFQUFFTixZQUFZLENBQUMsQ0FBQyxDQUFDO0lBRW5HLE1BQU1PLFlBQVksR0FBRyxNQUFNO01BQ3pCLElBQUksQ0FBQ0MsZ0JBQWdCLEVBQUU7SUFDekIsQ0FBQztJQUNELElBQUksQ0FBQ3BDLE9BQU8sQ0FBQ2lDLGdCQUFnQixDQUFDLE9BQU8sRUFBRUUsWUFBWSxDQUFDO0lBQ3BELElBQUksQ0FBQ3hDLFdBQVcsQ0FBQ0UsR0FBRyxDQUFDLElBQUkwQixnQkFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDdkIsT0FBTyxDQUFDa0MsbUJBQW1CLENBQUMsT0FBTyxFQUFFQyxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBRW5HLE1BQU1FLHVCQUF1QixHQUFHLE1BQU07TUFDcEN2QyxJQUFJLENBQUN3QyxJQUFJLENBQUM7UUFBQ0MsV0FBVyxFQUFFLENBQUN6QyxJQUFJLENBQUMwQyxnQkFBZ0IsRUFBRTtNQUFDLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBQ0QsSUFBSSxDQUFDaEIsSUFBSSxDQUFDaUIsV0FBVyxDQUFDUixnQkFBZ0IsQ0FBQyxPQUFPLEVBQUVJLHVCQUF1QixDQUFDO0lBQ3hFLElBQUksQ0FBQzFDLFdBQVcsQ0FBQ0UsR0FBRyxDQUFDLElBQUkwQixnQkFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDQyxJQUFJLENBQUNpQixXQUFXLENBQUNQLG1CQUFtQixDQUFDLE9BQU8sRUFBRUcsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO0lBRXZILElBQUksQ0FBQ0ssWUFBWSxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxJQUFJQyxxQkFBWSxFQUFFLENBQUM7SUFDL0QsSUFBSSxDQUFDRCxZQUFZLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxNQUFNLElBQUlFLG9CQUFXLEVBQUUsQ0FBQztJQUM1RCxJQUFJOUMsSUFBSSxDQUFDK0MsTUFBTSxDQUFDQyxTQUFTLENBQUMsNkJBQTZCLENBQUMsQ0FBQ0MsSUFBSSxLQUFLLEtBQUssRUFBRTtNQUN2RTtNQUNBLElBQUksQ0FBQ0wsWUFBWSxDQUFDLGNBQWMsRUFBRSxNQUFNLEVBQUUsTUFBTSxJQUFJTSx3QkFBZSxFQUFFLENBQUM7SUFDeEU7SUFDQSxJQUFLbEMsT0FBTyxDQUFDbUMsUUFBUSxLQUFLLE9BQU8sSUFBTUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDQyxRQUFRLElBQUksSUFBSyxFQUFFO01BQ3hFLE1BQU1DLFdBQVcsR0FBR0YsT0FBTyxDQUFDLHdCQUF3QixDQUFDO01BQ3JELElBQUksQ0FBQ1IsWUFBWSxDQUFDLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLElBQUlVLFdBQVcsRUFBRSxDQUFDO0lBQ3hFO0lBQ0EsSUFBSSxDQUFDVixZQUFZLENBQUMsYUFBYSxFQUFFLFVBQVUsRUFBRSxNQUFNLElBQUlXLHlCQUFnQixFQUFFLENBQUM7SUFDMUUsSUFBSSxDQUFDWCxZQUFZLENBQUMsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLElBQUlZLCtCQUFzQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUNwRSxjQUFjLENBQUMsQ0FBQztJQUNyRyxJQUFJLENBQUN3RCxZQUFZLENBQUMsUUFBUSxFQUFFLFVBQVUsRUFBRSxNQUFNLElBQUlhLG9CQUFXLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQ3JFLGNBQWMsQ0FBQyxDQUFDO0lBQ3pGLElBQUksQ0FBQ3dELFlBQVksQ0FBQyxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxJQUFJYyxxQkFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUN0RSxjQUFjLENBQUMsQ0FBQztJQUNqRyxJQUFJLENBQUN3RCxZQUFZLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxNQUFNLElBQUllLHFCQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQ3ZFLGNBQWMsQ0FBQyxDQUFDO0lBRXZGLElBQUksQ0FBQ3dFLGlCQUFpQixFQUFFO0lBRXhCLElBQUksQ0FBQyxJQUFJLENBQUN0RSxXQUFXLEVBQUU7TUFDckIsSUFBSSxDQUFDNEMsU0FBUyxDQUFDLE1BQU0sQ0FBQztJQUN4QjtJQUVBLElBQUkyQixRQUFRLENBQUNDLElBQUksQ0FBQ0MsUUFBUSxDQUFDLElBQUksQ0FBQzdELE9BQU8sQ0FBQyxFQUFFO01BQ3hDLElBQUksQ0FBQ3dCLElBQUksQ0FBQ3NDLE9BQU8sQ0FBQ0MsS0FBSyxDQUFDQyxLQUFLLEdBQUcsSUFBSSxDQUFDeEMsSUFBSSxDQUFDc0MsT0FBTyxDQUFDRyxXQUFXO0lBQy9EO0VBQ0Y7RUFFQUMsU0FBUyxHQUFJO0lBQ1gsT0FBTztNQUNMQyxZQUFZLEVBQUUsY0FBYztNQUM1QkMsT0FBTyxFQUFFLENBQUM7TUFDVmhGLFdBQVcsRUFBRSxJQUFJLENBQUNBLFdBQVcsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDQSxXQUFXLEdBQUcsSUFBSSxDQUFDQyxhQUFhO01BQzdFSixHQUFHLEVBQUUsSUFBSSxDQUFDQTtJQUNaLENBQUM7RUFDSDtFQUVBb0YsV0FBVyxHQUFJO0lBQ2IsSUFBSUMsMkJBQTJCO0lBQy9CLElBQUksSUFBSSxDQUFDL0QsUUFBUSxJQUFJLElBQUksRUFBRTtNQUFFLE9BQU8sSUFBSSxDQUFDQSxRQUFRO0lBQUM7SUFFbEQsSUFBSSxDQUFDQSxRQUFRLEdBQUdULElBQUksQ0FBQ1MsUUFBUSxDQUFDZ0UsaUJBQWlCLEVBQUU7SUFFakQsSUFBSTtNQUNGLE1BQU1DLGVBQWUsR0FBR3RCLE9BQU8sQ0FBQ3VCLGFBQUksQ0FBQ0MsSUFBSSxDQUFDNUUsSUFBSSxDQUFDNkUsZUFBZSxFQUFFLENBQUNDLFlBQVksRUFBRSxjQUFjLENBQUMsQ0FBQztNQUMvRk4sMkJBQTJCLEdBQUdFLGVBQWUsR0FBR0EsZUFBZSxDQUFDSyxhQUFhLEdBQUcsSUFBSTtJQUN0RixDQUFDLENBQUMsT0FBT0MsS0FBSyxFQUFFLENBQUM7O0lBRWpCO0lBQ0EsTUFBTUMsZ0JBQWdCLEdBQUdqRixJQUFJLENBQUMrQyxNQUFNLENBQUNtQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFO0lBQ3ZFLEtBQUssTUFBTUMsV0FBVyxJQUFJRixnQkFBZ0IsRUFBRTtNQUMxQyxJQUFJRyxRQUFRO01BQ1osTUFBTUMsV0FBVyxHQUFHckYsSUFBSSxDQUFDUyxRQUFRLENBQUM2RSxrQkFBa0IsQ0FBQ0gsV0FBVyxDQUFDO01BQ2pFLElBQUksQ0FBQ0UsV0FBVyxFQUFFO1FBQ2hCO01BQ0Y7TUFFQSxJQUFJO1FBQ0ZELFFBQVEsR0FBR2hDLE9BQU8sQ0FBQ3VCLGFBQUksQ0FBQ0MsSUFBSSxDQUFDUyxXQUFXLEVBQUUsY0FBYyxDQUFDLENBQUM7TUFDNUQsQ0FBQyxDQUFDLE9BQU9MLEtBQUssRUFBRTtRQUNkLElBQUlSLDJCQUEyQixJQUFJQSwyQkFBMkIsQ0FBQ1csV0FBVyxDQUFDLEVBQUU7VUFDM0VDLFFBQVEsR0FBR1osMkJBQTJCLENBQUNXLFdBQVcsQ0FBQyxDQUFDQyxRQUFRO1FBQzlEO01BQ0Y7TUFDQSxJQUFJQSxRQUFRLElBQUksSUFBSSxFQUFFO1FBQ3BCO01BQ0Y7TUFFQSxNQUFNdEUsSUFBSSxHQUFHc0UsUUFBUSxDQUFDdEUsSUFBSSxJQUFJLElBQUksR0FBR3NFLFFBQVEsQ0FBQ3RFLElBQUksR0FBR3FFLFdBQVc7TUFDaEUsSUFBSSxDQUFDSSx1QkFBQyxDQUFDQyxTQUFTLENBQUMsSUFBSSxDQUFDL0UsUUFBUSxFQUFFO1FBQUNLO01BQUksQ0FBQyxDQUFDLEVBQUU7UUFDdkMsSUFBSSxDQUFDTCxRQUFRLENBQUNnRixJQUFJLENBQUM7VUFBQzNFLElBQUk7VUFBRXNFLFFBQVE7VUFBRVQsSUFBSSxFQUFFVTtRQUFXLENBQUMsQ0FBQztNQUN6RDtJQUNGO0lBRUEsSUFBSSxDQUFDNUUsUUFBUSxDQUFDaUYsSUFBSSxDQUFDLENBQUNDLEtBQUssRUFBRUMsS0FBSyxLQUFLO01BQ25DLE1BQU1DLE1BQU0sR0FBRyxJQUFJLENBQUN6RyxjQUFjLENBQUMwRyxlQUFlLENBQUNILEtBQUssQ0FBQztNQUN6RCxNQUFNSSxNQUFNLEdBQUcsSUFBSSxDQUFDM0csY0FBYyxDQUFDMEcsZUFBZSxDQUFDRixLQUFLLENBQUM7TUFDekQsT0FBT0MsTUFBTSxDQUFDRyxhQUFhLENBQUNELE1BQU0sQ0FBQztJQUNyQyxDQUFDLENBQUM7SUFFRixPQUFPLElBQUksQ0FBQ3RGLFFBQVE7RUFDdEI7RUFFQW1DLFlBQVksQ0FBRTlCLElBQUksRUFBRW1GLFFBQVEsRUFBRUMsbUJBQW1CLEVBQUU7SUFDakQsTUFBTUMsYUFBYSxHQUFHdEMsUUFBUSxDQUFDdUMsYUFBYSxDQUFDLElBQUksQ0FBQztJQUNsREQsYUFBYSxDQUFDckYsSUFBSSxHQUFHQSxJQUFJO0lBQ3pCcUYsYUFBYSxDQUFDRSxZQUFZLENBQUMsTUFBTSxFQUFFdkYsSUFBSSxDQUFDO0lBRXhDLE1BQU13RixDQUFDLEdBQUd6QyxRQUFRLENBQUN1QyxhQUFhLENBQUMsR0FBRyxDQUFDO0lBQ3JDRSxDQUFDLENBQUNDLFNBQVMsQ0FBQ3hHLEdBQUcsQ0FBQyxNQUFNLEVBQUcsUUFBT2tHLFFBQVMsRUFBQyxDQUFDO0lBQzNDSyxDQUFDLENBQUNFLFdBQVcsR0FBRzFGLElBQUk7SUFDcEJxRixhQUFhLENBQUNNLFdBQVcsQ0FBQ0gsQ0FBQyxDQUFDO0lBRTVCLElBQUksQ0FBQzVFLElBQUksQ0FBQ2dGLGFBQWEsQ0FBQ0MsYUFBYSxDQUFDQyxZQUFZLENBQUNULGFBQWEsRUFBRSxJQUFJLENBQUN6RSxJQUFJLENBQUNnRixhQUFhLENBQUM7SUFDMUYsSUFBSSxDQUFDRyxRQUFRLENBQUMvRixJQUFJLEVBQUVvRixtQkFBbUIsQ0FBQztFQUMxQztFQUVBVyxRQUFRLENBQUUvRixJQUFJLEVBQUVvRixtQkFBbUIsRUFBRTtJQUNuQyxJQUFJLENBQUN4RyxvQkFBb0IsQ0FBQ29CLElBQUksQ0FBQyxHQUFHb0YsbUJBQW1CO0lBQ3JELElBQUksSUFBSSxDQUFDM0csYUFBYSxJQUFJLElBQUksQ0FBQ0EsYUFBYSxDQUFDdUIsSUFBSSxLQUFLQSxJQUFJLEVBQUU7TUFDMUQsSUFBSSxDQUFDOEMsaUJBQWlCLEVBQUU7SUFDMUI7RUFDRjtFQUVBa0QsZ0JBQWdCLENBQUVoRyxJQUFJLEVBQUVpRyxPQUFPLEVBQUU7SUFDL0IsSUFBSXpGLEtBQUssR0FBRyxJQUFJLENBQUM3QixZQUFZLENBQUNxQixJQUFJLENBQUM7SUFDbkMsSUFBSVEsS0FBSyxFQUFFLE9BQU9BLEtBQUs7SUFFdkIsSUFBSVIsSUFBSSxJQUFJLElBQUksQ0FBQ3BCLG9CQUFvQixFQUFFO01BQ3JDNEIsS0FBSyxHQUFHLElBQUksQ0FBQzVCLG9CQUFvQixDQUFDb0IsSUFBSSxDQUFDLEVBQUU7TUFDekMsT0FBTyxJQUFJLENBQUNwQixvQkFBb0IsQ0FBQ29CLElBQUksQ0FBQztJQUN4QyxDQUFDLE1BQU0sSUFBSWlHLE9BQU8sSUFBSUEsT0FBTyxDQUFDbkcsSUFBSSxFQUFFO01BQ2xDLElBQUksQ0FBQ21HLE9BQU8sQ0FBQ25HLElBQUksQ0FBQ3dFLFFBQVEsRUFBRTtRQUMxQjJCLE9BQU8sQ0FBQ25HLElBQUksQ0FBQ3dFLFFBQVEsR0FBR0csdUJBQUMsQ0FBQ3lCLEtBQUssQ0FBQ0QsT0FBTyxDQUFDbkcsSUFBSSxDQUFDO01BQy9DO01BQ0FVLEtBQUssR0FBRyxJQUFJMkYsMEJBQWlCLENBQUNGLE9BQU8sQ0FBQ25HLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDeEIsY0FBYyxFQUFFLElBQUksQ0FBQ0MsZ0JBQWdCLENBQUM7SUFDL0Y7SUFDQSxJQUFJaUMsS0FBSyxFQUFFO01BQ1QsSUFBSSxDQUFDN0IsWUFBWSxDQUFDcUIsSUFBSSxDQUFDLEdBQUdRLEtBQUs7SUFDakM7SUFFQSxPQUFPQSxLQUFLO0VBQ2Q7RUFFQTRGLG1CQUFtQixDQUFFcEcsSUFBSSxFQUFFO0lBQ3pCLE1BQU1xRyxxQkFBcUIsR0FBRyxJQUFJLENBQUN6RixJQUFJLENBQUNzQyxPQUFPLENBQUNvRCxhQUFhLENBQUMsU0FBUyxDQUFDO0lBQ3hFLElBQUlELHFCQUFxQixFQUFFO01BQ3pCQSxxQkFBcUIsQ0FBQ1osU0FBUyxDQUFDYyxNQUFNLENBQUMsUUFBUSxDQUFDO0lBQ2xEO0lBRUEsTUFBTUMsY0FBYyxHQUFHLElBQUksQ0FBQzVGLElBQUksQ0FBQ3NDLE9BQU8sQ0FBQ29ELGFBQWEsQ0FBRSxVQUFTdEcsSUFBSyxJQUFHLENBQUM7SUFDMUUsSUFBSXdHLGNBQWMsRUFBRTtNQUNsQkEsY0FBYyxDQUFDZixTQUFTLENBQUN4RyxHQUFHLENBQUMsUUFBUSxDQUFDO0lBQ3hDO0VBQ0Y7RUFFQXVDLGdCQUFnQixHQUFJO0lBQ2xCO0lBQ0EsS0FBSyxJQUFJaUYsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHLElBQUksQ0FBQzdGLElBQUksQ0FBQ0MsTUFBTSxDQUFDQyxRQUFRLENBQUNDLE1BQU0sRUFBRTBGLENBQUMsRUFBRSxFQUFFO01BQ3pELE1BQU1DLEtBQUssR0FBRyxJQUFJLENBQUM5RixJQUFJLENBQUNDLE1BQU0sQ0FBQ0MsUUFBUSxDQUFDMkYsQ0FBQyxDQUFDO01BQzFDLElBQUlDLEtBQUssQ0FBQ3JELFdBQVcsR0FBRyxDQUFDLEVBQUU7UUFDekJxRCxLQUFLLENBQUNDLEtBQUssRUFBRTtNQUNmO0lBQ0Y7RUFDRjtFQUVBN0QsaUJBQWlCLEdBQUk7SUFDbkIsSUFBSSxJQUFJLENBQUNyRSxhQUFhLEVBQUU7TUFDdEIsTUFBTTtRQUFDdUIsSUFBSTtRQUFFaUc7TUFBTyxDQUFDLEdBQUcsSUFBSSxDQUFDeEgsYUFBYTtNQUMxQyxJQUFJLENBQUMyQyxTQUFTLENBQUNwQixJQUFJLEVBQUVpRyxPQUFPLENBQUM7SUFDL0I7RUFDRjs7RUFFQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTdFLFNBQVMsQ0FBRXBCLElBQUksRUFBRWlHLE9BQU8sRUFBRTtJQUN4QixJQUFJLElBQUksQ0FBQ3pILFdBQVcsRUFBRTtNQUNwQixNQUFNb0ksSUFBSSxHQUFHLElBQUksQ0FBQ2pJLFlBQVksQ0FBQyxJQUFJLENBQUNILFdBQVcsQ0FBQ3dCLElBQUksQ0FBQztNQUNyRCxJQUFJNEcsSUFBSSxFQUFFO1FBQ1JBLElBQUksQ0FBQ0MsY0FBYyxHQUFHRCxJQUFJLENBQUN4SCxPQUFPLENBQUMwSCxTQUFTO01BQzlDO0lBQ0Y7SUFFQSxNQUFNdEcsS0FBSyxHQUFHLElBQUksQ0FBQ3dGLGdCQUFnQixDQUFDaEcsSUFBSSxFQUFFaUcsT0FBTyxDQUFDO0lBQ2xELElBQUl6RixLQUFLLEVBQUU7TUFDVCxJQUFJLENBQUN1RyxXQUFXLENBQUN2RyxLQUFLLEVBQUV5RixPQUFPLENBQUM7TUFDaEMsSUFBSSxDQUFDRyxtQkFBbUIsQ0FBQ3BHLElBQUksQ0FBQztNQUM5QixJQUFJLENBQUNnSCxjQUFjLENBQUNoSCxJQUFJLEVBQUVpRyxPQUFPLENBQUM7TUFDbEMsSUFBSSxDQUFDeEgsYUFBYSxHQUFHLElBQUk7SUFDM0IsQ0FBQyxNQUFNO01BQ0wsSUFBSSxDQUFDQSxhQUFhLEdBQUc7UUFBQ3VCLElBQUk7UUFBRWlHO01BQU8sQ0FBQztJQUN0QztFQUNGO0VBRUFnQixlQUFlLENBQUU1SSxHQUFHLEVBQUU7SUFDcEIsTUFBTTZJLEtBQUssR0FBRyx1Q0FBdUM7SUFDckQsTUFBTUMsS0FBSyxHQUFHRCxLQUFLLENBQUNFLElBQUksQ0FBQy9JLEdBQUcsQ0FBQztJQUU3QixJQUFJOEksS0FBSyxFQUFFO01BQ1QsTUFBTUUsS0FBSyxHQUFHRixLQUFLLENBQUMsQ0FBQyxDQUFDO01BQ3RCLE1BQU1HLEtBQUssR0FBR0gsS0FBSyxDQUFDLENBQUMsQ0FBQztNQUV0QixJQUFJRSxLQUFLLEtBQUssVUFBVSxJQUFJQyxLQUFLLElBQUksSUFBSSxFQUFFO1FBQ3pDLElBQUksQ0FBQ2xHLFNBQVMsQ0FBQ2tHLEtBQUssRUFBRTtVQUNwQmpKLEdBQUcsRUFBRUEsR0FBRztVQUNSeUIsSUFBSSxFQUFFO1lBQUNFLElBQUksRUFBRXNIO1VBQUssQ0FBQztVQUNuQkMsSUFBSSxFQUFFckksSUFBSSxDQUFDUyxRQUFRLENBQUM2SCxnQkFBZ0IsQ0FBQ0YsS0FBSyxDQUFDLEdBQUcsVUFBVSxHQUFHO1FBQzdELENBQUMsQ0FBQztNQUNKLENBQUMsTUFBTTtRQUNMLE1BQU1HLFNBQVMsR0FBR0osS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDSyxXQUFXLEVBQUUsR0FBR0wsS0FBSyxDQUFDTSxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQ3ZHLFNBQVMsQ0FBQ3FHLFNBQVMsRUFBRTtVQUFDcEo7UUFBRyxDQUFDLENBQUM7TUFDbEM7SUFDRjtFQUNGO0VBRUEwSSxXQUFXLENBQUV2RyxLQUFLLEVBQUV5RixPQUFPLEVBQUU7SUFDM0IsS0FBSyxJQUFJUSxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUcsSUFBSSxDQUFDN0YsSUFBSSxDQUFDQyxNQUFNLENBQUNDLFFBQVEsQ0FBQ0MsTUFBTSxFQUFFMEYsQ0FBQyxFQUFFLEVBQUU7TUFDekQsSUFBSSxDQUFDN0YsSUFBSSxDQUFDQyxNQUFNLENBQUNDLFFBQVEsQ0FBQzJGLENBQUMsQ0FBQyxDQUFDdEQsS0FBSyxDQUFDeUUsT0FBTyxHQUFHLE1BQU07SUFDckQ7SUFFQSxJQUFJLENBQUMsSUFBSSxDQUFDaEgsSUFBSSxDQUFDQyxNQUFNLENBQUNvQyxRQUFRLENBQUN6QyxLQUFLLENBQUNwQixPQUFPLENBQUMsRUFBRTtNQUM3QyxJQUFJLENBQUN3QixJQUFJLENBQUNDLE1BQU0sQ0FBQzhFLFdBQVcsQ0FBQ25GLEtBQUssQ0FBQ3BCLE9BQU8sQ0FBQztJQUM3QztJQUVBLElBQUlvQixLQUFLLENBQUNxSCxVQUFVLEVBQUU7TUFDcEJySCxLQUFLLENBQUNxSCxVQUFVLENBQUM1QixPQUFPLENBQUM7SUFDM0I7SUFDQXpGLEtBQUssQ0FBQ3NILElBQUksRUFBRTtJQUNadEgsS0FBSyxDQUFDbUcsS0FBSyxFQUFFO0VBQ2Y7RUFFQUssY0FBYyxDQUFFaEgsSUFBSSxFQUFFaUcsT0FBTyxHQUFHLENBQUMsQ0FBQyxFQUFFO0lBQ2xDLElBQUksQ0FBQ3pILFdBQVcsR0FBRztNQUFDd0IsSUFBSTtNQUFFaUc7SUFBTyxDQUFDO0lBRWxDLE1BQU16RixLQUFLLEdBQUcsSUFBSSxDQUFDN0IsWUFBWSxDQUFDcUIsSUFBSSxDQUFDO0lBQ3JDLElBQUlRLEtBQUssSUFBSUEsS0FBSyxDQUFDcUcsY0FBYyxFQUFFO01BQ2pDckcsS0FBSyxDQUFDcEIsT0FBTyxDQUFDMEgsU0FBUyxHQUFHdEcsS0FBSyxDQUFDcUcsY0FBYztNQUM5QyxPQUFPckcsS0FBSyxDQUFDcUcsY0FBYztJQUM3QjtFQUNGO0VBRUFrQixXQUFXLENBQUUvSCxJQUFJLEVBQUU7SUFDakIsTUFBTVEsS0FBSyxHQUFHLElBQUksQ0FBQzdCLFlBQVksQ0FBQ3FCLElBQUksQ0FBQztJQUNyQyxJQUFJUSxLQUFLLEVBQUU7TUFDVEEsS0FBSyxDQUFDRixPQUFPLEVBQUU7TUFDZixPQUFPLElBQUksQ0FBQzNCLFlBQVksQ0FBQ3FCLElBQUksQ0FBQztJQUNoQztFQUNGO0VBRUFnSSxRQUFRLEdBQUk7SUFDVixPQUFPLFVBQVU7RUFDbkI7RUFFQUMsV0FBVyxHQUFJO0lBQ2IsT0FBTyxPQUFPO0VBQ2hCO0VBRUFDLE1BQU0sR0FBSTtJQUNSLE9BQU8sSUFBSSxDQUFDN0osR0FBRztFQUNqQjtFQUVBOEosT0FBTyxDQUFFQyxLQUFLLEVBQUU7SUFDZCxPQUFPQSxLQUFLLFlBQVlqSyxZQUFZO0VBQ3RDO0VBRUFrQixRQUFRLEdBQUk7SUFDVixJQUFJLENBQUNELE9BQU8sQ0FBQzBILFNBQVMsSUFBSS9ELFFBQVEsQ0FBQ0MsSUFBSSxDQUFDcUYsWUFBWSxHQUFHLEVBQUU7RUFDM0Q7RUFFQS9JLFVBQVUsR0FBSTtJQUNaLElBQUksQ0FBQ0YsT0FBTyxDQUFDMEgsU0FBUyxJQUFJL0QsUUFBUSxDQUFDQyxJQUFJLENBQUNxRixZQUFZLEdBQUcsRUFBRTtFQUMzRDtFQUVBOUksTUFBTSxHQUFJO0lBQ1IsSUFBSSxDQUFDSCxPQUFPLENBQUMwSCxTQUFTLElBQUksSUFBSSxDQUFDMUgsT0FBTyxDQUFDaUosWUFBWTtFQUNyRDtFQUVBN0ksUUFBUSxHQUFJO0lBQ1YsSUFBSSxDQUFDSixPQUFPLENBQUMwSCxTQUFTLElBQUksSUFBSSxDQUFDMUgsT0FBTyxDQUFDaUosWUFBWTtFQUNyRDtFQUVBNUksV0FBVyxHQUFJO0lBQ2IsSUFBSSxDQUFDTCxPQUFPLENBQUMwSCxTQUFTLEdBQUcsQ0FBQztFQUM1QjtFQUVBcEgsY0FBYyxHQUFJO0lBQ2hCLElBQUksQ0FBQ04sT0FBTyxDQUFDMEgsU0FBUyxHQUFHLElBQUksQ0FBQzFILE9BQU8sQ0FBQ2tKLFlBQVk7RUFDcEQ7QUFDRjtBQUFDO0FBQUEifQ==