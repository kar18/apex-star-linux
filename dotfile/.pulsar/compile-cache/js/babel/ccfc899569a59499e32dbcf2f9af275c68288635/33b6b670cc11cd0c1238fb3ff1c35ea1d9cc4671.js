"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _etch = _interopRequireDefault(require("etch"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
class GuideView {
  constructor(props) {
    this.props = props;
    this.brand = atom.branding.name;
    this.didClickProjectButton = this.didClickProjectButton.bind(this);
    this.didClickGitButton = this.didClickGitButton.bind(this);
    this.didClickGitHubButton = this.didClickGitHubButton.bind(this);
    this.didClickTeletypeButton = this.didClickTeletypeButton.bind(this);
    this.didClickPackagesButton = this.didClickPackagesButton.bind(this);
    this.didClickThemesButton = this.didClickThemesButton.bind(this);
    this.didClickStylingButton = this.didClickStylingButton.bind(this);
    this.didClickInitScriptButton = this.didClickInitScriptButton.bind(this);
    this.didClickSnippetsButton = this.didClickSnippetsButton.bind(this);
    _etch.default.initialize(this);
  }
  update() {}
  render() {
    return _etch.default.dom("div", {
      className: "welcome is-guide"
    }, _etch.default.dom("div", {
      className: "welcome-container"
    }, _etch.default.dom("section", {
      className: "welcome-panel"
    }, _etch.default.dom("h1", {
      className: "welcome-title"
    }, "Get to know ", this.brand, "!"), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('project')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-repo"
    }, "Open a ", _etch.default.dom("span", {
      className: "welcome-highlight"
    }, "Project")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/project.svg"
    })), _etch.default.dom("p", null, "In ", this.brand, " you can open individual files or a whole folder as a project. Opening a folder will ad a tree view, on the left side (by default), listing all the files and folders belonging to your project."), _etch.default.dom("p", null, _etch.default.dom("button", {
      ref: "projectButton",
      onclick: this.didClickProjectButton,
      className: "btn btn-primary"
    }, "Open a Project")), _etch.default.dom("p", {
      className: "welcome-note"
    }, _etch.default.dom("strong", null, "Next time:"), " You can also open projects from the menu, keyboard shortcut or by dragging a folder onto the", this.brand, " dock icon."))), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('git')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-mark-github"
    }, "Version control with", ' ', _etch.default.dom("span", {
      class: "welcome-highlight"
    }, "Git and GitHub")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/package.svg"
    })), _etch.default.dom("p", null, "Track changes to your code as you work. Branch, commit, push, and pull without leaving the comfort of your editor. Collaborate with other developers on GitHub."), _etch.default.dom("p", null, _etch.default.dom("button", {
      onclick: this.didClickGitButton,
      className: "btn btn-primary inline-block"
    }, "Open the Git panel"), _etch.default.dom("button", {
      onclick: this.didClickGitHubButton,
      className: "btn btn-primary inline-block"
    }, "Open the GitHub panel")), _etch.default.dom("p", {
      className: "welcome-note"
    }, _etch.default.dom("strong", null, "Next time:"), " You can toggle the Git tab by clicking on the", _etch.default.dom("span", {
      className: "icon icon-diff"
    }), " button in your status bar."))), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('teletype')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-radio-tower"
    }, "Collaborate in real time with", ' ', _etch.default.dom("span", {
      class: "welcome-highlight"
    }, "Teletype")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/code.svg"
    })), _etch.default.dom("p", null, "Share your workspace with team members and collaborate on code in real time."), _etch.default.dom("p", null, _etch.default.dom("button", {
      onclick: this.didClickTeletypeButton,
      className: "btn btn-primary inline-block"
    }, "Install Teletype for ", this.brand)))), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('packages')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-package"
    }, "Install a ", _etch.default.dom("span", {
      className: "welcome-highlight"
    }, "Package")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/package.svg"
    })), _etch.default.dom("p", null, "One of the best things about ", this.brand, " is the package ecosystem. Installing packages adds new features and functionality you can use to make the editor suit your needs. Let's install one."), _etch.default.dom("p", null, _etch.default.dom("button", {
      ref: "packagesButton",
      onclick: this.didClickPackagesButton,
      className: "btn btn-primary"
    }, "Open Installer")), _etch.default.dom("p", {
      className: "welcome-note"
    }, _etch.default.dom("strong", null, "Next time:"), " You can install new packages from the settings."))), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('themes')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-paintcan"
    }, "Choose a ", _etch.default.dom("span", {
      class: "welcome-highlight"
    }, "Theme")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/theme.svg"
    })), _etch.default.dom("p", null, this.brand, " comes with preinstalled themes. Let's try a few."), _etch.default.dom("p", null, _etch.default.dom("button", {
      ref: "themesButton",
      onclick: this.didClickThemesButton,
      className: "btn btn-primary"
    }, "Open the theme picker")), _etch.default.dom("p", null, "You can also install themes created by the ", this.brand, " community. To install new themes, click on \"+ Install\" and switch the toggle to \"themes\"."), _etch.default.dom("p", {
      className: "welcome-note"
    }, _etch.default.dom("strong", null, "Next time:"), " You can switch themes from the settings."))), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('styling')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-paintcan"
    }, "Customize the ", _etch.default.dom("span", {
      class: "welcome-highlight"
    }, "Styling")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/code.svg"
    })), _etch.default.dom("p", null, "You can customize almost anything by adding your own CSS/LESS."), _etch.default.dom("p", null, _etch.default.dom("button", {
      ref: "stylingButton",
      onclick: this.didClickStylingButton,
      className: "btn btn-primary"
    }, "Open your Stylesheet")), _etch.default.dom("p", null, "Now uncomment some of the examples or try your own"), _etch.default.dom("p", {
      className: "welcome-note"
    }, _etch.default.dom("strong", null, "Next time:"), " You can open your stylesheet from Menu ", this.getApplicationMenuName(), "."))), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('init-script')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-code"
    }, "Hack on the ", _etch.default.dom("span", {
      class: "welcome-highlight"
    }, "Init Script")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/code.svg"
    })), _etch.default.dom("p", null, "The init script is a bit of JavaScript or CoffeeScript run at startup. You can use it to quickly change the behaviour of", this.brand, "."), _etch.default.dom("p", null, _etch.default.dom("button", {
      ref: "initScriptButton",
      onclick: this.didClickInitScriptButton,
      className: "btn btn-primary"
    }, "Open your Init Script")), _etch.default.dom("p", null, "Uncomment some of the examples or try out your own."), _etch.default.dom("p", {
      className: "welcome-note"
    }, _etch.default.dom("strong", null, "Next time:"), " You can open your init script from Menu > ", this.getApplicationMenuName(), "."))), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('snippets')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-code"
    }, "Add a ", _etch.default.dom("span", {
      class: "welcome-highlight"
    }, "Snippet")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/code.svg"
    })), _etch.default.dom("p", null, this.brand, " snippets allow you to enter a simple prefix in the editor and hit tab to expand the prefix into a larger code block with templated values."), _etch.default.dom("p", null, _etch.default.dom("button", {
      ref: "snippetsButton",
      onclick: this.didClickSnippetsButton,
      className: "btn btn-primary"
    }, "Open your Snippets")), _etch.default.dom("p", null, "In your snippets file, type ", _etch.default.dom("code", null, "snip"), " then hit", ' ', _etch.default.dom("code", null, "tab"), ". The ", _etch.default.dom("code", null, "snip"), " snippet will expand to create a snippet!"), _etch.default.dom("p", {
      className: "welcome-note"
    }, _etch.default.dom("strong", null, "Next time:"), " You can open your snippets in Menu > ", this.getApplicationMenuName(), "."))), _etch.default.dom("details", _extends({
      className: "welcome-card"
    }, this.getSectionProps('shortcuts')), _etch.default.dom("summary", {
      className: "welcome-summary icon icon-keyboard"
    }, "Learn ", _etch.default.dom("span", {
      class: "welcome-highlight"
    }, "Keyboard Shortcuts")), _etch.default.dom("div", {
      className: "welcome-detail"
    }, _etch.default.dom("p", null, _etch.default.dom("img", {
      className: "welcome-img",
      src: "atom://welcome/assets/shortcut.svg"
    })), _etch.default.dom("p", null, "If you only remember one keyboard shortcut make it", ' ', _etch.default.dom("kbd", {
      className: "welcome-key"
    }, this.getCommandPaletteKeyBinding()), ". This keystroke toggles the command palette, which lists every ", this.brand, " command. It's a good way to learn more shortcuts. Yes, you can try it now!"), _etch.default.dom("p", null, "If you want to use these guides again use the command palette", ' ', _etch.default.dom("kbd", {
      className: "welcome-key"
    }, this.getCommandPaletteKeyBinding()), ' ', "and search for ", _etch.default.dom("span", {
      className: "text-highlight"
    }, "Welcome"), "."))))));
  }
  getSectionProps(sectionName) {
    const props = {
      dataset: {
        section: sectionName
      }
    };
    if (this.props.openSections && this.props.openSections.indexOf(sectionName) !== -1) {
      props.open = true;
    }
    return props;
  }
  getCommandPaletteKeyBinding() {
    if (process.platform === 'darwin') {
      return 'cmd-shift-p';
    } else {
      return 'ctrl-shift-p';
    }
  }
  getApplicationMenuName() {
    if (process.platform === 'darwin') {
      return 'Pulsar';
    } else if (process.platform === 'linux') {
      return 'Edit';
    } else {
      return 'File';
    }
  }
  serialize() {
    return {
      deserializer: this.constructor.name,
      openSections: this.getOpenSections(),
      uri: this.getURI()
    };
  }
  getURI() {
    return this.props.uri;
  }
  getTitle() {
    return 'Welcome Guide';
  }
  isEqual(other) {
    return other instanceof GuideView;
  }
  getOpenSections() {
    return Array.from(this.element.querySelectorAll('details[open]')).map(sectionElement => sectionElement.dataset.section);
  }
  didClickProjectButton() {
    atom.commands.dispatch(atom.views.getView(atom.workspace), 'application:open');
  }
  didClickGitButton() {
    atom.commands.dispatch(atom.views.getView(atom.workspace), 'github:toggle-git-tab');
  }
  didClickGitHubButton() {
    atom.commands.dispatch(atom.views.getView(atom.workspace), 'github:toggle-github-tab');
  }
  didClickPackagesButton() {
    atom.workspace.open('atom://config/install', {
      split: 'left'
    });
  }
  didClickThemesButton() {
    atom.workspace.open('atom://config/themes', {
      split: 'left'
    });
  }
  didClickStylingButton() {
    atom.workspace.open('atom://.pulsar/stylesheet', {
      split: 'left'
    });
  }
  didClickInitScriptButton() {
    atom.workspace.open('atom://.pulsar/init-script', {
      split: 'left'
    });
  }
  didClickSnippetsButton() {
    atom.workspace.open('atom://.pulsar/snippets', {
      split: 'left'
    });
  }
  didClickTeletypeButton() {
    atom.workspace.open('atom://config/packages/teletype', {
      split: 'left'
    });
  }
}
exports.default = GuideView;
module.exports = exports.default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJHdWlkZVZpZXciLCJjb25zdHJ1Y3RvciIsInByb3BzIiwiYnJhbmQiLCJhdG9tIiwiYnJhbmRpbmciLCJuYW1lIiwiZGlkQ2xpY2tQcm9qZWN0QnV0dG9uIiwiYmluZCIsImRpZENsaWNrR2l0QnV0dG9uIiwiZGlkQ2xpY2tHaXRIdWJCdXR0b24iLCJkaWRDbGlja1RlbGV0eXBlQnV0dG9uIiwiZGlkQ2xpY2tQYWNrYWdlc0J1dHRvbiIsImRpZENsaWNrVGhlbWVzQnV0dG9uIiwiZGlkQ2xpY2tTdHlsaW5nQnV0dG9uIiwiZGlkQ2xpY2tJbml0U2NyaXB0QnV0dG9uIiwiZGlkQ2xpY2tTbmlwcGV0c0J1dHRvbiIsImV0Y2giLCJpbml0aWFsaXplIiwidXBkYXRlIiwicmVuZGVyIiwiZ2V0U2VjdGlvblByb3BzIiwiZ2V0QXBwbGljYXRpb25NZW51TmFtZSIsImdldENvbW1hbmRQYWxldHRlS2V5QmluZGluZyIsInNlY3Rpb25OYW1lIiwiZGF0YXNldCIsInNlY3Rpb24iLCJvcGVuU2VjdGlvbnMiLCJpbmRleE9mIiwib3BlbiIsInByb2Nlc3MiLCJwbGF0Zm9ybSIsInNlcmlhbGl6ZSIsImRlc2VyaWFsaXplciIsImdldE9wZW5TZWN0aW9ucyIsInVyaSIsImdldFVSSSIsImdldFRpdGxlIiwiaXNFcXVhbCIsIm90aGVyIiwiQXJyYXkiLCJmcm9tIiwiZWxlbWVudCIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJtYXAiLCJzZWN0aW9uRWxlbWVudCIsImNvbW1hbmRzIiwiZGlzcGF0Y2giLCJ2aWV3cyIsImdldFZpZXciLCJ3b3Jrc3BhY2UiLCJzcGxpdCJdLCJzb3VyY2VzIjpbImd1aWRlLXZpZXcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqIEBiYWJlbCAqL1xuLyoqIEBqc3ggZXRjaC5kb20gKi9cblxuaW1wb3J0IGV0Y2ggZnJvbSAnZXRjaCc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEd1aWRlVmlldyB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgdGhpcy5wcm9wcyA9IHByb3BzO1xuICAgIHRoaXMuYnJhbmQgPSBhdG9tLmJyYW5kaW5nLm5hbWU7XG4gICAgdGhpcy5kaWRDbGlja1Byb2plY3RCdXR0b24gPSB0aGlzLmRpZENsaWNrUHJvamVjdEJ1dHRvbi5iaW5kKHRoaXMpO1xuICAgIHRoaXMuZGlkQ2xpY2tHaXRCdXR0b24gPSB0aGlzLmRpZENsaWNrR2l0QnV0dG9uLmJpbmQodGhpcyk7XG4gICAgdGhpcy5kaWRDbGlja0dpdEh1YkJ1dHRvbiA9IHRoaXMuZGlkQ2xpY2tHaXRIdWJCdXR0b24uYmluZCh0aGlzKTtcbiAgICB0aGlzLmRpZENsaWNrVGVsZXR5cGVCdXR0b24gPSB0aGlzLmRpZENsaWNrVGVsZXR5cGVCdXR0b24uYmluZCh0aGlzKTtcbiAgICB0aGlzLmRpZENsaWNrUGFja2FnZXNCdXR0b24gPSB0aGlzLmRpZENsaWNrUGFja2FnZXNCdXR0b24uYmluZCh0aGlzKTtcbiAgICB0aGlzLmRpZENsaWNrVGhlbWVzQnV0dG9uID0gdGhpcy5kaWRDbGlja1RoZW1lc0J1dHRvbi5iaW5kKHRoaXMpO1xuICAgIHRoaXMuZGlkQ2xpY2tTdHlsaW5nQnV0dG9uID0gdGhpcy5kaWRDbGlja1N0eWxpbmdCdXR0b24uYmluZCh0aGlzKTtcbiAgICB0aGlzLmRpZENsaWNrSW5pdFNjcmlwdEJ1dHRvbiA9IHRoaXMuZGlkQ2xpY2tJbml0U2NyaXB0QnV0dG9uLmJpbmQodGhpcyk7XG4gICAgdGhpcy5kaWRDbGlja1NuaXBwZXRzQnV0dG9uID0gdGhpcy5kaWRDbGlja1NuaXBwZXRzQnV0dG9uLmJpbmQodGhpcyk7XG4gICAgZXRjaC5pbml0aWFsaXplKHRoaXMpO1xuICB9XG5cbiAgdXBkYXRlKCkge31cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwid2VsY29tZSBpcy1ndWlkZVwiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIndlbGNvbWUtY29udGFpbmVyXCI+XG4gICAgICAgICAgPHNlY3Rpb24gY2xhc3NOYW1lPVwid2VsY29tZS1wYW5lbFwiPlxuICAgICAgICAgICAgPGgxIGNsYXNzTmFtZT1cIndlbGNvbWUtdGl0bGVcIj5HZXQgdG8ga25vdyB7dGhpcy5icmFuZH0hPC9oMT5cblxuICAgICAgICAgICAgPGRldGFpbHNcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2VsY29tZS1jYXJkXCJcbiAgICAgICAgICAgICAgey4uLnRoaXMuZ2V0U2VjdGlvblByb3BzKCdwcm9qZWN0Jyl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxzdW1tYXJ5IGNsYXNzTmFtZT1cIndlbGNvbWUtc3VtbWFyeSBpY29uIGljb24tcmVwb1wiPlxuICAgICAgICAgICAgICAgIE9wZW4gYSA8c3BhbiBjbGFzc05hbWU9XCJ3ZWxjb21lLWhpZ2hsaWdodFwiPlByb2plY3Q8L3NwYW4+XG4gICAgICAgICAgICAgIDwvc3VtbWFyeT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3ZWxjb21lLWRldGFpbFwiPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWltZ1wiXG4gICAgICAgICAgICAgICAgICAgIHNyYz1cImF0b206Ly93ZWxjb21lL2Fzc2V0cy9wcm9qZWN0LnN2Z1wiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIEluIHt0aGlzLmJyYW5kfSB5b3UgY2FuIG9wZW4gaW5kaXZpZHVhbCBmaWxlcyBvciBhIHdob2xlIGZvbGRlciBhcyBhXG4gICAgICAgICAgICAgICAgICBwcm9qZWN0LiBPcGVuaW5nIGEgZm9sZGVyIHdpbGwgYWQgYSB0cmVlIHZpZXcsIG9uIHRoZSBsZWZ0IHNpZGUgXG4gICAgICAgICAgICAgICAgICAoYnkgZGVmYXVsdCksIGxpc3RpbmcgYWxsIHRoZSBmaWxlcyBhbmQgZm9sZGVycyBiZWxvbmdpbmcgdG8geW91ciBwcm9qZWN0LlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgcmVmPVwicHJvamVjdEJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgIG9uY2xpY2s9e3RoaXMuZGlkQ2xpY2tQcm9qZWN0QnV0dG9ufVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJidG4gYnRuLXByaW1hcnlcIlxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICBPcGVuIGEgUHJvamVjdFxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIndlbGNvbWUtbm90ZVwiPlxuICAgICAgICAgICAgICAgICAgPHN0cm9uZz5OZXh0IHRpbWU6PC9zdHJvbmc+IFlvdSBjYW4gYWxzbyBvcGVuIHByb2plY3RzIGZyb21cbiAgICAgICAgICAgICAgICAgIHRoZSBtZW51LCBrZXlib2FyZCBzaG9ydGN1dCBvciBieSBkcmFnZ2luZyBhIGZvbGRlciBvbnRvIHRoZVxuICAgICAgICAgICAgICAgICAge3RoaXMuYnJhbmR9IGRvY2sgaWNvbi5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kZXRhaWxzPlxuXG4gICAgICAgICAgICA8ZGV0YWlscyBjbGFzc05hbWU9XCJ3ZWxjb21lLWNhcmRcIiB7Li4udGhpcy5nZXRTZWN0aW9uUHJvcHMoJ2dpdCcpfT5cbiAgICAgICAgICAgICAgPHN1bW1hcnkgY2xhc3NOYW1lPVwid2VsY29tZS1zdW1tYXJ5IGljb24gaWNvbi1tYXJrLWdpdGh1YlwiPlxuICAgICAgICAgICAgICAgIFZlcnNpb24gY29udHJvbCB3aXRoeycgJ31cbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIndlbGNvbWUtaGlnaGxpZ2h0XCI+R2l0IGFuZCBHaXRIdWI8L3NwYW4+XG4gICAgICAgICAgICAgIDwvc3VtbWFyeT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3ZWxjb21lLWRldGFpbFwiPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWltZ1wiXG4gICAgICAgICAgICAgICAgICAgIHNyYz1cImF0b206Ly93ZWxjb21lL2Fzc2V0cy9wYWNrYWdlLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIFRyYWNrIGNoYW5nZXMgdG8geW91ciBjb2RlIGFzIHlvdSB3b3JrLiBCcmFuY2gsIGNvbW1pdCwgcHVzaCxcbiAgICAgICAgICAgICAgICAgIGFuZCBwdWxsIHdpdGhvdXQgbGVhdmluZyB0aGUgY29tZm9ydCBvZiB5b3VyIGVkaXRvci5cbiAgICAgICAgICAgICAgICAgIENvbGxhYm9yYXRlIHdpdGggb3RoZXIgZGV2ZWxvcGVycyBvbiBHaXRIdWIuXG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICBvbmNsaWNrPXt0aGlzLmRpZENsaWNrR2l0QnV0dG9ufVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJidG4gYnRuLXByaW1hcnkgaW5saW5lLWJsb2NrXCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgT3BlbiB0aGUgR2l0IHBhbmVsXG4gICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgb25jbGljaz17dGhpcy5kaWRDbGlja0dpdEh1YkJ1dHRvbn1cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYnRuIGJ0bi1wcmltYXJ5IGlubGluZS1ibG9ja1wiXG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIE9wZW4gdGhlIEdpdEh1YiBwYW5lbFxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIndlbGNvbWUtbm90ZVwiPlxuICAgICAgICAgICAgICAgICAgPHN0cm9uZz5OZXh0IHRpbWU6PC9zdHJvbmc+IFlvdSBjYW4gdG9nZ2xlIHRoZSBHaXQgdGFiIGJ5XG4gICAgICAgICAgICAgICAgICBjbGlja2luZyBvbiB0aGVcbiAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImljb24gaWNvbi1kaWZmXCIgLz4gYnV0dG9uIGluIHlvdXIgc3RhdHVzIGJhci5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kZXRhaWxzPlxuXG4gICAgICAgICAgICA8ZGV0YWlsc1xuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWNhcmRcIlxuICAgICAgICAgICAgICB7Li4udGhpcy5nZXRTZWN0aW9uUHJvcHMoJ3RlbGV0eXBlJyl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxzdW1tYXJ5IGNsYXNzTmFtZT1cIndlbGNvbWUtc3VtbWFyeSBpY29uIGljb24tcmFkaW8tdG93ZXJcIj5cbiAgICAgICAgICAgICAgICBDb2xsYWJvcmF0ZSBpbiByZWFsIHRpbWUgd2l0aHsnICd9XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ3ZWxjb21lLWhpZ2hsaWdodFwiPlRlbGV0eXBlPC9zcGFuPlxuICAgICAgICAgICAgICA8L3N1bW1hcnk+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwid2VsY29tZS1kZXRhaWxcIj5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2VsY29tZS1pbWdcIlxuICAgICAgICAgICAgICAgICAgICBzcmM9XCJhdG9tOi8vd2VsY29tZS9hc3NldHMvY29kZS5zdmdcIlxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgICAgICBTaGFyZSB5b3VyIHdvcmtzcGFjZSB3aXRoIHRlYW0gbWVtYmVycyBhbmQgY29sbGFib3JhdGUgb24gY29kZVxuICAgICAgICAgICAgICAgICAgaW4gcmVhbCB0aW1lLlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgb25jbGljaz17dGhpcy5kaWRDbGlja1RlbGV0eXBlQnV0dG9ufVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJidG4gYnRuLXByaW1hcnkgaW5saW5lLWJsb2NrXCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgSW5zdGFsbCBUZWxldHlwZSBmb3Ige3RoaXMuYnJhbmR9XG4gICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kZXRhaWxzPlxuXG4gICAgICAgICAgICA8ZGV0YWlsc1xuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWNhcmRcIlxuICAgICAgICAgICAgICB7Li4udGhpcy5nZXRTZWN0aW9uUHJvcHMoJ3BhY2thZ2VzJyl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxzdW1tYXJ5IGNsYXNzTmFtZT1cIndlbGNvbWUtc3VtbWFyeSBpY29uIGljb24tcGFja2FnZVwiPlxuICAgICAgICAgICAgICAgIEluc3RhbGwgYSA8c3BhbiBjbGFzc05hbWU9XCJ3ZWxjb21lLWhpZ2hsaWdodFwiPlBhY2thZ2U8L3NwYW4+XG4gICAgICAgICAgICAgIDwvc3VtbWFyeT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3ZWxjb21lLWRldGFpbFwiPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWltZ1wiXG4gICAgICAgICAgICAgICAgICAgIHNyYz1cImF0b206Ly93ZWxjb21lL2Fzc2V0cy9wYWNrYWdlLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIE9uZSBvZiB0aGUgYmVzdCB0aGluZ3MgYWJvdXQge3RoaXMuYnJhbmR9IGlzIHRoZSBwYWNrYWdlIGVjb3N5c3RlbS5cbiAgICAgICAgICAgICAgICAgIEluc3RhbGxpbmcgcGFja2FnZXMgYWRkcyBuZXcgZmVhdHVyZXMgYW5kIGZ1bmN0aW9uYWxpdHkgeW91XG4gICAgICAgICAgICAgICAgICBjYW4gdXNlIHRvIG1ha2UgdGhlIGVkaXRvciBzdWl0IHlvdXIgbmVlZHMuIExldCdzIGluc3RhbGwgb25lLlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgcmVmPVwicGFja2FnZXNCdXR0b25cIlxuICAgICAgICAgICAgICAgICAgICBvbmNsaWNrPXt0aGlzLmRpZENsaWNrUGFja2FnZXNCdXR0b259XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBidG4tcHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIE9wZW4gSW5zdGFsbGVyXG4gICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwid2VsY29tZS1ub3RlXCI+XG4gICAgICAgICAgICAgICAgICA8c3Ryb25nPk5leHQgdGltZTo8L3N0cm9uZz4gWW91IGNhbiBpbnN0YWxsIG5ldyBwYWNrYWdlcyBmcm9tXG4gICAgICAgICAgICAgICAgICB0aGUgc2V0dGluZ3MuXG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGV0YWlscz5cblxuICAgICAgICAgICAgPGRldGFpbHNcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2VsY29tZS1jYXJkXCJcbiAgICAgICAgICAgICAgey4uLnRoaXMuZ2V0U2VjdGlvblByb3BzKCd0aGVtZXMnKX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHN1bW1hcnkgY2xhc3NOYW1lPVwid2VsY29tZS1zdW1tYXJ5IGljb24gaWNvbi1wYWludGNhblwiPlxuICAgICAgICAgICAgICAgIENob29zZSBhIDxzcGFuIGNsYXNzPVwid2VsY29tZS1oaWdobGlnaHRcIj5UaGVtZTwvc3Bhbj5cbiAgICAgICAgICAgICAgPC9zdW1tYXJ5PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIndlbGNvbWUtZGV0YWlsXCI+XG4gICAgICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIndlbGNvbWUtaW1nXCJcbiAgICAgICAgICAgICAgICAgICAgc3JjPVwiYXRvbTovL3dlbGNvbWUvYXNzZXRzL3RoZW1lLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD57dGhpcy5icmFuZH0gY29tZXMgd2l0aCBwcmVpbnN0YWxsZWQgdGhlbWVzLiBMZXQncyB0cnkgYSBmZXcuPC9wPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICByZWY9XCJ0aGVtZXNCdXR0b25cIlxuICAgICAgICAgICAgICAgICAgICBvbmNsaWNrPXt0aGlzLmRpZENsaWNrVGhlbWVzQnV0dG9ufVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJidG4gYnRuLXByaW1hcnlcIlxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICBPcGVuIHRoZSB0aGVtZSBwaWNrZXJcbiAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIFlvdSBjYW4gYWxzbyBpbnN0YWxsIHRoZW1lcyBjcmVhdGVkIGJ5IHRoZSB7dGhpcy5icmFuZH0gY29tbXVuaXR5LiBUb1xuICAgICAgICAgICAgICAgICAgaW5zdGFsbCBuZXcgdGhlbWVzLCBjbGljayBvbiBcIisgSW5zdGFsbFwiIGFuZCBzd2l0Y2ggdGhlIHRvZ2dsZVxuICAgICAgICAgICAgICAgICAgdG8gXCJ0aGVtZXNcIi5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwid2VsY29tZS1ub3RlXCI+XG4gICAgICAgICAgICAgICAgICA8c3Ryb25nPk5leHQgdGltZTo8L3N0cm9uZz4gWW91IGNhbiBzd2l0Y2ggdGhlbWVzIGZyb20gdGhlXG4gICAgICAgICAgICAgICAgICBzZXR0aW5ncy5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kZXRhaWxzPlxuXG4gICAgICAgICAgICA8ZGV0YWlsc1xuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWNhcmRcIlxuICAgICAgICAgICAgICB7Li4udGhpcy5nZXRTZWN0aW9uUHJvcHMoJ3N0eWxpbmcnKX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHN1bW1hcnkgY2xhc3NOYW1lPVwid2VsY29tZS1zdW1tYXJ5IGljb24gaWNvbi1wYWludGNhblwiPlxuICAgICAgICAgICAgICAgIEN1c3RvbWl6ZSB0aGUgPHNwYW4gY2xhc3M9XCJ3ZWxjb21lLWhpZ2hsaWdodFwiPlN0eWxpbmc8L3NwYW4+XG4gICAgICAgICAgICAgIDwvc3VtbWFyeT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3ZWxjb21lLWRldGFpbFwiPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWltZ1wiXG4gICAgICAgICAgICAgICAgICAgIHNyYz1cImF0b206Ly93ZWxjb21lL2Fzc2V0cy9jb2RlLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIFlvdSBjYW4gY3VzdG9taXplIGFsbW9zdCBhbnl0aGluZyBieSBhZGRpbmcgeW91ciBvd24gQ1NTL0xFU1MuXG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICByZWY9XCJzdHlsaW5nQnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgb25jbGljaz17dGhpcy5kaWRDbGlja1N0eWxpbmdCdXR0b259XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBidG4tcHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIE9wZW4geW91ciBTdHlsZXNoZWV0XG4gICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPHA+Tm93IHVuY29tbWVudCBzb21lIG9mIHRoZSBleGFtcGxlcyBvciB0cnkgeW91ciBvd248L3A+XG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwid2VsY29tZS1ub3RlXCI+XG4gICAgICAgICAgICAgICAgICA8c3Ryb25nPk5leHQgdGltZTo8L3N0cm9uZz4gWW91IGNhbiBvcGVuIHlvdXIgc3R5bGVzaGVldCBmcm9tXG4gICAgICAgICAgICAgICAgICBNZW51IHt0aGlzLmdldEFwcGxpY2F0aW9uTWVudU5hbWUoKX0uXG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGV0YWlscz5cblxuICAgICAgICAgICAgPGRldGFpbHNcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2VsY29tZS1jYXJkXCJcbiAgICAgICAgICAgICAgey4uLnRoaXMuZ2V0U2VjdGlvblByb3BzKCdpbml0LXNjcmlwdCcpfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8c3VtbWFyeSBjbGFzc05hbWU9XCJ3ZWxjb21lLXN1bW1hcnkgaWNvbiBpY29uLWNvZGVcIj5cbiAgICAgICAgICAgICAgICBIYWNrIG9uIHRoZSA8c3BhbiBjbGFzcz1cIndlbGNvbWUtaGlnaGxpZ2h0XCI+SW5pdCBTY3JpcHQ8L3NwYW4+XG4gICAgICAgICAgICAgIDwvc3VtbWFyeT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3ZWxjb21lLWRldGFpbFwiPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWltZ1wiXG4gICAgICAgICAgICAgICAgICAgIHNyYz1cImF0b206Ly93ZWxjb21lL2Fzc2V0cy9jb2RlLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIFRoZSBpbml0IHNjcmlwdCBpcyBhIGJpdCBvZiBKYXZhU2NyaXB0IG9yIENvZmZlZVNjcmlwdCBydW4gYXRcbiAgICAgICAgICAgICAgICAgIHN0YXJ0dXAuIFlvdSBjYW4gdXNlIGl0IHRvIHF1aWNrbHkgY2hhbmdlIHRoZSBiZWhhdmlvdXIgb2ZcbiAgICAgICAgICAgICAgICAgIHt0aGlzLmJyYW5kfS5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgICAgICAgIHJlZj1cImluaXRTY3JpcHRCdXR0b25cIlxuICAgICAgICAgICAgICAgICAgICBvbmNsaWNrPXt0aGlzLmRpZENsaWNrSW5pdFNjcmlwdEJ1dHRvbn1cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYnRuIGJ0bi1wcmltYXJ5XCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgT3BlbiB5b3VyIEluaXQgU2NyaXB0XG4gICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPHA+VW5jb21tZW50IHNvbWUgb2YgdGhlIGV4YW1wbGVzIG9yIHRyeSBvdXQgeW91ciBvd24uPC9wPlxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIndlbGNvbWUtbm90ZVwiPlxuICAgICAgICAgICAgICAgICAgPHN0cm9uZz5OZXh0IHRpbWU6PC9zdHJvbmc+IFlvdSBjYW4gb3BlbiB5b3VyIGluaXQgc2NyaXB0IGZyb21cbiAgICAgICAgICAgICAgICAgIE1lbnUgPiB7dGhpcy5nZXRBcHBsaWNhdGlvbk1lbnVOYW1lKCl9LlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2RldGFpbHM+XG5cbiAgICAgICAgICAgIDxkZXRhaWxzXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIndlbGNvbWUtY2FyZFwiXG4gICAgICAgICAgICAgIHsuLi50aGlzLmdldFNlY3Rpb25Qcm9wcygnc25pcHBldHMnKX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHN1bW1hcnkgY2xhc3NOYW1lPVwid2VsY29tZS1zdW1tYXJ5IGljb24gaWNvbi1jb2RlXCI+XG4gICAgICAgICAgICAgICAgQWRkIGEgPHNwYW4gY2xhc3M9XCJ3ZWxjb21lLWhpZ2hsaWdodFwiPlNuaXBwZXQ8L3NwYW4+XG4gICAgICAgICAgICAgIDwvc3VtbWFyeT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3ZWxjb21lLWRldGFpbFwiPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWltZ1wiXG4gICAgICAgICAgICAgICAgICAgIHNyYz1cImF0b206Ly93ZWxjb21lL2Fzc2V0cy9jb2RlLnN2Z1wiXG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIHt0aGlzLmJyYW5kfSBzbmlwcGV0cyBhbGxvdyB5b3UgdG8gZW50ZXIgYSBzaW1wbGUgcHJlZml4IGluIHRoZSBlZGl0b3JcbiAgICAgICAgICAgICAgICAgIGFuZCBoaXQgdGFiIHRvIGV4cGFuZCB0aGUgcHJlZml4IGludG8gYSBsYXJnZXIgY29kZSBibG9jayB3aXRoXG4gICAgICAgICAgICAgICAgICB0ZW1wbGF0ZWQgdmFsdWVzLlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgcmVmPVwic25pcHBldHNCdXR0b25cIlxuICAgICAgICAgICAgICAgICAgICBvbmNsaWNrPXt0aGlzLmRpZENsaWNrU25pcHBldHNCdXR0b259XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBidG4tcHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIE9wZW4geW91ciBTbmlwcGV0c1xuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgSW4geW91ciBzbmlwcGV0cyBmaWxlLCB0eXBlIDxjb2RlPnNuaXA8L2NvZGU+IHRoZW4gaGl0eycgJ31cbiAgICAgICAgICAgICAgICAgIDxjb2RlPnRhYjwvY29kZT4uIFRoZSA8Y29kZT5zbmlwPC9jb2RlPiBzbmlwcGV0IHdpbGwgZXhwYW5kIHRvXG4gICAgICAgICAgICAgICAgICBjcmVhdGUgYSBzbmlwcGV0IVxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ3ZWxjb21lLW5vdGVcIj5cbiAgICAgICAgICAgICAgICAgIDxzdHJvbmc+TmV4dCB0aW1lOjwvc3Ryb25nPiBZb3UgY2FuIG9wZW4geW91ciBzbmlwcGV0cyBpbiBNZW51XG4gICAgICAgICAgICAgICAgICA+IHt0aGlzLmdldEFwcGxpY2F0aW9uTWVudU5hbWUoKX0uXG4gICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGV0YWlscz5cblxuICAgICAgICAgICAgPGRldGFpbHNcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2VsY29tZS1jYXJkXCJcbiAgICAgICAgICAgICAgey4uLnRoaXMuZ2V0U2VjdGlvblByb3BzKCdzaG9ydGN1dHMnKX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHN1bW1hcnkgY2xhc3NOYW1lPVwid2VsY29tZS1zdW1tYXJ5IGljb24gaWNvbi1rZXlib2FyZFwiPlxuICAgICAgICAgICAgICAgIExlYXJuIDxzcGFuIGNsYXNzPVwid2VsY29tZS1oaWdobGlnaHRcIj5LZXlib2FyZCBTaG9ydGN1dHM8L3NwYW4+XG4gICAgICAgICAgICAgIDwvc3VtbWFyeT5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3ZWxjb21lLWRldGFpbFwiPlxuICAgICAgICAgICAgICAgIDxwPlxuICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3ZWxjb21lLWltZ1wiXG4gICAgICAgICAgICAgICAgICAgIHNyYz1cImF0b206Ly93ZWxjb21lL2Fzc2V0cy9zaG9ydGN1dC5zdmdcIlxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgICAgICBJZiB5b3Ugb25seSByZW1lbWJlciBvbmUga2V5Ym9hcmQgc2hvcnRjdXQgbWFrZSBpdHsnICd9XG4gICAgICAgICAgICAgICAgICA8a2JkIGNsYXNzTmFtZT1cIndlbGNvbWUta2V5XCI+XG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLmdldENvbW1hbmRQYWxldHRlS2V5QmluZGluZygpfVxuICAgICAgICAgICAgICAgICAgPC9rYmQ+XG4gICAgICAgICAgICAgICAgICAuIFRoaXMga2V5c3Ryb2tlIHRvZ2dsZXMgdGhlIGNvbW1hbmQgcGFsZXR0ZSwgd2hpY2ggbGlzdHNcbiAgICAgICAgICAgICAgICAgIGV2ZXJ5IHt0aGlzLmJyYW5kfSBjb21tYW5kLiBJdCdzIGEgZ29vZCB3YXkgdG8gbGVhcm4gbW9yZSBzaG9ydGN1dHMuXG4gICAgICAgICAgICAgICAgICBZZXMsIHlvdSBjYW4gdHJ5IGl0IG5vdyFcbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgICAgICBJZiB5b3Ugd2FudCB0byB1c2UgdGhlc2UgZ3VpZGVzIGFnYWluIHVzZSB0aGUgY29tbWFuZCBwYWxldHRleycgJ31cbiAgICAgICAgICAgICAgICAgIDxrYmQgY2xhc3NOYW1lPVwid2VsY29tZS1rZXlcIj5cbiAgICAgICAgICAgICAgICAgICAge3RoaXMuZ2V0Q29tbWFuZFBhbGV0dGVLZXlCaW5kaW5nKCl9XG4gICAgICAgICAgICAgICAgICA8L2tiZD57JyAnfVxuICAgICAgICAgICAgICAgICAgYW5kIHNlYXJjaCBmb3IgPHNwYW4gY2xhc3NOYW1lPVwidGV4dC1oaWdobGlnaHRcIj5XZWxjb21lPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgLlxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2RldGFpbHM+XG4gICAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBnZXRTZWN0aW9uUHJvcHMoc2VjdGlvbk5hbWUpIHtcbiAgICBjb25zdCBwcm9wcyA9IHtcbiAgICAgIGRhdGFzZXQ6IHsgc2VjdGlvbjogc2VjdGlvbk5hbWUgfVxuICAgIH07XG4gICAgaWYgKFxuICAgICAgdGhpcy5wcm9wcy5vcGVuU2VjdGlvbnMgJiZcbiAgICAgIHRoaXMucHJvcHMub3BlblNlY3Rpb25zLmluZGV4T2Yoc2VjdGlvbk5hbWUpICE9PSAtMVxuICAgICkge1xuICAgICAgcHJvcHMub3BlbiA9IHRydWU7XG4gICAgfVxuICAgIHJldHVybiBwcm9wcztcbiAgfVxuXG4gIGdldENvbW1hbmRQYWxldHRlS2V5QmluZGluZygpIHtcbiAgICBpZiAocHJvY2Vzcy5wbGF0Zm9ybSA9PT0gJ2RhcndpbicpIHtcbiAgICAgIHJldHVybiAnY21kLXNoaWZ0LXAnO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJ2N0cmwtc2hpZnQtcCc7XG4gICAgfVxuICB9XG5cbiAgZ2V0QXBwbGljYXRpb25NZW51TmFtZSgpIHtcbiAgICBpZiAocHJvY2Vzcy5wbGF0Zm9ybSA9PT0gJ2RhcndpbicpIHtcbiAgICAgIHJldHVybiAnUHVsc2FyJztcbiAgICB9IGVsc2UgaWYgKHByb2Nlc3MucGxhdGZvcm0gPT09ICdsaW51eCcpIHtcbiAgICAgIHJldHVybiAnRWRpdCc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnRmlsZSc7XG4gICAgfVxuICB9XG5cbiAgc2VyaWFsaXplKCkge1xuICAgIHJldHVybiB7XG4gICAgICBkZXNlcmlhbGl6ZXI6IHRoaXMuY29uc3RydWN0b3IubmFtZSxcbiAgICAgIG9wZW5TZWN0aW9uczogdGhpcy5nZXRPcGVuU2VjdGlvbnMoKSxcbiAgICAgIHVyaTogdGhpcy5nZXRVUkkoKVxuICAgIH07XG4gIH1cblxuICBnZXRVUkkoKSB7XG4gICAgcmV0dXJuIHRoaXMucHJvcHMudXJpO1xuICB9XG5cbiAgZ2V0VGl0bGUoKSB7XG4gICAgcmV0dXJuICdXZWxjb21lIEd1aWRlJztcbiAgfVxuXG4gIGlzRXF1YWwob3RoZXIpIHtcbiAgICByZXR1cm4gb3RoZXIgaW5zdGFuY2VvZiBHdWlkZVZpZXc7XG4gIH1cblxuICBnZXRPcGVuU2VjdGlvbnMoKSB7XG4gICAgcmV0dXJuIEFycmF5LmZyb20odGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ2RldGFpbHNbb3Blbl0nKSkubWFwKFxuICAgICAgc2VjdGlvbkVsZW1lbnQgPT4gc2VjdGlvbkVsZW1lbnQuZGF0YXNldC5zZWN0aW9uXG4gICAgKTtcbiAgfVxuXG4gIGRpZENsaWNrUHJvamVjdEJ1dHRvbigpIHtcbiAgICBhdG9tLmNvbW1hbmRzLmRpc3BhdGNoKFxuICAgICAgYXRvbS52aWV3cy5nZXRWaWV3KGF0b20ud29ya3NwYWNlKSxcbiAgICAgICdhcHBsaWNhdGlvbjpvcGVuJ1xuICAgICk7XG4gIH1cblxuICBkaWRDbGlja0dpdEJ1dHRvbigpIHtcbiAgICBhdG9tLmNvbW1hbmRzLmRpc3BhdGNoKFxuICAgICAgYXRvbS52aWV3cy5nZXRWaWV3KGF0b20ud29ya3NwYWNlKSxcbiAgICAgICdnaXRodWI6dG9nZ2xlLWdpdC10YWInXG4gICAgKTtcbiAgfVxuXG4gIGRpZENsaWNrR2l0SHViQnV0dG9uKCkge1xuICAgIGF0b20uY29tbWFuZHMuZGlzcGF0Y2goXG4gICAgICBhdG9tLnZpZXdzLmdldFZpZXcoYXRvbS53b3Jrc3BhY2UpLFxuICAgICAgJ2dpdGh1Yjp0b2dnbGUtZ2l0aHViLXRhYidcbiAgICApO1xuICB9XG5cbiAgZGlkQ2xpY2tQYWNrYWdlc0J1dHRvbigpIHtcbiAgICBhdG9tLndvcmtzcGFjZS5vcGVuKCdhdG9tOi8vY29uZmlnL2luc3RhbGwnLCB7IHNwbGl0OiAnbGVmdCcgfSk7XG4gIH1cblxuICBkaWRDbGlja1RoZW1lc0J1dHRvbigpIHtcbiAgICBhdG9tLndvcmtzcGFjZS5vcGVuKCdhdG9tOi8vY29uZmlnL3RoZW1lcycsIHsgc3BsaXQ6ICdsZWZ0JyB9KTtcbiAgfVxuXG4gIGRpZENsaWNrU3R5bGluZ0J1dHRvbigpIHtcbiAgICBhdG9tLndvcmtzcGFjZS5vcGVuKCdhdG9tOi8vLnB1bHNhci9zdHlsZXNoZWV0JywgeyBzcGxpdDogJ2xlZnQnIH0pO1xuICB9XG5cbiAgZGlkQ2xpY2tJbml0U2NyaXB0QnV0dG9uKCkge1xuICAgIGF0b20ud29ya3NwYWNlLm9wZW4oJ2F0b206Ly8ucHVsc2FyL2luaXQtc2NyaXB0JywgeyBzcGxpdDogJ2xlZnQnIH0pO1xuICB9XG5cbiAgZGlkQ2xpY2tTbmlwcGV0c0J1dHRvbigpIHtcbiAgICBhdG9tLndvcmtzcGFjZS5vcGVuKCdhdG9tOi8vLnB1bHNhci9zbmlwcGV0cycsIHsgc3BsaXQ6ICdsZWZ0JyB9KTtcbiAgfVxuXG4gIGRpZENsaWNrVGVsZXR5cGVCdXR0b24oKSB7XG4gICAgYXRvbS53b3Jrc3BhY2Uub3BlbignYXRvbTovL2NvbmZpZy9wYWNrYWdlcy90ZWxldHlwZScsIHsgc3BsaXQ6ICdsZWZ0JyB9KTtcbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFHQTtBQUF3QjtBQUFBO0FBRVQsTUFBTUEsU0FBUyxDQUFDO0VBQzdCQyxXQUFXLENBQUNDLEtBQUssRUFBRTtJQUNqQixJQUFJLENBQUNBLEtBQUssR0FBR0EsS0FBSztJQUNsQixJQUFJLENBQUNDLEtBQUssR0FBR0MsSUFBSSxDQUFDQyxRQUFRLENBQUNDLElBQUk7SUFDL0IsSUFBSSxDQUFDQyxxQkFBcUIsR0FBRyxJQUFJLENBQUNBLHFCQUFxQixDQUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ2xFLElBQUksQ0FBQ0MsaUJBQWlCLEdBQUcsSUFBSSxDQUFDQSxpQkFBaUIsQ0FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQztJQUMxRCxJQUFJLENBQUNFLG9CQUFvQixHQUFHLElBQUksQ0FBQ0Esb0JBQW9CLENBQUNGLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDaEUsSUFBSSxDQUFDRyxzQkFBc0IsR0FBRyxJQUFJLENBQUNBLHNCQUFzQixDQUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3BFLElBQUksQ0FBQ0ksc0JBQXNCLEdBQUcsSUFBSSxDQUFDQSxzQkFBc0IsQ0FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQztJQUNwRSxJQUFJLENBQUNLLG9CQUFvQixHQUFHLElBQUksQ0FBQ0Esb0JBQW9CLENBQUNMLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDaEUsSUFBSSxDQUFDTSxxQkFBcUIsR0FBRyxJQUFJLENBQUNBLHFCQUFxQixDQUFDTixJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ2xFLElBQUksQ0FBQ08sd0JBQXdCLEdBQUcsSUFBSSxDQUFDQSx3QkFBd0IsQ0FBQ1AsSUFBSSxDQUFDLElBQUksQ0FBQztJQUN4RSxJQUFJLENBQUNRLHNCQUFzQixHQUFHLElBQUksQ0FBQ0Esc0JBQXNCLENBQUNSLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDcEVTLGFBQUksQ0FBQ0MsVUFBVSxDQUFDLElBQUksQ0FBQztFQUN2QjtFQUVBQyxNQUFNLEdBQUcsQ0FBQztFQUVWQyxNQUFNLEdBQUc7SUFDUCxPQUNFO01BQUssU0FBUyxFQUFDO0lBQWtCLEdBQy9CO01BQUssU0FBUyxFQUFDO0lBQW1CLEdBQ2hDO01BQVMsU0FBUyxFQUFDO0lBQWUsR0FDaEM7TUFBSSxTQUFTLEVBQUM7SUFBZSxtQkFBYyxJQUFJLENBQUNqQixLQUFLLE1BQU8sRUFFNUQ7TUFDRSxTQUFTLEVBQUM7SUFBYyxHQUNwQixJQUFJLENBQUNrQixlQUFlLENBQUMsU0FBUyxDQUFDLEdBRW5DO01BQVMsU0FBUyxFQUFDO0lBQWdDLGNBQzFDO01BQU0sU0FBUyxFQUFDO0lBQW1CLGFBQWUsQ0FDakQsRUFDVjtNQUFLLFNBQVMsRUFBQztJQUFnQixHQUM3Qiw2QkFDRTtNQUNFLFNBQVMsRUFBQyxhQUFhO01BQ3ZCLEdBQUcsRUFBQztJQUFtQyxFQUN2QyxDQUNBLEVBQ0osb0NBQ00sSUFBSSxDQUFDbEIsS0FBSyxxTUFHWixFQUNKLDZCQUNFO01BQ0UsR0FBRyxFQUFDLGVBQWU7TUFDbkIsT0FBTyxFQUFFLElBQUksQ0FBQ0kscUJBQXNCO01BQ3BDLFNBQVMsRUFBQztJQUFpQixvQkFHcEIsQ0FDUCxFQUNKO01BQUcsU0FBUyxFQUFDO0lBQWMsR0FDekIsK0NBQTJCLG1HQUUxQixJQUFJLENBQUNKLEtBQUssZ0JBQ1QsQ0FDQSxDQUNFLEVBRVY7TUFBUyxTQUFTLEVBQUM7SUFBYyxHQUFLLElBQUksQ0FBQ2tCLGVBQWUsQ0FBQyxLQUFLLENBQUMsR0FDL0Q7TUFBUyxTQUFTLEVBQUM7SUFBdUMsMkJBQ25DLEdBQUcsRUFDeEI7TUFBTSxLQUFLLEVBQUM7SUFBbUIsb0JBQXNCLENBQzdDLEVBQ1Y7TUFBSyxTQUFTLEVBQUM7SUFBZ0IsR0FDN0IsNkJBQ0U7TUFDRSxTQUFTLEVBQUMsYUFBYTtNQUN2QixHQUFHLEVBQUM7SUFBbUMsRUFDdkMsQ0FDQSxFQUNKLCtMQUlJLEVBQ0osNkJBQ0U7TUFDRSxPQUFPLEVBQUUsSUFBSSxDQUFDWixpQkFBa0I7TUFDaEMsU0FBUyxFQUFDO0lBQThCLHdCQUdqQyxFQUNUO01BQ0UsT0FBTyxFQUFFLElBQUksQ0FBQ0Msb0JBQXFCO01BQ25DLFNBQVMsRUFBQztJQUE4QiwyQkFHakMsQ0FDUCxFQUNKO01BQUcsU0FBUyxFQUFDO0lBQWMsR0FDekIsK0NBQTJCLG9EQUUzQjtNQUFNLFNBQVMsRUFBQztJQUFnQixFQUFHLGdDQUNqQyxDQUNBLENBQ0UsRUFFVjtNQUNFLFNBQVMsRUFBQztJQUFjLEdBQ3BCLElBQUksQ0FBQ1csZUFBZSxDQUFDLFVBQVUsQ0FBQyxHQUVwQztNQUFTLFNBQVMsRUFBQztJQUF1QyxvQ0FDMUIsR0FBRyxFQUNqQztNQUFNLEtBQUssRUFBQztJQUFtQixjQUFnQixDQUN2QyxFQUNWO01BQUssU0FBUyxFQUFDO0lBQWdCLEdBQzdCLDZCQUNFO01BQ0UsU0FBUyxFQUFDLGFBQWE7TUFDdkIsR0FBRyxFQUFDO0lBQWdDLEVBQ3BDLENBQ0EsRUFDSiw0R0FHSSxFQUNKLDZCQUNFO01BQ0UsT0FBTyxFQUFFLElBQUksQ0FBQ1Ysc0JBQXVCO01BQ3JDLFNBQVMsRUFBQztJQUE4Qiw0QkFFbEIsSUFBSSxDQUFDUixLQUFLLENBQ3pCLENBQ1AsQ0FDQSxDQUNFLEVBRVY7TUFDRSxTQUFTLEVBQUM7SUFBYyxHQUNwQixJQUFJLENBQUNrQixlQUFlLENBQUMsVUFBVSxDQUFDLEdBRXBDO01BQVMsU0FBUyxFQUFDO0lBQW1DLGlCQUMxQztNQUFNLFNBQVMsRUFBQztJQUFtQixhQUFlLENBQ3BELEVBQ1Y7TUFBSyxTQUFTLEVBQUM7SUFBZ0IsR0FDN0IsNkJBQ0U7TUFDRSxTQUFTLEVBQUMsYUFBYTtNQUN2QixHQUFHLEVBQUM7SUFBbUMsRUFDdkMsQ0FDQSxFQUNKLDhEQUNnQyxJQUFJLENBQUNsQixLQUFLLDBKQUd0QyxFQUNKLDZCQUNFO01BQ0UsR0FBRyxFQUFDLGdCQUFnQjtNQUNwQixPQUFPLEVBQUUsSUFBSSxDQUFDUyxzQkFBdUI7TUFDckMsU0FBUyxFQUFDO0lBQWlCLG9CQUdwQixDQUNQLEVBQ0o7TUFBRyxTQUFTLEVBQUM7SUFBYyxHQUN6QiwrQ0FBMkIscURBRXpCLENBQ0EsQ0FDRSxFQUVWO01BQ0UsU0FBUyxFQUFDO0lBQWMsR0FDcEIsSUFBSSxDQUFDUyxlQUFlLENBQUMsUUFBUSxDQUFDLEdBRWxDO01BQVMsU0FBUyxFQUFDO0lBQW9DLGdCQUM1QztNQUFNLEtBQUssRUFBQztJQUFtQixXQUFhLENBQzdDLEVBQ1Y7TUFBSyxTQUFTLEVBQUM7SUFBZ0IsR0FDN0IsNkJBQ0U7TUFDRSxTQUFTLEVBQUMsYUFBYTtNQUN2QixHQUFHLEVBQUM7SUFBaUMsRUFDckMsQ0FDQSxFQUNKLDZCQUFJLElBQUksQ0FBQ2xCLEtBQUssc0RBQXNELEVBQ3BFLDZCQUNFO01BQ0UsR0FBRyxFQUFDLGNBQWM7TUFDbEIsT0FBTyxFQUFFLElBQUksQ0FBQ1Usb0JBQXFCO01BQ25DLFNBQVMsRUFBQztJQUFpQiwyQkFHcEIsQ0FDUCxFQUNKLDRFQUM4QyxJQUFJLENBQUNWLEtBQUssbUdBR3BELEVBQ0o7TUFBRyxTQUFTLEVBQUM7SUFBYyxHQUN6QiwrQ0FBMkIsOENBRXpCLENBQ0EsQ0FDRSxFQUVWO01BQ0UsU0FBUyxFQUFDO0lBQWMsR0FDcEIsSUFBSSxDQUFDa0IsZUFBZSxDQUFDLFNBQVMsQ0FBQyxHQUVuQztNQUFTLFNBQVMsRUFBQztJQUFvQyxxQkFDdkM7TUFBTSxLQUFLLEVBQUM7SUFBbUIsYUFBZSxDQUNwRCxFQUNWO01BQUssU0FBUyxFQUFDO0lBQWdCLEdBQzdCLDZCQUNFO01BQ0UsU0FBUyxFQUFDLGFBQWE7TUFDdkIsR0FBRyxFQUFDO0lBQWdDLEVBQ3BDLENBQ0EsRUFDSiw4RkFFSSxFQUNKLDZCQUNFO01BQ0UsR0FBRyxFQUFDLGVBQWU7TUFDbkIsT0FBTyxFQUFFLElBQUksQ0FBQ1AscUJBQXNCO01BQ3BDLFNBQVMsRUFBQztJQUFpQiwwQkFHcEIsQ0FDUCxFQUNKLGtGQUF5RCxFQUN6RDtNQUFHLFNBQVMsRUFBQztJQUFjLEdBQ3pCLCtDQUEyQiw4Q0FDckIsSUFBSSxDQUFDUSxzQkFBc0IsRUFBRSxNQUNqQyxDQUNBLENBQ0UsRUFFVjtNQUNFLFNBQVMsRUFBQztJQUFjLEdBQ3BCLElBQUksQ0FBQ0QsZUFBZSxDQUFDLGFBQWEsQ0FBQyxHQUV2QztNQUFTLFNBQVMsRUFBQztJQUFnQyxtQkFDckM7TUFBTSxLQUFLLEVBQUM7SUFBbUIsaUJBQW1CLENBQ3RELEVBQ1Y7TUFBSyxTQUFTLEVBQUM7SUFBZ0IsR0FDN0IsNkJBQ0U7TUFDRSxTQUFTLEVBQUMsYUFBYTtNQUN2QixHQUFHLEVBQUM7SUFBZ0MsRUFDcEMsQ0FDQSxFQUNKLHlKQUdHLElBQUksQ0FBQ2xCLEtBQUssTUFDVCxFQUNKLDZCQUNFO01BQ0UsR0FBRyxFQUFDLGtCQUFrQjtNQUN0QixPQUFPLEVBQUUsSUFBSSxDQUFDWSx3QkFBeUI7TUFDdkMsU0FBUyxFQUFDO0lBQWlCLDJCQUdwQixDQUNQLEVBQ0osbUZBQTBELEVBQzFEO01BQUcsU0FBUyxFQUFDO0lBQWMsR0FDekIsK0NBQTJCLGlEQUNuQixJQUFJLENBQUNPLHNCQUFzQixFQUFFLE1BQ25DLENBQ0EsQ0FDRSxFQUVWO01BQ0UsU0FBUyxFQUFDO0lBQWMsR0FDcEIsSUFBSSxDQUFDRCxlQUFlLENBQUMsVUFBVSxDQUFDLEdBRXBDO01BQVMsU0FBUyxFQUFDO0lBQWdDLGFBQzNDO01BQU0sS0FBSyxFQUFDO0lBQW1CLGFBQWUsQ0FDNUMsRUFDVjtNQUFLLFNBQVMsRUFBQztJQUFnQixHQUM3Qiw2QkFDRTtNQUNFLFNBQVMsRUFBQyxhQUFhO01BQ3ZCLEdBQUcsRUFBQztJQUFnQyxFQUNwQyxDQUNBLEVBQ0osNkJBQ0csSUFBSSxDQUFDbEIsS0FBSyxnSkFHVCxFQUNKLDZCQUNFO01BQ0UsR0FBRyxFQUFDLGdCQUFnQjtNQUNwQixPQUFPLEVBQUUsSUFBSSxDQUFDYSxzQkFBdUI7TUFDckMsU0FBUyxFQUFDO0lBQWlCLHdCQUdwQixDQUNQLEVBQ0osNkRBQzhCLHVDQUFpQixlQUFVLEdBQUcsRUFDMUQsc0NBQWdCLFlBQU0sdUNBQWlCLDhDQUVyQyxFQUNKO01BQUcsU0FBUyxFQUFDO0lBQWMsR0FDekIsK0NBQTJCLDRDQUN4QixJQUFJLENBQUNNLHNCQUFzQixFQUFFLE1BQzlCLENBQ0EsQ0FDRSxFQUVWO01BQ0UsU0FBUyxFQUFDO0lBQWMsR0FDcEIsSUFBSSxDQUFDRCxlQUFlLENBQUMsV0FBVyxDQUFDLEdBRXJDO01BQVMsU0FBUyxFQUFDO0lBQW9DLGFBQy9DO01BQU0sS0FBSyxFQUFDO0lBQW1CLHdCQUEwQixDQUN2RCxFQUNWO01BQUssU0FBUyxFQUFDO0lBQWdCLEdBQzdCLDZCQUNFO01BQ0UsU0FBUyxFQUFDLGFBQWE7TUFDdkIsR0FBRyxFQUFDO0lBQW9DLEVBQ3hDLENBQ0EsRUFDSixtRkFDcUQsR0FBRyxFQUN0RDtNQUFLLFNBQVMsRUFBQztJQUFhLEdBQ3pCLElBQUksQ0FBQ0UsMkJBQTJCLEVBQUUsQ0FDL0Isc0VBRUMsSUFBSSxDQUFDcEIsS0FBSyxnRkFFZixFQUNKLDhGQUNnRSxHQUFHLEVBQ2pFO01BQUssU0FBUyxFQUFDO0lBQWEsR0FDekIsSUFBSSxDQUFDb0IsMkJBQTJCLEVBQUUsQ0FDL0IsRUFBQyxHQUFHLHFCQUNLO01BQU0sU0FBUyxFQUFDO0lBQWdCLGFBQWUsTUFFNUQsQ0FDQSxDQUNFLENBQ0YsQ0FDTixDQUNGO0VBRVY7RUFFQUYsZUFBZSxDQUFDRyxXQUFXLEVBQUU7SUFDM0IsTUFBTXRCLEtBQUssR0FBRztNQUNadUIsT0FBTyxFQUFFO1FBQUVDLE9BQU8sRUFBRUY7TUFBWTtJQUNsQyxDQUFDO0lBQ0QsSUFDRSxJQUFJLENBQUN0QixLQUFLLENBQUN5QixZQUFZLElBQ3ZCLElBQUksQ0FBQ3pCLEtBQUssQ0FBQ3lCLFlBQVksQ0FBQ0MsT0FBTyxDQUFDSixXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsRUFDbkQ7TUFDQXRCLEtBQUssQ0FBQzJCLElBQUksR0FBRyxJQUFJO0lBQ25CO0lBQ0EsT0FBTzNCLEtBQUs7RUFDZDtFQUVBcUIsMkJBQTJCLEdBQUc7SUFDNUIsSUFBSU8sT0FBTyxDQUFDQyxRQUFRLEtBQUssUUFBUSxFQUFFO01BQ2pDLE9BQU8sYUFBYTtJQUN0QixDQUFDLE1BQU07TUFDTCxPQUFPLGNBQWM7SUFDdkI7RUFDRjtFQUVBVCxzQkFBc0IsR0FBRztJQUN2QixJQUFJUSxPQUFPLENBQUNDLFFBQVEsS0FBSyxRQUFRLEVBQUU7TUFDakMsT0FBTyxRQUFRO0lBQ2pCLENBQUMsTUFBTSxJQUFJRCxPQUFPLENBQUNDLFFBQVEsS0FBSyxPQUFPLEVBQUU7TUFDdkMsT0FBTyxNQUFNO0lBQ2YsQ0FBQyxNQUFNO01BQ0wsT0FBTyxNQUFNO0lBQ2Y7RUFDRjtFQUVBQyxTQUFTLEdBQUc7SUFDVixPQUFPO01BQ0xDLFlBQVksRUFBRSxJQUFJLENBQUNoQyxXQUFXLENBQUNLLElBQUk7TUFDbkNxQixZQUFZLEVBQUUsSUFBSSxDQUFDTyxlQUFlLEVBQUU7TUFDcENDLEdBQUcsRUFBRSxJQUFJLENBQUNDLE1BQU07SUFDbEIsQ0FBQztFQUNIO0VBRUFBLE1BQU0sR0FBRztJQUNQLE9BQU8sSUFBSSxDQUFDbEMsS0FBSyxDQUFDaUMsR0FBRztFQUN2QjtFQUVBRSxRQUFRLEdBQUc7SUFDVCxPQUFPLGVBQWU7RUFDeEI7RUFFQUMsT0FBTyxDQUFDQyxLQUFLLEVBQUU7SUFDYixPQUFPQSxLQUFLLFlBQVl2QyxTQUFTO0VBQ25DO0VBRUFrQyxlQUFlLEdBQUc7SUFDaEIsT0FBT00sS0FBSyxDQUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDQyxPQUFPLENBQUNDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUNDLEdBQUcsQ0FDbkVDLGNBQWMsSUFBSUEsY0FBYyxDQUFDcEIsT0FBTyxDQUFDQyxPQUFPLENBQ2pEO0VBQ0g7RUFFQW5CLHFCQUFxQixHQUFHO0lBQ3RCSCxJQUFJLENBQUMwQyxRQUFRLENBQUNDLFFBQVEsQ0FDcEIzQyxJQUFJLENBQUM0QyxLQUFLLENBQUNDLE9BQU8sQ0FBQzdDLElBQUksQ0FBQzhDLFNBQVMsQ0FBQyxFQUNsQyxrQkFBa0IsQ0FDbkI7RUFDSDtFQUVBekMsaUJBQWlCLEdBQUc7SUFDbEJMLElBQUksQ0FBQzBDLFFBQVEsQ0FBQ0MsUUFBUSxDQUNwQjNDLElBQUksQ0FBQzRDLEtBQUssQ0FBQ0MsT0FBTyxDQUFDN0MsSUFBSSxDQUFDOEMsU0FBUyxDQUFDLEVBQ2xDLHVCQUF1QixDQUN4QjtFQUNIO0VBRUF4QyxvQkFBb0IsR0FBRztJQUNyQk4sSUFBSSxDQUFDMEMsUUFBUSxDQUFDQyxRQUFRLENBQ3BCM0MsSUFBSSxDQUFDNEMsS0FBSyxDQUFDQyxPQUFPLENBQUM3QyxJQUFJLENBQUM4QyxTQUFTLENBQUMsRUFDbEMsMEJBQTBCLENBQzNCO0VBQ0g7RUFFQXRDLHNCQUFzQixHQUFHO0lBQ3ZCUixJQUFJLENBQUM4QyxTQUFTLENBQUNyQixJQUFJLENBQUMsdUJBQXVCLEVBQUU7TUFBRXNCLEtBQUssRUFBRTtJQUFPLENBQUMsQ0FBQztFQUNqRTtFQUVBdEMsb0JBQW9CLEdBQUc7SUFDckJULElBQUksQ0FBQzhDLFNBQVMsQ0FBQ3JCLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtNQUFFc0IsS0FBSyxFQUFFO0lBQU8sQ0FBQyxDQUFDO0VBQ2hFO0VBRUFyQyxxQkFBcUIsR0FBRztJQUN0QlYsSUFBSSxDQUFDOEMsU0FBUyxDQUFDckIsSUFBSSxDQUFDLDJCQUEyQixFQUFFO01BQUVzQixLQUFLLEVBQUU7SUFBTyxDQUFDLENBQUM7RUFDckU7RUFFQXBDLHdCQUF3QixHQUFHO0lBQ3pCWCxJQUFJLENBQUM4QyxTQUFTLENBQUNyQixJQUFJLENBQUMsNEJBQTRCLEVBQUU7TUFBRXNCLEtBQUssRUFBRTtJQUFPLENBQUMsQ0FBQztFQUN0RTtFQUVBbkMsc0JBQXNCLEdBQUc7SUFDdkJaLElBQUksQ0FBQzhDLFNBQVMsQ0FBQ3JCLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtNQUFFc0IsS0FBSyxFQUFFO0lBQU8sQ0FBQyxDQUFDO0VBQ25FO0VBRUF4QyxzQkFBc0IsR0FBRztJQUN2QlAsSUFBSSxDQUFDOEMsU0FBUyxDQUFDckIsSUFBSSxDQUFDLGlDQUFpQyxFQUFFO01BQUVzQixLQUFLLEVBQUU7SUFBTyxDQUFDLENBQUM7RUFDM0U7QUFDRjtBQUFDO0FBQUEifQ==