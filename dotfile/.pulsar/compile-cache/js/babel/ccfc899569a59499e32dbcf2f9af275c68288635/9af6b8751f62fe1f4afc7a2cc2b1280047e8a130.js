"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _atom = require("atom");
var _electron = require("electron");
var _etch = _interopRequireDefault(require("etch"));
var _utils = require("./utils");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
/** @babel */
/** @jsx etch.dom */

let marked = null;
class PackageCard {
  constructor(pack, settingsView, packageManager, options = {}) {
    this.pack = pack;
    this.settingsView = settingsView;
    this.packageManager = packageManager;
    this.disposables = new _atom.CompositeDisposable();

    // It might be useful to either wrap this.pack in a class that has a
    // ::validate method, or add a method here. At the moment I think all cases
    // of malformed package metadata are handled here and in ::content but belt
    // and suspenders, you know
    this.client = this.packageManager.getClient();
    this.type = this.pack.theme ? 'theme' : 'package';
    this.name = this.pack.name;
    this.onSettingsView = options.onSettingsView;
    if (this.pack.latestVersion !== this.pack.version) {
      this.newVersion = this.pack.latestVersion;
    }
    if (this.pack.apmInstallSource && this.pack.apmInstallSource.type === 'git') {
      if (this.pack.apmInstallSource.sha !== this.pack.latestSha) {
        this.newSha = this.pack.latestSha;
      }
    }

    // Default to displaying the download count
    if (!options.stats) {
      options.stats = {
        downloads: true
      };
    }
    _etch.default.initialize(this);
    this.displayStats(options);
    this.handlePackageEvents();
    this.handleButtonEvents(options);
    this.loadCachedMetadata();

    // themes have no status and cannot be dis/enabled
    if (this.type === 'theme') {
      this.refs.statusIndicator.remove();
      this.refs.enablementButton.remove();
    }
    if (atom.packages.isBundledPackage(this.pack.name)) {
      this.refs.installButtonGroup.remove();
      this.refs.uninstallButton.remove();
    }
    if (!this.newVersion && !this.newSha) {
      this.refs.updateButtonGroup.style.display = 'none';
    }
    this.hasCompatibleVersion = true;
    this.updateInterfaceState();
  }
  render() {
    const displayName = (this.pack.gitUrlInfo ? this.pack.gitUrlInfo.project : this.pack.name) || '';
    const owner = (0, _utils.ownerFromRepository)(this.pack.repository);
    const description = this.pack.description || '';
    return _etch.default.dom("div", {
      className: "package-card col-lg-8"
    }, _etch.default.dom("div", {
      ref: "statsContainer",
      className: "stats pull-right"
    }, _etch.default.dom("span", {
      ref: "packageStars",
      className: "stats-item"
    }, _etch.default.dom("span", {
      ref: "stargazerIcon",
      className: "icon icon-star"
    }), _etch.default.dom("span", {
      ref: "stargazerCount",
      className: "value"
    })), _etch.default.dom("span", {
      ref: "packageDownloads",
      className: "stats-item"
    }, _etch.default.dom("span", {
      ref: "downloadIcon",
      className: "icon icon-cloud-download"
    }), _etch.default.dom("span", {
      ref: "downloadCount",
      className: "value"
    }))), _etch.default.dom("div", {
      className: "body"
    }, _etch.default.dom("h4", {
      className: "card-name"
    }, _etch.default.dom("a", {
      className: "package-name",
      ref: "packageName"
    }, displayName), _etch.default.dom("span", {
      className: "package-version"
    }, _etch.default.dom("span", {
      ref: "versionValue",
      className: "value"
    }, String(this.pack.version)))), _etch.default.dom("span", {
      ref: "packageDescription",
      className: "package-description"
    }, description), _etch.default.dom("div", {
      ref: "packageMessage",
      className: "package-message"
    })), _etch.default.dom("div", {
      className: "meta"
    }, _etch.default.dom("div", {
      ref: "metaUserContainer",
      className: "meta-user"
    }, _etch.default.dom("a", {
      ref: "avatarLink"
    }, _etch.default.dom("img", {
      ref: "avatar",
      className: "avatar",
      src: "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
    })), _etch.default.dom("a", {
      ref: "loginLink",
      className: "author"
    }, owner)), _etch.default.dom("div", {
      className: "meta-controls"
    }, _etch.default.dom("div", {
      className: "btn-toolbar"
    }, _etch.default.dom("div", {
      ref: "updateButtonGroup",
      className: "btn-group"
    }, _etch.default.dom("button", {
      type: "button",
      className: "btn btn-info icon icon-cloud-download install-button",
      ref: "updateButton"
    }, "Update")), _etch.default.dom("div", {
      ref: "installButtonGroup",
      className: "btn-group"
    }, _etch.default.dom("button", {
      type: "button",
      className: "btn btn-info icon icon-cloud-download install-button",
      ref: "installButton"
    }, "Install")), _etch.default.dom("div", {
      ref: "packageActionButtonGroup",
      className: "btn-group"
    }, _etch.default.dom("button", {
      type: "button",
      className: "btn icon icon-gear settings",
      ref: "settingsButton"
    }, "Settings"), _etch.default.dom("button", {
      type: "button",
      className: "btn icon icon-trashcan uninstall-button",
      ref: "uninstallButton"
    }, "Uninstall"), _etch.default.dom("button", {
      type: "button",
      className: "btn icon icon-playback-pause enablement",
      ref: "enablementButton"
    }, _etch.default.dom("span", {
      className: "disable-text"
    }, "Disable")), _etch.default.dom("button", {
      type: "button",
      className: "btn status-indicator",
      tabIndex: "-1",
      ref: "statusIndicator"
    }))))));
  }
  locateCompatiblePackageVersion(callback) {
    this.packageManager.loadCompatiblePackageVersion(this.pack.name, (err, pack) => {
      if (err != null) {
        console.error(err);
      }
      const packageVersion = pack.version;

      // A compatible version exist, we activate the install button and
      // set this.installablePack so that the install action installs the
      // compatible version of the package.
      if (packageVersion) {
        this.refs.versionValue.textContent = packageVersion;
        if (packageVersion !== this.pack.version) {
          this.refs.versionValue.classList.add('text-warning');
          this.refs.packageMessage.classList.add('text-warning');
          this.refs.packageMessage.textContent = `Version ${packageVersion} is not the latest version available for this package, but it's the latest that is compatible with your version of Atom.`;
        }
        this.installablePack = pack;
        this.hasCompatibleVersion = true;
      } else {
        this.hasCompatibleVersion = false;
        this.refs.versionValue.classList.add('text-error');
        this.refs.packageMessage.classList.add('text-error');
        this.refs.packageMessage.insertAdjacentText('beforeend', `There's no version of this package that is compatible with your Atom version. The version must satisfy ${this.pack.engines.atom}.`);
        console.error(`No available version compatible with the installed Atom version: ${atom.getVersion()}`);
      }
      callback();
    });
  }
  handleButtonEvents(options) {
    if (options && options.onSettingsView) {
      this.refs.settingsButton.style.display = 'none';
    } else {
      const clickHandler = event => {
        event.stopPropagation();
        this.settingsView.showPanel(this.pack.name, {
          back: options ? options.back : null,
          pack: this.pack
        });
      };
      this.element.addEventListener('click', clickHandler);
      this.disposables.add(new _atom.Disposable(() => {
        this.element.removeEventListener('click', clickHandler);
      }));
      this.refs.settingsButton.addEventListener('click', clickHandler);
      this.disposables.add(new _atom.Disposable(() => {
        this.refs.settingsButton.removeEventListener('click', clickHandler);
      }));
    }
    const installButtonClickHandler = event => {
      event.stopPropagation();
      this.install();
    };
    this.refs.installButton.addEventListener('click', installButtonClickHandler);
    this.disposables.add(new _atom.Disposable(() => {
      this.refs.installButton.removeEventListener('click', installButtonClickHandler);
    }));
    const uninstallButtonClickHandler = event => {
      event.stopPropagation();
      this.uninstall();
    };
    this.refs.uninstallButton.addEventListener('click', uninstallButtonClickHandler);
    this.disposables.add(new _atom.Disposable(() => {
      this.refs.uninstallButton.removeEventListener('click', uninstallButtonClickHandler);
    }));
    const updateButtonClickHandler = event => {
      event.stopPropagation();
      this.update().then(() => {
        let oldVersion = '';
        let newVersion = '';
        if (this.pack.apmInstallSource && this.pack.apmInstallSource.type === 'git') {
          oldVersion = this.pack.apmInstallSource.sha.substr(0, 8);
          newVersion = `${this.pack.latestSha.substr(0, 8)}`;
        } else if (this.pack.version && this.pack.latestVersion) {
          oldVersion = this.pack.version;
          newVersion = this.pack.latestVersion;
        }
        let detail = '';
        if (oldVersion && newVersion) {
          detail = `${oldVersion} -> ${newVersion}`;
        }
        const notification = atom.notifications.addSuccess(`Restart Atom to complete the update of \`${this.pack.name}\`.`, {
          dismissable: true,
          buttons: [{
            text: 'Restart now',
            onDidClick() {
              return atom.restartApplication();
            }
          }, {
            text: 'I\'ll do it later',
            onDidClick() {
              notification.dismiss();
            }
          }],
          detail
        });
      });
    };
    this.refs.updateButton.addEventListener('click', updateButtonClickHandler);
    this.disposables.add(new _atom.Disposable(() => {
      this.refs.updateButton.removeEventListener('click', updateButtonClickHandler);
    }));
    const packageNameClickHandler = event => {
      event.stopPropagation();
      _electron.shell.openExternal(`https://web.pulsar-edit.dev/packages/${this.pack.name}`);
    };
    this.refs.packageName.addEventListener('click', packageNameClickHandler);
    this.disposables.add(new _atom.Disposable(() => {
      this.refs.packageName.removeEventListener('click', packageNameClickHandler);
    }));
    const packageAuthorClickHandler = event => {
      event.stopPropagation();
      _electron.shell.openExternal(`https://pulsar-edit.dev/users/${(0, _utils.ownerFromRepository)(this.pack.repository)}`);
    };
    this.refs.loginLink.addEventListener('click', packageAuthorClickHandler);
    this.disposables.add(new _atom.Disposable(() => {
      this.refs.loginLink.removeEventListener('click', packageAuthorClickHandler);
    }));
    this.refs.avatarLink.addEventListener('click', packageAuthorClickHandler);
    this.disposables.add(new _atom.Disposable(() => {
      this.refs.avatarLink.removeEventListener('click', packageAuthorClickHandler);
    }));
    const enablementButtonClickHandler = event => {
      event.stopPropagation();
      event.preventDefault();
      if (this.isDisabled()) {
        atom.packages.enablePackage(this.pack.name);
      } else {
        atom.packages.disablePackage(this.pack.name);
      }
    };
    this.refs.enablementButton.addEventListener('click', enablementButtonClickHandler);
    this.disposables.add(new _atom.Disposable(() => {
      this.refs.enablementButton.removeEventListener('click', enablementButtonClickHandler);
    }));
    const packageMessageClickHandler = event => {
      const target = event.target.closest('a');
      if (target) {
        event.stopPropagation();
        event.preventDefault();
        if (target.href && target.href.startsWith('atom:')) {
          atom.workspace.open(target.href);
        }
      }
    };
    this.refs.packageMessage.addEventListener('click', packageMessageClickHandler);
    this.disposables.add(new _atom.Disposable(() => {
      this.refs.packageMessage.removeEventListener('click', packageMessageClickHandler);
    }));
  }
  destroy() {
    this.disposables.dispose();
    return _etch.default.destroy(this);
  }
  loadCachedMetadata() {
    this.client.avatar((0, _utils.ownerFromRepository)(this.pack.repository), (err, avatarPath) => {
      if (!err && avatarPath) {
        this.refs.avatar.src = `file://${avatarPath}`;
      }
    });
    this.client.package(this.pack.name, (err, data) => {
      // We don't need to actually handle the error here, we can just skip
      // showing the download count if there's a problem.
      if (!err) {
        if (data == null) {
          data = {};
        }
        if (this.pack.apmInstallSource && this.pack.apmInstallSource.type === 'git') {
          this.refs.downloadIcon.classList.remove('icon-cloud-download');
          this.refs.downloadIcon.classList.add('icon-git-branch');
          this.refs.downloadCount.textContent = this.pack.apmInstallSource.sha.substr(0, 8);
        } else {
          this.refs.stargazerCount.textContent = data.stargazers_count ? data.stargazers_count.toLocaleString() : '';
          this.refs.downloadCount.textContent = data.downloads ? data.downloads.toLocaleString() : '';
        }
      }
    });
  }
  updateInterfaceState() {
    this.refs.versionValue.textContent = (this.installablePack ? this.installablePack.version : null) || this.pack.version;
    if (this.pack.apmInstallSource && this.pack.apmInstallSource.type === 'git') {
      this.refs.downloadCount.textContent = this.pack.apmInstallSource.sha.substr(0, 8);
    }
    this.updateSettingsState();
    this.updateInstalledState();
    this.updateDisabledState();
  }
  updateSettingsState() {
    if (this.hasSettings() && !this.onSettingsView) {
      this.refs.settingsButton.style.display = '';
    } else {
      this.refs.settingsButton.style.display = 'none';
    }
  }

  // Section: disabled state updates

  updateDisabledState() {
    if (this.isDisabled()) {
      this.displayDisabledState();
    } else if (this.element.classList.contains('disabled')) {
      this.displayEnabledState();
    }
  }
  displayEnabledState() {
    this.element.classList.remove('disabled');
    if (this.type === 'theme') {
      this.refs.enablementButton.style.display = 'none';
    }
    this.refs.enablementButton.querySelector('.disable-text').textContent = 'Disable';
    this.refs.enablementButton.classList.add('icon-playback-pause');
    this.refs.enablementButton.classList.remove('icon-playback-play');
    this.refs.statusIndicator.classList.remove('is-disabled');
  }
  displayDisabledState() {
    this.element.classList.add('disabled');
    this.refs.enablementButton.querySelector('.disable-text').textContent = 'Enable';
    this.refs.enablementButton.classList.add('icon-playback-play');
    this.refs.enablementButton.classList.remove('icon-playback-pause');
    this.refs.statusIndicator.classList.add('is-disabled');
    this.refs.enablementButton.disabled = false;
  }

  // Section: installed state updates

  updateInstalledState() {
    if (this.isInstalled()) {
      this.displayInstalledState();
    } else {
      this.displayNotInstalledState();
    }
  }
  displayInstalledState() {
    if (this.newVersion || this.newSha) {
      this.refs.updateButtonGroup.style.display = '';
      if (this.newVersion) {
        this.refs.updateButton.textContent = `Update to ${this.newVersion}`;
      } else if (this.newSha) {
        this.refs.updateButton.textContent = `Update to ${this.newSha.substr(0, 8)}`;
      }
    } else {
      this.refs.updateButtonGroup.style.display = 'none';
    }
    this.refs.installButtonGroup.style.display = 'none';
    this.refs.packageActionButtonGroup.style.display = '';
    this.refs.uninstallButton.style.display = '';
  }
  displayNotInstalledState() {
    this.refs.uninstallButton.style.display = 'none';
    const atomVersion = this.packageManager.normalizeVersion(atom.getVersion());
    if (!this.packageManager.satisfiesVersion(atomVersion, this.pack)) {
      this.hasCompatibleVersion = false;
      this.setNotInstalledStateButtons();
      this.locateCompatiblePackageVersion(() => {
        this.setNotInstalledStateButtons();
      });
    } else {
      this.setNotInstalledStateButtons();
    }
  }
  setNotInstalledStateButtons() {
    if (!this.hasCompatibleVersion) {
      this.refs.installButtonGroup.style.display = 'none';
      this.refs.updateButtonGroup.style.display = 'none';
    } else if (this.newVersion || this.newSha) {
      this.refs.updateButtonGroup.style.display = '';
      this.refs.installButtonGroup.style.display = 'none';
    } else {
      this.refs.updateButtonGroup.style.display = 'none';
      this.refs.installButtonGroup.style.display = '';
    }
    this.refs.packageActionButtonGroup.style.display = 'none';
  }
  displayStats(options) {
    if (options && options.stats && options.stats.downloads) {
      this.refs.packageDownloads.style.display = '';
    } else {
      this.refs.packageDownloads.style.display = 'none';
    }
    if (options && options.stats && options.stats.stars) {
      this.refs.packageStars.style.display = '';
    } else {
      this.refs.packageStars.style.display = 'none';
    }
  }
  displayGitPackageInstallInformation() {
    this.refs.metaUserContainer.remove();
    this.refs.statsContainer.remove();
    const {
      gitUrlInfo
    } = this.pack;
    if (gitUrlInfo.default === 'shortcut') {
      this.refs.packageDescription.textContent = gitUrlInfo.https();
    } else {
      this.refs.packageDescription.textContent = gitUrlInfo.toString();
    }
    this.refs.installButton.classList.remove('icon-cloud-download');
    this.refs.installButton.classList.add('icon-git-commit');
    this.refs.updateButton.classList.remove('icon-cloud-download');
    this.refs.updateButton.classList.add('icon-git-commit');
  }
  displayAvailableUpdate(newVersion) {
    this.newVersion = newVersion;
    this.updateInterfaceState();
  }
  handlePackageEvents() {
    this.disposables.add(atom.packages.onDidDeactivatePackage(pack => {
      if (pack.name === this.pack.name) {
        this.updateDisabledState();
      }
    }));
    this.disposables.add(atom.packages.onDidActivatePackage(pack => {
      if (pack.name === this.pack.name) {
        this.updateDisabledState();
      }
    }));
    this.disposables.add(atom.config.onDidChange('core.disabledPackages', () => {
      this.updateDisabledState();
    }));
    this.subscribeToPackageEvent('package-installing theme-installing', () => {
      this.updateInterfaceState();
      this.refs.installButton.disabled = true;
      this.refs.installButton.classList.add('is-installing');
    });
    this.subscribeToPackageEvent('package-updating theme-updating', () => {
      this.updateInterfaceState();
      this.refs.updateButton.disabled = true;
      this.refs.updateButton.classList.add('is-installing');
    });
    this.subscribeToPackageEvent('package-uninstalling theme-uninstalling', () => {
      this.updateInterfaceState();
      this.refs.enablementButton.disabled = true;
      this.refs.uninstallButton.disabled = true;
      this.refs.uninstallButton.classList.add('is-uninstalling');
    });
    this.subscribeToPackageEvent('package-installed package-install-failed theme-installed theme-install-failed', () => {
      const loadedPack = atom.packages.getLoadedPackage(this.pack.name);
      const version = loadedPack && loadedPack.metadata ? loadedPack.metadata.version : null;
      if (version) {
        this.pack.version = version;
      }
      this.refs.installButton.disabled = false;
      this.refs.installButton.classList.remove('is-installing');
      this.updateInterfaceState();
    });
    this.subscribeToPackageEvent('package-updated theme-updated', () => {
      const loadedPack = atom.packages.getLoadedPackage(this.pack.name);
      const metadata = loadedPack ? loadedPack.metadata : null;
      if (metadata && metadata.version) {
        this.pack.version = metadata.version;
      }
      if (metadata && metadata.apmInstallSource) {
        this.pack.apmInstallSource = metadata.apmInstallSource;
      }
      this.newVersion = null;
      this.newSha = null;
      this.refs.updateButton.disabled = false;
      this.refs.updateButton.classList.remove('is-installing');
      this.updateInterfaceState();
    });
    this.subscribeToPackageEvent('package-update-failed theme-update-failed', () => {
      this.refs.updateButton.disabled = false;
      this.refs.updateButton.classList.remove('is-installing');
      this.updateInterfaceState();
    });
    this.subscribeToPackageEvent('package-uninstalled package-uninstall-failed theme-uninstalled theme-uninstall-failed', () => {
      this.newVersion = null;
      this.newSha = null;
      this.refs.enablementButton.disabled = false;
      this.refs.uninstallButton.disabled = false;
      this.refs.uninstallButton.classList.remove('is-uninstalling');
      this.updateInterfaceState();
    });
  }
  isInstalled() {
    return this.packageManager.isPackageInstalled(this.pack.name);
  }
  isDisabled() {
    return atom.packages.isPackageDisabled(this.pack.name);
  }
  hasSettings() {
    return this.packageManager.packageHasSettings(this.pack.name);
  }
  subscribeToPackageEvent(event, callback) {
    this.disposables.add(this.packageManager.on(event, ({
      pack,
      error
    }) => {
      if (pack.pack != null) {
        pack = pack.pack;
      }
      const packageName = pack.name;
      if (packageName === this.pack.name) {
        callback(pack, error);
      }
    }));
  }

  /*
  Section: Methods that should be on a Package model
  */

  install() {
    this.packageManager.install(this.installablePack != null ? this.installablePack : this.pack, error => {
      if (error != null) {
        console.error(`Installing ${this.type} ${this.pack.name} failed`, error.stack != null ? error.stack : error, error.stderr);
      } else {
        // if a package was disabled before installing it, re-enable it
        if (this.isDisabled()) {
          atom.packages.enablePackage(this.pack.name);
        }
      }
    });
  }
  update() {
    if (!this.newVersion && !this.newSha) {
      return Promise.resolve();
    }
    const pack = this.installablePack != null ? this.installablePack : this.pack;
    const version = this.newVersion ? `v${this.newVersion}` : `#${this.newSha.substr(0, 8)}`;
    return new Promise((resolve, reject) => {
      this.packageManager.update(pack, this.newVersion, error => {
        if (error != null) {
          atom.assert(false, 'Package update failed', assertionError => {
            assertionError.metadata = {
              type: this.type,
              name: pack.name,
              version,
              errorMessage: error.message,
              errorStack: error.stack,
              errorStderr: error.stderr
            };
          });
          console.error(`Updating ${this.type} ${pack.name} to ${version} failed:\n`, error, error.stderr != null ? error.stderr : '');
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }
  uninstall() {
    this.packageManager.uninstall(this.pack, error => {
      if (error != null) {
        console.error(`Uninstalling ${this.type} ${this.pack.name} failed`, error.stack != null ? error.stack : error, error.stderr);
      }
    });
  }
}
exports.default = PackageCard;
module.exports = exports.default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJtYXJrZWQiLCJQYWNrYWdlQ2FyZCIsImNvbnN0cnVjdG9yIiwicGFjayIsInNldHRpbmdzVmlldyIsInBhY2thZ2VNYW5hZ2VyIiwib3B0aW9ucyIsImRpc3Bvc2FibGVzIiwiQ29tcG9zaXRlRGlzcG9zYWJsZSIsImNsaWVudCIsImdldENsaWVudCIsInR5cGUiLCJ0aGVtZSIsIm5hbWUiLCJvblNldHRpbmdzVmlldyIsImxhdGVzdFZlcnNpb24iLCJ2ZXJzaW9uIiwibmV3VmVyc2lvbiIsImFwbUluc3RhbGxTb3VyY2UiLCJzaGEiLCJsYXRlc3RTaGEiLCJuZXdTaGEiLCJzdGF0cyIsImRvd25sb2FkcyIsImV0Y2giLCJpbml0aWFsaXplIiwiZGlzcGxheVN0YXRzIiwiaGFuZGxlUGFja2FnZUV2ZW50cyIsImhhbmRsZUJ1dHRvbkV2ZW50cyIsImxvYWRDYWNoZWRNZXRhZGF0YSIsInJlZnMiLCJzdGF0dXNJbmRpY2F0b3IiLCJyZW1vdmUiLCJlbmFibGVtZW50QnV0dG9uIiwiYXRvbSIsInBhY2thZ2VzIiwiaXNCdW5kbGVkUGFja2FnZSIsImluc3RhbGxCdXR0b25Hcm91cCIsInVuaW5zdGFsbEJ1dHRvbiIsInVwZGF0ZUJ1dHRvbkdyb3VwIiwic3R5bGUiLCJkaXNwbGF5IiwiaGFzQ29tcGF0aWJsZVZlcnNpb24iLCJ1cGRhdGVJbnRlcmZhY2VTdGF0ZSIsInJlbmRlciIsImRpc3BsYXlOYW1lIiwiZ2l0VXJsSW5mbyIsInByb2plY3QiLCJvd25lciIsIm93bmVyRnJvbVJlcG9zaXRvcnkiLCJyZXBvc2l0b3J5IiwiZGVzY3JpcHRpb24iLCJTdHJpbmciLCJsb2NhdGVDb21wYXRpYmxlUGFja2FnZVZlcnNpb24iLCJjYWxsYmFjayIsImxvYWRDb21wYXRpYmxlUGFja2FnZVZlcnNpb24iLCJlcnIiLCJjb25zb2xlIiwiZXJyb3IiLCJwYWNrYWdlVmVyc2lvbiIsInZlcnNpb25WYWx1ZSIsInRleHRDb250ZW50IiwiY2xhc3NMaXN0IiwiYWRkIiwicGFja2FnZU1lc3NhZ2UiLCJpbnN0YWxsYWJsZVBhY2siLCJpbnNlcnRBZGphY2VudFRleHQiLCJlbmdpbmVzIiwiZ2V0VmVyc2lvbiIsInNldHRpbmdzQnV0dG9uIiwiY2xpY2tIYW5kbGVyIiwiZXZlbnQiLCJzdG9wUHJvcGFnYXRpb24iLCJzaG93UGFuZWwiLCJiYWNrIiwiZWxlbWVudCIsImFkZEV2ZW50TGlzdGVuZXIiLCJEaXNwb3NhYmxlIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImluc3RhbGxCdXR0b25DbGlja0hhbmRsZXIiLCJpbnN0YWxsIiwiaW5zdGFsbEJ1dHRvbiIsInVuaW5zdGFsbEJ1dHRvbkNsaWNrSGFuZGxlciIsInVuaW5zdGFsbCIsInVwZGF0ZUJ1dHRvbkNsaWNrSGFuZGxlciIsInVwZGF0ZSIsInRoZW4iLCJvbGRWZXJzaW9uIiwic3Vic3RyIiwiZGV0YWlsIiwibm90aWZpY2F0aW9uIiwibm90aWZpY2F0aW9ucyIsImFkZFN1Y2Nlc3MiLCJkaXNtaXNzYWJsZSIsImJ1dHRvbnMiLCJ0ZXh0Iiwib25EaWRDbGljayIsInJlc3RhcnRBcHBsaWNhdGlvbiIsImRpc21pc3MiLCJ1cGRhdGVCdXR0b24iLCJwYWNrYWdlTmFtZUNsaWNrSGFuZGxlciIsInNoZWxsIiwib3BlbkV4dGVybmFsIiwicGFja2FnZU5hbWUiLCJwYWNrYWdlQXV0aG9yQ2xpY2tIYW5kbGVyIiwibG9naW5MaW5rIiwiYXZhdGFyTGluayIsImVuYWJsZW1lbnRCdXR0b25DbGlja0hhbmRsZXIiLCJwcmV2ZW50RGVmYXVsdCIsImlzRGlzYWJsZWQiLCJlbmFibGVQYWNrYWdlIiwiZGlzYWJsZVBhY2thZ2UiLCJwYWNrYWdlTWVzc2FnZUNsaWNrSGFuZGxlciIsInRhcmdldCIsImNsb3Nlc3QiLCJocmVmIiwic3RhcnRzV2l0aCIsIndvcmtzcGFjZSIsIm9wZW4iLCJkZXN0cm95IiwiZGlzcG9zZSIsImF2YXRhciIsImF2YXRhclBhdGgiLCJzcmMiLCJwYWNrYWdlIiwiZGF0YSIsImRvd25sb2FkSWNvbiIsImRvd25sb2FkQ291bnQiLCJzdGFyZ2F6ZXJDb3VudCIsInN0YXJnYXplcnNfY291bnQiLCJ0b0xvY2FsZVN0cmluZyIsInVwZGF0ZVNldHRpbmdzU3RhdGUiLCJ1cGRhdGVJbnN0YWxsZWRTdGF0ZSIsInVwZGF0ZURpc2FibGVkU3RhdGUiLCJoYXNTZXR0aW5ncyIsImRpc3BsYXlEaXNhYmxlZFN0YXRlIiwiY29udGFpbnMiLCJkaXNwbGF5RW5hYmxlZFN0YXRlIiwicXVlcnlTZWxlY3RvciIsImRpc2FibGVkIiwiaXNJbnN0YWxsZWQiLCJkaXNwbGF5SW5zdGFsbGVkU3RhdGUiLCJkaXNwbGF5Tm90SW5zdGFsbGVkU3RhdGUiLCJwYWNrYWdlQWN0aW9uQnV0dG9uR3JvdXAiLCJhdG9tVmVyc2lvbiIsIm5vcm1hbGl6ZVZlcnNpb24iLCJzYXRpc2ZpZXNWZXJzaW9uIiwic2V0Tm90SW5zdGFsbGVkU3RhdGVCdXR0b25zIiwicGFja2FnZURvd25sb2FkcyIsInN0YXJzIiwicGFja2FnZVN0YXJzIiwiZGlzcGxheUdpdFBhY2thZ2VJbnN0YWxsSW5mb3JtYXRpb24iLCJtZXRhVXNlckNvbnRhaW5lciIsInN0YXRzQ29udGFpbmVyIiwiZGVmYXVsdCIsInBhY2thZ2VEZXNjcmlwdGlvbiIsImh0dHBzIiwidG9TdHJpbmciLCJkaXNwbGF5QXZhaWxhYmxlVXBkYXRlIiwib25EaWREZWFjdGl2YXRlUGFja2FnZSIsIm9uRGlkQWN0aXZhdGVQYWNrYWdlIiwiY29uZmlnIiwib25EaWRDaGFuZ2UiLCJzdWJzY3JpYmVUb1BhY2thZ2VFdmVudCIsImxvYWRlZFBhY2siLCJnZXRMb2FkZWRQYWNrYWdlIiwibWV0YWRhdGEiLCJpc1BhY2thZ2VJbnN0YWxsZWQiLCJpc1BhY2thZ2VEaXNhYmxlZCIsInBhY2thZ2VIYXNTZXR0aW5ncyIsIm9uIiwic3RhY2siLCJzdGRlcnIiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsImFzc2VydCIsImFzc2VydGlvbkVycm9yIiwiZXJyb3JNZXNzYWdlIiwibWVzc2FnZSIsImVycm9yU3RhY2siLCJlcnJvclN0ZGVyciJdLCJzb3VyY2VzIjpbInBhY2thZ2UtY2FyZC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKiogQGJhYmVsICovXG4vKiogQGpzeCBldGNoLmRvbSAqL1xuXG5pbXBvcnQge0NvbXBvc2l0ZURpc3Bvc2FibGUsIERpc3Bvc2FibGV9IGZyb20gJ2F0b20nXG5pbXBvcnQge3NoZWxsfSBmcm9tICdlbGVjdHJvbidcbmltcG9ydCBldGNoIGZyb20gJ2V0Y2gnXG5cbmltcG9ydCB7b3duZXJGcm9tUmVwb3NpdG9yeX0gZnJvbSAnLi91dGlscydcblxubGV0IG1hcmtlZCA9IG51bGxcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGFja2FnZUNhcmQge1xuICBjb25zdHJ1Y3RvciAocGFjaywgc2V0dGluZ3NWaWV3LCBwYWNrYWdlTWFuYWdlciwgb3B0aW9ucyA9IHt9KSB7XG4gICAgdGhpcy5wYWNrID0gcGFja1xuICAgIHRoaXMuc2V0dGluZ3NWaWV3ID0gc2V0dGluZ3NWaWV3XG4gICAgdGhpcy5wYWNrYWdlTWFuYWdlciA9IHBhY2thZ2VNYW5hZ2VyXG4gICAgdGhpcy5kaXNwb3NhYmxlcyA9IG5ldyBDb21wb3NpdGVEaXNwb3NhYmxlKClcblxuICAgIC8vIEl0IG1pZ2h0IGJlIHVzZWZ1bCB0byBlaXRoZXIgd3JhcCB0aGlzLnBhY2sgaW4gYSBjbGFzcyB0aGF0IGhhcyBhXG4gICAgLy8gOjp2YWxpZGF0ZSBtZXRob2QsIG9yIGFkZCBhIG1ldGhvZCBoZXJlLiBBdCB0aGUgbW9tZW50IEkgdGhpbmsgYWxsIGNhc2VzXG4gICAgLy8gb2YgbWFsZm9ybWVkIHBhY2thZ2UgbWV0YWRhdGEgYXJlIGhhbmRsZWQgaGVyZSBhbmQgaW4gOjpjb250ZW50IGJ1dCBiZWx0XG4gICAgLy8gYW5kIHN1c3BlbmRlcnMsIHlvdSBrbm93XG4gICAgdGhpcy5jbGllbnQgPSB0aGlzLnBhY2thZ2VNYW5hZ2VyLmdldENsaWVudCgpXG4gICAgdGhpcy50eXBlID0gdGhpcy5wYWNrLnRoZW1lID8gJ3RoZW1lJyA6ICdwYWNrYWdlJ1xuICAgIHRoaXMubmFtZSA9IHRoaXMucGFjay5uYW1lXG4gICAgdGhpcy5vblNldHRpbmdzVmlldyA9IG9wdGlvbnMub25TZXR0aW5nc1ZpZXdcblxuICAgIGlmICh0aGlzLnBhY2subGF0ZXN0VmVyc2lvbiAhPT0gdGhpcy5wYWNrLnZlcnNpb24pIHtcbiAgICAgIHRoaXMubmV3VmVyc2lvbiA9IHRoaXMucGFjay5sYXRlc3RWZXJzaW9uXG4gICAgfVxuXG4gICAgaWYgKHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlICYmIHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlLnR5cGUgPT09ICdnaXQnKSB7XG4gICAgICBpZiAodGhpcy5wYWNrLmFwbUluc3RhbGxTb3VyY2Uuc2hhICE9PSB0aGlzLnBhY2subGF0ZXN0U2hhKSB7XG4gICAgICAgIHRoaXMubmV3U2hhID0gdGhpcy5wYWNrLmxhdGVzdFNoYVxuICAgICAgfVxuICAgIH1cblxuICAgIC8vIERlZmF1bHQgdG8gZGlzcGxheWluZyB0aGUgZG93bmxvYWQgY291bnRcbiAgICBpZiAoIW9wdGlvbnMuc3RhdHMpIHtcbiAgICAgIG9wdGlvbnMuc3RhdHMgPSB7ZG93bmxvYWRzOiB0cnVlfVxuICAgIH1cblxuICAgIGV0Y2guaW5pdGlhbGl6ZSh0aGlzKVxuXG4gICAgdGhpcy5kaXNwbGF5U3RhdHMob3B0aW9ucylcbiAgICB0aGlzLmhhbmRsZVBhY2thZ2VFdmVudHMoKVxuICAgIHRoaXMuaGFuZGxlQnV0dG9uRXZlbnRzKG9wdGlvbnMpXG4gICAgdGhpcy5sb2FkQ2FjaGVkTWV0YWRhdGEoKVxuXG4gICAgLy8gdGhlbWVzIGhhdmUgbm8gc3RhdHVzIGFuZCBjYW5ub3QgYmUgZGlzL2VuYWJsZWRcbiAgICBpZiAodGhpcy50eXBlID09PSAndGhlbWUnKSB7XG4gICAgICB0aGlzLnJlZnMuc3RhdHVzSW5kaWNhdG9yLnJlbW92ZSgpXG4gICAgICB0aGlzLnJlZnMuZW5hYmxlbWVudEJ1dHRvbi5yZW1vdmUoKVxuICAgIH1cblxuICAgIGlmIChhdG9tLnBhY2thZ2VzLmlzQnVuZGxlZFBhY2thZ2UodGhpcy5wYWNrLm5hbWUpKSB7XG4gICAgICB0aGlzLnJlZnMuaW5zdGFsbEJ1dHRvbkdyb3VwLnJlbW92ZSgpXG4gICAgICB0aGlzLnJlZnMudW5pbnN0YWxsQnV0dG9uLnJlbW92ZSgpXG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLm5ld1ZlcnNpb24gJiYgIXRoaXMubmV3U2hhKSB7XG4gICAgICB0aGlzLnJlZnMudXBkYXRlQnV0dG9uR3JvdXAuc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgIH1cblxuICAgIHRoaXMuaGFzQ29tcGF0aWJsZVZlcnNpb24gPSB0cnVlXG4gICAgdGhpcy51cGRhdGVJbnRlcmZhY2VTdGF0ZSgpXG4gIH1cblxuICByZW5kZXIgKCkge1xuICAgIGNvbnN0IGRpc3BsYXlOYW1lID0gKHRoaXMucGFjay5naXRVcmxJbmZvID8gdGhpcy5wYWNrLmdpdFVybEluZm8ucHJvamVjdCA6IHRoaXMucGFjay5uYW1lKSB8fCAnJ1xuICAgIGNvbnN0IG93bmVyID0gb3duZXJGcm9tUmVwb3NpdG9yeSh0aGlzLnBhY2sucmVwb3NpdG9yeSlcbiAgICBjb25zdCBkZXNjcmlwdGlvbiA9IHRoaXMucGFjay5kZXNjcmlwdGlvbiB8fCAnJ1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPSdwYWNrYWdlLWNhcmQgY29sLWxnLTgnPlxuICAgICAgICA8ZGl2IHJlZj0nc3RhdHNDb250YWluZXInIGNsYXNzTmFtZT0nc3RhdHMgcHVsbC1yaWdodCc+XG4gICAgICAgICAgPHNwYW4gcmVmPSdwYWNrYWdlU3RhcnMnIGNsYXNzTmFtZT0nc3RhdHMtaXRlbSc+XG4gICAgICAgICAgICA8c3BhbiByZWY9J3N0YXJnYXplckljb24nIGNsYXNzTmFtZT0naWNvbiBpY29uLXN0YXInIC8+XG4gICAgICAgICAgICA8c3BhbiByZWY9J3N0YXJnYXplckNvdW50JyBjbGFzc05hbWU9J3ZhbHVlJyAvPlxuICAgICAgICAgIDwvc3Bhbj5cblxuICAgICAgICAgIDxzcGFuIHJlZj0ncGFja2FnZURvd25sb2FkcycgY2xhc3NOYW1lPSdzdGF0cy1pdGVtJz5cbiAgICAgICAgICAgIDxzcGFuIHJlZj0nZG93bmxvYWRJY29uJyBjbGFzc05hbWU9J2ljb24gaWNvbi1jbG91ZC1kb3dubG9hZCcgLz5cbiAgICAgICAgICAgIDxzcGFuIHJlZj0nZG93bmxvYWRDb3VudCcgY2xhc3NOYW1lPSd2YWx1ZScgLz5cbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdib2R5Jz5cbiAgICAgICAgICA8aDQgY2xhc3NOYW1lPSdjYXJkLW5hbWUnPlxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPSdwYWNrYWdlLW5hbWUnIHJlZj0ncGFja2FnZU5hbWUnPntkaXNwbGF5TmFtZX08L2E+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J3BhY2thZ2UtdmVyc2lvbic+XG4gICAgICAgICAgICAgIDxzcGFuIHJlZj0ndmVyc2lvblZhbHVlJyBjbGFzc05hbWU9J3ZhbHVlJz57U3RyaW5nKHRoaXMucGFjay52ZXJzaW9uKX08L3NwYW4+XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPC9oND5cbiAgICAgICAgICA8c3BhbiByZWY9J3BhY2thZ2VEZXNjcmlwdGlvbicgY2xhc3NOYW1lPSdwYWNrYWdlLWRlc2NyaXB0aW9uJz57ZGVzY3JpcHRpb259PC9zcGFuPlxuICAgICAgICAgIDxkaXYgcmVmPSdwYWNrYWdlTWVzc2FnZScgY2xhc3NOYW1lPSdwYWNrYWdlLW1lc3NhZ2UnIC8+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPSdtZXRhJz5cbiAgICAgICAgICA8ZGl2IHJlZj0nbWV0YVVzZXJDb250YWluZXInIGNsYXNzTmFtZT0nbWV0YS11c2VyJz5cbiAgICAgICAgICAgIDxhIHJlZj0nYXZhdGFyTGluayc+XG4gICAgICAgICAgICAgIHsvKiBBIHRyYW5zcGFyZW50IGdpZiBzbyB0aGVyZSBpcyBubyBcImJyb2tlbiBib3JkZXJcIiAqL31cbiAgICAgICAgICAgICAgPGltZyByZWY9J2F2YXRhcicgY2xhc3NOYW1lPSdhdmF0YXInIHNyYz0nZGF0YTppbWFnZS9naWY7YmFzZTY0LFIwbEdPRGxoQVFBQkFJQUFBQUFBQVAvLy95SDVCQUVBQUFBQUxBQUFBQUFCQUFFQUFBSUJSQUE3JyAvPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPGEgcmVmPSdsb2dpbkxpbmsnIGNsYXNzTmFtZT0nYXV0aG9yJz57b3duZXJ9PC9hPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdtZXRhLWNvbnRyb2xzJz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdidG4tdG9vbGJhcic+XG4gICAgICAgICAgICAgIDxkaXYgcmVmPSd1cGRhdGVCdXR0b25Hcm91cCcgY2xhc3NOYW1lPSdidG4tZ3JvdXAnPlxuICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT0nYnV0dG9uJyBjbGFzc05hbWU9J2J0biBidG4taW5mbyBpY29uIGljb24tY2xvdWQtZG93bmxvYWQgaW5zdGFsbC1idXR0b24nIHJlZj0ndXBkYXRlQnV0dG9uJz5VcGRhdGU8L2J1dHRvbj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgcmVmPSdpbnN0YWxsQnV0dG9uR3JvdXAnIGNsYXNzTmFtZT0nYnRuLWdyb3VwJz5cbiAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9J2J1dHRvbicgY2xhc3NOYW1lPSdidG4gYnRuLWluZm8gaWNvbiBpY29uLWNsb3VkLWRvd25sb2FkIGluc3RhbGwtYnV0dG9uJyByZWY9J2luc3RhbGxCdXR0b24nPkluc3RhbGw8L2J1dHRvbj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgcmVmPSdwYWNrYWdlQWN0aW9uQnV0dG9uR3JvdXAnIGNsYXNzTmFtZT0nYnRuLWdyb3VwJz5cbiAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9J2J1dHRvbicgY2xhc3NOYW1lPSdidG4gaWNvbiBpY29uLWdlYXIgc2V0dGluZ3MnIHJlZj0nc2V0dGluZ3NCdXR0b24nPlNldHRpbmdzPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPSdidXR0b24nIGNsYXNzTmFtZT0nYnRuIGljb24gaWNvbi10cmFzaGNhbiB1bmluc3RhbGwtYnV0dG9uJyByZWY9J3VuaW5zdGFsbEJ1dHRvbic+VW5pbnN0YWxsPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPSdidXR0b24nIGNsYXNzTmFtZT0nYnRuIGljb24gaWNvbi1wbGF5YmFjay1wYXVzZSBlbmFibGVtZW50JyByZWY9J2VuYWJsZW1lbnRCdXR0b24nPlxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdkaXNhYmxlLXRleHQnPkRpc2FibGU8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPSdidXR0b24nIGNsYXNzTmFtZT0nYnRuIHN0YXR1cy1pbmRpY2F0b3InIHRhYkluZGV4PSctMScgcmVmPSdzdGF0dXNJbmRpY2F0b3InIC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKVxuICB9XG5cbiAgbG9jYXRlQ29tcGF0aWJsZVBhY2thZ2VWZXJzaW9uIChjYWxsYmFjaykge1xuICAgIHRoaXMucGFja2FnZU1hbmFnZXIubG9hZENvbXBhdGlibGVQYWNrYWdlVmVyc2lvbih0aGlzLnBhY2submFtZSwgKGVyciwgcGFjaykgPT4ge1xuICAgICAgaWYgKGVyciAhPSBudWxsKSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKVxuICAgICAgfVxuXG4gICAgICBjb25zdCBwYWNrYWdlVmVyc2lvbiA9IHBhY2sudmVyc2lvblxuXG4gICAgICAvLyBBIGNvbXBhdGlibGUgdmVyc2lvbiBleGlzdCwgd2UgYWN0aXZhdGUgdGhlIGluc3RhbGwgYnV0dG9uIGFuZFxuICAgICAgLy8gc2V0IHRoaXMuaW5zdGFsbGFibGVQYWNrIHNvIHRoYXQgdGhlIGluc3RhbGwgYWN0aW9uIGluc3RhbGxzIHRoZVxuICAgICAgLy8gY29tcGF0aWJsZSB2ZXJzaW9uIG9mIHRoZSBwYWNrYWdlLlxuICAgICAgaWYgKHBhY2thZ2VWZXJzaW9uKSB7XG4gICAgICAgIHRoaXMucmVmcy52ZXJzaW9uVmFsdWUudGV4dENvbnRlbnQgPSBwYWNrYWdlVmVyc2lvblxuICAgICAgICBpZiAocGFja2FnZVZlcnNpb24gIT09IHRoaXMucGFjay52ZXJzaW9uKSB7XG4gICAgICAgICAgdGhpcy5yZWZzLnZlcnNpb25WYWx1ZS5jbGFzc0xpc3QuYWRkKCd0ZXh0LXdhcm5pbmcnKVxuICAgICAgICAgIHRoaXMucmVmcy5wYWNrYWdlTWVzc2FnZS5jbGFzc0xpc3QuYWRkKCd0ZXh0LXdhcm5pbmcnKVxuICAgICAgICAgIHRoaXMucmVmcy5wYWNrYWdlTWVzc2FnZS50ZXh0Q29udGVudCA9IGBWZXJzaW9uICR7cGFja2FnZVZlcnNpb259IGlzIG5vdCB0aGUgbGF0ZXN0IHZlcnNpb24gYXZhaWxhYmxlIGZvciB0aGlzIHBhY2thZ2UsIGJ1dCBpdCdzIHRoZSBsYXRlc3QgdGhhdCBpcyBjb21wYXRpYmxlIHdpdGggeW91ciB2ZXJzaW9uIG9mIEF0b20uYFxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5pbnN0YWxsYWJsZVBhY2sgPSBwYWNrXG4gICAgICAgIHRoaXMuaGFzQ29tcGF0aWJsZVZlcnNpb24gPSB0cnVlXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmhhc0NvbXBhdGlibGVWZXJzaW9uID0gZmFsc2VcbiAgICAgICAgdGhpcy5yZWZzLnZlcnNpb25WYWx1ZS5jbGFzc0xpc3QuYWRkKCd0ZXh0LWVycm9yJylcbiAgICAgICAgdGhpcy5yZWZzLnBhY2thZ2VNZXNzYWdlLmNsYXNzTGlzdC5hZGQoJ3RleHQtZXJyb3InKVxuICAgICAgICB0aGlzLnJlZnMucGFja2FnZU1lc3NhZ2UuaW5zZXJ0QWRqYWNlbnRUZXh0KFxuICAgICAgICAgICdiZWZvcmVlbmQnLFxuICAgICAgICAgIGBUaGVyZSdzIG5vIHZlcnNpb24gb2YgdGhpcyBwYWNrYWdlIHRoYXQgaXMgY29tcGF0aWJsZSB3aXRoIHlvdXIgQXRvbSB2ZXJzaW9uLiBUaGUgdmVyc2lvbiBtdXN0IHNhdGlzZnkgJHt0aGlzLnBhY2suZW5naW5lcy5hdG9tfS5gXG4gICAgICAgIClcbiAgICAgICAgY29uc29sZS5lcnJvcihgTm8gYXZhaWxhYmxlIHZlcnNpb24gY29tcGF0aWJsZSB3aXRoIHRoZSBpbnN0YWxsZWQgQXRvbSB2ZXJzaW9uOiAke2F0b20uZ2V0VmVyc2lvbigpfWApXG4gICAgICB9XG5cbiAgICAgIGNhbGxiYWNrKClcbiAgICB9KVxuICB9XG5cbiAgaGFuZGxlQnV0dG9uRXZlbnRzIChvcHRpb25zKSB7XG4gICAgaWYgKG9wdGlvbnMgJiYgb3B0aW9ucy5vblNldHRpbmdzVmlldykge1xuICAgICAgdGhpcy5yZWZzLnNldHRpbmdzQnV0dG9uLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc3QgY2xpY2tIYW5kbGVyID0gKGV2ZW50KSA9PiB7XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICAgIHRoaXMuc2V0dGluZ3NWaWV3LnNob3dQYW5lbCh0aGlzLnBhY2submFtZSwge2JhY2s6IG9wdGlvbnMgPyBvcHRpb25zLmJhY2sgOiBudWxsLCBwYWNrOiB0aGlzLnBhY2t9KVxuICAgICAgfVxuXG4gICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBjbGlja0hhbmRsZXIpXG4gICAgICB0aGlzLmRpc3Bvc2FibGVzLmFkZChuZXcgRGlzcG9zYWJsZSgoKSA9PiB7IHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIGNsaWNrSGFuZGxlcikgfSkpXG5cbiAgICAgIHRoaXMucmVmcy5zZXR0aW5nc0J1dHRvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGNsaWNrSGFuZGxlcilcbiAgICAgIHRoaXMuZGlzcG9zYWJsZXMuYWRkKG5ldyBEaXNwb3NhYmxlKCgpID0+IHsgdGhpcy5yZWZzLnNldHRpbmdzQnV0dG9uLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2xpY2tIYW5kbGVyKSB9KSlcbiAgICB9XG5cbiAgICBjb25zdCBpbnN0YWxsQnV0dG9uQ2xpY2tIYW5kbGVyID0gKGV2ZW50KSA9PiB7XG4gICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgdGhpcy5pbnN0YWxsKClcbiAgICB9XG4gICAgdGhpcy5yZWZzLmluc3RhbGxCdXR0b24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBpbnN0YWxsQnV0dG9uQ2xpY2tIYW5kbGVyKVxuICAgIHRoaXMuZGlzcG9zYWJsZXMuYWRkKG5ldyBEaXNwb3NhYmxlKCgpID0+IHsgdGhpcy5yZWZzLmluc3RhbGxCdXR0b24ucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLCBpbnN0YWxsQnV0dG9uQ2xpY2tIYW5kbGVyKSB9KSlcblxuICAgIGNvbnN0IHVuaW5zdGFsbEJ1dHRvbkNsaWNrSGFuZGxlciA9IChldmVudCkgPT4ge1xuICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcbiAgICAgIHRoaXMudW5pbnN0YWxsKClcbiAgICB9XG4gICAgdGhpcy5yZWZzLnVuaW5zdGFsbEJ1dHRvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHVuaW5zdGFsbEJ1dHRvbkNsaWNrSGFuZGxlcilcbiAgICB0aGlzLmRpc3Bvc2FibGVzLmFkZChuZXcgRGlzcG9zYWJsZSgoKSA9PiB7IHRoaXMucmVmcy51bmluc3RhbGxCdXR0b24ucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLCB1bmluc3RhbGxCdXR0b25DbGlja0hhbmRsZXIpIH0pKVxuXG4gICAgY29uc3QgdXBkYXRlQnV0dG9uQ2xpY2tIYW5kbGVyID0gKGV2ZW50KSA9PiB7XG4gICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgdGhpcy51cGRhdGUoKS50aGVuKCgpID0+IHtcbiAgICAgICAgbGV0IG9sZFZlcnNpb24gPSAnJ1xuICAgICAgICBsZXQgbmV3VmVyc2lvbiA9ICcnXG5cbiAgICAgICAgaWYgKHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlICYmIHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlLnR5cGUgPT09ICdnaXQnKSB7XG4gICAgICAgICAgb2xkVmVyc2lvbiA9IHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlLnNoYS5zdWJzdHIoMCwgOClcbiAgICAgICAgICBuZXdWZXJzaW9uID0gYCR7dGhpcy5wYWNrLmxhdGVzdFNoYS5zdWJzdHIoMCwgOCl9YFxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucGFjay52ZXJzaW9uICYmIHRoaXMucGFjay5sYXRlc3RWZXJzaW9uKSB7XG4gICAgICAgICAgb2xkVmVyc2lvbiA9IHRoaXMucGFjay52ZXJzaW9uXG4gICAgICAgICAgbmV3VmVyc2lvbiA9IHRoaXMucGFjay5sYXRlc3RWZXJzaW9uXG4gICAgICAgIH1cblxuICAgICAgICBsZXQgZGV0YWlsID0gJydcbiAgICAgICAgaWYgKG9sZFZlcnNpb24gJiYgbmV3VmVyc2lvbikge1xuICAgICAgICAgIGRldGFpbCA9IGAke29sZFZlcnNpb259IC0+ICR7bmV3VmVyc2lvbn1gXG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBub3RpZmljYXRpb24gPSBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkU3VjY2VzcyhgUmVzdGFydCBBdG9tIHRvIGNvbXBsZXRlIHRoZSB1cGRhdGUgb2YgXFxgJHt0aGlzLnBhY2submFtZX1cXGAuYCwge1xuICAgICAgICAgIGRpc21pc3NhYmxlOiB0cnVlLFxuICAgICAgICAgIGJ1dHRvbnM6IFt7XG4gICAgICAgICAgICB0ZXh0OiAnUmVzdGFydCBub3cnLFxuICAgICAgICAgICAgb25EaWRDbGljayAoKSB7IHJldHVybiBhdG9tLnJlc3RhcnRBcHBsaWNhdGlvbigpIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHRleHQ6ICdJXFwnbGwgZG8gaXQgbGF0ZXInLFxuICAgICAgICAgICAgb25EaWRDbGljayAoKSB7IG5vdGlmaWNhdGlvbi5kaXNtaXNzKCkgfVxuICAgICAgICAgIH1dLFxuICAgICAgICAgIGRldGFpbFxuICAgICAgICB9KVxuICAgICAgfSlcbiAgICB9XG4gICAgdGhpcy5yZWZzLnVwZGF0ZUJ1dHRvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHVwZGF0ZUJ1dHRvbkNsaWNrSGFuZGxlcilcbiAgICB0aGlzLmRpc3Bvc2FibGVzLmFkZChuZXcgRGlzcG9zYWJsZSgoKSA9PiB7IHRoaXMucmVmcy51cGRhdGVCdXR0b24ucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLCB1cGRhdGVCdXR0b25DbGlja0hhbmRsZXIpIH0pKVxuXG4gICAgY29uc3QgcGFja2FnZU5hbWVDbGlja0hhbmRsZXIgPSAoZXZlbnQpID0+IHtcbiAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXG4gICAgICBzaGVsbC5vcGVuRXh0ZXJuYWwoYGh0dHBzOi8vd2ViLnB1bHNhci1lZGl0LmRldi9wYWNrYWdlcy8ke3RoaXMucGFjay5uYW1lfWApXG4gICAgfVxuICAgIHRoaXMucmVmcy5wYWNrYWdlTmFtZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHBhY2thZ2VOYW1lQ2xpY2tIYW5kbGVyKVxuICAgIHRoaXMuZGlzcG9zYWJsZXMuYWRkKG5ldyBEaXNwb3NhYmxlKCgpID0+IHsgdGhpcy5yZWZzLnBhY2thZ2VOYW1lLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgcGFja2FnZU5hbWVDbGlja0hhbmRsZXIpIH0pKVxuXG4gICAgY29uc3QgcGFja2FnZUF1dGhvckNsaWNrSGFuZGxlciA9IChldmVudCkgPT4ge1xuICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcbiAgICAgIHNoZWxsLm9wZW5FeHRlcm5hbChgaHR0cHM6Ly9wdWxzYXItZWRpdC5kZXYvdXNlcnMvJHtvd25lckZyb21SZXBvc2l0b3J5KHRoaXMucGFjay5yZXBvc2l0b3J5KX1gKVxuICAgIH1cbiAgICB0aGlzLnJlZnMubG9naW5MaW5rLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgcGFja2FnZUF1dGhvckNsaWNrSGFuZGxlcilcbiAgICB0aGlzLmRpc3Bvc2FibGVzLmFkZChuZXcgRGlzcG9zYWJsZSgoKSA9PiB7IHRoaXMucmVmcy5sb2dpbkxpbmsucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLCBwYWNrYWdlQXV0aG9yQ2xpY2tIYW5kbGVyKSB9KSlcbiAgICB0aGlzLnJlZnMuYXZhdGFyTGluay5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHBhY2thZ2VBdXRob3JDbGlja0hhbmRsZXIpXG4gICAgdGhpcy5kaXNwb3NhYmxlcy5hZGQobmV3IERpc3Bvc2FibGUoKCkgPT4geyB0aGlzLnJlZnMuYXZhdGFyTGluay5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIHBhY2thZ2VBdXRob3JDbGlja0hhbmRsZXIpIH0pKVxuXG4gICAgY29uc3QgZW5hYmxlbWVudEJ1dHRvbkNsaWNrSGFuZGxlciA9IChldmVudCkgPT4ge1xuICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgIGlmICh0aGlzLmlzRGlzYWJsZWQoKSkge1xuICAgICAgICBhdG9tLnBhY2thZ2VzLmVuYWJsZVBhY2thZ2UodGhpcy5wYWNrLm5hbWUpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBhdG9tLnBhY2thZ2VzLmRpc2FibGVQYWNrYWdlKHRoaXMucGFjay5uYW1lKVxuICAgICAgfVxuICAgIH1cbiAgICB0aGlzLnJlZnMuZW5hYmxlbWVudEJ1dHRvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGVuYWJsZW1lbnRCdXR0b25DbGlja0hhbmRsZXIpXG4gICAgdGhpcy5kaXNwb3NhYmxlcy5hZGQobmV3IERpc3Bvc2FibGUoKCkgPT4geyB0aGlzLnJlZnMuZW5hYmxlbWVudEJ1dHRvbi5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIGVuYWJsZW1lbnRCdXR0b25DbGlja0hhbmRsZXIpIH0pKVxuXG4gICAgY29uc3QgcGFja2FnZU1lc3NhZ2VDbGlja0hhbmRsZXIgPSAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IHRhcmdldCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCdhJylcbiAgICAgIGlmICh0YXJnZXQpIHtcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICBpZiAodGFyZ2V0LmhyZWYgJiYgdGFyZ2V0LmhyZWYuc3RhcnRzV2l0aCgnYXRvbTonKSkge1xuICAgICAgICAgIGF0b20ud29ya3NwYWNlLm9wZW4odGFyZ2V0LmhyZWYpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5yZWZzLnBhY2thZ2VNZXNzYWdlLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgcGFja2FnZU1lc3NhZ2VDbGlja0hhbmRsZXIpXG4gICAgdGhpcy5kaXNwb3NhYmxlcy5hZGQobmV3IERpc3Bvc2FibGUoKCkgPT4geyB0aGlzLnJlZnMucGFja2FnZU1lc3NhZ2UucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLCBwYWNrYWdlTWVzc2FnZUNsaWNrSGFuZGxlcikgfSkpXG4gIH1cblxuICBkZXN0cm95ICgpIHtcbiAgICB0aGlzLmRpc3Bvc2FibGVzLmRpc3Bvc2UoKVxuICAgIHJldHVybiBldGNoLmRlc3Ryb3kodGhpcylcbiAgfVxuXG4gIGxvYWRDYWNoZWRNZXRhZGF0YSAoKSB7XG4gICAgdGhpcy5jbGllbnQuYXZhdGFyKG93bmVyRnJvbVJlcG9zaXRvcnkodGhpcy5wYWNrLnJlcG9zaXRvcnkpLCAoZXJyLCBhdmF0YXJQYXRoKSA9PiB7XG4gICAgICBpZiAoIWVyciAmJiBhdmF0YXJQYXRoKSB7XG4gICAgICAgIHRoaXMucmVmcy5hdmF0YXIuc3JjID0gYGZpbGU6Ly8ke2F2YXRhclBhdGh9YFxuICAgICAgfVxuICAgIH0pXG5cbiAgICB0aGlzLmNsaWVudC5wYWNrYWdlKHRoaXMucGFjay5uYW1lLCAoZXJyLCBkYXRhKSA9PiB7XG4gICAgICAvLyBXZSBkb24ndCBuZWVkIHRvIGFjdHVhbGx5IGhhbmRsZSB0aGUgZXJyb3IgaGVyZSwgd2UgY2FuIGp1c3Qgc2tpcFxuICAgICAgLy8gc2hvd2luZyB0aGUgZG93bmxvYWQgY291bnQgaWYgdGhlcmUncyBhIHByb2JsZW0uXG4gICAgICBpZiAoIWVycikge1xuICAgICAgICBpZiAoZGF0YSA9PSBudWxsKSB7XG4gICAgICAgICAgZGF0YSA9IHt9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5wYWNrLmFwbUluc3RhbGxTb3VyY2UgJiYgdGhpcy5wYWNrLmFwbUluc3RhbGxTb3VyY2UudHlwZSA9PT0gJ2dpdCcpIHtcbiAgICAgICAgICB0aGlzLnJlZnMuZG93bmxvYWRJY29uLmNsYXNzTGlzdC5yZW1vdmUoJ2ljb24tY2xvdWQtZG93bmxvYWQnKVxuICAgICAgICAgIHRoaXMucmVmcy5kb3dubG9hZEljb24uY2xhc3NMaXN0LmFkZCgnaWNvbi1naXQtYnJhbmNoJylcbiAgICAgICAgICB0aGlzLnJlZnMuZG93bmxvYWRDb3VudC50ZXh0Q29udGVudCA9IHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlLnNoYS5zdWJzdHIoMCwgOClcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnJlZnMuc3RhcmdhemVyQ291bnQudGV4dENvbnRlbnQgPSBkYXRhLnN0YXJnYXplcnNfY291bnQgPyBkYXRhLnN0YXJnYXplcnNfY291bnQudG9Mb2NhbGVTdHJpbmcoKSA6ICcnXG4gICAgICAgICAgdGhpcy5yZWZzLmRvd25sb2FkQ291bnQudGV4dENvbnRlbnQgPSBkYXRhLmRvd25sb2FkcyA/IGRhdGEuZG93bmxvYWRzLnRvTG9jYWxlU3RyaW5nKCkgOiAnJ1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG4gIHVwZGF0ZUludGVyZmFjZVN0YXRlICgpIHtcbiAgICB0aGlzLnJlZnMudmVyc2lvblZhbHVlLnRleHRDb250ZW50ID0gKHRoaXMuaW5zdGFsbGFibGVQYWNrID8gdGhpcy5pbnN0YWxsYWJsZVBhY2sudmVyc2lvbiA6IG51bGwpIHx8IHRoaXMucGFjay52ZXJzaW9uXG4gICAgaWYgKHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlICYmIHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlLnR5cGUgPT09ICdnaXQnKSB7XG4gICAgICB0aGlzLnJlZnMuZG93bmxvYWRDb3VudC50ZXh0Q29udGVudCA9IHRoaXMucGFjay5hcG1JbnN0YWxsU291cmNlLnNoYS5zdWJzdHIoMCwgOClcbiAgICB9XG5cbiAgICB0aGlzLnVwZGF0ZVNldHRpbmdzU3RhdGUoKVxuICAgIHRoaXMudXBkYXRlSW5zdGFsbGVkU3RhdGUoKVxuICAgIHRoaXMudXBkYXRlRGlzYWJsZWRTdGF0ZSgpXG4gIH1cblxuICB1cGRhdGVTZXR0aW5nc1N0YXRlICgpIHtcbiAgICBpZiAodGhpcy5oYXNTZXR0aW5ncygpICYmICF0aGlzLm9uU2V0dGluZ3NWaWV3KSB7XG4gICAgICB0aGlzLnJlZnMuc2V0dGluZ3NCdXR0b24uc3R5bGUuZGlzcGxheSA9ICcnXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMucmVmcy5zZXR0aW5nc0J1dHRvbi5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgfVxuICB9XG5cbiAgLy8gU2VjdGlvbjogZGlzYWJsZWQgc3RhdGUgdXBkYXRlc1xuXG4gIHVwZGF0ZURpc2FibGVkU3RhdGUgKCkge1xuICAgIGlmICh0aGlzLmlzRGlzYWJsZWQoKSkge1xuICAgICAgdGhpcy5kaXNwbGF5RGlzYWJsZWRTdGF0ZSgpXG4gICAgfSBlbHNlIGlmICh0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXNhYmxlZCcpKSB7XG4gICAgICB0aGlzLmRpc3BsYXlFbmFibGVkU3RhdGUoKVxuICAgIH1cbiAgfVxuXG4gIGRpc3BsYXlFbmFibGVkU3RhdGUgKCkge1xuICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKCdkaXNhYmxlZCcpXG4gICAgaWYgKHRoaXMudHlwZSA9PT0gJ3RoZW1lJykge1xuICAgICAgdGhpcy5yZWZzLmVuYWJsZW1lbnRCdXR0b24uc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgIH1cbiAgICB0aGlzLnJlZnMuZW5hYmxlbWVudEJ1dHRvbi5xdWVyeVNlbGVjdG9yKCcuZGlzYWJsZS10ZXh0JykudGV4dENvbnRlbnQgPSAnRGlzYWJsZSdcbiAgICB0aGlzLnJlZnMuZW5hYmxlbWVudEJ1dHRvbi5jbGFzc0xpc3QuYWRkKCdpY29uLXBsYXliYWNrLXBhdXNlJylcbiAgICB0aGlzLnJlZnMuZW5hYmxlbWVudEJ1dHRvbi5jbGFzc0xpc3QucmVtb3ZlKCdpY29uLXBsYXliYWNrLXBsYXknKVxuICAgIHRoaXMucmVmcy5zdGF0dXNJbmRpY2F0b3IuY2xhc3NMaXN0LnJlbW92ZSgnaXMtZGlzYWJsZWQnKVxuICB9XG5cbiAgZGlzcGxheURpc2FibGVkU3RhdGUgKCkge1xuICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkaXNhYmxlZCcpXG4gICAgdGhpcy5yZWZzLmVuYWJsZW1lbnRCdXR0b24ucXVlcnlTZWxlY3RvcignLmRpc2FibGUtdGV4dCcpLnRleHRDb250ZW50ID0gJ0VuYWJsZSdcbiAgICB0aGlzLnJlZnMuZW5hYmxlbWVudEJ1dHRvbi5jbGFzc0xpc3QuYWRkKCdpY29uLXBsYXliYWNrLXBsYXknKVxuICAgIHRoaXMucmVmcy5lbmFibGVtZW50QnV0dG9uLmNsYXNzTGlzdC5yZW1vdmUoJ2ljb24tcGxheWJhY2stcGF1c2UnKVxuICAgIHRoaXMucmVmcy5zdGF0dXNJbmRpY2F0b3IuY2xhc3NMaXN0LmFkZCgnaXMtZGlzYWJsZWQnKVxuICAgIHRoaXMucmVmcy5lbmFibGVtZW50QnV0dG9uLmRpc2FibGVkID0gZmFsc2VcbiAgfVxuXG4gIC8vIFNlY3Rpb246IGluc3RhbGxlZCBzdGF0ZSB1cGRhdGVzXG5cbiAgdXBkYXRlSW5zdGFsbGVkU3RhdGUgKCkge1xuICAgIGlmICh0aGlzLmlzSW5zdGFsbGVkKCkpIHtcbiAgICAgIHRoaXMuZGlzcGxheUluc3RhbGxlZFN0YXRlKClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5kaXNwbGF5Tm90SW5zdGFsbGVkU3RhdGUoKVxuICAgIH1cbiAgfVxuXG4gIGRpc3BsYXlJbnN0YWxsZWRTdGF0ZSAoKSB7XG4gICAgaWYgKHRoaXMubmV3VmVyc2lvbiB8fCB0aGlzLm5ld1NoYSkge1xuICAgICAgdGhpcy5yZWZzLnVwZGF0ZUJ1dHRvbkdyb3VwLnN0eWxlLmRpc3BsYXkgPSAnJ1xuICAgICAgaWYgKHRoaXMubmV3VmVyc2lvbikge1xuICAgICAgICB0aGlzLnJlZnMudXBkYXRlQnV0dG9uLnRleHRDb250ZW50ID0gYFVwZGF0ZSB0byAke3RoaXMubmV3VmVyc2lvbn1gXG4gICAgICB9IGVsc2UgaWYgKHRoaXMubmV3U2hhKSB7XG4gICAgICAgIHRoaXMucmVmcy51cGRhdGVCdXR0b24udGV4dENvbnRlbnQgPSBgVXBkYXRlIHRvICR7dGhpcy5uZXdTaGEuc3Vic3RyKDAsIDgpfWBcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5yZWZzLnVwZGF0ZUJ1dHRvbkdyb3VwLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICB9XG5cbiAgICB0aGlzLnJlZnMuaW5zdGFsbEJ1dHRvbkdyb3VwLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICB0aGlzLnJlZnMucGFja2FnZUFjdGlvbkJ1dHRvbkdyb3VwLnN0eWxlLmRpc3BsYXkgPSAnJ1xuICAgIHRoaXMucmVmcy51bmluc3RhbGxCdXR0b24uc3R5bGUuZGlzcGxheSA9ICcnXG4gIH1cblxuICBkaXNwbGF5Tm90SW5zdGFsbGVkU3RhdGUgKCkge1xuICAgIHRoaXMucmVmcy51bmluc3RhbGxCdXR0b24uc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgIGNvbnN0IGF0b21WZXJzaW9uID0gdGhpcy5wYWNrYWdlTWFuYWdlci5ub3JtYWxpemVWZXJzaW9uKGF0b20uZ2V0VmVyc2lvbigpKVxuICAgIGlmICghdGhpcy5wYWNrYWdlTWFuYWdlci5zYXRpc2ZpZXNWZXJzaW9uKGF0b21WZXJzaW9uLCB0aGlzLnBhY2spKSB7XG4gICAgICB0aGlzLmhhc0NvbXBhdGlibGVWZXJzaW9uID0gZmFsc2VcbiAgICAgIHRoaXMuc2V0Tm90SW5zdGFsbGVkU3RhdGVCdXR0b25zKClcbiAgICAgIHRoaXMubG9jYXRlQ29tcGF0aWJsZVBhY2thZ2VWZXJzaW9uKCgpID0+IHsgdGhpcy5zZXROb3RJbnN0YWxsZWRTdGF0ZUJ1dHRvbnMoKSB9KVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldE5vdEluc3RhbGxlZFN0YXRlQnV0dG9ucygpXG4gICAgfVxuICB9XG5cbiAgc2V0Tm90SW5zdGFsbGVkU3RhdGVCdXR0b25zICgpIHtcbiAgICBpZiAoIXRoaXMuaGFzQ29tcGF0aWJsZVZlcnNpb24pIHtcbiAgICAgIHRoaXMucmVmcy5pbnN0YWxsQnV0dG9uR3JvdXAuc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgICAgdGhpcy5yZWZzLnVwZGF0ZUJ1dHRvbkdyb3VwLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICB9IGVsc2UgaWYgKHRoaXMubmV3VmVyc2lvbiB8fCB0aGlzLm5ld1NoYSkge1xuICAgICAgdGhpcy5yZWZzLnVwZGF0ZUJ1dHRvbkdyb3VwLnN0eWxlLmRpc3BsYXkgPSAnJ1xuICAgICAgdGhpcy5yZWZzLmluc3RhbGxCdXR0b25Hcm91cC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMucmVmcy51cGRhdGVCdXR0b25Hcm91cC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgICB0aGlzLnJlZnMuaW5zdGFsbEJ1dHRvbkdyb3VwLnN0eWxlLmRpc3BsYXkgPSAnJ1xuICAgIH1cbiAgICB0aGlzLnJlZnMucGFja2FnZUFjdGlvbkJ1dHRvbkdyb3VwLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgfVxuXG4gIGRpc3BsYXlTdGF0cyAob3B0aW9ucykge1xuICAgIGlmIChvcHRpb25zICYmIG9wdGlvbnMuc3RhdHMgJiYgb3B0aW9ucy5zdGF0cy5kb3dubG9hZHMpIHtcbiAgICAgIHRoaXMucmVmcy5wYWNrYWdlRG93bmxvYWRzLnN0eWxlLmRpc3BsYXkgPSAnJ1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJlZnMucGFja2FnZURvd25sb2Fkcy5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgfVxuXG4gICAgaWYgKG9wdGlvbnMgJiYgb3B0aW9ucy5zdGF0cyAmJiBvcHRpb25zLnN0YXRzLnN0YXJzKSB7XG4gICAgICB0aGlzLnJlZnMucGFja2FnZVN0YXJzLnN0eWxlLmRpc3BsYXkgPSAnJ1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJlZnMucGFja2FnZVN0YXJzLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICB9XG4gIH1cblxuICBkaXNwbGF5R2l0UGFja2FnZUluc3RhbGxJbmZvcm1hdGlvbiAoKSB7XG4gICAgdGhpcy5yZWZzLm1ldGFVc2VyQ29udGFpbmVyLnJlbW92ZSgpXG4gICAgdGhpcy5yZWZzLnN0YXRzQ29udGFpbmVyLnJlbW92ZSgpXG4gICAgY29uc3Qge2dpdFVybEluZm99ID0gdGhpcy5wYWNrXG4gICAgaWYgKGdpdFVybEluZm8uZGVmYXVsdCA9PT0gJ3Nob3J0Y3V0Jykge1xuICAgICAgdGhpcy5yZWZzLnBhY2thZ2VEZXNjcmlwdGlvbi50ZXh0Q29udGVudCA9IGdpdFVybEluZm8uaHR0cHMoKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJlZnMucGFja2FnZURlc2NyaXB0aW9uLnRleHRDb250ZW50ID0gZ2l0VXJsSW5mby50b1N0cmluZygpXG4gICAgfVxuICAgIHRoaXMucmVmcy5pbnN0YWxsQnV0dG9uLmNsYXNzTGlzdC5yZW1vdmUoJ2ljb24tY2xvdWQtZG93bmxvYWQnKVxuICAgIHRoaXMucmVmcy5pbnN0YWxsQnV0dG9uLmNsYXNzTGlzdC5hZGQoJ2ljb24tZ2l0LWNvbW1pdCcpXG4gICAgdGhpcy5yZWZzLnVwZGF0ZUJ1dHRvbi5jbGFzc0xpc3QucmVtb3ZlKCdpY29uLWNsb3VkLWRvd25sb2FkJylcbiAgICB0aGlzLnJlZnMudXBkYXRlQnV0dG9uLmNsYXNzTGlzdC5hZGQoJ2ljb24tZ2l0LWNvbW1pdCcpXG4gIH1cblxuICBkaXNwbGF5QXZhaWxhYmxlVXBkYXRlIChuZXdWZXJzaW9uKSB7XG4gICAgdGhpcy5uZXdWZXJzaW9uID0gbmV3VmVyc2lvblxuICAgIHRoaXMudXBkYXRlSW50ZXJmYWNlU3RhdGUoKVxuICB9XG5cbiAgaGFuZGxlUGFja2FnZUV2ZW50cyAoKSB7XG4gICAgdGhpcy5kaXNwb3NhYmxlcy5hZGQoYXRvbS5wYWNrYWdlcy5vbkRpZERlYWN0aXZhdGVQYWNrYWdlKChwYWNrKSA9PiB7XG4gICAgICBpZiAocGFjay5uYW1lID09PSB0aGlzLnBhY2submFtZSkge1xuICAgICAgICB0aGlzLnVwZGF0ZURpc2FibGVkU3RhdGUoKVxuICAgICAgfVxuICAgIH0pKVxuXG4gICAgdGhpcy5kaXNwb3NhYmxlcy5hZGQoYXRvbS5wYWNrYWdlcy5vbkRpZEFjdGl2YXRlUGFja2FnZSgocGFjaykgPT4ge1xuICAgICAgaWYgKHBhY2submFtZSA9PT0gdGhpcy5wYWNrLm5hbWUpIHtcbiAgICAgICAgdGhpcy51cGRhdGVEaXNhYmxlZFN0YXRlKClcbiAgICAgIH1cbiAgICB9KSlcblxuICAgIHRoaXMuZGlzcG9zYWJsZXMuYWRkKGF0b20uY29uZmlnLm9uRGlkQ2hhbmdlKCdjb3JlLmRpc2FibGVkUGFja2FnZXMnLCAoKSA9PiB7XG4gICAgICB0aGlzLnVwZGF0ZURpc2FibGVkU3RhdGUoKVxuICAgIH0pKVxuXG4gICAgdGhpcy5zdWJzY3JpYmVUb1BhY2thZ2VFdmVudCgncGFja2FnZS1pbnN0YWxsaW5nIHRoZW1lLWluc3RhbGxpbmcnLCAoKSA9PiB7XG4gICAgICB0aGlzLnVwZGF0ZUludGVyZmFjZVN0YXRlKClcbiAgICAgIHRoaXMucmVmcy5pbnN0YWxsQnV0dG9uLmRpc2FibGVkID0gdHJ1ZVxuICAgICAgdGhpcy5yZWZzLmluc3RhbGxCdXR0b24uY2xhc3NMaXN0LmFkZCgnaXMtaW5zdGFsbGluZycpXG4gICAgfSlcblxuICAgIHRoaXMuc3Vic2NyaWJlVG9QYWNrYWdlRXZlbnQoJ3BhY2thZ2UtdXBkYXRpbmcgdGhlbWUtdXBkYXRpbmcnLCAoKSA9PiB7XG4gICAgICB0aGlzLnVwZGF0ZUludGVyZmFjZVN0YXRlKClcbiAgICAgIHRoaXMucmVmcy51cGRhdGVCdXR0b24uZGlzYWJsZWQgPSB0cnVlXG4gICAgICB0aGlzLnJlZnMudXBkYXRlQnV0dG9uLmNsYXNzTGlzdC5hZGQoJ2lzLWluc3RhbGxpbmcnKVxuICAgIH0pXG5cbiAgICB0aGlzLnN1YnNjcmliZVRvUGFja2FnZUV2ZW50KCdwYWNrYWdlLXVuaW5zdGFsbGluZyB0aGVtZS11bmluc3RhbGxpbmcnLCAoKSA9PiB7XG4gICAgICB0aGlzLnVwZGF0ZUludGVyZmFjZVN0YXRlKClcbiAgICAgIHRoaXMucmVmcy5lbmFibGVtZW50QnV0dG9uLmRpc2FibGVkID0gdHJ1ZVxuICAgICAgdGhpcy5yZWZzLnVuaW5zdGFsbEJ1dHRvbi5kaXNhYmxlZCA9IHRydWVcbiAgICAgIHRoaXMucmVmcy51bmluc3RhbGxCdXR0b24uY2xhc3NMaXN0LmFkZCgnaXMtdW5pbnN0YWxsaW5nJylcbiAgICB9KVxuXG4gICAgdGhpcy5zdWJzY3JpYmVUb1BhY2thZ2VFdmVudCgncGFja2FnZS1pbnN0YWxsZWQgcGFja2FnZS1pbnN0YWxsLWZhaWxlZCB0aGVtZS1pbnN0YWxsZWQgdGhlbWUtaW5zdGFsbC1mYWlsZWQnLCAoKSA9PiB7XG4gICAgICBjb25zdCBsb2FkZWRQYWNrID0gYXRvbS5wYWNrYWdlcy5nZXRMb2FkZWRQYWNrYWdlKHRoaXMucGFjay5uYW1lKVxuICAgICAgY29uc3QgdmVyc2lvbiA9IGxvYWRlZFBhY2sgJiYgbG9hZGVkUGFjay5tZXRhZGF0YSA/IGxvYWRlZFBhY2subWV0YWRhdGEudmVyc2lvbiA6IG51bGxcbiAgICAgIGlmICh2ZXJzaW9uKSB7XG4gICAgICAgIHRoaXMucGFjay52ZXJzaW9uID0gdmVyc2lvblxuICAgICAgfVxuICAgICAgdGhpcy5yZWZzLmluc3RhbGxCdXR0b24uZGlzYWJsZWQgPSBmYWxzZVxuICAgICAgdGhpcy5yZWZzLmluc3RhbGxCdXR0b24uY2xhc3NMaXN0LnJlbW92ZSgnaXMtaW5zdGFsbGluZycpXG4gICAgICB0aGlzLnVwZGF0ZUludGVyZmFjZVN0YXRlKClcbiAgICB9KVxuXG4gICAgdGhpcy5zdWJzY3JpYmVUb1BhY2thZ2VFdmVudCgncGFja2FnZS11cGRhdGVkIHRoZW1lLXVwZGF0ZWQnLCAoKSA9PiB7XG4gICAgICBjb25zdCBsb2FkZWRQYWNrID0gYXRvbS5wYWNrYWdlcy5nZXRMb2FkZWRQYWNrYWdlKHRoaXMucGFjay5uYW1lKVxuICAgICAgY29uc3QgbWV0YWRhdGEgPSBsb2FkZWRQYWNrID8gbG9hZGVkUGFjay5tZXRhZGF0YSA6IG51bGxcbiAgICAgIGlmIChtZXRhZGF0YSAmJiBtZXRhZGF0YS52ZXJzaW9uKSB7XG4gICAgICAgIHRoaXMucGFjay52ZXJzaW9uID0gbWV0YWRhdGEudmVyc2lvblxuICAgICAgfVxuXG4gICAgICBpZiAobWV0YWRhdGEgJiYgbWV0YWRhdGEuYXBtSW5zdGFsbFNvdXJjZSkge1xuICAgICAgICB0aGlzLnBhY2suYXBtSW5zdGFsbFNvdXJjZSA9IG1ldGFkYXRhLmFwbUluc3RhbGxTb3VyY2VcbiAgICAgIH1cblxuICAgICAgdGhpcy5uZXdWZXJzaW9uID0gbnVsbFxuICAgICAgdGhpcy5uZXdTaGEgPSBudWxsXG4gICAgICB0aGlzLnJlZnMudXBkYXRlQnV0dG9uLmRpc2FibGVkID0gZmFsc2VcbiAgICAgIHRoaXMucmVmcy51cGRhdGVCdXR0b24uY2xhc3NMaXN0LnJlbW92ZSgnaXMtaW5zdGFsbGluZycpXG4gICAgICB0aGlzLnVwZGF0ZUludGVyZmFjZVN0YXRlKClcbiAgICB9KVxuXG4gICAgdGhpcy5zdWJzY3JpYmVUb1BhY2thZ2VFdmVudCgncGFja2FnZS11cGRhdGUtZmFpbGVkIHRoZW1lLXVwZGF0ZS1mYWlsZWQnLCAoKSA9PiB7XG4gICAgICB0aGlzLnJlZnMudXBkYXRlQnV0dG9uLmRpc2FibGVkID0gZmFsc2VcbiAgICAgIHRoaXMucmVmcy51cGRhdGVCdXR0b24uY2xhc3NMaXN0LnJlbW92ZSgnaXMtaW5zdGFsbGluZycpXG4gICAgICB0aGlzLnVwZGF0ZUludGVyZmFjZVN0YXRlKClcbiAgICB9KVxuXG4gICAgdGhpcy5zdWJzY3JpYmVUb1BhY2thZ2VFdmVudCgncGFja2FnZS11bmluc3RhbGxlZCBwYWNrYWdlLXVuaW5zdGFsbC1mYWlsZWQgdGhlbWUtdW5pbnN0YWxsZWQgdGhlbWUtdW5pbnN0YWxsLWZhaWxlZCcsICgpID0+IHtcbiAgICAgIHRoaXMubmV3VmVyc2lvbiA9IG51bGxcbiAgICAgIHRoaXMubmV3U2hhID0gbnVsbFxuICAgICAgdGhpcy5yZWZzLmVuYWJsZW1lbnRCdXR0b24uZGlzYWJsZWQgPSBmYWxzZVxuICAgICAgdGhpcy5yZWZzLnVuaW5zdGFsbEJ1dHRvbi5kaXNhYmxlZCA9IGZhbHNlXG4gICAgICB0aGlzLnJlZnMudW5pbnN0YWxsQnV0dG9uLmNsYXNzTGlzdC5yZW1vdmUoJ2lzLXVuaW5zdGFsbGluZycpXG4gICAgICB0aGlzLnVwZGF0ZUludGVyZmFjZVN0YXRlKClcbiAgICB9KVxuICB9XG5cbiAgaXNJbnN0YWxsZWQgKCkge1xuICAgIHJldHVybiB0aGlzLnBhY2thZ2VNYW5hZ2VyLmlzUGFja2FnZUluc3RhbGxlZCh0aGlzLnBhY2submFtZSlcbiAgfVxuXG4gIGlzRGlzYWJsZWQgKCkge1xuICAgIHJldHVybiBhdG9tLnBhY2thZ2VzLmlzUGFja2FnZURpc2FibGVkKHRoaXMucGFjay5uYW1lKVxuICB9XG5cbiAgaGFzU2V0dGluZ3MgKCkge1xuICAgIHJldHVybiB0aGlzLnBhY2thZ2VNYW5hZ2VyLnBhY2thZ2VIYXNTZXR0aW5ncyh0aGlzLnBhY2submFtZSlcbiAgfVxuXG4gIHN1YnNjcmliZVRvUGFja2FnZUV2ZW50IChldmVudCwgY2FsbGJhY2spIHtcbiAgICB0aGlzLmRpc3Bvc2FibGVzLmFkZCh0aGlzLnBhY2thZ2VNYW5hZ2VyLm9uKGV2ZW50LCAoe3BhY2ssIGVycm9yfSkgPT4ge1xuICAgICAgaWYgKHBhY2sucGFjayAhPSBudWxsKSB7XG4gICAgICAgIHBhY2sgPSBwYWNrLnBhY2tcbiAgICAgIH1cblxuICAgICAgY29uc3QgcGFja2FnZU5hbWUgPSBwYWNrLm5hbWVcbiAgICAgIGlmIChwYWNrYWdlTmFtZSA9PT0gdGhpcy5wYWNrLm5hbWUpIHtcbiAgICAgICAgY2FsbGJhY2socGFjaywgZXJyb3IpXG4gICAgICB9XG4gICAgfSkpXG4gIH1cblxuICAvKlxuICBTZWN0aW9uOiBNZXRob2RzIHRoYXQgc2hvdWxkIGJlIG9uIGEgUGFja2FnZSBtb2RlbFxuICAqL1xuXG4gIGluc3RhbGwgKCkge1xuICAgIHRoaXMucGFja2FnZU1hbmFnZXIuaW5zdGFsbCh0aGlzLmluc3RhbGxhYmxlUGFjayAhPSBudWxsID8gdGhpcy5pbnN0YWxsYWJsZVBhY2sgOiB0aGlzLnBhY2ssIChlcnJvcikgPT4ge1xuICAgICAgaWYgKGVycm9yICE9IG51bGwpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihgSW5zdGFsbGluZyAke3RoaXMudHlwZX0gJHt0aGlzLnBhY2submFtZX0gZmFpbGVkYCwgZXJyb3Iuc3RhY2sgIT0gbnVsbCA/IGVycm9yLnN0YWNrIDogZXJyb3IsIGVycm9yLnN0ZGVycilcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIGlmIGEgcGFja2FnZSB3YXMgZGlzYWJsZWQgYmVmb3JlIGluc3RhbGxpbmcgaXQsIHJlLWVuYWJsZSBpdFxuICAgICAgICBpZiAodGhpcy5pc0Rpc2FibGVkKCkpIHtcbiAgICAgICAgICBhdG9tLnBhY2thZ2VzLmVuYWJsZVBhY2thZ2UodGhpcy5wYWNrLm5hbWUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgdXBkYXRlICgpIHtcbiAgICBpZiAoIXRoaXMubmV3VmVyc2lvbiAmJiAhdGhpcy5uZXdTaGEpIHtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKVxuICAgIH1cblxuICAgIGNvbnN0IHBhY2sgPSB0aGlzLmluc3RhbGxhYmxlUGFjayAhPSBudWxsID8gdGhpcy5pbnN0YWxsYWJsZVBhY2sgOiB0aGlzLnBhY2tcbiAgICBjb25zdCB2ZXJzaW9uID0gdGhpcy5uZXdWZXJzaW9uID8gYHYke3RoaXMubmV3VmVyc2lvbn1gIDogYCMke3RoaXMubmV3U2hhLnN1YnN0cigwLCA4KX1gXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHRoaXMucGFja2FnZU1hbmFnZXIudXBkYXRlKHBhY2ssIHRoaXMubmV3VmVyc2lvbiwgZXJyb3IgPT4ge1xuICAgICAgICBpZiAoZXJyb3IgIT0gbnVsbCkge1xuICAgICAgICAgIGF0b20uYXNzZXJ0KGZhbHNlLCAnUGFja2FnZSB1cGRhdGUgZmFpbGVkJywgYXNzZXJ0aW9uRXJyb3IgPT4ge1xuICAgICAgICAgICAgYXNzZXJ0aW9uRXJyb3IubWV0YWRhdGEgPSB7XG4gICAgICAgICAgICAgIHR5cGU6IHRoaXMudHlwZSxcbiAgICAgICAgICAgICAgbmFtZTogcGFjay5uYW1lLFxuICAgICAgICAgICAgICB2ZXJzaW9uLFxuICAgICAgICAgICAgICBlcnJvck1lc3NhZ2U6IGVycm9yLm1lc3NhZ2UsXG4gICAgICAgICAgICAgIGVycm9yU3RhY2s6IGVycm9yLnN0YWNrLFxuICAgICAgICAgICAgICBlcnJvclN0ZGVycjogZXJyb3Iuc3RkZXJyXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSlcbiAgICAgICAgICBjb25zb2xlLmVycm9yKGBVcGRhdGluZyAke3RoaXMudHlwZX0gJHtwYWNrLm5hbWV9IHRvICR7dmVyc2lvbn0gZmFpbGVkOlxcbmAsIGVycm9yLCBlcnJvci5zdGRlcnIgIT0gbnVsbCA/IGVycm9yLnN0ZGVyciA6ICcnKVxuICAgICAgICAgIHJlamVjdChlcnJvcilcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXNvbHZlKClcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9KVxuICB9XG5cbiAgdW5pbnN0YWxsICgpIHtcbiAgICB0aGlzLnBhY2thZ2VNYW5hZ2VyLnVuaW5zdGFsbCh0aGlzLnBhY2ssIChlcnJvcikgPT4ge1xuICAgICAgaWYgKGVycm9yICE9IG51bGwpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihgVW5pbnN0YWxsaW5nICR7dGhpcy50eXBlfSAke3RoaXMucGFjay5uYW1lfSBmYWlsZWRgLCBlcnJvci5zdGFjayAhPSBudWxsID8gZXJyb3Iuc3RhY2sgOiBlcnJvciwgZXJyb3Iuc3RkZXJyKVxuICAgICAgfVxuICAgIH0pXG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBRUE7QUFBMkM7QUFQM0M7QUFDQTs7QUFRQSxJQUFJQSxNQUFNLEdBQUcsSUFBSTtBQUVGLE1BQU1DLFdBQVcsQ0FBQztFQUMvQkMsV0FBVyxDQUFFQyxJQUFJLEVBQUVDLFlBQVksRUFBRUMsY0FBYyxFQUFFQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLEVBQUU7SUFDN0QsSUFBSSxDQUFDSCxJQUFJLEdBQUdBLElBQUk7SUFDaEIsSUFBSSxDQUFDQyxZQUFZLEdBQUdBLFlBQVk7SUFDaEMsSUFBSSxDQUFDQyxjQUFjLEdBQUdBLGNBQWM7SUFDcEMsSUFBSSxDQUFDRSxXQUFXLEdBQUcsSUFBSUMseUJBQW1CLEVBQUU7O0lBRTVDO0lBQ0E7SUFDQTtJQUNBO0lBQ0EsSUFBSSxDQUFDQyxNQUFNLEdBQUcsSUFBSSxDQUFDSixjQUFjLENBQUNLLFNBQVMsRUFBRTtJQUM3QyxJQUFJLENBQUNDLElBQUksR0FBRyxJQUFJLENBQUNSLElBQUksQ0FBQ1MsS0FBSyxHQUFHLE9BQU8sR0FBRyxTQUFTO0lBQ2pELElBQUksQ0FBQ0MsSUFBSSxHQUFHLElBQUksQ0FBQ1YsSUFBSSxDQUFDVSxJQUFJO0lBQzFCLElBQUksQ0FBQ0MsY0FBYyxHQUFHUixPQUFPLENBQUNRLGNBQWM7SUFFNUMsSUFBSSxJQUFJLENBQUNYLElBQUksQ0FBQ1ksYUFBYSxLQUFLLElBQUksQ0FBQ1osSUFBSSxDQUFDYSxPQUFPLEVBQUU7TUFDakQsSUFBSSxDQUFDQyxVQUFVLEdBQUcsSUFBSSxDQUFDZCxJQUFJLENBQUNZLGFBQWE7SUFDM0M7SUFFQSxJQUFJLElBQUksQ0FBQ1osSUFBSSxDQUFDZSxnQkFBZ0IsSUFBSSxJQUFJLENBQUNmLElBQUksQ0FBQ2UsZ0JBQWdCLENBQUNQLElBQUksS0FBSyxLQUFLLEVBQUU7TUFDM0UsSUFBSSxJQUFJLENBQUNSLElBQUksQ0FBQ2UsZ0JBQWdCLENBQUNDLEdBQUcsS0FBSyxJQUFJLENBQUNoQixJQUFJLENBQUNpQixTQUFTLEVBQUU7UUFDMUQsSUFBSSxDQUFDQyxNQUFNLEdBQUcsSUFBSSxDQUFDbEIsSUFBSSxDQUFDaUIsU0FBUztNQUNuQztJQUNGOztJQUVBO0lBQ0EsSUFBSSxDQUFDZCxPQUFPLENBQUNnQixLQUFLLEVBQUU7TUFDbEJoQixPQUFPLENBQUNnQixLQUFLLEdBQUc7UUFBQ0MsU0FBUyxFQUFFO01BQUksQ0FBQztJQUNuQztJQUVBQyxhQUFJLENBQUNDLFVBQVUsQ0FBQyxJQUFJLENBQUM7SUFFckIsSUFBSSxDQUFDQyxZQUFZLENBQUNwQixPQUFPLENBQUM7SUFDMUIsSUFBSSxDQUFDcUIsbUJBQW1CLEVBQUU7SUFDMUIsSUFBSSxDQUFDQyxrQkFBa0IsQ0FBQ3RCLE9BQU8sQ0FBQztJQUNoQyxJQUFJLENBQUN1QixrQkFBa0IsRUFBRTs7SUFFekI7SUFDQSxJQUFJLElBQUksQ0FBQ2xCLElBQUksS0FBSyxPQUFPLEVBQUU7TUFDekIsSUFBSSxDQUFDbUIsSUFBSSxDQUFDQyxlQUFlLENBQUNDLE1BQU0sRUFBRTtNQUNsQyxJQUFJLENBQUNGLElBQUksQ0FBQ0csZ0JBQWdCLENBQUNELE1BQU0sRUFBRTtJQUNyQztJQUVBLElBQUlFLElBQUksQ0FBQ0MsUUFBUSxDQUFDQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUNqQyxJQUFJLENBQUNVLElBQUksQ0FBQyxFQUFFO01BQ2xELElBQUksQ0FBQ2lCLElBQUksQ0FBQ08sa0JBQWtCLENBQUNMLE1BQU0sRUFBRTtNQUNyQyxJQUFJLENBQUNGLElBQUksQ0FBQ1EsZUFBZSxDQUFDTixNQUFNLEVBQUU7SUFDcEM7SUFFQSxJQUFJLENBQUMsSUFBSSxDQUFDZixVQUFVLElBQUksQ0FBQyxJQUFJLENBQUNJLE1BQU0sRUFBRTtNQUNwQyxJQUFJLENBQUNTLElBQUksQ0FBQ1MsaUJBQWlCLENBQUNDLEtBQUssQ0FBQ0MsT0FBTyxHQUFHLE1BQU07SUFDcEQ7SUFFQSxJQUFJLENBQUNDLG9CQUFvQixHQUFHLElBQUk7SUFDaEMsSUFBSSxDQUFDQyxvQkFBb0IsRUFBRTtFQUM3QjtFQUVBQyxNQUFNLEdBQUk7SUFDUixNQUFNQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMxQyxJQUFJLENBQUMyQyxVQUFVLEdBQUcsSUFBSSxDQUFDM0MsSUFBSSxDQUFDMkMsVUFBVSxDQUFDQyxPQUFPLEdBQUcsSUFBSSxDQUFDNUMsSUFBSSxDQUFDVSxJQUFJLEtBQUssRUFBRTtJQUNoRyxNQUFNbUMsS0FBSyxHQUFHLElBQUFDLDBCQUFtQixFQUFDLElBQUksQ0FBQzlDLElBQUksQ0FBQytDLFVBQVUsQ0FBQztJQUN2RCxNQUFNQyxXQUFXLEdBQUcsSUFBSSxDQUFDaEQsSUFBSSxDQUFDZ0QsV0FBVyxJQUFJLEVBQUU7SUFFL0MsT0FDRTtNQUFLLFNBQVMsRUFBQztJQUF1QixHQUNwQztNQUFLLEdBQUcsRUFBQyxnQkFBZ0I7TUFBQyxTQUFTLEVBQUM7SUFBa0IsR0FDcEQ7TUFBTSxHQUFHLEVBQUMsY0FBYztNQUFDLFNBQVMsRUFBQztJQUFZLEdBQzdDO01BQU0sR0FBRyxFQUFDLGVBQWU7TUFBQyxTQUFTLEVBQUM7SUFBZ0IsRUFBRyxFQUN2RDtNQUFNLEdBQUcsRUFBQyxnQkFBZ0I7TUFBQyxTQUFTLEVBQUM7SUFBTyxFQUFHLENBQzFDLEVBRVA7TUFBTSxHQUFHLEVBQUMsa0JBQWtCO01BQUMsU0FBUyxFQUFDO0lBQVksR0FDakQ7TUFBTSxHQUFHLEVBQUMsY0FBYztNQUFDLFNBQVMsRUFBQztJQUEwQixFQUFHLEVBQ2hFO01BQU0sR0FBRyxFQUFDLGVBQWU7TUFBQyxTQUFTLEVBQUM7SUFBTyxFQUFHLENBQ3pDLENBQ0gsRUFFTjtNQUFLLFNBQVMsRUFBQztJQUFNLEdBQ25CO01BQUksU0FBUyxFQUFDO0lBQVcsR0FDdkI7TUFBRyxTQUFTLEVBQUMsY0FBYztNQUFDLEdBQUcsRUFBQztJQUFhLEdBQUVOLFdBQVcsQ0FBSyxFQUMvRDtNQUFNLFNBQVMsRUFBQztJQUFpQixHQUMvQjtNQUFNLEdBQUcsRUFBQyxjQUFjO01BQUMsU0FBUyxFQUFDO0lBQU8sR0FBRU8sTUFBTSxDQUFDLElBQUksQ0FBQ2pELElBQUksQ0FBQ2EsT0FBTyxDQUFDLENBQVEsQ0FDeEUsQ0FDSixFQUNMO01BQU0sR0FBRyxFQUFDLG9CQUFvQjtNQUFDLFNBQVMsRUFBQztJQUFxQixHQUFFbUMsV0FBVyxDQUFRLEVBQ25GO01BQUssR0FBRyxFQUFDLGdCQUFnQjtNQUFDLFNBQVMsRUFBQztJQUFpQixFQUFHLENBQ3BELEVBRU47TUFBSyxTQUFTLEVBQUM7SUFBTSxHQUNuQjtNQUFLLEdBQUcsRUFBQyxtQkFBbUI7TUFBQyxTQUFTLEVBQUM7SUFBVyxHQUNoRDtNQUFHLEdBQUcsRUFBQztJQUFZLEdBRWpCO01BQUssR0FBRyxFQUFDLFFBQVE7TUFBQyxTQUFTLEVBQUMsUUFBUTtNQUFDLEdBQUcsRUFBQztJQUFnRixFQUFHLENBQzFILEVBQ0o7TUFBRyxHQUFHLEVBQUMsV0FBVztNQUFDLFNBQVMsRUFBQztJQUFRLEdBQUVILEtBQUssQ0FBSyxDQUM3QyxFQUNOO01BQUssU0FBUyxFQUFDO0lBQWUsR0FDNUI7TUFBSyxTQUFTLEVBQUM7SUFBYSxHQUMxQjtNQUFLLEdBQUcsRUFBQyxtQkFBbUI7TUFBQyxTQUFTLEVBQUM7SUFBVyxHQUNoRDtNQUFRLElBQUksRUFBQyxRQUFRO01BQUMsU0FBUyxFQUFDLHNEQUFzRDtNQUFDLEdBQUcsRUFBQztJQUFjLFlBQWdCLENBQ3JILEVBQ047TUFBSyxHQUFHLEVBQUMsb0JBQW9CO01BQUMsU0FBUyxFQUFDO0lBQVcsR0FDakQ7TUFBUSxJQUFJLEVBQUMsUUFBUTtNQUFDLFNBQVMsRUFBQyxzREFBc0Q7TUFBQyxHQUFHLEVBQUM7SUFBZSxhQUFpQixDQUN2SCxFQUNOO01BQUssR0FBRyxFQUFDLDBCQUEwQjtNQUFDLFNBQVMsRUFBQztJQUFXLEdBQ3ZEO01BQVEsSUFBSSxFQUFDLFFBQVE7TUFBQyxTQUFTLEVBQUMsNkJBQTZCO01BQUMsR0FBRyxFQUFDO0lBQWdCLGNBQWtCLEVBQ3BHO01BQVEsSUFBSSxFQUFDLFFBQVE7TUFBQyxTQUFTLEVBQUMseUNBQXlDO01BQUMsR0FBRyxFQUFDO0lBQWlCLGVBQW1CLEVBQ2xIO01BQVEsSUFBSSxFQUFDLFFBQVE7TUFBQyxTQUFTLEVBQUMseUNBQXlDO01BQUMsR0FBRyxFQUFDO0lBQWtCLEdBQzlGO01BQU0sU0FBUyxFQUFDO0lBQWMsYUFBZSxDQUN0QyxFQUNUO01BQVEsSUFBSSxFQUFDLFFBQVE7TUFBQyxTQUFTLEVBQUMsc0JBQXNCO01BQUMsUUFBUSxFQUFDLElBQUk7TUFBQyxHQUFHLEVBQUM7SUFBaUIsRUFBRyxDQUN6RixDQUNGLENBQ0YsQ0FDRixDQUNGO0VBRVY7RUFFQUssOEJBQThCLENBQUVDLFFBQVEsRUFBRTtJQUN4QyxJQUFJLENBQUNqRCxjQUFjLENBQUNrRCw0QkFBNEIsQ0FBQyxJQUFJLENBQUNwRCxJQUFJLENBQUNVLElBQUksRUFBRSxDQUFDMkMsR0FBRyxFQUFFckQsSUFBSSxLQUFLO01BQzlFLElBQUlxRCxHQUFHLElBQUksSUFBSSxFQUFFO1FBQ2ZDLE9BQU8sQ0FBQ0MsS0FBSyxDQUFDRixHQUFHLENBQUM7TUFDcEI7TUFFQSxNQUFNRyxjQUFjLEdBQUd4RCxJQUFJLENBQUNhLE9BQU87O01BRW5DO01BQ0E7TUFDQTtNQUNBLElBQUkyQyxjQUFjLEVBQUU7UUFDbEIsSUFBSSxDQUFDN0IsSUFBSSxDQUFDOEIsWUFBWSxDQUFDQyxXQUFXLEdBQUdGLGNBQWM7UUFDbkQsSUFBSUEsY0FBYyxLQUFLLElBQUksQ0FBQ3hELElBQUksQ0FBQ2EsT0FBTyxFQUFFO1VBQ3hDLElBQUksQ0FBQ2MsSUFBSSxDQUFDOEIsWUFBWSxDQUFDRSxTQUFTLENBQUNDLEdBQUcsQ0FBQyxjQUFjLENBQUM7VUFDcEQsSUFBSSxDQUFDakMsSUFBSSxDQUFDa0MsY0FBYyxDQUFDRixTQUFTLENBQUNDLEdBQUcsQ0FBQyxjQUFjLENBQUM7VUFDdEQsSUFBSSxDQUFDakMsSUFBSSxDQUFDa0MsY0FBYyxDQUFDSCxXQUFXLEdBQUksV0FBVUYsY0FBZSwwSEFBeUg7UUFDNUw7UUFFQSxJQUFJLENBQUNNLGVBQWUsR0FBRzlELElBQUk7UUFDM0IsSUFBSSxDQUFDdUMsb0JBQW9CLEdBQUcsSUFBSTtNQUNsQyxDQUFDLE1BQU07UUFDTCxJQUFJLENBQUNBLG9CQUFvQixHQUFHLEtBQUs7UUFDakMsSUFBSSxDQUFDWixJQUFJLENBQUM4QixZQUFZLENBQUNFLFNBQVMsQ0FBQ0MsR0FBRyxDQUFDLFlBQVksQ0FBQztRQUNsRCxJQUFJLENBQUNqQyxJQUFJLENBQUNrQyxjQUFjLENBQUNGLFNBQVMsQ0FBQ0MsR0FBRyxDQUFDLFlBQVksQ0FBQztRQUNwRCxJQUFJLENBQUNqQyxJQUFJLENBQUNrQyxjQUFjLENBQUNFLGtCQUFrQixDQUN6QyxXQUFXLEVBQ1YsMEdBQXlHLElBQUksQ0FBQy9ELElBQUksQ0FBQ2dFLE9BQU8sQ0FBQ2pDLElBQUssR0FBRSxDQUNwSTtRQUNEdUIsT0FBTyxDQUFDQyxLQUFLLENBQUUsb0VBQW1FeEIsSUFBSSxDQUFDa0MsVUFBVSxFQUFHLEVBQUMsQ0FBQztNQUN4RztNQUVBZCxRQUFRLEVBQUU7SUFDWixDQUFDLENBQUM7RUFDSjtFQUVBMUIsa0JBQWtCLENBQUV0QixPQUFPLEVBQUU7SUFDM0IsSUFBSUEsT0FBTyxJQUFJQSxPQUFPLENBQUNRLGNBQWMsRUFBRTtNQUNyQyxJQUFJLENBQUNnQixJQUFJLENBQUN1QyxjQUFjLENBQUM3QixLQUFLLENBQUNDLE9BQU8sR0FBRyxNQUFNO0lBQ2pELENBQUMsTUFBTTtNQUNMLE1BQU02QixZQUFZLEdBQUlDLEtBQUssSUFBSztRQUM5QkEsS0FBSyxDQUFDQyxlQUFlLEVBQUU7UUFDdkIsSUFBSSxDQUFDcEUsWUFBWSxDQUFDcUUsU0FBUyxDQUFDLElBQUksQ0FBQ3RFLElBQUksQ0FBQ1UsSUFBSSxFQUFFO1VBQUM2RCxJQUFJLEVBQUVwRSxPQUFPLEdBQUdBLE9BQU8sQ0FBQ29FLElBQUksR0FBRyxJQUFJO1VBQUV2RSxJQUFJLEVBQUUsSUFBSSxDQUFDQTtRQUFJLENBQUMsQ0FBQztNQUNyRyxDQUFDO01BRUQsSUFBSSxDQUFDd0UsT0FBTyxDQUFDQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUVOLFlBQVksQ0FBQztNQUNwRCxJQUFJLENBQUMvRCxXQUFXLENBQUN3RCxHQUFHLENBQUMsSUFBSWMsZ0JBQVUsQ0FBQyxNQUFNO1FBQUUsSUFBSSxDQUFDRixPQUFPLENBQUNHLG1CQUFtQixDQUFDLE9BQU8sRUFBRVIsWUFBWSxDQUFDO01BQUMsQ0FBQyxDQUFDLENBQUM7TUFFdkcsSUFBSSxDQUFDeEMsSUFBSSxDQUFDdUMsY0FBYyxDQUFDTyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUVOLFlBQVksQ0FBQztNQUNoRSxJQUFJLENBQUMvRCxXQUFXLENBQUN3RCxHQUFHLENBQUMsSUFBSWMsZ0JBQVUsQ0FBQyxNQUFNO1FBQUUsSUFBSSxDQUFDL0MsSUFBSSxDQUFDdUMsY0FBYyxDQUFDUyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUVSLFlBQVksQ0FBQztNQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3JIO0lBRUEsTUFBTVMseUJBQXlCLEdBQUlSLEtBQUssSUFBSztNQUMzQ0EsS0FBSyxDQUFDQyxlQUFlLEVBQUU7TUFDdkIsSUFBSSxDQUFDUSxPQUFPLEVBQUU7SUFDaEIsQ0FBQztJQUNELElBQUksQ0FBQ2xELElBQUksQ0FBQ21ELGFBQWEsQ0FBQ0wsZ0JBQWdCLENBQUMsT0FBTyxFQUFFRyx5QkFBeUIsQ0FBQztJQUM1RSxJQUFJLENBQUN4RSxXQUFXLENBQUN3RCxHQUFHLENBQUMsSUFBSWMsZ0JBQVUsQ0FBQyxNQUFNO01BQUUsSUFBSSxDQUFDL0MsSUFBSSxDQUFDbUQsYUFBYSxDQUFDSCxtQkFBbUIsQ0FBQyxPQUFPLEVBQUVDLHlCQUF5QixDQUFDO0lBQUMsQ0FBQyxDQUFDLENBQUM7SUFFL0gsTUFBTUcsMkJBQTJCLEdBQUlYLEtBQUssSUFBSztNQUM3Q0EsS0FBSyxDQUFDQyxlQUFlLEVBQUU7TUFDdkIsSUFBSSxDQUFDVyxTQUFTLEVBQUU7SUFDbEIsQ0FBQztJQUNELElBQUksQ0FBQ3JELElBQUksQ0FBQ1EsZUFBZSxDQUFDc0MsZ0JBQWdCLENBQUMsT0FBTyxFQUFFTSwyQkFBMkIsQ0FBQztJQUNoRixJQUFJLENBQUMzRSxXQUFXLENBQUN3RCxHQUFHLENBQUMsSUFBSWMsZ0JBQVUsQ0FBQyxNQUFNO01BQUUsSUFBSSxDQUFDL0MsSUFBSSxDQUFDUSxlQUFlLENBQUN3QyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUVJLDJCQUEyQixDQUFDO0lBQUMsQ0FBQyxDQUFDLENBQUM7SUFFbkksTUFBTUUsd0JBQXdCLEdBQUliLEtBQUssSUFBSztNQUMxQ0EsS0FBSyxDQUFDQyxlQUFlLEVBQUU7TUFDdkIsSUFBSSxDQUFDYSxNQUFNLEVBQUUsQ0FBQ0MsSUFBSSxDQUFDLE1BQU07UUFDdkIsSUFBSUMsVUFBVSxHQUFHLEVBQUU7UUFDbkIsSUFBSXRFLFVBQVUsR0FBRyxFQUFFO1FBRW5CLElBQUksSUFBSSxDQUFDZCxJQUFJLENBQUNlLGdCQUFnQixJQUFJLElBQUksQ0FBQ2YsSUFBSSxDQUFDZSxnQkFBZ0IsQ0FBQ1AsSUFBSSxLQUFLLEtBQUssRUFBRTtVQUMzRTRFLFVBQVUsR0FBRyxJQUFJLENBQUNwRixJQUFJLENBQUNlLGdCQUFnQixDQUFDQyxHQUFHLENBQUNxRSxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztVQUN4RHZFLFVBQVUsR0FBSSxHQUFFLElBQUksQ0FBQ2QsSUFBSSxDQUFDaUIsU0FBUyxDQUFDb0UsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUUsRUFBQztRQUNwRCxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUNyRixJQUFJLENBQUNhLE9BQU8sSUFBSSxJQUFJLENBQUNiLElBQUksQ0FBQ1ksYUFBYSxFQUFFO1VBQ3ZEd0UsVUFBVSxHQUFHLElBQUksQ0FBQ3BGLElBQUksQ0FBQ2EsT0FBTztVQUM5QkMsVUFBVSxHQUFHLElBQUksQ0FBQ2QsSUFBSSxDQUFDWSxhQUFhO1FBQ3RDO1FBRUEsSUFBSTBFLE1BQU0sR0FBRyxFQUFFO1FBQ2YsSUFBSUYsVUFBVSxJQUFJdEUsVUFBVSxFQUFFO1VBQzVCd0UsTUFBTSxHQUFJLEdBQUVGLFVBQVcsT0FBTXRFLFVBQVcsRUFBQztRQUMzQztRQUVBLE1BQU15RSxZQUFZLEdBQUd4RCxJQUFJLENBQUN5RCxhQUFhLENBQUNDLFVBQVUsQ0FBRSw0Q0FBMkMsSUFBSSxDQUFDekYsSUFBSSxDQUFDVSxJQUFLLEtBQUksRUFBRTtVQUNsSGdGLFdBQVcsRUFBRSxJQUFJO1VBQ2pCQyxPQUFPLEVBQUUsQ0FBQztZQUNSQyxJQUFJLEVBQUUsYUFBYTtZQUNuQkMsVUFBVSxHQUFJO2NBQUUsT0FBTzlELElBQUksQ0FBQytELGtCQUFrQixFQUFFO1lBQUM7VUFDbkQsQ0FBQyxFQUNEO1lBQ0VGLElBQUksRUFBRSxtQkFBbUI7WUFDekJDLFVBQVUsR0FBSTtjQUFFTixZQUFZLENBQUNRLE9BQU8sRUFBRTtZQUFDO1VBQ3pDLENBQUMsQ0FBQztVQUNGVDtRQUNGLENBQUMsQ0FBQztNQUNKLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCxJQUFJLENBQUMzRCxJQUFJLENBQUNxRSxZQUFZLENBQUN2QixnQkFBZ0IsQ0FBQyxPQUFPLEVBQUVRLHdCQUF3QixDQUFDO0lBQzFFLElBQUksQ0FBQzdFLFdBQVcsQ0FBQ3dELEdBQUcsQ0FBQyxJQUFJYyxnQkFBVSxDQUFDLE1BQU07TUFBRSxJQUFJLENBQUMvQyxJQUFJLENBQUNxRSxZQUFZLENBQUNyQixtQkFBbUIsQ0FBQyxPQUFPLEVBQUVNLHdCQUF3QixDQUFDO0lBQUMsQ0FBQyxDQUFDLENBQUM7SUFFN0gsTUFBTWdCLHVCQUF1QixHQUFJN0IsS0FBSyxJQUFLO01BQ3pDQSxLQUFLLENBQUNDLGVBQWUsRUFBRTtNQUN2QjZCLGVBQUssQ0FBQ0MsWUFBWSxDQUFFLHdDQUF1QyxJQUFJLENBQUNuRyxJQUFJLENBQUNVLElBQUssRUFBQyxDQUFDO0lBQzlFLENBQUM7SUFDRCxJQUFJLENBQUNpQixJQUFJLENBQUN5RSxXQUFXLENBQUMzQixnQkFBZ0IsQ0FBQyxPQUFPLEVBQUV3Qix1QkFBdUIsQ0FBQztJQUN4RSxJQUFJLENBQUM3RixXQUFXLENBQUN3RCxHQUFHLENBQUMsSUFBSWMsZ0JBQVUsQ0FBQyxNQUFNO01BQUUsSUFBSSxDQUFDL0MsSUFBSSxDQUFDeUUsV0FBVyxDQUFDekIsbUJBQW1CLENBQUMsT0FBTyxFQUFFc0IsdUJBQXVCLENBQUM7SUFBQyxDQUFDLENBQUMsQ0FBQztJQUUzSCxNQUFNSSx5QkFBeUIsR0FBSWpDLEtBQUssSUFBSztNQUMzQ0EsS0FBSyxDQUFDQyxlQUFlLEVBQUU7TUFDdkI2QixlQUFLLENBQUNDLFlBQVksQ0FBRSxpQ0FBZ0MsSUFBQXJELDBCQUFtQixFQUFDLElBQUksQ0FBQzlDLElBQUksQ0FBQytDLFVBQVUsQ0FBRSxFQUFDLENBQUM7SUFDbEcsQ0FBQztJQUNELElBQUksQ0FBQ3BCLElBQUksQ0FBQzJFLFNBQVMsQ0FBQzdCLGdCQUFnQixDQUFDLE9BQU8sRUFBRTRCLHlCQUF5QixDQUFDO0lBQ3hFLElBQUksQ0FBQ2pHLFdBQVcsQ0FBQ3dELEdBQUcsQ0FBQyxJQUFJYyxnQkFBVSxDQUFDLE1BQU07TUFBRSxJQUFJLENBQUMvQyxJQUFJLENBQUMyRSxTQUFTLENBQUMzQixtQkFBbUIsQ0FBQyxPQUFPLEVBQUUwQix5QkFBeUIsQ0FBQztJQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNILElBQUksQ0FBQzFFLElBQUksQ0FBQzRFLFVBQVUsQ0FBQzlCLGdCQUFnQixDQUFDLE9BQU8sRUFBRTRCLHlCQUF5QixDQUFDO0lBQ3pFLElBQUksQ0FBQ2pHLFdBQVcsQ0FBQ3dELEdBQUcsQ0FBQyxJQUFJYyxnQkFBVSxDQUFDLE1BQU07TUFBRSxJQUFJLENBQUMvQyxJQUFJLENBQUM0RSxVQUFVLENBQUM1QixtQkFBbUIsQ0FBQyxPQUFPLEVBQUUwQix5QkFBeUIsQ0FBQztJQUFDLENBQUMsQ0FBQyxDQUFDO0lBRTVILE1BQU1HLDRCQUE0QixHQUFJcEMsS0FBSyxJQUFLO01BQzlDQSxLQUFLLENBQUNDLGVBQWUsRUFBRTtNQUN2QkQsS0FBSyxDQUFDcUMsY0FBYyxFQUFFO01BQ3RCLElBQUksSUFBSSxDQUFDQyxVQUFVLEVBQUUsRUFBRTtRQUNyQjNFLElBQUksQ0FBQ0MsUUFBUSxDQUFDMkUsYUFBYSxDQUFDLElBQUksQ0FBQzNHLElBQUksQ0FBQ1UsSUFBSSxDQUFDO01BQzdDLENBQUMsTUFBTTtRQUNMcUIsSUFBSSxDQUFDQyxRQUFRLENBQUM0RSxjQUFjLENBQUMsSUFBSSxDQUFDNUcsSUFBSSxDQUFDVSxJQUFJLENBQUM7TUFDOUM7SUFDRixDQUFDO0lBQ0QsSUFBSSxDQUFDaUIsSUFBSSxDQUFDRyxnQkFBZ0IsQ0FBQzJDLGdCQUFnQixDQUFDLE9BQU8sRUFBRStCLDRCQUE0QixDQUFDO0lBQ2xGLElBQUksQ0FBQ3BHLFdBQVcsQ0FBQ3dELEdBQUcsQ0FBQyxJQUFJYyxnQkFBVSxDQUFDLE1BQU07TUFBRSxJQUFJLENBQUMvQyxJQUFJLENBQUNHLGdCQUFnQixDQUFDNkMsbUJBQW1CLENBQUMsT0FBTyxFQUFFNkIsNEJBQTRCLENBQUM7SUFBQyxDQUFDLENBQUMsQ0FBQztJQUVySSxNQUFNSywwQkFBMEIsR0FBSXpDLEtBQUssSUFBSztNQUM1QyxNQUFNMEMsTUFBTSxHQUFHMUMsS0FBSyxDQUFDMEMsTUFBTSxDQUFDQyxPQUFPLENBQUMsR0FBRyxDQUFDO01BQ3hDLElBQUlELE1BQU0sRUFBRTtRQUNWMUMsS0FBSyxDQUFDQyxlQUFlLEVBQUU7UUFDdkJELEtBQUssQ0FBQ3FDLGNBQWMsRUFBRTtRQUN0QixJQUFJSyxNQUFNLENBQUNFLElBQUksSUFBSUYsTUFBTSxDQUFDRSxJQUFJLENBQUNDLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRTtVQUNsRGxGLElBQUksQ0FBQ21GLFNBQVMsQ0FBQ0MsSUFBSSxDQUFDTCxNQUFNLENBQUNFLElBQUksQ0FBQztRQUNsQztNQUNGO0lBQ0YsQ0FBQztJQUNELElBQUksQ0FBQ3JGLElBQUksQ0FBQ2tDLGNBQWMsQ0FBQ1ksZ0JBQWdCLENBQUMsT0FBTyxFQUFFb0MsMEJBQTBCLENBQUM7SUFDOUUsSUFBSSxDQUFDekcsV0FBVyxDQUFDd0QsR0FBRyxDQUFDLElBQUljLGdCQUFVLENBQUMsTUFBTTtNQUFFLElBQUksQ0FBQy9DLElBQUksQ0FBQ2tDLGNBQWMsQ0FBQ2MsbUJBQW1CLENBQUMsT0FBTyxFQUFFa0MsMEJBQTBCLENBQUM7SUFBQyxDQUFDLENBQUMsQ0FBQztFQUNuSTtFQUVBTyxPQUFPLEdBQUk7SUFDVCxJQUFJLENBQUNoSCxXQUFXLENBQUNpSCxPQUFPLEVBQUU7SUFDMUIsT0FBT2hHLGFBQUksQ0FBQytGLE9BQU8sQ0FBQyxJQUFJLENBQUM7RUFDM0I7RUFFQTFGLGtCQUFrQixHQUFJO0lBQ3BCLElBQUksQ0FBQ3BCLE1BQU0sQ0FBQ2dILE1BQU0sQ0FBQyxJQUFBeEUsMEJBQW1CLEVBQUMsSUFBSSxDQUFDOUMsSUFBSSxDQUFDK0MsVUFBVSxDQUFDLEVBQUUsQ0FBQ00sR0FBRyxFQUFFa0UsVUFBVSxLQUFLO01BQ2pGLElBQUksQ0FBQ2xFLEdBQUcsSUFBSWtFLFVBQVUsRUFBRTtRQUN0QixJQUFJLENBQUM1RixJQUFJLENBQUMyRixNQUFNLENBQUNFLEdBQUcsR0FBSSxVQUFTRCxVQUFXLEVBQUM7TUFDL0M7SUFDRixDQUFDLENBQUM7SUFFRixJQUFJLENBQUNqSCxNQUFNLENBQUNtSCxPQUFPLENBQUMsSUFBSSxDQUFDekgsSUFBSSxDQUFDVSxJQUFJLEVBQUUsQ0FBQzJDLEdBQUcsRUFBRXFFLElBQUksS0FBSztNQUNqRDtNQUNBO01BQ0EsSUFBSSxDQUFDckUsR0FBRyxFQUFFO1FBQ1IsSUFBSXFFLElBQUksSUFBSSxJQUFJLEVBQUU7VUFDaEJBLElBQUksR0FBRyxDQUFDLENBQUM7UUFDWDtRQUVBLElBQUksSUFBSSxDQUFDMUgsSUFBSSxDQUFDZSxnQkFBZ0IsSUFBSSxJQUFJLENBQUNmLElBQUksQ0FBQ2UsZ0JBQWdCLENBQUNQLElBQUksS0FBSyxLQUFLLEVBQUU7VUFDM0UsSUFBSSxDQUFDbUIsSUFBSSxDQUFDZ0csWUFBWSxDQUFDaEUsU0FBUyxDQUFDOUIsTUFBTSxDQUFDLHFCQUFxQixDQUFDO1VBQzlELElBQUksQ0FBQ0YsSUFBSSxDQUFDZ0csWUFBWSxDQUFDaEUsU0FBUyxDQUFDQyxHQUFHLENBQUMsaUJBQWlCLENBQUM7VUFDdkQsSUFBSSxDQUFDakMsSUFBSSxDQUFDaUcsYUFBYSxDQUFDbEUsV0FBVyxHQUFHLElBQUksQ0FBQzFELElBQUksQ0FBQ2UsZ0JBQWdCLENBQUNDLEdBQUcsQ0FBQ3FFLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ25GLENBQUMsTUFBTTtVQUNMLElBQUksQ0FBQzFELElBQUksQ0FBQ2tHLGNBQWMsQ0FBQ25FLFdBQVcsR0FBR2dFLElBQUksQ0FBQ0ksZ0JBQWdCLEdBQUdKLElBQUksQ0FBQ0ksZ0JBQWdCLENBQUNDLGNBQWMsRUFBRSxHQUFHLEVBQUU7VUFDMUcsSUFBSSxDQUFDcEcsSUFBSSxDQUFDaUcsYUFBYSxDQUFDbEUsV0FBVyxHQUFHZ0UsSUFBSSxDQUFDdEcsU0FBUyxHQUFHc0csSUFBSSxDQUFDdEcsU0FBUyxDQUFDMkcsY0FBYyxFQUFFLEdBQUcsRUFBRTtRQUM3RjtNQUNGO0lBQ0YsQ0FBQyxDQUFDO0VBQ0o7RUFFQXZGLG9CQUFvQixHQUFJO0lBQ3RCLElBQUksQ0FBQ2IsSUFBSSxDQUFDOEIsWUFBWSxDQUFDQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUNJLGVBQWUsR0FBRyxJQUFJLENBQUNBLGVBQWUsQ0FBQ2pELE9BQU8sR0FBRyxJQUFJLEtBQUssSUFBSSxDQUFDYixJQUFJLENBQUNhLE9BQU87SUFDdEgsSUFBSSxJQUFJLENBQUNiLElBQUksQ0FBQ2UsZ0JBQWdCLElBQUksSUFBSSxDQUFDZixJQUFJLENBQUNlLGdCQUFnQixDQUFDUCxJQUFJLEtBQUssS0FBSyxFQUFFO01BQzNFLElBQUksQ0FBQ21CLElBQUksQ0FBQ2lHLGFBQWEsQ0FBQ2xFLFdBQVcsR0FBRyxJQUFJLENBQUMxRCxJQUFJLENBQUNlLGdCQUFnQixDQUFDQyxHQUFHLENBQUNxRSxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNuRjtJQUVBLElBQUksQ0FBQzJDLG1CQUFtQixFQUFFO0lBQzFCLElBQUksQ0FBQ0Msb0JBQW9CLEVBQUU7SUFDM0IsSUFBSSxDQUFDQyxtQkFBbUIsRUFBRTtFQUM1QjtFQUVBRixtQkFBbUIsR0FBSTtJQUNyQixJQUFJLElBQUksQ0FBQ0csV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUN4SCxjQUFjLEVBQUU7TUFDOUMsSUFBSSxDQUFDZ0IsSUFBSSxDQUFDdUMsY0FBYyxDQUFDN0IsS0FBSyxDQUFDQyxPQUFPLEdBQUcsRUFBRTtJQUM3QyxDQUFDLE1BQU07TUFDTCxJQUFJLENBQUNYLElBQUksQ0FBQ3VDLGNBQWMsQ0FBQzdCLEtBQUssQ0FBQ0MsT0FBTyxHQUFHLE1BQU07SUFDakQ7RUFDRjs7RUFFQTs7RUFFQTRGLG1CQUFtQixHQUFJO0lBQ3JCLElBQUksSUFBSSxDQUFDeEIsVUFBVSxFQUFFLEVBQUU7TUFDckIsSUFBSSxDQUFDMEIsb0JBQW9CLEVBQUU7SUFDN0IsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDNUQsT0FBTyxDQUFDYixTQUFTLENBQUMwRSxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUU7TUFDdEQsSUFBSSxDQUFDQyxtQkFBbUIsRUFBRTtJQUM1QjtFQUNGO0VBRUFBLG1CQUFtQixHQUFJO0lBQ3JCLElBQUksQ0FBQzlELE9BQU8sQ0FBQ2IsU0FBUyxDQUFDOUIsTUFBTSxDQUFDLFVBQVUsQ0FBQztJQUN6QyxJQUFJLElBQUksQ0FBQ3JCLElBQUksS0FBSyxPQUFPLEVBQUU7TUFDekIsSUFBSSxDQUFDbUIsSUFBSSxDQUFDRyxnQkFBZ0IsQ0FBQ08sS0FBSyxDQUFDQyxPQUFPLEdBQUcsTUFBTTtJQUNuRDtJQUNBLElBQUksQ0FBQ1gsSUFBSSxDQUFDRyxnQkFBZ0IsQ0FBQ3lHLGFBQWEsQ0FBQyxlQUFlLENBQUMsQ0FBQzdFLFdBQVcsR0FBRyxTQUFTO0lBQ2pGLElBQUksQ0FBQy9CLElBQUksQ0FBQ0csZ0JBQWdCLENBQUM2QixTQUFTLENBQUNDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQztJQUMvRCxJQUFJLENBQUNqQyxJQUFJLENBQUNHLGdCQUFnQixDQUFDNkIsU0FBUyxDQUFDOUIsTUFBTSxDQUFDLG9CQUFvQixDQUFDO0lBQ2pFLElBQUksQ0FBQ0YsSUFBSSxDQUFDQyxlQUFlLENBQUMrQixTQUFTLENBQUM5QixNQUFNLENBQUMsYUFBYSxDQUFDO0VBQzNEO0VBRUF1RyxvQkFBb0IsR0FBSTtJQUN0QixJQUFJLENBQUM1RCxPQUFPLENBQUNiLFNBQVMsQ0FBQ0MsR0FBRyxDQUFDLFVBQVUsQ0FBQztJQUN0QyxJQUFJLENBQUNqQyxJQUFJLENBQUNHLGdCQUFnQixDQUFDeUcsYUFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDN0UsV0FBVyxHQUFHLFFBQVE7SUFDaEYsSUFBSSxDQUFDL0IsSUFBSSxDQUFDRyxnQkFBZ0IsQ0FBQzZCLFNBQVMsQ0FBQ0MsR0FBRyxDQUFDLG9CQUFvQixDQUFDO0lBQzlELElBQUksQ0FBQ2pDLElBQUksQ0FBQ0csZ0JBQWdCLENBQUM2QixTQUFTLENBQUM5QixNQUFNLENBQUMscUJBQXFCLENBQUM7SUFDbEUsSUFBSSxDQUFDRixJQUFJLENBQUNDLGVBQWUsQ0FBQytCLFNBQVMsQ0FBQ0MsR0FBRyxDQUFDLGFBQWEsQ0FBQztJQUN0RCxJQUFJLENBQUNqQyxJQUFJLENBQUNHLGdCQUFnQixDQUFDMEcsUUFBUSxHQUFHLEtBQUs7RUFDN0M7O0VBRUE7O0VBRUFQLG9CQUFvQixHQUFJO0lBQ3RCLElBQUksSUFBSSxDQUFDUSxXQUFXLEVBQUUsRUFBRTtNQUN0QixJQUFJLENBQUNDLHFCQUFxQixFQUFFO0lBQzlCLENBQUMsTUFBTTtNQUNMLElBQUksQ0FBQ0Msd0JBQXdCLEVBQUU7SUFDakM7RUFDRjtFQUVBRCxxQkFBcUIsR0FBSTtJQUN2QixJQUFJLElBQUksQ0FBQzVILFVBQVUsSUFBSSxJQUFJLENBQUNJLE1BQU0sRUFBRTtNQUNsQyxJQUFJLENBQUNTLElBQUksQ0FBQ1MsaUJBQWlCLENBQUNDLEtBQUssQ0FBQ0MsT0FBTyxHQUFHLEVBQUU7TUFDOUMsSUFBSSxJQUFJLENBQUN4QixVQUFVLEVBQUU7UUFDbkIsSUFBSSxDQUFDYSxJQUFJLENBQUNxRSxZQUFZLENBQUN0QyxXQUFXLEdBQUksYUFBWSxJQUFJLENBQUM1QyxVQUFXLEVBQUM7TUFDckUsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDSSxNQUFNLEVBQUU7UUFDdEIsSUFBSSxDQUFDUyxJQUFJLENBQUNxRSxZQUFZLENBQUN0QyxXQUFXLEdBQUksYUFBWSxJQUFJLENBQUN4QyxNQUFNLENBQUNtRSxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBRSxFQUFDO01BQzlFO0lBQ0YsQ0FBQyxNQUFNO01BQ0wsSUFBSSxDQUFDMUQsSUFBSSxDQUFDUyxpQkFBaUIsQ0FBQ0MsS0FBSyxDQUFDQyxPQUFPLEdBQUcsTUFBTTtJQUNwRDtJQUVBLElBQUksQ0FBQ1gsSUFBSSxDQUFDTyxrQkFBa0IsQ0FBQ0csS0FBSyxDQUFDQyxPQUFPLEdBQUcsTUFBTTtJQUNuRCxJQUFJLENBQUNYLElBQUksQ0FBQ2lILHdCQUF3QixDQUFDdkcsS0FBSyxDQUFDQyxPQUFPLEdBQUcsRUFBRTtJQUNyRCxJQUFJLENBQUNYLElBQUksQ0FBQ1EsZUFBZSxDQUFDRSxLQUFLLENBQUNDLE9BQU8sR0FBRyxFQUFFO0VBQzlDO0VBRUFxRyx3QkFBd0IsR0FBSTtJQUMxQixJQUFJLENBQUNoSCxJQUFJLENBQUNRLGVBQWUsQ0FBQ0UsS0FBSyxDQUFDQyxPQUFPLEdBQUcsTUFBTTtJQUNoRCxNQUFNdUcsV0FBVyxHQUFHLElBQUksQ0FBQzNJLGNBQWMsQ0FBQzRJLGdCQUFnQixDQUFDL0csSUFBSSxDQUFDa0MsVUFBVSxFQUFFLENBQUM7SUFDM0UsSUFBSSxDQUFDLElBQUksQ0FBQy9ELGNBQWMsQ0FBQzZJLGdCQUFnQixDQUFDRixXQUFXLEVBQUUsSUFBSSxDQUFDN0ksSUFBSSxDQUFDLEVBQUU7TUFDakUsSUFBSSxDQUFDdUMsb0JBQW9CLEdBQUcsS0FBSztNQUNqQyxJQUFJLENBQUN5RywyQkFBMkIsRUFBRTtNQUNsQyxJQUFJLENBQUM5Riw4QkFBOEIsQ0FBQyxNQUFNO1FBQUUsSUFBSSxDQUFDOEYsMkJBQTJCLEVBQUU7TUFBQyxDQUFDLENBQUM7SUFDbkYsQ0FBQyxNQUFNO01BQ0wsSUFBSSxDQUFDQSwyQkFBMkIsRUFBRTtJQUNwQztFQUNGO0VBRUFBLDJCQUEyQixHQUFJO0lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUN6RyxvQkFBb0IsRUFBRTtNQUM5QixJQUFJLENBQUNaLElBQUksQ0FBQ08sa0JBQWtCLENBQUNHLEtBQUssQ0FBQ0MsT0FBTyxHQUFHLE1BQU07TUFDbkQsSUFBSSxDQUFDWCxJQUFJLENBQUNTLGlCQUFpQixDQUFDQyxLQUFLLENBQUNDLE9BQU8sR0FBRyxNQUFNO0lBQ3BELENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQ3hCLFVBQVUsSUFBSSxJQUFJLENBQUNJLE1BQU0sRUFBRTtNQUN6QyxJQUFJLENBQUNTLElBQUksQ0FBQ1MsaUJBQWlCLENBQUNDLEtBQUssQ0FBQ0MsT0FBTyxHQUFHLEVBQUU7TUFDOUMsSUFBSSxDQUFDWCxJQUFJLENBQUNPLGtCQUFrQixDQUFDRyxLQUFLLENBQUNDLE9BQU8sR0FBRyxNQUFNO0lBQ3JELENBQUMsTUFBTTtNQUNMLElBQUksQ0FBQ1gsSUFBSSxDQUFDUyxpQkFBaUIsQ0FBQ0MsS0FBSyxDQUFDQyxPQUFPLEdBQUcsTUFBTTtNQUNsRCxJQUFJLENBQUNYLElBQUksQ0FBQ08sa0JBQWtCLENBQUNHLEtBQUssQ0FBQ0MsT0FBTyxHQUFHLEVBQUU7SUFDakQ7SUFDQSxJQUFJLENBQUNYLElBQUksQ0FBQ2lILHdCQUF3QixDQUFDdkcsS0FBSyxDQUFDQyxPQUFPLEdBQUcsTUFBTTtFQUMzRDtFQUVBZixZQUFZLENBQUVwQixPQUFPLEVBQUU7SUFDckIsSUFBSUEsT0FBTyxJQUFJQSxPQUFPLENBQUNnQixLQUFLLElBQUloQixPQUFPLENBQUNnQixLQUFLLENBQUNDLFNBQVMsRUFBRTtNQUN2RCxJQUFJLENBQUNPLElBQUksQ0FBQ3NILGdCQUFnQixDQUFDNUcsS0FBSyxDQUFDQyxPQUFPLEdBQUcsRUFBRTtJQUMvQyxDQUFDLE1BQU07TUFDTCxJQUFJLENBQUNYLElBQUksQ0FBQ3NILGdCQUFnQixDQUFDNUcsS0FBSyxDQUFDQyxPQUFPLEdBQUcsTUFBTTtJQUNuRDtJQUVBLElBQUluQyxPQUFPLElBQUlBLE9BQU8sQ0FBQ2dCLEtBQUssSUFBSWhCLE9BQU8sQ0FBQ2dCLEtBQUssQ0FBQytILEtBQUssRUFBRTtNQUNuRCxJQUFJLENBQUN2SCxJQUFJLENBQUN3SCxZQUFZLENBQUM5RyxLQUFLLENBQUNDLE9BQU8sR0FBRyxFQUFFO0lBQzNDLENBQUMsTUFBTTtNQUNMLElBQUksQ0FBQ1gsSUFBSSxDQUFDd0gsWUFBWSxDQUFDOUcsS0FBSyxDQUFDQyxPQUFPLEdBQUcsTUFBTTtJQUMvQztFQUNGO0VBRUE4RyxtQ0FBbUMsR0FBSTtJQUNyQyxJQUFJLENBQUN6SCxJQUFJLENBQUMwSCxpQkFBaUIsQ0FBQ3hILE1BQU0sRUFBRTtJQUNwQyxJQUFJLENBQUNGLElBQUksQ0FBQzJILGNBQWMsQ0FBQ3pILE1BQU0sRUFBRTtJQUNqQyxNQUFNO01BQUNjO0lBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQzNDLElBQUk7SUFDOUIsSUFBSTJDLFVBQVUsQ0FBQzRHLE9BQU8sS0FBSyxVQUFVLEVBQUU7TUFDckMsSUFBSSxDQUFDNUgsSUFBSSxDQUFDNkgsa0JBQWtCLENBQUM5RixXQUFXLEdBQUdmLFVBQVUsQ0FBQzhHLEtBQUssRUFBRTtJQUMvRCxDQUFDLE1BQU07TUFDTCxJQUFJLENBQUM5SCxJQUFJLENBQUM2SCxrQkFBa0IsQ0FBQzlGLFdBQVcsR0FBR2YsVUFBVSxDQUFDK0csUUFBUSxFQUFFO0lBQ2xFO0lBQ0EsSUFBSSxDQUFDL0gsSUFBSSxDQUFDbUQsYUFBYSxDQUFDbkIsU0FBUyxDQUFDOUIsTUFBTSxDQUFDLHFCQUFxQixDQUFDO0lBQy9ELElBQUksQ0FBQ0YsSUFBSSxDQUFDbUQsYUFBYSxDQUFDbkIsU0FBUyxDQUFDQyxHQUFHLENBQUMsaUJBQWlCLENBQUM7SUFDeEQsSUFBSSxDQUFDakMsSUFBSSxDQUFDcUUsWUFBWSxDQUFDckMsU0FBUyxDQUFDOUIsTUFBTSxDQUFDLHFCQUFxQixDQUFDO0lBQzlELElBQUksQ0FBQ0YsSUFBSSxDQUFDcUUsWUFBWSxDQUFDckMsU0FBUyxDQUFDQyxHQUFHLENBQUMsaUJBQWlCLENBQUM7RUFDekQ7RUFFQStGLHNCQUFzQixDQUFFN0ksVUFBVSxFQUFFO0lBQ2xDLElBQUksQ0FBQ0EsVUFBVSxHQUFHQSxVQUFVO0lBQzVCLElBQUksQ0FBQzBCLG9CQUFvQixFQUFFO0VBQzdCO0VBRUFoQixtQkFBbUIsR0FBSTtJQUNyQixJQUFJLENBQUNwQixXQUFXLENBQUN3RCxHQUFHLENBQUM3QixJQUFJLENBQUNDLFFBQVEsQ0FBQzRILHNCQUFzQixDQUFFNUosSUFBSSxJQUFLO01BQ2xFLElBQUlBLElBQUksQ0FBQ1UsSUFBSSxLQUFLLElBQUksQ0FBQ1YsSUFBSSxDQUFDVSxJQUFJLEVBQUU7UUFDaEMsSUFBSSxDQUFDd0gsbUJBQW1CLEVBQUU7TUFDNUI7SUFDRixDQUFDLENBQUMsQ0FBQztJQUVILElBQUksQ0FBQzlILFdBQVcsQ0FBQ3dELEdBQUcsQ0FBQzdCLElBQUksQ0FBQ0MsUUFBUSxDQUFDNkgsb0JBQW9CLENBQUU3SixJQUFJLElBQUs7TUFDaEUsSUFBSUEsSUFBSSxDQUFDVSxJQUFJLEtBQUssSUFBSSxDQUFDVixJQUFJLENBQUNVLElBQUksRUFBRTtRQUNoQyxJQUFJLENBQUN3SCxtQkFBbUIsRUFBRTtNQUM1QjtJQUNGLENBQUMsQ0FBQyxDQUFDO0lBRUgsSUFBSSxDQUFDOUgsV0FBVyxDQUFDd0QsR0FBRyxDQUFDN0IsSUFBSSxDQUFDK0gsTUFBTSxDQUFDQyxXQUFXLENBQUMsdUJBQXVCLEVBQUUsTUFBTTtNQUMxRSxJQUFJLENBQUM3QixtQkFBbUIsRUFBRTtJQUM1QixDQUFDLENBQUMsQ0FBQztJQUVILElBQUksQ0FBQzhCLHVCQUF1QixDQUFDLHFDQUFxQyxFQUFFLE1BQU07TUFDeEUsSUFBSSxDQUFDeEgsb0JBQW9CLEVBQUU7TUFDM0IsSUFBSSxDQUFDYixJQUFJLENBQUNtRCxhQUFhLENBQUMwRCxRQUFRLEdBQUcsSUFBSTtNQUN2QyxJQUFJLENBQUM3RyxJQUFJLENBQUNtRCxhQUFhLENBQUNuQixTQUFTLENBQUNDLEdBQUcsQ0FBQyxlQUFlLENBQUM7SUFDeEQsQ0FBQyxDQUFDO0lBRUYsSUFBSSxDQUFDb0csdUJBQXVCLENBQUMsaUNBQWlDLEVBQUUsTUFBTTtNQUNwRSxJQUFJLENBQUN4SCxvQkFBb0IsRUFBRTtNQUMzQixJQUFJLENBQUNiLElBQUksQ0FBQ3FFLFlBQVksQ0FBQ3dDLFFBQVEsR0FBRyxJQUFJO01BQ3RDLElBQUksQ0FBQzdHLElBQUksQ0FBQ3FFLFlBQVksQ0FBQ3JDLFNBQVMsQ0FBQ0MsR0FBRyxDQUFDLGVBQWUsQ0FBQztJQUN2RCxDQUFDLENBQUM7SUFFRixJQUFJLENBQUNvRyx1QkFBdUIsQ0FBQyx5Q0FBeUMsRUFBRSxNQUFNO01BQzVFLElBQUksQ0FBQ3hILG9CQUFvQixFQUFFO01BQzNCLElBQUksQ0FBQ2IsSUFBSSxDQUFDRyxnQkFBZ0IsQ0FBQzBHLFFBQVEsR0FBRyxJQUFJO01BQzFDLElBQUksQ0FBQzdHLElBQUksQ0FBQ1EsZUFBZSxDQUFDcUcsUUFBUSxHQUFHLElBQUk7TUFDekMsSUFBSSxDQUFDN0csSUFBSSxDQUFDUSxlQUFlLENBQUN3QixTQUFTLENBQUNDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQztJQUM1RCxDQUFDLENBQUM7SUFFRixJQUFJLENBQUNvRyx1QkFBdUIsQ0FBQywrRUFBK0UsRUFBRSxNQUFNO01BQ2xILE1BQU1DLFVBQVUsR0FBR2xJLElBQUksQ0FBQ0MsUUFBUSxDQUFDa0ksZ0JBQWdCLENBQUMsSUFBSSxDQUFDbEssSUFBSSxDQUFDVSxJQUFJLENBQUM7TUFDakUsTUFBTUcsT0FBTyxHQUFHb0osVUFBVSxJQUFJQSxVQUFVLENBQUNFLFFBQVEsR0FBR0YsVUFBVSxDQUFDRSxRQUFRLENBQUN0SixPQUFPLEdBQUcsSUFBSTtNQUN0RixJQUFJQSxPQUFPLEVBQUU7UUFDWCxJQUFJLENBQUNiLElBQUksQ0FBQ2EsT0FBTyxHQUFHQSxPQUFPO01BQzdCO01BQ0EsSUFBSSxDQUFDYyxJQUFJLENBQUNtRCxhQUFhLENBQUMwRCxRQUFRLEdBQUcsS0FBSztNQUN4QyxJQUFJLENBQUM3RyxJQUFJLENBQUNtRCxhQUFhLENBQUNuQixTQUFTLENBQUM5QixNQUFNLENBQUMsZUFBZSxDQUFDO01BQ3pELElBQUksQ0FBQ1csb0JBQW9CLEVBQUU7SUFDN0IsQ0FBQyxDQUFDO0lBRUYsSUFBSSxDQUFDd0gsdUJBQXVCLENBQUMsK0JBQStCLEVBQUUsTUFBTTtNQUNsRSxNQUFNQyxVQUFVLEdBQUdsSSxJQUFJLENBQUNDLFFBQVEsQ0FBQ2tJLGdCQUFnQixDQUFDLElBQUksQ0FBQ2xLLElBQUksQ0FBQ1UsSUFBSSxDQUFDO01BQ2pFLE1BQU15SixRQUFRLEdBQUdGLFVBQVUsR0FBR0EsVUFBVSxDQUFDRSxRQUFRLEdBQUcsSUFBSTtNQUN4RCxJQUFJQSxRQUFRLElBQUlBLFFBQVEsQ0FBQ3RKLE9BQU8sRUFBRTtRQUNoQyxJQUFJLENBQUNiLElBQUksQ0FBQ2EsT0FBTyxHQUFHc0osUUFBUSxDQUFDdEosT0FBTztNQUN0QztNQUVBLElBQUlzSixRQUFRLElBQUlBLFFBQVEsQ0FBQ3BKLGdCQUFnQixFQUFFO1FBQ3pDLElBQUksQ0FBQ2YsSUFBSSxDQUFDZSxnQkFBZ0IsR0FBR29KLFFBQVEsQ0FBQ3BKLGdCQUFnQjtNQUN4RDtNQUVBLElBQUksQ0FBQ0QsVUFBVSxHQUFHLElBQUk7TUFDdEIsSUFBSSxDQUFDSSxNQUFNLEdBQUcsSUFBSTtNQUNsQixJQUFJLENBQUNTLElBQUksQ0FBQ3FFLFlBQVksQ0FBQ3dDLFFBQVEsR0FBRyxLQUFLO01BQ3ZDLElBQUksQ0FBQzdHLElBQUksQ0FBQ3FFLFlBQVksQ0FBQ3JDLFNBQVMsQ0FBQzlCLE1BQU0sQ0FBQyxlQUFlLENBQUM7TUFDeEQsSUFBSSxDQUFDVyxvQkFBb0IsRUFBRTtJQUM3QixDQUFDLENBQUM7SUFFRixJQUFJLENBQUN3SCx1QkFBdUIsQ0FBQywyQ0FBMkMsRUFBRSxNQUFNO01BQzlFLElBQUksQ0FBQ3JJLElBQUksQ0FBQ3FFLFlBQVksQ0FBQ3dDLFFBQVEsR0FBRyxLQUFLO01BQ3ZDLElBQUksQ0FBQzdHLElBQUksQ0FBQ3FFLFlBQVksQ0FBQ3JDLFNBQVMsQ0FBQzlCLE1BQU0sQ0FBQyxlQUFlLENBQUM7TUFDeEQsSUFBSSxDQUFDVyxvQkFBb0IsRUFBRTtJQUM3QixDQUFDLENBQUM7SUFFRixJQUFJLENBQUN3SCx1QkFBdUIsQ0FBQyx1RkFBdUYsRUFBRSxNQUFNO01BQzFILElBQUksQ0FBQ2xKLFVBQVUsR0FBRyxJQUFJO01BQ3RCLElBQUksQ0FBQ0ksTUFBTSxHQUFHLElBQUk7TUFDbEIsSUFBSSxDQUFDUyxJQUFJLENBQUNHLGdCQUFnQixDQUFDMEcsUUFBUSxHQUFHLEtBQUs7TUFDM0MsSUFBSSxDQUFDN0csSUFBSSxDQUFDUSxlQUFlLENBQUNxRyxRQUFRLEdBQUcsS0FBSztNQUMxQyxJQUFJLENBQUM3RyxJQUFJLENBQUNRLGVBQWUsQ0FBQ3dCLFNBQVMsQ0FBQzlCLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQztNQUM3RCxJQUFJLENBQUNXLG9CQUFvQixFQUFFO0lBQzdCLENBQUMsQ0FBQztFQUNKO0VBRUFpRyxXQUFXLEdBQUk7SUFDYixPQUFPLElBQUksQ0FBQ3ZJLGNBQWMsQ0FBQ2tLLGtCQUFrQixDQUFDLElBQUksQ0FBQ3BLLElBQUksQ0FBQ1UsSUFBSSxDQUFDO0VBQy9EO0VBRUFnRyxVQUFVLEdBQUk7SUFDWixPQUFPM0UsSUFBSSxDQUFDQyxRQUFRLENBQUNxSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUNySyxJQUFJLENBQUNVLElBQUksQ0FBQztFQUN4RDtFQUVBeUgsV0FBVyxHQUFJO0lBQ2IsT0FBTyxJQUFJLENBQUNqSSxjQUFjLENBQUNvSyxrQkFBa0IsQ0FBQyxJQUFJLENBQUN0SyxJQUFJLENBQUNVLElBQUksQ0FBQztFQUMvRDtFQUVBc0osdUJBQXVCLENBQUU1RixLQUFLLEVBQUVqQixRQUFRLEVBQUU7SUFDeEMsSUFBSSxDQUFDL0MsV0FBVyxDQUFDd0QsR0FBRyxDQUFDLElBQUksQ0FBQzFELGNBQWMsQ0FBQ3FLLEVBQUUsQ0FBQ25HLEtBQUssRUFBRSxDQUFDO01BQUNwRSxJQUFJO01BQUV1RDtJQUFLLENBQUMsS0FBSztNQUNwRSxJQUFJdkQsSUFBSSxDQUFDQSxJQUFJLElBQUksSUFBSSxFQUFFO1FBQ3JCQSxJQUFJLEdBQUdBLElBQUksQ0FBQ0EsSUFBSTtNQUNsQjtNQUVBLE1BQU1vRyxXQUFXLEdBQUdwRyxJQUFJLENBQUNVLElBQUk7TUFDN0IsSUFBSTBGLFdBQVcsS0FBSyxJQUFJLENBQUNwRyxJQUFJLENBQUNVLElBQUksRUFBRTtRQUNsQ3lDLFFBQVEsQ0FBQ25ELElBQUksRUFBRXVELEtBQUssQ0FBQztNQUN2QjtJQUNGLENBQUMsQ0FBQyxDQUFDO0VBQ0w7O0VBRUE7QUFDRjtBQUNBOztFQUVFc0IsT0FBTyxHQUFJO0lBQ1QsSUFBSSxDQUFDM0UsY0FBYyxDQUFDMkUsT0FBTyxDQUFDLElBQUksQ0FBQ2YsZUFBZSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUNBLGVBQWUsR0FBRyxJQUFJLENBQUM5RCxJQUFJLEVBQUd1RCxLQUFLLElBQUs7TUFDdEcsSUFBSUEsS0FBSyxJQUFJLElBQUksRUFBRTtRQUNqQkQsT0FBTyxDQUFDQyxLQUFLLENBQUUsY0FBYSxJQUFJLENBQUMvQyxJQUFLLElBQUcsSUFBSSxDQUFDUixJQUFJLENBQUNVLElBQUssU0FBUSxFQUFFNkMsS0FBSyxDQUFDaUgsS0FBSyxJQUFJLElBQUksR0FBR2pILEtBQUssQ0FBQ2lILEtBQUssR0FBR2pILEtBQUssRUFBRUEsS0FBSyxDQUFDa0gsTUFBTSxDQUFDO01BQzVILENBQUMsTUFBTTtRQUNMO1FBQ0EsSUFBSSxJQUFJLENBQUMvRCxVQUFVLEVBQUUsRUFBRTtVQUNyQjNFLElBQUksQ0FBQ0MsUUFBUSxDQUFDMkUsYUFBYSxDQUFDLElBQUksQ0FBQzNHLElBQUksQ0FBQ1UsSUFBSSxDQUFDO1FBQzdDO01BQ0Y7SUFDRixDQUFDLENBQUM7RUFDSjtFQUVBd0UsTUFBTSxHQUFJO0lBQ1IsSUFBSSxDQUFDLElBQUksQ0FBQ3BFLFVBQVUsSUFBSSxDQUFDLElBQUksQ0FBQ0ksTUFBTSxFQUFFO01BQ3BDLE9BQU93SixPQUFPLENBQUNDLE9BQU8sRUFBRTtJQUMxQjtJQUVBLE1BQU0zSyxJQUFJLEdBQUcsSUFBSSxDQUFDOEQsZUFBZSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUNBLGVBQWUsR0FBRyxJQUFJLENBQUM5RCxJQUFJO0lBQzVFLE1BQU1hLE9BQU8sR0FBRyxJQUFJLENBQUNDLFVBQVUsR0FBSSxJQUFHLElBQUksQ0FBQ0EsVUFBVyxFQUFDLEdBQUksSUFBRyxJQUFJLENBQUNJLE1BQU0sQ0FBQ21FLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFFLEVBQUM7SUFDeEYsT0FBTyxJQUFJcUYsT0FBTyxDQUFDLENBQUNDLE9BQU8sRUFBRUMsTUFBTSxLQUFLO01BQ3RDLElBQUksQ0FBQzFLLGNBQWMsQ0FBQ2dGLE1BQU0sQ0FBQ2xGLElBQUksRUFBRSxJQUFJLENBQUNjLFVBQVUsRUFBRXlDLEtBQUssSUFBSTtRQUN6RCxJQUFJQSxLQUFLLElBQUksSUFBSSxFQUFFO1VBQ2pCeEIsSUFBSSxDQUFDOEksTUFBTSxDQUFDLEtBQUssRUFBRSx1QkFBdUIsRUFBRUMsY0FBYyxJQUFJO1lBQzVEQSxjQUFjLENBQUNYLFFBQVEsR0FBRztjQUN4QjNKLElBQUksRUFBRSxJQUFJLENBQUNBLElBQUk7Y0FDZkUsSUFBSSxFQUFFVixJQUFJLENBQUNVLElBQUk7Y0FDZkcsT0FBTztjQUNQa0ssWUFBWSxFQUFFeEgsS0FBSyxDQUFDeUgsT0FBTztjQUMzQkMsVUFBVSxFQUFFMUgsS0FBSyxDQUFDaUgsS0FBSztjQUN2QlUsV0FBVyxFQUFFM0gsS0FBSyxDQUFDa0g7WUFDckIsQ0FBQztVQUNILENBQUMsQ0FBQztVQUNGbkgsT0FBTyxDQUFDQyxLQUFLLENBQUUsWUFBVyxJQUFJLENBQUMvQyxJQUFLLElBQUdSLElBQUksQ0FBQ1UsSUFBSyxPQUFNRyxPQUFRLFlBQVcsRUFBRTBDLEtBQUssRUFBRUEsS0FBSyxDQUFDa0gsTUFBTSxJQUFJLElBQUksR0FBR2xILEtBQUssQ0FBQ2tILE1BQU0sR0FBRyxFQUFFLENBQUM7VUFDNUhHLE1BQU0sQ0FBQ3JILEtBQUssQ0FBQztRQUNmLENBQUMsTUFBTTtVQUNMb0gsT0FBTyxFQUFFO1FBQ1g7TUFDRixDQUFDLENBQUM7SUFDSixDQUFDLENBQUM7RUFDSjtFQUVBM0YsU0FBUyxHQUFJO0lBQ1gsSUFBSSxDQUFDOUUsY0FBYyxDQUFDOEUsU0FBUyxDQUFDLElBQUksQ0FBQ2hGLElBQUksRUFBR3VELEtBQUssSUFBSztNQUNsRCxJQUFJQSxLQUFLLElBQUksSUFBSSxFQUFFO1FBQ2pCRCxPQUFPLENBQUNDLEtBQUssQ0FBRSxnQkFBZSxJQUFJLENBQUMvQyxJQUFLLElBQUcsSUFBSSxDQUFDUixJQUFJLENBQUNVLElBQUssU0FBUSxFQUFFNkMsS0FBSyxDQUFDaUgsS0FBSyxJQUFJLElBQUksR0FBR2pILEtBQUssQ0FBQ2lILEtBQUssR0FBR2pILEtBQUssRUFBRUEsS0FBSyxDQUFDa0gsTUFBTSxDQUFDO01BQzlIO0lBQ0YsQ0FBQyxDQUFDO0VBQ0o7QUFDRjtBQUFDO0FBQUEifQ==