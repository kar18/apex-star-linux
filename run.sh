# this will do update and upgrade
apx update -y &&
apx upgrade -y &&
sudo apx install zsh -y && #(powerful shell bash alternative )
#make ZSH the default shell

sudo chmod +x ssdm_i3.sh &&
apx install lxpolkit  -y &&
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo &&  #! adding flatpak


apx install i3 -y && #the i3 window manager
apx install picom -y && #  (compositor for Xorg) (Opacity transparency)
apx install feh -y && # A fast and light image viewer and  wallpaper manager
apx install copyq -y && # Clipboard Manager with Advanced Features
apx install dunst -y && # Lightweight and customizable notification daemon
#sudo dnf install timeshift -y && # System restore tool for Linux.
apx install fonts-font-awesome -y && #Font Awesome is a font and icon toolkit
apx install sxhkd -y && # Simple X hotkey daemon
apx install ranger -y && # A VIM-inspired filemanager for the console
apx install rofi -y &&  # Application launcher, a window switcher and dmenu replacement
apx install udiskie -y && # Automounter for removable media
apx install volumeicon-alsa -y && # Lightweight volume control
apx install flameshot -y && # Powerful yet simple to use screenshot software
apx install partitionmanager -y && #Partition Editor for creating, reorganizing and deleting partitions
#sudo dnf install guake -y && #Guake is a drop-down terminal (F12)
apx install mpv -y && #Command line video player
apx install qbittorrent -y && # qBittorrent BitTorrent client
apx install bleachbit -y &&  # BleachBit disk space cleaner, privacy manager, and computer system optimizer.
apx install kdeconnect -y && # Multi-platform app that allows your devices to communicate
sudo dnf install ./rpm/sublime-text-4143-1.x86_64.rpm -y &&
apx install fastfetch -y &&
apx install tilda -y &&
apx install slock -y &&
apx install blueman -y &&
apx install xkill -y &&
apx install vim -y &&
apx install ./rpm/Ferdium-linux-6.2.3.rpm -y &&
#if you want Chromium base go with brave browser https://brave.com/linux/#release-channel-installation

#sudo dnf install

#Install Preload: if you have upto 16gb ram enable it
#sudo dnf copr enable elxreno/preload -y && sudo dnf install preload -y

rm -rf ~/.gtkrc-2.0 && # avoiding conflict
rm -rf ~/.zshrc && # avoiding conflict
cp -rf ./dotfile/* ~/ && #copy all the configuration file(nonehide dir)
cp -rf ./dotfile/.* ~/ && #copy all the configuration file(hide dir)
cp .keynavrc ~/ &&

#guake --restore-preferences ~/.config/guake/guake_conf && # restore guake terminal
#Install Media Codecs fix sound problem
#sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
#sudo dnf groupupdate sound-and-video
echo "wana set-up 'systemctl graphical.target' run ./ssdm_i3.sh dont know what is that ? just skip"
echo "if you want partation manager type 'sudo dnf install gparted fdisk'"
echo "Congratulation, Installation has been successful. Please restart your system"
echo "and run chsh -s /usr/bin/zsh"
